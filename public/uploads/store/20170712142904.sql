-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2017 at 09:30 AM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `checkedin_app`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAllPost` (IN `iUserID` BIGINT(20))  NO SQL
BEGIN
	SELECT p.iPostID,p.iUserID,p.iLifeCategoryID,p.vCaption,DATE_FORMAT(p.dCreatedDate,'%Y-%m-%d %H:%i:%s') as postedOn, u.vName,u.vImage,lc.vName as categoryName,IFNULL(GROUP_CONCAT(ap.vPhotoName),"") as photoList FROM tbl_life_post as p LEFT JOIN tbl_life_category as lc ON lc.iCategoryID = p.iLifeCategoryID  LEFT JOIN tbl_user as u ON u.iUserID =p.iUserID  LEFT JOIN tbl_album as al ON (al.iPostID = p.iPostID AND al.eStatus='y' )LEFT JOIN tbl_album_photos as ap ON (ap.iAlbumID = al.iAlbumID AND ap.eStatus = 'y') WHERE p.iUserID = iUserID;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emailtemplate`
--

CREATE TABLE `tbl_emailtemplate` (
  `iTemplateID` int(11) NOT NULL,
  `sender_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sender_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `reply_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email_body` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modify_by` int(11) NOT NULL,
  `modify_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_emailtemplate`
--

INSERT INTO `tbl_emailtemplate` (`iTemplateID`, `sender_name`, `sender_email`, `reply_email`, `subject`, `email_body`, `created_by`, `created_on`, `modify_by`, `modify_on`) VALUES
(1, 'Checkedin', 'hello@checkedintheapp.com', 'hello@checkedintheapp.com', 'Welcome to Checkedin !', '<table align="center" bgcolor="#494c50" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td align="center" background="http://52.36.34.178/assets/1566-783.jpg" data-bg="Main BG" style="background-size:cover; background-position:top;">\r\n			<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n				<tbody>\r\n					<tr>\r\n						<td height="50">&nbsp;</td>\r\n					</tr>\r\n					<!-- logo -->\r\n					<tr>\r\n						<td align="center" style="line-height: 0px;"><img alt="logo" src="http://checkedin.net.in/wp-content/uploads/2014/03/logo-icon.png" style="display:block; line-height:0px; font-size:0px; border:0px;" width="50" /></td>\r\n					</tr>\r\n					<!-- end logo -->\r\n					<tr>\r\n						<td height="20">&nbsp;</td>\r\n					</tr>\r\n					<!-- slogan -->\r\n					<tr>\r\n						<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:11px; color:#FFFFFF;text-transform:uppercase; letter-spacing:4px;"><singleline label="slogan">Discover Likeminded People Nearby.</singleline></td>\r\n					</tr>\r\n					<!-- end slogan -->\r\n					<tr>\r\n						<td height="40">&nbsp;</td>\r\n					</tr>\r\n					<tr>\r\n						<td align="center" width="600">\r\n						<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" style="border-radius:4px; box-shadow: 0px -3px 0px #D4D2D2;" width="95%">\r\n							<tbody>\r\n								<tr>\r\n									<td height="50">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td align="center">\r\n									<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%"><!-- title -->\r\n										<tbody>\r\n											<tr>\r\n												<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:36px; color:#3b3b3b; font-weight: bold; letter-spacing:4px;"><singleline label="title">Welcome to Checked<span style="color: #f4994a;">In</span></singleline></td>\r\n											</tr>\r\n											<!-- end title -->\r\n											<tr>\r\n												<td align="center">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="25">\r\n													<tbody>\r\n														<tr>\r\n															<td data-border-bottom-color="Dash" height="20" style="border-bottom:2px solid #df812d;">&nbsp;</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n												</td>\r\n											</tr>\r\n											<tr>\r\n												<td height="20">&nbsp;</td>\r\n											</tr>\r\n											<!-- content -->\r\n											<tr>\r\n												<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d; line-height:30px;"><multiline label="content"> <span style="font-family: \'Open Sans\', Arial, sans-serif; font-size: 18px; font-weight: bold; color: #4A4A4A; line-height: 28px;">Hey %MEMBER_NAME%,</span><br />\r\n												congratulations and welcome to CheckedIn. Before you start to check in in really cool places and make friends, it&rsquo;s a good idea to confirm your email address. This will improve your account security and if you forget your password, it will ensure you get your account back. And don&rsquo;t worry, we won&rsquo;t be spamming you!</multiline></td>\r\n											</tr>\r\n											<!-- end content -->\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n								<tr>\r\n									<td height="40">&nbsp;</td>\r\n								</tr>\r\n								<!-- button -->\r\n								<tr>\r\n									<td align="center">\r\n									<table align="center" bgcolor="#df812d" border="0" cellpadding="0" cellspacing="0" class="textbutton" data-bgcolor="Button" style=" border-radius:30px; box-shadow: 0px 1px 0px #D4D2D2;">\r\n										<tbody>\r\n											<tr>\r\n												<td align="center" data-link-color="Button Link" data-link-style="text-decoration:none; color:#FFFFFF;" data-size="Button" height="55" mc:edit="noti-1-5" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:18px; color:#FFFFFF; font-weight: bold;padding-left: 25px;padding-right: 25px;"><a data-color="Button Link" href="#" style="color:#ffffff;text-decoration:none;"><singleline label="button">Confirm Email</singleline> </a></td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n								<!-- end button -->\r\n								<tr>\r\n									<td height="45">&nbsp;</td>\r\n								</tr>\r\n								<!-- option -->\r\n								<tr>\r\n									<td align="center" bgcolor="#f3f3f3" data-bgcolor="Link BG" style=" border-bottom-left-radius:4px; border-bottom-right-radius:4px;">\r\n									<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n										<tbody>\r\n											<tr>\r\n												<td height="15">&nbsp;</td>\r\n											</tr>\r\n											<tr>\r\n												<td align="center" data-color="Main Text" data-link-color="Text Link" data-link-style="text-decoration:none; color:#7f8c8d;" mc:edit="noti-1-6" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#7f8c8d;">&nbsp;</td>\r\n											</tr>\r\n											<tr>\r\n												<td height="15">&nbsp;</td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n								<!-- end option -->\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td align="center" style="line-height:0px;"><img alt="img" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/07/16/9NKCh8sSoOMVamfiPg0Wzydu/Notification-1/images/point.png" style="display:block; line-height:0px; font-size:0px; border:0px;" /></td>\r\n					</tr>\r\n					<tr>\r\n						<td height="30">&nbsp;</td>\r\n					</tr>\r\n					<tr>\r\n						<td align="center">\r\n						<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n							<tbody>\r\n								<tr>\r\n									<td align="center" style="line-height:0xp;"><a href="#"><img alt="img" data-crop="false" editable="" label="social-1" mc:edit="noti-1-8" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/07/16/9NKCh8sSoOMVamfiPg0Wzydu/Notification-1/images/fb.png" style="display:block; line-height:0px; font-size:0px; border:0px;" width="16" /> </a></td>\r\n									<td width="20">&nbsp;</td>\r\n									<td align="center" style="line-height:0xp;"><a href="#"><img alt="img" data-crop="false" editable="" label="social-2" mc:edit="noti-1-9" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/07/16/9NKCh8sSoOMVamfiPg0Wzydu/Notification-1/images/tw.png" style="display:block; line-height:0px; font-size:0px; border:0px;" width="16" /> </a></td>\r\n									<td width="20">&nbsp;</td>\r\n									<td align="center" style="line-height:0xp;"><a href="#"><img alt="img" data-crop="false" editable="" label="social-3" mc:edit="noti-1-10" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/07/16/9NKCh8sSoOMVamfiPg0Wzydu/Notification-1/images/gg.png" style="display:block; line-height:0px; font-size:0px; border:0px;" width="16" /> </a></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n					<!-- end social -->\r\n					<tr>\r\n						<td height="15">&nbsp;</td>\r\n					</tr>\r\n					<!-- copyright -->\r\n					<tr>\r\n						<td align="center" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:13px; color:#FFFFFF;"><singleline label="copyright">&copy; 2016 -17 CheckedIn. All Rights Reserved.</singleline></td>\r\n					</tr>\r\n					<!-- end copyright -->\r\n					<tr>\r\n						<td height="30">&nbsp;</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 'CheckedIn', 'hello@checkedintheapp.com', 'hello@checkedintheapp.com', 'CheckedIn  - Your new password', '<meta charset="UTF-8" />\r\n<title></title>\r\n<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />\r\n<table bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<table align="center" bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0" width="600px">\r\n				<tbody>\r\n					<tr>\r\n						<td colspan="3" style="padding: 0 20px; text-align:center; font-family: \'Open Sans\', sans-serif;">\r\n						<h2 style="font-size: 35px; color: #6b6b78; margin-top: 50px; ">FORGOT YOUR CHECKEDIN PASSWORD?</h2>\r\n\r\n						<p style="color:#6b6b78; font-size: 16px;height : 50px;margin-bottom: 50px;">&nbsp;</p>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td align="center">\r\n						<table bgcolor="#ffffff" cellpadding="0" cellspacing="0" style="padding: 20px;" width="600px">\r\n							<tbody>\r\n								<tr>\r\n									<td style="padding: 0 0 15px; border-bottom: 2px solid #f9f9fb;">\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">Hello %NAME%</span></small></p>\r\n\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">NO WORRIES! TEAM CHECKEDIN IS HAPPY TO HELP YOU.</span></small></p>\r\n\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">Use below code to reset the password</span></small></p>\r\n\r\n									<p>%OTP%</p>\r\n\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">Need help? We are always here for you!</span></small></p>\r\n\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">Email address &ndash; hello@checkedintheapp.com</span></small></p>\r\n\r\n									<p><small><span style="font-size: 18px; color: #8e8e8e;">Support &ndash; +91-9981822000.</span></small></p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2015-08-13 00:00:00', 1, '2015-08-13 00:00:00'),
(3, 'CheckedIn', 'hello@checkedintheapp.com', 'hello@checkedintheapp.com', 'Greetings!', '<div class="ui-sortable" id="frame" style="min-height: 250px;">\r\n<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" data-bgcolor="Main BG" data-module="cta" mc:hideable="" mc:repeatable="layout" mc:variant="cta" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td height="20">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td align="center" background="http://52.36.34.178//assets/Email/header-bg.png" bgcolor="#184572" data-bg="CTA" data-bgcolor="CTA" style="background-size:cover; background-position:center;">\r\n			<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n				<tbody>\r\n					<tr>\r\n						<td align="center" width="600">\r\n						<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">\r\n							<tbody>\r\n								<tr>\r\n									<td height="25">&nbsp;</td>\r\n								</tr>\r\n								<!-- title -->\r\n								<tr>\r\n									<td align="center" data-color="CTA" data-size="CTA" mc:edit="main-1-85" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:24px; color:#ffffff;font-weight: bold;"><singleline label="title">You&#39;re Invited !</singleline></td>\r\n								</tr>\r\n								<!-- end title -->\r\n								<tr>\r\n									<td height="25">&nbsp;</td>\r\n								</tr>\r\n								<!-- button -->\r\n								<tr>\r\n									<td align="center">\r\n									<table align="center" bgcolor="#FF646A" border="0" cellpadding="0" cellspacing="0" data-bgcolor="Button" style="border-radius:30px;">\r\n										<tbody>\r\n											<tr>\r\n												<td align="center" data-link-color="Button Link" data-link-style="text-decoration:none; color:#ffffff;" data-size="Button" height="35" mc:edit="main-1-86" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:14px; color:#ffffff; padding-left:20px; padding-right:20px;"><a data-color="Button Link" href="http://checkedintheapp.com/" style="text-decoration:none; color:#ffffff"><singleline label="button">Way to web</singleline> </a></td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</td>\r\n								</tr>\r\n								<!-- end button -->\r\n								<tr>\r\n									<td height="35">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td align="center" style="line-height:0px;"><img alt="img" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2016/07/16/kXMDdSAmZrsaIFE5o3cHpG4z/Main-01/images/point-up.png" style="display:block; border:0px; line-height:0px; font-size:0px;" /></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td height="20">&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div>\r\n<table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="currentTable" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td align="center">\r\n			<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n				<tbody>\r\n					<tr>\r\n						<td height="20">&nbsp;</td>\r\n					</tr>\r\n					<tr>\r\n						<td align="center" width="600">\r\n						<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n							<tbody>\r\n								<tr>\r\n									<td align="center" style="text-align:center;vertical-align:top;font-size:0;"><!--[if (gte mso 9)|(IE)]>\r\n                                                    </td>\r\n                                                    <td align="center" style="text-align:center;vertical-align:top;font-size:0;">\r\n                                                    <![endif]-->\r\n									<div style="display:inline-block;vertical-align:top;"><!--right-->\r\n									<div style="display:inline-block;vertical-align:top;">\r\n									<table align="center" border="0" cellpadding="0" cellspacing="0">\r\n										<tbody>\r\n											<tr>\r\n												<td align="center" width="300">\r\n												<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">\r\n													<tbody>\r\n														<tr>\r\n															<td align="center" bgcolor="#ecf0f1" style="border-radius:4px;">\r\n															<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%">\r\n																<tbody>\r\n																	<tr>\r\n																		<td height="50">&nbsp;</td>\r\n																	</tr>\r\n																	<tr>\r\n																		<td align="center" data-color="Open Quote" data-size="Open Quote" style="font-family: \'Arial\', Open Sans, sans-serif; font-size:65px; color:#FF646A;  line-height:5px;">&ldquo;</td>\r\n																	</tr>\r\n																	<!-- content -->\r\n																	<tr>\r\n																		<td align="center" data-color="Content" data-link-color="Content" data-link-style="text-decoration:none; color:#FF646A;" data-size="Content" mc:edit="main-1-91" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:14px; color:#3b3b3b; line-height:28px;"><multiline label="content">You are invited by %NAME% to join checkedIn and be a part of the new social networking app.</multiline></td>\r\n																	</tr>\r\n																	<!-- end content -->\r\n																	<tr>\r\n																		<td height="30">&nbsp;</td>\r\n																	</tr>\r\n																</tbody>\r\n															</table>\r\n															</td>\r\n														</tr>\r\n														<tr>\r\n															<td align="center" style="line-height:0px" valign="top"><img alt="img" src="http://52.36.34.178/assets/Email/point-down.png" style="display:block; border:0px; line-height:0px; font-size:0px;" /></td>\r\n														</tr>\r\n														<tr>\r\n															<td height="20">&nbsp;</td>\r\n														</tr>\r\n														<!-- img -->\r\n														<tr>\r\n															<td align="center" style="line-height:0px"><img alt="img" src="%IMG_URL%" style="display:block; line-height:0px; font-size:0px; border:0px;" width="80" /></td>\r\n														</tr>\r\n														<!-- end img --><!--dash-->\r\n														<tr>\r\n															<td align="center">\r\n															<table align="center" border="0" cellpadding="0" cellspacing="0" width="30">\r\n																<tbody>\r\n																	<tr>\r\n																		<td data-border-bottom-color="Dash" height="20" style="border-bottom:3px solid #FF646A;">&nbsp;</td>\r\n																	</tr>\r\n																</tbody>\r\n															</table>\r\n															</td>\r\n														</tr>\r\n														<!--end dash-->\r\n														<tr>\r\n															<td height="10">&nbsp;</td>\r\n														</tr>\r\n														<!-- name -->\r\n														<tr>\r\n															<td align="center" data-color="Name" data-size="Name" mc:edit="main-1-93" style="font-family: \'Open Sans\', Arial, sans-serif; font-size:18px; color:#3b3b3b;font-weight: bold;"><singleline label="name">%NAME%</singleline></td>\r\n														</tr>\r\n														<!-- end name -->\r\n														<tr>\r\n															<td height="5">&nbsp;</td>\r\n														</tr>\r\n														<!-- detail --><!-- end detail -->\r\n													</tbody>\r\n												</table>\r\n												</td>\r\n											</tr>\r\n											<tr>\r\n												<td height="20">&nbsp;</td>\r\n											</tr>\r\n										</tbody>\r\n									</table>\r\n									</div>\r\n									<!--end right--></div>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>', 1, '2015-08-13 00:00:00', 1, '2015-08-13 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_follow_following`
--

CREATE TABLE `tbl_follow_following` (
  `iAutoID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `iFollowingID` bigint(20) UNSIGNED NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_follow_following`
--

INSERT INTO `tbl_follow_following` (`iAutoID`, `iUserID`, `iFollowingID`, `tUpdatedDate`) VALUES
(4, 3, 1, '2017-04-25 16:46:31'),
(5, 6, 5, '2017-07-06 06:15:39'),
(6, 32, 5, '2017-07-06 10:55:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_life_category`
--

CREATE TABLE `tbl_life_category` (
  `iCategoryID` int(10) UNSIGNED NOT NULL,
  `iParent` int(11) NOT NULL DEFAULT '0',
  `vName` varchar(150) NOT NULL,
  `vIcon` varchar(150) NOT NULL,
  `isPredefined` tinyint(1) NOT NULL DEFAULT '1',
  `eStatus` enum('y','n','d') NOT NULL,
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_life_category`
--

INSERT INTO `tbl_life_category` (`iCategoryID`, `iParent`, `vName`, `vIcon`, `isPredefined`, `eStatus`, `dCreatedDate`, `tUpdatedDate`) VALUES
(1, 0, 'Eat & Drink', 'ic_eat_drink.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:13:40'),
(2, 0, 'Sports you play', 'ic_sports_you_play.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:13:47'),
(3, 0, 'Travel Diaries', 'ic_travel_diaries.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:14:10'),
(4, 0, 'Fun & Celebration', 'ic_fun_celebration.png', 1, '', '2017-04-24 00:00:00', '2017-06-19 18:14:38'),
(5, 0, 'Reviews & Feedback', 'ic_reviews_feedback.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:14:45'),
(6, 0, 'Hobbies & Timepass', 'ic_hobbies_timepass.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:14:52'),
(7, 0, 'Reading', 'ic_reading.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:14:58'),
(8, 0, 'Watching', 'ic_watching.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:15:04'),
(9, 0, 'Hangout Zone', 'ic_hangout_zone.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:15:24'),
(10, 0, 'Thought of the day', 'ic_thought_day.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 18:15:42'),
(11, 0, 'Feeling', 'ic_feeling.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-22 17:02:14'),
(12, 0, 'Planning', 'ic_planning.png', 1, 'y', '2017-04-24 00:00:00', '2017-04-24 15:10:25'),
(13, 0, 'My Opinion', 'ic_my_opinion.png', 1, 'y', '2017-04-24 00:00:00', '2017-06-19 16:51:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_life_post`
--

CREATE TABLE `tbl_life_post` (
  `iPostID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `iLifeCategoryID` int(11) NOT NULL COMMENT 'For poll/my opinion category refer tbl_poll',
  `vCaption` varchar(255) NOT NULL DEFAULT '',
  `eStatus` enum('y','n','d') NOT NULL DEFAULT 'y',
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_life_post`
--

INSERT INTO `tbl_life_post` (`iPostID`, `iUserID`, `iLifeCategoryID`, `vCaption`, `eStatus`, `dCreatedDate`, `tUpdatedDate`) VALUES
(1, 1, 1, 'first post', 'y', '2017-04-26 16:51:47', '2017-04-26 16:51:47'),
(2, 1, 2, 'just enjoying', 'y', '2017-04-29 14:09:58', '2017-04-29 14:09:58'),
(3, 1, 2, 'just enjoying', 'y', '2017-04-29 14:12:12', '2017-04-29 14:12:12'),
(4, 1, 2, 'just enjoying', 'y', '2017-04-29 14:27:26', '2017-04-29 14:27:26'),
(5, 1, 13, '', 'y', '2017-04-30 11:39:00', '2017-04-30 11:39:00'),
(6, 1, 13, '', 'y', '2017-04-30 11:40:39', '2017-04-30 11:40:39'),
(7, 1, 13, '', 'y', '2017-04-30 11:40:50', '2017-04-30 11:40:50'),
(8, 5, 1, 'first post', 'y', '2017-06-14 06:09:55', '2017-06-14 06:09:55'),
(9, 5, 2, 'just enjoying', 'y', '2017-06-14 06:25:40', '2017-06-14 06:25:40'),
(10, 5, 1, 'just enjoying test', 'y', '2017-06-14 06:32:04', '2017-06-14 06:32:04'),
(11, 5, 13, '', 'y', '2017-06-14 06:34:51', '2017-06-14 06:34:51'),
(12, 6, 13, '', 'y', '2017-06-16 13:02:18', '2017-06-16 13:02:18'),
(13, 6, 13, '', 'y', '2017-06-17 04:19:53', '2017-06-17 04:19:53'),
(14, 6, 13, '', 'y', '2017-06-17 04:19:59', '2017-06-17 04:19:59'),
(15, 6, 13, '', 'y', '2017-06-17 04:20:12', '2017-06-17 04:20:12'),
(16, 6, 13, '', 'y', '2017-06-17 04:20:17', '2017-06-17 04:20:17'),
(17, 6, 13, '', 'y', '2017-06-17 04:20:58', '2017-06-17 04:20:58'),
(18, 1, 13, '', 'y', '2017-06-17 04:22:29', '2017-06-17 04:22:29'),
(19, 1, 13, '', 'y', '2017-06-17 04:23:21', '2017-06-17 04:23:21'),
(20, 1, 13, '', 'y', '2017-06-17 04:28:11', '2017-06-17 04:28:11'),
(21, 6, 13, '', 'y', '2017-06-17 04:31:29', '2017-06-17 04:31:29'),
(22, 6, 13, '', 'y', '2017-06-17 04:31:30', '2017-06-17 04:31:30'),
(23, 6, 13, '', 'y', '2017-06-17 04:36:44', '2017-06-17 04:36:44'),
(24, 6, 13, '', 'y', '2017-06-17 04:42:49', '2017-06-17 04:42:49'),
(25, 6, 13, '', 'y', '2017-06-17 04:42:50', '2017-06-17 04:42:50'),
(26, 6, 13, '', 'y', '2017-06-17 08:04:48', '2017-06-17 08:04:48'),
(27, 6, 13, '', 'y', '2017-06-17 08:08:12', '2017-06-17 08:08:12'),
(28, 6, 2, 'just enjoying', 'y', '2017-06-20 07:46:42', '2017-06-20 07:46:42'),
(35, 6, 2, 'dsfdsfdsf', 'y', '2017-06-22 05:38:28', '2017-06-22 05:38:28'),
(36, 6, 2, 'nice bro', 'y', '2017-06-22 05:41:25', '2017-06-22 05:41:25'),
(37, 13, 13, '', 'y', '2017-06-22 18:30:48', '2017-06-22 18:30:48'),
(38, 13, 13, '', 'y', '2017-06-22 18:31:15', '2017-06-22 18:31:15'),
(39, 13, 13, '', 'y', '2017-06-22 18:32:47', '2017-06-22 18:32:47'),
(40, 13, 13, '', 'y', '2017-06-22 18:33:52', '2017-06-22 18:33:52'),
(41, 13, 13, '', 'y', '2017-06-22 18:34:47', '2017-06-22 18:34:47'),
(42, 13, 13, '', 'y', '2017-06-22 18:39:06', '2017-06-22 18:39:06'),
(43, 13, 13, '', 'y', '2017-06-22 18:39:24', '2017-06-22 18:39:24'),
(44, 13, 13, '', 'y', '2017-06-22 18:40:37', '2017-06-22 18:40:37'),
(45, 13, 13, '', 'y', '2017-06-22 18:41:07', '2017-06-22 18:41:07'),
(46, 1, 13, '', 'y', '2017-06-22 18:43:07', '2017-06-22 18:43:07'),
(47, 1, 13, '', 'y', '2017-06-22 18:43:15', '2017-06-22 18:43:15'),
(48, 1, 13, '', 'y', '2017-06-22 18:43:17', '2017-06-22 18:43:17'),
(49, 1, 13, '', 'y', '2017-06-22 18:43:19', '2017-06-22 18:43:19'),
(50, 1, 13, '', 'y', '2017-06-22 18:43:36', '2017-06-22 18:43:36'),
(51, 1, 13, '', 'y', '2017-06-22 18:43:40', '2017-06-22 18:43:40'),
(52, 1, 13, '', 'y', '2017-06-22 18:43:41', '2017-06-22 18:43:41'),
(53, 1, 13, '', 'y', '2017-06-22 18:43:42', '2017-06-22 18:43:42'),
(54, 13, 13, '', 'y', '2017-06-22 18:43:46', '2017-06-22 18:43:46'),
(55, 1, 13, '', 'y', '2017-06-22 18:50:08', '2017-06-22 18:50:08'),
(56, 1, 13, '', 'y', '2017-06-22 18:50:23', '2017-06-22 18:50:23'),
(57, 13, 13, '', 'y', '2017-06-22 18:53:44', '2017-06-22 18:53:44'),
(58, 13, 13, '', 'y', '2017-06-22 18:53:58', '2017-06-22 18:53:58'),
(59, 13, 13, '', 'y', '2017-06-22 18:54:00', '2017-06-22 18:54:00'),
(60, 13, 13, '', 'y', '2017-06-22 18:54:01', '2017-06-22 18:54:01'),
(61, 13, 13, '', 'y', '2017-06-22 18:54:03', '2017-06-22 18:54:03'),
(62, 13, 13, '', 'y', '2017-06-22 18:54:04', '2017-06-22 18:54:04'),
(63, 13, 13, '', 'y', '2017-06-22 18:54:05', '2017-06-22 18:54:05'),
(64, 13, 13, '', 'y', '2017-06-22 18:54:07', '2017-06-22 18:54:07'),
(65, 13, 13, '', 'y', '2017-06-22 18:54:08', '2017-06-22 18:54:08'),
(66, 13, 13, '', 'y', '2017-06-22 18:54:09', '2017-06-22 18:54:09'),
(67, 13, 13, '', 'y', '2017-06-22 18:54:11', '2017-06-22 18:54:11'),
(68, 13, 13, '', 'y', '2017-06-22 18:54:12', '2017-06-22 18:54:12'),
(69, 1, 13, '', 'y', '2017-06-22 18:54:22', '2017-06-22 18:54:22'),
(70, 1, 13, '', 'y', '2017-06-22 18:55:20', '2017-06-22 18:55:20'),
(71, 1, 13, '', 'y', '2017-06-23 18:04:02', '2017-06-23 18:04:02'),
(72, 6, 2, 'asdasdasdas', 'y', '2017-06-24 13:23:57', '2017-06-24 13:23:57'),
(73, 13, 13, '', 'y', '2017-06-24 14:22:30', '2017-06-24 14:22:30'),
(74, 5, 11, 'the issued', 'y', '2017-06-27 09:24:44', '2017-06-27 09:24:44'),
(75, 5, 13, '', 'y', '2017-06-27 09:25:50', '2017-06-27 09:25:50'),
(76, 5, 13, '', 'y', '2017-06-27 09:25:54', '2017-06-27 09:25:54'),
(77, 5, 13, '', 'y', '2017-06-27 09:25:55', '2017-06-27 09:25:55'),
(78, 6, 13, '', 'y', '2017-06-27 09:26:40', '2017-06-27 09:26:40'),
(79, 5, 13, '', 'y', '2017-06-27 09:27:36', '2017-06-27 09:27:36'),
(80, 6, 13, '', 'y', '2017-06-27 09:37:27', '2017-06-27 09:37:27'),
(81, 13, 13, '', 'y', '2017-06-27 17:34:53', '2017-06-27 17:34:53'),
(82, 13, 13, '', 'y', '2017-06-27 17:41:27', '2017-06-27 17:41:27'),
(83, 13, 13, '', 'y', '2017-06-27 17:46:05', '2017-06-27 17:46:05'),
(84, 13, 13, '', 'y', '2017-06-27 17:54:09', '2017-06-27 17:54:09'),
(85, 13, 13, '', 'y', '2017-06-27 17:55:29', '2017-06-27 17:55:29'),
(86, 1, 13, '', 'y', '2017-06-27 17:57:39', '2017-06-27 17:57:39'),
(87, 13, 13, '', 'y', '2017-06-27 18:04:56', '2017-06-27 18:04:56'),
(88, 6, 13, '', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(89, 6, 1, 'dasdasdsad', 'y', '2017-06-28 10:36:06', '2017-06-28 10:36:06'),
(90, 23, 13, '', 'y', '2017-06-28 18:48:08', '2017-06-28 18:48:08'),
(91, 24, 13, '', 'y', '2017-06-29 03:49:10', '2017-06-29 03:49:10'),
(92, 23, 1, 'Hi', 'y', '2017-06-29 18:06:28', '2017-06-29 18:06:28'),
(93, 23, 2, 'Hello', 'y', '2017-06-29 18:10:11', '2017-06-29 18:10:11'),
(94, 23, 2, 'Hello', 'y', '2017-06-29 18:11:06', '2017-06-29 18:11:06'),
(95, 23, 3, 'The', 'y', '2017-06-29 18:31:04', '2017-06-29 18:31:04'),
(96, 23, 2, 'Hmm', 'y', '2017-06-29 18:42:03', '2017-06-29 18:42:03'),
(97, 23, 13, '', 'y', '2017-06-29 18:42:26', '2017-06-29 18:42:26'),
(98, 23, 9, 'Cool', 'y', '2017-06-29 18:42:37', '2017-06-29 18:42:37'),
(99, 25, 1, 'Cake', 'y', '2017-06-29 19:04:30', '2017-06-29 19:04:30'),
(100, 25, 13, '', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(101, 6, 2, 'sdasdas', 'y', '2017-06-30 07:22:15', '2017-06-30 07:22:15'),
(102, 6, 2, 'dasdasda', 'y', '2017-06-30 07:22:31', '2017-06-30 07:22:31'),
(103, 6, 3, 'fdsfdsfs', 'y', '2017-06-30 07:23:05', '2017-06-30 07:23:05'),
(104, 6, 2, 'dasdas', 'y', '2017-06-30 08:27:02', '2017-06-30 08:27:02'),
(105, 6, 2, 'fdfsfs', 'y', '2017-06-30 08:28:10', '2017-06-30 08:28:10'),
(106, 6, 2, 'dasdasd', 'y', '2017-06-30 08:29:24', '2017-06-30 08:29:24'),
(107, 6, 2, 'ggjgjg', 'y', '2017-06-30 08:31:28', '2017-06-30 08:31:28'),
(108, 6, 2, 'dsfdsf', 'y', '2017-06-30 08:32:12', '2017-06-30 08:32:12'),
(109, 6, 2, 'dfsfdsf', 'y', '2017-06-30 08:33:02', '2017-06-30 08:33:02'),
(110, 6, 2, 'fsdfsdf', 'y', '2017-06-30 08:33:57', '2017-06-30 08:33:57'),
(111, 6, 2, 'fcbcg', 'y', '2017-06-30 08:34:18', '2017-06-30 08:34:18'),
(112, 6, 2, 'dfddg', 'y', '2017-06-30 08:40:08', '2017-06-30 08:40:08'),
(113, 6, 13, '', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(114, 6, 13, '', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(115, 6, 13, '', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(116, 6, 13, '', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(117, 6, 13, '', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(118, 6, 13, '', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(119, 6, 2, 'sadsad', 'y', '2017-06-30 08:47:28', '2017-06-30 08:47:28'),
(120, 6, 2, 'dfdsf', 'y', '2017-06-30 08:48:26', '2017-06-30 08:48:26'),
(121, 6, 13, '', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(122, 23, 1, 'This', 'y', '2017-07-01 18:38:39', '2017-07-01 18:38:39'),
(123, 23, 1, 'Hmmm', 'y', '2017-07-01 18:46:57', '2017-07-01 18:46:57'),
(124, 23, 1, 'Hi', 'y', '2017-07-01 19:07:33', '2017-07-01 19:07:33'),
(125, 23, 2, 'Hmm', 'y', '2017-07-01 19:08:33', '2017-07-01 19:08:33'),
(126, 23, 2, 'The', 'y', '2017-07-01 19:11:15', '2017-07-01 19:11:15'),
(127, 23, 7, 'The', 'y', '2017-07-01 19:12:34', '2017-07-01 19:12:34'),
(128, 23, 5, 'The', 'y', '2017-07-01 19:17:37', '2017-07-01 19:17:37'),
(129, 23, 7, 'Could', 'y', '2017-07-01 19:23:30', '2017-07-01 19:23:30'),
(130, 27, 13, '', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(131, 28, 1, 'At office', 'y', '2017-07-04 07:16:04', '2017-07-04 07:16:04'),
(132, 6, 5, 'fdfdsfdsf', 'y', '2017-07-05 15:27:28', '2017-07-05 15:27:28'),
(133, 32, 5, 'asdsadsad', 'y', '2017-07-06 12:16:37', '2017-07-06 12:16:37'),
(134, 32, 5, 'dsadsa', 'y', '2017-07-06 12:17:39', '2017-07-06 12:17:39'),
(135, 32, 1, 'first post', 'y', '2017-07-06 12:19:47', '2017-07-06 12:19:47'),
(136, 32, 5, 'sdsadasdsaad', 'y', '2017-07-06 12:20:35', '2017-07-06 12:20:35'),
(137, 32, 5, 'dsdasdsadsad', 'y', '2017-07-06 12:22:08', '2017-07-06 12:22:08'),
(138, 32, 11, 'dsad', 'y', '2017-07-06 14:02:36', '2017-07-06 14:02:36'),
(139, 32, 9, 'dsadasd', 'y', '2017-07-06 14:03:03', '2017-07-06 14:03:03'),
(140, 32, 2, 'dasdsadas', 'y', '2017-07-07 14:03:59', '2017-07-07 14:03:59'),
(141, 32, 1, 'dasdasd', 'y', '2017-07-09 06:57:14', '2017-07-09 06:57:14'),
(142, 6, 2, 'oipio', 'y', '2017-07-09 10:06:02', '2017-07-09 10:06:02'),
(143, 6, 2, 'just enjoying', 'y', '2017-07-10 07:38:27', '2017-07-10 07:38:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_poll_answer`
--

CREATE TABLE `tbl_poll_answer` (
  `iAnswerID` bigint(20) UNSIGNED NOT NULL,
  `iQuestionID` bigint(20) UNSIGNED NOT NULL,
  `vAnswer` varchar(255) NOT NULL,
  `eStatus` enum('y','n','d') NOT NULL DEFAULT 'y',
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_poll_answer`
--

INSERT INTO `tbl_poll_answer` (`iAnswerID`, `iQuestionID`, `vAnswer`, `eStatus`, `dCreatedDate`, `tUpdatedDate`) VALUES
(1, 1, 'answer1', 'y', '2017-04-30 11:40:39', '2017-04-30 11:40:39'),
(2, 1, 'answer2', 'y', '2017-04-30 11:40:39', '2017-04-30 11:40:39'),
(3, 1, 'answer3', 'y', '2017-04-30 11:40:39', '2017-04-30 11:40:39'),
(4, 2, 'answer1', 'y', '2017-04-30 11:40:50', '2017-04-30 11:40:50'),
(5, 2, 'answer2', 'y', '2017-04-30 11:40:50', '2017-04-30 11:40:50'),
(6, 2, 'answer3', 'y', '2017-04-30 11:40:50', '2017-04-30 11:40:50'),
(7, 3, 'answer1', 'y', '2017-06-14 06:34:51', '2017-06-14 06:34:51'),
(8, 3, 'answer2', 'y', '2017-06-14 06:34:51', '2017-06-14 06:34:51'),
(9, 3, 'answer3', 'y', '2017-06-14 06:34:51', '2017-06-14 06:34:51'),
(10, 10, 'answer1', 'y', '2017-06-17 04:22:29', '2017-06-17 04:22:29'),
(11, 10, 'answer2', 'y', '2017-06-17 04:22:29', '2017-06-17 04:22:29'),
(12, 10, 'answer3', 'y', '2017-06-17 04:22:29', '2017-06-17 04:22:29'),
(13, 12, 'answer1', 'y', '2017-06-17 04:28:11', '2017-06-17 04:28:11'),
(14, 12, 'answer2', 'y', '2017-06-17 04:28:11', '2017-06-17 04:28:11'),
(15, 18, 'dsfdsfds', 'y', '2017-06-17 08:04:48', '2017-06-17 08:04:48'),
(16, 18, ' fdsfds', 'y', '2017-06-17 08:04:48', '2017-06-17 08:04:48'),
(17, 19, 'dsadasdsa', 'y', '2017-06-17 08:08:12', '2017-06-17 08:08:12'),
(18, 19, 'dasdasd', 'y', '2017-06-17 08:08:12', '2017-06-17 08:08:13'),
(19, 20, 'nbnbnbnvnv', 'y', '2017-06-20 09:05:33', '2017-06-20 09:05:33'),
(20, 20, 'vbnvbnvbnv', 'y', '2017-06-20 09:05:33', '2017-06-20 09:05:33'),
(21, 25, 'answer1', 'y', '2017-06-22 18:34:47', '2017-06-22 18:34:47'),
(22, 25, 'answer2', 'y', '2017-06-22 18:34:47', '2017-06-22 18:34:47'),
(23, 25, 'answer3', 'y', '2017-06-22 18:34:47', '2017-06-22 18:34:47'),
(24, 27, 'answer1', 'y', '2017-06-22 18:39:24', '2017-06-22 18:39:25'),
(25, 27, 'answer2', 'y', '2017-06-22 18:39:24', '2017-06-22 18:39:25'),
(26, 27, 'answer3', 'y', '2017-06-22 18:39:24', '2017-06-22 18:39:25'),
(27, 28, 'Hi', 'y', '2017-06-22 18:40:37', '2017-06-22 18:40:37'),
(28, 28, 'The', 'y', '2017-06-22 18:40:37', '2017-06-22 18:40:37'),
(29, 28, 'Now', 'y', '2017-06-22 18:40:37', '2017-06-22 18:40:37'),
(30, 29, 'Hi', 'y', '2017-06-22 18:41:07', '2017-06-22 18:41:07'),
(31, 29, 'The', 'y', '2017-06-22 18:41:07', '2017-06-22 18:41:07'),
(32, 29, 'Now', 'y', '2017-06-22 18:41:07', '2017-06-22 18:41:07'),
(33, 30, 'answer1', 'y', '2017-06-22 18:43:07', '2017-06-22 18:43:07'),
(34, 30, 'answer2', 'y', '2017-06-22 18:43:07', '2017-06-22 18:43:07'),
(35, 30, 'answer3', 'y', '2017-06-22 18:43:07', '2017-06-22 18:43:07'),
(36, 31, 'answer1', 'y', '2017-06-22 18:43:15', '2017-06-22 18:43:15'),
(37, 31, 'answer2', 'y', '2017-06-22 18:43:15', '2017-06-22 18:43:15'),
(38, 31, 'answer3', 'y', '2017-06-22 18:43:15', '2017-06-22 18:43:15'),
(39, 32, 'answer1', 'y', '2017-06-22 18:43:17', '2017-06-22 18:43:17'),
(40, 32, 'answer2', 'y', '2017-06-22 18:43:17', '2017-06-22 18:43:17'),
(41, 32, 'answer3', 'y', '2017-06-22 18:43:17', '2017-06-22 18:43:17'),
(42, 33, 'answer1', 'y', '2017-06-22 18:43:19', '2017-06-22 18:43:19'),
(43, 33, 'answer2', 'y', '2017-06-22 18:43:19', '2017-06-22 18:43:19'),
(44, 33, 'answer3', 'y', '2017-06-22 18:43:19', '2017-06-22 18:43:19'),
(45, 34, 'answer1', 'y', '2017-06-22 18:43:36', '2017-06-22 18:43:36'),
(46, 34, 'answer2', 'y', '2017-06-22 18:43:36', '2017-06-22 18:43:36'),
(47, 34, 'answer3', 'y', '2017-06-22 18:43:36', '2017-06-22 18:43:36'),
(48, 35, 'answer1', 'y', '2017-06-22 18:43:40', '2017-06-22 18:43:40'),
(49, 35, 'answer2', 'y', '2017-06-22 18:43:40', '2017-06-22 18:43:40'),
(50, 35, 'answer3', 'y', '2017-06-22 18:43:40', '2017-06-22 18:43:40'),
(51, 36, 'answer1', 'y', '2017-06-22 18:43:41', '2017-06-22 18:43:41'),
(52, 36, 'answer2', 'y', '2017-06-22 18:43:41', '2017-06-22 18:43:41'),
(53, 36, 'answer3', 'y', '2017-06-22 18:43:41', '2017-06-22 18:43:41'),
(54, 37, 'answer1', 'y', '2017-06-22 18:43:42', '2017-06-22 18:43:42'),
(55, 37, 'answer2', 'y', '2017-06-22 18:43:42', '2017-06-22 18:43:42'),
(56, 37, 'answer3', 'y', '2017-06-22 18:43:42', '2017-06-22 18:43:42'),
(57, 39, 'answer1', 'y', '2017-06-22 18:50:08', '2017-06-22 18:50:08'),
(58, 39, 'answer2', 'y', '2017-06-22 18:50:08', '2017-06-22 18:50:08'),
(59, 39, 'answer3', 'y', '2017-06-22 18:50:08', '2017-06-22 18:50:08'),
(60, 40, 'answer1', 'y', '2017-06-22 18:50:23', '2017-06-22 18:50:23'),
(61, 40, 'answer2', 'y', '2017-06-22 18:50:23', '2017-06-22 18:50:23'),
(62, 40, 'answer3', 'y', '2017-06-22 18:50:23', '2017-06-22 18:50:23'),
(63, 53, 'answer1', 'y', '2017-06-22 18:54:22', '2017-06-22 18:54:22'),
(64, 53, 'answer2', 'y', '2017-06-22 18:54:22', '2017-06-22 18:54:22'),
(65, 53, 'answer3', 'y', '2017-06-22 18:54:22', '2017-06-22 18:54:22'),
(66, 54, 'answer1', 'y', '2017-06-22 18:55:20', '2017-06-22 18:55:20'),
(67, 54, 'answer2', 'y', '2017-06-22 18:55:20', '2017-06-22 18:55:20'),
(68, 54, 'answer3', 'y', '2017-06-22 18:55:20', '2017-06-22 18:55:20'),
(69, 55, 'answer1', 'y', '2017-06-23 18:04:02', '2017-06-23 18:04:02'),
(70, 55, 'answer2', 'y', '2017-06-23 18:04:02', '2017-06-23 18:04:02'),
(71, 55, 'answer3', 'y', '2017-06-23 18:04:02', '2017-06-23 18:04:02'),
(72, 60, 'dasdas', 'y', '2017-06-27 09:26:40', '2017-06-27 09:26:40'),
(73, 60, 'dsadasd', 'y', '2017-06-27 09:26:40', '2017-06-27 09:26:40'),
(74, 61, 'nn', 'y', '2017-06-27 09:27:36', '2017-06-27 09:27:36'),
(75, 61, 'nn', 'y', '2017-06-27 09:27:36', '2017-06-27 09:27:36'),
(76, 62, 'yes', 'y', '2017-06-27 09:37:27', '2017-06-27 09:37:27'),
(77, 62, 'no', 'y', '2017-06-27 09:37:27', '2017-06-27 09:37:27'),
(78, 62, 'so-so', 'y', '2017-06-27 09:37:27', '2017-06-27 09:37:27'),
(79, 69, 'answer1', 'y', '2017-06-27 17:57:39', '2017-06-27 17:57:39'),
(80, 69, 'answer2', 'y', '2017-06-27 17:57:39', '2017-06-27 17:57:39'),
(81, 69, 'answer3', 'y', '2017-06-27 17:57:39', '2017-06-27 17:57:39'),
(82, 70, 'The', 'y', '2017-06-27 18:04:56', '2017-06-27 18:04:56'),
(83, 70, 'Hi', 'y', '2017-06-27 18:04:56', '2017-06-27 18:04:56'),
(84, 71, '[', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(85, 71, '"', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(86, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(87, 71, 'a', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(88, 71, 's', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(89, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(90, 71, 's', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(91, 71, 'a', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(92, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(93, 71, '"', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(94, 71, ',', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(95, 71, '"', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(96, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(97, 71, 's', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(98, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(99, 71, 's', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(100, 71, 'a', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(101, 71, 'd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(102, 71, '"', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(103, 71, ']', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(104, 72, 'Hi', 'y', '2017-06-28 18:48:08', '2017-06-28 18:48:08'),
(105, 72, 'The', 'y', '2017-06-28 18:48:08', '2017-06-28 18:48:08'),
(106, 72, 'Ok', 'y', '2017-06-28 18:48:08', '2017-06-28 18:48:08'),
(107, 73, 'Gaga', 'y', '2017-06-29 03:49:10', '2017-06-29 03:49:10'),
(108, 73, 'Agaha', 'y', '2017-06-29 03:49:10', '2017-06-29 03:49:10'),
(109, 73, 'Gaga', 'y', '2017-06-29 03:49:10', '2017-06-29 03:49:10'),
(110, 74, 'I', 'y', '2017-06-29 18:42:26', '2017-06-29 18:42:26'),
(111, 74, 'The', 'y', '2017-06-29 18:42:26', '2017-06-29 18:42:26'),
(112, 75, '[', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(113, 75, '"', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(114, 75, 'y', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(115, 75, '"', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(116, 75, ',', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(117, 75, '"', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(118, 75, 'g', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(119, 75, '"', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(120, 75, ']', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(121, 76, '[', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(122, 76, '"', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(123, 76, 'g', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(124, 76, 'f', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(125, 76, 'd', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(126, 76, 'g', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(127, 76, 'd', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(128, 76, 'f', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(129, 76, '"', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(130, 76, ',', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(131, 76, '"', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(132, 76, 'g', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(133, 76, 'd', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(134, 76, 'f', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(135, 76, 'g', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(136, 76, '"', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(137, 76, ']', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(138, 77, '[', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(139, 77, '"', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(140, 77, 'd', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(141, 77, 's', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(142, 77, 'a', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(143, 77, 'd', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(144, 77, 'a', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(145, 77, 's', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(146, 77, 'd', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(147, 77, '"', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(148, 77, ',', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(149, 77, '"', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(150, 77, 'd', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(151, 77, 's', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(152, 77, 'a', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(153, 77, 'd', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(154, 77, 'a', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(155, 77, 's', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(156, 77, '"', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(157, 77, ']', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(158, 78, '[', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(159, 78, '"', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(160, 78, 's', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(161, 78, 'd', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(162, 78, 'f', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(163, 78, 's', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(164, 78, 'd', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(165, 78, 'f', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(166, 78, '"', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(167, 78, ',', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(168, 78, '"', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(169, 78, 'f', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(170, 78, 's', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(171, 78, 'd', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(172, 78, 'f', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(173, 78, 's', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(174, 78, 'd', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(175, 78, '"', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(176, 78, ']', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(177, 79, '[', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(178, 79, '"', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(179, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(180, 79, 'a', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(181, 79, 's', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(182, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(183, 79, 's', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(184, 79, 'a', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(185, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(186, 79, '"', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(187, 79, ',', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(188, 79, '"', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(189, 79, 's', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(190, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(191, 79, 'a', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(192, 79, 's', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(193, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(194, 79, 'a', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(195, 79, 's', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(196, 79, 'd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(197, 79, '"', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(198, 79, ']', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(199, 80, '[', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(200, 80, '"', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(201, 80, 's', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(202, 80, 'd', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(203, 80, 'a', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(204, 80, 's', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(205, 80, 'd', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(206, 80, '"', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(207, 80, ',', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(208, 80, '"', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(209, 80, 's', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(210, 80, 'a', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(211, 80, 'd', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(212, 80, 'a', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(213, 80, 's', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(214, 80, '"', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(215, 80, ']', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(216, 81, '[', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(217, 81, '"', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(218, 81, 'f', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(219, 81, 'd', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(220, 81, 's', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(221, 81, 'f', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(222, 81, 's', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(223, 81, 'd', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(224, 81, '"', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(225, 81, ',', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(226, 81, '"', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(227, 81, 'f', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(228, 81, 'd', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(229, 81, 's', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(230, 81, 'f', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(231, 81, 'd', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(232, 81, 's', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(233, 81, '"', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(234, 81, ']', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(235, 82, '[', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(236, 82, '"', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(237, 82, '"', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(238, 82, ']', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(239, 83, '[', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(240, 83, '"', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(241, 83, 'g', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(242, 83, 'o', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(243, 83, 'o', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(244, 83, 'd', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(245, 83, '"', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(246, 83, ',', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(247, 83, '"', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(248, 83, 'f', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(249, 83, 'i', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(250, 83, 'n', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(251, 83, 'e', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(252, 83, '"', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10'),
(253, 83, ']', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_poll_question`
--

CREATE TABLE `tbl_poll_question` (
  `iQuestionID` bigint(20) UNSIGNED NOT NULL,
  `iPostID` bigint(20) UNSIGNED NOT NULL,
  `tQuestion` text NOT NULL,
  `eStatus` enum('y','n','d') NOT NULL DEFAULT 'y',
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_poll_question`
--

INSERT INTO `tbl_poll_question` (`iQuestionID`, `iPostID`, `tQuestion`, `eStatus`, `dCreatedDate`, `tUpdatedDate`) VALUES
(1, 6, 'new question', 'y', '2017-04-30 11:40:39', '2017-04-30 11:40:39'),
(2, 7, 'new question', 'y', '2017-04-30 11:40:50', '2017-04-30 11:40:50'),
(3, 11, 'new question', 'y', '2017-06-14 06:34:51', '2017-06-14 06:34:51'),
(4, 12, 'sdasdas', 'y', '2017-06-16 13:02:18', '2017-06-16 13:02:18'),
(5, 13, 'csdssdf', 'y', '2017-06-17 04:19:53', '2017-06-17 04:19:53'),
(6, 14, 'csdssdf', 'y', '2017-06-17 04:19:59', '2017-06-17 04:19:59'),
(7, 15, 'csdssdf', 'y', '2017-06-17 04:20:12', '2017-06-17 04:20:12'),
(8, 16, 'csdssdf', 'y', '2017-06-17 04:20:17', '2017-06-17 04:20:17'),
(9, 17, 'fsdfsdfsfsdf', 'y', '2017-06-17 04:20:58', '2017-06-17 04:20:58'),
(10, 18, 'new question', 'y', '2017-06-17 04:22:29', '2017-06-17 04:22:29'),
(11, 19, 'new question', 'y', '2017-06-17 04:23:21', '2017-06-17 04:23:21'),
(12, 20, 'new question', 'y', '2017-06-17 04:28:11', '2017-06-17 04:28:11'),
(13, 21, 'jhhhjhj', 'y', '2017-06-17 04:31:29', '2017-06-17 04:31:29'),
(14, 22, 'jhhhjhj', 'y', '2017-06-17 04:31:30', '2017-06-17 04:31:30'),
(15, 23, 'dasdas', 'y', '2017-06-17 04:36:44', '2017-06-17 04:36:44'),
(16, 24, 'dfsfsdfsd', 'y', '2017-06-17 04:42:49', '2017-06-17 04:42:49'),
(17, 25, 'dfsfsdfsd', 'y', '2017-06-17 04:42:50', '2017-06-17 04:42:50'),
(18, 26, 'sfdsfsdf', 'y', '2017-06-17 08:04:48', '2017-06-17 08:04:48'),
(19, 27, 'sdasdas', 'y', '2017-06-17 08:08:12', '2017-06-17 08:08:12'),
(20, 29, 'vnbvnvn', 'y', '2017-06-20 09:05:33', '2017-06-20 09:05:33'),
(21, 37, 'Hi I my', 'y', '2017-06-22 18:30:48', '2017-06-22 18:30:48'),
(22, 38, 'Hi I my', 'y', '2017-06-22 18:31:15', '2017-06-22 18:31:15'),
(23, 39, 'Hi I have', 'y', '2017-06-22 18:32:47', '2017-06-22 18:32:47'),
(24, 40, 'Hi I have', 'y', '2017-06-22 18:33:52', '2017-06-22 18:33:52'),
(25, 41, 'new question', 'y', '2017-06-22 18:34:47', '2017-06-22 18:34:47'),
(26, 42, 'Hi hello I', 'y', '2017-06-22 18:39:06', '2017-06-22 18:39:06'),
(27, 43, 'new question', 'y', '2017-06-22 18:39:24', '2017-06-22 18:39:24'),
(28, 44, 'Hi hello I', 'y', '2017-06-22 18:40:37', '2017-06-22 18:40:37'),
(29, 45, 'Hi hello I', 'y', '2017-06-22 18:41:07', '2017-06-22 18:41:07'),
(30, 46, 'new question', 'y', '2017-06-22 18:43:07', '2017-06-22 18:43:07'),
(31, 47, '"new question"', 'y', '2017-06-22 18:43:15', '2017-06-22 18:43:15'),
(32, 48, '"new question"', 'y', '2017-06-22 18:43:17', '2017-06-22 18:43:17'),
(33, 49, '"new question"', 'y', '2017-06-22 18:43:19', '2017-06-22 18:43:19'),
(34, 50, '"new question"', 'y', '2017-06-22 18:43:36', '2017-06-22 18:43:36'),
(35, 51, '"new question"', 'y', '2017-06-22 18:43:40', '2017-06-22 18:43:40'),
(36, 52, '"new question"', 'y', '2017-06-22 18:43:41', '2017-06-22 18:43:41'),
(37, 53, '"new question"', 'y', '2017-06-22 18:43:42', '2017-06-22 18:43:42'),
(38, 54, 'Hi hello I', 'y', '2017-06-22 18:43:46', '2017-06-22 18:43:46'),
(39, 55, '"new question"', 'y', '2017-06-22 18:50:08', '2017-06-22 18:50:08'),
(40, 56, '"new question"', 'y', '2017-06-22 18:50:23', '2017-06-22 18:50:23'),
(41, 57, 'I don\'t know', 'y', '2017-06-22 18:53:44', '2017-06-22 18:53:44'),
(42, 58, 'I don\'t know', 'y', '2017-06-22 18:53:58', '2017-06-22 18:53:58'),
(43, 59, 'I don\'t know', 'y', '2017-06-22 18:54:00', '2017-06-22 18:54:00'),
(44, 60, 'I don\'t know', 'y', '2017-06-22 18:54:01', '2017-06-22 18:54:01'),
(45, 61, 'I don\'t know', 'y', '2017-06-22 18:54:03', '2017-06-22 18:54:03'),
(46, 62, 'I don\'t know', 'y', '2017-06-22 18:54:04', '2017-06-22 18:54:04'),
(47, 63, 'I don\'t know', 'y', '2017-06-22 18:54:05', '2017-06-22 18:54:05'),
(48, 64, 'I don\'t know', 'y', '2017-06-22 18:54:07', '2017-06-22 18:54:07'),
(49, 65, 'I don\'t know', 'y', '2017-06-22 18:54:08', '2017-06-22 18:54:08'),
(50, 66, 'I don\'t know', 'y', '2017-06-22 18:54:09', '2017-06-22 18:54:09'),
(51, 67, 'I don\'t know', 'y', '2017-06-22 18:54:11', '2017-06-22 18:54:11'),
(52, 68, 'I don\'t know', 'y', '2017-06-22 18:54:12', '2017-06-22 18:54:12'),
(53, 69, '"new question"', 'y', '2017-06-22 18:54:22', '2017-06-22 18:54:22'),
(54, 70, '"new question"', 'y', '2017-06-22 18:55:20', '2017-06-22 18:55:20'),
(55, 71, 'new question', 'y', '2017-06-23 18:04:02', '2017-06-23 18:04:02'),
(56, 73, 'Hi my mother', 'y', '2017-06-24 14:22:30', '2017-06-24 14:22:30'),
(57, 75, 'hththth', 'y', '2017-06-27 09:25:50', '2017-06-27 09:25:50'),
(58, 76, 'hththth', 'y', '2017-06-27 09:25:54', '2017-06-27 09:25:54'),
(59, 77, 'hththth', 'y', '2017-06-27 09:25:55', '2017-06-27 09:25:55'),
(60, 78, 'dsadsadas', 'y', '2017-06-27 09:26:40', '2017-06-27 09:26:40'),
(61, 79, 'nnjjn', 'y', '2017-06-27 09:27:36', '2017-06-27 09:27:36'),
(62, 80, 'did you like tubelight?', 'y', '2017-06-27 09:37:27', '2017-06-27 09:37:27'),
(63, 81, 'The app is now app that', 'y', '2017-06-27 17:34:53', '2017-06-27 17:34:53'),
(64, 82, 'The app', 'y', '2017-06-27 17:41:27', '2017-06-27 17:41:27'),
(65, 83, 'Hi I my', 'y', '2017-06-27 17:46:05', '2017-06-27 17:46:05'),
(66, 83, 'Hi I my', 'y', '2017-06-27 17:46:05', '2017-06-27 17:47:13'),
(67, 84, 'Hi I have', 'y', '2017-06-27 17:54:09', '2017-06-27 17:54:09'),
(68, 85, 'Hi I have', 'y', '2017-06-27 17:55:29', '2017-06-27 17:55:29'),
(69, 86, 'new question', 'y', '2017-06-27 17:57:39', '2017-06-27 17:57:39'),
(70, 87, 'Hi hello my', 'y', '2017-06-27 18:04:56', '2017-06-27 18:04:56'),
(71, 88, 'dsaasd', 'y', '2017-06-28 10:34:45', '2017-06-28 10:34:45'),
(72, 90, 'Hi The I hello', 'y', '2017-06-28 18:48:08', '2017-06-28 18:48:08'),
(73, 91, 'Gahaj', 'y', '2017-06-29 03:49:10', '2017-06-29 03:49:10'),
(74, 97, 'Hi', 'y', '2017-06-29 18:42:26', '2017-06-29 18:42:26'),
(75, 100, 't', 'y', '2017-06-30 06:44:55', '2017-06-30 06:44:55'),
(76, 113, 'gdfgdfg', 'y', '2017-06-30 08:40:43', '2017-06-30 08:40:43'),
(77, 114, 'dsa', 'y', '2017-06-30 08:41:56', '2017-06-30 08:41:56'),
(78, 115, 'fdsfs', 'y', '2017-06-30 08:44:22', '2017-06-30 08:44:22'),
(79, 116, 'dasdasd', 'y', '2017-06-30 08:44:34', '2017-06-30 08:44:34'),
(80, 117, 'sadas', 'y', '2017-06-30 08:45:50', '2017-06-30 08:45:50'),
(81, 118, 'dfdsf', 'y', '2017-06-30 08:46:33', '2017-06-30 08:46:33'),
(82, 121, 'xcvcxv', 'y', '2017-06-30 08:57:17', '2017-06-30 08:57:17'),
(83, 130, 'how are you savan', 'y', '2017-07-03 14:37:10', '2017-07-03 14:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_checkin`
--

CREATE TABLE `tbl_post_checkin` (
  `iCheckinID` bigint(20) NOT NULL,
  `iPostID` bigint(20) NOT NULL,
  `iPlaceID` bigint(20) NOT NULL,
  `eStatus` enum('y','n','d') NOT NULL,
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_photos`
--

CREATE TABLE `tbl_post_photos` (
  `iPhotoID` bigint(20) UNSIGNED NOT NULL,
  `iPostID` bigint(20) UNSIGNED NOT NULL,
  `vPhotoName` varchar(255) NOT NULL,
  `eStatus` enum('y','n','d') NOT NULL DEFAULT 'y',
  `dCreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post_photos`
--

INSERT INTO `tbl_post_photos` (`iPhotoID`, `iPostID`, `vPhotoName`, `eStatus`, `dCreatedDate`) VALUES
(1, 1, 'f167f9543dbd1763858de72190ca334b.png', 'y', '2017-07-01 18:22:44'),
(2, 1, '2e16418d6cd76e2281e0e145d8855ee1.png', 'y', '2017-07-01 18:22:44'),
(3, 1, 'e231ff9908c7a5c56936959369474cce.png', 'y', '2017-07-01 18:22:44'),
(4, 125, 'b743088a528c2c20cba3e089fbf773bd.jpeg', 'y', '2017-07-01 19:10:32'),
(5, 125, 'b369341dce83490a6b34a2f2aa4d49cd.jpeg', 'y', '2017-07-01 19:10:32'),
(6, 126, '38ce75441f8c0e18f4416316af4a8267.jpeg', 'y', '2017-07-01 19:11:23'),
(7, 126, 'a0b6ce1b65f4ffda1b39d373d0bbafcb.jpeg', 'y', '2017-07-01 19:11:23'),
(8, 128, '416743bfc4b41929a110cb5dbb229658.jpeg', 'y', '2017-07-01 19:17:41'),
(9, 128, '86065a3fa97a7fa2539e8d8ccd7a3cdc.jpeg', 'y', '2017-07-01 19:17:41'),
(10, 128, '49a0faca21c8504b3a99ea27ecd26927.jpeg', 'y', '2017-07-01 19:17:41'),
(11, 129, 'a687382cd67c9d8bf3be41bdb91845ec.jpeg', 'y', '2017-07-01 19:23:41'),
(12, 129, 'ad7a081f571661cbb85d88e9c8f8f4b3.jpeg', 'y', '2017-07-01 19:23:41'),
(13, 129, 'a0a381f8e91cc14e61701f76dd854256.jpeg', 'y', '2017-07-01 19:23:41'),
(14, 129, '13b3d4df20d64831983550e0e659fb32.jpeg', 'y', '2017-07-01 19:23:41'),
(15, 129, '166224aa20e369a26eeeacbd430f8e71.jpeg', 'y', '2017-07-01 19:23:41'),
(16, 129, 'cae1901da294fd4814c116ff664a9d3f.jpeg', 'y', '2017-07-01 19:23:41'),
(17, 129, '3ac23055aab6f851be9ff0c06a462906.jpeg', 'y', '2017-07-01 19:23:41'),
(18, 129, 'ac7e53403be6ae92ed0d2bef40676c02.jpeg', 'y', '2017-07-01 19:23:41'),
(19, 129, 'df5d13ba6861b93868a948cde64cfb61.jpeg', 'y', '2017-07-01 19:23:41'),
(20, 129, 'cdfe216e925f8022b9cf80ec8452fb39.jpeg', 'y', '2017-07-01 19:23:41'),
(21, 131, '6c5f22a858191a377bc716c2886fc216.jpeg', 'y', '2017-07-04 07:16:04'),
(22, 131, '4e0bbf7c4018af4982c104aca67f2057.jpeg', 'y', '2017-07-04 07:16:04'),
(23, 132, 'b4579019ae9baca640c0d077abfa1b75.jpeg', 'y', '2017-07-05 15:27:28'),
(24, 132, 'f827f61643eabc8883888cef5aa5e424.jpeg', 'y', '2017-07-05 15:27:28'),
(25, 132, 'd76ade2d080f42c3a6764db616fb4f4b.jpeg', 'y', '2017-07-05 15:27:28'),
(26, 137, '90bbbac112c9d52982ad17f8e18b77e5.jpeg', 'y', '2017-07-06 12:22:08'),
(27, 138, '025e2f0df6311b4614cbd59a30afbe66.jpeg', 'y', '2017-07-06 14:02:36'),
(28, 139, 'e0536daf61697dc5d9703206739d55ae.jpeg', 'y', '2017-07-06 14:03:03'),
(29, 139, 'd8759921a787324e816ca12a8d3a3cab.jpeg', 'y', '2017-07-06 14:03:03'),
(30, 139, 'dc02e88d29bfabf6322528ab293a59de.jpeg', 'y', '2017-07-06 14:03:03'),
(31, 140, 'cb18a7d10524304eea207ba59add4f4f.jpeg', 'y', '2017-07-07 14:03:59'),
(32, 140, 'd9a71f2560188620f1f13ed525fec4bd.jpeg', 'y', '2017-07-07 14:03:59'),
(33, 140, '0c219d50357b4241c430cd7be1c23d38.jpeg', 'y', '2017-07-07 14:03:59'),
(34, 140, '0e4a17d368afb40c6e32aff0b455243e.jpeg', 'y', '2017-07-07 14:03:59'),
(35, 141, '88a6cff7dd4e990542216f3eee3284ed.jpeg', 'y', '2017-07-09 06:57:14'),
(36, 143, '945c8d8ee2fe36fc6c99a7c5d58cdcc4.png', 'y', '2017-07-10 07:38:27'),
(37, 143, '5985838e256a116b7af0a824f38141bc.jpg', 'y', '2017-07-10 07:38:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_planning`
--

CREATE TABLE `tbl_post_planning` (
  `iPlanID` bigint(20) NOT NULL,
  `iPostID` bigint(20) NOT NULL,
  `dtPlanTime` datetime NOT NULL,
  `eStatus` enum('y','n','d') NOT NULL,
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_star`
--

CREATE TABLE `tbl_post_star` (
  `iStarID` bigint(20) UNSIGNED NOT NULL,
  `iPostID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) NOT NULL,
  `dCreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post_star`
--

INSERT INTO `tbl_post_star` (`iStarID`, `iPostID`, `iUserID`, `dCreatedDate`) VALUES
(3, 1, 1, '2017-05-02 16:39:50'),
(5, 1, 32, '2017-07-07 05:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post_tag_user`
--

CREATE TABLE `tbl_post_tag_user` (
  `iTagID` bigint(20) UNSIGNED NOT NULL,
  `iPostID` bigint(20) UNSIGNED NOT NULL,
  `iTaggedUserID` bigint(20) UNSIGNED NOT NULL,
  `eStatus` enum('y','d') NOT NULL,
  `dCreatedDate` datetime NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `vFacebookId` varchar(20) NOT NULL,
  `vName` varchar(150) NOT NULL,
  `vEmail` varchar(100) NOT NULL,
  `vPassword` varchar(50) NOT NULL,
  `vMobileNumber` varchar(20) NOT NULL,
  `dBirthDate` date DEFAULT NULL,
  `eGender` enum('','male','female') NOT NULL DEFAULT '',
  `vImage` varchar(255) DEFAULT NULL,
  `eRelationship` enum('single','married','separated','divorced','widowed') NOT NULL DEFAULT 'single',
  `vCity` varchar(255) NOT NULL,
  `vHomeTown` varchar(255) NOT NULL,
  `vEducation` varchar(255) NOT NULL,
  `vWorkPlace` varchar(255) NOT NULL,
  `vActivationToken` varchar(100) NOT NULL DEFAULT '',
  `vOTP` varchar(50) DEFAULT NULL,
  `vResetReqTime` varchar(50) DEFAULT NULL,
  `isInvisibleFromLounge` char(1) NOT NULL DEFAULT '0' COMMENT '0:Visible in Lounge, 1:Invisible from lounge',
  `isSendNotification` char(1) NOT NULL DEFAULT '1' COMMENT '0:Don''t send me notification,1:send me notification',
  `eStatus` enum('y','n','d') NOT NULL,
  `cVersion` char(5) NOT NULL DEFAULT '1',
  `dCreatedDate` varchar(100) NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`iUserID`, `vFacebookId`, `vName`, `vEmail`, `vPassword`, `vMobileNumber`, `dBirthDate`, `eGender`, `vImage`, `eRelationship`, `vCity`, `vHomeTown`, `vEducation`, `vWorkPlace`, `vActivationToken`, `vOTP`, `vResetReqTime`, `isInvisibleFromLounge`, `isSendNotification`, `eStatus`, `cVersion`, `dCreatedDate`, `tUpdatedDate`) VALUES
(1, '', 'Test1 User', 'test1@test.co.in', 'e10adc3949ba59abbe56e057f20f883e', '9898989898', '1987-01-01', '', '', 'single', '', '', '', '', '7nwy5c2tam7ctlcx155xrhp8u0m2rqfq', 'JtM3xT', '2017-06-16 18:01:27', '1', '1', 'y', '', '2017-04-21T15:07:24Z', '2017-07-06 13:36:47'),
(3, '', 'Test2 user', 'test2.user@demo.com', 'e10adc3949ba59abbe56e057f20f883e', '9898989898', NULL, '', '', 'single', '', '', '', '', '2upvp0tkrtreqpwg1sfr8t2341i27jyh', NULL, NULL, '0', '1', 'y', '1', '2017-04-25T16:44:59Z', '2017-05-05 05:27:56'),
(4, '', 'Kp', 'kp@gmail.com', '56855d12df9bd661d0d6259d67287e14', '86476 56666', NULL, '', NULL, 'single', '', '', '', '', 'rdzkwps802inwnr97l5ag9g68euo0n55', '06wp3b', '2017-06-17 18:57:49', '0', '1', 'y', '1', '2017-06-09 16:17:02', '2017-06-17 18:57:49'),
(5, '', 'Bhavin Test', 'bhavin.steptowebsoft@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9909939159', NULL, '', NULL, 'single', '', '', '', '', '7nbff73jn365tz7xfhuaqzxue07jy6fp', NULL, NULL, '1', '1', 'y', '1', '2017-06-14 05:51:31', '2017-06-27 09:34:45'),
(6, '', 'sachin', 'sachin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9998458080', NULL, '', '0705f0c44ce7cf3a0840f9ab18933e78.jpeg', 'single', '', '', '', '', 'ah0pgahvv9gqheeys6neamic5ejqac37', 'S5VbuU', '2017-06-16 07:58:45', '0', '1', 'y', '1', '2017-06-15 12:42:27', '2017-06-30 08:26:31'),
(7, '', 'Pravin', 'kp1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '535 764 6777', NULL, '', NULL, 'single', '', '', '', '', 'pmo6vqdawm2isiywe4uptrv4g2qlblhl', NULL, NULL, '0', '1', 'y', '1', '2017-06-17 19:00:08', '2017-06-17 19:00:08'),
(8, '', 'Pravin', 'kp2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '74656 45756', NULL, '', NULL, 'single', '', '', '', '', '5n67ent7ks2yq3cpovf3lg04nws8deoc', NULL, NULL, '0', '1', 'y', '1', '2017-06-17 19:15:42', '2017-06-17 19:15:42'),
(9, '', 'Pravin', 'kp3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '74656 45756', NULL, '', NULL, 'single', '', '', '', '', 'wk33m1r7y03ztdw2pckvxptsytmd7s5n', NULL, NULL, '0', '1', 'y', '1', '2017-06-17 19:16:18', '2017-06-17 19:16:18'),
(10, '', 'Test08 User', 'tester@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9036985212', NULL, '', NULL, 'single', '', '', '', '', 'foe2obl6c6i8dm3qhe2lw8bn8y6bjlm1', NULL, NULL, '0', '1', 'y', '1', '2017-06-19 17:59:26', '2017-06-19 17:59:26'),
(11, '', 'Test08 User', 'tester98@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9036985212', NULL, '', NULL, 'single', '', '', '', '', '1d4j7nloikpwwcq34q0v90ir0ah9mt8q', NULL, NULL, '0', '1', 'y', '1', '2017-06-19 18:31:25', '2017-06-19 18:31:25'),
(12, '', 'Pravin', 'kp4@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '7485 367 376', NULL, '', 'd6d7e5fd46b70ce8d0effebeafdf1809.jpeg', 'single', '', '', '', '', 's7un96enzqk5s85zm3exan4gshoofdin', NULL, NULL, '0', '1', 'y', '1', '2017-06-20 16:39:57', '2017-07-04 17:51:12'),
(13, '', 'Pravin Kp', 'pravin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '878 656 5656', NULL, '', 'e63f0f261922fa31d93aa9d5ccd6ff59.jpeg', 'single', '', '', '', '', '7yfbg94b9gy61qwzw8p2476ie18wuq90', NULL, NULL, '0', '1', 'y', '1', '2017-06-20 16:44:47', '2017-06-20 16:45:05'),
(14, '', 'Bhavin', 'jja@jjja.aaa', 'd5138fad21bc04727b7d0b36c1de1613', '1234567890', NULL, '', NULL, 'single', '', '', '', '', 'h4dgdjls1tvhdtdnzgtvaa7cpk7yt7e5', NULL, NULL, '0', '1', 'y', '1', '2017-06-21 10:02:56', '2017-06-21 10:02:56'),
(15, '', 'jdjddjjdd', 'hdhs@hdh.jdjd', 'fc220dfc566640df4377f34d98e9157a', '6895665', NULL, '', NULL, 'single', '', '', '', '', '0ozxky73gkmyhc4bkk3nn8eiayjea8t7', NULL, NULL, '0', '1', 'y', '1', '2017-06-21 10:04:16', '2017-06-21 10:04:16'),
(17, '', 'karan', 'karan@gmail.com', '96e79218965eb72c92a549dd5a330112', '1', NULL, '', NULL, 'single', '', '', '', '', 'pzncogqtvnwi9f0ijy48028x1ser4btj', NULL, NULL, '0', '1', 'y', '1', '2017-06-22 05:47:59', '2017-06-22 05:47:59'),
(18, '123456789', 'Test08 User', 'tester989@gmail.com', '', '9036985212', NULL, 'male', NULL, 'single', '', '', '', '', '', NULL, NULL, '0', '1', 'y', '1', '2017-06-27 18:39:51', '2017-06-30 07:08:01'),
(19, '34567890', 'Facebook User', '', '', '', NULL, '', NULL, 'single', '', '', '', '', '', NULL, NULL, '0', '1', 'y', '1', '2017-06-27 18:52:46', '2017-06-27 18:52:46'),
(20, '1237434393016178', 'Sachin', '', '', '', NULL, '', 'f08a07ad02edc0cba0cc298dc1c2954d.png', 'single', '', '', '', '', '', NULL, NULL, '1', '0', 'y', '1', '2017-06-28 09:07:30', '2017-07-03 13:24:03'),
(24, '', 'Checkedin', 'akashryp@hotmail.com', 'f87e382a9b929c35b2caac549497765d', '95848 22000', NULL, '', '6520d6e911a8ce0dff0adbd60e55ebf2.jpeg', 'single', '', '', '', '', 'tos98lmavhrjccfz8xnmo1b0ftp0wp3v', NULL, NULL, '0', '1', 'y', '1', '2017-06-29 03:46:15', '2017-06-29 03:47:40'),
(25, '763093240511964', 'Robert Smith', 'robertsmithrocking@gmail.com', '', '', NULL, '', 'f779424c4dbf3775bfc67d711ba951d9.jpeg', 'single', '', '', '', '', '', NULL, NULL, '0', '1', 'y', '1', '2017-06-29 18:53:36', '2017-07-04 11:30:21'),
(26, '1111222', 'Test08 User', 'hello@gmail.com', '', '9036985212', NULL, 'female', NULL, 'single', '', '', '', '', '', NULL, NULL, '0', '1', 'y', '1', '2017-06-30 10:16:45', '2017-06-30 10:16:45'),
(27, '1213771202010203', 'Bhavin Parmar', 'bhavinparmar32@gmail.com', '', '', NULL, 'male', '1e9b6ecb9af8b61083ff7376f722ffff.jpeg', 'single', '', '', '', '', '', NULL, NULL, '0', '1', 'y', '1', '2017-07-03 06:55:14', '2017-07-03 14:37:31'),
(28, '', 'Bhavin', 'bhavin.jscope@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '12345978901', NULL, '', NULL, 'single', '', '', '', '', 'a9b1vvvgi264uis278v1e42a9strb41g', NULL, NULL, '0', '1', 'y', '1', '2017-07-04 07:14:34', '2017-07-04 07:14:34'),
(29, '', 'karan johar', 'johar@gmail.com', '96e79218965eb72c92a549dd5a330112', '9998458080', NULL, '', NULL, 'single', '', '', '', '', 'csiq3g20ntow8qxzrqxnq6lmkuoo96eo', NULL, NULL, '0', '1', 'y', '1', '2017-07-04 09:26:46', '2017-07-04 09:26:46'),
(30, '', 'karan', 'karanjohar@gmail.com', '96e79218965eb72c92a549dd5a330112', '1111222333', NULL, '', NULL, 'single', '', '', '', '', 'w9axchohsujal25p5p5fscmgtip7nafv', NULL, NULL, '0', '1', 'y', '1', '2017-07-04 09:29:42', '2017-07-04 09:29:42'),
(31, '', 'nirav', 'nirav@gmail.com', '96e79218965eb72c92a549dd5a330112', '111222333', NULL, '', '7ee8a5e90acd65638025675655d45b02.jpeg', 'single', '', '', '', '', 'lijhdtju8f8vhawx78xbozk35rzwbb9k', NULL, NULL, '0', '1', 'y', '1', '2017-07-04 09:31:58', '2017-07-04 09:32:08'),
(32, '', 'Test08 User', 'sachins@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9036985212', NULL, '', 'fde1ff1afa5cc0f0a61b0fc5e95267f6.jpeg', 'single', '', '', '', '', 'gfjybckxxemk1jnjfy8z9s7vpy8usdj1', NULL, NULL, '0', '1', 'y', '1', '2017-07-06 10:30:20', '2017-07-06 14:47:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_block`
--

CREATE TABLE `tbl_user_block` (
  `iAutoBlockID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `iBlockUserID` bigint(20) UNSIGNED NOT NULL,
  `dCreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_favourite`
--

CREATE TABLE `tbl_user_favourite` (
  `iAutoFavID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `iFavUserID` bigint(20) UNSIGNED NOT NULL,
  `dCreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_favourite`
--

INSERT INTO `tbl_user_favourite` (`iAutoFavID`, `iUserID`, `iFavUserID`, `dCreatedDate`) VALUES
(1, 1, 7, '2017-04-30 18:57:03'),
(2, 1, 5, '2017-04-30 13:28:45'),
(4, 1, 3, '2017-04-30 13:57:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_tokens`
--

CREATE TABLE `tbl_user_tokens` (
  `iTokenID` bigint(20) UNSIGNED NOT NULL,
  `iUserID` bigint(20) UNSIGNED NOT NULL,
  `vAuthToken` varchar(255) NOT NULL,
  `vPushToken` varchar(255) DEFAULT NULL,
  `eDeviceType` enum('Android','iOS') NOT NULL,
  `tUpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_tokens`
--

INSERT INTO `tbl_user_tokens` (`iTokenID`, `iUserID`, `vAuthToken`, `vPushToken`, `eDeviceType`, `tUpdatedDate`) VALUES
(1, 3, '0383acd66195272e963a36d2b993f8d5', NULL, 'Android', '2017-04-25 16:46:10'),
(2, 1, '786aa7229427bf4b81bbede38a14e20d', NULL, 'Android', '2017-07-01 18:12:36'),
(3, 4, '4fb855d47b058e7d52c903acbf98140d', NULL, 'iOS', '2017-06-17 17:17:29'),
(4, 5, '513e6499fcf2d4aeec8203e612e0fd5c', NULL, 'Android', '2017-06-27 09:24:00'),
(5, 6, '578c53f17888992594b02cc00f745cee', NULL, 'Android', '2017-07-10 07:38:08'),
(6, 7, '9af306375aa27a1aadf6c6d40b00407b', NULL, 'iOS', '2017-06-19 16:46:15'),
(7, 8, 'c648ee709bbbd3107fcd38121f892c94', NULL, 'iOS', '2017-07-03 18:17:28'),
(8, 9, '494842e7ca9e5ca22c42d36e8151e6aa', NULL, 'iOS', '2017-06-17 19:23:52'),
(9, 10, '9ce44e0be4b1c00ddea5ba7e5803fa2e', NULL, 'Android', '2017-06-19 17:59:26'),
(10, 11, 'ab499b2a7e7bb46efc89bb22b3162c0c', NULL, 'Android', '2017-06-19 18:31:25'),
(11, 12, '86a4f805dcd26904f5775d56e6c600a3', NULL, 'iOS', '2017-07-04 17:50:56'),
(12, 13, '11e4480b1d5cb53a9e8573d7a70ff830', NULL, 'iOS', '2017-06-22 17:03:50'),
(13, 14, '8ff3447703765c826a5d231bb0c249a0', NULL, 'Android', '2017-06-21 10:02:56'),
(14, 15, '3486bdc8ae2fa86c3b8262ddb05d4802', NULL, 'Android', '2017-06-21 10:04:16'),
(15, 16, 'ca4fbf7e24010eaab250e76763fcd39f', NULL, 'Android', '2017-06-22 05:47:08'),
(16, 17, '0a86ba183294aa4bcc00017b16350b2f', NULL, 'Android', '2017-06-22 05:47:59'),
(17, 18, '6f6a9afd699e2b4e9c5d32e5cccef7c4', NULL, 'Android', '2017-06-30 10:16:03'),
(18, 19, '9685b360a58217ca15ae8b5f756dd5c3', NULL, 'Android', '2017-06-27 18:52:46'),
(19, 20, 'ea8867eeb39b791e9e33a89e669fa3ac', NULL, 'Android', '2017-06-30 10:12:15'),
(20, 21, '7b694b7ee76494212b041984f5f2bf54', NULL, 'iOS', '2017-06-28 18:37:12'),
(21, 22, '012bef6292d2cf2af46c30d6647539a4', NULL, 'iOS', '2017-06-28 18:40:44'),
(22, 23, '22d9101ba6766bccfb4ba430b1195c2e', NULL, 'iOS', '2017-06-28 18:47:06'),
(23, 24, '8637d2c813a7fea3110451b68c88bf24', NULL, 'iOS', '2017-06-29 03:46:15'),
(24, 25, 'caffffa0dca490169c6d80bad668023d', NULL, 'Android', '2017-07-04 11:29:48'),
(25, 26, 'd8ff1700ddca79181a02ff0ac2aa4ae4', NULL, 'Android', '2017-06-30 10:16:45'),
(26, 27, '22c758ccc15644e85b9fe8be99974cb5', NULL, 'Android', '2017-07-03 06:55:14'),
(27, 28, 'b55484367b9fb816c2cc5807d88a1a56', NULL, 'Android', '2017-07-04 07:15:08'),
(28, 29, '5a89bbd1f3189f1eb752891f10f0336d', NULL, 'Android', '2017-07-04 09:26:46'),
(29, 30, '5e201e9068d0ff9daddf024b49bb3b42', NULL, 'Android', '2017-07-04 09:29:42'),
(30, 31, '0b36d1fd8ddf120f70729a4519a271c7', NULL, 'Android', '2017-07-04 09:32:37'),
(31, 32, 'ec2ce5601a3821b0af8c592572d2ff02', NULL, 'Android', '2017-07-09 06:21:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_emailtemplate`
--
ALTER TABLE `tbl_emailtemplate`
  ADD PRIMARY KEY (`iTemplateID`);

--
-- Indexes for table `tbl_follow_following`
--
ALTER TABLE `tbl_follow_following`
  ADD PRIMARY KEY (`iAutoID`);

--
-- Indexes for table `tbl_life_category`
--
ALTER TABLE `tbl_life_category`
  ADD PRIMARY KEY (`iCategoryID`);

--
-- Indexes for table `tbl_life_post`
--
ALTER TABLE `tbl_life_post`
  ADD PRIMARY KEY (`iPostID`);

--
-- Indexes for table `tbl_poll_answer`
--
ALTER TABLE `tbl_poll_answer`
  ADD PRIMARY KEY (`iAnswerID`);

--
-- Indexes for table `tbl_poll_question`
--
ALTER TABLE `tbl_poll_question`
  ADD PRIMARY KEY (`iQuestionID`);

--
-- Indexes for table `tbl_post_photos`
--
ALTER TABLE `tbl_post_photos`
  ADD PRIMARY KEY (`iPhotoID`);

--
-- Indexes for table `tbl_post_star`
--
ALTER TABLE `tbl_post_star`
  ADD PRIMARY KEY (`iStarID`);

--
-- Indexes for table `tbl_post_tag_user`
--
ALTER TABLE `tbl_post_tag_user`
  ADD PRIMARY KEY (`iTagID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`iUserID`);

--
-- Indexes for table `tbl_user_block`
--
ALTER TABLE `tbl_user_block`
  ADD PRIMARY KEY (`iAutoBlockID`);

--
-- Indexes for table `tbl_user_favourite`
--
ALTER TABLE `tbl_user_favourite`
  ADD PRIMARY KEY (`iAutoFavID`);

--
-- Indexes for table `tbl_user_tokens`
--
ALTER TABLE `tbl_user_tokens`
  ADD PRIMARY KEY (`iTokenID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_emailtemplate`
--
ALTER TABLE `tbl_emailtemplate`
  MODIFY `iTemplateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_follow_following`
--
ALTER TABLE `tbl_follow_following`
  MODIFY `iAutoID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_life_category`
--
ALTER TABLE `tbl_life_category`
  MODIFY `iCategoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_life_post`
--
ALTER TABLE `tbl_life_post`
  MODIFY `iPostID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `tbl_poll_answer`
--
ALTER TABLE `tbl_poll_answer`
  MODIFY `iAnswerID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `tbl_poll_question`
--
ALTER TABLE `tbl_poll_question`
  MODIFY `iQuestionID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `tbl_post_photos`
--
ALTER TABLE `tbl_post_photos`
  MODIFY `iPhotoID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tbl_post_star`
--
ALTER TABLE `tbl_post_star`
  MODIFY `iStarID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_post_tag_user`
--
ALTER TABLE `tbl_post_tag_user`
  MODIFY `iTagID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `iUserID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_user_block`
--
ALTER TABLE `tbl_user_block`
  MODIFY `iAutoBlockID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_user_favourite`
--
ALTER TABLE `tbl_user_favourite`
  MODIFY `iAutoFavID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_user_tokens`
--
ALTER TABLE `tbl_user_tokens`
  MODIFY `iTokenID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
