<?php 
ob_start();
ini_set("display_errors", "1");
error_reporting(E_ALL);
	
if(isset($_REQUEST['total']) && isset($_REQUEST['customer_id']) && isset($_REQUEST['guest_id']) && isset($_REQUEST['appointment_id']) && isset($_REQUEST['mis_data'])){
   $total_amount = $_REQUEST['total'];
   $customer_id = $_REQUEST['customer_id'];
   $guest_id = $_REQUEST['guest_id'];
   $appointment_id = $_REQUEST['appointment_id'];
   $mis_data = $_REQUEST['mis_data'];

	//require_once "/home/wiggle/wiggle/knet_dev/com/aciworldwide/commerce/gateway/plugins/e24PaymentPipe.inc.php" ;
   require_once "com/aciworldwide/commerce/gateway/plugins/e24PaymentPipe.inc.php";
	$Pipe = new e24PaymentPipe;

   $digits = 4;
   $randno = rand(pow(10, $digits-1), pow(10, $digits)-1);
   
   // http://www.rollingscissors.com/knet/buy.php?UDF1=\(totalAmount)&UDF2=\(SINGLETON.getCustomer().id!)&UDF3=\(guestIDs)&UDF4=\(objOrder[0].appointment_id!)&UDF5=404c4d2e7da03fe2,\(device_type),\(objAdd.id!),0.000,\(NSTimeZone.local.secondsFromGMT()),\(objOrder[0].appointmentdetail_id!)"
   //http://wiggleapp.co/knet_dev/result.php?PaymentID=2517207291272820&Result=CAPTURED&PostDate=1009&TranID=2040507291272820&Auth=756247&Ref=728212258649&TrackID=3174&UDF1=14&UDF2=1&UDF3=2&UDF4=3&UDF5=4
   //http://wiggleapp.co/knet_dev/buy.php?total=14&customer_id=1&guest_id=123&appointment_id=1&mis_data
   

   $Pipe->setAction(1);
   $Pipe->setCurrency(414);
   $Pipe->setLanguage("ENG"); //change it to "ARA" for arabic language
   $Pipe->setResponseURL("http://wiggleapp.co/knet_dev/response.php"); // set your respone page URL   
   $Pipe->setErrorURL("http://wiggleapp.co/knet_dev/error.php"); //set your error page URL
   $Pipe->setAmt($total_amount); //set the amount for the transaction
   //$Pipe->setResourcePath("/Applications/MAMP/htdocs/php-toolkit/resource/");
   $Pipe->setResourcePath("resource/"); //change the path where your resource file is
   $Pipe->setAlias("wiggle"); //set your alias name here
   $Pipe->setTrackId($randno);//generate the random number here
 
   $Pipe->setUdf1($total_amount); //set User defined value
   $Pipe->setUdf2($customer_id); //set User defined value
   $Pipe->setUdf3($guest_id); //set User defined value
   $Pipe->setUdf4($appointment_id); //set User defined value
   $Pipe->setUdf5($mis_data); //set User defined value

   //get results
	if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){
     
			echo "Result=".$Pipe->SUCCESS;
			echo "<br>".$Pipe->getErrorMsg();
			echo "<br>".$Pipe->getDebugMsg();
             die('Failure here!');
			#header("location: https://www.wiggleapp.co/knet_dev/error.php");
		
	}else {
      
		$payID = $Pipe->getPaymentId();
         $payURL = $Pipe->getPaymentPage();
		#echo $Pipe->getDebugMsg();
		header("location: ".$payURL."?PaymentID=".$payID);
	}
}else{
    echo '{"status":'. (int) 1 .',"message":"Sorry, Paramater is missing.","message_ar":"عذرا، باراماتر مفقود."}';
    die();
}
?>