<?php

$sql = 'SELECT firstname,lastname FROM customers where id='.$udf2;
$result = mysql_query($sql) or die(mysql_error());
$row = mysql_fetch_assoc($result);
$name = $row['firstname'].' '.$row['lastname'];

$to = 'alpesh.jscope@gmail.com';
$subject = 'Booking Confirmation';
$from = 'Wiggle@wiggleapp.co';
 
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
 
// Compose a simple HTML email message
$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   
   <title>WiggleApp Email</title>
   <style type="text/css">
      html,body{height: 100%;}
      .ExternalClass{display: block !important;}
   </style>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" bgcolor="#ffffff" style="margin: 0;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="height: 100%;">
   <tbody><tr>
      <td valign="top">
         <table cellpadding="0" cellspacing="0" border="0" width="737" align="center" style="margin-left: auto; margin-right: auto;">
            <tbody><tr>
               <td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
            </tr>
            <tr>
               <td colspan="3" align="center" style="text-align: center;"><img src="http://wiggleapp.co/Kennel_Confirmation_files/img-header.png" alt="" width="503" height="217" border="0" style="vertical-align: top;"></td>
            </tr>
            <tr>
               <td width="113" valign="top"><img src="http://wiggleapp.co/Kennel_Confirmation_files/img-dog.png" alt="" width="113" height="542" border="0" style="vertical-align: top;"></td>
               <td valign="top" bgcolor="#ffffff" style="border: 1px solid #ee1e99; border-radius: 18px;">
                  <table cellpadding="0" cellspacing="0" border="0" width="100%">
                     <tbody><tr>
                        <td align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 54px; line-height: 64px; color: #666666; font-weight: 500; letter-spacing: 7px; padding: 46px 10px 47px; text-align: center;">Confirmation </td>
                     </tr>
                     <tr>
                        <td>
                           <table cellpadding="0" cellspacing="0" border="0" width="406" align="center" style="margin-left: auto; margin-right: auto;">
                              <tbody><tr>
                                 <td bgcolor="#d6d6d6" height="3" style="font-size: 0; line-height: 0; border-radius: 9999px;"></td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                     <tr>
                        <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 34px; line-height: 41px; color: #666666; font-weight: 300; padding: 9px 30px 24px 56px;">Thank you for booking with Wiggle. </td>
                     </tr>
                     <tr>
                        <td style="padding: 0 46px 0 56px;">
                           <table cellpadding="0" cellspacing="0" border="0" width="100%">
                              <tbody><tr>
                                 <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 36px; line-height: 42px; color: #00adee; font-weight: 300; padding-bottom: 30px;">Payment Detail</td>
                              </tr>
                              <tr>
                                 <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                       <tbody><tr>
                                          <td width="" valign="top">
                                             <table cellpadding="5" cellspacing="0" border="1" width="100%">
                                             <tbody>
                                                <tr>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 600; padding-top: 5px;">Total Amount </td>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 300; padding-top: 5px;">'.$udf1.' KD </td>
                                                </tr>

                                                <tr>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 600; padding-top: 5px;">Payment ID </td>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 300; padding-top: 5px;">'.$PaymentID.'</td>
                                                </tr>

                                                <tr>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 600; padding-top: 5px;">Transaction ID </td>
                                                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 300; padding-top: 5px;">'.$tranRowID.' </td>
                                                </tr>

                                                <tr>
                                                   <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 600; padding-top: 5px;">Payment Status </td>
                                                   <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; color: #666666; font-weight: 300; padding-top: 5px;"> SUCCESS </td>
                                                </tr>
                                             </tbody></table>
                                          </td>
                                          
                                       </tr>
                                       <tr>
                                          <td colspan="2" height="25" style="font-size: 0; line-height: 0;"></td>
                                       </tr>
                                    </tbody></table>
                                    
                                 </td>
                              </tr>
                           </tbody></table>
                        </td>
                     </tr>
                  </tbody></table>
               </td>
               <td width="113" valign="bottom"><img src="http://wiggleapp.co/Kennel_Confirmation_files/img-cat.png" alt="" width="113" height="205" border="0" style="vertical-align: top;"></td>
            </tr>
            <tr>
               <td width="113" style="font-size: 0; line-height: 0;"></td>
               <td valign="top">
                  <table cellpadding="0" cellspacing="0" border="0" width="100%">
                     <tbody><tr>
                        <td colspan="3" height="12" style="font-size: 0; line-height: 0;"></td>
                     </tr>
                     <tr>
                        <td width="87" style="font-size: 0; line-height: 0;"></td>
                        <td width="54"><img src="http://wiggleapp.co/Kennel_Confirmation_files/ico-email.png" alt="" width="40" height="27" border="0" style="vertical-align: top;"></td>
                        <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee; font-weight: 400;"><a href="mailto:info@wiggleapp.co" style="text-decoration: none; color: #00adee;" target="_blank">info@wiggleapp.co</a></td>
                     </tr>
                     <tr>
                        <td colspan="3" height="15" style="font-size: 0; line-height: 0;"></td>
                     </tr>
                     <tr>
                        <td width="87" style="font-size: 0; line-height: 0;"></td>
                        <td width="54">
                           <table cellpadding="0" cellspacing="0" border="0" width="100%">
                              <tbody><tr>
                                 <td width="9" style="font-size: 0; line-height: 0;"></td>
                                 <td><img src="http://wiggleapp.co/Kennel_Confirmation_files/ico-web.png" alt="" width="25" height="32" border="0" style="vertical-align: middle;"></td>
                              </tr>
                           </tbody></table>
                        </td>
                        <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee;"><a href="http://www.wiggleapp.co/" style="text-decoration: none; color: #00adee;" target="_blank">www.wiggleapp.co</a></td>
                     </tr>
                  </tbody></table>
               </td>
               <td width="113" style="font-size: 0; line-height: 0;"></td>
            </tr>
            <tr>
               <td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
            </tr>
         </tbody></table>
      </td>
   </tr>
</tbody></table>

</body></html>';
 
// Sending email
if(mail($to, $subject, $message, $headers)){
    /*echo 'Your mail has been sent successfully.';*/
} else{
    /*echo 'Unable to send email. Please try again.';*/
}
?>