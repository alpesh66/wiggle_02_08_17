<?php

/**
 * TO get the array of all roles for superadmin
 * @return array returns the array for the super admin
 */
function super_admin_roles() {
    return 'super-*';
    // return ['super_admin', 'super_sub'];
}

/**
 * TO get the array of all roles for ProviderAdmin
 * @return array returns the array for the Provider Admin
 */
function provider_admin_roles() {
    return 'provider-*';
}

/**
 * TO get the array of all roles for ProviderAdmin
 * @return array returns the array for the Provider Admin
 */
function society_admin_roles() {
    return 'society-*';
}

function display_role_permissions($value) {
    $fil = array_pluck($value, 'display_name');
    return implode(', ', $fil);
}

function providerAdmin_permissions() {
    return ['9', '10', '11', '12', '13', '20', '22'];
}

function societyAdmin_permissions() {
    return ['15', '16', '17', '18', '19', '21', '26'];
}

function get_multiple_categories($categories, $selected = '') {
    $categoryHtml = '';
    $categoryHtml .= '<option value="0" >Main Parent</option>';
    foreach ($categories as $category) {
        $selected = ($category->id == $selected) ? 'selected="selected"' : '';

        $categoryHtml .= "<option value=$category->id $selected> $category->name </option>";
        if (count($category->subcategory) > 0) {
            foreach ($category->subcategory as $subcategorys) {
                $child_selected = ($subcategorys->id == $selected) ? 'selected="selected"' : '';
                $categoryHtml .= "<option value=$subcategorys->id $child_selected > - -$subcategorys->name </option>";
            }
        }

        // @if(count( $subcategorys->subcategory) > 0 )
        //     <ul>
        //     @foreach($subcategorys->subcategory as $subcategy)
        //         <li>{{ $subcategy->name }}</li>
        //     @endforeach 
        //     </ul>
        // @endif
    }
    echo $categoryHtml;
}

function json_multiple_categories($categories) {
    $nodarray = array();
    foreach ($categories as $k => $category) {

        $nodarray[$k]['text'] = "$category->name";
        if (count($category->subcategory) > 0) {

            $ndNodeLevel = array();
            foreach ($category->subcategory as $l => $subcategorys) {

                $ndNodeLevel[$l]['text'] = "$subcategorys->name";

                if (count($subcategorys->subcategory) > 0) {
                    $rdNodeLevel = array();
                    foreach ($subcategorys->subcategory as $s => $subcategy) {
                        $rdNodeLevel[$s]['text'] = "$subcategy->name";
                    }
                    if (is_array($rdNodeLevel) && count($rdNodeLevel) > 0) {
                        $ndNodeLevel[$l]['nodes'] = $rdNodeLevel;
                    }
                }
            }
            if (is_array($ndNodeLevel) && count($ndNodeLevel) > 0) {

                $nodarray[$k]['nodes'] = $ndNodeLevel;
            }
        }
        $finalarray[] = $nodarray;
    }
    echo (json_encode($nodarray));
}

function provider_get_multiple_categories($categories, $selected = '') {
    $categoryHtml = '';
    foreach ($categories as $category) {
        $opt_selected = ($category->id == $selected) ? 'selected="selected"' : '';

        // $categoryHtml .= "<option value=$category->id $opt_selected> $category->name </option>";
        $categoryHtml .= "<optgroup label=$category->name>";
        if (count($category->subcategory) > 0) {
            foreach ($category->subcategory as $subcategorys) {
                $child_selected = ($subcategorys->id == $selected) ? 'selected="selected"' : '';
                $categoryHtml .= "<option value=$subcategorys->id $child_selected data-type=$subcategorys->parentid >$subcategorys->name </option>";

                // if(count($subcategorys->subcategory) > 0){
                //     foreach ($subcategorys->subcategory as $finalchild) {
                //         $last_selected = ($finalchild->id == $selected)? 'selected="selected"' : '' ;
                //         $categoryHtml .= "<option value=$finalchild->id $last_selected > - - - -$finalchild->name </option>";
                //     }
                // }
            }
            $categoryHtml .= '</optgroup>';
        }
    }
    echo $categoryHtml;
}

function display_pet_name($value) {
    $petData = $value->toArray();
    $petHtml = '';
    if (is_array($petData) && count($petData) > 0) {
        if (strpos(\Request::path(), 'societies/') !== FALSE) {
            foreach ($petData as $key => $v) {
                $petHtml .= '<a style="font-weight:bold" href=' . url('societies/pet', $v['id']) . '>' . $v['name'] . '</a> ,';
            }
        } else {
            foreach ($petData as $key => $v) {
                $petHtml .= '<a style="font-weight:bold" href=' . url('providers/pet', $v['id']) . '>' . $v['name'] . '</a> ,';
            }
        }
    }
    $petHtml = rtrim($petHtml, ',');
    echo $petHtml;
}

function salution() {
    $salutions = array('Dr.' => 'Dr.', 'Mr.' => 'Mr.', 'Mrs' => 'Mrs', 'Miss' => 'Miss');
    return $salutions;
}

function groom_frequency() {
    $frequency = array(['1' => '1 Week'], ['2' => '2 Week'], ['3' => '3 Week'], ['4' => '4 Week'], ['5' => '5 Week'], ['6' => '6 Week'], ['7' => '7 Week'], ['8' => '8 Week'], ['9' => '9 Week'], ['10' => '10 Week'], ['11' => '11 Week'], ['12' => '12 Week']);
    return $frequency;
}

function groom_frequencyInSuperAdmin() {
    return ['1 Month' => '1 Week',
        '2 Weeks' => '2 Weeks',
        '3 Weeks' => '3 Weeks',
        '4 Weeks' => '4 Weeks',
        '5 Weeks' => '5 Weeks',
        '6 Weeks' => '6 Weeks',
        '7 Weeks' => '7 Weeks',
        '8 Weeks' => '8 Weeks',
        '9 Weeks' => '9 Weeks',
        '10 Weeks' => '10 Weeks',
        '11 Weeks' => '11 Weeks',
        '12 Weeks' => '12 Weeks',
    ];
}

//
function weekdays() {
    $weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    return $weekdays;
}

function checkmyIP() {
    $ohmyIp = '';
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $ohmyIp = $_SERVER['REMOTE_ADDR'];
    }

    if ($ohmyIp == '127.0.0.1' || $ohmyIp == '::1' || $ohmyIp == '182.70.125.145') {
        return true;
    }

    return false;

    // $ohmyIp = $_SERVER['REMOTE_ADDR'];
    // if( $ohmyIp == '127.0.0.1' || $ohmyIp == '182.70.125.145')
    // {
    //     return true;
    // }
    // return false;
}

function showGender($value) {
    return ($value == '0') ? 'Female' : 'Male';
}

function showStatus($value) {
    return ($value == '0') ? 'Inactive' : 'Active';
}

function countryCode_explode($value) {
    $country_code = explode('-', $value);
    $dial_number = '';
    if (count($country_code) == 2) {
        $dial_number[0] = $country_code[0];
        $dial_number[1] = $country_code[1];
    } else {
        $dial_number[0] = '965';
        $dial_number[1] = $country_code[0];
    }
    return $dial_number;
}

//function send_push_notification_org($requestData) {
//    $logFile = 'storage/logs/send_push_notification_' . date('d-m-Y') . '.txt';
//    file_put_contents($logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
//    file_put_contents($logFile, "\n\n REQUEST DATA :" . json_encode($requestData), FILE_APPEND | LOCK_EX);
//
//    try {
//        if ($requestData["uDeviceType"] == 1 && !empty($requestData["uDeviceToken"])) {
//            // send ios push notification for english
//            $messageEn = array();
//            $messageEn['aps']['badge'] = (int) 1;
//            $messageEn['aps']['sound'] = "default";
//            $messageEn['aps']['name'] = "Vaccination reminder";
//            $messageEn['aps']['alert'] = "reminder message";
//
//            send_notification_ios($requestData["uDeviceToken"], $messageEn);
//            file_put_contents($logFile, "\n\n IOS MESSAGE FOR ENGLISH :" . json_encode($messageEn), FILE_APPEND | LOCK_EX);
//
//            // send ios push notification for arabic
//            $messageAr = array();
//            $messageAr['aps']['badge'] = (int) 1;
//            $messageAr['aps']['sound'] = "default";
//            $messageAr['aps']['name'] = "Vaccination reminder";
//            $messageAr['aps']['alert'] = "reminder message";
//
//            send_notification_ios($requestData["uDeviceToken"], $messageAr);
//            file_put_contents($logFile, "\n\n IOS MESSAGE FOR ARABIC :" . json_encode($messageAr), FILE_APPEND | LOCK_EX);
//        } else if ($requestData["uDeviceType"] == 2 && !empty($requestData["uDeviceToken"])) {
//            // send android push notification for english
//            $message = array();
//            $message['android']['badge'] = (int) 1;
//            $message['android']['sound'] = "default";
//            $message['android']['name'] = "Vaccination reminder";
//            $message['android']['message'] = "reminder message";
//            send_notification_android($requestData["uDeviceToken"], $messageEn);
//            file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ENGLISH :" . json_encode($message), FILE_APPEND | LOCK_EX);
//
//            // send android push notification for arabic
//            $messageAr = array();
//            $messageAr['android']['badge'] = (int) 1;
//            $messageAr['android']['sound'] = "default";
//            $messageAr['android']['name'] = "Vaccination reminder";
//            $messageAr['android']['message'] = "reminder message";
//            send_notification_android($requestData["uDeviceToken"], $messageAr);
//            file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ARABIC :" . json_encode($messageAr), FILE_APPEND | LOCK_EX);
//        }
//    } catch (Exception $e) {
//        file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ENGLISH :" . json_encode($e->getMessage()), FILE_APPEND | LOCK_EX);
//    }
//}
//
//function send_push_notification_old($requestData, $requestDataEn, $requestDataAr) {
//    $logFile = 'storage/logs/send_push_notification_' . date('d-m-Y') . '.txt';
//    file_put_contents($logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
//    file_put_contents($logFile, "\n\n REQUEST DATA :" . json_encode($requestData), FILE_APPEND | LOCK_EX);
//
//    try {
//        if ($requestData["uDeviceType"] == 1 && !empty($requestData["uDeviceToken"])) {
//            // send ios push notification for english
//            $messageEn = array();
//            $messageEn['aps']['badge'] = (int) 1;
//            $messageEn['aps']['sound'] = "default";
//            $messageEn['aps']['name'] = $requestDataEn['title'];
//            $messageEn['aps']['alert'] = $requestDataEn['message'];
//
//            send_notification_ios($requestData["uDeviceToken"], $messageEn);
//            file_put_contents($logFile, "\n\n IOS MESSAGE FOR ENGLISH :" . json_encode($messageEn), FILE_APPEND | LOCK_EX);
//
//            // send ios push notification for arabic
//            $messageAr = array();
//            $messageAr['aps']['badge'] = (int) 1;
//            $messageAr['aps']['sound'] = "default";
//            $messageEn['aps']['name'] = $requestDataAr['title'];
//            $messageEn['aps']['alert'] = $requestDataAr['message'];
//
//            send_notification_ios($requestData["uDeviceToken"], $messageAr);
//            file_put_contents($logFile, "\n\n IOS MESSAGE FOR ARABIC :" . json_encode($messageAr), FILE_APPEND | LOCK_EX);
//        } else if ($requestData["uDeviceType"] == 2 && !empty($requestData["uDeviceToken"])) {
//            // send android push notification for english
//            $messageEn = array();
//            $messageEn['android']['badge'] = (int) 1;
//            $messageEn['android']['sound'] = "default";
//            $messageEn['android']['name'] = $requestDataEn['title'];
//            $messageEn['android']['message'] = $requestDataEn['message'];
//
//            send_notification_android($requestData["uDeviceToken"], $messageEn);
//            file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ENGLISH :" . json_encode($message), FILE_APPEND | LOCK_EX);
//
//            // send android push notification for arabic
//            $messageAr = array();
//            $messageAr['android']['badge'] = (int) 1;
//            $messageAr['android']['sound'] = "default";
//            $messageAr['android']['name'] = $requestDataAr['title'];
//            $messageAr['android']['message'] = $requestDataAr['message'];
//            send_notification_android($requestData["uDeviceToken"], $messageAr);
//            file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ARABIC :" . json_encode($messageAr), FILE_APPEND | LOCK_EX);
//        }
//    } catch (Exception $e) {
//        file_put_contents($logFile, "\n\n ANDROID MESSAGE FOR ENGLISH :" . json_encode($e->getMessage()), FILE_APPEND | LOCK_EX);
//    }
//}
//
//function send_notification_android($registatoin_ids, $message, $fcm_key = ANDROID_FCM_KEY) {
//
//
//    // Set POST variables
//    $url = 'https://fcm.googleapis.com/fcm/send';
//
//    $fields = array(
//        'registration_ids' => $registatoin_ids,
//        'data' => $message,
//    );
//    $headers = array(
//        'Authorization: key=' . $fcm_key,
//        'Content-Type: application/json'
//    );
//    // Open connection
//    $ch = curl_init();
//
//    // Set the url, number of POST vars, POST data
//    curl_setopt($ch, CURLOPT_URL, $url);
//
//    curl_setopt($ch, CURLOPT_POST, true);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//    // Disabling SSL Certificate support temporarly
//    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//
//    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//
//    // Execute post
//    $result = curl_exec($ch);
//
//    // Close connection
//    curl_close($ch);
//
//    $resultArr = json_decode($result, true);
//
//    if ($resultArr['success'] == 1) {
//        return true;
//    }
//    return false;
//}
//
//function send_notification_ios($user_device_token, $body = array(), $pem_file_path = IOS_PEM_PATH, $app_is_live = APP_IS_LIVE) {
//
//    //Setup notification message
//    $body = array();
//    $body['aps']['alert'] = 'My push notification message!';
//    $body['aps']['sound'] = 'default';
//    $body['aps']['badge'] = 1;
//    $body['aps']['icon'] = 'appicon';
//    $body['aps']['vibrate'] = 'true';
//
//    // Construct the notification payload
////        if(!isset($body['aps']['sound'])){
////            $body['aps']['sound'] = "default";
////        }
//    //Setup stream (connect to Apple Push Server)
//    $ctx = stream_context_create();
//    stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file_path);
//    if ($app_is_live == 'true') {
//        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
//    } else {
//        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
//    }
//    stream_set_blocking($fp, 0);
//
//    if (!$fp) {
//        return TRUE;
//    } else {
//        $apple_expiry = time() + (90 * 24 * 60 * 60); //Keep push alive (waiting for delivery) for 90 days
//        foreach ($user_device_token as $key => $value) {
//            $apple_identifier = $key;
//            $device_token = $value["token"];
//            // check that current token is exist in bad token table or not.
//            // if not exist then send push notification.
//            $payload = json_encode($body);
//            //Enhanced Notification
//            $msg = pack("C", 1) . pack("N", $apple_identifier) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', str_replace(' ', '', $device_token)) . pack("n", strlen($payload)) . $payload;
//            //SEND PUSH
//            fwrite($fp, $msg);
//        }
//        fclose($fp);
//    }
//    return true;
//}
// Helpers/Common.php
function constants($key = null) {
    $constants = config('constants');
    return is_null($key) ? $constants : array_get($constants, $key);
}

function push_messages($key = null) {
    $constants = config('push_messages');
    return is_null($key) ? $constants : array_get($constants, $key);
}

//function sendPushNoti_v2_org($message = NULL, $device_type = 0, $cid = NULL, $req_id = NULL, $flag = 0) {
//
//    $APPLICATION_ID = PARSE_APP_ID;
//    $REST_API_KEY = PARSE_REST_API_KEY;
//
//    $url = 'https://api.parse.com/1/push';
//
//    $customer_id = $cid;
//
//    $conditions = array();
//    if ($cid) {
//        $cid = array_map('intval', explode(',', $cid));
//        $conditions = array('customer_id' => array('$in' => ($cid)));
//    }
//
//    $datas['message'] = $message;
//    $messa = $this->security_save($datas);
//    $message = $messa['message'];
//
//    // for iOS - device type  1
//    if ($device_type == '0' || $device_type == '1') {
//        $conditionsi = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('ios'))));
//        $data = array(
//            'where' => $conditionsi,
//            'data' => array(
//                "alert" => $message,
//                "sound" => "default",
//                "reach" => array("request_id" => $req_id, "flag" => $flag, "customer_id" => $customer_id),
//            ),
//        );
//        $_data = json_encode($data);
//    }
//
//
//    // for Android - device type = 2
//    if ($device_type == '0' || $device_type == '2') {
//        $conditionsa = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('android'))));
//        $data = array(
//            'where' => $conditionsa,
//            'data' => array(
//                "action" => "com.si.reach.UPDATE_STATUS",
//                "message" => $message,
//                "request_id" => $req_id,
//                "flag" => $flag,
//                "customer_id" => $customer_id
//            ),
//        );
//
//        $_data = json_encode($data);
//
//        $headers = array(
//            'X-Parse-Application-Id: ' . $APPLICATION_ID,
//            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($_data),
//        );
//
//        $curl = curl_init($url);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($curl);
//    }
//}
//
//function sendParsePush($msg = NULL, $sid = NULL) {
//
//    date_default_timezone_set('Asia/Kuwait');
//
//    $APPLICATION_ID = "GiAwmWBKEkcNlc6y3fCxVLH4XDFat0BuCXKDhSYc";
//    $REST_API_KEY = "gReohNTtsYLagIVM468cY6W74VeOY5mDH2LU9BM2";
//
//    $url = 'https://api.parse.com/1/push';
//
//    if (isset($sid) && !empty($sid)) {
//
//        $data = array(
//            'where' => array(
//                'deviceType' => array('$in' => array('android', 'ios')),
//                'customer_id' => (string) $sid,
//            ),
//            'data' => array(
//                "alert" => $msg,
//                "badge" => "Increment",
//                "sound" => "default",
//                "title" => "Title Name!",
//            ),
//        );
//    } else {
//
//        $data = array(
//            'where' => array(
//                'deviceType' => array('$in' => array('android', 'ios')),
//            ),
//            'data' => array(
//                "alert" => $msg,
//                "badge" => "Increment",
//                "sound" => "default",
//                "title" => "Title Name!",
//            ),
//        );
//    }
//
//    $_data = json_encode($data);
//
//    $headers = array(
//        'X-Parse-Application-Id: ' . $APPLICATION_ID,
//        'X-Parse-REST-API-Key: ' . $REST_API_KEY,
//        'Content-Type: application/json',
//        'Content-Length: ' . strlen($_data),
//    );
//
//    $curl = curl_init($url);
//    curl_setopt($curl, CURLOPT_POST, 1);
//    curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
//    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//    $result = curl_exec($curl);
//    return $result;
//}
//
//function security_save($array, $passed_array = array()) {
//
//    $array = array_map("trim", $array);
//    $array = array_map("strip_tags", $array);
//
//    return $array;
//}
//
//function send_push_notification($requestDataEn = NULL, $requestDataAr = NULL, $customer_id = NULL) {
//    try {
//        $logFile = 'storage/logs/send_push_notification_' . date('d-m-Y') . '.txt';
//
//        file_put_contents($logFile, "\n\n ========= send_push_notification LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
//        file_put_contents($logFile, "\n\n MESSAGE FOR ENGLISH :" . json_encode($requestDataEn), FILE_APPEND | LOCK_EX);
//        file_put_contents($logFile, "\n\n MESSAGE FOR ARABIC :" . json_encode($requestDataAr), FILE_APPEND | LOCK_EX);
//
//        // send push norification for english message in both device
//        send_push_notification_now($requestDataEn, 0, $customer_id);
//
//        // send push norification for arabic message in both device
//        send_push_notification_now($requestDataAr, 0, $customer_id);
//    } catch (Exception $e) {
//        file_put_contents($logFile, "\n\n send_push_notification  Exception :" . json_encode($e->getMessage()), FILE_APPEND | LOCK_EX);
//    }
//}
//
//function send_push_notification_now($message = NULL, $device_type = 0, $cid = NULL) {
//    try {
//        $logFile = 'storage/logs/send_push_notification_' . date('d-m-Y') . '.txt';
//        file_put_contents($logFile, "\n\n ========= send_push_notification_now LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
//        $APPLICATION_ID = env('PARSE_APP_ID');
//        $REST_API_KEY = env('PARSE_REST_API_KEY');
//
//        $url = 'https://api.parse.com/1/push';
//
//        $customer_id = $cid;
//
//        $conditions = array();
//        if (!empty($cid)) {
//            $cid = array_map('intval', explode(',', $cid));
//            $conditions = array('customer_id' => array('$in' => ($cid)));
//        }
//
//        $datas['message'] = $message;
//        $messa = security_save($datas);
//        $message = $messa['message'];
//
//        // for iOS - device type  1
//        if ($device_type == '0' || $device_type == '1') {
//            $conditionsi = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('ios'))));
//            $data = array(
//                'where' => $conditionsi,
//                'data' => array(
//                    "alert" => $message,
//                    "sound" => "default",
//                    "wiggle" => array("customer_id" => $customer_id),
//                ),
//            );
//            $_data = json_encode($data);
//            file_put_contents($logFile, "\n\n send_push_notification_now 1_data :" . json_encode($_data), FILE_APPEND | LOCK_EX);
//        }
//
//
//        // for Android - device type = 2
//        if ($device_type == '0' || $device_type == '2') {
//            $conditionsa = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('android'))));
//            $data = array(
//                'where' => $conditionsa,
//                'data' => array(
//                    "action" => "com.si.reach.UPDATE_STATUS",
//                    "message" => $message,
//                    "customer_id" => $customer_id
//                ),
//            );
//
//            $_data = json_encode($data);
//
//            file_put_contents($logFile, "\n\n send_push_notification_now 2_data :" . json_encode($_data), FILE_APPEND | LOCK_EX);
//        }
//
//        $headers = array(
//            'X-Parse-Application-Id: ' . $APPLICATION_ID,
//            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($_data),
//        );
//
//        file_put_contents($logFile, "\n\n send_push_notification_now headers :" . json_encode($headers), FILE_APPEND | LOCK_EX);
//
//        $curl = curl_init($url);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($curl);
//
//        file_put_contents($logFile, "\n\n send_push_notification_now result :" . json_encode($result), FILE_APPEND | LOCK_EX);
//    } catch (Exception $e) {
//        file_put_contents($logFile, "\n\n send_push_notification_now  Exception :" . json_encode($e->getMessage()), FILE_APPEND | LOCK_EX);
//    }
//}
//
////function sendPushNoti($message = NULL, $device_type = 0, $cid = NULL, $req_id = NULL, $flag = 0, $for=NULL) {
//    
//    $APPLICATION_ID = env('PARSE_APP_ID');
//    $REST_API_KEY = env('PARSE_REST_API_KEY');
//
//    $url = 'https://api.parse.com/1/push';
//
//    $conditions = array();
//    if ($cid) {
//        $cid = array_map('intval', explode(',', $cid));
//        $conditions = array('customer_id' => array('$in' => ($cid)));
//    }
//
//
//    // for iOS - device type  1
//    if ($device_type == '0' || $device_type == '1') {
//        $conditionsi = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('ios'))));
//        $data = array(
//            'where' => $conditionsi,
//            'data' => array(
//                "alert" => $message,
//                "sound" => "default",
//                "wiggle" => array("request_id" => $req_id, "flag" => $flag,"notification_for"=>$for),
//            ),
//        );
//        $_data = json_encode($data);
//        $headers = array(
//            'X-Parse-Application-Id: ' . $APPLICATION_ID,
//            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($_data),
//        );
//
//        $curl = curl_init($url);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($curl);
//    }
//
//
//    // for Android - device type = 2
//    if ($device_type == '0' || $device_type == '2') {
//        $conditionsa = array_merge($conditions, array('isNotificationOn' => true, 'deviceType' => array('$in' => array('android'))));
//        $data = array(
//            'where' => $conditionsa,
//            'data' => array(
//                "action" => "com.si.reach.UPDATE_STATUS",
//                "message" => $message,
//                "request_id" => $req_id,
//                "flag" => $flag
//            ),
//        );
//        $_data = json_encode($data);
//
//        $headers = array(
//            'X-Parse-Application-Id: ' . $APPLICATION_ID,
//            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($_data),
//        );
//
//        $curl = curl_init($url);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($curl);
//    }
//}

function sendPushNoti($message = NULL, $messageAr = NULL, $cid = NULL, $mid = NULL,$extraData=NULL) {
    
    
    /*
      https://parse.buddy.com/apps/b234df1c-e904-4d44-b82b-fd2048577344/settings/keys
      zahur@simplifiedinformatics.com
      zahur1234
     */
 
    date_default_timezone_set('Asia/Kuwait');
    
    $url = 'https://parse.buddy.com/parse/push/';
    
    $APPLICATION_ID = env('PARSE_BUDDY_API_KEY');
    $REST_API_KEY = env('PARSE_BUDDY_REST_KEY');
    $MASTER_KEY=env('PARSE_BUDDY_MASTER_KEY');
    
    $conditions = array();
    if ($cid) {
        $conditions = array('customer_id' => array('$in' => array($cid)));
    }

    // for iOS - English
    $conditionsi = array_merge($conditions, array('deviceType' => array('$in' => array('ios')), 'lang' => 'en'));
    $data = array(
        'where' => $conditionsi,
        'data' => array(
            "alert" => $message,
            "badge" => "Increment",
            "sound" => "default",
            "customer_id" => $cid,
            "message_id" => $mid
        ),
    );
    #pr($data);
    
    if($extraData!=NULL){
       $data['data']= array_merge($data['data'],$extraData); 
    }
    

    $ios_en_data = json_encode($data);
    #pr($_data, 1);

    $headers = array(
        'X-Parse-Application-Id: ' . $APPLICATION_ID,
        //'X-Parse-REST-API-Key: ' . $REST_API_KEY,
        'X-Parse-Master-Key: ' . $MASTER_KEY,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($ios_en_data),
    );

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $ios_en_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $ios_en_result = curl_exec($curl);

    // for iOS - Arabic
    $conditionsi = array_merge($conditions, array('deviceType' => array('$in' => array('ios')), 'app_lang' => 'ar'));
    $data = array(
        'where' => $conditionsi,
        'data' => array(
            "alert" => $messageAr,
            "badge" => "Increment",
            "sound" => "default",
            "customer_id" => $cid,
            "message_id" => $mid
        ),
    );
    #pr($data);
    if($extraData!=NULL){
       $data['data']= array_merge($data['data'],$extraData); 
    }
    $ios_ar_data = json_encode($data);
    #pr($_data, 1);

    $headers = array(
        'X-Parse-Application-Id: ' . $APPLICATION_ID,
        //'X-Parse-REST-API-Key: ' . $REST_API_KEY,
        'X-Parse-Master-Key: ' . $MASTER_KEY,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($ios_ar_data),
    );

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $ios_ar_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $ios_ar_result = curl_exec($curl);

    // for Android - English
    $conditionsa = array_merge($conditions, array('deviceType' => array('$in' => array('android')), 'app_lang' => 'en'));
    $data = array(
        'where' => $conditionsa,
        'data' => array(
            "action" => "com.si.sakani.UPDATE_STATUS",
            "sakani" => $message,
            "customer_id" => $cid,
            "message_id" => $mid
        ),
    );
    #pr($data);
    if($extraData!=NULL){
       $data['data']= array_merge($data['data'],$extraData); 
    }
    $android_en_data = json_encode($data);
    #pr($_data, 1);

    $headers = array(
        'X-Parse-Application-Id: ' . $APPLICATION_ID,
        //'X-Parse-REST-API-Key: ' . $REST_API_KEY,
        'X-Parse-Master-Key: ' . $MASTER_KEY,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($android_en_data),
    );

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $android_en_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $android_en_result = curl_exec($curl);


    // for Android - Arabic
    $conditionsa = array_merge($conditions, array('deviceType' => array('$in' => array('android')), 'app_lang' => 'ar'));
    $data = array(
        'where' => $conditionsa,
        'data' => array(
            "action" => "com.si.sakani.UPDATE_STATUS",
            "sakani" => $messageAr,
            "customer_id" => $cid,
            "message_id" => $mid
        ),
    );
    #pr($data);
    if($extraData!=NULL){
       $data['data']= array_merge($data['data'],$extraData); 
    }
    $android_ar_data = json_encode($data);
    #pr($_data, 1);

    $headers = array(
        'X-Parse-Application-Id: ' . $APPLICATION_ID,
        //'X-Parse-REST-API-Key: ' . $REST_API_KEY,
        'X-Parse-Master-Key: ' . $MASTER_KEY,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($android_ar_data),
    );

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $android_ar_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $android_ar_result = curl_exec($curl);
    $logFile = '../storage/logs/send_push_notification_' . date('d-m-Y') . '.txt';
    try {
       
        file_put_contents($logFile, "\n\n ========= send_push_notification LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========= MESSAGE FOR IOS ENGLISH [" . json_encode($ios_en_data) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========== RESULT FOR IOS ENGLISH [" . json_encode($ios_en_result) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========= MESSAGE FOR IOS ARABIC [" . json_encode($ios_ar_data) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========== RESULT FOR IOS ARABIC [" . json_encode($ios_ar_result) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========= MESSAGE FOR ANDROID ENGLISH [" . json_encode($android_en_data) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========== RESULT FOR ANDROID ENGLISH [" . json_encode($android_en_result) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========= MESSAGE FOR ANDROID ARABIC [" . json_encode($android_ar_data) . "] =========\n\n", FILE_APPEND | LOCK_EX);
        file_put_contents($logFile, "\n\n ========== RESULT FOR ANDROID ARABIC [" . json_encode($android_ar_result) . "] =========\n\n", FILE_APPEND | LOCK_EX);
    } catch (Exception $e) {
        file_put_contents($logFile, "\n\n send_push_notification  Exception :" . json_encode($e->getMessage()), FILE_APPEND | LOCK_EX);
    }
    
    #echo $result; die;
    // send parse push for property status update
}
