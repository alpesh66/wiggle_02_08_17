<?php

return [
    
    #DASHBOARD TEXT
    'totalCustomers' => ' Clients',
    'petStores'     => ' Paw Pages',
    'totalPets'     => ' Pets',
    'totalSP'       => ' Service Providers',
    'cmsPages'      => ' CMS Pages',
    'totalBooks'    => ' Bookings',
    'newCustomers'    => 'New Clients',
    'newproviders'    => 'New Service Providers',
    'newPaws'    => 'Paw Pages',
    'backtoAdmin'    => 'Back To Admin',

    'view'      => 'View Details',
    'delete'    => 'Delete',
    'edit'      => 'Edit Data',
    'superadmin' => 'Super Admin',

// Providers - Admin
    'providers' => 'Providers',
    'customers' => 'Clients',
    'technicians' => 'Specialists',
    'services' => 'Service',
    'bookings' => 'Bookings',
    'newCustomer' => 'New Clients',
    'newTechnicians' => 'New Specialists',

// Content
    'Content' => 'Content',

// Pets
    'Pet' => 'Pets',
    'editPet' => 'Edit Pet',
    
// User 
    'user' => 'Admin',
    'showUser' => 'Admin Detail',
    'addUser' => 'Add New Admin',
    'editUser' => 'Update Admin',
// Show Ratings 
    'rating' => 'Ratings',
    'showRating' => 'Rating Detail',
    'addRating' => 'Add New Rating',
    'editRating' => 'Update Rating',

// Role
    'superCreateRole' => 'Create New Role',

// Role
    'showRole' => 'Show Role',
    'editRole' => 'Edit Role',
    'Role' => 'Roles Management',

// Medical Card
    'medicalcard' => 'Add Medical Card',

// Category
    'createCategory'    => 'Create Category',
    'Category'    => 'Categories',
    'showCategory'    => 'Categories Details',
    'editCategory'    => 'Edit Categories',

// CMS
    'createCms'    => 'Create CMS',
    'cms'    => 'CMS',
    'showCms'    => 'CMS Details',
    'editCms'    => 'Edit CMS',

// Services - Providers
    'servicecreate'     => 'Create New Service',
    'serviceedit'       => 'Edit Service',
    'service'           => 'Services',
    'serviceshow'       => 'Show Services',

    'store' => 'Paw Pages',
    'showStore' => 'Paw Page',
    'editStore' => ' Update Paw Page',
    'createStore' => ' Add New Paw Page',

    'event' => 'Events',
    'editEvent' => 'Edit Event',
    'showEvent' => 'Event Details',

    'booking' => 'Booking Details',
    'vetbooking' => 'Veternary Booking Details',
    'grombooking' => 'Groomer Booking Details',
    'trainbooking' => 'Trainner Booking Details',

    // Transaction
    'transaction' => 'Transaction Details',

// Species
    'createbreed' => 'Create Breed',
    'showbreed' => 'Show Breed',
    'editbreeds' => 'Edit Breed',
    'Breed' => 'Breed Management',

    'createspecies' => 'Create Specie',
    'editspecies' => 'Edit Specie',

    'sales' => 'Sales Report',
];