@extends('layouts.master')
@section('title', trans('text.showRole'))
@section('content')
<div class="container">

    <h1>@lang('text.showRole') 

    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>                
                <tr><th> Name </th><td> {{ $temperament->name }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
