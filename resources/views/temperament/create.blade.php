@extends('layouts.master')
@section('title', trans('text.superCreateTemperament'))
@section('content')
<div class="container">
    <h1>@lang('text.superCreateTemperament')</h1>
    <hr/>
    {!! Form::open(['url' => '/temperament', 'class' => 'form-horizontal']) !!}
    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label' ,'required' => 'required']) !!}
        <div class="col-sm-6">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
<script type="text/javascript">
    $(document).ready(function () {


    });
</script>
@endsection