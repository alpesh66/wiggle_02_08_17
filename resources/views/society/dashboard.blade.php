@extends('layouts.master')
@section('title', 'Society Dashboard')
@section('content')
<style type="text/css">
  .mymargin{
    margin-left: 15%;
  }
  .fivcolMrgn{
   margin-left: 7%; 
  }
  .fivcolMrgn1{
   margin-left: 9%; 
  }
  .vl {
    border-left: 2px solid #01aef2;
    padding-left: 20px;
  }
  .centerAlign{
        text-align: center;
  }
  .custmMargin{
    margin-top: 7px;
    margin-bottom: 7px;
  }
</style>
<div class="clearfix"></div>
<div class="row">

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Successful Adopt</b><hr></h5>
          <h1 class="mt5">{{$successAdopt}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Cat</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptSucCat}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Dog</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptSucDog}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Total Foster</b><hr></h5>
          <h1 class="mt5">{{$foster}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Approved</b></h5>
            <h4 class="nomargin centerAlign">{{$fosterApprove}}</h4>
          </div>
          <div class="pull-left" style="margin-left: 30%;">
            <h5 class="md-title nomargin"><b>Pending</b></h5>
            <h4 class="nomargin centerAlign">{{$fosterPending}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Rejected</b></h5>
            <h4 class="nomargin centerAlign">{{$fosterRejected}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Total Volunteer</b><hr></h5>
          <h1 class="mt5">{{$volunteer}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Approved</b></h5>
            <h4 class="nomargin  centerAlign">{{$volunteerApprove}}</h4>
          </div>
          <div class="pull-left" style="margin-left: 30%;">
            <h5 class="md-title nomargin"><b>Pending</b></h5>
            <h4 class="nomargin centerAlign">{{$volunteerPending}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Rejected</b></h5>
            <h4 class="nomargin centerAlign">{{$volunteerRejected}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Total Surrender</b><hr></h5>
          <h1 class="mt5">{{$surrender}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Approved</b></h5>
            <h4 class="nomargin centerAlign">{{$surrenderApprove}}</h4>
          </div>
          <div class="pull-left" style="margin-left: 30%;">
            <h5 class="md-title nomargin"><b>Pending</b></h5>
            <h4 class="nomargin centerAlign">{{$surrenderPending}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Rejected</b></h5>
            <h4 class="nomargin centerAlign">{{$surrenderRejected}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Total Pets</b><hr></h5>
          <h1 class="mt5">{{$pet}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Draft</b></h5>
            <h4 class="nomargin centerAlign">{{$petDraft}}</h4>
          </div>
          <div class="pull-left" style="margin-left: 30%;">
            <h5 class="md-title nomargin"><b>Available</b></h5>
            <h4 class="nomargin centerAlign">{{$petAvailable}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Foster</b></h5>
            <h4 class="nomargin centerAlign">{{$petFoster}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Total Adopt</b><hr></h5>
          <h1 class="mt5">{{$adopt}}</h1>
        </div><!-- media-body --> 
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Cat</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptCat}}</h4>
          </div>
          <div class="pull-left fivcolMrgn">
            <h5 class="md-title nomargin"><b>Dog</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptDog}}</h4>
          </div>
          <div class="pull-left fivcolMrgn1 vl">
            <h5 class="md-title nomargin"><b>Approved</b></h5>
            <h4 class="nomargin centerAlign">{{$successAdopt}}</h4>
          </div>
          <div class="pull-left fivcolMrgn1">
            <h5 class="md-title nomargin"><b>Pending</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptPending}}</h4>
          </div>
          <div class="pull-left fivcolMrgn1">
            <h5 class="md-title nomargin "><b>Rejected</b></h5>
            <h4 class="nomargin centerAlign">{{$adoptRejected}}</h4>
          </div>       
        </div>
       
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>

  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Rank Activities</b><hr></h5>
        </div><!-- media-body -->  
        <div class="clearfix mt20">
          <?php
          $i=1;
          foreach($rankArray as $x => $x_value)
             {
              if($i==1){ ?>
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title custmMargin">{{$i}} : {{$x}} </h5>
                    </div>
                </div>
              <?php
              }else { ?>
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title custmMargin">{{$i}} : {{$x}} </h5>
                    </div>
                </div>
              <?php
              }
             $i++;
             }
          ?>

        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>
 

  @if($provider_type_count>0)
  <div class="col-md-4">
    <div class="panel panel-success-alt noborder">
      <div class="panel-heading noborder">
        <div class="media-body">
          <h5 class="md-title nomargin"><b>Kennel Request</b><hr></h5>
          <h1 class="mt5">{{$totalKennel}}</h1>
        </div><!-- media-body -->               
        <div class="clearfix mt20">
          <div class="pull-left">
            <h5 class="md-title nomargin"><b>Approved</b></h5>
            <h4 class="nomargin centerAlign">{{$approveKennel}}</h4>
          </div>
          <div class="pull-left" style="margin-left: 30%;">
            <h5 class="md-title nomargin"><b>Pending</b></h5>
            <h4 class="nomargin centerAlign">{{$pendingKennel}}</h4>
          </div>
          <div class="pull-right">
            <h5 class="md-title nomargin"><b>Rejected</b></h5>
            <h4 class="nomargin centerAlign">{{$rejectedKennel}}</h4>
          </div>       
        </div>
      </div><!-- panel-body -->
    </div><!-- panel -->
  </div>
  @endif
</div>
<div class="row">
  @if($volunteer_list)
   <div class="col-md-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Volunteer </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($volunteer_list as $item)
        <article class="media event">
          <a class="pull-left date">
            <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
            <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
          </a>
          <div class="media-body">
            @if($item->customer)
            <a href="{{ url('societies/volunteers/' . $item->id) }}" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
            <p>{{ $item->customer->mobile }}</p>
            @endif
          </div>
        </article>
        @endforeach                    
      </div>
    </div>
  </div>
  @endif
  @if($foster_list)
  <div class="col-md-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Foster Returns</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($foster_list as $item)
        <article class="media event">
          <a class="pull-left date">
            <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
            <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
          </a>
          <div class="media-body">
            <a href="{{ url('societies/fosters/' . $item->id) }}" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
            <p>{{ $item->customer->mobile }}</p>
          </div>
        </article>
        @endforeach                    
      </div>
    </div>
  </div>
  @endif

  @if($foster_list_pending)
  <div class="col-md-4">
    <div class="x_panel">
      <div class="x_title">
        <h2>Foster Available</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($foster_list_pending as $item)
        <article class="media event">
          <a class="pull-left date">
            <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
            <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
          </a>
          <div class="media-body">
            <a href="{{ url('societies/fosters/' . $item->id) }}" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
            <p>{{ $item->customer->mobile }}</p>
          </div>
        </article>
        @endforeach                    
      </div>
    </div>
  </div>
  @endif

</div>


{{--
<div class="row">
  @if($adopt_list)
    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Adopt </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($adopt_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
    
    
    @if($surrender_list)
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Surrender </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($surrender_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Pet </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($pet_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
               <a href="#" class="title">{{ ucfirst($item->name) }}</a>
              <p>{{ $item->breeds->name }}</p> 
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Kennel </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($kennel_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
               <a href="#" class="title">{{ ucfirst($item->name) }}</a>
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>

   
  </div>  --}}
@endsection