@extends('layouts.master')
@section('title', 'Society Dashboard')
@section('content')

  <div class="clearfix"></div>
  <div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> Total Adopt</span>
    <div class="count">{{$adopt}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-paw"></i> Total Foster</span>
    <div class="count">{{$foster}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-venus-mars"></i> Total Volunteer</span>
    <div class="count">{{$volunteer}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-scissors"></i> Total Surrender</span>
    <div class="count">{{$surrender}}</div>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-home"></i> Total Kennel</span>
    <div class="count">{{$kennel}}</div>

  </div>
 {{--  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-file"></i> Total Initiatives</span>
    <div class="count">{{$initiative}}</div>
    
  </div> --}}
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-check-square-o"></i> Total Pets</span>
    <div class="count">{{$pet}}</div>
  </div>
</div>

<div class="row">
  @if($adopt_list)
    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Adopt </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($adopt_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
    @if($foster_list)
    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Foster </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($foster_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
    @if($volunteer_list)
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Volunteer </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($volunteer_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
    @if($surrender_list)
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Surrender </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($surrender_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
    @endif
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Pet </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($pet_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
               <a href="#" class="title">{{ ucfirst($item->name) }}</a>
              <p>{{ $item->breeds->name }}</p> 
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>
     <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Kennel </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($kennel_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
               <a href="#" class="title">{{ ucfirst($item->name) }}</a>
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->customer->email }}</p>
            </div>
          </article>
          @endforeach                    
        </div>
      </div>
    </div>

   
  </div>  
@endsection