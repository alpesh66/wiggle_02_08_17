@extends('layouts.master')
@section('title', 'Adopt')

@section('content')
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h1>Adopt
                {{-- <a href="{{ url('societies/adopts/' . $adopt->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Adopt"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a> --}}
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['societies/adopts', $adopt->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Adopt',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                {!! Form::close() !!}
            </h1>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                 <tr><th>First Name </th><td> {{ $adopt->customer->firstname }} </td></tr>
                <tr><th>Middle Name </th><td> {{ $adopt->customer->middlename }} </td></tr>
                <tr><th> Last Name</th><td> {{ $adopt->customer->lastname }} </td></tr>
                <tr><th> Email </th><td> {{ $adopt->customer->email }} </td></tr>
                <tr><th> Gender </th><td> {{ showGender($adopt->customer->gender) }} </td></tr>
                <tr><th> Date of Birth </th><td> {{ $adopt->customer->dob }} </td></tr>
                <tr><th> Mobile </th><td> {{ $adopt->customer->mobile }} </td></tr>
                <!-- <tr><th> Address </th><td> {{ $adopt->customer->address }} </td></tr>
                <tr><th> Area </th><td> {{ $adopt->customer->area_id }} </td></tr> -->
                <tr><th> Status </th><td> {{ showStatus($adopt->customer->status) }} </td></tr>
                {{--<tr><th> Title </th><td> {{ $adopt->title }} </td></tr><tr><th> Description </th><td> {{ $adopt->description }} </td></tr>--}}
                <!-- <tr><th> Pet Information </th><td> {{ display_pet_name($adopt->pet) }} </td></tr> -->
                <tr><th> Pet </th><td> 
                @if($customer->pet)
                    @foreach($customer->pet as $pets)
                        <?php 
                         if(isset($adopt->pet[0])){  ?>
                            @if($adopt->pet[0]->id==$pets->id)
                                <a class="btn btn-success" href="{{ url('societies/pet', $pets->id) }}">  
                                <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a> 
                            @else
                                <a class="btn btn-success" style="pointer-events: none;cursor: default;" href="javascript:void(0)">  
                                <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a>
                            @endif
                        <?php 
                        }else { ?>
                            <a class="btn btn-success" style="pointer-events: none;cursor: default;" href="javascript:void(0)">  
                                <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a>
                        <?php 
                        } ?>
                    @endforeach

                @else
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
                @endif

                </td></tr>
               

            </tbody>
        </table>
         <h1>Pet Information</h1>
   <table class="table table-bordered table-striped table-hover" id="customers">
            <thead>

                <tr>
                   
                    <th>Name </th>
                    <th>Date of Birth</th>

                    <th>Species</th>
                    <th>Gender</th>

                    <th>Weight</th>
                    <th>Size</th>

                    <th>Microchip</th>
                    <th>Grooming</th>

                    <!-- <th>Temperament</th> -->
                    <th>Origin</th>
                    <th>Veternary</th>
                    <th>Groom</th>
                </tr>
                 <tbody>

                        @foreach($adopt->pet as $item)
                        <tr>
                            <td> {{ $item->name }} </td>
                            <td> {{ $item->dob }} </td>

                            <td> {{ $item->breeds->name }} </td>
                            <td> {{ showGender($item->gender) }} </td>

                            <td> {{ $item->weight }} </td>
                            <td> {{ $item->size }} </td>

                            <td> {{ $item->chipno }} </td>
                            <td> {{ $item->grooming }} </td>
                            
                            
                            
                            {{--<td> {{ $item->temperament }} </td>--}}
                            <td> {{ $item->origin }} </td>
                            <td> {{ $item->next_vaccination_date }} </td>
                            <td> {{ $item->next_groom_appointment }} </td>
                        </tr>
                        @endforeach   
                </tbody> 
            </thead>
            
        </table>


        <!--<a class="btn btn-danger reply" href="#" >Reply</a>-->
    </div>

</div>
        </div>
      </div>
</div>
@endsection
