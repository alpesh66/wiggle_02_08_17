<a href="{{ url('societies/adopts/' . $adopts->id) }}" class="btn btn-success btn-xs" title="View Adopt"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        @if($adopts->parent_id == 0)
                        <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$adopts->id}}" title="{{$adopts->title}}" ids="{{$adopts->id}}" customer_id="{{$adopts->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $adopts->customer_id  }}" data-id="{{ $adopts->customer_id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
                         @endif
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['societies/adopts', $adopts->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Adopt" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Adopt',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}