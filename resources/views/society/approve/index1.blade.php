@extends('layouts.master')
@section('title', trans('text.customers'))
@section('content')
@section('linkcss')
<style type="text/css">
    
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection
<div class="container">

    <h1>@lang('text.customers')</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTable">
            <thead>

                <tr>
                    <th>Name</th>
                    <th>Email</th> 
                    <th>Number</th>
                    <th>Status</th>
                    <th>Number of Pets</th>
                    <th>Dogs</th>
                    <th>Cats</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach($customer as $item)
                <tr>
                    <td><?php echo $item['firstname'].' '.$item['middlename'].' '.$item['lastname'] ?></td>
                    <td>{{ $item['email'] }}</td>
                    <td>{{ $item['mobile'] }}</td>
                    <td>{{ $item['status'] }}</td>
                    <td><?php $pets=get_total_customer_pet($item['id']);
                    echo $pets['total_pet'];
                    ?></td>
                    <td><?php echo $pets['dog']; ?></td>
                    <td><?php echo $pets['cat']; ?></td>
                    <td><a href="{{url('societies/customers/'.$item['id'])}}" title="View">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a></td>
                    
                </tr>
                @endforeach
            </tbody>
            
        </table>

    </div>

</div>

@endsection

@section('jqueries')

<script>
$(document).ready(function(){
    $('#dataTable').DataTable( {
        "order": [[ 0, "asc" ]],
        "pageLength": 15
    });
    
$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('societies/approve/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('societies/approve/') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#customers').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("societies/approve") }}',
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [
            { data: 'id', name:'id', "searchable": false},
            { data: 'name', name: 'firstname' },

            { data: 'email', name: 'email' },
            { data: 'mobile', name: 'mobile' },
            { data: 'status', name: 'status'},
            { data: 'nospets', name: 'nospets',"orderable":false},
            { data: 'nosdogs', name: 'nosdogs',"orderable":false},
            { data: 'noscats', name: 'noscats',"orderable":false},
            /*{ data: 'action', name: 'action', "searchable": false,"orderable":false},*/
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    
    var counter = table.rows('.selected').data().length;

    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="firstname">First Name</option> <option value="pets.name">Pet Name</option> <option value="email">Email</option><option value="mobile">Mobile</option>  <option value="pets.chipno">Chip No.</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url("societies/approve")}}'+'/'+id,
            
        success:function(response){
            console.log(response);
            if(response.success)
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}

   $('#customers').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });

});


</script>
@endsection