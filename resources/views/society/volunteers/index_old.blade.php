@extends('layouts.master')
@section('title', 'Volunteer')

@section('content')
<div class="container">

    <h1>Volunteer {{-- <a href="{{ url('societies/adopts/create') }}" class="btn btn-primary btn-xs" title="Add New Adopt"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a> --}}</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTable">
            <thead>
                <tr>
                    <th>S.No</th><th> Customer Name </th><th> Title </th>
                    <th> Request Date </th><th> Reply Date </th><th> Status </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($volunteers as $item)
                {{-- */$x++;/* --}}
                <tr @if($item->is_read == 1 && $item->sender == 1) class="read_color" @endif>
                    <td>{{ $x }}</td>
                    <td><a style="font-weight: bold;" href="{{url('societies/customers/'.$item->customer->id)}}">{{ $item->customer->firstname }} {{ $item->customer->lastname }}</a></td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->created_at  }}</td>
                    <td>{{ ($item->parent_id == 0)?NULL:$item->updated_at }}</td>
                    <td class="status{{ $item->id  }}"> @if($item->status == 1) Approved @elseif($item->status == 2) Reject @else Pending  @endif </td>
                    <td>
                        <a href="{{ url('societies/volunteers/' . $item->id) }}" class="btn btn-success btn-xs" title="View Volunteer"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                       @if($item->parent_id == 0)
                        <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$item->id}}" title="{{$item->title}}" ids="{{$item->id}}" customer_id="{{$item->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $item->customer->id  }}" data-id="{{ $item->customer->id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
                         @endif
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['societies/volunteers', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Volunteer" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Volunteer',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $volunteers->render() !!} </div>
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reply</h4>
      </div>
           
       
          
          

    {!! Form::open(['url' => 'societies/forsters/reply', 'class' => 'form-horizontal', 'id'=>'request_reply_form']) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-8">
                   {!! Form::hidden('customer_id', null, ['class' => 'form-control customer_id_replay']) !!}
                    {!! Form::hidden('title', null, ['class' => 'form-control title_reply']) !!}
                    {!! Form::hidden('ids', null, ['class' => 'form-control ids_reply']) !!}
                </div>
            </div>
           
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <select name="status" id="status" class="form-control">
                        <option value="0">Pending</option>
                        <option value="1">Approved</option>
                        <option value="2">Reject</option>
                        
                    </select>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Comments', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
          
           
            


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="button" class="btn btn-primary submit"><i class="fa fa-refresh fa-spin hide" id="loader"></i> Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    {!! Form::close() !!}

          
        
      
        </div>
    </div>
</div>
<link href="{{ asset('vendor/custom/general.css') }}" rel="stylesheet" type="text/css" >
@endsection
@section('jqueries')
<script type="text/javascript">
  $(document).ready(function() {
    $('#dataTable').DataTable( {
        "order": [[ 0, "asc" ]],
        "pageLength": 15
    });
  });
$('.replyClick').click(function(){
  
  $('.customer_id_replay').val($(this).attr('customer_id'));
  $('.title_reply').val($(this).attr('title'));
  $('.ids_reply').val($(this).attr('ids'));
  
});
$('.submit').on('click', function(){
    var id = $('.ids_reply').val();
    var status = $('#status').val();
    $('.submit').attr("disabled", true);
    $('#loader').removeClass("hide");
   $.ajax({
           type:'POST',
           url:'{{ url('/societies/volunteers/reply') }}',
           data:{'id':id,'customer_id':$('.customer_id_replay').val(),'status':$('#status').val(),'title':$('.title_reply').val(),'description':$('#description').val()},
           success:function(data){
              $('.submit').attr("disabled", false);
              $('#loader').addClass("hide");
              $('#myModal').modal('toggle');
              $('.replyStatus'+id).css('display','none');
               if($('#status').val() == 1) {
                $('.status'+id).html('Approved');
              } else if($('#status').val() == 2) {
                $('.status'+id).html('Reject');
              } else {
                $('.status'+id).html('Pending');  
              }
              
              var response = jQuery.parseJSON(data);
              $.bootstrapGrowl(response.message, { type: 'success',offset: {from: 'top', amount: 70},align: 'right',width: 250,allow_dismiss: false});
           },
           error: function(data){
              var errors = data.responseJSON;
              $.each(errors, function(index, value) {
                  $('#'+index).parent().parent().removeClass('has-error');
                  $('#'+index).next().remove()
                  $('#'+index).parent().parent().addClass('has-error');
                  $('#'+index).after('<p class="help-block">'+value+'</p>');
           });
    }
    });    
});
</script>
@endsection
