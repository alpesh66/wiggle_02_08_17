@extends('layouts.master')
@section('title', 'Volunteer')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1>Volunteer
                    {{-- <a href="{{ url('societies/volunteers/' . $volunteer->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit volunteer"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a> --}}
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['societies/volunteers', $volunteer->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Volunteer',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                    {!! Form::close() !!}
                </h1>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <tbody>
                            <tr><th>First Name </th><td> {{ $volunteer->customer->firstname }} </td></tr>
                            <tr><th>Middle Name </th><td> {{ $volunteer->customer->middlename }} </td></tr>
                            <tr><th> Last Name</th><td> {{ $volunteer->customer->lastname }} </td></tr>
                            <tr><th> Email </th><td> {{ $volunteer->customer->email }} </td></tr>
                            <tr><th> Gender </th><td> {{ showGender($volunteer->customer->gender) }} </td></tr>
                            <tr><th> Date of Birth </th><td> {{ $volunteer->customer->dob }} </td></tr>
                            <tr><th> Mobile </th><td> {{ $volunteer->customer->mobile }} </td></tr>
                            {{--<tr><th> Address </th><td> {{ $volunteer->customer->address }} </td></tr>
                                                                                    <tr><th> Area </th><td> {{ $volunteer->customer->area_id }} </td></tr>--}}
                            <tr><th> Status </th><td> {{ showStatus($volunteer->customer->status) }} </td></tr>
                            {{--<tr><th> Title </th><td> {{ $volunteer->title }} </td></tr>--}}
                            <tr><th> Comment </th><td> {{ $volunteer->description }} </td></tr>
                            <tr><th> Start Date </th><td> {{ $volunteer->startdate }} </td></tr>
                            <tr><th> End Date </th><td> {{ $volunteer->enddate }} </td></tr>
                            <tr>
                                <th> Pet </th><td> 
                                @if($customer->pet)
                                    @foreach($customer->pet as $pets)
                                        <a class="btn btn-success" style="pointer-events: none;cursor: default;" href="javascript:void(0)">  
                                        <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a>
                                    @endforeach
                                @else
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
                                @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
