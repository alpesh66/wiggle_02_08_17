<a href="{{ url('societies/volunteers/' . $volunteers->id) }}" class="btn btn-success btn-xs" title="View Volunteer"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
@if($volunteers->parent_id == 0)
    <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$volunteers->id}}" title="Volunteer reply" ids="{{$volunteers->id}}" customer_id="{{$volunteers->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $volunteers->customer_id  }}" data-id="{{ $volunteers->customer_id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
 @endif
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['societies/volunteers', $volunteers->id],
    'style' => 'display:inline'
]) !!}
    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Volunteer" />', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Delete Volunteer',
            'onclick'=>'return confirm("Confirm delete?")'
    ));!!}
{!! Form::close() !!}