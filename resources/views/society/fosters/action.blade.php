<a href="{{ url('societies/fosters/' . $fosters->id) }}" class="btn btn-success btn-xs" title="View Foster"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>

@if($fosters->parent_id == 0)
    <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$fosters->id}}" data-breed="{{ $fosters->breed_id }}" title="Foster Reply" ids="{{$fosters->id}}" customer_id="{{$fosters->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $fosters->customer->id  }}" data-id="{{ $fosters->customer->id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
@endif
{!! Form::open([
'method'=>'DELETE',
'url' => ['societies/fosters', $fosters->id],
'style' => 'display:inline'
]) !!}
    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Foster" />', array(
    'type' => 'submit',
    'class' => 'btn btn-danger btn-xs',
    'title' => 'Delete Foster',
    'onclick'=>'return confirm("Confirm delete?")'
    ));!!}
{!! Form::close() !!}