@extends('layouts.master')
@section('title', 'Foster')

@section('content')
<div class="container">

    <h1>Fosters {{-- <a href="{{ url('societies/adopts/create') }}" class="btn btn-primary btn-xs" title="Add New Adopt"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a> --}}</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTable">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th> Customer Name </th>
                    <th> Pet Type</th>
                    <th> Start Date </th>
                    <th> End Date </th>
                    <th> Title </th>
                    <th> Request Date</th>
                    <th> Reply Date</th>
                    <th> Status </th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {{-- */$x=0;/* --}}
                @foreach($fosters as $item)
                {{-- */$x++;/* --}}
                <tr @if($item->is_read == 1 && $item->sender == 1) class="read_color" @endif>
                     <td>{{ $x }}</td>
                    <td><a style="font-weight: bold;" href="{{url('societies/customers/'.$item->customer->id)}}">{{ $item->customer->firstname }} {{ $item->customer->lastname }}</a></td>
                    <td>{{ ($item->breed_id==1)?'Cat':'Dog' }}</td>
                    <td>{{ $item->startdate }}</td>
                    <td>{{ $item->enddate }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->created_at  }}</td>
                    <td>{{ ($item->parent_id == 0)?NULL:$item->updated_at }}</td>
                    <td class="status{{ $item->id  }}"> @if($item->status == 1) Approved @elseif($item->status == 2) Reject @else Pending  @endif </td>
                    <td>
                        <a href="{{ url('societies/fosters/' . $item->id) }}" class="btn btn-success btn-xs" title="View Foster"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        @if($item->parent_id == 0)
                        <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$item->id}}" data-breed="{{ $item->breed_id }}" title="{{$item->title}}" ids="{{$item->id}}" customer_id="{{$item->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $item->customer->id  }}" data-id="{{ $item->customer->id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
                        @endif
                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['societies/fosters', $item->id],
                        'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Foster" />', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Foster',
                        'onclick'=>'return confirm("Confirm delete?")'
                        ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $fosters->render() !!} </div>
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reply</h4>
            </div>





            {!! Form::open(['url' => 'societies/forsters/reply', 'class' => 'form-horizontal', 'id'=>'request_reply_form']) !!}

            <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-8">
                    {!! Form::hidden('customer_id', null, ['class' => 'form-control customer_id_replay']) !!}
                    {!! Form::hidden('title', null, ['class' => 'form-control title_reply']) !!}
                    {!! Form::hidden('breed', null, ['class' => 'form-control breed_reply']) !!}
                    {!! Form::hidden('ids', null, ['class' => 'form-control ids_reply']) !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <select name="status" id="status" class="form-control">
                        <option value="0">Pending</option>
                        <option value="1">Approved</option>
                        <option value="2">Reject</option>

                    </select>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group petdog {{ $errors->has('pet') ? 'has-error' : ''}}" style="display: none">
                {!! Form::label('pet','Pet', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select('pet',$pet_dog,null,['class' => 'form-control selectpetdog']) !!}
                    {!! $errors->first('pet', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group petcat {{ $errors->has('pet') ? 'has-error' : ''}}" style="display: none">
                {!! Form::label('pet','Pet', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::select('pet',$pet_cat,null,['class' => 'form-control selectpetcat']) !!}
                    {!! $errors->first('pet', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Comments', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>





            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="button" class="btn btn-primary submit"><i class="fa fa-refresh fa-spin hide" id="loader"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
            {!! Form::close() !!}




        </div>
    </div>
</div>
<link href="{{ asset('vendor/custom/general.css') }}" rel="stylesheet" type="text/css" >
@endsection
@section('jqueries')
<script type="text/javascript">
    // dataTable Sort
    $(document).ready(function() {
      $('#dataTable').DataTable( {
          "order": [[ 0, "asc" ]],
          "pageLength": 15
      });
    });

    $('.replyClick').click(function () {
        $('.customer_id_replay').val($(this).attr('customer_id'));
        $('.title_reply').val($(this).attr('title'));
        $('.ids_reply').val($(this).attr('ids'));
        var breed=$(this).attr('data-breed');
        $('.breed_reply').val(breed);
        if(breed=="1"){
        $('.petcat').show();$('.petdog').hide();
        }else{
            $('.petcat').hide();$('.petdog').show();
        }
    });
    $('.submit').on('click', function () {
        var id = $('.ids_reply').val();
        var status = $('#status').val();
        $('.submit').attr("disabled", true);
        $('#loader').removeClass("hide");
        var breed=$('.breed_reply').val();
        var pet='';
        if(breed=="1"){
            pet=$('.selectpetcat').val();
        }else{
            pet=$('.selectpetdog').val();
        }
        $.ajax({
            type: 'POST',
            url: '{{ url('societies/fosters/reply') }}',
            data: {
                'id': id,
                'pet':pet,
                'customer_id': $('.customer_id_replay').val(), 
                'status': $('#status').val(), 
                'title': $('.title_reply').val(), 
                'description': $('#description').val()
            },
            success: function (data) {
                $('.submit').attr("disabled", false);
                $('#loader').addClass("hide");
                $('#myModal').modal('toggle');
                $('.replyStatus' + id).css('display', 'none');
                if ($('#status').val() == 1) {
                    $('.status' + id).html('Approved');
                } else if ($('#status').val() == 2) {
                    $('.status' + id).html('Reject');
                } else {
                    $('.status' + id).html('Pending');
                }
                var response = jQuery.parseJSON(data);
                $.bootstrapGrowl(response.message, {type: 'success', offset: {from: 'top', amount: 70}, align: 'right', width: 250, allow_dismiss: false});
            },
            error: function (data) {
                var errors = data.responseJSON;
                $.each(errors, function (index, value) {
                    $('#' + index).parent().parent().removeClass('has-error');
                    $('#' + index).next().remove()
                    $('#' + index).parent().parent().addClass('has-error');
                    $('#' + index).after('<p class="help-block">' + value + '</p>');
                });
            }
        });
    });
</script>
@endsection
