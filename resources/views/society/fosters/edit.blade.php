@extends('layouts.master')
@section('title', 'Foster')

@section('content')
<div class="container">

    <h1>Edit Foster {{ $foster->id }}</h1>

    {!! Form::model($foster, [
        'method' => 'PATCH',
        'url' => ['societies/fosters', $foster->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                {!! Form::label('customer_id', 'Customer Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('customer_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('provider_id') ? 'has-error' : ''}}">
                {!! Form::label('provider_id', 'Provider Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('provider_id', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('provider_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('is_read') ? 'has-error' : ''}}">
                {!! Form::label('is_read', 'Is Read', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('is_read', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('is_read', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('is_read', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('sender') ? 'has-error' : ''}}">
                {!! Form::label('sender', 'Sender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('sender', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('sender', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
@endsection