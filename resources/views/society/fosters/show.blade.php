@extends('layouts.master')
@section('title', 'Foster')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1>Foster
                    {{-- <a href="{{ url('societies/fosters/' . $foster->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit foster"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a> --}}
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['societies/fosters', $foster->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Foster',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                    {!! Form::close() !!}
                </h1>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <tbody>
                            <tr><th>First Name </th><td> {{ $foster->customer->firstname }} </td></tr>
                            <tr><th>Middle Name </th><td> {{ $foster->customer->middlename }} </td></tr>
                            <tr><th> Last Name</th><td> {{ $foster->customer->lastname }} </td></tr>
                            <tr><th> Email </th><td> {{ $foster->customer->email }} </td></tr>
                            <tr><th> Gender </th><td> {{ showGender($foster->customer->gender) }} </td></tr>
                            <tr><th> Date of Birth </th><td> {{ $foster->customer->dob }} </td></tr>
                            <tr><th> Mobile </th><td> {{ $foster->customer->mobile }} </td></tr>
                            {{--<tr><th> Address </th><td> {{ $foster->customer->address }} </td></tr>
                                                                                    <tr><th> Area </th><td> {{ $foster->customer->area_id }} </td></tr>--}}
                            <tr><th> Status </th><td> {{ showStatus($foster->customer->status) }} </td></tr>
                            {{--<tr><th> Title </th><td> {{ $foster->title }} </td></tr>
                            --}}
                            <tr><th> Comment </th><td> {{ $foster->description }} </td></tr>
                            <tr><th> Start Date </th><td> {{ $foster->startdate }} </td></tr>
                            <tr><th> End Date </th><td> {{ $foster->enddate }} </td></tr>
                            <tr>
                                <th> Pet </th><td> 
                                @if($customer->pet)
                                    @foreach($customer->pet as $pets)
                                        <a class="btn btn-success" style="pointer-events: none;cursor: default;" href="javascript:void(0)">  
                                        <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a>
                                    @endforeach
                                @else
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
                                @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
