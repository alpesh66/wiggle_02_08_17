@extends('layouts.master')
@section('title', 'Create Breed')
@section('content')
<div class="container">
    <h1>Create New Pet</h1>
    <hr/>

    {!! Form::open(['url' => '/societies/pet', 'class' => 'form-horizontal', 'files' => true]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
                {!! Form::label('dob', 'Dob', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('dob', null, ['class' => 'form-control meratime']) !!}
                    {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('breed') ? 'has-error' : ''}}">
                {!! Form::label('breed', 'Specie', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('breed', $breed,null, ['class' => 'form-control breedStatus']) !!}
                    {!! $errors->first('breed', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('gender', '1') !!} Male</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('gender', '0', true) !!} Female</label>
            </div>
                    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <!-- <div class="form-group {{ $errors->has('height') ? 'has-error' : ''}}">
                {!! Form::label('height', 'Height', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('height', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('height', '<p class="help-block">:message</p>') !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('photo', 'Picture', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('temperament') ? 'has-error' : ''}}">
                {!! Form::label('temperament', 'Temperament', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('temperament[]',$temperaments, null, ['class' => 'form-control','multiple'=>true,'id'=>'temperament']) !!}
                    {!! $errors->first('temperament', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('energy_level') ? 'has-error' : ''}}">
                {!! Form::label('energy_level', 'Energy Level', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('energy_level',['','High', 'Medium', 'Low'], null, ['class' => 'form-control','id'=>'energy_level']) !!}
                    {!! $errors->first('energy_level', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('maintenance') ? 'has-error' : ''}}">
                {!! Form::label('maintenance', 'Maintenance', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('maintenance',['','High', 'Medium', 'Low'], null, ['class' => 'form-control','id'=>'maintenance']) !!}
                    {!! $errors->first('maintenance', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <!-- <div class="form-group {{ $errors->has('good_with_children') ? 'has-error' : ''}}">
                {!! Form::label('good_with_children', 'Good With Children', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('good_with_children', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('good_with_children', '<p class="help-block">:message</p>') !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('daily_exercises') ? 'has-error' : ''}}">
                {!! Form::label('daily_exercises', 'Daily Exercises', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('daily_exercises', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('daily_exercises', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('origin') ? 'has-error' : ''}}">
                {!! Form::label('origin', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('origin', null, ['class' => 'form-control originText']) !!}
                    {!! Form::select('origin',$specie, null, ['class' => 'form-control originSelect']) !!}
                    {!! $errors->first('origin', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('size') ? 'has-error' : ''}}">
                {!! Form::label('size', 'Size', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('size',$size, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('chipno') ? 'has-error' : ''}}">
                {!! Form::label('chipno', 'Microchip', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('chipno', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('chipno', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('weight') ? 'has-error' : ''}}">
                {!! Form::label('weight', 'Weight', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('weight', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('grooming') ? 'has-error' : ''}}">
                {!! Form::label('grooming', 'Grooming', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('grooming', $frequency, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('grooming', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('notes') ? 'has-error' : ''}}">
                {!! Form::label('notes', 'Notes',['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('notes', null, ['class' => 'form-control','rows'=>3]) !!}
                    {!! $errors->first('notes', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

<script type="text/javascript">
    $(document).ready(function(){

        // $('.time').bootstrapMaterialDatePicker({ date: false });
$('.originText').show();
$('.originSelect').hide();
        $('.meratime').datetimepicker({
                 sideBySide:false,
                 format: 'DD-MM-YYYY',
                 maxDate: new Date
                 });
        $("#temperament").select2({
                    maximumSelectionLength: 5
                });
        $('.breedStatus').on('change', function(){
            var selValue = ($(this).val());

            if(selValue == 1){
                // Cat
                $('.originText').show();
                $('.originSelect').hide();

            } else {
                // Dog
                $('.originText').hide();
                $('.originSelect').show();
            }
        });
    });
</script>
@endsection
