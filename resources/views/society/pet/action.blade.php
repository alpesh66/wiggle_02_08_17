<a href="{{ url('societies/pet/'.$pets->id) }}" title="@lang('text.view')">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('societies/pet/'.$pets->id.'/edit') }}" title="@lang('text.edit')"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$pets->id}}" data-model="users" title="@lang('text.delete')" name="deleteButton"><i class="fa fa-trash-o"></i></button>
<a href="{{ url('societies/medicalcrdd/'.$pets->id) }}" title="@lang('text.medicalcard')"> 
    <button class="btn btn-danger btn-xs">M</button></a>