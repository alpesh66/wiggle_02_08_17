@extends('layouts.master')
@section('title', 'Pet Log')
@section('content')
<div class="container">

    <h1>Pet Log</h1>
    <div class="table">
         <table class="table table-bordered table-striped table-hover" id="pets">
            <thead>
                <tr>
                    <th> Sr. No</th>
                    <th> Pet Name </th>
                    <th> Datetime</th>
                    <th> Admin </th>
                    <th> Status</th>
                    <th> Customer </th>
                </tr>
            </thead>            
        </table>

    </div>

</div>
@endsection

@section('jqueries')

<script>
$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {
        $('#post').val('');
        dt.ajax.reload();
    }
};

var table = $('#pets').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6 text-right'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [1, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['reload'],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("societies/petlog") }}',
            data: function (d) {               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },
        },
        columns: [
            {data: 'id', name:'pets.id'},
            { data: 'pet', name: 'pets.name',render:function(data,type,row){return '<a href="{{url('societies/pet')}}/'+row.id+'" ><i class="fa fa-eye"></i> '+data+'</a>';} },
            { data: 'datetime', name: 'datetime'},
            { data: 'admin', name: 'admin'},
            /*{ data: 'provider', name: 'provider',render:function(data,type,row){ return (row.admin !=null)?data+'-'+row.admin:data;} },*/
            { data: 'status', name: 'pet_log.status' },
            { data: 'customer', name: 'customer',render:function(data,type,row){
                    if(data==null) return '';
                    return '<a href="{{url('societies/customers')}}/'+row.customerid+'" ><i class="fa fa-eye"></i> '+data+'</a>';} },
        ],
    });
$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form">\n\
    <div class="form-group">\n\
        <select id="filter_header" name="name" >\n\
            <option value="pets.name">Pet Name</option>\n\
            <option value="providers.name">Provider</option> \n\
            <option value="customers.name">Customer</option>\n\
        </select>\n\
    </div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

});


</script>
@endsection