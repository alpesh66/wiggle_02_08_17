<a href="{{ url('societies/surrenders/' . $surrenders->id) }}" class="btn btn-success btn-xs" title="View Foster"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
@if($surrenders->parent_id == 0)
    <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$surrenders->id}}" title="Surrender Reply" ids="{{$surrenders->id}}" customer_id="{{$surrenders->customer_id}}"" data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $surrenders->customer_id  }}" data-id="{{ $surrenders->customer_id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
 @endif
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['societies/surrenders', $surrenders->id],
    'style' => 'display:inline'
]) !!}
    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Surrender" />', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Delete Foster',
            'onclick'=>'return confirm("Confirm delete?")'
    ));!!}
{!! Form::close() !!}