@extends('layouts.master')
@section('title', 'Surrender')

@section('content')
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h1>Surrender
                {{-- <a href="{{ url('societies/surrenders/' . $surrender->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit surrender"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a> --}}
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['societies/surrenders', $surrender->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Surrender',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                {!! Form::close() !!}
            </h1>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                
                  <tr><th>First Name </th><td> {{ $surrender->customer->firstname }} </td></tr>
                <tr><th>Middle Name </th><td> {{ $surrender->customer->middlename }} </td></tr>
                <tr><th> Last Name</th><td> {{ $surrender->customer->lastname }} </td></tr>
                <tr><th> Email </th><td> {{ $surrender->customer->email }} </td></tr>
                <tr><th> Gender </th><td> {{ showGender($surrender->customer->gender) }} </td></tr>
                <tr><th> Date of Birth </th><td> {{ $surrender->customer->dob }} </td></tr>
                <tr><th> Mobile </th><td> {{ $surrender->customer->mobile }} </td></tr>
                <!-- <tr><th> Address </th><td> {{ $surrender->customer->address }} </td></tr>
                <tr><th> Area </th><td> {{ $surrender->customer->area_id }} </td></tr> -->
                <tr><th> Status </th><td> {{ showStatus($surrender->customer->status) }} </td></tr>
                {{--<tr><th> Title </th><td> {{ $surrender->title }} </td></tr><tr><th> Description </th><td> {{ $surrender->description }} </td></tr>--}}
                <!-- <tr><th> Pet Information </th><td> {{ display_pet_name($surrender->pet) }} </td></tr> -->
               <tr><th> Pet </th><td> 
               @if($customer->pet)
                   @foreach($customer->pet as $pets)
                       @if(isset($surrender->pet[0]->id) && $surrender->pet[0]->id==$pets->id)
                           <a class="btn btn-success" href="{{ url('societies/pet', $pets->id) }}">  
                           <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a> 
                       @else
                       <a class="btn btn-success" style="pointer-events: none;cursor: default;" href="javascript:void(0)">  
                           <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a>
                       @endif
                   @endforeach

               @else
                   <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
               @endif

               </td></tr>

            </tbody>
        </table>
          <h1>Pet Information</h1>
   <table class="table table-bordered table-striped table-hover" id="customers">
            <thead>

                <tr>
                   
                    <th>Name </th>
                    <th>Date of Birth</th>
                    <th>Specie</th>
                    <th>Gender</th>
                    <!-- <th>Height</th> -->
                    <th>Weight</th>
                    <th>Size</th>
                    <th>Microchip</th>
                    <th>Grooming</th>
                    <th>Temperament</th>
                    <th>Origin</th>
                    <th>Veternary</th>
                    <th>Groom</th>
                </tr>
                 <tbody>
                        @foreach($surrender->pet as $item)
                        <tr>
                            <td> {{ $item->name }} </td>
                            <td> {{ $item->dob }} </td>
                            <td> {{ $item->breeds->name }} </td>
                            <td> {{ showGender($item->gender) }} </td>
                            {{--<td> {{ $item->height }} </td>--}}
                            <td> {{ $item->weight }} </td>
                            <td> {{ $item->size }} </td>
                            <td> {{ $item->temperament }} </td>
                            <td> {{ $item->origin }} </td>
                            <td> {{ $item->chipno }} </td>
                            <td> {{ $item->grooming }} </td>
                            <td> {{ $item->veternary }} </td>
                            <td> {{ $item->groom }} </td>
                        </tr>
                        @endforeach   
                </tbody> 
            </thead>
            
        </table>
    </div>

</div>
        </div>
      </div>
</div>


@endsection
@section('jqueries')
<script type="text/javascript">
$(document).ready(function(){
    $('.reply_form').css('display','none');
     $('.reply').click(function(){
        $('.reply_form').css('display','block');
     });
    $('.reply_cancel').click(function(){
        $('.reply_form').css('display','none');
    });
})
</script>
@endsection
