<div class="col-md-12 col-sm-12 col-xs-12 reply_form" @if($errors->any()) style="display:block;" @endif>
        <div class="x_panel">
          <div class="x_title">
            <h2>Reply</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    {!! Form::open(['url' => 'societies/surrenders/reply', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-6">
                    {!! Form::hidden('customer_id', $surrender->customer_id, ['class' => 'form-control']) !!}
                </div>
            </div>
           
            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
          
           
            


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            <a class="btn btn-danger reply_cancel" href="{{ url()->previous() }}" >Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

            </div>
        </div>
      </div>