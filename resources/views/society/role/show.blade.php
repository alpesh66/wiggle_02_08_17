@extends('layouts.master')
@section('title', trans('text.showRole'))
@section('content')
<div class="container">

    <h1>@lang('text.showRole') 

    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                <tr><th> Name </th><td> {{ $role->display_name }} </td></tr>
                <tr><th> Description </th><td> {{ $role->description }} </td></tr>
                <tr><th> Permissions </th><td> 
                {{ display_role_permissions($role->perms) }}
                </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
