@extends('layouts.master')
@section('title', 'Clients Detail')
@section('content')

@if(strpos(\Request::path(),'societies/customers') !==FALSE)
  @php $setAction = 'societies/pet'; @endphp
@else
    @php $setAction = 'providers/pet'; @endphp
@endif
<div class="container">

    <h1>Clients</h1>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
               
                <tr><th>First Name </th><td> {{ $customer->firstname }} </td></tr>
                <tr><th>Middle Name </th><td> {{ $customer->middlename }} </td></tr>
                <tr><th> Last Name</th><td> {{ $customer->lastname }} </td></tr>
                <tr><th> Email </th><td> {{ $customer->email }} </td></tr>
                <tr><th> Gender </th><td> {{ showGender($customer->gender) }} </td></tr>
                <tr><th> Date of Birth </th><td> {{ $customer->dob }} </td></tr>
                <tr><th> Mobile </th><td> {{ $customer->mobile }} </td></tr>
                <tr><th> Block </th><td> {{ $customer->block }} </td></tr>
                <tr><th> Street </th><td> {{ $customer->street }} </td></tr>
                <tr><th> House </th><td> {{ $customer->house }} </td></tr>
                <tr><th> Appartment </th><td>
                    @if(!empty($customer->appartment))
                        {{ $customer->appartment }} 
                    @endif
                  </td></tr>
                <tr><th> Area </th><td> 
                    @if(!empty($customer->area->name))
                        {{ $customer->area->name }} 
                    @endif
                    </td></tr>
                <tr><th> Status </th><td> {{ showStatus($customer->status) }} </td></tr>
                <tr><th> Pet </th><td> 
                @if($customer->pet)
                    @foreach($customer->pet as $pets)
                    <a class="btn @if($pets->breed == 1) btn-success @else btn-info @endif btn-xs" href="{{ url($setAction, $pets->id) }}"> {{ $pets->name}}  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a> 
                    @endforeach

                @else
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
                @endif

                </td></tr>

                  
            </tbody>
        </table>
    </div>

    <div class="table">
        <h3>Pets</h3>
        <table class="table table-bordered table-striped table-hover" id="pets">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th> Name </th>
                    <th> Breed </th>
                    <th> Weight </th>
                    <th> DOB </th>
                    <th> Gender </th>
                    <th> Microchip </th>
                    <th>Grooming #</th>
                    <th>Upcoming Vaccination</th>
                    <th>Upcoming Grooming</th>
                </tr>
            </thead>
            <tbody>{{-- */$x=1;/* --}}
                @foreach($customer->pet as $pet)
                <tr>
                    <td>{{ $x++ }}</td>
                    <td> {{ $pet->name }} </td>
                    <td> {{ $pet->breedname }} </td>
                    <td> {{ $pet->weight }} </td>
                    <td> {{ $pet->dob }} </td>
                    <td> {{ ($pet->gender==1)?'Male':'Female' }} </td>
                    <td> {{ $pet->chipno }} </td>
                    <td>{{ $pet->grooming }}</td>
                    <td>
                        <?php
                        echo $pet->next_vaccination_date;
                        /*if($pet->veternary=="" || $pet->veternary=="0000-00-00"){
                            $date = date_create($pet->created_at);
                            date_modify($date, $pet->grooming);
                            echo $next_veternary_date = date_format($date, 'Y-m-d');
                        }
                        else{
                            $datev = date_create($pet->veternary);                        
                            echo $next_groom_date = date_format($datev, 'Y-m-d');
                        }*/
                        ?>
                    </td>
                    <td>
                    <?php
                    echo $pet->next_groom_appointment;
                   /* if($pet->next_groom_appointment=="" || $pet->next_groom_appointment=="0000-00-00"){
                        $date = date_create($pet->created_at);
                        date_modify($date, $pet->grooming);
                        echo $next_groom_date = date_format($date, 'Y-m-d');
                        
                    }
                    else{
                        $dateg = date_create($pet->next_groom_appointment);                        
                        echo $next_groom_date = date_format($dateg, 'Y-m-d');
                    }         */               
                    ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

{{-- booking jquery --}}

@section('jqueries')

<script>
$(document).ready(function(){
    
});
</script>
@endsection
