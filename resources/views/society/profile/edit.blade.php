@extends('layouts.master')
@section('title', 'Edit Provider')
@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Profile <h1>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

    {!! Form::model($provider, [
        'method' => 'PATCH',
        'url' => ['/societies/profile_update'],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('photo', 'Logo', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}

                @if(!empty($provider->photo))
                    <img src="{{$provider->photo}}" width="25%">
                @endif
                </div>
            </div>

             <div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
                {!! Form::label('icon', 'Icon', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('icon', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}

                @if(!empty($provider->icon))
                    <img src="{{$provider->icon}}" width="25%">
                @endif
                </div>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{--<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div> --}}
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', 'About', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about_ar') ? 'has-error' : ''}}">
                {!! Form::label('about_ar', 'About Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
                {!! Form::label('area', 'Area', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('area', $area ,null, ['class' => 'form-control']) !!}
                    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('start') ? 'has-error' : ''}}">
                {!! Form::label('start', 'Start', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('start', null, ['class' => 'form-control startdate', 'readonly' => 'readonly']) !!}
                    {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('end') ? 'has-error' : ''}}">
                {!! Form::label('end', 'End', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('end', null, ['class' => 'form-control enddate', 'readonly' => 'readonly']) !!}
                    {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                    <div class="col-sm-6">                            
                        <img width="32" height="32" onclick="valideopenerform();" style="cursor:pointer;" src="{{url('/images/map_icon.png')}}">
                    </div>
            </div>

            {{-- {!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' => 'latitude']) !!}

            {!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' => 'longitude']) !!} --}}


            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', 'Contact', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('contact',  $country_code[0], ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


             <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                {!! Form::label('website', 'Website', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('website', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
                {!! Form::label('facebook', 'Facebook', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
                {!! Form::label('twitter', 'Twitter', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
                {!! Form::label('instagram', 'Instagram', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
           

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


    </div>
</div>
</div>
</div>
@endsection

@section('jqueries')
    <script type="text/javascript">
    function valideopenerform(lat, long) {

        var lat = $('#latitude').val();
        var long = $('#longitude').val();

        if (!lat) {
            lat = '29.31166';
        }
        if (!long) {
            long = '47.48176';
        }

        var popy = window.open('{{url('map')}}?lat=' + lat + '&long=' + long, 'popup_form', 'location=no, menubar=no, status=no, top=50%, left=50%, height=550, width=750');
    }
    
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();;

        $('#tags_type').select2({
          placeholder: "Select Type",
          allowClear: true
        });
        $('.sele_type').select2({
          placeholder: "Select Country",
        }).val([{{ $country_code[0] }}]).trigger('change');


        $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now' 
        });

        $('.enddate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now'
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
        });

    });
    </script>
@endsection
