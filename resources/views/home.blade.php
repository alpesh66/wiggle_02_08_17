@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
<style type="text/css">
  .mymargin{
    margin-left: 15%;
  }
  .fivcolMrgn{
   margin-left: 7%; 
  }
  .fivcolMrgn1{
   margin-left: 9%; 
  }
  .vl {
    border-left: 2px solid #808285;
    padding-left: 20px;
  }
  .centerAlign{
        text-align: center;
  }
  .custmMargin{
    margin-top: 7px;
    margin-bottom: 7px;
  }
</style>
{{-- <div class="page-title">
  <div class="title_left">
    <h3>Dashboard</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div>
</div> --}}
<div class="title_left">
    <h3>Dashboard</h3>
  </div>

<div class="clearfix"></div>
{{--
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> @lang('text.customers')</span>
    <div class="count">{{$customers}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-circle"></i> @lang('text.services')</span>
    <div class="count">{{$service_count}}</div>

  </div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">

    <span class="count_top"><i class="fa fa-scissors"></i> @lang('text.technicians')</span>
    <div class="count">{{$tech_count}}</div>

  </div>

  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-check-square-o"></i> @lang('text.bookings')</span>
    <div class="count">{{$bookings}}</div>
  </div>
</div>
--}}

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Bookings</b><hr></h5>
                    <h1 class="mt5">{{$bookings}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Previous</b></h5>
                        <h4 class="nomargin">{{$previouss}}</h4>
                    </div>
                    <div class="pull-right">
                        <h5 class="md-title nomargin"><b>Upcoming</b></h5>
                        <h4 class="nomargin">{{$upcomings}}</h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Clients</b><hr></h5>
                    <h1 class="mt5">{{$customers}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Last 30 Days</b></h5>
                        <h4 class="nomargin">{{$LastThirtyDay}}</h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>

    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Last 30 Day Booking</b><hr></h5>
                    <h1 class="mt5">{{$booking_LastThirtyDays}}</h1>
                </div><!-- media-body -->               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Manual Booking</b></h5>
                        <h4 class="nomargin">{{$booking_by_web}}</h4>
                    </div>
	                <div class="pull-right">
                        <h5 class="md-title nomargin"><b>App Booking</b></h5>
                        <h4 class="nomargin">{{$booking_by_app}}</h4>
                    </div>		
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    
    <div class="col-md-4">
         <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Pets</b><hr></h5>
                    <h1 class="mt5">{{$pets_count}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Cats</b></h5>
                        <h4 class="nomargin">{{$cat}}</h4>
                    </div>
                    <div class="pull-right">
                        <h5 class="md-title nomargin"><b>Dogs</b></h5>
                        <h4 class="nomargin">{{$dog}}</h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>

    <div class="col-md-4">
         <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Today Booking</b><hr></h5>
                    <h1 class="mt5">{{$todaybooking}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b></b></h5>
                        <h4 class="nomargin"></h4>
                    </div>
                    <div class="pull-right">
                        <h5 class="md-title nomargin"><b></b></h5>
                        <h4 class="nomargin"></h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>

    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Kennel Request</b><hr></h5>
                    <h1 class="mt5">{{$totalKennel}}</h1>
                </div><!-- media-body -->               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Approve</b></h5>
                        <h4 class="nomargin">{{$approveKennel}}</h4>
                    </div>
                    <div class="pull-left" style="margin-left: 30%;">
                        <h5 class="md-title nomargin"><b>Pending</b></h5>
                        <h4 class="nomargin">{{$pendingKennel}}</h4>
                    </div>
                    <div class="pull-right">
                        <h5 class="md-title nomargin"><b>Rejected</b></h5>
                        <h4 class="nomargin">{{$rejectedKennel}}</h4>
                    </div>      
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>	
</div>

<div class="title_left">
    <h3>Society Dashboard</h3>
</div>

<div class="row">
    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Successful Adopt</b><hr></h5>
            <h1 class="mt5">{{$successAdopt}}</h1>
          </div><!-- media-body -->               
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Cats</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptSucCat}}</h4>
            </div>
            <div class="pull-right">
              <h5 class="md-title nomargin"><b>Dogs</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptSucDog}}</h4>
            </div>       
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Total Adopt</b><hr></h5>
            <h1 class="mt5">{{$adopt}}</h1>
          </div><!-- media-body --> 
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Cat</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptCat}}</h4>
            </div>
            <div class="pull-left fivcolMrgn">
              <h5 class="md-title nomargin"><b>Dog</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptDog}}</h4>
            </div>
            <div class="pull-left fivcolMrgn1 vl">
              <h5 class="md-title nomargin"><b>Approved</b></h5>
              <h4 class="nomargin centerAlign">{{$successAdopt}}</h4>
            </div>
            <div class="pull-left fivcolMrgn1">
              <h5 class="md-title nomargin"><b>Pending</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptPending}}</h4>
            </div>
            <div class="pull-left fivcolMrgn1">
              <h5 class="md-title nomargin "><b>Rejected</b></h5>
              <h4 class="nomargin centerAlign">{{$adoptRejected}}</h4>
            </div>       
          </div>
         
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Total Foster</b><hr></h5>
            <h1 class="mt5">{{$foster}}</h1>
          </div><!-- media-body -->               
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Approved</b></h5>
              <h4 class="nomargin centerAlign">{{$fosterApprove}}</h4>
            </div>
            <div class="pull-left" style="margin-left: 30%;">
              <h5 class="md-title nomargin"><b>Pending</b></h5>
              <h4 class="nomargin centerAlign">{{$fosterPending}}</h4>
            </div>
            <div class="pull-right">
              <h5 class="md-title nomargin"><b>Rejected</b></h5>
              <h4 class="nomargin centerAlign">{{$fosterRejected}}</h4>
            </div>       
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Total Volunteer</b><hr></h5>
            <h1 class="mt5">{{$volunteer}}</h1>
          </div><!-- media-body -->               
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Approved</b></h5>
              <h4 class="nomargin  centerAlign">{{$volunteerApprove}}</h4>
            </div>
            <div class="pull-left" style="margin-left: 30%;">
              <h5 class="md-title nomargin"><b>Pending</b></h5>
              <h4 class="nomargin centerAlign">{{$volunteerPending}}</h4>
            </div>
            <div class="pull-right">
              <h5 class="md-title nomargin"><b>Rejected</b></h5>
              <h4 class="nomargin centerAlign">{{$volunteerRejected}}</h4>
            </div>       
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Total Surrender</b><hr></h5>
            <h1 class="mt5">{{$surrender}}</h1>
          </div><!-- media-body -->               
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Approved</b></h5>
              <h4 class="nomargin centerAlign">{{$surrenderApprove}}</h4>
            </div>
            <div class="pull-left" style="margin-left: 30%;">
              <h5 class="md-title nomargin"><b>Pending</b></h5>
              <h4 class="nomargin centerAlign">{{$surrenderPending}}</h4>
            </div>
            <div class="pull-right">
              <h5 class="md-title nomargin"><b>Rejected</b></h5>
              <h4 class="nomargin centerAlign">{{$surrenderRejected}}</h4>
            </div>       
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Total Pets</b><hr></h5>
            <h1 class="mt5">{{$pet}}</h1>
          </div><!-- media-body -->               
          <div class="clearfix mt20">
            <div class="pull-left">
              <h5 class="md-title nomargin"><b>Draft</b></h5>
              <h4 class="nomargin centerAlign">{{$petDraft}}</h4>
            </div>
            <div class="pull-left" style="margin-left: 30%;">
              <h5 class="md-title nomargin"><b>Available</b></h5>
              <h4 class="nomargin centerAlign">{{$petAvailable}}</h4>
            </div>
            <div class="pull-right">
              <h5 class="md-title nomargin"><b>Foster</b></h5>
              <h4 class="nomargin centerAlign">{{$petFoster}}</h4>
            </div>       
          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Rank Activities</b><hr></h5>
          </div><!-- media-body -->  
          <div class="clearfix mt20">
            <?php
            $i=1;
            foreach($rankArray as $x => $x_value)
               {
                if($i==1){ ?>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                          <h5 class="md-title custmMargin">{{$i}} : {{$x}} </h5>
                      </div>
                  </div>
                <?php
                }else { ?>
                  <div class="clearfix mt20">
                      <div class="pull-left">
                          <h5 class="md-title custmMargin">{{$i}} : {{$x}} </h5>
                      </div>
                  </div>
                <?php
                }
               $i++;
               }
            ?>

          </div>
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>
</div>
{{--
<div class="title_left">
    <h3>Provider Dashboard</h3>
</div>
<div class="row">
    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Most Used Services</b><hr></h5>
          </div><!-- media-body -->
              @if(isset($category))         
                  @foreach($category as $item)
                  <div class="clearfix mt20">
                    <div class="pull-left">
                      <h5 class="md-title nomargin">{{ $item['name'] }} : {{$item['count']}} </h5>
                    </div>
                  </div>
                  @endforeach
              @else     
                  <div class="clearfix mt20">
                  <div class="pull-left">
                      <h1 class="mt5"></h5>
                  </div>
                </div>      
              @endif    
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>

     <div class="col-md-4">
       <div class="panel panel-success-alt noborder">
         <div class="panel-heading noborder">
           <div class="media-body">
             <h5 class="md-title nomargin"><b>Last Rating And Review</b><hr></h5>
           </div><!-- media-body -->
           <div class="clearfix mt20">
           @foreach($rating_list as $item)
             <article class="media event">
               <a class="pull-left date">
                 <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                 <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
               </a>
               <div class="media-body">
                   <a href="#" class=""><label>Customer name : </label> {{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
                   <p><label>Rate : </label>{{ $item->rate }}</p>
                   <p><label>Review : </label> {{ $item->review }}</p>
               </div>
           </article>
           @endforeach          
           </div>
         </div><!-- panel-body -->
       </div>
    </div>  
</div>--}}

@endsection
