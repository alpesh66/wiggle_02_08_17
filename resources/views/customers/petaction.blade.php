<a href="{{ url('pet/'.$customer_pets->id) }}" title="@lang('text.view')">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>


    <button class="btn btn-danger btn-xs deactive_pet" name="{{$customer_pets->id}}"><i class=" fa fa-trash"></i></button>



<a href="{{ url('pet/'.$customer_pets->id.'/edit') }}" title="@lang('text.edit')"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

<a href="{{ url('medicalcd/'.$customer_pets->id) }}" title="@lang('text.medicalcard')"> 
    <button class="btn btn-danger btn-xs">M</button></a>
