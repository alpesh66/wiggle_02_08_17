@extends('layouts.master')
@section('title', trans('text.Pet'))
@section('content')
<div class="container">

    <h1>@lang('text.Pet') 
    <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a>
       {{--  <a href="{{ url('pet/' . $pet->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Pet"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['pet', $pet->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Pet',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!} --}}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

                @if(!empty($pet->photo))
                   <tr><th>Photo</th><td>
                   {{-- <img src="{{$pet->photo}}" width="25%"> --}}
                   <a href="{{$pet->photo}}" target="_blank" > Click Here</a>
                   </td></tr>
                @endif
                <tr><th> Name </th><td> {{ $pet->name }} </td></tr>
                <tr><th> Dob </th><td> {{ $pet->dob }} </td></tr>
                <tr><th> Specie </th><td> {{ $pet->breeds->name }} </td></tr>
                <tr><th> Gender </th><td>  @if($pet->gender == 0)
                                Female
                                @else
                                Male
                                @endif
                </td></tr>
                {{--<tr><th> Height </th><td> {{ $pet->height }} </td></tr>--}}
                <tr><th> Weight </th><td> {{ $pet->weight }} </td></tr>
                <tr><th> Size </th><td> {{ $pet->sizes->name }} </td></tr>
                <tr><th> Temperament </th><td> {{ $pet->temperament }} </td></tr>
                <tr><th> Origin </th><td> {{ $pet->origin }} </td></tr>
                <tr><th> Chip No </th><td> {{ $pet->chipno }} </td></tr>
             
            </tbody>
        </table>
    </div>

</div>
@endsection
