<a href="{{ route('customers.show', $customer->id) }}" title="View">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('customers/'.$customer->id.'/edit') }}" title="Edit"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$customer->id}}" data-model="customers" title="Delete" name="deleteButton"><i class="fa fa-trash-o"></i></button>
