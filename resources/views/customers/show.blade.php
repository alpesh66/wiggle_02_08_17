@extends('layouts.master')
@section('title', 'Clients Detail')
@section('content')
<div class="container">

    <h1>Clients 
        {{-- <a href="{{ url('customers/' . $customer->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit customers"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['customers', $customer->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete customers',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!} --}}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
            @if(!empty($customer->photo))

               <tr><th>Photo</th><td>
               {{-- <img src="{{$customer->photo}}" width="25%"> --}}
                    <a href="{{$customer->photo}}" target="_blank" > Click Here</a>
               </td></tr>
            @endif
                <tr><th>First Name </th><td> {{ $customer->firstname }} </td></tr>
                <tr><th>Middle Name </th><td> {{ $customer->middlename }} </td></tr>
                <tr><th> Last Name</th><td> {{ $customer->lastname }} </td></tr>
                <tr><th> Email </th><td> {{ $customer->email }} </td></tr>
                <tr><th> Gender </th><td> {{ showGender($customer->gender) }} </td></tr>
                <tr><th> Date of Birth </th><td> {{ $customer->dob }} </td></tr>
                <tr><th> Mobile </th><td> {{ $customer->mobile }} </td></tr>
                <tr><th> Block </th><td> {{ $customer->block }} </td></tr>
                <tr><th> Street </th><td> {{ $customer->street }} </td></tr>
                <tr><th> House </th><td> {{ $customer->house }} </td></tr>
                <tr><th> Appartment </th><td> {{ $customer->appartment }} </td></tr>
                <tr><th> Area </th><td> 
                    @if(!empty($customer->area->name))
                        {{ $customer->area->name }} 
                    @endif
                </td></tr>
                <tr><th> Status </th><td> {{ showStatus($customer->status) }} </td></tr>
                <tr><th> Pet </th><td> 
                @if($customer->pet)
                    @foreach($customer->pet as $pets)
                       
                    <a class="btn btn-success" href="{{ url('pet', $pets->id) }}">  
                        <i class="small-icon detailCusPets @if($pets->breed == 1) cat @elseif($pets->breed == 2) dog @endif"></i><span>{{ $pets->name}}  </span></a> 
                    @endforeach

                @else
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/>
                @endif

                </td></tr>

                  
            </tbody>
        </table>
    </div>

    <div class="table">
        <h3>Pets</h3>
        <table class="table table-bordered table-striped table-hover" id="pets">
            <thead>
                <tr>
                    <th>S.No</th><th> Name </th><th> Specie </th><th> Weight </th><th> Date of Birth </th><th> Gender </th><th> Microchip </th><th>Grooming Frequency</th><th>Upcoming Vaccination</th><th>Upcoming Grooming</th><th>Action</th>
                </tr>
            </thead>            
        </table>
    </div>

    <div class="table">
        <h3>Bookings</h3>
        <table class="table table-bordered table-striped table-hover" id="dataTables">
            <thead>
                <tr>
                    <th>S.No</th><th>Pet</th><th>Technician</th><th>Provider</th><th>Service</th><th>Type</th><th>Appointment On</th><th>Status</th>
                </tr>
            </thead>          
        </table>
    </div>


</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reason of Deactivation</h4>
      </div>
      <div class="modal-body">
         
            
                <div>
                                <div>
                <label>{!! Form::radio('disabled_question', 'Death',null,['class' => 'disabled_question','checked'=>'checked']) !!} Death</label>
            </div>
            <div>
                <label>{!! Form::radio('disabled_question', 'Surrender to a shelter',null,['class' => 'disabled_question']) !!} Surrender to a shelter</label>
            </div>
            <div>
                <label>{!! Form::radio('disabled_question', 'New Parent',null,['class' => 'disabled_question']) !!} New Parent</label>
                <label>{!! Form::text('disabled_comments', null,['class' => 'form-control disabled_comments new_parent','placeholder'=>'Email or Mobile No']) !!}</label>
                
                
            </div>
            <div>
                <label>{!! Form::radio('disabled_question', 'Move to new country',null,['class' => 'disabled_question ']) !!} Move to New Country</label>
                <label>{!! Form::text('disabled_comments', null, ['class' => 'form-control disabled_comments move_to_new','placeholder'=>'Country Name']) !!}</label>
            </div>
                    <label>{!! Form::hidden('pet_id', 3, ['class' => 'form-control pet_ids']) !!}</label>
                </div>
            
           
        <div>
            {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_pet']) !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
        </div>
     
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>     --}}
      </div>
    </div>

  </div>
</div>

@endsection

@section('jqueries')

<script>





$(document).ready(function(){

$('body').on('click', '.deactive_pet', function(e){
   var petid = $(this).attr('name');
   $('.pet_ids').val(petid);
   $('#myModal').modal('show');   
});

var custid = '1';


$('.disabled_comments').css('display','none');

$('.disabled_question').change(function() {
    if($('.disabled_question:checked').val() == 'New Parent'){
        $('.new_parent').css('display','block');
        $('.move_to_new').css('display','none');
    } else if($('.disabled_question:checked').val() == 'Move to new country') {
        $('.new_parent').css('display','none');
        $('.move_to_new').css('display','block');
    } else {
        $('.new_parent').css('display','none');
        $('.move_to_new').css('display','none');
    }
});

$('.submit_pet').click(function(){
    
     var  disabled_comments_val = '';
     if($('.disabled_question:checked').val() == "New Parent") {
        disabled_comments_val = $('.disabled_comments:first').val();
     } else if($('.disabled_question:checked').val() == "Move to new country"){
       disabled_comments_val = $('.disabled_comments:last').val();
     } 
     
     $.ajax({
        type: 'post',
        data: {pet_id:$('.pet_ids').val(),disabled_question:$('.disabled_question:checked').val(),
                        disabled_comments:disabled_comments_val,status:'0'},
        url: '{{ url('customers/customerPetDeactivation')}}',            
        success:function(response){
            if(response.success)
            {
                table.ajax.reload();
                $('#myModal').modal('hide');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        },
    });
});


$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('customers/'.$customer->id.'/customerpets/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('customers/'.$customer->id.'/customerpets/') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#pets').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td:first-child'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: "{{ url('customers/'.$customer->id.'/customerpets')}}",
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },

            { data: 'breed', name: 'breed' },
            { data: 'weight', name: 'weight' },
            { data: 'dob', name: 'dob' },
            { data: 'gender', name: 'gender'},
            { data: 'chipno', name: 'chipno'},
            { data: 'grooming', name: 'grooming'},
            { data: 'next_vaccination_date', name: 'next_vaccination_date'},
            { data: 'next_groom_appointment', name: 'next_groom_appointment'},
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="pets.name">Name</option><option value="pets.dob">DOB</option> <option value="pets.breed">Breed</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url("customers/'.$customer->id.'/customerpets")}}'+'/'+id,
        success:function(response){
        console.log(response);
        if(response.success)
        {
            table.ajax.reload();
            $('.deleteButton').addClass('disabled');
            grown_noti(response.message,'success');
        }
        else
        {
            table.ajax.reload();
            $('.deleteButton').addClass('disabled');
            grown_noti(response.message,'danger');
        }
        
        },
    });
}

   $('#pets').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });

});


</script>
{{-- @endsection --}}


{{-- booking jquery --}}

{{-- @section('jqueries') --}}

<script>
$(document).ready(function(){
$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('customers/'.$customer->id.'/customerbookings') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('customers/'.$customer->id.'/customerbookings') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#dataTables').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch2'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td:first-child'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: "{{ url('customers/'.$customer->id.'/customerbookings')}}",
            data: function (d) {
               
                d.name = $('select[name=cname]').val();
                d.operator = $('select[name=coperator]').val();
                d.post = $('input[name=cpost]').val();
            },

        },
        columns: [
            { data: 'bookid', name:'bookid', "searchable": false},

            { data: 'petname', name: 'petname' },
            { data: 'techname', name: 'techname' },
            { data: 'provider_name', name: 'provider_name' },
            { data: 'servicename', name: 'servicename' },
            { data: 'type_id', name: 'type_id' },
            { data: 'start', name: 'start' },
            { data: 'status', name: 'status'},
            // { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearchCustomer', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch2').append('<form method="POST" class="form-inline advanceSearchCustomer" role="form"><div class="form-group"><select id="filter_header" name="cname"><option value="technicians.name">Technician</option> <option value="pets.name">Pet</option> <option value="categories.name">Service</option><option value="email">Email</option>  <option value="status">Status</option></select></div><div class="form-group"><select name="coperator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="cpost" id="post"></div><button type="submit" class="btn btn-primary advance-searchCustomer-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url('booking')}}'+'/'+id+'/delbooking',
            
        success:function(response){
            console.log(response);
            if(response.success)
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}

   $('#dataTables').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });

});

</script>
@endsection

