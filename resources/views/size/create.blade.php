@extends('layouts.master')
@section('title', 'size')

@section('content')
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Create New size</h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    {!! Form::open(['url' => '/size', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                                <div class="checkbox">
                <label>{!! Form::radio('status', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('status', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

            </div>
        </div>
      </div>
</div>
@endsection