@extends('layouts.master')
@section('title', 'size')

@section('content')
<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h1>size {{ $size->id }}
                <a href="{{ url('size/' . $size->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit size"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['size', $size->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete size',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                {!! Form::close() !!}
            </h1>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $size->id }}</td>
                </tr>
                <tr><th> Name </th><td> {{ $size->name }} </td></tr><tr><th> Status </th><td> {{ $size->status }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
        </div>
      </div>
</div>
@endsection
