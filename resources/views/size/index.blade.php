@extends('layouts.master')
@section('title', 'size')

@section('content')
<div class="container">

    <h1>Size <a href="{{ url('/size/create') }}" class="btn btn-primary btn-xs" title="Add New size"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> Name </th><th> Status </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($size as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->name }}</td><td>{{ $item->status }}</td>
                    <td>
                       
                        <a href="{{ url('/size/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit size"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {{-- {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/size', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete size" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete size',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!} --}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $size->render() !!} </div>
    </div>

</div>
@endsection
