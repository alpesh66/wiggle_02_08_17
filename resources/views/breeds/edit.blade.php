@extends('layouts.master')
@section('title', trans('text.editbreeds'))
@section('content')
<div class="container">

    <h1>@lang('text.editbreeds')</h1>

    {!! Form::model($species, [
        'method' => 'PATCH',
        'url' => ['/species', $species->id],
        'class' => 'form-horizontal'
    ]) !!}

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label' ,'required' => 'required']) !!}
            <div class="col-sm-6">
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
            {!! Form::label('name_ar', 'Name Ar *', ['class' => 'col-sm-3 control-label' ,'required' => 'required']) !!}
            <div class="col-sm-6">
                {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
            </div>
        </div>           
        <div class="form-group hide {{ $errors->has('breed_id') ? 'has-error' : ''}}">
                {!! Form::label('breed_id', 'Specie', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('breed_id',$breed, 2, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">

        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
@endsection