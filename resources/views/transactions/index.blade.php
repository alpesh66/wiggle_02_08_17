@extends('layouts.master')
@section('title', trans('text.transaction'))
@section('content')
@section('linkcss')
<style type="text/css">
    
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection
<div class="container">

    <h1>@lang('text.transaction')</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="transaction">
            <thead>

                <tr>
                    <th></th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Amount</th> 
                    <th>Transaction ID </th>
                    <th>Track ID</th>
                    <!-- <th>Device ID</th> -->
                    <th>Payment ID</th>
                    <th>Result</th>
                    <th>Action</th>
                    <!-- <th>Auth</th>
                    <th>Ref</th> -->
                    
                    <!-- <th>Ip Address</th> -->
                    
            </thead>
            
        </table>

    </div>
    <div class="modal fade" id="bookingDetailed" tabindex="-1" role="dialog" aria-labelledby="labelBooking" style="outline:0 !important; z-index:50000">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="labelBooking">Booking Details </h4>
          </div>
          <div class="modal-body" id="detailedBody"> </div>  <!-- Modal-body -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

</div>

@endsection

@section('jqueries')

<script>


$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('transaction/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('transaction/') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#transaction').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        select: false,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['reload'/*,'delete', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }*/],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("transaction") }}',
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [

            { data: 'id', name:'id', "searchable": false},
            { data: 'created_at', name: 'created_at'},
            { data: 'customer_name', name: 'customer_name' },
            { data: 'mobile', name: 'mobile' },
            { data: 'amount', name: 'amount' },
            { data: 'TranID', name:'TranID' },
            { data: 'TrackID', name: 'TrackID'},
            { data: 'PaymentID', name: 'PaymentID' },
            { data: 'result', name: 'result'},
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    
    var counter = table.rows('.selected').data().length;

    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="firstname">Name</option> <option value="mobile">Number</option><option value="amount">Amount</option><option value="TranID">Transaction ID</option><option value="TrackID">Track ID</option><option value="transactions.created_at">Date</option> </select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url("transaction")}}'+'/'+id,
            
        success:function(response){
            console.log(response);
            if(response.success)
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}

   $('#transaction').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });
   $('#transaction').on('click', '.detailedBookings', function(e){
        var getCustomerName = $(this).data('name');
        var getCustomeEmail = $(this).data('email');
        var getCustomMobile = $(this).data('mobile');
        var ontime = $(this).data('ontime');
        $('#labelBooking').text('Booking details of '+getCustomerName+' - '+getCustomeEmail+' - '+getCustomMobile);

        $.ajax({
                url: '{{ url('transactionbookings') }}',
                method: 'post',
                data:{ ontime:ontime },

                success:function(data) {
                    // console.log(data);

                    $('#detailedBody').html(data);
                    $('#bookingDetailed').modal({show:true });
                },
                complete: function(){
                    // $.loader.close();
                }
            });
    
   });

});


</script>
@endsection