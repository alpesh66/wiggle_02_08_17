@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
{{-- <div class="page-title">
  <div class="title_left">
    <h3>Dashboard</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div>
</div> --}}

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> Dashboard</h2>
      {{-- <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul> --}}
      <div class="clearfix"></div>
    </div>
    <div class="x_content" style="display: block;">


      <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
          <li role="presentation" class="active"><a href="#tab_providers" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Providers</a>
          </li>
          <li role="presentation" class=""><a href="#tab_societies" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Societies</a>
          </li>
         
        </ul>
        <div id="myTabContent" class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="tab_providers" aria-labelledby="home-tab">
           
           {{-- PROVIDER DASHBOARD START --}}

                <div class="row tile_count">
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-users"></i>@lang('text.totalCustomers')</span>
                    <a href="{{url('customers')}}"><div class="count">{{$customers}}</div></a>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-paw"></i>@lang('text.petStores')</span>
                    <a href="{{url('store')}}"><div class="count">{{$pet_stores}}</div></a>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-venus-mars"></i>@lang('text.totalPets')</span>
                    <div class="count green">{{$pets}}</div>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-scissors"></i>@lang('text.totalSP')</span>
                    <a href="{{url('provider')}}"><div class="count">{{$providers}}</div></a>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-file"></i>@lang('text.cmsPages')</span>
                    <a href="{{url('content')}}"><div class="count">{{$cmspages}}</div></a>
                    
                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-check-square-o"></i>@lang('text.totalBooks')</span>
                    <div class="count">{{$bookings}}</div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>@lang('text.newCustomers') </h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($customer_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="{{url('customers/'.$item->id)}}" class="title">{{ ucfirst($item->firstname).' '.$item->lastname }}</a>
                              <p>{{ $item->email }}</p>
                            </div>
                          </article>
                          @endforeach                    
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>@lang('text.newproviders')</h2>          
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($provider_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="{{url('provider/'.$item->id)}}" class="title">{{ ucfirst($item->name) }}</a>
                              <p>{{ $item->email }}</p>
                            </div>
                          </article>
                          @endforeach              
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>@lang('text.newPaws')</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($store_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="{{url('store/'.$item->id)}}" class="title">{{ ucfirst($item->name) }}</a>
                              <p>{{ $item->area }}</p>
                            </div>
                          </article>
                          @endforeach          
                        </div>
                      </div>
                    </div>
                  </div>  

           {{-- PROVIDER DASHBOARD ENDS --}}
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab_societies" aria-labelledby="profile-tab">
            {{-- SOCIETIES DASHBOARD START --}}

                 <div class="row tile_count">
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-file"></i> Total Societies</span>
                    <div class="count">{{$societies}}</div>
                    
                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-users"></i> Total Adopt</span>
                    <div class="count">{{$adopt}}</div>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-paw"></i> Total Foster</span>
                    <div class="count">{{$foster}}</div>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-venus-mars"></i> Total Volunteer</span>
                    <div class="count">{{$volunteer}}</div>

                  </div>
                  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-scissors"></i> Total Surrender</span>
                    <div class="count">{{$surrender}}</div>

                  </div>

               {{--    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-check-square-o"></i> Total Pets</span>
                    <div class="count">0</div>
                  </div> --}}
                </div>

                <div class="row">
                  @if($adopt_list)
                    <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>New Adopt </h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($adopt_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
                              <p>{{ $item->customer->email }}</p>
                            </div>
                          </article>
                          @endforeach                    
                        </div>
                      </div>
                    </div>
                    @endif
                   
                    @if($volunteer_list)
                     <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>New Volunteer </h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($volunteer_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
                              <p>{{ $item->customer->email }}</p>
                            </div>
                          </article>
                          @endforeach                    
                        </div>
                      </div>
                    </div>
                    @endif
                    @if($surrender_list)
                     <div class="col-md-4">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>New Surrender </h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          @foreach($surrender_list as $item)
                          <article class="media event">
                            <a class="pull-left date">
                              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
                            </a>
                            <div class="media-body">
                              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
                              <p>{{ $item->customer->email }}</p>
                            </div>
                          </article>
                          @endforeach                    
                        </div>
                      </div>
                    </div>
                    @endif
                    

                   
                  </div>  

            {{-- SOCIETIES DASHBOARD ENDS --}}
          </div>
          
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="clearfix"></div>

@endsection
