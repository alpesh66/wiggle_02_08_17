@extends('layouts.master')
@section('title', trans('text.providers'))
@section('content')
<div class="container">

    <h1> @lang('text.providers') <a href="{{ url('/provider/create') }}" class="btn btn-primary btn-xs" title="Add New Provider"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTables">
            <thead>
                <tr>
                    <th>S.No</th><th> Name </th><th> Email </th><th> Number </th><th>Admin Name </th><th>Admin Email </th><th>Admin Number </th><th>Type </th><th>Action </th>
                </tr>
            </thead>            
        </table>

    </div>

</div>
@endsection

@section('jqueries')

<script>
$(document).ready(function(){

    $.fn.dataTable.ext.buttons.delete = {
        text: 'Delete',
        className: 'buttons-danger disabled deleteButton',
        action: function ( e, dt, node, config ) {
            var id = (table.rows('.selected').data().pluck('id').toArray());
            if(id.length == 0)
            {
                 $('.deleteButton').addClass('disabled');
                 return false;
            }
            var checkonce = confirm('Are you sure you want to delete?');
            if(checkonce)
            {
                deleteData(id);
            }
        }
    };

    $.fn.dataTable.ext.buttons.reload = {
        text: 'Reload',
        className: 'buttons-alert',
        action: function ( e, dt, node, config ) {

            $('#post').val('');
            dt.ajax.reload();
            $('.deleteButton').addClass('disabled');
        }
    };

    var table = $('#dataTables').DataTable({

        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [[0, 'desc']],
        columnDefs: [ {
            orderable: true,
            className: 'select-checkbox',
            targets:0,
            visible: false
        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("provider") }}',
            data: function (d) {               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },
        },
        columns: [
            { data: 'id', name:'id', "searchable": false},
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'contact', name: 'contact' },
            { data: 'uname', name: 'users.name' },
            { data: 'uemail' },
            { data: 'umobile', name: 'users.mobile' },
            { data: 'ptype', name: 'ptype' },
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });


    table.on('select', function ( e, dt, type, indexes ) {

        var counter = table.rows('.selected').data().length;
        if(counter == 0) {
            $('.deleteButton').addClass('disabled');
        } else if(counter == 1) {
            $('.deleteButton').removeClass('disabled');
        } else if(counter > 1){
            $('.deleteButton').removeClass('disabled');
        }
    } );

   table.on( 'deselect', function ( e, dt, type, indexes ) {        
        var counter = table.rows('.selected').data().length;
        if(counter == 0)
        {
            $('.deleteButton').addClass('disabled');
        }
        else if(counter >= 1)
        {
            $('.deleteButton').removeClass('disabled');  
        } 
    } );

    $('.dt-buttons').addClass('pull-right');

    $('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="providers.name">Name</option> <option value="name_ar">Arabic Name</option> <option value="providers.status">Status</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');


    $('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

    function deleteData(id) {
        $.ajax({
            type: 'post',
            data: {id: id, _method: 'delete'},
            url: '{{ url("provider")}}'+'/'+id,
                
            success:function(response){

                if(response.success)
                {
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'success');
                }
                else
                {
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'danger');
                }
            
            },
        });
    }

    $('#dataTables').on('click', '.redirectAdmin', function(e){
        var checkonce = confirm('Are you sure you want to Login?');
        var user_id = $(this).data('id');
        var ptype = $(this).data('ptype');
        var providerid = $(this).data('providerid');
        var providerName = $(this).data('name');
        // console.log(providerid);
        // console.log(user_id);
        // console.log(ptype);

        if(checkonce)
        {
            $.ajax({
                type: 'post',
                data: { user_id: user_id, ptype: ptype, providerid: providerid, provider_name: providerName },
                url: '{{ url('subadminDashboard') }}',
                success: function(resp){
                    // console.log(resp);
                    if(resp.success)
                    {
                        // window.location.href = resp.url;
                        window.location.replace(resp.url);
                    } else{
                        window.location.reload();
                    }
                }
            });
        }
   });

   $('#dataTables').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = $(this).data('id');
            // console.log(id);
            deleteData(id);
        }
   });

});
</script>
@endsection
