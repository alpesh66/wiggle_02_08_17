@extends('layouts.master')
@section('title', 'Edit Provider')
@section('content')
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Edit Provider</h1>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

    {!! Form::model($provider, [
        'method' => 'PATCH',
        'url' => ['/provider', $provider->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

           <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('type', 'Service', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('type[]', $type, null, ['class' => 'form-control', 'id' => 'tags_type', 'multiple' => 'multiple']) !!}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('photo', 'Logo', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}

                    
                    @if(!empty($provider->photo))
                        <img src="{{$provider->photo}}" width="20%" title="{{ $provider->photo }}" />
                    @endif
                </div>
            </div>

            <div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
                {!! Form::label('icon', 'Icon', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('icon', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}

                    
                    @if(!empty($provider->icon))
                        <img src="{{$provider->icon}}" width="20%" title="{{ $provider->icon }}" />
                    @endif
                </div>
            </div>


            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', 'About', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about_ar') ? 'has-error' : ''}}">
                {!! Form::label('about_ar', 'About Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
                {!! Form::label('area', 'Area', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('area', $area, $provider->area, ['class' => 'form-control']) !!}
                    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('start') ? 'has-error' : ''}}">
                {!! Form::label('start', 'Subcription Start', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('start', null, ['class' => 'form-control startdate']) !!}
                    {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('end') ? 'has-error' : ''}}">
                {!! Form::label('end', 'Subcription End', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('end', null, ['class' => 'form-control enddate']) !!}
                    {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
             <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                    <div class="col-sm-6">                            
                        <img width="32" height="32" onclick="valideopenerform();" style="cursor:pointer;" src="{{url('/images/map_icon.png')}}">
                    </div>
            </div>
            {{-- {!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' => 'latitude']) !!}
            {!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' => 'longitude']) !!} --}}
            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', 'Contact', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('contact', $country_code[1], ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" @if($provider->status) checked="checked" @endif data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary ']) !!}
            <a class='btn btn-danger' href="{{ url('provider') }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
</div>
</div>
</div>
@endsection

@section('jqueries')
    <script type="text/javascript">
    function valideopenerform(lat, long) {

            var lat = $('#latitude').val();
            var long = $('#longitude').val();

            if (!lat) {
                lat = '29.31166';
            }
            if (!long) {
                long = '47.48176';
            }

            var popy = window.open('{{url('map')}}?lat=' + lat + '&long=' + long, 'popup_form', 'location=no, menubar=no, status=no, top=50%, left=50%, height=550, width=750');

        }
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();

        $('#tags_type').select2({
          placeholder: "Select Type",
          allowClear: true
        }).val([{{ $selected_types }}]).trigger("change");
        $('.sele_type').select2({
          placeholder: "Select Country",
        }).val([{{ $country_code[0] }}]).trigger('change');

        var startDate = new Date();
        var FromEndDate = new Date();
        var ToEndDate = new Date();

        // ToEndDate.setDate(ToEndDate.getDate()+365);
        // $('.startdate').datepicker({
        //     weekStart: 1,
        //     today:true,
        //     autoclose: true
        // }).on('changeDate', function(selected){
        //     startDate = new Date(selected.date.valueOf());
        //     var tempStartDate = new Date(startDate);
        //     var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate()+365);
        //     $('.enddate').datepicker('setDate', default_end);
        // });
        // $('.enddate').datepicker();

        $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
           // minDate: 'now' 
        });

        $('.enddate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
           // minDate: 'now'
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
        });


    });
    </script>
@endsection