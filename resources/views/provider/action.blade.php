<a href="{{ route('provider.show', $provider->id) }}" title="@lang('text.view')">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('provider/'.$provider->id.'/edit') }}" title="@lang('text.edit')"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$provider->id}}" data-model="provider" title="Delete" name="deleteButton"><i class="fa fa-trash-o"></i></button>

<button class="btn btn-success btn-xs redirectAdmin" data-name="{{$provider->name}}" data-id='{{$provider->uid}}' data-providerid='{{$provider->id}}' data-ptype='{{$provider->ptype}}'><i class="fa fa-dashboard"></i></button>