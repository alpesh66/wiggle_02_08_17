@extends('layouts.master')
@section('title', trans('text.providers'))
@section('content')
<div class="container">

    <h1>@lang('text.providers') 
        <a href="{{ url('provider/' . $provider->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Provider"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {{-- <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a> --}}
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['provider', $provider->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Provider',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

                @if(!empty($provider->photo))
                   <tr><th>Logo</th><td><img src="{{$provider->photo}}" width="25%"></td></tr>
                @endif
                @if(!empty($provider->icon))
                   <tr><th>Icon</th><td><img src="{{$provider->icon}}" width="25%"></td></tr>
                @endif

                <tr><th> Name </th><td> {{ $provider->name }} </td></tr><tr><th> About </th><td> {{ $provider->about }} </td></tr>
                <tr><th> Name Ar </th><td> {{ $provider->name_ar }} </td></tr><tr><th> About Ar </th><td> {{ $provider->about_ar }} </td></tr>
                <tr><th> Address </th><td> {{ $provider->address }} </td></tr>
                <tr><th> contact </th> <td> {{ $provider->contact }}
                <tr><th> Email </th> <td> {{ $provider->email }}
                <tr><th> Latitude </th> <td> {{ $provider->latitude }}
                <tr><th> Longitude </th> <td> {{ $provider->longitude }}
                <tr><th> Status </th> <td>
                    @if($provider->status) 
                        <label class="label btn-primary"> Active</label> 
                    @else
                        <label class="label btn-danger">Inactive</label>
                    @endif
                </td>
            </tbody>
        </table>
    </div>

</div>
@endsection
