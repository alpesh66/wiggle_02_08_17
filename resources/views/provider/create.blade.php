@extends('layouts.master')
@section('title', 'Add Provider')
@section('content')


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Create New Provider</h2>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
            
    {!! Form::open(['url' => '/provider', 'class' => 'form-horizontal','files' => true]) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}"> 
                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control col-md-7 col-xs-12" name="name" id="name" value="{{ old('name') }}">
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {{-- {!! Form::label('email', 'Support Email', ['class' => 'col-sm-3  col-md-3 control-label']) !!} --}}
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Support Email <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::email('email', null, ['class' => 'col-md-7 col-xs-12 form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('ptype') ? 'has-error' : ''}}">
                {!! Form::label('ptype', 'Type', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="radio" class="ptypes" name="ptype" value="1" checked="checked"> Service Providers
                    <input type="radio" class="ptypes" name="ptype" value="2"> Societies
                    {!! $errors->first('ptype', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('type', 'Services', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                <div id="provid">
                    {!! Form::select('type_1[]', $providers, null, ['class' => 'form-control tags_type', 'multiple' => 'multiple']) !!}
                </div>
                <div id="societ">
                    {!! Form::select('type_2[]', $societies, null, ['class' => 'form-control tags_type', 'multiple' => 'multiple']) !!}
                </div>
                    
                </div>
            </div>

            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Logo', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}                    
                </div>
            </div>
             <div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
                {!! Form::label('icon', 'Icon', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('icon', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}                    
                </div>
            </div>

            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', 'About', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about_ar') ? 'has-error' : ''}}">
                {!! Form::label('about_ar', 'About Ar', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('about_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address *', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
                {!! Form::label('area', 'Area', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('area',$area, null, ['class' => 'form-control get_areas']) !!}
                    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('start') ? 'has-error' : ''}}">
                {!! Form::label('start', 'Subcription Start', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('start', null, ['class' => 'form-control startdate']) !!}
                    {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('end') ? 'has-error' : ''}}">
                {!! Form::label('end', 'Subcription End', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('end', null, ['class' => 'form-control enddate']) !!}
                    {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                    <div class="col-sm-6">                            
                        <img width="32" height="32" onclick="valideopenerform();" style="cursor:pointer;" src="{{url('/images/map_icon.png')}}">
                    </div>
            </div>
            {{-- {!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' => 'latitude']) !!}
            {!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' => 'longitude']) !!} --}}
            <div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', 'Contact', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}

                    {{-- <select name="conty">

                        @foreach($country as $key => $valu)
                            <option value="{{$key}}"> {{$valu}} ({{$key}})</option>
                        @endforeach
                    </select> --}}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <hr>
            <h2>Provider Admin Detail</h2>
            <div class="form-group {{ $errors->has('users.name') ? 'has-error' : ''}}">
                <label for="username" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="users[name]" id="username" value="{{ old('users.name') }}">
                  {!! $errors->first('users.name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('users.email') ? 'has-error' : ''}}">
                <label for="useremail" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="users[email]" id="useremail" value="{{ old('users.email') }}">
                  {!! $errors->first('users.email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('users.password') ? 'has-error' : ''}}">
                <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" class="form-control col-md-7 col-xs-12" required="required" name="users[password]" id="password" value="{{ old('users.password') }}">
                  {!! $errors->first('users.password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url('provider') }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

      </div>
    </div>
  </div>
</div>

@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
    function typeval_alreadyselected() {
        var got_val = $('input[name=ptype]:checked').val();
        if(got_val == 2){
            $('#societ').show();
            $('#provid').hide();

        } else{
            $('#provid').show();
            $('#societ').hide();
        }
    }
    
    function valideopenerform(lat, long) {

        var lat = $('#latitude').val();
        var long = $('#longitude').val();

        if (!lat) {
            lat = '29.31166';
        }
        if (!long) {
            long = '47.48176';
        }

        var popy = window.open('{{url('map')}}?lat=' + lat + '&long=' + long, 'popup_form', 'location=no, menubar=no, status=no, top=50%, left=50%, height=550, width=750');
    }


    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();;

        $('.tags_type').select2({
          placeholder: "Select Type",
          allowClear: true
        });
        $('.get_areas').select2({
          placeholder: "Select Area",
          allowClear: true
        });

        $('.sele_type').select2({
          placeholder: "Select Country",
        }).val(['965']).trigger('change');

        $('#societ').hide();
        $('.ptypes').on('change', function(){
            var selected_val = $(this).val();
            $('.sele_type').select2().val(null).change();
            if(selected_val == 2){
                $('#societ').show();
                $('#provid').hide();

            } else{
                $('#provid').show();
                $('#societ').hide();
            }

        });


        var startDate = new Date();

        $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now'
        });

        $('.enddate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now'
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
             update_endTime(e.date);
        });

function update_endTime(date) {
                 var tempStartDate = new Date(date);
                var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate()+365);
                var date = (default_end.getMonth() + 1) + '/' + default_end.getDate() + '/' +  default_end.getFullYear();
                $('.enddate').val(date);
             }
update_endTime(startDate);
typeval_alreadyselected();

    });
    </script>
@endsection