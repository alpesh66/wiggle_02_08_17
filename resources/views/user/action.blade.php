<a href="{{ route('user.show', $user->id) }}" title="View">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('user/'.$user->id.'/edit') }}" title="Edit"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$user->id}}" data-model="users" title="@lang('text.delete')" name="deleteButton"><i class="fa fa-trash-o"></i></button>