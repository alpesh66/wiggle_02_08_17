@extends('layouts.master')
@section('title', trans('text.addUser'))
@section('content')

<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>@lang('text.addUser') <small></small></h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

        <form action='{{ url('user') }}' class='form-horizontal form-label-left' method="post"  id='useradd' >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}" required="required" id="name" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="email" id="email" value="{{ old('email') }}">
                </div>
            </div>
             <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                {!! Form::label('mobile', 'Mobile No', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group">
                <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" class="form-control col-md-7 col-xs-12" autocomplete="off" required="required" name="password" id="password">
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="control-label col-md-3 col-sm-3 col-xs-12">Role<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('roles', $roles, null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                    <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
                </div>
            </div>
        </form>

          </div>
        </div>
      </div>

</div>
@endsection

@section('jqueries')
<script type="text/javascript">
    $(document).ready(function(){

         $('.sele_type').select2({
          placeholder: "Select Country",
         }).val(['965']).trigger('change');

    });
</script>
@endsection