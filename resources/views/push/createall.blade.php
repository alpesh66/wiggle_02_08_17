@extends('layouts.master')
@section('title', 'Add Specialists')
@section('content')
<div class="container">
    <h1>Push Notification For All</h1>
    @if (Session::has('message'))
       <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <hr/>
    {!! Form::open(['url' => '/sendpushForAll', 'class' => 'form-horizontal techniciansForm', 'files' => true,'onsubmit' => 'return ConfirmDelete()']) !!}
    {{ csrf_field() }}
        <!-- <div class="form-group {{ $errors->has('message_ar') ? 'has-error' : ''}}">
            {!! Form::label('android', 'Device Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::label('android', 'Android', ['class' => '']) !!}
                <input type="checkbox" name="android" class="bootswitch1" value="1" id="forDog"  data-on-text="Yes" data-off-text="No">                
                {!! Form::label('ios', 'iOS', ['class' => '']) !!}
                <input type="checkbox" name="ios" class="bootswitch1" value="2" id="forDog"  data-on-text="Yes" data-off-text="No">
            
            </div>
        </div>     -->    
        <div class="form-group {{ $errors->has('customers') ? 'has-error' : ''}}">
            {!! Form::label('customers', 'Select Customers', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('customers[]',$customers, null, ['class' => 'form-control','multiple'=>true,'id'=>'customers']) !!}
                {!! $errors->first('customers', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('appointment', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('type[]',$type, null, ['class' => 'form-control','multiple'=>true,'id'=>'type']) !!}
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
            <!-- <div class="col-sm-2">
                {!! Form::label('cat', 'Cat', ['class' => '']) !!}
                 <input type="radio" name="type" value="1">
            </div>
            <div class="col-sm-2">
                {!! Form::label('dog', 'Dog', ['class' => '']) !!}
                 <input type="radio" name="type" value="2">
            </div>
            <div class="col-sm-2">
                {!! Form::label('all', 'All', ['class' => '']) !!}
                 <input type="radio" name="type" value="3" checked="true">
            </div> -->
        </div>
        <div class="form-group {{ $errors->has('message_en') ? 'has-error' : ''}}">
            {!! Form::label('message_en', 'Message', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('message_en', null, ['class' => 'form-control','required' => 'required','onkeyup'=>'countChar(this)']) !!}                
            </div>
            {!! $errors->first('message_en', '<p class="help-block">:message</p>') !!}
            <div id="charNum" style="font-size: 25px;color: #01aef2;"></div>
        </div>

        <div class="form-group">
            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Reset', ['class' => 'btn btn-danger reset']) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('jqueries')
<script type="text/javascript">
$('.bootswitch1').bootstrapSwitch();
$('#customers').select2();
$('#type').select2();
$('.reset').on('click', function () {
       $("#customers").select2("val", "");    
       $("#type").select2("val", "");    
});

$('.startdate').datetimepicker({
    sideBySide:false,
    format: 'MM/DD/YYYY',
});

$('.enddate').datetimepicker({
    sideBySide:false,
    format: 'MM/DD/YYYY',

});
function countChar(val) {
    var len = val.value.length;
    if (len >= 140) {
        val.value = val.value.substring(0, 140);
    } else {
        $('#charNum').text(140 - len);
    }
};
function ConfirmDelete()
{
  var x = confirm("Are you sure you want to send?");
  if (x)
    return true;
  else
    return false;
}
</script>
@endsection