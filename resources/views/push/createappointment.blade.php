@extends('layouts.master')
@section('title', 'Add Specialists')
@section('content')
<div class="container">
    <h1>Push Notification For Appointment</h1>
    @if (Session::has('message'))
       <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <hr/>
    {!! Form::open(['url' => 'sendpushForAppointment', 'class' => 'form-horizontal techniciansForm', 'files' => true,'onsubmit' => 'return ConfirmDelete()']) !!}
    {{ csrf_field() }}
     <!--    <div class="form-group {{ $errors->has('message_ar') ? 'has-error' : ''}}">
            {!! Form::label('android', 'Device Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::label('android', 'Android', ['class' => '']) !!}
                <input type="checkbox" name="android" class="bootswitch1" value="2" id="forDog"  data-on-text="Yes" data-off-text="No">                
                {!! Form::label('ios', 'iOS', ['class' => '']) !!}
                <input type="checkbox" name="ios" class="bootswitch1" value="2" id="forDog"  data-on-text="Yes" data-off-text="No">
            
            </div>
        </div>     -->    
        <div class="form-group {{ $errors->has('providers') ? 'has-error' : ''}}">
            {!! Form::label('providers', 'Select Providers', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('providers[]',$providers, null, ['class' => 'form-control','multiple'=>true,'id'=>'providers']) !!}
                {!! $errors->first('providers', '<p class="help-block">:message</p>') !!}
            </div>
        </div>        
        {{--<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                                {!! Form::label('appointment', 'Type', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('type[]',$type, null, ['class' => 'form-control','multiple'=>true,'id'=>'type']) !!}
                                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>--}}
        <div class="form-group {{ $errors->has('start') ? 'has-error' : ''}}">
            {!! Form::label('start', 'Select Duration', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
            <div class="col-sm-6">
                <div class="col-sm-6">
                    {!! Form::label('start', 'From', ['class' => '']) !!}
                    <div class='input-group date startdate' >
                        <input type='text' name="start" id="start" class="form-control startdate" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('end', 'To', ['class' => '']) !!} 
                    <div class='input-group date enddate'>
                        <input type='text' name="end" id="end" class="form-control enddate" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>                   
                     {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        {{--<div class="form-group {{ $errors->has('appointment') ? 'has-error' : ''}}">
                            {!! Form::label('appointment', 'Appointment', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                <input type="checkbox" name="appointment" class="bootswitch1" value="1" id=""  data-on-text="Booked" data-off-text="All">
                            </div>
                        </div>--}}
        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                                        {!! Form::label('appointment', 'Appointment', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::select('appointment[]',$appointment, null, ['class' => 'form-control','multiple'=>true,'id'=>'appointment']) !!}
                                        {!! $errors->first('appointment', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
        <!-- <div class="form-group {{ $errors->has('appointment') ? 'has-error' : ''}}">
            {!! Form::label('appointment', 'Appointment', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="checkbox" name="appointment" class="bootswitch1" value="1" id=""  data-on-text="Booked" data-off-text="Not Booked">
            </div>
        </div> -->

        <div class="form-group {{ $errors->has('message_en') ? 'has-error' : ''}}">
            {!! Form::label('message_en', 'Message', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('message_en', null, ['class' => 'form-control','required' => 'required','onkeyup'=>'countChar(this)']) !!}
                {!! $errors->first('message_en', '<p class="help-block">:message</p>') !!}
            </div>
            <div id="charNum" style="font-size: 25px;color: #01aef2;"></div>
        </div>
        <div class="form-group">
            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-md-6">
                {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                {!! Form::reset('Reset', ['class' => 'btn btn-danger reset']) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('jqueries')
<style type="text/css">
    .bootstrap-switch{
    width: 220px !important;
}
.bootstrap-switch-handle-on{width: 109px !important;}
.bootstrap-switch-handle-off{width: 109px !important;}
</style>
<script type="text/javascript">
$('.bootswitch1').bootstrapSwitch();
$('#appointment').select2();
$('#providers').select2();
$('.reset').on('click', function () {
       $("#providers").select2("val", "");    
       $("#type").select2("val", "");    
});
$('.startdate').datetimepicker({
    sideBySide:false,
    format: 'MM/DD/YYYY',
});

$('.enddate').datetimepicker({
    sideBySide:false,
    format: 'MM/DD/YYYY',

});
function countChar(val) {
    var len = val.value.length;
    if (len >= 140) {
        val.value = val.value.substring(0, 140);
    } else {
        $('#charNum').text(140 - len);
    }
};
function ConfirmDelete()
{
  var x = confirm("Are you sure you want to send?");
  if (x)
    return true;
  else
    return false;
}
</script>
@endsection