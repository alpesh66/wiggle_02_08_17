<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='shortcut icon' type='image/x-icon' href="http://wiggleapp.co/favicon.ico" />
<link href="http://wiggleapp.co/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
<link href="http://wiggleapp.co/vendor/wiggle-my-font/wiggle-font.css" rel="stylesheet" type="text/css" >
<style>
  html, body{height:100%;margin:0;padding:0;}
  body{background: url(./images/paws.png) 50% 50% no-repeat;}
  body:before{content:'';display:block;position:absolute;bottom:20px;left:0;right:0;height:50%;background: transparent url(./images/cat-dog.png) 50% 100% no-repeat;background-size: contain;z-index:-1;}
  .content {margin-top: 5%;text-align: center;z-index:10;}
  .logo{width:100%;max-width:90px;margin:20px auto;}
  .wiggle-logo{display:block;width:100%;margin:0 auto;} 
  .login-form{display:block;width:100%;max-width:300px;margin:20px auto;padding:50px;background:#ffffff;border-radius:40px;border: 2px solid #231f20;-webkit-box-shadow: inset 30px -30px 0px 0px rgba(241,241,242,1);-moz-box-shadow: inset 30px -30px 0px 0px rgba(241,241,242,1);box-shadow: inset 30px -30px 0px 0px rgba(241,241,242,1);}
  .btn-success {background: #ec008c;color: #ffffff;padding: 10px 15px;border-radius: 5px;border:0;}
  .btn-success:hover,
  .btn-success:active,
  .btn-success:focus {background: #bb0872;}
</style>
</head>
<body>
  <div class="container">
    <div class="content">
      <div class="logo">
        <a href="http://wiggleapp.co" class="site_title"><img class="wiggle-logo" src="./images/Wiggle-Logo-main.png" alt="Wiggle"></a>
      </div>  
        
      @if (Auth::guest())
      <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
          <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="">
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control"  placeholder="Password" name="password">
        </div>
        <button class="btn btn-success" type="submit">Sign in</button>
      </form>
      @else
        @if(session('provider_type') == 'Admin')
          <a class="btn btn-success" href="{{ url('/dashboard')}}">Dashboard</a>
        @elseif(session('provider_type') == 'Provider')
          <a class="btn btn-success" href="{{ url('/providers/dashboard') }}">Go Dashboard</a>
        @elseif(session('provider_type') == 'Society')
          <a class="btn btn-success" href="{{ url('/societies/dashboard') }}">Go to Dashboard</a>
        @endif
      @endif
    </div>
  </div>
<script src="http://wiggleapp.co/vendor/jquery/dist/jquery.min.js"></script>
</body>
</html>
