@extends('layouts.master')
@section('title', trans('text.editPet'))
@section('content')
<div class="container">

    <h1>Edit Pet </h1>

    {!! Form::model($pet, [
        'method' => 'PATCH',
        'url' => ['/pet', $pet->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
                {!! Form::label('dob', 'Dob  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('dob', null, ['class' => 'form-control startCalendar']) !!}
                    {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('breed') ? 'has-error' : ''}}">
                {!! Form::label('breed', 'Specie  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('breed', $breed, null, ['class' => 'form-control breedStatus']) !!}
                    {!! $errors->first('breed', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender  *', ['class' => ' col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('gender', '1',false,['class'=>'genders']) !!} Male</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('gender', '0', true,['class'=>'genders']) !!} Female</label>
            </div>
                    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group neutereds {{ $errors->has('neutered') ? 'has-error' : ''}}" @if($pet->gender == 0) style="display:none;" @endif>
                {!! Form::label('neutered', 'Neutered  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('neutered', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('neutered', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('neutered', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group spayeds  {{ $errors->has('spayed') ? 'has-error' : ''}}" @if($pet->gender == 1) style="display:none;" @endif>
                {!! Form::label('spayed', 'Spayed  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('spayed', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('spayed', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('spayed', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
<!--             <div class="form-group {{ $errors->has('height') ? 'has-error' : ''}}">
                {!! Form::label('height', 'Height  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('height', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('height', '<p class="help-block">:message</p>') !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('picture', 'Picture  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    @if($pet->photo)
                        <img src="{{$pet->photo_thumbnail}}" title="{{$pet->name}}">
                    @endif

                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('temperament') ? 'has-error' : ''}}">
                {!! Form::label('temperament', 'Temperament  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('temperament', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('temperament', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('origin') ? 'has-error' : ''}}">
                {!! Form::label('origin', 'Breed  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('origin', null, ['class' => 'form-control originText']) !!}
                    {!! Form::select('origin',$specie, null, ['class' => 'form-control originSelect']) !!}
                    {!! $errors->first('origin', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('size') ? 'has-error' : ''}}">
                {!! Form::label('size', 'Size  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('size', $size, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('chipno') ? 'has-error' : ''}}">
                {!! Form::label('chipno', 'Microchip  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('chipno', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('chipno', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('weight') ? 'has-error' : ''}}">
                {!! Form::label('Weight  *', 'weight', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('weight', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('grooming') ? 'has-error' : ''}}">
                {!! Form::label('grooming', 'Grooming  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('grooming', $frequency, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('grooming', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
@endsection

@section('jqueries')

<script type="text/javascript">

function checkBreedSize() {
    if($('.breedStatus').val() == 1) {
    $('#size option:last').hide();
    $('#size option:nth-child(3)').hide();

    } else {
         $('#size option:last').show();
        $('#size option:nth-child(3)').show();
    }
}

checkBreedSize();
$('.breedStatus').change(function(){
    checkBreedSize();
});


$('.genders').click(function(){
    if($('.genders:checked').val() == 1) {
        $('.neutereds').css('display','block');
        $('.spayeds').css('display','none');
    } else {
        $('.spayeds').css('display','block');
        $('.neutereds').css('display','none');
    }
});
    $(document).ready(function(){
        $('.startCalendar').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
        });

        $('.originText').show();
        $('.originSelect').hide();


        $('.breedStatus').on('change', function(){
            var selValue = ($(this).val());

            if(selValue == 1){
                // Cat
                $('.originText').show();
                $('.originSelect').hide();

            } else {
                // Dog
                $('.originText').hide();
                $('.originSelect').show();
            }
        });

    });
    </script>
@endsection