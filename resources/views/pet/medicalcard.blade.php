@extends('layouts.master')
@section('title', 'Add Medical Card')
@section('content')
<div class="container">

    <h1>Add Medical Card</h1>
    <hr/>

    {!! Form::open(['url' => '/medicalcd/'.$pet->id, 'class' => 'form-horizontal', 'files' => true]) !!}

              
            <div class="form-group {{ $errors->has('givenon') ? 'has-error' : ''}}">
                {!! Form::label('givenon', 'Date  *', ['class' => 'col-sm-3 col-md-3  control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('givenon', null, ['class' => 'form-control givenon']) !!}
                    {!! $errors->first('givenon', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
                       
            <div class="form-group {{ $errors->has('vet') ? 'has-error' : ''}}">
                {!! Form::label('vet', 'Doctor  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('vet', null, ['class' => 'form-control']) !!}
                    
                    {!! $errors->first('vet', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
           <div class="form-group {{ $errors->has('vet') ? 'has-error' : ''}}">
               {!! Form::label('vaccination_type_id', 'Vaccination', ['class' => 'col-sm-3 control-label']) !!}
               <div class="col-sm-6">
                   {!! Form::select('vaccination_type_id', $vaccination,null, ['class' => 'form-control']) !!}
                   {!! $errors->first('vaccination_type_id', '<p class="help-block">:message</p>') !!}
               </div>
           </div>
            <div class="form-group {{ $errors->has('institution') ? 'has-error' : ''}}">
                {!! Form::label('institution', 'Institution  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('institution', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('picture', 'Photo of Card  *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
<script type="text/javascript">
    
    $(document).ready(function(){
         $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'YYYY/MM/DD',
        });
     });
</script>
@endsection