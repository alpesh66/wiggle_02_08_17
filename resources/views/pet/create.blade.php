@extends('layouts.master')

@section('content')
<div class="container">

    <h1>Create New Pet</h1>
    <hr/>

    {!! Form::open(['url' => '/pet', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
                {!! Form::label('dob', 'Dob', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('dob', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('breed') ? 'has-error' : ''}}">
                {!! Form::label('breed', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('breed', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('breed', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('gender', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('gender', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <!-- <div class="form-group {{ $errors->has('height') ? 'has-error' : ''}}">
                {!! Form::label('height', 'Height', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('height', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('height', '<p class="help-block">:message</p>') !!}
                </div>
            </div> -->
            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
                {!! Form::label('picture', 'Picture', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('temperament') ? 'has-error' : ''}}">
                {!! Form::label('temperament', 'Temperament', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('temperament', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('temperament', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('origin') ? 'has-error' : ''}}">
                {!! Form::label('origin', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('origin', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('origin', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('size') ? 'has-error' : ''}}">
                {!! Form::label('size', 'Size', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('size', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('chipno') ? 'has-error' : ''}}">
                {!! Form::label('chipno', 'Microchip', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('chipno', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('chipno', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('weight') ? 'has-error' : ''}}">
                {!! Form::label('weight', 'Weight', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('weight', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('grooming') ? 'has-error' : ''}}">
                {!! Form::label('grooming', 'Grooming', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('grooming', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('grooming', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection