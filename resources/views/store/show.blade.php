@extends('layouts.master')
@section('title', trans('text.showStore'))
@section('content')
<div class="container">

    <h1>@lang('text.showStore') 
        <a href="{{ url('store/' . $store->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Store"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {{-- <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a> --}}
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['store', $store->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Store',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr><th> Name </th>
                    <td> 
                        @if($store->type==1) 
                        Gromming
                        @elseif($store->type==2)
                        petStores
                        @elseif($store->type==3)
                        Socities
                        @elseif($store->type==4)
                        Dog friendly areas
                        @endif
                        
                    </td>
                </tr>
                <tr><th> Name </th><td> {{ $store->name }} </td></tr>
                <tr><th> Name Ar </th><td> {{ $store->name_ar }} </td></tr>
                <tr><th> Logo </th><td><img src="{{$store->image}}" width="25%"></td></tr>
                <tr><th> About </th><td> {{ $store->about }} </td></tr>
                <tr><th> About Ar </th><td> {{ $store->about_ar }} </td></tr>
                <tr><th> Address </th><td> {{ $store->block .' '.$store->floor.' '.$store->judda.' '.$store->building.' '.$store->street }} </td></tr>
                <tr><th> Area </th><td> {{ $store->area }} </td></tr>
                <tr><th> Latitude </th><td> {{ $store->latitude }} </td></tr>
                <tr><th> Longitude </th><td> {{ $store->longitude }} </td></tr>
                <tr><th> Contact Number </th><td> {{ $store->contact }} </td></tr>
                <tr><th> Timing </th><td> {{ $store->starttime.' to '.$store->endtime }} </td></tr>
                <tr><th> Email </th><td> {{ $store->email }} </td></tr>                
                <tr><th> Website </th><td> {{ $store->website }} </td></tr>
                <tr><th> Facebook </th><td> {{ $store->facebook }} </td></tr>
                <tr><th> Twitter </th><td> {{ $store->twitter }} </td></tr>
                <tr><th> Instagram </th><td> {{ $store->instagram }} </td></tr>
                <tr><th> Status </th> <td>
                    @if($store->status) 
                        <label class="label btn-primary"> Active</label> 
                    @else
                        <label class="label btn-danger">Inactive</label>
                    @endif
                </td>                
            </tbody>
        </table>
    </div>

</div>
@endsection
