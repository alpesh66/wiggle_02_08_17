@extends('layouts.master')
@section('title', trans('text.createStore'))
@section('content')
<div class="container">

    <h1>@lang('text.createStore')</h1>
    <hr/>

    {!! Form::open(['url' => '/store', 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('type', 'Store Type', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('type',$type, null, ['class' => 'form-control get_areas']) !!}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', 'Contact *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('block') ? 'has-error' : ''}}">
                {!! Form::label('block', 'Block', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('block', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('block', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('floor') ? 'has-error' : ''}}">
                {!! Form::label('floor', 'Floor', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('floor', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('floor', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('judda') ? 'has-error' : ''}}">
                {!! Form::label('judda', 'Judda', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('judda', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('judda', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('building') ? 'has-error' : ''}}">
                {!! Form::label('building', 'Building', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('building', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('building', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
                {!! Form::label('street', 'Street', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('street', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
                {!! Form::label('area', 'Area', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('area',$area, null, ['class' => 'form-control get_areas']) !!}
                    {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('starttime') ? 'has-error' : ''}}">
                {!! Form::label('starttime', 'Start Time', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('starttime', null, ['class' => 'form-control time']) !!}
                    {!! $errors->first('starttime', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('endtime') ? 'has-error' : ''}}">
                {!! Form::label('endtime', 'End Time', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('endtime', null, ['class' => 'form-control time']) !!}
                    {!! $errors->first('endtime', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about') ? 'has-error' : ''}}">
                {!! Form::label('about', 'About', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('about', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('about_ar') ? 'has-error' : ''}}">
                {!! Form::label('about_ar', 'About Ar', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('about_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('about_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
                {!! Form::label('website', 'Website', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('website', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
                {!! Form::label('facebook', 'Facebook', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
                {!! Form::label('twitter', 'Twitter', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
                {!! Form::label('instagram', 'Instagram', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}                    
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                    <div class="col-sm-6">                            
                        <img width="32" height="32" onclick="valideopenerform();" style="cursor:pointer;" src="{{url('/images/map_icon.png')}}">
                    </div>
            </div>
            {{-- {!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' => 'latitude']) !!}
            {!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' => 'longitude']) !!} --}}

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{-- <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('type', 'Type', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('type', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div> --}}


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
@endsection

@section('jqueries')
    <script type="text/javascript">
    function valideopenerform(lat, long) {

        var lat = $('#latitude').val();
        var long = $('#longitude').val();

        if (!lat) {
            lat = '29.31166';
        }
        if (!long) {
            long = '47.48176';
        }

        var popy = window.open('{{url('map')}}?lat=' + lat + '&long=' + long, 'popup_form', 'location=no, menubar=no, status=no, top=50%, left=50%, height=550, width=750');
    }
    

    $(document).ready(function(){
        
        $('#starttime').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm',
            switchOnClick: true,
        }).on('change', function(e, date)
        {
            $('#endtime').bootstrapMaterialDatePicker('setMinDate', $('#starttime').val());
        });
        $('#endtime').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm',
            switchOnClick: true,
          /*  minDate : $('#starttime').val()*/
        });
        

        $('.bootswitch').bootstrapSwitch();
        $('.get_areas').select2({
          placeholder: "Select Area",
          allowClear: true
        });

        $('.sele_type').select2({
          placeholder: "Select Country",
        }).val(['965']).trigger('change');

        /*$("#endtime").on("dp.change", function (e) {
            $('#starttime').data("DateTimePicker").maxDate(e.date);
        });*/
      

    });
    </script>
@endsection