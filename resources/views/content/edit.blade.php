@extends('layouts.master')
@section('title', trans('text.editCms'))
@section('content')
<div class="container">

    <h1>@lang('text.editCms') </h1>

    {!! Form::model($content, [
        'method' => 'PATCH',
        'url' => ['/content', $content->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('meta') ? 'has-error' : ''}}">
                {!! Form::label('meta', 'Meta', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('meta', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('meta', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('meta_desc') ? 'has-error' : ''}}">
                {!! Form::label('meta_desc', 'Meta Desc', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('meta_desc', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('meta_desc', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
                {!! Form::label('content', 'Content *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('content', null, ['class' => 'form-control textarea', 'id' => 'contenteditor']) !!}
                    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('content_ar') ? 'has-error' : ''}}">
                {!! Form::label('content_ar', 'Content Arabic *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('content_ar', null, ['class' => 'form-control textarea', 'id' => 'contenteditor_ar']) !!}
                    {!! $errors->first('content_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" @if($content->status) checked="checked" @endif data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
       <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


</div>
@endsection

@section('jqueries')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"> </script>

<script type="text/javascript">
    jQuery(document).ready(function () {
    CKEDITOR.replace('contenteditor', {
        //language:'fr',  
        //uiColor:'#AADC6E',
        filebrowserBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=files',
        filebrowserImageBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=images',
        filebrowserFlashBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=flash',
        filebrowserUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=files',
        filebrowserImageUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=images',
        filebrowserFlashUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=flash'
    });
    CKEDITOR.replace('contenteditor_ar', {
        //language:'fr',  
        //uiColor:'#AADC6E',
        filebrowserBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=files',
        filebrowserImageBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=images',
        filebrowserFlashBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=flash',
        filebrowserUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=files',
        filebrowserImageUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=images',
        filebrowserFlashUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=flash'
    });

        $('.bootswitch').bootstrapSwitch();
    });
</script>
@endsection