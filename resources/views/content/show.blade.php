@extends('layouts.master')
@section('title', trans('text.editCms'))
@section('content')
<div class="container">

    <h1>@lang('text.editCms') 
        <a href="{{ url('content/' . $content->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Content"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
       {{--  {!! Form::open([
            'method'=>'DELETE',
            'url' => ['content', $content->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Content',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!} --}}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                <tr><th> Name </th><td> {{ $content->name }} </td></tr><tr><th> Name Ar </th><td> {{ $content->name_ar }} </td></tr><tr><th> Meta </th><td> {{ $content->meta }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
