<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Confirm Email</title>
  <style type="text/css">
  body {margin:0px; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  .content {width:100%; max-width:100%;}
  .header {padding: 40px 30px 20px 30px;}
  .innerpadding {padding: 30px 30px 30px 30px;}
  .buttonpadding{padding:0px 30px 0px 30px;}
  .h1, .h2, .h3 .bodycopy {color: #000; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #000;}
  .footercopy a {color: #000; text-decoration: underline;}
  @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
  body[yahoo] .hide {display: none!important;}
  body[yahoo] .buttonwrapper {background-color: transparent!important;}
  body[yahoo] .button {padding: 0px!important;}
  body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
  body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }
  
  </style>
</head>
<body yahoo bgcolor="#f6f8f1">
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>
       
    <table bgcolor="#ccc" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class="header">
          <table width="115" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="100" style="padding: 0 20px 20px 0;">
                <img class="fix" src="logo.png" width="115" height="100" border="0" alt="ZOOKKS" />
              </td>
            </tr>
          </table>
          
        </td>
      </tr>
      <tr>
        <td class="innerpadding">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="h2">
                Hello Prathibha,
              </td>
            </tr>
            <tr>
              <td class="bodycopy">
                Welcome to zooKKs! In order to get started, please confirm your email address.
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="buttonpadding">
          
      
          <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 380px;">
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <table class="buttonwrapper" bgcolor="#fe696e" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="button" height="45">
                            <a href="#">Confirm Email</a>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        
        </td>
      </tr>

      <tr>
        <td class="innerpadding bodycopy">
          Thanks,<br />
          zooKK's Team
        </td>
      </tr>
      
      <tr>
        <td class="footer">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            
            <tr>
              <td align="center">
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                      <a href="http://www.facebook.com/">
                        <img src="facebook.png" width="100%" alt="Facebook" border="0" />
                      </a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                      <a href="http://www.twitter.com/">
                        <img src="twitter.png" width="100%" alt="Twitter" border="0" />
                      </a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            
            <tr>
              <td align="center" class="footercopy" style="padding:20px 0 0 0;">
                zooKKs Inc. | 222 W Merchandise Mart Plaza, Ste# 1212, Chicago, IL 60654 - USA.
              </td>
            </tr>
          </table>
        </td>
      </tr>

    </table>
  
    </td>
  </tr>
</table>
</body>
</html>