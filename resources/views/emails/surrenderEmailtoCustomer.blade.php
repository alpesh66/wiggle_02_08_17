

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        html,body{height: 100%;}
        .ExternalClass{display: block !important;}
    </style>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0"  bgcolor="#ffffff" style="margin: 0;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="height: 100%;">
    <tr>
        <td valign="top">
            <table cellpadding="0" cellspacing="0" border="0" width="737" align="center" style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="text-align: center;"><img src="{{ asset('images/img-header.png') }}" alt="" width="503" height="217" border="0" style="vertical-align: top;"/></td>
                </tr>
                <tr>
                    <td width="113" valign="top"><img src="{{ asset('images/img-dog.png') }}" alt="" width="113" height="542" border="0" style="vertical-align: top;"/></td>
                    <td valign="top" bgcolor="#ffffff" style="border: 1px solid #ee1e99; border-radius: 18px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 34px; line-height: 40px; color: #666666; font-weight: 500; letter-spacing: 7px; text-transform: uppercase; padding: 57px 10px 30px; text-align: center;">{{$msgCust}} </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="406" align="center" style="margin-left: auto; margin-right: auto;">
                                        <tr>
                                            <td bgcolor="#d6d6d6" height="3" style="font-size: 0; line-height: 0; border-radius: 9999px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 34px; line-height: 40px; color: #666666; font-weight: 300; text-align: center; padding: 0px;">For PET {!! $petname !!}</td>
                            </tr>
                            <!-- <tr>
                                <td valign="top" align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 48px; line-height: 58px; color: #666666; font-weight: 300; text-align: center; padding: 46px;">Start date : </td>
                            </tr>
                             <tr>
                                <td valign="top" align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 48px; line-height: 58px; color: #666666; font-weight: 300; text-align: center; padding: 46px;">End date : </td>
                            </tr> -->
                            
                            <tr>
                                <td valign="top" align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 34px; line-height: 40px; color: #666666; font-weight: 300; text-align: center; padding: 0px;">provider name : {!! $provider_name !!}</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 135px; padding-bottom: 45px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="87" style="font-size: 0; line-height: 0;"></td>
                                            <td width="54"><img src="{{ asset('images/ico-email.png') }}" alt="" width="40" height="27" border="0" style="vertical-align: top;"/></td>
                                            <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee; font-weight: 400;"><a href="mailto:info@wiggleapp.co" style="text-decoration: none; color: #00adee;" target="_blank">info@wiggleapp.co</a></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="15" style="font-size: 0; line-height: 0;"></td>
                                        </tr>
                                        <tr>
                                            <td width="87" style="font-size: 0; line-height: 0;"></td>
                                            <td width="54">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="9" style="font-size: 0; line-height: 0;"></td>
                                                        <td><img src="{{ asset('images/ico-web.png') }}" alt="" width="25" height="32" border="0" style="vertical-align: middle;"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee;"><a href="http://www.wiggleapp.co" style="text-decoration: none; color: #00adee;" target="_blank">www.wiggleapp.co</a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="113" valign="bottom"><img src="{{ asset('images/img-cat.png') }}" alt="" width="113" height="205" border="0" style="vertical-align: top;"/></td>
                </tr>
                <tr>
                    <td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
