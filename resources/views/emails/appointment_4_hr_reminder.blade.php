<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0121)file:///C:/Users/Lamya/Desktop/HTML%20Emails/WiggleApp%20Email%20-%20Reminder_files/WiggleApp%20Email%20-%20Reminder.html -->
<html xmlns:v="urn:schemas-microsoft-com:vml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>WiggleApp Email</title>
	<style type="text/css">
		html,body{height: 100%;}
		.ExternalClass{display: block !important;}
	</style>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0" bgcolor="#ffffff" style="margin: 0;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="height: 100%;">
	<tbody><tr>
		<td valign="top">
			<table cellpadding="0" cellspacing="0" border="0" width="737" align="center" style="margin-left: auto; margin-right: auto;">
				<tbody><tr>
					<td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
				</tr>
				<tr>
					<td colspan="3" align="center" style="text-align: center;"><img src="{{ asset('Kennel_Confirmation_files/img-header.png') }}" alt="" width="503" height="217" border="0" style="vertical-align: top;"></td>
				</tr>
				<tr>
					<td width="113" valign="top"><img src="{{ asset('Kennel_Confirmation_files/img-dog.png') }}" alt="" width="113" height="542" border="0" style="vertical-align: top;"></td>
					<td valign="top" bgcolor="#ffffff" style="border: 1px solid #ee1e99; border-radius: 18px;">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody><tr>
								<td align="center" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 54px; line-height: 64px; color: #666666; font-weight: 500; letter-spacing: 7px; padding: 46px 10px 47px; text-align: center;">Reminder</td>
							</tr>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" border="0" width="406" align="center" style="margin-left: auto; margin-right: auto;">
										<tbody><tr>
											<td bgcolor="#d6d6d6" height="3" style="font-size: 0; line-height: 0; border-radius: 9999px;"></td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 34px; line-height: 41px; color: #666666; font-weight: 300; padding: 9px 30px 24px 56px;">Dear <span style="color: #00adee;">{{$customer_name}}</span>, We would like to remind you of your appointment with {{$provider_name}} for {{$email_m}}</td>
							</tr>
							<tr>
								
							</tr>
						</tbody></table>
					</td>
					<td width="113" valign="bottom"><img src="{{asset('Kennel_Confirmation_files/img-cat.png')}}" alt="" width="113" height="205" border="0" style="vertical-align: top;"></td>
				</tr>
				<tr>
					<td width="113" style="font-size: 0; line-height: 0;"></td>
					<td valign="top">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody><tr>
								<td colspan="3" height="12" style="font-size: 0; line-height: 0;"></td>
							</tr>
							<tr>
								<td width="87" style="font-size: 0; line-height: 0;"></td>
								<td width="54"><img src="{{ asset('Kennel_Confirmation_files/ico-email.png') }}" alt="" width="40" height="27" border="0" style="vertical-align: top;"></td>
								<td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee; font-weight: 400;"><a href="mailto:info@wiggleapp.co" style="text-decoration: none; color: #00adee;" target="_blank">info@wiggleapp.co</a></td>
							</tr>
							<tr>
								<td colspan="3" height="15" style="font-size: 0; line-height: 0;"></td>
							</tr>
							<tr>
								<td width="87" style="font-size: 0; line-height: 0;"></td>
								<td width="54">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tbody><tr>
											<td width="9" style="font-size: 0; line-height: 0;"></td>
											<td><img src="{{ asset('Kennel_Confirmation_files/ico-web.png') }}" alt="" width="25" height="32" border="0" style="vertical-align: middle;"></td>
										</tr>
									</tbody></table>
								</td>
								<td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 31px; line-height: 41px; color: #00adee;"><a href="http://www.wiggleapp.co/" style="text-decoration: none; color: #00adee;" target="_blank">www.wiggleapp.co</a></td>
							</tr>
						</tbody></table>
					</td>
					<td width="113" style="font-size: 0; line-height: 0;"></td>
				</tr>
				<tr>
					<td colspan="3" height="30" style="font-size: 0; line-height: 0;"></td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

</body></html>