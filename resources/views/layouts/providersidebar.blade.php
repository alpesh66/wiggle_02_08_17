<li><a href="{{ url('providers/dashboard') }}" class="icon-m"><i class="ico"><img src={{asset('images/dash.png')}} alt="dashboard"></i><span class="ic-t">Dashboard</span></a></li>
<li><a href="{{ url('providers/calendar') }}" class="icon-m"><i class="ico"><img src={{asset('images/calendar.png')}} alt="calendar"></i><span class="ic-t">Calendar</span></a></li>
<li><a href="{{ url('providers/booking') }}" class="icon-m"><i class="ico"><img src={{asset('images/book.png')}} alt="booking"></i><span class="ic-t">Booking</span></a></li>
{{-- 
@if($user->haveYouSubscribed(1)  && $user->can('provider-vet'))
    <li><a><i class="icon-h"><img src={{asset('images/vet.png')}} alt="Veternary"></i> Veternary <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="{{ url('providers/veternary') }}">Veternary</a></li>
        </ul>
    </li>
@endif

@if($user->haveYouSubscribed(2)  && $user->can('provider-groomer') )    
    <li><a><i class="icon-h"><img src={{asset('images/groom.png')}} alt="Groomer"></i>Groomer<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="{{ url('providers/groomer') }}">Bookings</a></li>
        </ul>
    </li>
@endif

@if($user->haveYouSubscribed(19)  && $user->can('provider-walker') )    
    <li><a><i class="fa fa-street-view"></i>Walker<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="{{ url('providers/walker') }}">Bookings</a></li>
        </ul>
    </li>
@endif

@if($user->haveYouSubscribed(4)  && $user->can('provider-trainer') )    
    <li><a><i class="icon-h"><img src={{asset('images/trainer.png')}} alt="Trainer"></i>Trainer<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="{{ url('providers/trainer') }}">Bookings</a></li>
        </ul>
    </li>
@endif

 --}}
@if($user->can('provider-admin'))
    <li><a class="icon-m"><i class="ico"><img src={{asset('images/admin.png')}} alt="Admin"></i><span class="ic-t">Admin</span><span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('providers/user') }}">Users</a></li>
          <li><a href="{{ url('providers/role') }}">Role</a></li>
          <li><a href="{{ url('providers/logintech') }}">Specialist Login</a></li>

        </ul>
    </li>
@endif


  <li><a href="{{ url('providers/customers') }}" class="icon-m"><i class="ico"><img src={{asset('images/customer.png')}} alt="Clients"></i><span class="ic-t">Clients</span></a>
  </li>
  
@if($user->can('provider-service'))
    <li><a href="{{ url('providers/services') }}" class="icon-m"><i class="ico"><img src={{asset('images/services.png')}} alt="Services"></i><span class="ic-t">Services</span></a>    </li>
@endif

<li><a href="{{ url('providers/ratings') }}" class="icon-m"><i class="ico"><img src={{asset('images/review.png')}} alt="Ratings"></i><span class="ic-t">Ratings</span></a></li>

<li><a class="icon-m"><i class="ico"><img src={{asset('images/specialist.png')}} alt="Clients"></i><span class="ic-t">Specialists</span><span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="{{ url('providers/technicians') }}">Specialists</a></li>
      {{-- <li><a href="{{ url('providers/technicianbreak') }}">Technicians Break</a></li> --}}
      <li><a href="{{ url('providers/technicianholiday') }}">Attendance exceptions</a></li>
      <li><a href="{{ url('providers/technicianleave') }}">Leave</a></li>
      {{-- <li><a href="{{ url('providers/techniciantimings') }}">Technicians Timings</a></li> --}}
    </ul>
</li>

<li><a class="icon-m"><i class="ico"><img src={{asset('images/Profile.png')}} alt="Profile"></i><span class="ic-t">Profile</span><span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="{{ url('providers/profile') }}">Update Profile</a></li>
      <li><a href="{{ url('providers/providertimings') }}">Timings</a></li>
      {{-- <li><a href="{{ url('providers/gallery') }}">Gallery</a></li> --}}
    </ul>
</li>


@if($user->haveYouSubscribed(5)  && $user->can('provider-kennel') )    
<li><a class="icon-m"><i class="ico"><img width="45" src={{asset('images/kennel.png')}} alt="Kennel"></i><span class="ic-t">Kennel <span class="badge badge-primary total_unread_kennel">0</span></span><span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
	<li><a href="{{ url('providers/kennelservice') }}">Kennel Service</a></li>
          <li><a href="{{ url('providers/kennels') }}">Request <span class="badge badge-primary unread_kennel">0</span></a></li>
          <li><a href="{{ url('providers/missyou') }}">Miss you request <span class="badge badge-primary unread_missyou">0</span></a></li>
        </ul>
    </li>
@endif
<li><a href="{{ url('providers/provider_sales') }}" class="icon-m"><i class="ico"><img src={{asset('images/Sales-Report.png')}} alt="Sales Report"></i><span class="ic-t">Sales Report</span></a>
