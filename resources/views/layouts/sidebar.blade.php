<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    
    @if($user->can(super_admin_roles()))
        <h2>@lang('text.superadmin')</h2>
    @elseif($user->can(provider_admin_roles()))
        <h2>{{$user->pro_nameDisplay}}</h2>
    @elseif($user->can(society_admin_roles()))
        <h2>{{$user->pro_nameDisplay}}</h2>
     @elseif($user->can('calendar-only'))
        <h2>{{$user->pro_nameDisplay}}</h2>
    @endif

    <ul class="nav side-menu side-m-ul">
    {{-- Here we have all for ADMIN only, provider-admin is down --}}
      @if($user->can(super_admin_roles()))
      <li><a href="{{ url('/dashboard') }}" class="icon-m"><i class="ico"><img src={{asset('images/dash.png')}} alt="Dashboard"></i><span class="ic-t">Dashboard</span></a>
        {{-- <ul class="nav child_menu">
          <li><a href="index.html">Dashboard</a></li>
          <li><a href="index2.html">Dashboard2</a></li>
          <li><a href="index3.html">Dashboard3</a></li>
        </ul> --}}
      </li>
  @if($user->can('super-admin'))    
      <li><a class="icon-m"><i class="ico"><img src={{asset('images/admin.png')}} alt="Admin"></i><span class="ic-t">Admin</span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('/user') }}">Admin</a></li>
          <li><a href="{{ url('/role') }}">Roles</a></li>
          <li><a href="{{ url('/temperament') }}">Temperaments</a></li>
          {{-- <li><a href="{{ url('/dashboard') }}">Permission</a></li> --}}
         
        </ul>
      </li>
  @endif

  @if($user->can('super-customer'))
      <li><a href="{{ url('/customers') }}" class="icon-m"><i class="ico"><img src={{asset('images/customer.png')}} alt="Clients"></i><span class="ic-t">Clients</span></a>
      </li>
  @endif

  @if($user->can('super-category'))
      <li><a href="{{ url('/categories') }}" class="icon-m"><i class="ico"><img src={{asset('images/catego.png')}} alt="Service Categories"></i><span class="ic-t">Service Categories</span></a>
      </li>
  @endif

  @if($user->can('super-cms'))    
      <li><a href="{{ url('/content') }}" class="icon-m"><i class="ico"><img src={{asset('images/cms.png')}} alt="CMS"></i><span class="ic-t">CMS</span></a>
        
      </li>
  @endif

  @if($user->can('super-provider'))    
      <li><a href="{{ url('/provider') }}" class="icon-m"><i class="ico"><img src={{asset('images/provider.png')}} alt="CMS"></i><span class="ic-t">Providers</span></a>
      </li>
  @endif

  @if($user->can('super-store'))
      <li><a href="{{ url('/store') }}" class="icon-m"><i class="ico"><img src={{asset('images/Paw-Pages.png')}} width="50" alt="Paw Pages"></i><span class="ic-t">Paw Pages</span></a>

      </li>
  @endif


@if($user->can('super-store'))
      <li><a href="{{ url('ratings') }}" class="icon-m"><i class="ico"><img src={{asset('images/review.png')}} alt="Ratings"></i><span class="ic-t">Ratings</span></a></li>

      </li>
  @endif
  @if($user->can('super-event'))
      <li><a href="{{ url('/events') }}" class="icon-m"><i class="ico"><img src={{asset('images/Events.png')}} alt="Events"></i><span class="ic-t">Events</span></a> </li>
      <!-- <li><a href="{{ url('/sales') }}" class="icon-m"><i class="fa fa-bar-chart ico ico-only"></i><span class="ic-t">Sales Report</span></a></li> -->

      <li><a class="icon-m"><i class="ico"><img src={{asset('images/Sales-Report.png')}} alt="Sales Report"></i><span class="ic-t">Reports</span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('/sales') }}">Sales Report</a></li>
          <li><a href="{{ url('/Society-Report') }}">Society Report</a></li>
        </ul>
      </li>

      <li><a class="icon-m"><i class="ico"><img src={{asset('images/Push-Notifications.png')}} alt="Push Notifications"></i><span class="ic-t">Push Notification</span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('/push-all') }}">For All</a></li>
          <li><a href="{{ url('/push-appointment') }}">For Appointment</a></li>
        </ul>
      </li>

      <li>
        <a class="icon-h" href="{{ url('pet-deactive') }}">
            <i class="ico"><img width="50" src={{asset('images/Inactive-Pet.png')}} alt="Adopt"></i><span class="ic-t">Inactive Pets</span>
        </a>
      </li>
      <li><a href="{{ url('/transaction') }}" class="icon-m"><i class="fa fa-desktop ico ico-only"></i><span class="ic-t">Transaction</span></a></li>
  @endif

  <!-- @if($user->can('super-transaction'))
  
  @endif -->
  @if($user->can('super-species'))
  <li><a href="{{ url('/species') }}" class="icon-m"><i class="fa fa-desktop ico ico-only"></i><span class="ic-t">Breeds</span></a></li>
  @endif
  
      @endif

      {{-- For the Providers Sub-admin --}}
      @if($user->can(provider_admin_roles()) )
          @include('layouts.providersidebar')
      @endif
      
      {{-- For the Providers Sub-admin --}}
      @if($user->can(society_admin_roles()) )
          @include('layouts.societysidebar')
      </li>
      @endif

      {{-- Providers Only Tech LOgin --}}
      @if($user->can('calendar-only') )
          @include('layouts.onlycalendarsidebar')
        @endif

    </ul>
  </div>
</div>
