<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel='shortcut icon' type='image/x-icon' href="{{ asset('favicon.ico') }}" />
    <title>Wiggle | @yield('title')</title>

    {{-- <link rel="stylesheet" href="{{ elixir('css/all.css') }}"> --}}
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/custom/custom.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/wiggle-my-font/wiggle-font.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/fullcalendar/dist/fullcalendar-arshaw.css')}}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('vendor/bootstrap-switcher/dist/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/nprogress/nprogress.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/bootstrap-datepicker/datepicker_single.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/bootstrap-treeview/bootstrap-treeview.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" > 
    <link href="{{ asset('vendor/jquery-ui/jquery-ui.css')}}" rel="stylesheet" type="text/css" >

    {{-- <link href="{{ asset('vendor/custom/custom.min.css') }}" rel="stylesheet" type="text/css" > --}}
    

    <link href="{{ asset('vendor/datatables/DataTables/datatables.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/datatables/DataTables/Buttons-1.2.2/css/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/bootstrap-datepicker/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" type="text/css" >
    
    <link href="{{ asset('vendor/hover/hover-min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('vendor/animate/animate.css')}}" rel="stylesheet" type="text/css" >
    {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> --}}
    
    <style type="text/css">
      .icon-w {
    color: #fff;
    float: left;
    font-size: 57px;
    margin-right: 20px;
}
.navbar.nav_title.logo-icon-top {
    margin-top: 10px;
}
    </style>
    @yield('linkcss')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <div class="navbar nav_title" style="border: 0;">
        @if($user->can(super_admin_roles()) )
              <a href="{{ url('/dashboard') }}" class="site_title"><b class="icon-wn icon-logon"><img src="{{ asset('images/Wiggle-Logo-main.png') }}" alt="Wiggle"></b> <span>Wiggle</span></a>
        @endif
          
          @if($user->can(provider_admin_roles()) )
              <a href="{{ url('providers/dashboard') }}" class="site_title"><b class="icon-wn icon-logon"><img src="{{ asset('images/Wiggle-Logo-main.png') }}" alt="Wiggle"></b> <span>Wiggle</span></a>
          @endif

           @if($user->can(society_admin_roles()) )
              <a href="{{ url('societies/dashboard') }}" class="site_title"><b class="icon-wn icon-logon"><img src="{{ asset('images/Wiggle-Logo-main.png') }}" alt="Wiggle"></b> <span>Wiggle</span></a>
          @endif

          @if($user->can('calendar-only') )
            <a href="#" class="site_title"><b class="icon-wn icon-logon"><img src="{{ asset('images/Wiggle-Logo-main.png') }}" alt="Wiggle"></b> <span>Wiggle</span></a>
            @endif
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           {{--  <div class="profile">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div> --}}
            <!-- /menu profile quick info -->

            <br />
            <!-- sidebar menu -->
            @include('layouts.sidebar')
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            @include('layouts.footer')
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        @include('layouts.top')
        <!-- /top navigation -->

        <!-- page content -->
         <div class="right_col" role="main">
          <div class="">


    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

          @yield('content')
            {{-- <div class="page-title">
              <div class="title_left">
                <h3>Plain Page</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Plain Page</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      Add content to the page ...
                  </div>
                </div>
              </div>
            </div> --}}
          </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Developed by <a target="_blank" title="Developed by Simplified Informatics" href="http://simplifiedinformatics.com">Simplified Informatics</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    {{-- <script src="{{ elixir('js/all.js') }}"></script> --}}
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-switcher/dist/js/bootstrap-switch.min.js') }}"> </script>
    <script src="{{ asset('vendor/moment/min/moment.min.js') }}"> </script>
    <script src="{{ asset('vendor/fullcalendar/dist/fullcalendar-arshaw.js') }}"> </script>

    <script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"> </script>
    <script src="{{ asset('vendor/nprogress/nprogress.js') }}"> </script>
    <script src="{{ asset('vendor/Chart.js/dist/Chart.js') }}"> </script>
    <script src="{{ asset('vendor/Flot/jquery.flot.js') }}"> </script>
    <script src="{{ asset('vendor/Flot/jquery.flot.pie.js') }}"> </script>
    <script src="{{ asset('vendor/Flot/jquery.flot.time.js') }}"> </script>
    <script src="{{ asset('vendor/Flot/jquery.flot.stack.js') }}"> </script>
    <script src="{{ asset('vendor/Flot/jquery.flot.resize.js') }}"> </script>
    <script src="{{ asset('vendor/select2/dist/js/select2.min.js') }}"> </script>
    <script src="{{ asset('vendor/custom/custom.min.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap-treeview/bootstrap-treeview.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap-growl/jquery.bootstrap-growl.min.js') }}"> </script>
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"> </script>
    
    
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-material-datetimepicker.js') }}"> </script>
    <script src="{{ asset('vendor/datatables/DataTables/datatables.min.js') }}"> </script>
    {{-- <script src="{{ asset('vendor/datatables/DataTables/Buttons-1.2.2/js/dataTables.buttons.min.js') }}"> </script> --}}
    {{-- <script src="{{ asset('vendor/js-xlsx-master/dist/jszip.js') }}"> </script> --}}
    {{-- <script src="{{ asset('vendor/js-xlsx-master/dist/xlsx.min.js') }}"> </script> --}}

    {{-- <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script> --}}

    @yield('jqueries')
    <script type="text/javascript">
    function grown_noti(message,type) {
        if(typeof type == 'undefined'){
            type = 'success';  // if type is not defined then, info:danger:success
        }
        $.bootstrapGrowl(message, {
                type: type,
                width: 'auto',
                allow_dismiss: false,
                stackup_spacing: 10
            });
    }
      $(document).ready(function(){
          @if($user->can(society_admin_roles()) || $user->can(provider_admin_roles()))
            get_unread_request_count();
            window.setInterval(function(){
               get_unread_request_count();
            },10000);
          @endif
          
          // reset reply form
          $('.replyClick').click(function(){
            $('#request_reply_form').find('textarea').val('');
            $('#request_reply_form').find('select').val(0);
            $('.customer_id_replay').val($(this).attr('customer_id'));
            $('.title_reply').val($(this).attr('title'));
            $('.ids_reply').val($(this).attr('ids'));
          });

          $('.alert').delay(3500).slideUp();
      // function grown_noti(message,type) {
      //       if(typeof type == 'undefined'){
      //           type = 'success';  // if type is not defined then, info:danger:success
      //       }
      //       $.bootstrapGrowl(message, {
      //               type: type,
      //               width: 'auto',
      //               allow_dismiss: false,
      //               stackup_spacing: 10
      //           });
      //   }

        @if(session('flash_message'))
          grown_noti('{{ session('flash_message') }}', 'info');
        @endif

        $('#backToAdmin').on('click', function(){
              $.ajax({
                type: 'post',
                url: '{{ url('superAdmin') }}',
                success: function(resp){

                    if(resp.success)
                    {
                        // window.location.href = resp.url;
                        window.location.replace(resp.url);

                    } else{
                        window.location.reload();
                    }
                }
            });
        });

      });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.submit_change_password').click(function(){
    
    
     
     $.ajax({
        type: 'post',
        data: {'password':$('#password').val(),'confirm_password':$('#confirm_password').val()},
        url: '{{ url('user/changepassword')}}',  
         error: function(data){
          $('.valid_').html('');
          var errors = data.responseJSON;
            $.each(errors, function(index, value) {
              

              $('.'+index).html(value);
            });
        },         
        success:function(response){
            $('.valid_').html('');
            if(response.success)
            {
                $('#myModals').modal('hide');
                $('#password').val('');
                $('#confirm_password').val('');
                grown_noti(response.message,'success');
            }
            else
            {
                grown_noti(response.message,'danger');
            }
        },
    });
});

// use this function for get unread request count
function get_unread_request_count(){
    $.ajax({
                type: 'post',
                url: '{{ url('get_unread_request_count') }}',
                dataType:'json',
                success: function(resp){
                    if(resp.success){
                        $('.unread_adopt').text(resp.unread_adopt);
                        $('.unread_foster').text(resp.unread_foster);
                        $('.unread_kennel').text(resp.unread_kennel);
                        $('.unread_missyou').text(resp.unread_missyou);
                        $('.unread_surrender').text(resp.unread_surrender);
                        $('.unread_volunteer').text(resp.unread_volunteer);
                        $('.total_unread_kennel').text(parseInt(resp.unread_kennel+resp.unread_missyou));
                    }
                }
            });
}
    </script>
  </body>
</html>
