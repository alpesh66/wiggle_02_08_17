<style>
    .nav > li > a {
        display: block;
        line-height: 50px;
        padding: 13px 15px 12px;
        position: relative;
      }
      .nav-sm .container.body .col-md-3.left_col {
        width: 90px;
      }
      .menu_section h2 {
        padding-left: 16px;
        font-size: 15px;
      }
      .nav-sm .container.body .right_col {
        margin-left: 90px;
        padding: 10px 20px;
        z-index: 2;
      }
      .toggle a {
        cursor: pointer;
        margin: 0;
        padding: 15px 30px 0;
      }
      .nav-sm ul.nav.child_menu {
        background: rgb(255, 255, 255) none repeat scroll 0 0;
        position: absolute;
      }
      .nav.child_menu > li > a {
        color: rgb(128, 130, 133);
      }
      .nav.child_menu > li > a {
        color: rgba(128, 130, 133, 0.75);
        font-size: 12px;
        padding: 0 9px;
        line-height: 39px;
      }
</style>
<li><a href="{{ url('societies/dashboard') }}" class="icon-h"><i class="ico"><img src={{asset('images/dash.png')}} alt="Dashboard"></i><span>Dashboard</span></a>
{{-- && $user->can('manage_adopt' --}}

@if($user->can('society-admin'))
<li><a class="icon-h"><i class="ico"><img src={{asset('images/admin.png')}} alt="Admin"></i><span>Admin Users</span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('societies/user') }}">Users</a></li>
          <li><a href="{{ url('societies/role') }}">Role</a></li>
          {{-- <li><a href="{{ url('/dashboard') }}">Permission</a></li> --}}
         
        </ul>
      </li>
@endif

{{-- <li><a href="{{ url('societies/customers') }}"><i class="fa fa-desktop"></i> Clients</a> --}}

@if($user->haveYouSubscribed(6)  && $user->can('society-adopt') )
<li><a class="icon-h"><i class="ico"><img width="50" src={{asset('images/Active-Pet.png')}} alt="Pets"></i><span>Pets</span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a class="icon-h" href="{{ url('societies/pet') }}">Pets</a></li>
          <li><a class="icon-h" href="{{ url('societies/petlog') }}">Pet Log</a></li>
        </ul>
    </li>
    <!--li>
        <a class="icon-h" href="{{ url('societies/pet') }}"><i class="ico"><img width="50" src={{asset('images/adopt.png')}} alt="Adopt"></i><span>Pets </span></a>
    </li>
    <li>
        <a class="icon-h" href="{{ url('societies/petlog') }}"><i class="ico"><img width="50" src={{asset('images/adopt.png')}} alt="Adopt"></i><span>Pets Log </span></a>
    </li-->
    <li>
        <a class="icon-h" href="{{ url('societies/adopts') }}"><i class="ico"><img width="50" src={{asset('images/adopt.png')}} alt="Adopt"></i><span>Adopt <span class="badge badge-primary unread_adopt">0</span></span></a>
    </li>
@endif

@if($user->haveYouSubscribed(7) && $user->can('society-foster') )
    <li>
        <a class="icon-h" href="{{ url('societies/fosters') }}"><i class="ico"><img width="50" src={{asset('images/foster.png')}} alt="Foster"></i><span>Foster <span class="badge badge-primary unread_foster">0</span></span></a>
    </li>
@endif

@if($user->haveYouSubscribed(8) && $user->can('society-vol') )
    <li>
        <a class="icon-h" href="{{ url('societies/volunteers') }}"><i class="ico"><img width="50" src={{asset('images/volunteering.png')}} alt="Volunteer"></i><span>Volunteer <span class="badge badge-primary unread_volunteer">0</span></span></a>
    </li>
@endif

@if($user->haveYouSubscribed(9) && $user->can('society-surr') )
    <li>
        <a class="icon-h" href="{{ url('societies/surrenders') }}"><i class="ico"><img width="50" src={{asset('images/surrender.png')}} alt="Surrender"></i><span>Surrender <span class="badge badge-primary unread_surrender">0</span></span></a>
    </li>
@endif

<li><a href="{{ url('societies/ratings') }}" class="icon-m"><i class="ico"><img src={{asset('images/review.png')}} alt="Ratings"></i><span class="ic-t">Ratings</span></a></li>

<li><a href="{{ url('societies/approve') }}" class="icon-m"><i class="ico"><img src={{asset('images/customer.png')}} alt="Ratings"></i><span class="ic-t">Clients</span></a></li>


<li><a class="icon-m"><i class="ico"><img src={{asset('images/Profile.png')}} alt="Profile"></i><span class="ic-t">Profile</span><span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="{{ url('societies/profile') }}">Update Profile</a></li>
    </ul>
</li>


@if($user->haveYouSubscribed(11) && $user->can('society-kennel'))
    <li><a class="icon-h"><i class="ico"><img width="50" src={{asset('images/kennel.png')}} alt="Kennel"></i><span>Kennel <span class="badge badge-primary total_unread_kennel">0</span></span> <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{ url('societies/kennels') }}">Request <span class="badge badge-primary unread_kennel">0</span></a></li>
          <li><a href="{{ url('societies/missyou') }}">Miss you request <span class="badge badge-primary unread_missyou">0</span></a></li>
        </ul>
    </li>
@endif

     {{-- <li><a href="{{ url('societies/pet') }}"><i class="fa fa-user"></i>Pet</a>
    </li>
 --}}
 <li><a href="{{ url('societies/report') }}" class="icon-m"><i class="ico"><img src={{asset('images/Sales-Report.png')}} alt="Report"></i><span class="ic-t">Report</span></a>
