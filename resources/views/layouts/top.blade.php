<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle">
        
          <!--<i class="fa fa-bars"></i>-->
          <i class="fa nav-row">
                <span class="icon-bar color-one"></span>
                <span class="icon-bar color-two"></span>
                <span class="icon-bar color-three"></span>
          </i>
        </a>
      </div>



      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ $user->name }}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a  href="javascript:;" data-toggle="modal" data-target="#myModals"> Change Password</a></li>
            {{-- <li><a href="javascript:;"> Profile</a></li> --}}
            {{-- <li>
              <a href="javascript:;">
                <span class="badge bg-red pull-right">50%</span>
                <span>Settings</span>
              </a>
            </li>
            <li><a href="javascript:;">Help</a></li> --}}
            <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
    @if(session('superAdmin'))
        <li><button id="backToAdmin" class="btn btn-danger hvr-sweep-to-left hvr-shadow" title="@lang('text.backtoAdmin')">@lang('text.backtoAdmin')</button></li>
		@endif
        {{-- <li role="presentation" class="dropdown">
          <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>
            <span class="badge bg-green">6</span>
          </a>
          <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
            <li>
              <a>
                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                <span>
                  <span>John Smith</span>
                  <span class="time">3 mins ago</span>
                </span>
                <span class="message">
                  Film festivals used to be do-or-die moments for movie makers. They were where...
                </span>
              </a>
            </li>
            <li>
              <a>
                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                <span>
                  <span>John Smith</span>
                  <span class="time">3 mins ago</span>
                </span>
                <span class="message">
                  Film festivals used to be do-or-die moments for movie makers. They were where...
                </span>
              </a>
            </li>
            <li>
              <a>
                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                <span>
                  <span>John Smith</span>
                  <span class="time">3 mins ago</span>
                </span>
                <span class="message">
                  Film festivals used to be do-or-die moments for movie makers. They were where...
                </span>
              </a>
            </li>
            <li>
              <a>
                <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                <span>
                  <span>John Smith</span>
                  <span class="time">3 mins ago</span>
                </span>
                <span class="message">
                  Film festivals used to be do-or-die moments for movie makers. They were where...
                </span>
              </a>
            </li>
            <li>
              <div class="text-center">
                <a>
                  <strong>See All Alerts</strong>
                  <i class="fa fa-angle-right"></i>
                </a>
              </div>
            </li>
          </ul>
        </li> --}}
      </ul>
    </nav>
  </div>
  </div>

 <div id="myModals" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Password</h4>
      </div>
           
        {!! Form::open(['url' => 'users/changepassword', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                
            </div>
           
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                {!! Form::label('password', 'New Password', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                     <input type="password" class="form-control col-md-7 col-xs-12 {{ $errors->has('password') ? ' has-error' : '' }}" required="required" id="password" name="password" value="">
                    <p class="password valid_" style="color: rgb(169, 68, 66);"></p>
                </div>
            </div>
            <div class="form-group {{ $errors->has('confirm_password') ? 'has-error' : ''}}">
                {!! Form::label('confirm_password', 'Confirm Password', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <input type="password" class="form-control col-md-7 col-xs-12 {{ $errors->has('confirm_password') ? ' has-error' : '' }}" required="required" id="confirm_password" name="confirm_password" value="">
                    <p class="confirm_password valid_" style="color: rgb(169, 68, 66);"></p>
                </div>
            </div>
          
           
            


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="button" class="btn btn-primary submit_change_password">Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    {!! Form::close() !!}

          
        
      
        </div>
    </div>
</div>