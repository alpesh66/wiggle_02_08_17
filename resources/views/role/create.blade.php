@extends('layouts.master')
@section('title', trans('text.superCreateRole'))
@section('content')
<div class="container">

    <h1>@lang('text.superCreateRole')</h1>
    <hr/>

    {!! Form::open(['url' => '/role', 'class' => 'form-horizontal']) !!}

            <div class="form-group {{ $errors->has('display_name') ? 'has-error' : ''}}">
                {!! Form::label('display_name', 'Name *', ['class' => 'col-sm-3 control-label' ,'required' => 'required']) !!}
                <div class="col-sm-6">
                    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('permission') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Permissions *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('permission[]', $perm, null, ['class' => 'form-control tags_type', 'multiple' => 'multiple']) !!}
                    {!! $errors->first('permission', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
 <script type="text/javascript">
 $(document).ready(function(){

        $('.tags_type').select2({
          placeholder: "Choose Permission",
          allowClear: true
        });
});
 </script>
 @endsection