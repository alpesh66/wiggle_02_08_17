@extends('layouts.master')
@section('title', trans('text.showCategory'))
@section('content')
<div class="container">

    <h1>@lang('text.showCategory') 
        <a href="{{ url('categories/' . $category->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Category"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['categories', $category->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Category',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                <tr><th> Name </th><td> {{ $category->name }} </td></tr>
                <tr><th> Name Ar </th><td> {{ $category->name_er }} </td></tr>
                <tr><th> Category </th><td> {{ $cat_name->name }} </td></tr>
                
                @if($category->image)
                    <tr><th> Image </th><td> <img src="{{ asset('uploads/categories/'.$category->image) }}" width="30%" title="{{ $category->name }}" /></td></tr>
                @endif
                
                @if($category->image_ar)
                    <tr><th> Image </th><td> <img src="{{ asset('uploads/categories/'.$category->image_ar) }}" width="30%" title="{{ $category->name_ar }}" /></td></tr>
                @endif
                <tr><th> Display Order </th><td> {{ $category->display }} </td></tr>
                <tr><th> Status </th><td> 
                    @if($category->status) 
                        <label class="label btn-primary"> Active</label> 
                    @else
                        <label class="label btn-danger">Inactive</label>
                    @endif
                </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
