@extends('layouts.master')
@section('title',trans('text.editCategory'))
@section('content')

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">

    <h2>@lang('text.editCategory') </h2>
 <div class="clearfix"></div>
      </div>
      <div class="x_content">
    {!! Form::model($category, [
        'method' => 'PATCH',
        'url' => ['/categories', $category->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        <div class="form-group {{ $errors->has('parentid') ? 'has-error' : ''}}">
                    {!! Form::label('parentid', 'Parent', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                    <div class="col-sm-6">

                        {{-- <select class='form-control' name="parentid"> {{ get_multiple_categories( $categories, $category->parentid )}}
                        </select> --}}
                        {!! Form::select('parentid', $categories, null, ['class' => 'form-control']) !!}
                        {!! $errors->first('parentid', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_er') ? 'has-error' : ''}}">
                {!! Form::label('name_er', 'Arabic Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_er', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_er', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{-- <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    @if($category->image)
                        <img src="{{ asset('uploads/categories/'.$category->image) }}" width="30%" title="{{ $category->name }}" />
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('image_ar') ? 'has-error' : ''}}">
                {!! Form::label('image_ar', 'Image Ar', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image_ar', '<p class="help-block">:message</p>') !!}
                    @if($category->image_ar)
                        <img src="{{ asset('uploads/categories/'.$category->image_ar) }}" width="30%" title="{{ $category->name_ar }}" />
                    @endif
                </div>
            </div> --}}
            <div class="form-group {{ $errors->has('display') ? 'has-error' : ''}}">
                {!! Form::label('display', 'Order', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('display', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('display', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" {{  ($category->status == 1)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url('categories') }}">Cancel</a>
        </div>
    </div>
    {!! Form::close() !!}


      </div>
    </div>
  </div>
</div>
@endsection


@section('jqueries')
<script type="text/javascript">
$(document).ready(function(){
    $('.bootswitch').bootstrapSwitch();;
    
});
</script>
@endsection