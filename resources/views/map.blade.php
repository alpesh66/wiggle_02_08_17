<?php
$lat = 29.31166;
$long = 47.48176;

if (isset($_REQUEST['lat']) && !empty($_REQUEST['lat']))
    $lat = trim($_REQUEST['lat']);
else
    $lat = 29.31166;

if (isset($_REQUEST['long']) && !empty($_REQUEST['long']))
    $long = trim($_REQUEST['long']);
else
    $long = 47.48176;

if (isset($_REQUEST['y']) && !empty($_REQUEST['y']))
    $y = trim($_REQUEST['y'] . '_');
else
    $y = '';

?>
<html>
<head>
<title>Opener</title>
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>

<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>

</head>

<body>

<form id='form2' name='form2' >
  <input type="text" value="" id="address" onKeyPress="return EnterEvent(event)" placeholder="Enter a location" autocomplete="off" style="width: 600px;">
  <input type="button" value="Geocode" onClick="codeAddress()">
  <br/>
  <div id="map" style="width:700px; height:480px;">map</div>
  <script language="javascript">

var geocoder;

var map;

var marker;

function codeAddress() {

    var address = document.getElementById('address').value;

    geocoder.geocode({'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            map.setCenter(results[0].geometry.location); 

            //console.log(results[0].geometry.location.lat()+'===='+results[0].geometry.location.lng());

            marker.setPosition(  new google.maps.LatLng( results[0].geometry.location.lat(), results[0].geometry.location.lng() ) );

            //map.panTo( new google.maps.LatLng( 0, 0 ) );

            $('#text3').val(results[0].geometry.location.lat().toFixed(4));

            $('#text4').val(results[0].geometry.location.lng().toFixed(4));  

        } else {

            alert('Geocode was not successful for the following reason: ' + status);

        }

    });

    map.setZoom(10);

}                                

window.onload = function() {

    var input = document.getElementById('address');

    //var options = {componentRestrictions: {country: 'us'}};

    var options = {                      

      types: []

    };



    autocomplete = new google.maps.places.Autocomplete(input,options);

    var latlng = new google.maps.LatLng({{$lat}},{{$long}});



    map = new google.maps.Map(document.getElementById('map'), {

        center: latlng,

        zoom: 8,

        mapTypeId: google.maps.MapTypeId.ROADMAP

    });



    marker = new google.maps.Marker({

        position: latlng,

        map: map,

        title: 'Set lat/lon values for this property',

        draggable: true

    });



    google.maps.event.addListener(marker, 'dragend', function(a) {

        $('#text3').val(a.latLng.lat().toFixed(4));

        $('#text4').val(a.latLng.lng().toFixed(4));                        

    });

//                    google.maps.event.addListener(autocomplete, 'place_changed', function() {

//                        codeAddress();                        

//                    });

    

    google.maps.event.addListener(autocomplete, 'place_changed', function(a) {



       marker.setVisible(false);

       var place = autocomplete.getPlace();

       if (!place.geometry) {

         return;

       }



       // If the place has a geometry, then present it on a map.

       if (place.geometry.viewport) {

         map.fitBounds(place.geometry.viewport);

       } else {

         map.setCenter(place.geometry.location);

         map.setZoom(17);  // Why 17? Because it looks good.

       }

//                       marker.setIcon(/** @type {google.maps.Icon} */({

//                         url: place.icon,

//                         size: new google.maps.Size(71, 71),

//                         origin: new google.maps.Point(0, 0),

//                         anchor: new google.maps.Point(17, 34),

//                         scaledSize: new google.maps.Size(35, 35)

//                       }));

       marker.setPosition(place.geometry.location);
       marker.setVisible(true);

       var address = '';

       if (place.address_components) {

         address = [

           (place.address_components[0] && place.address_components[0].short_name || ''),

           (place.address_components[1] && place.address_components[1].short_name || ''),

           (place.address_components[2] && place.address_components[2].short_name || '')

         ].join(' ');

       }    

       $('#text3').val(place.geometry.location.lat().toFixed(4));

        $('#text4').val(place.geometry.location.lng().toFixed(4));  

     });
     
    geocoder = new google.maps.Geocoder();
    
};

function EnterEvent(e) {

    if (e.keyCode == 13) {

        codeAddress();

    }

}

$(".pac-item").click(function (){

    codeAddress();

});

 function validepopupform() {

        self.opener.document.getElementById('latitude').value = document.form2.text3.value;
        self.opener.document.getElementById('longitude').value = document.form2.text4.value;
        self.close();
    
    }

</script>
<input type='text' id='text3' name='text3' value="{{$lat}}" />
<input type='text' id='text4' name='text4' value="{{$long}}" />
<input type='button' value='go' onclick='validepopupform();' />
</form>
</body>
</html>
