@extends('layouts.master')
@section('title', 'Gallery')
@section('content')

@section('linkcss')
    <link href="{{ asset('vendor/dropzone/dist/min/dropzone.min.css')}}" rel="stylesheet" type="text/css" >    
@endsection
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Gallery<h1>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form action="{{ url('providers/gallery') }}" class="dropzone" enctype='multipart/form-data'>
            {{ csrf_field() }}
        </form>

</div>
</div>
</div>
</div>
@endsection

@section('jqueries')
    <script src="{{ asset('vendor/dropzone/dist/min/dropzone.min.js') }}"> </script>

    <script type="text/javascript">
    $(document).ready(function(){
       

    });
    </script>
@endsection