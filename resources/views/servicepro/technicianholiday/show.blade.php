@extends('layouts.master')
@section('title', 'Show Attendance exceptions')
@section('content')
<div class="container">

    <h1>Technician Attendance exceptions 
        <a href="{{ url('providers/technicianholiday/' . $technicianholiday->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit technicianholiday"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['providers/technicianholiday', $technicianholiday->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete technicianholiday',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>Technician</th><td>{{ $technicianholiday->technician->name }}</td>
                </tr>
                <tr>    
                    <th>Day</th><td>{{ $technicianholiday->weekday }}</td>
                </tr>
                <tr>    
                    <th>Start</th><td>{{ $technicianholiday->start }}</td>
                </tr>
                <tr>
                    <th>End</th><td>{{ $technicianholiday->end }}</td>
                </tr>
                <!-- <tr>
                    <th>Type</th><td>{{ $technicianholiday->type }}</td>
                </tr> -->
                <tr>
                    <th>Status</th><td>{{ $technicianholiday->status }}</td>
                </tr>
                
            </tbody>
        </table>
    </div>

</div>
@endsection
