@extends('layouts.master')
@section('title', 'Edit Attendance exceptions')
<style type="text/css">
    .disnone{
        display: none;
    }
</style>
@section('content')
<div class="row">

    <h1>Edit Attendance exceptions</h1>

    {!! Form::model($technicianholiday, [
        'method' => 'PATCH',
        'url' => ['/providers/technicianholiday', $technicianholiday->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('technician_id  ', 'Technician', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {{ Form::select('technician_id', $technicians, null, ['class' => 'form-control']) }}
                {!! $errors->first('technician_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('Holiday Type', 'Holiday Date / Day', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="radio" class="" id="date" name="holidaytype" value="0" {{  ($technicianholiday->weekday == '') ? 'checked="checked"' : "" }} > Date
                <input type="radio" class="" id="day" name="holidaytype" value="1" {{  ($technicianholiday->weekday != '') ? 'checked="checked"' : "" }} > Day
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group date {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('start', 'Start Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('start', date('m/d/Y',strtotime($technicianholiday->start)) , ['class' => 'form-control startdate']) !!}
                {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group date  {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('end', 'End Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('end', date('m/d/Y',strtotime($technicianholiday->end)), ['class' => 'form-control enddate']) !!}
                {!! $errors->first('toend', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group day {{  ($technicianholiday->weekday == '') ? 'disnone' : '' }} {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('weekday', 'Day', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="radio" class="weekday" value="monday" name="weekday" {{  ($technicianholiday->weekday == 'monday') ? 'checked="checked"' : "" }} > Monday <br>
                <input type="radio" class="weekday" value="tuesday" name="weekday" {{  ($technicianholiday->weekday == 'wednesday') ? 'checked="checked"' : "" }} > Tuesday <br>
                <input type="radio" class="weekday" value="wednesday" name="weekday" {{  ($technicianholiday->weekday == 'wednesday') ? 'checked="checked"' : "" }} > Wednesday <br>
                <input type="radio" class="weekday" value="thursday" name="weekday" {{  ($technicianholiday->weekday == 'thursday') ? 'checked="checked"' : "" }} > Thursday <br>
                <input type="radio" class="weekday" value="friday" name="weekday" {{  ($technicianholiday->weekday == 'friday') ? 'checked="checked"' : "" }} > Friday <br>
                <input type="radio" class="weekday" value="saturday" name="weekday" {{  ($technicianholiday->weekday == 'saturday') ? 'checked="checked"' : "" }} > Saturday <br>
                <input type="radio" class="weekday" value="sunday" name="weekday" {{  ($technicianholiday->weekday == 'sunday') ? 'checked="checked"' : "" }} > Sunday <br>
                {!! $errors->first('weekday', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('type', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="radio" class="" id="full" name="type" value="0" {{  ($technicianholiday->type == 0) ? 'checked="checked"' : "" }} > Full Day
                <input type="radio" class="" id="half" name="type" value="1" {{  ($technicianholiday->type == 1) ? 'checked="checked"' : "" }} > Half Day
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        
        <div class="form-group breaktime {{  ($technicianholiday->type == 0) ? 'disnone' : '' }} {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('breakstart', 'Start Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('breakstart', null, ['class' => 'form-control time','id' => 'breakstart']) !!}
                {!! $errors->first('breakstart', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group breaktime {{  ($technicianholiday->type == 0) ? 'disnone' : '' }} {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('breakend', 'End Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('breakend', null, ['class' => 'form-control time','id' => 'breakend']) !!}
                {!! $errors->first('breakend', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <!-- <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="checkbox" name="status" class="bootswitch" {{  ($technicianholiday->status == 1) ? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No">
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div> -->

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Update', ['class' => 'btn btn-primary ']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}


</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}
<script type="text/javascript">
        $(document).ready(function(){
            $('.bootswitch').bootstrapSwitch();

            $("#full").click(function(){
                $(".breaktime").hide();
                $("#breakstart").val('00:00');
                $("#breakend").val('00:00');
            });

            $("#half").click(function(){
                $(".breaktime").show();
            });

            $("#date").click(function(){
                $(".day").hide();
                
                $('.weekday').attr('checked', false);
            });

            $("#day").click(function(){
                $(".day").show();
                
                $(".startdate").val('');
                $(".enddate").val('');                
            });

            $('.time').bootstrapMaterialDatePicker
            ({
                date: false,
                shortTime: false,
                format: 'HH:mm'
            });

        });

        var startDate = new Date();
        var FromEndDate = new Date();
        var ToEndDate = new Date();

        // ToEndDate.setDate(ToEndDate.getDate()+365);
        // $('.startdate').datepicker({
        //     weekStart: 1,
        //     today:true,
        //     autoclose: true,
        //     //dateFormat: 'yy-mm-dd' 
        // }).on('changeDate', function(selected){
        //     startDate = new Date(selected.date.valueOf());
        //     var tempStartDate = new Date(startDate);
        //     var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate());
        //     $('.enddate').datepicker('setDate', default_end);
        // });

        // $('.enddate').datepicker({
        //     weekStart: 1,
        //     today:true,
        //     autoclose: true,
        //     //dateFormat: 'yy-mm-dd' 
        // });

        $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now' 
        });

        $('.enddate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now'
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
        });
</script>
@endsection