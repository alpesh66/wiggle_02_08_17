@extends('layouts.master')
@section('title', 'Dashboard')


@section('linkcss')
<style type="text/css">
  .fc-today {
    background: #FFF !important;
    font-weight: bold;
} 


#external-events {
    /*background-color: #333;*/
    /*float:left;*/
    box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; -o-box-sizing: border-box;
    padding:10px 10px; 
    display: block;
    text-align: left;
    margin-bottom: 15px;
  }

#external-events.stick {
    position: fixed;
    top: 0;
    z-index: 1030;
    border-radius: 0 0 0.5em 0.5em;
    width: 82%;
}
    
  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
  }
    
#external-events .fc-event {
    margin: 2px 2px 2px 2px;
    cursor: pointer;
    /*display: none ;*/
    display: inline-block !important; 
    padding:5px;
  -webkit-border-radius:5px; 
  -moz-border-radius:5px; 
  border-radius:5px; 
  }
    
  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }
    
  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }

</style>
@endsection
@section('content')


<div class="row">
    <div class="x_panel">
      <div class=" position-abs" id="CustomerData_collapse" style="display: none">
  

    <div class="col-lg-12">

    <!-- NEW Search Box START -->
      <div class="form-group searchbox-class">
        <div class="searchbox-input">
          <input type="text" name="searchbox"  class="form-control searchbox" id="searchbox" placeholder="Search by Mobile/Email">
        </div>
        <div class="searchbox-button">
          <button type="button" class="btn btn-success" title="Search" id="search_button_customer"> <i class="fa fa-search"></i></button>
          <button type="button" class="btn btn-primary priCustomerAddition" title="Add New Customer" data-toggle="modal" data-target="#new_customer"> <i class="fa fa-plus"></i></button>

        </div>

      </div>
      <div id="petsarea"><button style="margin-bottom:0px;display:none;" type="button" class="btn btn-primary" title="Add New Customer" data-toggle="modal" data-target="#new_petofcustomer" id="addNewPetButtons"> <i class="fa fa-plus"></i></button><div class="btn-group petsarea" data-toggle="buttons"></div></div>
      <div class="dragservices"><h4 id="drag_service_h4" class="pull-left"></h4>

      </div>
    <!-- NEW Search Box ENDS -->
  </div> <!--End col lg 12-->
    
    <div id="external-events" >
  {{-- <p style="display: none;" class="alert alert-danger error_data2"></p> --}}
              
  {{-- <div class="input-group input-group-drag exter-srchkeyword">
      <input type="text" id="nameforit"   class="form-control drag-box-input exter-srchkeyinput" placeholder="Search Keyword"/>
      <span class="input-group-btn drag-clear-btn">
          <button class="btn btn-default" type="button" id="clear_button_drag">Clear</button>
      </span>
  </div> --}}
  <div class="allservices-drag">
     
  </div>

  <div class="done-f"><button id="customer_close_collapse" title="Done" class="btn btn-danger pull-right">Done</button> </div>
  <div style"clear:both"></div>
  </div>

    </div>
      <div class="x_content calndr_wrp_inr">
      <div id="calendar"></div>
      </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="bookingScreen" tabindex="-1" role="dialog" aria-labelledby="eventclickLabel" style="outline:0 !important; z-index:50000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="eventclickLabel">Book Appointment on </h4>
      </div>
      <form class="form-horizontal" role="form" action="{{ url('/providers/booking') }}" id="calendarbooking" method="post"  autocomplete="off">
      <p id="error_data" style="display: none;" class="alert alert-danger"></p>
      <div class="modal-body ">
            <div class="row externaldrop">
           <div class="col-lg-12 col-md-12 col-sm-12">


              <div class="form-group hide">
                  <label  class="col-sm-3 control-label">Technician <span class="text-maroon">*</span></label>
                  <div class="col-sm-9">
                      {!! Form::select('dropdown_technician_id', $forDroptechnicians, null, ['class' => 'form-control', 'id' => 'technicianLists']) !!}
                  </div>
              </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label">Select Service <span class="text-maroon">*</span></label>
                  <div class="col-sm-9">
                     
                    <select name="service_id" class="form-control" id="spa_service" >
                        <option value="">--- Select ----</option>

                      </select>
                      
                  </div>
                </div>
            <div class="form-group">
                    <label  class="col-sm-3 control-label">Breed <span class="text-maroon">*</span></label>
                  <div class="col-sm-9">
                     
                    <select name="breed" class="form-control" id="breed">
                        <option value="">--- Select ----</option>

                      </select>
                      
                  </div>
                </div>
            <div class="form-group">
                    <label  class="col-sm-3 control-label">Size <span class="text-maroon">*</span></label>
                  <div class="col-sm-9">
                     
                    <select name="size" class="form-control" id="size">


                      </select>
                      
                  </div>
                </div>
                <input name="start" type="hidden" id="startdate">
                <input name="end" type="hidden" id="enddate">
                <input name="technician_id" type="hidden" id="technician_id">
                <input name="customer_id" type="hidden" id="customer_id">
                <input name="type_id" type="hidden" id="type_id">
      </div>
      </div>
     <!--  </div>

      <div class="modal-body"> -->
      <p id="error_data2" style="display: none;" class="alert alert-danger error_data2"></p>
            <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12">
              <h2>Customer Details</h2>
              <div class="suport_doc_wrap">
                 <div class="col-lg-12 col-md-12 col-sm-12">

                          <p id="info_data2" style="display: none;" class="alert alert-info info_data2"></p>
                          {{--  <div class="form-group">
                            <label  class="col-sm-3 control-label">Mobile <span class="text-maroon">*</span> </label>
                            <div class="col-sm-3">
                              <select name="countrycode" id="countrycode"  class="form-control countrycode">
                             
                              </select>
                            </div>
                            <div class="col-sm-4">
                              <input type="text" name="phon" id="calbookphon"  class="form-control calbookphon" maxlength="10">

                            </div>
                            <div class="col-sm-2">
                              <button type="button" class="btn btn-info" onclick="searchcustomer('mblnumber')"><i class="fa fa-search"></i></button>

                            </div>
                          </div> --}}

                           <div class="form-group">
                            <label  class="col-sm-3 control-label">Search By </label>
                            <div class="col-sm-9">
                                <input type="text" name="email" placeholder="Email, Name or Mobile" class="form-control custemail" id="custemail" >
                                <!-- onkeyup="searchcustomer_byemail(this.value)" onblur="checkcustomer_byemail(this.value)" -->

                            </div>
                            {{-- <div class="col-sm-2">
                              <button type="button" class="btn btn-info" onclick="searchcustomer('useremail')"><i class="fa fa-search"></i></button>

                            </div> --}}
                          </div>

                          <div class="form-group formappointmentadd">
                              <label  class="col-sm-3 control-label">First Name <span class="text-maroon">*</span> </label>
                            <div class="col-sm-9">
                                <input type="text" name="firstname" id="firstname" class="form-control firstname" >
                            </div>
                          </div>

                           <div class="form-group formappointmentadd">
                              <label  class="col-sm-3 control-label">Last Name <span class="text-maroon">*</span> </label>
                            <div class="col-sm-9">
                                <input type="text" name="lastname" id="lastname" class="form-control lastname" >
                            </div>
                          </div>

                          <div class="form-group">
                              <label  class="col-sm-3 control-label">Pets <span class="text-maroon">*</span> </label>
                            <div class="col-sm-9 div-pets">
                                <select name="pet_id" id="pets" class="form-control pets" >

                                </select> 
                            </div>
                            <div class="col-sm-8 div-pets-empty" style="display:none"><p>Customer haven't added any pets yet!!</p></div>
                          </div>

                           <div class="form-group formappointmentadd">
                            <label  class="col-sm-3 control-label">Notes</label>
                            <div class="col-sm-9">
                                <textarea class="form-control calbooknotes" name="notes" id="calbooknotes"></textarea>
                            </div>
                          </div>


                   </div>
                </div>
           </div>
           </div>
           </div>  <!-- Modal-body -->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="book_now">Book Now</button>
      </div>
      </form>
    </div>
  </div>
</div>


{{-- add Customer --}}

<div class="modal fade" id="new_customer" tabindex="-1" role="dialog" aria-labelledby="eventclickLabel" style="outline:0 !important; z-index:50000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="eventclickLabel">Add New Client </h4>
      </div>
      <form class="form-horizontal" role="form" action="{{ url('/providers/customers') }}" id="addCustomer" method="post"  autocomplete="off">

      <div class="modal-body"> 
      <p id="error_data2" style="display: none;" class="alert alert-danger error_data2"></p>
            <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12">

              <div class="suport_doc_wrap">
                 <div class="col-lg-12 col-md-12 col-sm-12">

                          <p id="info_data2" style="display: none;" class="alert alert-info info_data2"></p>
         

            <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                {!! Form::label('firstname', ' First Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                    <span id="span_firstname" class="err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('middlename') ? 'has-error' : ''}}">
                {!! Form::label('middlename', ' Middle Name', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('middlename', null, ['class' => 'form-control']) !!}
                    <span id="span_middlename" class="err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                {!! Form::label('lastname', ' Last Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                    <span id="span_lastname" class="err_show"></span>
                </div>
            </div>

            <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
                {!! Form::label('dob', 'Dob', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('dob', null, ['class' => 'form-control startdate']) !!}
                    <span id="span_dob" class="err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('email', null, ['class' => 'form-control ']) !!}
                    <span id="span_email" class="err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                {!! Form::label('gender', 'Gender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                                <div class="checkbox calendar-box">
                <label>{!! Form::radio('gender', '1', true) !!} Male</label>
            </div>
            <div class="checkbox calendar-box">
                <label>{!! Form::radio('gender', '0') !!} Female</label>
            </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                {!! Form::label('mobile', 'Mobile *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-3">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
                    <span id="span_mobile" class="err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('block') ? 'has-error' : ''}}">
                {!! Form::label('block', 'Block *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('block', null, ['class' => 'form-control']) !!}
                    <span id="span_block" class="err_show"></span>
                </div>
            </div>
                     <div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
                         {!! Form::label('street', 'Street *', ['class' => 'col-sm-3 control-label']) !!}
                         <div class="col-sm-9">
                             {!! Form::text('street', null, ['class' => 'form-control']) !!}
                             <span id="span_street" class="err_show"></span>
                         </div>
                     </div>
                     <div class="form-group {{ $errors->has('judda') ? 'has-error' : ''}}">
                         {!! Form::label('judda', 'Jadda', ['class' => 'col-sm-3 control-label']) !!}
                         <div class="col-sm-9">
                             {!! Form::text('judda', null, ['class' => 'form-control']) !!}<span id="span_judda" class="err_show"></span>
                         </div>
                     </div>
                     <div class="form-group {{ $errors->has('house') ? 'has-error' : ''}}">
                         {!! Form::label('house', 'House *', ['class' => 'col-sm-3 control-label']) !!}
                         <div class="col-sm-9">
                             {!! Form::text('house', null, ['class' => 'form-control']) !!} <span id="span_house" class="err_show"></span>
                         </div>
                     </div>
                     <div class="form-group {{ $errors->has('apartment') ? 'has-error' : ''}}">
                         {!! Form::label('apartment', 'Apartment *', ['class' => 'col-sm-3 control-label']) !!}
                         <div class="col-sm-9">
                             {!! Form::text('apartment', null, ['class' => 'form-control']) !!} <span id="span_apartment" class="err_show"></span>
                         </div>
                     </div>
            <div class="form-group {{ $errors->has('area_id') ? 'has-error' : ''}}">
                {!! Form::label('area_id', 'Area', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('area_id',$area, null, ['class' => 'form-control get_areas']) !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('pets.name') ? 'has-error' : ''}}">
                {!! Form::label('pets[name]', 'Pet Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('pets[name]', null, ['class' => 'form-control' ]) !!}
                    <span id="span_pets" class="name err_show"></span>
                </div>
            </div>
          {{--   <div class="form-group {{ $errors->has('pets[dob]') ? 'has-error' : ''}}">
                {!! Form::label('pets[dob]', 'Dob', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::date('pets[dob]', null, ['class' => 'form-control']) !!}
                     <span id="span_pets" class="dob err_show"></span>
                </div>
            </div> --}}
            <div class="form-group {{ $errors->has('pets[breed]') ? 'has-error' : ''}}">
                {!! Form::label('pets[breed]', 'Specie *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('pets[breed]', $breed, null, ['class' => 'form-control patchbreed_two' ]) !!}
                     <span id="span_pets" class="breed err_show"></span>
                </div>
            </div>
            <div class="form-group {{ $errors->has('pets[gender]') ? 'has-error' : ''}}">
                {!! Form::label('pets[gender]', 'Gender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                                <div class="checkbox calendar-box">
                <label>{!! Form::radio('pets[gender]', '1',true) !!} Male</label>
            </div>
            <div class="checkbox calendar-box">
                <label>{!! Form::radio('pets[gender]', '0', true) !!} Female</label>
            </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('pets[size]') ? 'has-error' : ''}}">
                {!! Form::label('pets[size]', 'Size *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('pets[size]', $size,  null, ['class' => 'form-control patchsize_two']) !!}
                     <span id="span_pets" class="size err_show"></span>
                </div>
            </div>

   {{--  <div class="form-group">
        <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
           
        </div>
    </div> --}}
    

                   </div>
                </div>
           </div>
        </div>
    </div>  <!-- Modal-body -->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="createNewCustomer">Create</button>
      </div>
      </form>
    </div>
  </div>
</div>

{{-- ADD PET FORM --}}


<div class="modal fade" id="new_petofcustomer" tabindex="-1" role="dialog" aria-labelledby="lablePetStoring" style="outline:0 !important; z-index:50000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lablePetStoring">Add Pet</h4>
      </div>
      <form class="form-horizontal" role="form" action="{{ url('/providers/pet') }}" id="addPetsForCustomer" method="post"  autocomplete="off">

      <div class="modal-body"> 
     
        <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12">

              <div class="suport_doc_wrap">
                 <div class="col-lg-12 col-md-12 col-sm-12">

            <div class="form-group">
                {!! Form::label('name', 'Pet Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('petname', null, ['class' => 'form-control' ]) !!}
                    <span id="petspan_petname" class="petname err_storepet"></span>
                </div>
            </div>
         {{--    <div class="form-group">
                {!! Form::label('dob', 'Dob', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('petdob', null, ['class' => 'form-control']) !!}
                     <span id="petspan_petdob" class="petdob err_storepet"></span>
                </div>
            </div> --}}
            <div class="form-group">
                {!! Form::label('breed', 'Specie *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('petbreed', $breed, null, ['class' => 'form-control patchbreed' ]) !!}
                     <span id="petspan_petbreed" class="petbreed err_storepet"></span>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('gender', 'Gender', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox calendar-box">
                <label>{!! Form::radio('petgender', '1') !!} Male</label>
            </div>
            <div class="checkbox calendar-box">
                <label>{!! Form::radio('petgender', '0', true) !!} Female</label>
            </div>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('size', 'Size *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('petsize', $size,  null, ['class' => 'form-control patchsize']) !!}
                     <span id="petspan_petsize" class="petsize err_storepet"></span>
                </div>
            </div>
            <input type='hidden' name='petaddCustomerID' id='petaddCustomerID'>
   {{--  <div class="form-group">
        <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
           
        </div>
    </div> --}}
    

                   </div>
                </div>
           </div>
        </div>
    </div>  <!-- Modal-body -->

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="addPetToCustomer">Create</button>
      </div>
      </form>
    </div>
  </div>
</div>

{{-- Multiple Appointment FORM --}}
<form id="multi_booking" class="hide">
<input type="text" name="customer_id" id="multi_customerid">
<input type="text" name="pet_id" id="multi_petid">
<input type="text" name="technician_id" id="multi_technicianid">
<input type="text" name="service_id" id="multi_serviceid">
<input type="text" name="start" id="multi_start">
<input type="text" name="end" id="multi_end">
<input type="text" name="type_id" id="multi_typeid">
<input type="text" name="status" id="multi_status" value="1">
<input type="text" name="notes" id="multi_notes">
<input type="text" name="price" id="multi_price">
<input type="text" name="madefrom" value="1">
{{-- <input type="text" name="ontime" id="multi_ontime"> --}}
</form> 

{{-- BOOKING SUMMARY MODAL --}}
<div class="modal fade" id="bookingDetailed" tabindex="-1" role="dialog" aria-labelledby="labelBooking" style="outline:0 !important; z-index:50000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelBooking">Booking Details </h4>
      </div>
      <div class="modal-body" id="detailedBody"> </div>  <!-- Modal-body -->
      <div class="modal-footer">
      <div class="pull-left"> <span>Payment Status </span><input type="checkbox" name="transactionStatus" id="transactionStatus" class="bootswitch pull-left" data-on-text="Paid" data-off-text="Unpaid"> </div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('jqueries')

<script type="text/javascript">
    function checkBreedSize() {
        if($('.patchbreed').val() == 1) {
            $('.patchsize option:last').hide();
//            $('.patchsize option:nth-child(3)').hide();

        } else {
            $('.patchsize option:last').show();
//            $('.patchsize option:nth-child(3)').show();
        }
        $('.patchsize').val(1);
    }

    checkBreedSize();
    $('.patchbreed').change(function(){
        checkBreedSize();
    });

    function checkBreedSize_two() {
        if($('.patchbreed_two').val() == 1) {
            $('.patchsize_two option:last').hide();
//            $('.patchsize_two option:nth-child(3)').hide();

        } else {
            $('.patchsize_two option:last').show();
//            $('.patchsize_two option:nth-child(3)').show();
        }
        $('.patchsize_two').val(1);
    }

    checkBreedSize_two();
    $('.patchbreed_two').change(function(){
        checkBreedSize_two();
    });
var drag_check_service = false;
var refresh_after  =  60000;  // 2 Mins
var drag_check_service  =  false;  
      

function remove_popover() {
    $(".popover").remove();
}

$(document).on("click", ".popover .close" , function(e){
    e.preventDefault();
    $(".popover").remove();
});
function current_timeformat(when_next) {
    var dNow = new Date();
    if(when_next !== undefined){
      var dNow = new Date();
      dNow = new Date(dNow.getTime() + when_next);
    }
    var localdate= dNow.getDate() + '-' + (dNow.getMonth()+1) + '-' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes() + ':' + dNow.getSeconds() ;
    return localdate;
}

 var last_refreshed_at  = current_timeformat() + ' Next will be refreshed at ' + current_timeformat(refresh_after); 

var goTocalendar = '<span class="fc-button fc-corner-right  fc-state-default screencalendar-pp"> <a title="Calender" class="goto_cal" id="goto_cal"><i class="fa fa-calendar"></i></a></span> ';

  var refres_dash = '<span class="fc-header-space"></span><span class="fc-button fc-corner-left fc-state-default refresh-pp"><a title="Last refreshed at '+last_refreshed_at+'" id="auto_refresh" href="#"><i class="fa fa-refresh"></i></a></span>';

$(document).on('click',"#auto_refresh", function(e){
    e.preventDefault();
    refresh_calendar();
});  // #auto_refresh ENDS

$(document).ready(function(){

    $('.sele_type').select2({
          placeholder: "Select Country",
    }).val(['965']).trigger('change');


    $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'YYYY/MM/DD',
        });

    $('.bootswitch').bootstrapSwitch();

    $('#calendar').fullCalendar({
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,resourceDay'
      },
      defaultView: 'resourceDay',
      titleFormat: {'day':'dddd, MMMM D'},
      minTime: "08:00:00",
      maxTime: "23:00:00",
      scrollTime:  moment().format('H:m'),
      eventLimit: true, // allow "more" link when too many events
      resources:[
          {!! $technicians !!}
      ],
      slotDuration: '00:15:00',
      allDaySlot: false,
      lazyFetching: false,
      droppable: true,
      // defaultTimedEventDuration: '00:30:00',
      selectable: true,
      selectHelper: true,
      unselectAuto: true,
      timeFormat: {'agenda':'hh:mm A','':'hh:mm A'},
      dropAccept: '.fc-event',
      eventTextColor: '#666',
      eventColor: '#8cc63f',
      // Calender Method-1: AJAX events will loaded from this code
      events:function(start, end, timezone, callback) {
        console.log('start Date form event '+start+ ' it gets end at '+end+' for Timezone '+timezone );
        $.ajax({
              url: "{{ url('tech/calendarevents') }}",
              dataType: 'json',
              type:'POST',
              // async: false, //blocks window close
              data: {
                  start: start.unix(),
                  end: end.unix(),
                  timezone: 'Asia/Kuwait',
                  viewtype: $('#calendar').fullCalendar('getView').name,
              },
              success:function(response){
                callback(response);
              },
          });
        },
      // Calender Method-2: events information will loaded from this code
      eventRender: function (event, element) {

     if(event.rendering != undefined){
        return true;
    }
    var starttime = $.fullCalendar.moment(event.start);
    starttime = starttime.format("MM/DD/YYYY  h:mm");
    var endtime = $.fullCalendar.moment(event.end);
    endtime = endtime.format("MM/DD/YYYY  h:mm");
    if(event.type_id == '1'){
      // $(element).css("backgroundColor", "#8cc63f");
      $(element).addClass("color-blue");
    } else if(event.type_id == '2'){
      // $(element).css("backgroundColor", '#00aeef');
      $(element).addClass("color-green");
    } else { 
      $(element).addClass("color-pink");
      // $(element).css("backgroundColor", '#ec008c');
    }
        element.popover({
            placement: 'bottom',
            html:true,
            title: event.firstname + " booked Appointment " + "<a href='#' class='close'>&times;</a>",
            content:'Start: '+starttime+'<br/>Service: '+event.servicename+'<br/> Technician: '+event.techname+'<br/>Customer: '+event.firstname+' '+event.lastname+ '<textarea cols="21" id="noteid_'+event.id+'">'+event.notes+'</textarea><br/>',
//            <a onclick="update_comments('+event.id+')" class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i></a>&nbsp;<a onclick="cancel_appointment('+event.id+')" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
            html: true,
            container: 'body',
            'trigger':'click'
        }).on("click", function () {
              $('.popover').remove();
              var _this = this;
              $(this).popover("show");
          }).on("mouseleave", function () {
              $( ".popover" ).each(function() {
                $( this ).addClass( "foo" );
                  if(!$(this).hasClass('in')) {
                      $(this).remove();
                  }
              });
              var _this = this;
              setTimeout(function () {
                  if (!$(".popover:hover").length) {
                      $(_this).popover("hide");
                      $( ".popover" ).each(function() {
                        $( this ).addClass( "foo" );
                          if(!$(this).hasClass('in')) {
                              $(this).remove();
                          }
                      });

                  }
              }, 300);
          });
      

      },
      // Calender Method-3: Click on Calendar And Book single appointment
      select: function(start, end, ev, view) {
        console.log(start);
        console.log(end);
        console.log(view);
         // if(view.name == 'month' || view.name == 'agendaWeek'){ 
         //     $('#calendar').fullCalendar('changeView', 'resourceDay');
         //     $('#calendar').fullCalendar('gotoDate', start);
         //     return false;
         // }
        
         // var startformat = start.format('YYYY-MM-DD HH:mm:ss');
         // var startformat = start.format('YYYY-MM-DD HH:mm:ss');
         // var endcurrformat = '{{ date("Y/m/d H:i:s") }}';

         // var currentDateTime = (Date.parse(start.format('YYYY/MM/DD HH:mm:ss')));
         // var inputDateTime   = (Date.parse(endcurrformat));

         // if(ev.data != undefined){
         //   $('#technician_id').val(ev.data.id);
         // }

         // if (inputDateTime >= currentDateTime){
         //     alert('The date/time you chosen has already passed.');
         //     return false;
         // }
         // $('#technicianLists').change();
         // $('#startdate').val(startformat);
         // $('#eventclickLabel').html('Book Appointment on ' + startformat);
         // $('#bookingScreen').modal({show:true })

      },
      // Calender Method-4: Drag drop the Calendar Events
      eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ) {
          var bookId = event.id;
          var techId = event.resources;
          var starttime = event.start.format('YYYY-MM-DD HH:mm:ss');
          var endtime = event.end.format('YYYY-MM-DD HH:mm:ss');

          $.ajax({
              url: '{{ url('providers/booking/') }}'+'/'+bookId,
              method: 'PATCH',
              data: { booking_id: bookId, technician_id: techId[0], start: starttime, end:endtime},
              dataType: 'json',
              success: function(data){
                if(data.success){
                    grown_noti(data.message, 'success');
                }else {
                    grown_noti(data.message, 'danger');
                }
                
              }
          });
      },
      // Calender Method-5: Increase Decrease the Time Duration
      eventResize: function(event, delta, revertFunc) {

          var bookId = event.id;
          var starttime = event.start.format('YYYY-MM-DD HH:mm:ss');
          var endtime = event.end.format('YYYY-MM-DD HH:mm:ss');
          var deltamins = delta.minutes();
// console.log(bookId);
          $.ajax({
              url: '{{ url('providers/booking/') }}'+'/'+bookId,
              method: 'PATCH',
              data: { booking_id: bookId, start: starttime, end:endtime},
              dataType: 'json',
              success: function(data){
                if(data.success){
                    grown_noti(data.message, 'success');
                }else {
                    grown_noti(data.message, 'danger');
                }
                
              }
          });
      },
      // Calender Method-6: External services drop for multiple booking
    drop: function(date, jsEvent, ui ) { // this function is called when something is dropped
      
        // retrieve the dropped element's stored Event Object
        var techni_id = jsEvent.data.id;
        var startdate = (date.format('YYYY-MM-DD HH:mm:ss'));
      var technician_name = (jsEvent.data.name);
      var endcurrformat = '{{date("Y/m/d H:i:s")}}';
console.log(techni_id+ ' dropped');

      var currentDateTime = (Date.parse(date.format('YYYY/MM/DD HH:mm:ss')));
      var inputDateTime   = (Date.parse(endcurrformat));
      var selDate   = (date.format('MM/DD/YYYY'));
      var displayDate_format = (date.format('dddd, MMMM D YYYY hh:mm A'));
      if (inputDateTime >= currentDateTime){
          alert('The date/time you chosen has already passed.');
          return false;
      }

      // var validate_customer =  $('#drag_service_h4').html();
      // if(validate_customer.length == 0){
      //   grown_noti('Select customer or add new!!','danger');
      //   return false;
      // }
        var originalEventObject = $(this).data('eventObject');
        
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        console.log(copiedEventObject);
        console.log(jsEvent.data);

        // STOP drop if technician is not providing that service
        var tech_provider = copiedEventObject.technician.split(',');


        var selec_techni = String(techni_id);
        if(!($.inArray(selec_techni, tech_provider) !== -1) ) {
          grown_noti('This technician is not providing this service', 'danger');
          return false;
        }

        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.resources = techni_id;
        var serviceid = copiedEventObject.id;
        var title = copiedEventObject.title;
        var total_service = copiedEventObject.duration;
        var pets_id = $('.petsarea input:radio:checked').val();
        $('#multi_technicianid').val(techni_id);
        $('#multi_serviceid').val(serviceid);
        $('#multi_end').val(total_service);
        $('#multi_start').val(startdate);
        $('#multi_typeid').val(copiedEventObject.parentid);
        $('#multi_price').val(copiedEventObject.price);
        $('#multi_petid').val(pets_id);

        // // assign it the date that was reported
        // copiedEventObject.start = date;
        // HERE I force the end date based on the start date + duration
        // copiedEventObject.end = new Date(date.toDate().getTime() + copiedEventObject.duration * 60 * 1000);

      $('#calbookappointmentdate').val(date.format("MM/DD/YYYY"));
      $('#calbookappointtimestart').val(date.format("MM/DD/YYYY HH:mm"));


//         getservices_bytechId(techni_id);

//         $('#spa_service').val(serviceid);
//         // var whataretechs = gettechnician(serviceid);
// // console.log(whataretechs);return false;

        var frm = $('#multi_booking');
        var action = frm.attr('action');

         // console.log(action);console.log(new_action);return true;
        var confirm_book = confirm(title+' at '+displayDate_format+' with '+technician_name+'?');

          if(confirm_book){
            // loader_start('Confirming appointment. Please wait...');
                $.ajax({
                      type: 'post',
                      url: "{{url('providers/booking')}}",
                      data: frm.serialize(),
                      dataType: 'json',
                      success: function (data) {
                        console.log(data);
                        if(data.success){
                            $('#calendar').fullCalendar('renderEvent', data.event,false); 
                            $('.calbookcustomerid').val(data.customerid);

                        }else{
                          grown_noti('Some error occured, try again!', 'danger');
                        }                
                      },
                      error: function (jXHR, textStatus, errorThrown) {
                          alert(errorThrown);
                          location.reload();
                      },
                      complete: function(){
                        // $.loader.close();
                      }
                  });
          } else{
              return false;
          }
      },

  }); // Calendar View ENDS

    $('#technicianLists').on('change', function(){
        var technician_ID = ($('#technician_id').val());

        $.ajax({
          url: '{{ url('providers/technicianwithservice') }}',
          method: 'POST',
          data: { techID: technician_ID},
          dataType: 'json',
          success: function(data){
            console.log(data);
            $('#spa_service').html('');
            $('#spa_service').html(data);
            setTimeout(function(){
              $('#spa_service').change();  
            }, 300);
            
          }
        })
    });


    $('#spa_service').on('change', function(){
      var service_id = $(this).val();
      var service_typeid  = $(this).find(':selected').data("typeid");
      $('#type_id').val(service_typeid);

      console.log(service_id);
      $.ajax({
          url: '{{ url('providers/getbreed') }}',
          method: 'POST',
          data: { service : service_id},
          dataType: 'json',
          success: function(data){
            console.log(data);
            $('#breed').html('');
            $('#breed').html(data.breed);
            
            if(data.size)
              {
                $('#size').html(data.size);
              }
          }
      });
    });

    $('#breed').on('change', function(){
      var service_id = $('#spa_service').val();
      var breed_id = $(this).val();
      console.log(service_id);
      $.ajax({
          url: '{{ url('providers/getsize') }}',
          method: 'POST',
          data: { service : service_id, breed: breed_id},
          dataType: 'json',
          success: function(data){
            console.log(data);        
              $('#size').html('');
          }
      });
    });

    $('#book_now').on('click', function(){
        book_now();
    });


$( "#CustomerData_collapse" ).resizable({
      resize: function( event, ui ) {
      //ui.size.height = Math.round( ui.size.height / 30 ) * 30;
    
    var uiwidth = ui.size.width;
    if (uiwidth < 780) {
      $(".allservices-drag").addClass("allservices-drag-width");
      $(".dragservices").addClass("dragservices-width"); 
      $(".searchbox-class").addClass("searchbox-class-width");
      $(".exter-srchkeyword").addClass("exter-srchkeyword-width");
      
    }else
    {
      $(".allservices-drag").removeClass("allservices-drag-width");
      $(".dragservices").removeClass("dragservices-width"); 
      $(".searchbox-class").removeClass("searchbox-class-width"); 
      $(".exter-srchkeyword").removeClass("exter-srchkeyword-width");
    }

    }
  });

  $( "#custemail" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{ url('providers/customers/search') }}",
          dataType: "json",
          method:'POST',
          data: {
            name: request.term
          },
          success: function( data ) {
            console.log(data);
            console.log(data.size());
            response( data );
          }
        } );
      },
      minLength: 2,
      select: function( event, ui ) {
        console.log( "Selected forautocomplete: " + ui.item.firstname + " aka " + ui.item.id );
          $('#custemail').val(ui.item.firstname);
          $('#firstname').val(ui.item.firstname);
          $('#lastname').val(ui.item.lastname);
          $('#customer_id').val(ui.item.id);
          $('.allservices-drag').html('');
          if(ui.item.pet.length > 0)
          {
            $('#pets').html('');
            $('.div-pets').show();
            $('.div-pets-empty').hide();
            $(ui.item.pet).each(function(key, value){
                $('#pets').append($("<option></option>")
                    .attr("value",value.id)
                    .text(value.name)); 
            });
          } else {
            $('.div-pets').hide();
            $('.div-pets-empty').show();
          }
      }
    } );


$('#CustomerData_collapse').on('shown.bs.collapse', function () {
    drag_check_service  = true;
    $('#CustomerData_collapse').fadeIn().draggable().resizable();
    $('.appointment-btn').hide();
    $('#customer_close_collapse').show();
    $('#searchbox').focus();
    // $('.appointment-btn').removeClass('btn-success');

    
});

$('#new_petofcustomer').on('hidden.bs.modal', function () {
  console.log('called from new_petsofcustomer for hidden');
    $('#new_petofcustomer input[name=petname]').val('');
});

$('#new_customer').on('hidden.bs.modal', function () {
    $('#new_customer .form-group input').val('');
    console.log('customer form cleared');
});

$('#bookingDetailed').on('hidden.bs.modal', function () {
    $('#transactionStatus').attr('data-tranid',null);
    $("#transactionStatus").bootstrapSwitch('state', false);
    $("#transactionStatus").bootstrapSwitch('destroy');
    console.log('bookingdetaile destroyed on modal closed');
});


// For Customer #CustomerData_collapse
$('#CustomerData_collapse').on('hidden.bs.collapse', function () {
  // alert(1);

  $('.priCustomerAddition').show();
    $('#addNewPetButtons').hide();
    var customerIDforTransaction = $('#multi_customerid').val();
    $('.petsarea').html('');
    $('.allservices-drag').html('');

    $.ajax({
          url: '{{ url('providers/storeinboxmessage') }}',
          method: 'POST',
          data: {customerid : customerIDforTransaction},
          dataType: 'json',
          success: function(data){
            console.log(data);            
            if(data.success)
            {
              $('#detailedBody').html(data.summary);
              $("#transactionStatus").bootstrapSwitch();
              $('#transactionStatus').attr('data-tranid',data.transaction);
              $('#bookingDetailed').modal({show:true });
            }
          }
      });
    $('#multi_customerid').val('');
    drag_check_service  = false;
}); 
  // #CustomerData_collapse ENDS
  
  // Payment Status Update
$('body').on('switchChange.bootstrapSwitch', '#transactionStatus', function(event, state) {
// $('body').on('click', '#transactionStatus'. function(event) {
     //var getTranID = $('#transactionStatus').data('tranid');
     var getTranID = $('#transactionStatus').attr('data-tranid');
     console.log(getTranID);
 
     if(typeof getTranID === 'undefined' ){
      console.log('undefined catched in part3');
      return false;
     }
     
     // var state = $("#transactionStatus").bootstrapSwitch('state');
     console.log(getTranID);
     var resultStatus = '';
    // if(state == true) {
    //     update_transactionState(getTranID, '0');
    // } else {
       update_transactionState(getTranID, '1');        
    // }
    $('#bookingDetailed').modal('hide');
});

function update_transactionState(transID, status) {

    $.ajax({
          url: '{{ url('providers/transaction') }}'+'/'+transID,
          method: 'PATCH',
          data: {id : transID, result : status, _method: 'PATCH'},
          dataType: 'json',
          success: function(data){
            console.log(data);            
            if(data.success)
            {
               grown_noti('Payment made successfully', 'success');
            } else {
              grown_noti('Payment still Pending', 'danger');
            }
          }
      });
}

    $('.fc-header-left').append(refres_dash);


$('.goto_cal').datepicker({
    autoclose: true
}).on('changeDate', function(selected){

        var start = new Date(selected.date.valueOf());
        $('#calendar').fullCalendar('changeView', 'resourceDay');
        $('#calendar').fullCalendar('gotoDate', start);
    });;

    
    $('#customer_close_collapse').on('click', function(){
// $('.appointment-btn').on('click', function(){
    // var custid = $('.calbookcustomerid').val();
    // var name =  $('.calbookfirstname').val();

   

    var showOrNot = $('#bookingSummarytext').text();
    $('.appointment-btn').show();
    $('#bookingSummaryLabel').text('Appointment details for '+name);
    if(showOrNot.length > 0){
        $('#bookingSummaryModal').modal('show');
    }
    $('.appointment-btn').show();
    $('#customer_close_collapse').hide();


    var ser_value = $('#searchbox').val();
    if(ser_value.length > 0){
        // $('#searchbox').autocomplete();
        // $('#searchbox').autocomplete("destroy");
        $('#searchbox').val("");
    }
    $('#drag_service_h4').html('');
    $('#CustomerData_collapse').collapse('hide');
    
});

$('#createNewCustomer').on('click', function(e) {

    e.preventDefault();
    var allInputDiv = $('#addCustomer .form-group');
    $.each(allInputDiv, function(key, value) {
            $(this).removeClass('has-error');

        });

    $.each($('#addCustomer .form-group span.err_show'), function(key, value) {
            $(this).text('');
            // console.log($(this));
        });

    var clientform = $('#addCustomer');

        var posturl = clientform.attr('action');

         // console.log(action);console.log(new_action);return true;
        // var confirm_book = confirm(title+' at '+displayDate_format+' with '+technician_name+'?');
        var confirm_book = true;
        if(confirm_book){
            // loader_start('Confirming appointment. Please wait...');
            $.ajax({
                  type: 'post',
                  url: posturl,
                  data: clientform.serialize(),
                  dataType: 'json',
                  success: function (data) {
                    console.log(data);
                    if(data.success){
                        $('#new_customer').modal('hide');
                        grown_noti('Client added successfully', 'success');
                        var variabl = $('#searchbox').val(data.customer.email);
                        $('#searchbox').autocomplete('search', variabl);

                    }else{
                      grown_noti('Some error occured, try again!', 'danger');
                    }                
                  },
                  error: function (data) {
                       var errors = $.parseJSON(data.responseText);
                        console.log(errors);

                        $.each(data.responseJSON, function (key, value) {
                            var input = '#addCustomer #span_' + key;
                            $(input).text(value);
                            console.log(input);
                            // $(input).attr('placeholder',value);
                            $(input).parent().parent().addClass('has-error');
                        });

                  },
                  complete: function(){
                    // $.loader.close();
                  }
              });
        } else{
            return false;
        }
});


$('#addPetToCustomer').on('click', function(e) {

    e.preventDefault();
    var allInputDiv = $('#addPetsForCustomer .form-group');
    $.each(allInputDiv, function(key, value) {
            $(this).removeClass('has-error');
        });

    var get_customerID = $('#multi_customerid').val();

    if(get_customerID == '' || !$.isNumeric(get_customerID))
    {
        grown_noti('Customer not found, please try again', 'danger');
        return false;
    }
    $('#petaddCustomerID').val(get_customerID);
    $.each($('#addPetsForCustomer .form-group span'), function(key, value) {
            $(this).text('');

        });
    var clientform = $('#addPetsForCustomer');

        var posturl = clientform.attr('action');

         // console.log(action);console.log(new_action);return true;
        // var confirm_book = confirm(title+' at '+displayDate_format+' with '+technician_name+'?');
        var confirm_book = true;
        if(confirm_book){
            // loader_start('Confirming appointment. Please wait...');
            $.ajax({
                  type: 'post',
                  url: posturl,
                  data: clientform.serialize(),
                  dataType: 'json',
                  success: function (data) {
                    console.log(data);
                    if(data.success){
                        $('#new_petofcustomer').modal('hide');
                        grown_noti('Pet has been successfully added', 'success');
                        console.log(data.mypet.breed);
                        var breed_class = (data.mypet.breed == 1)? 'cat' : 'dog';
                        // console.log(breed_class);
                        $('.petsarea').append('<label class="btn btn-primary"><i class="small-icon '+breed_class+'"></i><input class="petPrimary-radio" type="radio" data-breed="'+data.mypet.breed+'" data-size="'+data.mypet.size+'" name="options" value="'+data.mypet.id+'"  autocomplete="off">'+data.mypet.name+'</label>');

                    }else{
                      grown_noti('Some error occured, try again!', 'danger');
                    }                
                  },
                  error: function (data) {
                       var errors = $.parseJSON(data.responseText);
                        console.log(errors);

                        $.each(data.responseJSON, function (key, value) {
                            var input = '#new_petofcustomer #petspan_' + key;
                            $(input).text(value);
                            console.log(input);
                            // $(input).attr('placeholder',value);
                            $(input).parent().parent().addClass('has-error');
                        });

                  },
                  complete: function(){
                    // $.loader.close();
                  }
              });
        } else{
            return false;
        }
});



});

function searchcustomer(email) {
  var email = $('#custemail').val();
    $.ajax({
          url: '{{ url('providers/getinfo') }}',
          method: 'POST',
          data: { email: email},
          dataType: 'json',
          success: function(data){
            console.log(data);
            $('#firstname').val(data.firstname);
            $('#lastname').val(data.lastname);
            $('#customer_id').val(data.id);
            $('#pets').html(data.allpets);
          }
        });
}

function book_now() {
  var frm = $('#calendarbooking');
  var serviceTime = $('#size').find(':selected').data("service");
  var servicePrice = $('#size').find(':selected').data("price");
  $('#enddate').val(serviceTime);

  $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        dataType: 'json',
        success: function (data) {
          console.log(data);
          if(data.success){
              $('#calendar').fullCalendar('renderEvent', data, false); 
              location.reload();
          }
          else {
              grown_noti('Some error occured, try again!', 'danger');
          }
          $('#bookingScreen').modal(hide)
        },
        error: function (jXHR, textStatus, errorThrown) {
            // alert(errorThrown);
            location.reload();
        },
        complete: function(){

        }
    });
return true;
}


function refresh_calendar() {
  if(!drag_check_service){
      var ok_refresh = 'Last refreshed at ' + current_timeformat() + ' Next will be refreshed at ' + current_timeformat(refresh_after);
      $('#auto_refresh').attr('title',ok_refresh);
      $('#calendar').fullCalendar('refetchEvents');
  }
}

function update_comments(id){

    var comm_id = '#noteid_'+id;
    var comm = $(comm_id).val();
    if(id != ''){
        // loader_start('Updating comment. Please wait...');
        $.ajax({
                url: '{{ url('providers/booking/') }}'+'/'+id,
              method: 'PATCH',
                data:{ id:id,notes:comm },
                dataType: 'json',
                success:function(data) {
console.log(data);
                    if(data.success) {
                        remove_popover();
                        grown_noti( 'Comment updated');
                        $('#calendar').fullCalendar('removeEvents', id);
                        $('#calendar').fullCalendar('renderEvent', data.event);
                        // location.reload();
                    }else{
                        grown_noti( 'Comment not updated, try again.','danger');
                    }
                },
                complete: function(){
                    // $.loader.close();
                }
            });
    }
}

function cancel_appointment(id){

    var comm_id = '#noteid_'+id;

    if(id != ''){
        // loader_start('Updating comment. Please wait...');
        $.ajax({
                url: '{{ url('providers/booking/') }}'+'/'+id,
              method: 'PATCH',
                data:{ id:id,status:2 },
                dataType: 'json',
                success:function(data) {

                    if(data.success) {
                        remove_popover();
                        grown_noti('Booking Cancelled Successfully');
                        $('#calendar').fullCalendar('removeEvents', id);
                        // $('#calendar').fullCalendar('renderEvent', data.event);

                    }else{
                        grown_noti('Booking not cancelled, try again.','danger');
                    }
                },
                complete: function(){
                    // $.loader.close();
                }
            });
    }
}

$( "#searchbox" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{ url('providers/customers/search') }}",
          dataType: "json",
          method:'POST',
          data: {
            name: request.term
          },
          success: function( data ) {
            $('.petsarea').html('');
            $('.priCustomerAddition').show();
console.log(data);
            response(data);
          }
        } );
      },
      minLength: 2,
      select: function( event, ui ) {
        console.log( "Selected: " + ui.item.firstname + " aka " + ui.item.id );
          // $('#custemail').val(ui.item.email);
          // $('#firstname').val(ui.item.firstname);
          // $('#lastname').val(ui.item.lastname);
          $('#multi_customerid').val(ui.item.id);
          $('#addNewPetButtons').show();
          // Custoemr Found so hiding the Add button
          $('.priCustomerAddition').hide();
          if(ui.item.pet.length > 0)
          {
            $('#pets').html('');

            $('.div-pets').show();
            $('.div-pets-empty').hide();
            $(ui.item.pet).each(function(key, value){
// console.log(key);
                var breed_class = (value.breed == 1)? 'cat' : 'dog';
                $('.petsarea').append('<label class="btn btn-primary"><i class="small-icon '+breed_class+'"></i><input class="petPrimary-radio" type="radio" data-breed="'+value.breed+'" data-size="'+value.size+'" name="options" value="'+value.id+'" id="" autocomplete="off">'+value.name+'</label>');
                // $('.petsarea').append(createPetHtml(key, value));
            });
          } else {
            $('.div-pets').hide();
            $('.div-pets-empty').show();
          }
      }
    } );
    
    // $('body').on('click', '.petsarea', function(){

    // // console.log($(this).val());

    // console.log($('.petsarea input:radio:checked').val());
    // }); 

    $('.petsarea').on('change', function(){
        var petID = $('.petsarea input:radio:checked').val();
        var breed_id = $('.petsarea input:radio:checked').data('breed');
        var size_id = $('.petsarea input:radio:checked').data('size');
        console.log('size is'+size_id+ ' of breed as ' + breed_id+' PET ID'+petID);

        $('#pet_id').val(petID);
        $.ajax({
              type        : "POST",
              url         : "{{url('providers/services/getforthispet')}}",
              data        : { 'breed' : breed_id , 'size': size_id},
              dataType      : "json",
              success       : function(data){
                    // console.log(data);
                    $('.allservices-drag').html('');
                    if(data.success){
                        $('.allservices-drag').append(data.html);
                        
                        makeDraggableEvent();
                    } else {
                        $('.allservices-drag').append(data.html);
                    }
              }
            });
    });

// Search Icon for customer search
$('#search_button_customer').on('click', function(){
    var variabl = $('#searchbox').val();
    $('#searchbox').autocomplete('search', variabl);
});

// function createPetHtml(key, value) {
//     var petSelect = '';
//     var petChecke = '';
//     if(key == 0){
//         petSelect = 'active';
//         petChecke = 'checked';
//     } else {
//         petSelect = '';
//         petChecke = '';
//     }

//     return '<label class="btn btn-primary '+petSelect+'"><input type="radio" name="options" data-breed="'+value.breed+'" data-size="'+value.size+'" value="'+value.id+'" id="" autocomplete="off" '+petChecke+' > '+value.name+'</label>'
// }

function makeDraggableEvent() {
    $('#external-events .fc-event').each(function() {
        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var oh_duration = ($(this).attr('duration'));

        var eventObject = {
            title: $.trim($(this).text()), // use the element's text as the event title
            id: $(this).data('id'),
            duration:oh_duration,
            parentid:$(this).data('parentid'),
            price:$(this).data('price'),
            technician:$(this).attr('technician'),
            editable: true,
            backgroundColor: '#8ec641',
        };
// console.log(eventObject);
        // get_services_names.push($.trim($(this).text()));
        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);
        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 999,
          revert: true,      // will cause the event to go back to its
          revertDuration: 0,  //  original position after the drag

        });
    });
}

</script>
@endsection