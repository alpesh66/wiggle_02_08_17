@extends('layouts.master')
@section('title', 'Specialists Edit')
@section('content')
<div class="container">

    <h1>Specialists 
        <a href="{{ url('providers/technicians/' . $technician->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit technician"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        
    </h1>
    <div class="technician table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $technician->id }}</td>
                </tr>
                <tr>
                    <th> Name </th>
                    <td> {{ $technician->name }} </td>
                </tr>
                <tr>
                    <th> Name Ar </th>
                    <td> {{ $technician->name_ar }} </td>
                </tr>
                <tr>
                    <th> Number </th>
                    <td> {{ $technician->number }} </td>
                </tr>
                <tr>
                    <th> Email </th>
                    <td> {{ $technician->email }} </td>
                </tr>
                <tr>
                    <th> Address </th>
                    <td> {{ $technician->address }} </td>
                </tr>
                <tr>
                    <th> Biodata </th>
                    <td> {{ $technician->biodata }} </td>
                </tr>
                <tr>
                    <th> Status </th>
                    <td> 
                    @if($technician->status) 
                        <label class="label btn-primary"> Active</label> 
                    @else
                        <label class="label btn-danger">Inactive</label>
                    @endif
                </td>
                </tr>
            </tbody>
        </table>

        <div class="row">
              <div class="col-sm-12">
                <h2>Working Time</h2>
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-main">
                        <div class="form-group">
                        
                            <div class="col-sm-2">
                            {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                            </div>
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                            </div>                   
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                            </div>
                        
                        </div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-list technician-row-list">
                        
                         <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" disabled="disabled" value="sunday" class="" {{  ($technician->techtime[0]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Sunday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">
                            {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[0]->starttime)), ['class' => 'time form-control' , 'disabled']) !!}
                        </div> 

                        

                        <div class="col-sm-5">
                           {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[0]->endtime)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>

                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="monday" class="" {{  ($technician->techtime[1]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Monday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}                             
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[1]->starttime)), ['class' => 'time form-control' ,'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[1]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="tuesday" class="" {{  ($technician->techtime[2]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Tuesday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}                            
                        </div>                    
                        


                        <div class="col-sm-5">

                           {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[2]->starttime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[2]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="wednesday" class="" {{  ($technician->techtime[3]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Wednesday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}                        
                        </div>                    
                        


                        <div class="col-sm-5">

                           {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[3]->starttime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                           {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[3]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="thursday" class="" {{  ($technician->techtime[4]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Thursday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}                   
                        </div>                    
                        


                        <div class="col-sm-5">

                           {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[4]->starttime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                          {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[4]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="friday" class="" {{  ($technician->techtime[5]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Friday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                          {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[5]->starttime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                           {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[5]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            <input type="checkbox" disabled="disabled" name="techtime[day][]" value="saturday" class="" {{  ($technician->techtime[6]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Saturday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                          {!! Form::text('techtime[stime][]', date('H:i', strtotime($technician->techtime[6]->starttime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                           {!! Form::text('techtime[etime][]', date('H:i', strtotime($technician->techtime[6]->endtime)), ['class' => 'time form-control','disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        
                    </div>
                  </div>
                </div><!--new work row-->
                                 
              </div>

              
              <!-- break block -->
              <div class="col-sm-12">
                <h2>Break Time</h2>
                
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-main">
                        <div class="form-group">
                        
                            <div class="col-sm-2">
                            {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                            </div>
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                            </div>                   
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                            </div>
                        
                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-list technician-row-list">
                        
                         <div class="form-group">

                        <div class="col-sm-2 day-list">

                            Sunday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[0]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[0]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>

                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            Monday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[1]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[1]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                             Tuesday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[2]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[2]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            Wednesday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[3]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[3]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            Thursday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[4]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[4]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                           Friday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[5]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[5]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        <div class="form-group">

                        <div class="col-sm-2 day-list">
                            Saturday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        


                        <div class="col-sm-5">

                            {!! Form::text('breaktime[stime][]', date('H:i', strtotime($technician->techbreaktime[6]->breakstart)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        

                        <div class="col-sm-5">
                            {!! Form::text('breaktime[etime][]', date('H:i', strtotime($technician->techbreaktime[6]->breakend)), ['class' => 'time form-control', 'disabled']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>                

                        </div>
                        
                        
                    </div>
                  </div>
                </div><!--new work row-->

    </div>

</div>
</div>
</div>

@endsection
