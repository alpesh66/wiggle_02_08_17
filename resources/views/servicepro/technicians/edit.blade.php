@extends('layouts.master')
@section('title', 'Edit Specialists')

@section('content')
<div class="container">

    <h1>Edit Specialists</h1>

    {!! Form::model($technician, [
        'method' => 'PATCH',
        'url' => ['/providers/technicians', $technician->id],
        'class' => 'form-horizontal techniciansForm',
        'files' => true
    ]) !!}

        {{--     <div class="form-group">
                {!! Form::label('title  ', 'Type *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {{ Form::select('type_id', $types, null, ['class' => 'form-control']) }}
                    {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div> --}}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('title  ', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {{ Form::select('title', $salutions, null, ['class' => 'form-control']) }}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    @if($technician->image)
                        <img src="{{$technician->image}}" width="25%" title="{{ $technician->name }}" />
                    @endif
                </div>
            </div>
            <div class="form-group {{ $errors->has('number') ? 'has-error' : ''}}">
                {!! Form::label('number', 'Number *', ['class' => 'col-sm-3 control-label']) !!}
                 <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('number', $country_code[1], ['class' => 'form-control']) !!}
                    {!! $errors->first('number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('biodata') ? 'has-error' : ''}}">
                {!! Form::label('biodata', 'Biodata', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('biodata', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('biodata', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" {{  ($technician->status == 1)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>                
            </div>

            <div class="row">
              <div class="col-sm-6">
                <h2>Working Time</h2>
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-main">
                        <div class="form-group">
                        
                        	<div class="col-sm-2">
                            {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                            </div>
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                            </div>                   
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                            </div>
                        
                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                  	<div class="work-row-list">
                    	
                        {{-- Technician Timings --}}
                        @foreach($technician->techtime as $key => $teTimes)
                            <div class="form-group">

                                <div class="col-sm-2 day-list">
                                    <input type="checkbox" name='techtimedata[{{$key}}][weekoff]' value="{{$teTimes['weekoff']}}" class="dayss " {{  ($teTimes['weekoff'] == 0)? 'checked="checked"' : "" }} > {{ucfirst($teTimes['weekday'])}}
                                    {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="col-sm-5">
                                  <input type="text" name="techtimedata[{{$key}}][starttime]" id="starttime{{$key}}" class="form-control" value="{{$teTimes['starttime']}}">
                                    {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="col-sm-5">
                                   <input type="text" name="techtimedata[{{$key}}][endtime]" id="endtime{{$key}}" class="form-control" value="{{$teTimes['endtime']}}">
                                    {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                                </div>
                                <input type="hidden" name="techtimedata[{{$key}}][id]" value="{{$teTimes['id']}}">
                            </div>
                        @endforeach
                        {{-- Technician Timings ENDS --}}
                        
                    </div>
                  </div>
                </div><!--new work row-->
                                 
              </div>

              
              <!-- break block -->
              <div class="col-sm-6">
                <h2>Break Time</h2>
                
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-main">
                        <div class="form-group">
                        
                        	<div class="col-sm-2">
                            {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                            </div>
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                            </div>                   
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                            </div>
                        
                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                  	<div class="work-row-list">
                    	
                        {{-- Technician Break --}}
                        @foreach($technician->techbreaktime as $skey => $breakTimes)
                            <div class="form-group">

                                <div class="col-sm-2 day-list">
                                    {{ucfirst($breakTimes['weekday'])}}
                                </div>

                                <div class="col-sm-5">
                                  <input type="text" name="breaktime[{{$skey}}][breakstart]" id="breaktimestart{{$skey}}" class="time form-control" value="{{$breakTimes['breakstart']}}">
                                    {!! $errors->first('breakstart', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="col-sm-5">
                                   <input type="text" name="breaktime[{{$skey}}][breakend]" id="breaktimeend{{$skey}}"  class="time form-control" value="{{$breakTimes['breakend']}}">
                                    {!! $errors->first('breakend', '<p class="help-block">:message</p>') !!}
                                </div>
                                <input type="hidden" name="breaktime[{{$skey}}][id]" value="{{$breakTimes['id']}}">
                            </div>
                        @endforeach
                        {{-- Technician Break ENDS --}}

                    </div>
                  </div>
                </div><!--new work row-->
                
                
                
              </div>
            </div>     


        <div class="form-group text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::button('Update', ['class' => 'btn btn-primary submit_tech']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();
        $('.submit_tech').click(function(){
            if($('.dayss:checked').length == 0){
                 grown_noti('Please select working time','danger');   
            } else {
                $('.techniciansForm').submit();
            }
        });
        $('#breaktimestart0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true

        }).on('change', function(e, date)
        {
            $('#breaktimeend0').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart0').val());
        });
        $('#breaktimestart1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend1').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart1').val());
        });
        $('#breaktimestart2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend2').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart2').val());
        });
        $('#breaktimestart3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend3').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart3').val());
        });
        $('#breaktimestart4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend4').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart4').val());
        });
        $('#breaktimestart5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend5').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart5').val());
        });
        $('#breaktimestart6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true
        }).on('change', function(e, date)
        {
            $('#breaktimeend6').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart6').val());
        });

        $('#breaktimeend0').bootstrapMaterialDatePicker({
            date: false,
            shortTime: false,
            format: 'HH:mm:ss',
            switchOnClick: true,
            minDate : $('#breaktimestart0').val()
        });
        $('#breaktimeend1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart1').val()
        });
        $('#breaktimeend2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart2').val()
        });
        $('#breaktimeend3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart3').val()
        });
        $('#breaktimeend4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart4').val()
        });
        $('#breaktimeend5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart5').val()
        });
        $('#breaktimeend6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate : $('#breaktimestart6').val()
        });
        $('#starttime0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        });
        $('#starttime1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        });
         $('#starttime2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        });
          $('#starttime3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        });
           $('#starttime4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        });
            $('#starttime5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        });
             $('#starttime6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        });


        $('#endtime0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        });
        $('#endtime1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        });
         $('#endtime2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        });
          $('#endtime3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        });
           $('#endtime4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        });
            $('#endtime5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        });
             $('#endtime6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        });

        $('.sele_type').select2({
            placeholder: "Select Country",
        }).val([{{ $country_code[0] }}]).trigger('change');
    });
    </script>
@endsection