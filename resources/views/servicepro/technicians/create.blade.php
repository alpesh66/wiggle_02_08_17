@extends('layouts.master')
@section('title', 'Add Specialists')
@section('content')
<div class="container">

    <h1>Create New Specialists</h1>
    

    <hr/>

    {!! Form::open(['url' => 'providers/technicians', 'class' => 'form-horizontal techniciansForm', 'files' => true]) !!}

          {{--   <div class="form-group">
                {!! Form::label('title  ', 'Type *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {{ Form::select('type_id', $types, null, ['class' => 'form-control']) }}
                    {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div> --}}
            <div class="form-group">
                {!! Form::label('title  ', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {{ Form::select('title', $salutions, null, ['class' => 'form-control']) }}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('number') ? 'has-error' : ''}}">
                {!! Form::label('number', 'Number *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('country', $country, null, ['class' => 'form-control sele_type']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::text('number', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('biodata') ? 'has-error' : ''}}">
                {!! Form::label('biodata', 'Biodata', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('biodata', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('biodata', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <h2>Working Time</h2>
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="work-row-main">
                    <div class="form-group">
                        
                        <div class="col-sm-2">
                        {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                        </div>
                        
                        <div class="col-sm-5 text-center">
                        {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                        </div>                   
                        
                        <div class="col-sm-5 text-center">
                        {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                        </div>    
                        
                    </div>
                    </div>
                  </div>                  
                </div>

                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                  	<div class="work-row-list" id="worktime">
                        
                        {{-- Technician Timings --}}
                        {{-- @for($key = 0; $key==6; $key++) --}}
                        @foreach(weekdays() as $key => $value)
                            <div class="form-group"  id="work_block_{{$key}}">

                                <div class="col-sm-2 day-list">
                                    <input type="checkbox" name="techtimes[{{$key}}][day]"  value="{{$value}}" class="dayss" checked="checked">{{$value}}
                                </div>

                                <div class="col-sm-4">
                                  <input type="text" name="techtimes[{{$key}}][stime]" id="starttime{{$key}}" class="work_stime form-control" value="{{$providerData[$key]->starttime}}">
                                </div>

                                <div class="col-sm-4">
                                    <input type="text" name="techtimes[{{$key}}][etime]" id="endtime{{$key}}" class="work_etime form-control" value="{{$providerData[$key]->endtime}}">
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-primary apply_all_work" id="{{$key}}">Apply All</a>
                                </div>  
                            </div>
                        @endforeach
                        {{-- Technician Timings ENDS --}}

                        
                                          
                    </div>
                  </div>                  
                </div>
                
              </div>         

              <!-- break block col-sm-2 day-list-->
              <div class="col-sm-6">
                <h2>Break Time</h2>
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                  	<div class="work-row-main">
                        <div class="form-group">
                            <div class="col-sm-2">
                            {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                            </div>                    
                            
                            <div class="col-sm-5 text-center">
                            {!! Form::label('Start Time', 'Start Time', ['class' => 'time control-label']) !!}
                            </div>                    

                            <div class="col-sm-5 text-center">
                            {!! Form::label('End Time', 'End Time', ['class' => 'time control-label']) !!}
                            </div>                    
                        </div>
                    </div>
                  </div>                  
                </div>
                
                
                
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                  	<div class="work-row-list" id="breaktime">
                    	 <div class="form-group" id="break_block_6">

                        <div class="col-sm-2 day-list">
                            Sunday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[stime][]" id="breaktimestart0" class="time break_stime form-control" value="13:00">
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[etime][]" id="breaktimeend0" class="time break_etime form-control" value="14:00">
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-primary apply_all_break" id="6">Apply All</a>
                        </div>                    
                    </div>

                        <div class="form-group" id="break_block_0">
                            <div class="col-sm-2 day-list">
                                Monday
                                {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                            </div>                    


                            <div class="col-sm-4">
                                <input type="text" name="breaktime[stime][]" id="breaktimestart1" class="time break_stime form-control" >
                                {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                            </div>                    

                            <div class="col-sm-4">
                                <input type="text" name="breaktime[etime][]" id="breaktimeend1" class="time break_etime form-control" >
                                {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-primary apply_all_break" id="0">Apply All</a>
                            </div>                     
                        </div>
                    
                    	<div class="form-group" id="break_block_1">

                            <div class="col-sm-2 da-list">
                                Tuesday
                                {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                            </div>                    

                            <div class="col-sm-4">
                                <input type="text" name="breaktime[stime][]" id="breaktimestart2" class="time break_stime form-control">
                                {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                            </div>                    

                            <div class="col-sm-4">
                                <input type="text" name="breaktime[etime][]" id="breaktimeend2" class="time break_etime form-control">
                                {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-primary apply_all_break" id="1">Apply All</a>
                            </div>                    
                        </div>

                    	<div class="form-group" id="break_block_2">
                        <div class="col-sm-2 day-list">
                            Wednesday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[stime][]" id="breaktimestart3" class="time break_stime form-control">
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[etime][]" id="breaktimeend3" class="time break_etime form-control">
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                                <a class="btn btn-primary apply_all_break" id="2">Apply All</a>
                            </div>                    
                    </div>

	                    <div class="form-group" id="break_block_3">

                        <div class="col-sm-2 day-list">
                            Thursday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    

                        <div class="col-sm-4">
                            <input type="text" name="breaktime[stime][]" id="breaktimestart4" class="time break_stime form-control">
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>                    

                        <div class="col-sm-4">
                            <input type="text" name="breaktime[etime][]" id="breaktimeend4" class="time break_etime form-control">
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-primary apply_all_break" id="3">Apply All</a>
                        </div>                    
                    </div>

	                    <div class="form-group" id="break_block_4">

                        <div class="col-sm-2 day-list">
                           Friday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[stime][]" id="breaktimestart5" class="time break_stime form-control">
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[etime][]" id="breaktimeend5" class="time break_etime form-control">
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-primary apply_all_break" id="4">Apply All</a>
                        </div>                    
                    </div>

	                    <div class="form-group" id="break_block_5">
                        <div class="col-sm-2 day-list">
                            Saturday
                            {!! $errors->first('bday', '<p class="help-block">:message</p>') !!}
                        </div>                    

                        <div class="col-sm-4">
                            <input type="text" name="breaktime[stime][]" id="breaktimestart6" class="time break_stime form-control">
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>                    
                        <div class="col-sm-4">
                            <input type="text" name="breaktime[etime][]" id="breaktimeend6" class="time break_etime form-control">
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-primary apply_all_break" id="5">Apply All</a>
                        </div>                    
                    </div>

	               
                    </div>
                  </div>                  
                </div>
                
                
              </div>
            </div>    

        <div class="form-group text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
            {!! Form::button('Create', ['class' => 'btn btn-primary submit_tech']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();

        $('.submit_tech').click(function(){
            if($('.dayss:checked').length == 0){
                 grown_noti('Please select working time','danger');   
            } else {
                $('.techniciansForm').submit();
            }
        });

        // $('.time').bootstrapMaterialDatePicker({ date: false });

        

        
        $('#breaktimestart0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend0').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart0').val());
        });
        $('#breaktimestart1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend1').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart1').val());
        });
        $('#breaktimestart2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend2').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart2').val());
        });
        $('#breaktimestart3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend3').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart3').val());
        });
        $('#breaktimestart4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend4').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart4').val());
        });
        $('#breaktimestart5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend5').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart5').val());
        });
        $('#breaktimestart6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        }).on('change', function(e, date)
        {
            $('#breaktimeend6').bootstrapMaterialDatePicker('setMinDate', $('#breaktimestart6').val());
        });
        
        $('#breaktimeend0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        });
        $('#breaktimeend1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        });
        $('#breaktimeend2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        });
        $('#breaktimeend3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        });
        $('#breaktimeend4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        });
        $('#breaktimeend5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        });
        $('#breaktimeend6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        });
        $('#starttime0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        });
        $('#starttime1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        });
         $('#starttime2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        });
          $('#starttime3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        });
           $('#starttime4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        });
            $('#starttime5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        });
             $('#starttime6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        });


        $('#endtime0').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[0]->starttime}}',
                maxDate:'{{$providerData[0]->endtime}}'
        });
        $('#endtime1').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[1]->starttime}}',
                maxDate:'{{$providerData[1]->endtime}}'
        });
         $('#endtime2').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[2]->starttime}}',
                maxDate:'{{$providerData[2]->endtime}}'
        });
          $('#endtime3').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[3]->starttime}}',
                maxDate:'{{$providerData[3]->endtime}}'
        });
           $('#endtime4').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[4]->starttime}}',
                maxDate:'{{$providerData[4]->endtime}}'
        });
            $('#endtime5').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[5]->starttime}}',
                maxDate:'{{$providerData[5]->endtime}}'
        });
             $('#endtime6').bootstrapMaterialDatePicker({
                date: false,
                shortTime: false,
                format: 'HH:mm:ss',
                switchOnClick: true,
                minDate:'{{$providerData[6]->starttime}}',
                maxDate:'{{$providerData[6]->endtime}}'
        });     
        

        $( ".apply_all_work" ).click(function() {
            
            var id = $(this).attr('id');
            console.log(id);
            //get time

            var stime = $("#work_block_"+id+" .work_stime").val();
            var etime = $("#work_block_"+id+" .work_etime").val();

            //set time
            
            $('.work_stime').val(stime);
            $('.work_etime').val(etime);
        });    

        $( ".apply_all_break" ).click(function() {
            var id = $(this).attr('id');
            console.log(id);
            //get time
            var stime = $("#break_block_"+id+" .break_stime").val();
            var etime = $("#break_block_"+id+" .break_etime").val();

            //set time
            $('.break_stime').val(stime);
            $('.break_etime').val(etime);
        });

        $('.sele_type').select2({
          placeholder: "Select Country",
        }).val(['965']).trigger('change');;

    });

    </script>
@endsection