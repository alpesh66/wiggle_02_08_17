<a href="{{ url('providers/technicians/'.$technicians->id) }}" title="@lang('text.view')">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('providers/technicians/'.$technicians->id.'/edit') }}" title="@lang('text.edit')"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$technicians->id}}" data-model="users" title="@lang('text.delete')" name="deleteButton"><i class="fa fa-trash-o"></i></button>