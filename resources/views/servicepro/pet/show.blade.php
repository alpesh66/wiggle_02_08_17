@extends('layouts.master')
@section('title', trans('text.Pet'))
@section('content')
<div class="container">

    <h1>@lang('text.Pet') 
    <a class='btn btn-success btn-xs' title="Back" href="{{ url('providers/customers').'/'.$c_id }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a>
       {{--  <a href="{{ url('pet/' . $pet->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Pet"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['pet', $pet->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Pet',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!} --}}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                {{-- {{dump($pet->toArray())}} --}}
                @if(!empty($pet->photo))
                   <tr><th>Photo</th><td>
                   {{-- <img src="{{$pet->photothumbnail}}" width="25%"> --}}
                   <a href="{{$pet->photo}}" target="_blank" > Click Here</a>
                   </td></tr>
                @endif
                <tr><th> Name </th><td> {{ $pet->name }} </td></tr>
                <tr><th> Dob </th><td> {{ $pet->dob }} </td></tr>
                <tr><th> Species </th><td> {{ $pet->breeds->name }} </td></tr>
                <tr><th> Gender </th><td>  @if($pet->gender == 0)
                                Female
                                @else
                                Male
                                @endif
                </td></tr>
                <!-- <tr><th> Height </th><td> </td></tr> -->
                <tr><th> Weight </th><td> {{ $pet->weight }} </td></tr>
                <tr><th> Size </th><td> {{ $pet->sizes->name }} </td></tr>
                <tr><th> Temperament </th><td> {{ $pet->temperament }} </td></tr>
                <tr><th> Breed </th><td> {{ $pet->origin }} </td></tr>
                <tr><th> Microchip </th><td> {{ $pet->chipno }} </td></tr>
                <tr><th> Grooming Frequency </th><td> {{ $pet->grooming }} </td></tr>
                <tr>
                    <th> Upcoming Vaccination </th>
                    <td> 
                        <?php
                          echo $pet->next_vaccination_date;
                       /* if($pet->veternary=="" || $pet->veternary=="0000-00-00"){
                            $date = date_create($pet->created_at);
                            date_modify($date, $pet->grooming);
                            echo $next_veternary_date = date_format($date, 'Y-m-d');
                        }
                        else{
                            $datev = date_create($pet->veternary);                        
                            echo $next_groom_date = date_format($datev, 'Y-m-d');
                        }*/
                        ?>
                    </td>
                </tr>
                <tr>
                    <th> Upcoming Grooming  </th>
                    <td>
                        <?php
                        echo $pet->next_groom_appointment;
                        /*if($pet->groom=="" || $pet->groom=="0000-00-00"){
                            $date = date_create($pet->created_at);
                            date_modify($date, $pet->grooming);
                            echo $next_groom_date = date_format($date, 'Y-m-d');
                            
                        }
                        else{
                            $dateg = date_create($pet->groom);                        
                            echo $next_groom_date = date_format($dateg, 'Y-m-d');
                        }                        */
                        ?> 
                    </td>
                </tr>
             
            </tbody>
        </table>
    </div>

    <div class="table">
        <h2>Medical Cards</h2>
        <table class="table table-bordered table-striped table-hover" id="dataTables">
            <thead>
                <tr>
                    <th>S.No</th><th>Date</th><th>Vaccination name</th><th> Doctor </th><th> Institution </th><th> Photo Card </th><th> Action </th>
                </tr>
            </thead>            
        </table>

    </div>
</div>
@endsection

@section('jqueries')

<script>
$(document).ready(function(){

    $.fn.dataTable.ext.buttons.delete = {
        text: 'Delete',
        className: 'buttons-danger disabled deleteButton',
        action: function ( e, dt, node, config ) {
            var id = (table.rows('.selected').data().pluck('id').toArray());
            if(id.length == 0)
            {
                 $('.deleteButton').addClass('disabled');
                 return false;
            }
            var checkonce = confirm('Are you sure you want to delete?');
            if(checkonce)
            {
                deleteData(id);
            }
        }
    };

    $.fn.dataTable.ext.buttons.reload = {
        text: 'Reload',
        className: 'buttons-alert',
        action: function ( e, dt, node, config ) {

            $('#post').val('');
            dt.ajax.reload();
            $('.deleteButton').addClass('disabled');
        }
    };

    var table = $('#dataTables').DataTable({

        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [[0, 'desc']],
        columnDefs: [ {
            orderable: true,
            className: 'select-checkbox',
            targets:0,
            visible: false
        } ],
        select: {
            style:    'opts',
            selector: 'td:first-child'
        },
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("providers/getmedicalCard/".$pet->id) }}',
            data: function (d) {               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },
        },
        columns: [
            { data: 'gid', name:'gid', "searchable": false},
            { data: 'givenon', name: 'givenon' },
            { data: 'type', name: 'type' },
            { data: 'vet', name: 'vet' },
            { data: 'institution', name: 'institution' },
            { data: 'photo', name: 'photo' },

            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });


    table.on('select', function ( e, dt, type, indexes ) {

        var counter = table.rows('.selected').data().length;
        if(counter == 0) {
            $('.deleteButton').addClass('disabled');
        } else if(counter == 1) {
            $('.deleteButton').removeClass('disabled');
        } else if(counter > 1){
            $('.deleteButton').removeClass('disabled');
        }
    } );

   table.on( 'deselect', function ( e, dt, type, indexes ) {        
        var counter = table.rows('.selected').data().length;
        if(counter == 0)
        {
            $('.deleteButton').addClass('disabled');
        }
        else if(counter >= 1)
        {
            $('.deleteButton').removeClass('disabled');  
        } 
    } );

    $('.dt-buttons').addClass('pull-right');

    $('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="givenon">Given On</option> <option value="vet">Veternary</option><option value="institution">Institution</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');


    $('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

    function deleteData(id) {
        $.ajax({
            type: 'post',
            data: {id: id, _method: 'delete'},
            url: '{{ url("providers/medicalcard")}}'+'/'+id,
                
            success:function(response){

                if(response.success)
                {
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'success');
                }
                else
                {
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'danger');
                }
            
            },
        });
    }

   $('#dataTables').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = $(this).data('id');
            // console.log(id);
            deleteData(id);
        }
   });

});
</script>
@endsection
