@extends('layouts.master')
@section('title', trans('text.trainbooking'))

@section('content')
<div class="container">

    <h1>@lang('text.trainbooking') </h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> Customer </th><th>Pet</th><th>Technician</th><th>Service</th><th>Appointment On</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($booking as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->customer->firstname }}</td>
                    <td>{{ $item->pet->name }}</td>
                    <td>{{ $item->technician->name }}</td>
                    <td>{{ $item->category->name }}</td>
                    <td>{{ $item->start }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $booking->render() !!} </div>
    </div>

</div>
@endsection
