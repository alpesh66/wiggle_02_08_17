@extends('layouts.master')
@section('title', trans('text.booking'))

@section('content')
<div class="container">

    <h1>@lang('text.booking') </h1>
    {{-- {{dump($booking) }} --}}
{{--     <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTabless">
            <thead>
                <tr>
                    <th> Customer </th><th>Pet</th><th>Services</th><th>Appointment Date/Time</th><th>Status</th><th>Action</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td><div class="cust-d">Adityabhai<br>965-123456<br>aditya@gmail.com</div></td>
                    <td><span class="breed-c">Cat:</span> Tomy<br><span class="breed-c">Dog:</span> Pluto</td>
                    <td><span class="breed-c">Vet:</span> 1<br><span class="breed-c">Groom:</span> 2<br><span class="breed-c">Trainer:</span> 3</td>
                    <td><div class="cust-d">2016-10-04 15:25:00</div></td>
                	<td><div class="cust-d">Paid</div></td>
                    <td><div class="cust-d">
<a href="#"><button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button></a>
<a href="#"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-model="users" name="deleteButton"><i class="fa fa-trash-o"></i></button></div></td>
                </tr>
                <tr>
                	<td><div class="cust-d">Zahurbhai<br>965-123456<br>zahur@gmail.com</div></td>
                    <td><span class="breed-c">Cat:</span> Tomy<br><span class="breed-c">Dog:</span> Pluto</td>
                    <td><span class="breed-c">Vet:</span> 1<br><span class="breed-c">Groom:</span> 2<br><span class="breed-c">Trainer:</span> 3</td>
                    <td><div class="cust-d">2016-10-04 15:25:00</div></td>
                	<td><div class="cust-d">Paid</div></td>
                    <td><div class="cust-d">
<a href="#"><button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button></a>
<a href="#"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-model="users" name="deleteButton"><i class="fa fa-trash-o"></i></button></div></td>
                </tr>

                @foreach($booking as $bookData)
                    @foreach($bookData as $ohData)
                    <tr>
                        <td><div class="cust-d">{{$ohData->firstname}}<br>{{$ohData->mobile}}<br>{{$ohData->email}}</div></td>
                        <td><span class="breed-c">Cat:</span>{{$ohData->petname}}<br><span class="breed-c">Dog:</span> Pluto</td>
                        <td>{{$ohData->servicename}}</td>
                        <td>{{$ohData->start}}</td>
                        <td>{{$ohData->type_id}}</td>
                        <td>{{$ohData->status}}</td>
                    </tr>

                    @endforeach
                @endforeach
            </tbody>
            
           
        </table>

    </div> --}}
    
    
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTables">
            <thead>
                <tr>
                    <th>S.No</th><th> Customer </th><th>Pet</th><th>Technician</th><th>Service</th><th>Appointment On</th><th>Status</th><th>Payment</th><th>Action</th>
                </tr>
            </thead>          
        </table>
    </div>

    {{--  FOR DETAILED VIEW OF BOOKINGS --}}
<div class="modal fade" id="bookingDetailed" tabindex="-1" role="dialog" aria-labelledby="labelBooking" style="outline:0 !important; z-index:50000">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="labelBooking">Booking Details </h4>
      </div>
      <div class="modal-body" id="detailedBody"> </div>  <!-- Modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- For Status Updates --}}
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mymodalforStatusUpdate" id="statusUpdateDetail">
  <div class="modal-dialog modal-sm" role="document" style="width: 310px;">
    <div class="modal-content">
    <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
        
        <h4 class="modal-title" id="myStatusChange">Change Status</h4> </div>
        <div class="modal-body"> 
            <div class="btn-group statusUpdate" data-toggle="buttons">
                <label class="btn btn-success"><input class="bookStatus-radio" type="radio" name="options" value="1" autocomplete="off">Confirm</label>
                <label class="btn btn-primary"><input class="bookStatus-radio" type="radio" name="options" value="0" autocomplete="off">Unconfirmed</label>
                <label class="btn btn-danger"><input class="bookStatus-radio" type="radio" name="options" value="2"  autocomplete="off">Cancel</label>
            </div>
<input type="hidden" name="changeStatusOntime" id="changeStatusOntime">
<input type="hidden" name="statusCustomerID" id="statusCustomerID">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-sm sendBookingReminder" style="display:none">Send Reminder</button>
            <button type="button" class="btn btn-red btn-sm noshow" style="display:none">No Show</button> 
            <button type="button" class="btn btn-success btn-sm updateBookingStatus" aria-label="check-yes"><i class="fa fa-refresh fa-spin" id="loader" style="display: none;"></i> <span aria-hidden="true">Update</span></button> 
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


</div>
@endsection


@section('jqueries')

<script>
$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('providers/bookings/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('ontime').toArray());
       
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('providers/booking') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#dataTables').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("providers/booking") }}',
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [
            { data: 'ontime', name:'ontime', "searchable": false},
            { data: 'firstname', name: 'firstname' },
            { data: 'petname', name: 'petname' },
            { data: 'techname', name: 'techname' },
            { data: 'servicename', name: 'servicename' },
            { data: 'start', name: 'start' },
            { data: 'status', name: 'status'},
            { data: 'payment', name: 'payment' },
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ]
    });

// $('#filter_comparator').change( function() { table.draw(); } );

$('body').on('click', '.paymentStatus', function(event, state) {
// $('body').on('click', '#transactionStatus'. function(event) {
     var transID = $(this).data('transactionid');
     var result = $(this).data('result');
     var thisStatus = $(this);
    console.log(result);
    if(confirm('Do you want to update the payment status?'))
    {
        if(result == 0){
            result = '1';            
        } else {
            result = '0';
        }

        $.ajax({
            url: '{{ url('providers/transaction') }}'+'/'+transID,
            method: 'PATCH',
            data: {id : transID, result : result, _method: 'PATCH'},
            dataType: 'json',
            async: false,
            success: function(data){
                console.log(data);
                if(data.success)
                {
                    // thisStatus.addClass('btn-success');
                    // thisStatus.removeClass('btn-danger');
                    // thisStatus.attr('data-result','0');
                    // thisStatus.text('Paid');
                   grown_noti('Payment made successfully', 'success');
                } else {
                    // thisStatus.removeClass('btn-success');
                    // thisStatus.addClass('btn-danger');
                    // thisStatus.attr('data-result','1');
                    // thisStatus.text('Unpaid');
                    grown_noti('Payment still Pending', 'danger');
                }
                table.draw();
            }
        });
    }

});

$('body').on('click', '.bookingStatus', function(event, state) {

     var ontime     = $(this).data('ontime');
     var currstatus = $(this).data('currstatus');
     var customerid = $(this).data('customerid');

     var thisStatus = $(this);

     $('#changeStatusOntime').val(ontime);
     $('#statusCustomerID').val(customerid);

     $('.statusUpdate').children('.btn-success').removeClass('active');
     $('.statusUpdate').children('.btn-primary').removeClass('active');
     $('.statusUpdate').children('.btn-danger').removeClass('active');
     if(currstatus == 1){
        $('.statusUpdate').children('.btn-success').addClass('active');
        $('.sendBookingReminder').hide();
        $('.noshow').show();
     } else if(currstatus == 0) {
        $('.statusUpdate').children('.btn-primary').addClass('active');
        $('.sendBookingReminder').show();
        $('.noshow').hide();
     } else {
        $('.statusUpdate').children('.btn-danger').addClass('active');
        $('.sendBookingReminder').hide();
        $('.noshow').hide();
     }


    $('#statusUpdateDetail').modal('show');
    return false;
    

});

$('body').on('click', '.sendBookingReminder', function(event, state) {

    var ontime = $('#changeStatusOntime').val();
    var customer = $('#statusCustomerID').val();
    if(ontime.length > 0)
    {
        $.ajax({
            url: '{{ url('providers/sendreminder') }}',
            method: 'POST',
            data: {ontime : ontime, customerid: customer},
            dataType: 'json',
            async: false,
            success: function(data){
                console.log(data);
                if(data.success)
                {
                    $('.sendBookingReminder').fadeOut();
                    $('#statusUpdateDetail').modal('hide');
                   grown_noti(data.message, 'success');
                } else {
                    // $(this).bootstrapSwitch('state', false);
                  grown_noti(data.message, 'danger');
                }
            }
        });
    }
});
$('body').on('click', '.noshow', function(event, state) {

    var ontime = $('#changeStatusOntime').val();
    var customer = $('#statusCustomerID').val();

    if(ontime.length > 0)
    {
        $.ajax({
            url: '{{ url('providers/setnoshow') }}',
            method: 'POST',
            data: {ontime : ontime, customerid: customer},
            dataType: 'json',
            async: false,
            success: function(data){
                console.log(data);
                if(data.success)
                {
                    $('.noshow').fadeOut();
                    $('#statusUpdateDetail').modal('hide');
                   grown_noti(data.message, 'success');
                   table.ajax.reload();
                } else {
                  grown_noti(data.message, 'danger');
                }
            }
        });
    }
});

$('.updateBookingStatus').on('click', function() {
    var ontime = $('#changeStatusOntime').val();
    var customer = $('#statusCustomerID').val();
    var status = $('.statusUpdate input:radio:checked').val();
    $('#loader').show();
    setTimeout(function(){ 
        $('.updateBookingStatus').attr("disabled", true);
        if(ontime.length > 0 && customer.length > 0)
        {
            $.ajax({
                url: '{{ url('providers/updateBookStatus') }}',
                method: 'POST',
                data: {ontime : ontime, customerid: customer, status: status},
                dataType: 'json',
                async: false,
                success: function(data){
                    $('#statusUpdateDetail').modal('hide');
                    $('.updateBookingStatus').attr("disabled", false);
                    $('#loader').hide();
                    if(data.success)
                    {
                       grown_noti(data.message, 'success');
                    } else {
                      grown_noti(data.message, 'danger');
                    }
                    table.draw();
                }
            });
        }
    }, 200);
    

});

$('#statusUpdateDetail').on('hidden.bs.modal', function () {
    console.log('Status modal hidden');
    $('#changeStatusOntime').val('');
    $('#statusCustomerID').val('');
});

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="customers.firstname">First Name</option> <option value="pets.name">Pet Name</option> <option value="technicians.name">Technician</option><option value="categories.name">Service</option>  <option value="bookings.status">Status</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url("providers/booking")}}'+'/'+id,
            
        success:function(response){
            console.log(response);
            if(response.success)
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}

   $('#dataTables').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });

   $('#dataTables').on('click', '.detailedBookings', function(e){
        var getCustomerName = $(this).data('name');
        var getCustomeEmail = $(this).data('email');
        var getCustomMobile = $(this).data('mobile');
        var ontime = $(this).data('ontime');
        $('#labelBooking').text('Booking details of '+getCustomerName+' - '+getCustomeEmail+' - '+getCustomMobile);

        $.ajax({
                url: '{{ url('providers/detailedbookings') }}',
                method: 'post',
                data:{ ontime:ontime },

                success:function(data) {
                    // console.log(data);

                    $('#detailedBody').html(data);
                    $('#bookingDetailed').modal({show:true });
                },
                complete: function(){
                    // $.loader.close();
                }
            });
    
   });

});

</script>
@endsection
