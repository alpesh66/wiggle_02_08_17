@if($booking->status == 1)
    <button class="btn btn-success btn-xs bookingStatus" data-bookingid={{$bookid}}>Confirmed</button>
@elseif($booking->status == 2)
    <button class="btn btn-danger btn-xs bookingStatus" data-bookingid={{$bookid}}>Cancelled</button>
@elseif($booking->status == 0)
    <button class="btn btn-primary btn-xs bookingStatus" data-bookingid={{$bookid}} >Unconfirmed</button> 
@endif