{{-- <input type="checkbox" name="payment_status" class="bootswitch paymentStatus bootstrap-switch-mini" data-transactionid={{$booking->transaction_id}}  data-on-text="Paid" 
 --}}
@if($booking->result)
    <button class="btn btn-success btn-xs paymentStatus" data-result={{$booking->result}} data-transactionid={{$booking->transaction_id}}>Paid</button>
@else
    <button class="btn btn-danger btn-xs paymentStatus" data-result={{$booking->result}}  data-transactionid={{$booking->transaction_id}} >Unpaid</button>
@endif