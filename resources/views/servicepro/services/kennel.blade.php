@extends('layouts.master')
@section('title', trans('text.service'))

@section('content')
<div class="container">
    <h1>Boarding  Available For <!-- : <span id="service_radio_span">Dog  Cat </span> --></h1>
    <div class="table">        
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width: 50%;">Species</th>
                    <th style="width: 50%;">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr class="odd" role="row">
                    <td>Dog</td>
                    <td><input type="checkbox" name="service_type" class="bootswitch1" value="2" id="forDog"  data-on-text="Yes" data-off-text="No"></td>
                </tr>
                <tr class="even" role="row">
                    <td>Cat</td>
                    <td><input type="checkbox" name="service_type" class="bootswitch1" id="forCat" value="1" data-on-text="Yes" data-off-text="No"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr>    
    <h1>@lang('text.service') </h1>
    <div class="table">        
        <table class="table table-bordered table-striped table-hover" id="services">
            <thead>
                <tr>
                    <th>S.No</th><th> Category </th><th>Actions</th>
                </tr>
            </thead>            
        </table>
    </div>

</div>
@endsection

@section('jqueries')

<script>
    
    $(document).ready(function () {

        $.fn.dataTable.ext.buttons.reload = {
            text: 'Reload',
            className: 'buttons-alert',
            action: function (e, dt, node, config) {

                $('#post').val('');
                dt.ajax.reload();
                $('.viewButton, .deleteButton, .editButton').addClass('disabled');
            }
        };

        $.fn.dataTable.ext.buttons.edit = {
            text: 'Edit',
            className: 'buttons-alert disabled editButton',
            action: function (e, dt, node, config) {
                var id = (table.rows('.selected').data().pluck('id'));
                var myfirst = ('{{ url('providers / services / ') }}' + '/' + id[0] + '/edit');

                window.location.href = myfirst;
            }
        };

        $.fn.dataTable.ext.buttons.delete = {
            text: 'Delete',
            className: 'buttons-danger disabled deleteButton',
            action: function (e, dt, node, config) {
                var id = (table.rows('.selected').data().pluck('sid').toArray());

                if (id.length == 0)
                {
                    $('.deleteButton').addClass('disabled');
                    return false;
                }
                var checkonce = confirm('Are you sure you want to delete?');
                if (checkonce)
                {
                    deleteData(id);
                }
            }
        };

        var table = $('#services').DataTable({
            // sDom: 'Brtlip',
            dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'li><'col-sm-7'p>>",
            order: [0, "desc"],
            columnDefs: [{
                    orderable: 'true',
                    className: 'select-checkbox',
                    targets: 0,
                    visible: false

                }],
            select: {
                style: 'opts',
                selector: 'td'
            },
            // select: true,
            processing: true,
            stateSave: true,
            deferRender: true,
            stateDuration: 30,
            //     buttons: [
            //     {
            //        text: 'Reload',
            //        extend: 'reload'
            //     }, 'excel'
            // ],
            buttons: ['delete', 'reload', 'selectAll', {extend: 'selectNone', text: 'Unselect All'}],
            serverSide: true, // use this to load only visible columns in table.

            ajax: {
                url: '{{ url("providers/kennelservice") }}',
                data: function (d) {

                    d.name = $('select[name=name]').val();
                    d.operator = $('select[name=operator]').val();
                    d.post = $('input[name=post]').val();
                },

            },
            columns: [
                {data: 'sid', name: 'sid', "searchable": false},
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', "searchable": false, "orderable": false},
            ],
            "drawCallback": function (settings) {
                $('.bootswitch').bootstrapSwitch();
                $('.bootswitch').on('switchChange.bootstrapSwitch', function (event, state) {
                    var id = ($(this).data('id'));
                    var status=($(this).is(":checked"))?1:0;
                    makekennel(id,status);
                });
                $('.bootswitch1').bootstrapSwitch();
                $('.bootswitch1').on('switchChange.bootstrapSwitch', function (event, state) {
                    var Dog=($('#forDog').is(":checked"))?1:0;
                    var Cat=($('#forCat').is(":checked"))?1:0;
                    if(Dog==1 && Cat==1)
                        status = 0;
                    else if(Dog==1 && Cat==0)
                        status = 2;
                    else if(Dog==0 && Cat==1)
                        status = 1;
                    else if(Dog==0 && Cat==0)
                        status = 3;
                    changeServiceType(status);
                });
            }
        });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

        $('body').on('submit', '.advanceSearch', function (e) {
            table.draw();
            e.preventDefault();
        });

        table.on('select', function (e, dt, type, indexes) {
            console.log(indexes);

            if (type === 'row') {
                var data = table.rows(indexes).data().pluck('id');

                // do something with the ID of the selected items
            }

            var counter = table.rows('.selected').data().length;
            if (counter == 0)
            {
                $('.viewButton, .deleteButton, .editButton').addClass('disabled');
            } else if (counter == 1)
            {
                $('.deleteButton, .viewButton, .editButton').removeClass('disabled');

            } else if (counter > 1) {
                $('.viewButton, .editButton').addClass('disabled');
                $('.deleteButton').removeClass('disabled');
            }
        });


        table.on('deselect', function (e, dt, type, indexes) {
            console.log(type);

            var counter = table.rows('.selected').data().length;
            console.log(counter);
            if (counter == 0)
            {
                $('.viewButton, .deleteButton, .editButton').addClass('disabled');
            } else if (counter == 1)
            {
                $('.deleteButton, .viewButton, .editButton').removeClass('disabled');

            } else if (counter > 1) {
                $('.viewButton, .editButton').addClass('disabled');
                $('.deleteButton').removeClass('disabled');
            }


        });

        $('.dt-buttons').addClass('pull-right');
        $('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="className">Category</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

        function makekennel(id,status) {

            $.ajax({
                type: 'post',
                data: {id: id, _method: 'patch','is_kennel':status},
                url: '{{ url("providers/kennelservice")}}' + '/' + id,

                success: function (response) {
                    console.log(response);
                    if (response.success)
                    {
                        table.ajax.reload();
                        $('.deleteButton').addClass('disabled');
                        grown_noti(response.message, 'success');
                    } else
                    {
                        table.ajax.reload();
                        $('.deleteButton').addClass('disabled');
                        grown_noti(response.message, 'danger');
                    }

                },
            });
        }
        function changeServiceType(status){
            var pass_data = {
                 'radio' : status,
            };
            $.ajax({
                url : "setServiceType",
                type : "POST",
                data: pass_data,
                success : function(data) {
                }
            });
        }


    });

</script>
<script type="text/javascript">
$(function () {    
    var pass_data = {};
    $.ajax({
        url : "getServiceType",
        type : "POST",
        data: pass_data,
        success : function(data) {
            if(data.service_type==2)
                $("#forDog").bootstrapSwitch('state', true);
            else if(data.service_type==1)
                $("#forCat").bootstrapSwitch('state', true);
            else if(data.service_type==3){
                $("#forDog").bootstrapSwitch('state', false);
                $("#forCat").bootstrapSwitch('state', false);
            }
            else{
                $("#forDog").bootstrapSwitch('state', true);
                $("#forCat").bootstrapSwitch('state', true);
            }

        }
    });
})
</script>
@endsection
