@extends('layouts.master')
@section('title', trans('text.serviceshow'))

@section('content')
<div class="container">

    <h1>@lang('text.serviceshow') 
        <a href="{{ url('providers/services/' . $service->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Service"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['providers/services', $service->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Service',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">

        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>Category</th><td>{{ $service->category->name }}</td>
                </tr>
                <tr><th> Technician </th>
                <td>
                    @foreach($service->technician as $techs)
                         <a class="btn btn-success btn-xs" href="{{ url('providers/technicians', $techs->id) }}"> {{ $techs->name}}  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                    @endforeach
                </td>
                </tr>
             
                <tr><th> Status </th><td> 
                    @if($service->status) 
                        <label class="label btn-primary"> Active</label> 
                    @else
                        <label class="label btn-danger">Inactive</label>
                    @endif
                </td></tr>
            </tbody>
        </table>

    @if($service->breed)
         <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Breed</th><th>Size</th><th>Price</th><th>Time</th>
                </tr>
             </thead>
             <tbody>   
                @foreach($service->breed as $beeds)

                    <tr> 
                        <td>{{array_get($breeData, $beeds->breed_id) }}</td>
                        <td>{{array_get($size, $beeds->size_id)}}</td>
                        <td>{{$beeds->price}}</td>
                        <td>{{$beeds->servicetime}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    </div>

</div>
@endsection
