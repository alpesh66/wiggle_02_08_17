<a href="{{ url('providers/services/'.$services->sid) }}" title="Show">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<a href="{{ url('providers/services/'.$services->sid.'/edit') }}" title="Edit"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>

<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$services->sid}}" data-model="services" title="Delete" name="deleteButton"><i class="fa fa-trash-o"></i></button>
