@extends('layouts.master')
@section('title', trans('text.servicecreate'))

@section('content')
<div class="container">

    <h1>@lang('text.servicecreate')</h1>
    <hr/>

    {!! Form::open(['url' => 'providers/services', 'class' => 'form-horizontal service_addss', 'files' => true]) !!}

            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select class='form-control' id="getCategoryID" name="category_id"> {{ provider_get_multiple_categories( $categories, old('category_id') )}}</select>
                    <p class="error technician-help-block" style="display:none"></p>
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

           {!! Form::hidden('provider_id', session('provider_id') ) !!}


           <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            {!! Form::label('category_id', 'Breeds', ['class' => 'col-sm-3 control-label']) !!}
                       <div class="col-sm-6 padding-top">
                   <div class="col-sm-12">

                    <input type="checkbox" name="allSelected" class="bootswitch" id="allSelect" checked data-on-text="All" data-off-text="Custom">
                   </div>
                   
                   <div class="col-sm-12 padding-top">
                        
                        <div class="show-check-c">
                            
                            <div class="row" >
                                
                                <div class="col-lg-12" >
                                <div class="row sep-row" style="margin-top: 10px">
                                    <div class="col-sm-2">Breed </div>
                                    <div class="col-sm-2">Size </div>
                                    <div class="col-sm-2">Price </div>
                                    <div class="col-sm-2">Service Time </div>
                                    <div class="col-sm-2"> </div>
                                </div>
                                <div class="sep-row">
                                @if(is_array(old('breed')) && count(old('breed') > 0) )
                                    {{-- */$x=0;/* --}}
                                    @foreach(old('breed') as $oldBreed)
                                        
                                        <div class="row pluscount">
                                        <div class="col-sm-2 image-select-img">
                                            {{-- <select id="filter_header" name="breed[breed_id][]" class="form-control new-select" v-model="event.breednew">
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select> --}}
                                            {!! Form::select('breed[{{$x}}][breed_id]', $breed, null, ['id'=>'filter_header','class' => 'form-control new-select breed_id']) !!}
                                                {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2 image-select-img">
                                             {!! Form::select('breed[0][size_id]', $size, null, ['class' => 'form-control size_id size_select new-select']) !!}
                                             {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="number" name="breed[0][price]" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                           {{--  <select class="form-control new-select" >
                                              <option disabled selected>Time</option>
                                              
                                            </select> --}}
                                            {!! Form::select('breed[0][servicetime]',$range, null, ['class' => 'form-control new-select']) !!}
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="add_service_box btn btn-success"> <i class=" fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    {{-- */$x++;/* --}}
                                    @endforeach
                                @endif
                                    <div class="row pluscount">
                                        <div class="col-sm-2 image-select-img">
                                            {{-- <select id="filter_header" name="breed[breed_id][]" class="form-control new-select" v-model="event.breednew">
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select> --}}
                                            {!! Form::select('breed[0][breed_id]', $breed, null, ['id'=>'filter_header','class' => 'form-control new-select breed_id']) !!}
                                                {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2 image-select-img">
                                             {!! Form::select('breed[0][size_id]', $size, null, ['class' => 'form-control size_id size_select new-select']) !!}
                                             {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="number" name="breed[0][price]" placeholder="Price" class="form-control price_value">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                           {{--  <select class="form-control new-select" >
                                              <option disabled selected>Time</option>
                                              
                                            </select> --}}
                                            {!! Form::select('breed[0][servicetime]',$range, null, ['class' => 'form-control new-select']) !!}
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="add_service_box btn btn-success"> <i class=" fa fa-plus"></i></button>
                                        </div>
                                    </div><!--Row End-->
                                    <div class="servicebox"></div>
                                    <div class="error"></div>
                                   
                                 
                                </div><!--Sep-row-->
                                </div>
                                
                            </div>
                            <!--<br><span>Checked names: @{{ ChooseCategory | json }}</span>-->
                        </div>
                        
                   </div>
                </div>

            </div>   
        
            
           
            <div class="form-group price_block {{ $errors->has('price') ? 'has-error' : ''}}">
                {!! Form::label('price', 'Price *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('price', null, ['class' => 'form-control ']) !!}
                    <p class="price_help-block" style="display:none"> </p> 
                    {!! $errors->first('price', '<p class="help-block"></p>') !!}
                </div>
            </div>
             <div class="form-group service_time_block  {{ $errors->has('servicetime') ? 'has-error' : ''}}">
                {!! Form::label('servicetime', 'Service Time *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('servicetime',$range, null, ['class' => 'form-control']) !!}

                    {!! $errors->first('servicetime', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('technicians') ? 'has-error' : ''}}">
                {!! Form::label('technicians', 'Technicians *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('technicians[]',$technicians, null, ['class' => 'form-control technicians', 'multiple']) !!}

                    {!! $errors->first('technicians', '<p class="help-block">:message</p>') !!}
                    <div class="technicianss_error"></div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Active', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::button('Create', ['class' => 'btn btn-primary add_services']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}
    <div class="hide">
        {!! Form::select('allBreeds', $breed, null, ['id'=>'allBreeds']) !!}
        {!! Form::select('allSizes', $size, null, ['id' => 'allSizes']) !!}
        {!! Form::select('allService',$range, null, ['id' => 'allServices']) !!}
    </div>
</div>
@endsection

@section('jqueries')
<script type="text/javascript">

    $(document).ready(function(){

        $('.add_services').click(function(){

            var techs = $('#getCategoryID').val();
            var checkUniqueService = $.ajax({
                url: "{{url('providers/services/checkService')}}",
                type: 'post',
                data: {technician: techs},
                async: false,
                success: function(data){
                    if(data.status){
                        $('.technician-help-block').show().text('This Category exists');
                        $('.technician-help-block').css('color','#a94442');
                        setTimeout(function(){
                            $('.technician-help-block').slideUp();
                        }, 4000);
                        return data.status;
                    }
                }

            });
            var whatResult = $.parseJSON(checkUniqueService.responseText);
            if(whatResult.status){
                return false;
            }

            var defaultSet = $('#allSelect').bootstrapSwitch('state');
            if(defaultSet){
                $priceValueData = $('#price').val();
                if($priceValueData == ''){
                    $('.price_help-block').show().text('Price must not be empty.');
                    $('.price_help-block').css('color','#a94442');
                    return false;
                } else if ($('.technicians').val() == null) {
                   $('.error').text('');
                   $('.technicianss_error').text('Please select technicians.');
                   $('.technicianss_error').css('color','#a94442');
                   var res = false;
                   return false;
                }

                $('.service_addss').submit();
            }
            var breed_services = [];    
            var service_id = [];
            var breed_id = [];
            $('.breed_id').each(function(){
                var com = [];
                breed_services.push({
                    'breed_id': $(this).val(),
                    'service_id': $(this).parent().next().find('.size_select').val(),
                });                   
                
                breed_id.push($(this).val());
                service_id.push($(this).parent().next().find('.size_select').val());
            });

            var checkcombination = breed_services.filter((thing, index, self) => self.findIndex((t) => {return t.breed_id === thing.breed_id && t.service_id === thing.service_id; }) === index)



             
             if(checkcombination.length != breed_services.length) {
                $('.error').text('Pet and size are not unique.');
                $('.error').css('color','#a94442');
                 var res = false;
                return false;
             } else if ($('.technicians').val() == null) {
                $('.error').text('');
                $('.technicianss_error').text('Please select technicians.');
                $('.technicianss_error').css('color','#a94442');
                 var res = false;
                return false;
             } else{
                $('.error').text('');
                $('.technicianss_error').text('');
                var res = false;

                $('.price_value').each(function(index,value){
                   // res.push($(this).val());
                    if($(this).val() == '') {
                        $('.error').text('Price can not be empty.');                       
                        $('.error').css('color','#a94442');
                        res = false;
                    } else {
                        res = true;
                    }
                });
                
                
             }
            if(res) {
                
                $('.service_addss').submit();   
            }
             
            
        });

        $('.sep-row').hide();
                
        $('.bootswitch').bootstrapSwitch();
        $('.technicians').select2({
              placeholder: "Choose Technicians",
              allowClear: true
        });

        $('#getCategoryID').on('change',function(){
            var type = $('#getCategoryID :selected').data('type');
        });
$('body').on('change','.breed_id',function(){
    
    if($(this).val() == 1) {
        $(this).parent().next().find('option:last').hide();
        $(this).parent().next().find('option:first').attr('selected','selected');
        $(this).parent().next().find('.size_select').val('1');
//        $(this).parent().next().find('option:nth-child(3)').hide();
    } else {
        $(this).parent().next().find('option:first').attr('selected','selected');
        $(this).parent().next().find('option:last').show();
        $(this).parent().next().find('.size_select').val('1');
//        $(this).parent().next().find('option:nth-child(3)').show();
    }
});
       
$('.breed_id').each(function(index,value){
    
    if($(this).val() == 1) {
        $(this).parent().next().find('option:last').hide();
        $(this).parent().next().find('.size_select').val('1');
//        $(this).parent().next().find('option:nth-child(3)').hide();
    } else {
        $(this).parent().next().find('.size_select').val('1');
        $(this).parent().next().find('option:last').show();
//        $(this).parent().next().find('option:nth-child(3)').show();
    }
});
       
        $('.add_service_box').click(function(){
            var i = $('.pluscount').length;
                var servicehtml = "<div class='row pluscount'><div class='col-sm-2 image-select-img'><select id=filter_header class='form-control new-select breed_id' name=breed["+i+"][breed_id]>"+$('#allBreeds').html()+"</select></div><div class='col-sm-2 image-select-img'><select class='form-control new-select size_select' name=breed["+i+"][size_id] >"+$('#allSizes').html()+"</select></div><div class=col-sm-2><input type=number name=breed["+i+"][price] placeholder=Price class='form-control price_value'></div><div class='col-sm-2 image-select-img'><select class=form-control new-select name=breed["+i+"][servicetime]>"+$('#allServices').html()+"</select></div><div class=col-sm-2><button type=button class='delete_size btn btn-success'> <i class= 'fa fa-trash'></i></button></div></div>";
              $('.servicebox').before(servicehtml); 

              if($('.breed_id:last').val() == 1) {
                    $('.breed_id:last').parent().next().find('option:last').hide();
                    $('.breed_id:last').parent().next().find('option:first').attr('selected','selected');
//                    $('.breed_id:last').parent().next().find('option:nth-child(3)').hide();
                } else {
                    $('.breed_id:last').parent().next().find('option:last').show();
//                    $('.breed_id:last').parent().next().find('option:nth-child(3)').show();
                }                                         
        });
        $('input[name="allSelected"]').on('switchChange.bootstrapSwitch', function(event, state) {
            if(state == true) {
                $('.sep-row').hide();
                $('.service_time_block').show();
                $('.price_block').show();
            } else {
                $('.sep-row').show();
                $('.service_time_block').hide();
                $('.price_block').hide();
                
            }
        });
        $('body').on('click','.delete_size',function(){
            $(this).parent().parent().remove();
        });


    });
</script>
@endsection