@extends('layouts.master')
@section('title', trans('text.serviceedit'))

@section('content')
<div class="container">

    <h1>@lang('text.serviceedit') </h1>

    {!! Form::model($service, [
        'method' => 'PATCH',
        'url' => ['providers/services', $service->id],
        'class' => 'form-horizontal service_addss',
        'files' => true
    ]) !!}
{{-- {{dump($service)}} --}}
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">

                    <select class='form-control' id="getCategoryID" name="category_id"> {{ provider_get_multiple_categories( $categories, $service->category_id )}}</select>
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                    <p class="error technician-help-block" style="display:none"></p>
                </div>
            </div>
             {!! Form::hidden('provider_id', session('provider_id') ) !!}
            {{-- <div class="form-group {{ $errors->has('breed_id') ? 'has-error' : ''}}">
                {!! Form::label('breed_id', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('breed_id', $breed, $service->breed_id, ['class' => 'form-control']) !!}
                    {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('size_id') ? 'has-error' : ''}}">
                {!! Form::label('size_id', 'Size', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('size_id', $size, $service->size_id , ['class' => 'form-control']) !!}
                    {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                {!! Form::label('price', 'Price', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('servicetime') ? 'has-error' : ''}}">
                {!! Form::label('servicetime', 'Servicetime', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('servicetime',$range, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('servicetime', '<p class="help-block">:message</p>') !!}
                </div>
            </div> --}}
            <div class="form-group">
            <label class="col-sm-3 control-label">Breed</label>
            {{-- <div class="col-sm-9"> --}}
                    {{-- <button type="button" class="add_service_box btn btn-success"> <i class=" fa fa-plus"></i></button> --}}
            {{-- </div> --}}
            <div class="col-sm-6 padding-top">
                <div class="col-sm-12 padding-top">
                            
                            <div class="show-check-c">
                                
                                <div class="row" >
                                    
                                    <div class="col-lg-12" >
                                    <div class="row" style="margin-top: 10px">
                                    <div class="col-sm-2">Breed </div>
                                    <div class="col-sm-2">Size </div>
                                    <div class="col-sm-2">Price </div>
                                    <div class="col-sm-2">Service Time </div>

                                    <div class="col-sm-2"> </div>
                                </div>
                                <div class="sep-row">
                                {{-- */$x=0;/* --}}
                         <button type="button" class="add_service_box btn btn-success fixAdd"> <i class=" fa fa-plus"></i></button>
            @foreach($service->breed as $singlebreeds)

                <div class="row pluscount">

                
                	<div class="col-sm-2 image-select-img">
                   {{-- <div class="form-group {{ $errors->has('breed_id') ? 'has-error' : ''}}">--}}
                        {{--{!! Form::label('breed_id', 'Breed', ['class' => 'col-sm-3 control-label']) !!}--}}
                        
                            {{-- {!! Form::select('breed_id', $breed, $service->breed_id, ['class' => 'form-control']) !!} --}}
                            <select name="breed[{{$x}}][breed_id]" class="form-control new-select breed_id">
                                @foreach($breed as $b_id => $breeds)
                                    <option value="{{$b_id}}" @if($b_id == $singlebreeds['breed_id']) selected=selected @endif>{{$breeds}}</option>
                                @endforeach
                            </select>

                            {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                        
                    {{--</div>--}}
                    </div>
                   

					<div class="col-sm-2 image-select-img">
                            {{-- {!! Form::select('size_id', $size, $service->size_id , ['class' => 'form-control']) !!} --}}
                            <select name="breed[{{$x}}][size_id]" class="form-control new-select size_select">

                                @foreach($size as $sid => $sizes)

                                    <option value="{{$sid}}" @if($sid == $singlebreeds['size_id']) selected=selected @endif >{{$sizes}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}

                        </div>
                 
                     <div class="col-sm-2">

                            {{-- {!! Form::text('price', null, ['class' => 'form-control']) !!} --}}
                            <input type="number" name="breed[{{$x}}][price]" class="form-control price_value" placeholder="Price" value="{{ $singlebreeds['price']}}">

                            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}

                    </div>
                    
                    <div class="col-sm-2 image-select-img">
                    {{-- {{ dump($range) }} --}}

                            {{-- {!! Form::select('servicetime',$range, null, ['class' => 'form-control']) !!} --}}
                            <select name="breed[{{$x}}][servicetime]" class="form-control new-select">

                                @foreach($range as $ranges)
                                    <option value="{{$ranges}}" @if($ranges == $singlebreeds['servicetime']) selected=selected @endif >{{$ranges}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('servicetime', '<p class="help-block">:message</p>') !!}
                        
                    </div>
                    
                    
                    <div class="col-sm-2">
                    	<button type=button class='delete_size btn btn-success'> <i class= 'fa fa-trash'></i></button>
                    </div>


                    <br/>
                </div>

                {{-- */$x++;/* --}}
            @endforeach
            <div class="servicebox"></div>
                                    <div class="error"></div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

            <div class="form-group {{ $errors->has('technicians') ? 'has-error' : ''}}">
                {!! Form::label('technicians', 'Technicians *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('technicians[]',$technicians, null, ['class' => 'form-control technicians', 'multiple']) !!}

                    {!! $errors->first('technicians', '<p class="help-block">:message</p>') !!}
                    <div class="technicianss_error"></div>
                </div>
            </div>
           <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Active', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" @if($service->status) checked="checked" @endif data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        <div class="form-group">
           <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::button('Update', ['class' => 'btn btn-primary add_services']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}

</div>
 <div class="hide">
        {!! Form::select('allBreeds', $breed, null, ['id'=>'allBreeds']) !!}
        {!! Form::select('allSizes', $size, null, ['id' => 'allSizes']) !!}
        {!! Form::select('allService',$range, null, ['id' => 'allServices']) !!}
    </div>
@endsection

@section('jqueries')
<script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();
        $('.technicians').select2({
          placeholder: "Choose Technicians",
          allowClear: true
        }).val([{{ $selected_techs }}]).trigger("change");
        

        $('.add_services').click(function(){

            var techs = $('#getCategoryID').val();
            var checkUniqueService = $.ajax({
                url: "{{url('providers/services/checkService')}}",
                type: 'post',
                data: {technician: techs, service_id: {{$service->id}}},
                async: false,
                success: function(data){
                    if(data.status){

                        $('.technician-help-block').show().text('This Category exists');
                        $('.technician-help-block').css('color','#a94442');
                        setTimeout(function(){
                            $('.technician-help-block').slideUp();
                        }, 4000);
                        return data.status;
                    }
                }

            });
            var whatResult = $.parseJSON(checkUniqueService.responseText);
            if(whatResult.status){
                $('.technician-help-block').show().text('This Category exists');
                        $('.technician-help-block').css('color','#a94442');
                return false;
            }

            
            var breed_services = [];    
            var service_id = [];
            var breed_id = [];
            $('.breed_id').each(function(){
                var com = [];
                breed_services.push({
                    'breed_id': $(this).val(),
                    'service_id': $(this).parent().next().find('.size_select').val(),
                });                   
                
                breed_id.push($(this).val());
                service_id.push($(this).parent().next().find('.size_select').val());
            });

            var checkcombination = breed_services.filter((thing, index, self) => self.findIndex((t) => {return t.breed_id === thing.breed_id && t.service_id === thing.service_id; }) === index)

            // console.log(checkcombination);
             
             if(checkcombination.length != breed_services.length) {
                $('.error').text('Pet and size are not unique.');
                $('.error').css('color','#a94442');
                
                return false;
             } else if ($('.technicians').val() == null) {
                $('.error').text('');
                $('.technicianss_error').text('Please select technicians.');
                $('.technicianss_error').css('color','#a94442');
                return false;
             } else{
                $('.error').text('');
                $('.technicianss_error').text('');
                var res = false;

                $('.price_value').each(function(index,value){
                   // res.push($(this).val());
                    if($(this).val() == '') {
                        $('.error').text('Price can not be empty.');                       
                        $('.error').css('color','#a94442');
                        res = false;
                    } else {
                        res = true;
                    }
                });
                
                
             }
             
            if(res) {
                $('.service_addss').submit();   
            }
             
            
        });

        function checkBreedSize() {
            $('#size').val(1);
    if($('.breed_id').val() == 1) {
    $('#size option:last').hide();
//    $('#size option:nth-child(3)').hide();

    } else {
         $('#size option:last').show();
//        $('#size option:nth-child(3)').show();
    }
}
$('body').on('change','.breed_id',function(){

    if($(this).val() == 1) {
        $(this).parent().next().find('option:last').hide();
        $(this).parent().next().find('option:first').attr('selected','selected');
//        $(this).parent().next().find('option:nth-child(3)').hide();
    } else {
        $(this).parent().next().find('option:last').show();
        $(this).parent().next().find('option:first').attr('selected','selected');
//        $(this).parent().next().find('option:nth-child(3)').show();
    }
});

//checkBreedSize();
$('.breed_id').each(function(index,value){
    
    if($(this).val() == 1) {
        $(this).parent().next().find('option:last').hide();
//        $(this).parent().next().find('option:nth-child(3)').hide();
    } else {
        $(this).parent().next().find('option:last').show();
//        $(this).parent().next().find('option:nth-child(3)').show();
    }
});
// $('.breed_id').change(function(){
//     checkBreedSize();
// });
         $('.add_service_box').click(function(){
            var i = $('.pluscount').length;
                var servicehtml = "<div class='row pluscount'><div class='col-sm-2 image-select-img'><select id=filter_header class='form-control new-select breed_id' name=breed["+i+"][breed_id]>"+$('#allBreeds').html()+"</select></div><div class='col-sm-2 image-select-img'><select class='form-control new-select size_select' name=breed["+i+"][size_id] >"+$('#allSizes').html()+"</select></div><div class=col-sm-2><input type=number name=breed["+i+"][price] placeholder=Price class='form-control price_value'></div><div class='col-sm-2 image-select-img'><select class=form-control new-select name=breed["+i+"][servicetime]>"+$('#allServices').html()+"</select></div><div class=col-sm-2><button type=button class='delete_size btn btn-success'> <i class= 'fa fa-trash'></i></button></div></div>";
                $('.servicebox').before(servicehtml);  

                if($('.breed_id:last').val() == 1) {
                    $('.breed_id:last').parent().next().find('option:last').hide();
                    $('.breed_id:last').parent().next().find('option:first').attr('selected','selected');
//                    $('.breed_id:last').parent().next().find('option:nth-child(3)').hide();
                } else {
                    $('.breed_id:last').parent().next().find('option:last').show();
//                    $('.breed_id:last').parent().next().find('option:nth-child(3)').show();
                }

        });

         $('body').on('click','.delete_size',function(){
            $(this).parent().parent().remove();
        });


    });
</script>
@endsection