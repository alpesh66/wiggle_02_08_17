@extends('layouts.master')
@section('title', trans('text.servicecreate'))

@section('content')
<div class="container">

    <h1>@lang('text.servicecreate')</h1>
    <hr/>

    {!! Form::open(['url' => 'providers/services', 'class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select class='form-control' id="getTechnicians" name="category_id"> {{ provider_get_multiple_categories( $categories, old('category_id') )}}</select>
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
           {!! Form::hidden('provider_id', session('provider_id') ) !!}
            <div class="form-group {{ $errors->has('breed_id') ? 'has-error' : ''}}">
                {!! Form::label('breed_id', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('breed_id[]', $breed, null, ['class' => 'form-control breed_id', 'multiple']) !!}
                    {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('size_id') ? 'has-error' : ''}}">
                {!! Form::label('size_id', 'Size', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('size_id[]', $size, null, ['class' => 'form-control size_id', 'multiple']) !!}
                    {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                {!! Form::label('price', 'Price *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('servicetime') ? 'has-error' : ''}}">
                {!! Form::label('servicetime', 'Service Time *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('servicetime',$range, null, ['class' => 'form-control']) !!}

                    {!! $errors->first('servicetime', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('technicians') ? 'has-error' : ''}}">
                {!! Form::label('technicians', 'Technicians *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('technicians[]',$technicians, null, ['class' => 'form-control technicians', 'multiple']) !!}

                    {!! $errors->first('technicians', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Active', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
<script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();
        $('.technicians').select2({
              placeholder: "Choose Technicians",
              allowClear: true
        });

        $('#getTechnicians').on('change',function(){

            var type = $('#getTechnicians :selected').data('type');
            console.log(type);
        })
    });
</script>
@endsection