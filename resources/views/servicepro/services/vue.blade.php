@extends('layouts.master')
@section('title', trans('text.servicecreate'))

@section('content')
<div class="container">

    <h1>@lang('text.servicecreate')</h1>
    <hr/>
    

<!--  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <a class="navbar-brand"><i class="glyphicon glyphicon-bullhorn"></i> Vue</a>
    </div>
  </nav>
  <div class="container" id="events">
    <div class="col-sm-7">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3>Heading</h3>
        </div>
        <div class="panel-body">
          <div>
            <input class="form-control" placeholder="Event Title" v-model="event.title">
            <textarea class="form-control" placeholder="Event Details" v-model="event.detail"></textarea>
            <input type="date" class="form-control" placeholder="Event Date" v-model="event.date">
            <button class="btn btn-primary" v-on:click="addEvent">Add Event</button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-5">
      <div class="list-group">
        <a href="#" class="list-group-item" v-for="event in events">
          <h4 class="list-group-item-heading"><i class="glyphicon glyphicon-bullhorn"></i> @{{ event.title }}</h4>
          <h5><i class="glyphicon glyphicon-calendar" v-if="event.date"></i> @{{ event.date }}</h5>
          <p class="list-group-item-text" v-if="event.detail">@{{ event.detail }}</p>
          <button class="btn btn-xs btn-danger" v-on:click="deleteEvent($index)">Add Event Two</button>
        </a>
      </div>
    </div>
  </div>
  -->
  
  
  
<div id="events">
<!--    {!! Form::open(['url' => 'providers/services', 'class' => 'form-horizontal', 'files' => true]) !!}-->

<div class="form-horizontal image-select-img">
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select class='form-control new-select' name="category_id"> {{ provider_get_multiple_categories( $categories, old('category_id') )}}</select>
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
           {!! Form::hidden('provider_id', session('provider_id') ) !!}
            <div class="form-group {{ $errors->has('breed_id') ? 'has-error' : ''}}" >
                {!! Form::label('breed_id', 'Breed', ['class' => 'col-sm-3 control-label']) !!}
                
                <div class="col-sm-6 padding-top">
                   <div class="col-sm-12">
                        <input type="checkbox" v-model="event.custom"> @{{event.custom ? "All" : "Custom"}}                        
                   </div>
                   
                   <div class="col-sm-12 padding-top">
                        
                        <div class="show-check-c" v-show="!event.custom">
                        	
                            <div class="row" >
                            	
                                <div class="col-lg-12" v-model="event.allonerow">
                                	<div class="sep-row">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="filter_header" name="breed" class="form-control new-select" v-model="event.breednew">
                                              <option disabled selected>Breed</option>
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.sizenew">
                                              <option disabled selected>Size</option>
                                              <option>All</option>
                                              <option>L</option>
                                              <option>M</option>
                                              <option>S</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="text" v-model="event.pricenew" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.timenew">
                                              <option disabled selected>Time</option>
                                              <option>5</option>
                                              <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                        	<a href="#"><button class="btn btn-success"> <i class=" fa fa-plus"></i></button></a>
                                        </div>
                                    </div><!--Row End-->
                                    
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="filter_header" name="breed" class="form-control new-select" v-model="event.breednew1">
                                              <option disabled selected>Breed</option>
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.sizenew1">
                                              <option disabled selected>Size</option>
                                              <option>All</option>
                                              <option>L</option>
                                              <option>M</option>
                                              <option>S</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="text" v-model="event.pricenew1" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.timenew1">
                                              <option disabled selected>Time</option>
                                              <option>5</option>
                                              <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                        	<a href="#"><button class="btn btn-success"> <i class=" fa fa-plus"></i></button></a>
                                        </div>
                                    </div><!--Row End-->
                                    
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="filter_header" name="breed" class="form-control new-select" v-model="event.breednew2">
                                              <option disabled selected>Breed</option>
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.sizenew2">
                                              <option disabled selected>Size</option>
                                              <option>All</option>
                                              <option>L</option>
                                              <option>M</option>
                                              <option>S</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="text" v-model="event.pricenew2" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.timenew2">
                                              <option disabled selected>Time</option>
                                              <option>5</option>
                                              <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                        	<a href="#"><button class="btn btn-success"> <i class=" fa fa-plus"></i></button></a>
                                        </div>
                                    </div><!--Row End-->
                                    
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="filter_header" name="breed" class="form-control new-select" v-model="event.breednew3">
                                              <option disabled selected>Breed</option>
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.sizenew3">
                                              <option disabled selected>Size</option>
                                              <option>All</option>
                                              <option>L</option>
                                              <option>M</option>
                                              <option>S</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="text" v-model="event.pricenew3" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.timenew3">
                                              <option disabled selected>Time</option>
                                              <option>5</option>
                                              <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                        	<a href="#"><button class="btn btn-success"> <i class=" fa fa-plus"></i></button></a>
                                        </div>
                                    </div><!--Row End-->
                                    
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <select id="filter_header" name="breed" class="form-control new-select" v-model="event.breednew5">
                                              <option disabled selected>Breed</option>
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.sizenew5">
                                              <option disabled selected>Size</option>
                                              <option>All</option>
                                              <option>L</option>
                                              <option>M</option>
                                              <option>S</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="text" v-model="event.pricenew5" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <select class="form-control new-select" v-model="event.timenew5">
                                              <option disabled selected>Time</option>
                                              <option>5</option>
                                              <option>10</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                        	<a href="#"><button class="btn btn-success"> <i class=" fa fa-plus"></i></button></a>
                                        </div>
                                    </div><!--Row End-->
								</div><!--Sep-row-->
                                </div>
                            	
                            </div>
                        
                        
                           
                            
                            <!--<br><span>Checked names: @{{ ChooseCategory | json }}</span>-->
                        </div>
                        
                   </div>
                   
                   
                   
                   
                   
                   
                
                
                </div>
            </div>
            
            
             <div class="form-group {{ $errors->has('technicians') ? 'has-error' : ''}}">
                {!! Form::label('technicians', 'Technicians *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('technicians[]',$technicians, null, ['class' => 'form-control technicians', 'multiple']) !!}

                    {!! $errors->first('technicians', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Active', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                
                 <button class="btn btn-primary" v-on:click="addEvent">demo</button>
                
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
</div>
<!--    {!! Form::close() !!}-->
    </div>
</div>
@endsection

@section('jqueries')
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>    
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();
        $('.technicians').select2({
          placeholder: "Choose Technicians",
          allowClear: true
        });
        
    });
    
        
  new Vue({
      el:'#events',

      data: {
      event: {allonerow:[],breednew:[],sizenew:[],pricenew:[],timenew:[],custom: true,
      clbreed:"1",
      casizes: [{'name' : 'sizeAll',  'status': true}],
      
      /*clsize_type: "L", clprice: "",clselected: "5",
      
      cmsize_type: "M", cmprice: "",cmselected: "5",
      
      cssize_type: "S", csprice: "",csselected: "5",*/
      
      clsize_type: ["2","3","4"], clprice: [],clselected:["5","5","5"],
      
      cmsize_type: "M", cmprice: "",cmselected: "5",
      
      cssize_type: "S", csprice: "",csselected: "5",

      clsize:[],cmsize:[],cssize:[]
      
        /*selected: '1',
        options: [{ text: 'Cat', value: '1' }, { text: 'Dog', value: '2' },],
         checkedNames: ["1"],
         checkedNamesCat: [],
         checkedNamesOther: [],*/
         /*
         ChooseCategoryDog:[],
         boxes: [{'name' : 'zero',  'status': true}],
         boxesCat: [{'name' : 'zero',  'status': true}],
         
         selected: 'A',
        options: [{ text: 'One', value: 'A' }, { text: 'Two', value: 'B' },{ text: 'Three', value: 'C' }],
         */
      },

        events: []/*,
        
        cat:["checked: true",""]*/
      },
  
    methods: {

    

    addEvent: function () {
      if (this.event) {
          
        
        // this.events.push(this.event);
        // this.event = { title: '', detail: '', date: '' };
        
        this.event.allonerow.push(this.event.breednew,this.event.sizenew,this.event.pricenew,this.event.timenew);
        
        this.event.clsize.push(this.event.clbreed, this.event.casizes[0].status, this.event.clsize_type[0], this.event.clprice[0], this.event.clselected[0] );
 
        /*this.event.clbreed = '';
        this.event.casizes[0].status= '';
        this.event.clsize_type = '';
        this.event.clprice = '';
        this.event.clselected = '';*/
        
        this.event.cmsize.push(this.event.clbreed, this.event.casizes[0].status, this.event.clsize_type[1], this.event.clprice[1], this.event.clselected[1] );
 
        /*this.event.clbreed = '';
        this.event.casizes[0].status= '';
        this.event.cmsize_type = '';
        this.event.cmprice = '';
        this.event.cmselected = '';*/
        
        this.event.cssize.push(this.event.clbreed, this.event.casizes[0].status, this.event.clsize_type[2], this.event.clprice[2], this.event.clselected[2] );
 
        /*this.event.clbreed = '';
        this.event.casizes[0].status= '';
        this.event.cssize_type = '';
        this.event.csprice = '';
        this.event.csselected = '';
        */
        this.$http.post('providers/services/store', this.event)

          .success(function (res) {
            this.events.push(this.event);
            console.log('Event added!');
          })
          
    
          .error(function (err) {
            console.log(err);
          });
          
          
          
          
      }
    }
    
    
    }
      
      
    });
</script>
@endsection



@extends('layouts.master')
@section('title', trans('text.servicecreate'))

@section('content')
<div class="container">

    <h1>@lang('text.servicecreate')</h1>
    <hr/>

    {!! Form::open(['url' => 'providers/services', 'class' => 'form-horizontal', 'files' => true]) !!}

            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category Id', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select class='form-control' id="getTechnicians" name="category_id"> {{ provider_get_multiple_categories( $categories, old('category_id') )}}</select>
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

           {!! Form::hidden('provider_id', session('provider_id') ) !!}


{{ old('allSelected').' got av' }}
{{ dump(old('breed') ) }}
{{-- {{ old('allSelected') or 'Defaultddd value' }} --}}
           <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            {!! Form::label('category_id', 'Breeds', ['class' => 'col-sm-3 control-label']) !!}
                       <div class="col-sm-6 padding-top">
                   <div class="col-sm-12">

                    <input type="checkbox" name="allSelected" class="bootswitch" selected='selected' data-on-text="All" data-off-text="Custom">
                   </div>
                   
                   <div class="col-sm-12 padding-top">
                        
                        <div class="show-check-c">
                            
                            <div class="row" >
                                
                                <div class="col-lg-12" >
                                <div class="sep-row">
                                @if(is_array(old('breed')) && count(old('breed') > 0) )
                                    {{-- */$x=0;/* --}}
                                    @foreach(old('breed') as $oldBreed)
                                        
                                        <div class="row pluscount">
                                        <div class="col-sm-2 image-select-img">
                                            {{-- <select id="filter_header" name="breed[breed_id][]" class="form-control new-select" v-model="event.breednew">
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select> --}}
                                            {!! Form::select('breed[{{$x}}][breed_id]', $breed, null, ['id'=>'filter_header','class' => 'form-control new-select breed_id']) !!}
                                                {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2 image-select-img">
                                             {!! Form::select('breed[0][size_id]', $size, null, ['class' => 'form-control size_id size_select new-select']) !!}
                                             {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="number" name="breed[0][price]" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                           {{--  <select class="form-control new-select" >
                                              <option disabled selected>Time</option>
                                              
                                            </select> --}}
                                            {!! Form::select('breed[0][servicetime]',$range, null, ['class' => 'form-control new-select']) !!}
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="add_service_box btn btn-success"> <i class=" fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    {{-- */$x++;/* --}}
                                    @endforeach
                                @endif
                                    <div class="row pluscount">
                                        <div class="col-sm-2 image-select-img">
                                            {{-- <select id="filter_header" name="breed[breed_id][]" class="form-control new-select" v-model="event.breednew">
                                              <option>Cat</option>
                                              <option>Dog</option>
                                            </select> --}}
                                            {!! Form::select('breed[0][breed_id]', $breed, null, ['id'=>'filter_header','class' => 'form-control new-select breed_id']) !!}
                                                {!! $errors->first('breed_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2 image-select-img">
                                             {!! Form::select('breed[0][size_id]', $size, null, ['class' => 'form-control size_id size_select new-select']) !!}
                                             {!! $errors->first('size_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <input type="number" name="breed[0][price]" placeholder="Price" class="form-control">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                           {{--  <select class="form-control new-select" >
                                              <option disabled selected>Time</option>
                                              
                                            </select> --}}
                                            {!! Form::select('breed[0][servicetime]',$range, null, ['class' => 'form-control new-select']) !!}
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="add_service_box btn btn-success"> <i class=" fa fa-plus"></i></button>
                                        </div>
                                    </div><!--Row End-->
                                    <div class="servicebox"></div>
                                   
                                 
                                </div><!--Sep-row-->
                                </div>
                                
                            </div>
                            <!--<br><span>Checked names: @{{ ChooseCategory | json }}</span>-->
                        </div>
                        
                   </div>
                </div>

            </div>   
        
            
           
            <div class="form-group price_block {{ $errors->has('price') ? 'has-error' : ''}}">
                {!! Form::label('price', 'Price *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('price', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group service_time_block  {{ $errors->has('servicetime') ? 'has-error' : ''}}">
                {!! Form::label('servicetime', 'Service Time *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('servicetime',$range, null, ['class' => 'form-control']) !!}

                    {!! $errors->first('servicetime', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             <div class="form-group {{ $errors->has('technicians') ? 'has-error' : ''}}">
                {!! Form::label('technicians', 'Technicians *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('technicians[]',$technicians, null, ['class' => 'form-control technicians', 'multiple']) !!}

                    {!! $errors->first('technicians', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Active', ['class' => 'col-sm-3 col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}
    <div class="hide">
        {!! Form::select('allBreeds', $breed, null, ['id'=>'allBreeds']) !!}
        {!! Form::select('allSizes', $size, null, ['id' => 'allSizes']) !!}
        {!! Form::select('allService',$range, null, ['id' => 'allServices']) !!}
    </div>
</div>
@endsection

@section('jqueries')
<script type="text/javascript">
    $(document).ready(function(){
        $('.sep-row').hide();
                
        $('.bootswitch').bootstrapSwitch();
        $('.technicians').select2({
              placeholder: "Choose Technicians",
              allowClear: true
        });

        $('#getTechnicians').on('change',function(){
            var type = $('#getTechnicians :selected').data('type');
        });

        $('body').on('change','.size_select',function(){
            var size = $(this).val();
            var breed_id = $(this).parent().prev().find('.breed_id').val();;    
            var response  =  getUniquebreedData(breed_id,size);
            console.log(response);
            
        });

        function getUniquebreedData(breed_id,size_id) {
            console.log(breed_id);
            console.log(size_id);
            var response = '';
            var counter = 0;

            $('.breed_id').each(function(index,value){
                debugger;
                var older_breed_id = $(this).val();       
                var older_selected_size = $(this).parent().next().find('.size_select').val();
                 if(size_id == older_selected_size && breed_id == older_breed_id) {
                     counter++;
                 }  
            });

            if(counter > 1) {
                counter = 0;
                return 'hemant';
            }

           
        }

        $('.add_service_box').click(function(){
            var i = $('.pluscount').length;
                var servicehtml = "<div class='row pluscount'><div class='col-sm-2 image-select-img'><select id=filter_header class='form-control new-select breed_id' name=breed["+i+"][breed_id]>"+$('#allBreeds').html()+"</select></div><div class='col-sm-2 image-select-img'><select class='form-control new-select size_select' name=breed["+i+"][size_id] >"+$('#allSizes').html()+"</select></div><div class=col-sm-2><input type=number name=breed["+i+"][price] placeholder=Price class=form-control></div><div class='col-sm-2 image-select-img'><select class=form-control new-select name=breed["+i+"][servicetime]>"+$('#allServices').html()+"</select></div><div class=col-sm-2><button type=button class='delete_size btn btn-success'> <i class= 'fa fa-trash'></i></button></div></div>";
              $('.servicebox').before(servicehtml);                                          
        });
        $('input[name="allSelected"]').on('switchChange.bootstrapSwitch', function(event, state) {
            if(state == true) {
                $('.sep-row').hide();
                $('.service_time_block').show();
                $('.price_block').show();
            } else {
                $('.sep-row').show();
                $('.service_time_block').hide();
                $('.price_block').hide();
                
            }
        });
        $('body').on('click','.delete_size',function(){
            $(this).parent().parent().remove();
        });


    });
</script>
@endsection