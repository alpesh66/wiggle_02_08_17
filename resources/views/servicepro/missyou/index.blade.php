@extends('layouts.master')
@section('title', 'Miss You Request')

@section('content')

@if(strpos(\Request::path(),'societies/missyou') !==FALSE)
@php $setAction = 'societies/missyou'; @endphp
@else
@php $setAction = 'providers/missyou'; @endphp
@endif

<div class="container">
    <h1>Miss You Request</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="missyou_table">
            <thead>
                <tr>
                    <th> S.No</th>
                    <th> Customer Name </th>
                    <th> Pet</th>
                    <th> Pet Type</th>
                    <!-- <th> Message </th> -->
                    <th> Request Date</th>
                    <th> Replay Date</th>
                    <th> Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        {{--<div class="pagination-wrapper"> {!! $missyou->render() !!} </div>--}}
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reply</h4>
            </div>
            {!! Form::open(['url' => 'servicepro/missyou/reply', 'class' => 'form-horizontal']) !!}
            <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-8">
                    {!! Form::hidden('customer_id', null, ['class' => 'form-control customer_id_replay']) !!}
                    {!! Form::hidden('ids', null, ['class' => 'form-control ids_reply']) !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Message *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'id' => 'contenteditor']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{--@ckeditor('contenteditor')--}}
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="button" class="btn btn-primary submit"><i class="fa fa-refresh fa-spin hide" id="loader"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<link href="{{ asset('vendor/custom/general.css') }}" rel="stylesheet" type="text/css" >
@endsection
@section('jqueries')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<!--<script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>-->
<script type="text/javascript">
    jQuery(document).ready(function () {
        CKEDITOR.replace('contenteditor', {
            //language:'fr',  
            //uiColor:'#AADC6E',
        filebrowserBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=files',
        filebrowserImageBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=images',
        filebrowserFlashBrowseUrl: '/vendor/templateEditor/kcfinder/browse.php?opener=ckeditor&type=flash',
        filebrowserUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=files',
        filebrowserImageUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=images',
        filebrowserFlashUploadUrl: '/vendor/templateEditor/kcfinder/upload.php?opener=ckeditor&type=flash',
            
            //UserFilesPath:'http://wiggle.com/'
            //extraPlugins:'imageuploader'
        });
    });
    /*$('.replyClick').click(function () {
        alert($(this).attr('customer_id')+"  "+$(this).attr('ids'));
        $('.customer_id_replay').val($(this).attr('customer_id'));
        $('.title_reply').val($(this).attr('title'));
        $('.ids_reply').val($(this).attr('ids'));
        CKEDITOR.instances.contenteditor.setData('');
    });*/
    $('#missyou_table').on('click', '.replyClick', function(){
        $('.customer_id_replay').val($(this).attr('customer_id'));
        $('.title_reply').val($(this).attr('title'));
        $('.ids_reply').val($(this).attr('ids'));
    });

    $(document).ready(function(){

    $.fn.dataTable.ext.buttons.reload = {
        text: 'Reload',
        className: 'buttons-alert',
        action: function ( e, dt, node, config ) {

            $('#post').val('');
            dt.ajax.reload();
            $('.viewButton, .deleteButton, .editButton').addClass('disabled');
        }
    };

    $.fn.dataTable.ext.buttons.edit = {
        text: 'Edit',
        className: 'buttons-alert disabled editButton',
        action: function ( e, dt, node, config ) {
            var id = (table.rows('.selected').data().pluck('id'));
            var myfirst = ('{{ url($setAction) }}'+'/'+id[0]+'/edit');

            window.location.href = myfirst;
        }
    };

    $.fn.dataTable.ext.buttons.delete = {
        text: 'Delete',
        className: 'buttons-danger disabled deleteButton',
        action: function ( e, dt, node, config ) {
            var id = (table.rows('.selected').data().pluck('id').toArray());
            
            if(id.length == 0)
            {
                 $('.deleteButton').addClass('disabled');
                 return false;
            }
            var checkonce = confirm('Are you sure you want to delete?');
            if(checkonce)
            {
                deleteData(id);
            }
        }
    };


    $.fn.dataTable.ext.buttons.view = {
        text: 'View',
        className: 'buttons-alert disabled viewButton',
        action: function ( e, dt, node, config ) {
            // alert( dt.rows('.selected').data().length +' row(s) selected' );
            var id = (table.rows('.selected').data().pluck('id'));
            var myfirst = ('{{ url($setAction) }}'+'/'+id[0]);

            window.location.href = myfirst;
        }
    };
       
        var table = $('#missyou_table').DataTable({
            // sDom: 'Brtlip',
            dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'li><'col-sm-7'p>>",
            order: [0, "desc"],
            columnDefs: [ {
                orderable: 'true',
                className: 'select-checkbox',
                targets:   0,
                visible: false

            } ],
            select: {
                style:    'opts',
                selector: 'td'
            },
            // select: true,
            processing: true,
            stateSave: true,
            deferRender: true,
            stateDuration: 30,
        //     buttons: [
        //     {
        //        text: 'Reload',
        //        extend: 'reload'
        //     }, 'excel'
        // ],
            buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
            serverSide: true,  // use this to load only visible columns in table.

            ajax: {
                url: '{{ url($setAction) }}',
                data: function (d) {
                   
                    d.name = $('select[name=name]').val();
                    d.operator = $('select[name=operator]').val();
                    d.post = $('input[name=post]').val();
                },

            },
            columns: [
                { data: 'id', name:'id', "searchable": false},
                { data: 'customer_name', name: 'customer_name' },
                { data: 'pet', name: 'pet' },
                { data: 'petType', name: 'petType',"searchable": false },
                { data: 'created_at', name: 'created_at',"searchable": false},
                { data: 'updated_at', name: 'updated_at',"searchable": false},
                { data: 'action', name: 'action', "searchable": false,"orderable":false},
            ],

        });

    // $('#filter_comparator').change( function() { table.draw(); } );
    //     $('#filter_value').keyup( function() { table.draw(); } );

    $('body').on('submit','.advanceSearch', function(e) {
            table.draw();
            e.preventDefault();
        });

       table.on( 'select', function ( e, dt, type, indexes ) {
        console.log(indexes);

        if ( type === 'row' ) {
            var data = table.rows( indexes ).data().pluck( 'id' );

            // do something with the ID of the selected items
        }

        var counter = table.rows('.selected').data().length;
        if(counter == 0)
        {
            $('.viewButton, .deleteButton, .editButton').addClass('disabled');
        }
        else if(counter == 1)
        {
            $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
            
        } else if(counter > 1){
            $('.viewButton, .editButton').addClass('disabled');
            $('.deleteButton').removeClass('disabled');
        }
    } );


       table.on( 'deselect', function ( e, dt, type, indexes ) {
        console.log(type);
        
        var counter = table.rows('.selected').data().length;
        console.log(counter);
        if(counter == 0)
        {
            $('.viewButton, .deleteButton, .editButton').addClass('disabled');
        }
        else if(counter == 1)
        {
            $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
            
        } else if(counter > 1){
            $('.viewButton, .editButton').addClass('disabled');
            $('.deleteButton').removeClass('disabled');
        }

        
    } );
    $('.dt-buttons').addClass('pull-right');
    $('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="firstname">Customer Name</option> <option value="pets.name">Pet</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');
    function deleteData(id) {
        $.ajax({
            type: 'post',
            data: {id: id, _method: 'delete'},
            url: '{{ url($setAction) }}'+'/'+id,
                
            success:function(response){

                if(response.success)
                {
                    console.log(response.success);
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'success');
                }
                else
                {
                    table.ajax.reload();
                    $('.deleteButton').addClass('disabled');
                    grown_noti(response.message,'danger');
                }
            
            },
        });
    }
    $('.submit').on('click', function () {
        var id = $('.ids_reply').val();
        var status = $('#status').val();
        var description = CKEDITOR.instances['contenteditor'].getData();
        /*alert(id+" "+status+" "+description);*/
        $('.submit').attr("disabled", true);
        $('#loader').removeClass("hide");
        $.ajax({
            type: 'POST',
            url: '{{ url($setAction.'/reply') }}',
            dataType: 'json',
            data: {'id': id, 'customer_id': $('.customer_id_replay').val(), 'status': $('#status').val(), 'title': $('.title_reply').val(), 'description': description},
            success: function (response) {
                $('.submit').attr("disabled", false);
                $('#loader').addClass("hide");
                $('#myModal').modal('toggle');
                CKEDITOR.instances['contenteditor'].setData("")
                $('.replyStatus' + id).css({
                    'background-color':'rgb(1, 174, 242)',
                    'border-color':'rgb(6, 142, 196'
                });
                if ($('#status').val() == 1) {
                    $('.status' + id).html('Approved');
                } else if ($('#status').val() == 2) {
                    $('.status' + id).html('Reject');
                } else {
                    $('.status' + id).html('Pending');
                }
                table.ajax.reload();
                $.bootstrapGrowl(response.message, {type: 'success', offset: {from: 'top', amount: 70}, align: 'right', width: 250, allow_dismiss: false});
            },
            error: function (response) {
                var errors = response.responseJSON;
                $.each(errors, function (index, value) {
                    $('#' + index).parent().parent().removeClass('has-error');
                    $('#' + index).next().remove()
                    $('#' + index).parent().parent().addClass('has-error');
                    $('#' + index).after('<p class="help-block">' + value + '</p>');
                });
            }
        });
    });
    });
</script>
@endsection
