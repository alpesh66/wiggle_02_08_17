@extends('layouts.master')
@section('title', 'Miss You Request')

@section('content')
@if(strpos(\Request::path(),'societies/missyou') !==FALSE)
@php $setAction = 'societies/missyou'; @endphp
@else
@php $setAction = 'providers/missyou'; @endphp
@endif
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1>Miss You Request
                    <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => [$setAction, $missyou->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Miss You Request',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                    {!! Form::close() !!}
                </h1>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <tbody>
                            
                             <tr><th> First Name </th><td> {{ $missyou->customer->firstname }} </td></tr>
                                <tr><th> Middle Name </th><td> {{ $missyou->customer->middlename }} </td></tr>
                                <tr><th> Last Name</th><td> {{ $missyou->customer->lastname }} </td></tr>
                                <tr><th> Email </th><td> {{ $missyou->customer->email }} </td></tr>
                                <tr><th> Gender </th><td> {{ showGender($missyou->customer->gender) }} </td></tr>
                                <tr><th> Date of Birth </th><td> {{ $missyou->customer->dob }} </td></tr>
                                <tr><th> Mobile </th><td> {{ $missyou->customer->mobile }} </td></tr>
                                <tr><th> Address </th><td> {{ $missyou->customer->address }} </td></tr>
                                <tr><th> Area </th><td> {{ $missyou->customer->area_id }} </td></tr>
                                <tr><th> Status </th><td> {{ showStatus($missyou->customer->status) }} </td></tr>
                                <tr><th> Reply Message </th>
                                <td>
                                <?php
                                foreach ($message as $key => $value) {
                                    $k=$key+1;
                                    echo $k.":";
                                    echo htmlspecialchars_decode(stripslashes($value['message'])); 
                                }
                                
                                ?>  </td></tr>
                                <tr><th> Pet Information </th><td> {{ display_pet_name($missyou->pet) }} </td></tr>
                
                        </tbody>
                    </table>
                    <h1>Pet Information</h1>
   <table class="table table-bordered table-striped table-hover" id="customers">
            <thead>

                <tr>
                   
                    <th>Name </th>                    
                    <th>Breed</th>
                    <th>Weight</th>                    
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Microchip</th>
                    <th>Grooming#</th>
                    <th>Upcoming Vaccination</th>
                    <th>Upcoming Grooming</th>
                </tr>
                 <tbody>
                        @foreach($missyou->pet as $item)
                        <tr>
                            <td> {{ $item->name }} </td>
                            <td><?php
                            if($item->breed==1){
                                echo "Cat";
                            }
                            else{
                                echo "Dog";
                            }
                            ?></td>
                            <td> {{ $item->weight }} </td>
                            <td> {{ $item->dob }} </td>
                            <td> {{ showGender($item->gender) }} </td>
                            <td> {{ $item->chipno }} </td>
                            <td> {{ $item->grooming }} </td>
                            <td> {{ $item->veternary }} </td>
                            <td> {{ $item->groom }} </td>
                        </tr>
                        @endforeach   
                </tbody> 
            </thead>
            
        </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('jqueries')
<script type="text/javascript">
    $(document).ready(function () {
        $('.reply_form').css('display', 'none');
        $('.reply').click(function () {
            $('.reply_form').css('display', 'block');
        });
        $('.reply_cancel').click(function () {
            $('.reply_form').css('display', 'none');
        });
    })
</script>
@endsection
