@extends('layouts.master')
@section('title', trans('text.Pet'))
@section('content')
<div class="container">

    <h1>@lang('text.Pet') 
    <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a>
       {{--  <a href="{{ url('pet/' . $pet->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Pet"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['pet', $pet->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Pet',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!} --}}
    </h1>
    {{-- {{dump($pet) }} --}}
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

               {{-- {{dump($pet->toArray())}} --}}
                {{-- @if(!empty($pet->photo)) --}}
                   <tr><th>Photo</th><td>
                   <img src="{{$pet->photo}}" width="5%">
                   {{-- <a href="{{$pet->photo}}" target="_blank" > Click Here</a> --}}
                   </td></tr>
                {{-- @endif --}}
                <tr><th> Name </th><td> {{ $pet->name }} </td></tr>
                <tr><th> Dob </th><td> {{ $pet->dob }} </td></tr>
                <tr><th> Species </th><td> {{ $pet->breeds->name }} </td></tr>
                <tr><th> Gender </th><td>  @if($pet->gender == 0)
                                Female
                                @else
                                Male
                                @endif
                </td></tr>
                {{--<tr><th> Height </th><td> {{ $pet->height }} </td></tr>--}}
                <tr><th> weight </th><td> {{ $pet->weight }} </td></tr>
                <tr><th> Size </th><td> {{ $pet->sizes->name }} </td></tr>
                <tr><th> Temperament </th><td> {{ $pet->temperament }} </td></tr>
                <tr><th> Origin </th><td> {{ $pet->origin }} </td></tr>
                <tr><th> Chip No </th><td> {{ $pet->chipno }} </td></tr>
                <tr><th> Grooming Frequency </th><td> {{ $pet->grooming }} </td></tr>
                <tr><th> Upcoming Vaccination </th><td> <?php echo $pet->next_vaccination_date; ?> </td></tr>
                <tr><th> Upcoming Grooming  </th><td> <?php echo $pet->next_groom_appointment; ?> </td></tr>
             
            </tbody>
        </table>

         <div class="table">
        <table class="table table-bordered table-striped table-hover" id="dataTables">
            <thead>
                <tr>
                    <th>S.No</th><th>Given On</th><th> Veternary </th><th> Institution </th><th> Card </th>
                </tr>
            </thead>            
        </table>

    </div>

    </div>

</div>
@endsection

