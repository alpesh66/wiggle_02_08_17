@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
{{-- <div class="page-title">
  <div class="title_left">
    <h3>Dashboard</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div>
</div> --}}

<div class="clearfix"></div>
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> @lang('text.customers')</span>
    <div class="count">{{$customers}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-circle"></i> @lang('text.services')</span>
    <div class="count">{{$service_count}}</div>

  </div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">

    <span class="count_top"><i class="fa fa-scissors"></i> @lang('text.technicians')</span>
    <div class="count">{{$tech_count}}</div>

  </div>

  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-check-square-o"></i> @lang('text.bookings')</span>
    <div class="count">{{$bookings}}</div>
  </div>
</div>
<div class="row">
    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>@lang('text.newCustomer')</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($customers_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->firstname).' '.$item->lastname }}</a>
              <p>{{ $item->email }}</p>
            </div>
          </article>
          @endforeach            
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>@lang('text.newTechnicians')</h2>          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($tech_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ $item->title.' '.ucfirst($item->name) }}</a>
              <p>{{ $item->number }}</p>
            </div>
          </article>
          @endforeach            
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="x_panel">
        <div class="x_title">
          <h2>New Ratings</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          @foreach($rating_list as $item)
          <article class="media event">
            <a class="pull-left date">
              <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
              <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
            </a>
            <div class="media-body">
              <a href="#" class="title">{{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}</a>
              <p>{{ $item->rate }}</p>
            </div>
          </article>
          @endforeach
        </div>
      </div>
    </div>

  </div>  
@endsection
