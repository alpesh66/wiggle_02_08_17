@extends('layouts.master')
@section('title', 'Update Specialist Login Create')
@section('content')

<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Add Specialist </h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

        <form action='{{ url('providers/techuser/').'/'.$user->id }}' class='form-horizontal form-label-left' method="post" id='' >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PATCH">
            <div class="form-group">
                <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}" required="required" id="name" name="name" value="{{ $user->name}}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="email" id="email" value="{{$user->email}}">
                </div>
            </div>

            <div class="form-group">
                <label for="role" class="control-label col-md-3 col-sm-3 col-xs-12">Specialist<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('technicians', $technicians, $user->technicians, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-success" type="submit">Update</button>
                  <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
                </div>
            </div>
        </form>

          </div>
        </div>
      </div>

</div>
@endsection