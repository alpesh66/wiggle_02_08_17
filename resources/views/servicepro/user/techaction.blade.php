<a href="{{ url('providers/techuser/'.$user->id.'/edit') }}" title="@lang('text.edit')">
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$user->id}}" data-model="users" title="@lang('text.delete')" name="deleteButton"><i class="fa fa-trash-o"></i></button>