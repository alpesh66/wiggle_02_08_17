@extends('layouts.master')
@section('title', 'Admin Create')
@section('content')

<div class="row">
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Add User <small>also Role</small></h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

        <form action='{{ url('providers/user') }}' class='form-horizontal form-label-left' method="post" id='useradd' >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}" required="required" id="name" name="name" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="email" id="email" value="{{ old('email') }}">
                </div>
            </div>
             <div class="form-group">
                <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" required="required" name="password" id="password">
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="control-label col-md-3 col-sm-3 col-xs-12">Role<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  {{ Form::select('roles', $roles, null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button class="btn btn-success" type="submit">Submit</button>
                  <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
                </div>
            </div>
        </form>

          </div>
        </div>
      </div>

</div>
@endsection