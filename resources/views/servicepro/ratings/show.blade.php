@extends('layouts.master')
@section('title', trans('text.showRating'))
@section('content')
<?php $user = \Auth::user() ?> 
<?php
$url = '';
if ($user->can(super_admin_roles())):
    $url = 'ratings';
elseif ($user->can(provider_admin_roles())):
    $url = 'providers/ratings';
elseif ($user->can(society_admin_roles())):
    $url = 'societies/ratings';
endif
?>
<div class="container">

    <h1>Rating 
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['providers/ratings', $rating->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete rating',
                    'onclick'=>'return confirm("Confirm delete?")'
            ));!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $rating->id }}</td>
                </tr>
                <tr><th> Customer </th>
                    <td>
                    @if(isset($item->customer->firstname))
                        {{ $rating->customer->firstname }} 
                    @else
                        Anonymous
                    @endif 
                    </td>
                </tr>
                <tr><th> Rate </th><td> {{ $rating->rate }} </td></tr>
                <tr><th> Review </th><td width="1100"> {{ $rating->review }} </td></tr>
                <tr><th> Status </th>
                    <td>
                        <input type="radio" id="{{ $rating->id }}" name="status" value="1" class="changestatus" {{  ($rating->status == 1)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Public
                        <input type="radio" id="{{ $rating->id }}" name="status" value="0" class="changestatus" {{  ($rating->status == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Private
                    </td>
                </tr>                
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
        <?php $user=\Auth::user() ?> 
    $(document).ready(function(){
        $('.changestatus').click(function(){
            var status = this.value;    
            var id = this.id;
            //console.log(status);
            if(id != ''){
                $.ajax({
                    type:'POST',
                    @if ($user -> can(super_admin_roles()))
                    url: '{{ url("/ratings/changestatus") }}',
                    @elseif($user -> can(provider_admin_roles()))
                    url:'{{ url('/providers/ratings/changestatus') }}',
                    @elseif($user -> can(society_admin_roles()))
                    url:'{{ url('/societies/ratings/changestatus') }}',
                    @endif
                    
                    data:{'id':id,'status':status},
                    success:function(data){
                        var response = jQuery.parseJSON(data);
                        $.bootstrapGrowl(response.message, { type: 'success',offset: {from: 'top', amount: 70},align: 'right',width: 300,allow_dismiss: false}); 
                    },
                    error: function(data){
                        
                    }
                });    
            }                        
        });

        $('.bootswitch').bootstrapSwitch();        
        
    });
    </script>
@endsection    