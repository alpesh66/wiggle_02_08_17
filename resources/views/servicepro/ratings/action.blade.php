<?php $user = \Auth::user() ?> 
<?php
$url = '';
if ($user->can(super_admin_roles())):
    $url = 'ratings/';
elseif ($user->can(provider_admin_roles())):
    $url = 'providers/ratings/';
elseif ($user->can(society_admin_roles())):
    $url = 'societies/ratings/';
endif
?>
<a href="{{ url($url.$ratings->rid) }}" title="@lang('text.view')">
    <button class="btn btn-success btn-xs"> <i class=" fa fa-eye"></i></button>
</a>

<!-- <a href="{{ url('providers/ratings/'.$ratings->id.'/edit') }}" title="@lang('text.edit')"> 
    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a> -->
<button class="btn btn-danger btn-xs deleteAjax" data-id="{{$ratings->rid}}" data-model="users" title="@lang('text.delete')" name="deleteButton"><i class="fa fa-trash-o"></i></button>