@extends('layouts.master')
@section('title', 'Rating Management')
<?php $user = \Auth::user() ?>
@section('content')

@if(strpos(\Request::path(),'societies/ratings') !==FALSE)
  @php $setAction = 'societies/ratings'; @endphp
@elseif(strpos(\Request::path(),'providers/ratings') !==FALSE)
    @php $setAction = 'providers/ratings'; @endphp
@else 
    @php $setAction = 'ratings'; @endphp
@endif
<div class="container">

    <h1>Ratings</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="ratings">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Rating For </th>
                    <th>Customer </th><th> Rating </th><th> Comments </th><th> Status </th><th>Actions</th>
                </tr>
            </thead>
            
        </table>
    </div>

</div>
@endsection

@section('jqueries')

<script>
$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('providers/ratings/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('rid').toArray());
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }        
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


    $.fn.dataTable.ext.buttons.view = {
        text: 'View',
        className: 'buttons-alert disabled viewButton',
        action: function ( e, dt, node, config ) {
            // alert( dt.rows('.selected').data().length +' row(s) selected' );
            var id = (table.rows('.selected').data().pluck('id'));
            var myfirst = ('{{ url("providers/ratings/") }}'+'/'+id[0]);

            window.location.href = myfirst;
        }
    };
    var table = $('#ratings').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url($setAction) }}',
            data: function (d) {
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },
        },
        columns: [
            { data: 'rid', name:'rid', "searchable": false},
            { data: 'rating_for', name: 'rating_for' },
            { data: 'firstname', name: 'firstname' },
            // { data: 'lastname', name: 'lastname' },
            { data: 'rate', name: 'rate' },
            { data: 'review', name: 'review' },
            { data: 'status', name: 'status'},
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });
// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );

$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="firstname">Customer</option><option value="rate">Rate</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');

function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url($setAction) }}'+'/'+id,
            
        success:function(response){
            if(response.success)
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
                /*location.reload();*/
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}

   $('#ratings').on('click', '.deleteAjax', function(e){
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            var id = [];
            id.push($(this).data('id'));
            deleteData(id);
        }
   });

});


</script>
@endsection