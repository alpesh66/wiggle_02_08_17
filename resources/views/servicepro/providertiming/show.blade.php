@extends('layouts.master')

@section('content')
<div class="container">

    <h1>Technician 
        <a href="{{ url('providers/technicians/' . $technician->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit technician"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['technicians', $technician->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete technician',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID</th><td>{{ $technician->id }}</td>
                </tr>
                <tr>
                    <th> Name </th>
                    <td> {{ $technician->name }} </td>
                </tr>
                <tr>
                    <th> Name Ar </th>
                    <td> {{ $technician->name_ar }} </td>
                </tr>
                <tr>
                    <th> Number </th>
                    <td> {{ $technician->number }} </td>
                </tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
