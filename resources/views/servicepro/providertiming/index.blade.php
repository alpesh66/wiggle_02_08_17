@extends('layouts.master')
@section('title', 'Provider Timing')
@section('content')
<div class="container">

    <h1>Provider Timing</h1>    
    <hr/>

   
    {!! Form::model($protiming, [
        'method' => 'PATCH',
        'url' => ['/providers/providertimings/'],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

            <div class="row">
              <div class="col-sm-12">
              <div class="row">
              <div class="work-row-main">
                    <div class="form-group">
                        
                        <div class="col-sm-2">
                        {!! Form::label('Day', 'Day', ['class' => 'time control-label']) !!}
                        </div>
                        
                        <div class="col-sm-4 text-center">
                        {!! Form::label('Start Time', 'Start Time', ['class' => 'control-label']) !!}
                        </div>                   
                        
                        <div class="col-sm-4 text-center">
                        {!! Form::label('End Time', 'End Time', ['class' => 'control-label']) !!}
                        </div>    
                        
                    </div>
                    </div>
                     
              
                  <div class="pro-main-list">
                  
                  <div class="work-row-list">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}" id="protime_{{$protiming[0]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="sunday" class="day" {{  ($protiming[0]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Sunday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[0]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[0]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[0]->id}}" class="btn btn-primary submit">Save</button>
                        </div>                   
                    </div>
                    
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}" id="protime_{{$protiming[1]->id}}">

                        <div class="col-sm-2 day-list">
                             <input type="checkbox" name="pro[day][]" value="monday" class="day" {{  ($protiming[1]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Monday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}                     
                        </div>                    
                        
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[1]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div> 

                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[1]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[1]->id}}" class="btn btn-primary submit">Save</button>
                        </div>                

                        </div>
                        
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}" id="protime_{{$protiming[2]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="tuesday" class="day" {{  ($protiming[2]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Tuesday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>  
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[2]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[2]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                           <button type="button" id="{{$protiming[2]->id}}" class="btn btn-primary submit">Save</button>        
                        </div>                      
                    </div>
                        
                    <div class="form-group" id="protime_{{$protiming[3]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="wednesday" class="day" {{  ($protiming[3]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Wednesday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[3]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[3]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>  
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[3]->id}}" class="btn btn-primary submit">Save</button>              
                        </div>                   
                    </div>
                    
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}" id="protime_{{$protiming[4]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="thursday" class="day" {{  ($protiming[4]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Thursday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[4]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[4]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[4]->id}}" class="btn btn-primary submit">Save</button>                        
                        </div>                    
                    </div>
                    
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}" id="protime_{{$protiming[5]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="friday" class="day" {{  ($protiming[5]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Friday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[5]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[5]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[5]->id}}" class="btn btn-primary submit">Save</button>
                        </div>                    
                    </div>
                    
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}"" id="protime_{{$protiming[6]->id}}">
                        <div class="col-sm-2 day-list">
                            <input type="checkbox" name="pro[day][]" value="saturday" class="day" {{  ($protiming[6]->weekoff == 0)? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No"> Saturday
                            {!! $errors->first('day', '<p class="help-block">:message</p>') !!}
                        </div> 
                        <div class="col-sm-4">
                            {!! Form::text('pro[stime][]', date('H:i', strtotime($protiming[6]->starttime)), ['class' => 'time stime form-control']) !!}
                            {!! $errors->first('stime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-4">
                            {!! Form::text('pro[etime][]', date('H:i', strtotime($protiming[6]->endtime)), ['class' => 'time etime form-control']) !!}
                            {!! $errors->first('etime', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="col-sm-2">
                            <button type="button" id="{{$protiming[6]->id}}" class="btn btn-primary submit">Save</button>
                        </div>                     
                    </div>
                    
                    
                  </div><!--End work-row-list-->
                  
                  </div><!--Pro Main -->
                                      
                  
                  
                  
                  
                                    
                </div><!--Row end-->
                
               
                
                
                
                
                
                
                
               
                
                                  
              </div>   

              <!-- break block col-sm-2 day-list-->
                

        <!-- <div class="form-group text-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div> -->
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();

        // $('.time').bootstrapMaterialDatePicker({ date: false });

        $('.time').bootstrapMaterialDatePicker
            ({
                date: false,
                shortTime: false,
                format: 'HH:mm',
				switchOnClick: true
            });
    });

    $('.submit').on('click', function(){
       var id = this.id;
       
       if(id != ''){
            var stime = $('#protime_'+id+' .stime').val();   
            var etime = $('#protime_'+id+' .etime').val();   
            
            day   = $('#protime_'+id+' .day').is(":checked")
            //console.log(stime+'-'+etime+'-'+day);
            if(day == true){
                var weekoff = '0';
            } else {
                var weekoff = '1';
            }

            $.ajax({
                type:'POST',
                url:'{{ url('/providers/providertimings/updatetime') }}',
                data:{'id':id,'starttime':stime,'endtime':etime,'weekoff':weekoff},
                success:function(data){
                    var response = jQuery.parseJSON(data);
                    $.bootstrapGrowl(response.message, { type: 'success',offset: {from: 'top', amount: 70},align: 'right',width: 250,allow_dismiss: false}); 
                },
                error: function(data){

                }
            });    
       }
       return false;          
    });

    </script>
@endsection