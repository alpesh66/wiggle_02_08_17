@extends('layouts.master')
@section('title', 'Kennel')

@section('content')
@if(strpos(\Request::path(),'societies/kennels') !==FALSE)
  @php $setAction = 'societies/kennels'; @endphp
@else
    @php $setAction = 'providers/kennels'; @endphp
@endif

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h1>Kennel {{ $kennel->id }}
                <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a>
                {{-- <a href="{{ url('societies/kennels/' . $kennel->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit kennel"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a> --}}
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => [$setAction, $kennel->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Kennel',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ));!!}
                {!! Form::close() !!}
            </h1>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>

    
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                <tr><th> First Name </th><td> {{ $kennel->customer->firstname }} </td></tr>
                <tr><th> Middle Name </th><td> {{ $kennel->customer->middlename }} </td></tr>
                <tr><th> Last Name</th><td> {{ $kennel->customer->lastname }} </td></tr>
                <tr><th> Email </th><td> {{ $kennel->customer->email }} </td></tr>
                <tr><th> Gender </th><td> {{ showGender($kennel->customer->gender) }} </td></tr>
                <tr><th> Date of Birth </th><td> {{ $kennel->customer->dob }} </td></tr>
                <tr><th> Mobile </th><td> {{ $kennel->customer->mobile }} </td></tr>
                <tr><th> Address </th><td> {{ $kennel->customer->address }} </td></tr>
                <tr><th> Area </th><td> {{ $kennel->customer->area_id }} </td></tr>
                <tr><th> Status </th><td> {{ showStatus($kennel->customer->status) }} </td></tr>
                <!-- <tr><th> Title </th><td> {{ $kennel->title }} </td></tr> -->
                <tr><th> Description </th>
                <td> 
                <ul>
                @foreach($catname as $item) 
                    <li>{{ $item->name }} </li>
                @endforeach   
                </ul>
                </td></tr>
                <tr><th> Start Date </th><td> {{ $kennel->startdate }} </td></tr>
                <tr><th> End Date </th><td> {{ $kennel->enddate }} </td></tr>
                <tr><th> Pet Information </th><td> {{ display_pet_name($kennel->pet) }} </td></tr>
            </tbody>
        </table>
           <h1>Pet Information</h1>
   <table class="table table-bordered table-striped table-hover" id="customers">
            <thead>

                <tr>
                   
                   <th>Name </th>                    
                    <th>Breed</th>
                    <th>Weight</th>                    
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Microchip</th>
                    <th>Grooming#</th>
                    <th>Upcoming Vaccination</th>
                    <th>Upcoming Grooming</th>
                </tr>
                 <tbody>
                        @foreach($kennel->pet as $item)
                        <tr>
                            <td> {{ $item->name }} </td>
                            <td><?php
                            if($item->breed==1){
                                echo "Cat";
                            }
                            else{
                                echo "Dog";
                            }
                            ?></td>
                            <td> {{ $item->weight }} </td>
                            <td> {{ $item->dob }} </td>
                            <td> {{ showGender($item->gender) }} </td>
                            <td> {{ $item->chipno }} </td>
                            <td> {{ $item->grooming }} </td>
                            <td> {{ $item->veternary }} </td>
                            <td> {{ $item->groom }} </td>
                        </tr>
                        @endforeach   
                </tbody> 
            </thead>
            
        </table>
        
    </div>

</div>
        </div>
      </div>
</div>


@endsection
@section('jqueries')
<script type="text/javascript">
$(document).ready(function(){
    $('.reply_form').css('display','none');
     $('.reply').click(function(){
        $('.reply_form').css('display','block');
     });
    $('.reply_cancel').click(function(){
        $('.reply_form').css('display','none');
    });
})
</script>
@endsection
