<?php //echo "<pre>"; print_r($kennels); die();?>
@extends('layouts.master')
@section('title', 'Kennel')

@section('content')
        
@if(strpos(\Request::path(),'societies/kennels') !==FALSE)
  @php $setAction = 'societies/kennels'; @endphp
@else
    @php $setAction = 'providers/kennels'; @endphp
@endif
<div class="container">
    <h1>Kennel {{-- <a href="{{ url('servicepro/adopts/create') }}" class="btn btn-primary btn-xs" title="Add New Adopt"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a> --}}</h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="kennel">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Customer Name</th>
                    <th>Pet</th>
                    <th>Pet Type</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Title</th>
                    <th>Request Date</th>
                    <th>Reply Date</th>
                    <th>Status</th>
		   {{-- <th>parent_id</th> --}}
		    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($kennels as $item)
            
                {{-- */$x++;/* --}}

                <tr @if($item->is_read == 1 && $item->sender == 1) style="background-color:#808285 !important;color: #fff;" @endif>
                    <td>{{ $x }}</td>
                    <td><a style="color:blue;" href="{{ url('societies/customers/'.$item->customer->id) }}">{{ $item->customer->firstname }} {{ $item->customer->lastname }}</a></td>
                     @if($item->is_read == 1 && $item->sender == 1)
                        <td class="set_pet_name"><i class="fa fa-eye"></i> {{ display_pet_name($item->pet) }} </td>
                    @else
                        <td><i class="fa fa-eye"></i> {{ display_pet_name($item->pet) }} </td>
                    @endif
                    <td>{{ (count($item->pet)>0)?$item->pet[0]->breedname:NULL }}</td>
                    <td>{{ $item->startdate }}</td>
                    <td>{{ $item->enddate }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->created_at  }}</td>
                    <td>{{ ($item->parent_id == 0)?NULL:$item->updated_at}}</td>
                    
                    <td class="status{{ $item->id  }}"> @if($item->status == 1) Approved @elseif($item->status == 2) Reject @else Pending  @endif </td>
                   {{-- <td>{{ $item->parent_id }}</td> --}}
                    <td>
                        <a href="{{ url($setAction .'/'. $item->id) }}" class="btn btn-success btn-xs" title="View Kennel"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        @if($item->parent_id == 0)
                        <button type="button" class="btn btn-primary btn-xs replyClick replyStatus{{$item->id}}" title="{{$item->title}}" ids="{{$item->id}}" customer_id={{$item->customer_id}} data-toggle="modal"  data-target="#myModal" id="replyStatus{{ $item->customer->id  }}" data-id="{{ $item->customer->id  }}" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/> </button>
                        @endif

                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => [$setAction, $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Kennel" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Kennel',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $kennels->render() !!} </div>
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reply</h4>
      </div>
           
       
          
          

    {!! Form::open(['url' => 'servicepro/kennels/reply', 'class' => 'form-horizontal', 'id'=>'request_reply_form']) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-8">
                    {!! Form::hidden('customer_id', null, ['class' => 'form-control customer_id_replay']) !!}
                    {!! Form::hidden('title', null, ['class' => 'form-control title_reply']) !!}
                    {!! Form::hidden('ids', null, ['class' => 'form-control ids_reply']) !!}

                </div>
            </div>
           
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <select name="status" id="status" class="form-control">
                        <option value="0">Pending</option>
                        <option value="1">Approved</option>
                        <option value="2">Reject</option>
                        
                    </select>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
           <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Comments', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="button" class="btn btn-primary submit">Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    {!! Form::close() !!}

          
        
      
        </div>
    </div>
</div>

@endsection
@section('jqueries')
<script type="text/javascript">

$('.replyClick').click(function(){
  $('.customer_id_replay').val($(this).attr('customer_id'));
  $('.title_reply').val($(this).attr('title'));
  $('.ids_reply').val($(this).attr('ids'));
});


$('.submit').on('click', function(){
   var id = $('.ids_reply').val();
   var status = $('#status').val();
   $.ajax({
           type:'POST',
           url:'{{ url($setAction.'/reply') }}',
           data:{'id':id,'customer_id':$('.customer_id_replay').val(),'status':$('#status').val(),'title':$('.title_reply').val(),'description':$('#description').val()},
           success:function(data){
              $('#myModal').modal('toggle');
              $('.replyStatus'+id).css('display','none');
               if($('#status').val() == 1) {
                $('.status'+id).html('Approved');
              } else if($('#status').val() == 2) {
                $('.status'+id).html('Reject');
              } else {
                $('.status'+id).html('Pending');  
              }
              var response = jQuery.parseJSON(data);
              $.bootstrapGrowl(response.message, { type: 'success',offset: {from: 'top', amount: 70},align: 'right',width: 250,allow_dismiss: false});
           },
           error: function(data){
              var errors = data.responseJSON;
              $.each(errors, function(index, value) {
                  $('#'+index).parent().parent().removeClass('has-error');
                  $('#'+index).next().remove()
                  $('#'+index).parent().parent().addClass('has-error');
                  $('#'+index).after('<p class="help-block">'+value+'</p>');
           });
    }
    });    
});



$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('providers/kennels/') }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url('providers/customers/') }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#customers').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url("providers/kennels") }}',
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [
            { data: 'id', name:'id', "searchable": false},
            { data: 'firstname', name: 'firstname' },
            // { data: 'lastname', name: 'lastname' },
            { data: 'startdate', name: 'startdate' },
            { data: 'enddate', name: 'enddate' },
            { data: 'title', name: 'title'},
            { data: 'created_at', name: 'created_at'},
            { data: 'noscats', name: 'noscats'},
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );



});
</script>
@endsection
