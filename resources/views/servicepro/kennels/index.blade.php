<?php //echo "<pre>"; print_r($kennels); die();?>
@extends('layouts.master')
@section('title', 'Kennel')

@section('content')
        
@if(strpos(\Request::path(),'societies/kennels') !==FALSE)
  @php $setAction = 'societies/kennels'; @endphp
@else
    @php $setAction = 'providers/kennels'; @endphp
@endif
<div class="container">
    <h1>Kennel {{-- <a href="{{ url('servicepro/adopts/create') }}" class="btn btn-primary btn-xs" title="Add New Adopt"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a> --}}</h1>  
    <div class="table">
        <table class="table table-bordered table-striped table-hover" id="kennel_table">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Customer Name</th>
                    <th>Pet</th>
                    <th>Pet Type</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th style="width:250px;">Description</th>
                    <th>Services</th>
                    <th>Request Date</th>
                    <th>Reply Date</th>
                    <th>Status</th>
		                {{-- <th>parent_id</th> --}}
		                <th>Actions</th>
                </tr>
            </thead>
        </table>
        
    </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reply</h4>
      </div>
    {!! Form::open(['url' => 'servicepro/kennels/reply', 'class' => 'form-horizontal', 'id'=>'request_reply_form']) !!}

                <div class="form-group {{ $errors->has('customer_id') ? 'has-error' : ''}}">
                <div class="col-sm-8">
                    {!! Form::hidden('customer_id', null, ['class' => 'form-control customer_id_replay']) !!}
                    {!! Form::hidden('title', null, ['class' => 'form-control title_reply']) !!}
                    {!! Form::hidden('ids', null, ['class' => 'form-control ids_reply']) !!}

                </div>
            </div>
           
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    <select name="status" id="status" class="form-control" required>
                        <option value="0">Pending</option>
                        <option value="1">Approved</option>
                        <option value="2">Reject</option>
                        
                    </select>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
           <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Comments', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    <span id="alert_comment" class="red hide">Please add comments</span>
                </div>
            </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="button" class="btn btn-primary submit" id="reply_submit"><i class="fa fa-refresh fa-spin hide" id="loader"></i> Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    {!! Form::close() !!}

          
        
      
        </div>
    </div>
</div>
<link href="{{ asset('vendor/custom/general.css') }}" rel="stylesheet" type="text/css" >
@endsection
@section('jqueries')
<script type="text/javascript">
$('#kennel_table').on('click', '.replyClick', function(){
    $('.customer_id_replay').val($(this).attr('customer_id'));
    $('.title_reply').val($(this).attr('title'));
    $('.ids_reply').val($(this).attr('ids'));
});
/*$('#reply_submit').click(function() {
    var desc = $('#description').val();
    if(desc=="" || desc=="undefined" || desc==null){
        $('#alert_comment').removeClass('hide');
        return false;
    }
    else{
        $('#alert_comment').display('none');
    }    
    return true;
});*/
/*this.disabled = true;
    $('#reply_submit_loader').removeClass('hide');*/

$(document).ready(function(){

$.fn.dataTable.ext.buttons.reload = {
    text: 'Reload',
    className: 'buttons-alert',
    action: function ( e, dt, node, config ) {

        $('#post').val('');
        dt.ajax.reload();
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
};

$.fn.dataTable.ext.buttons.edit = {
    text: 'Edit',
    className: 'buttons-alert disabled editButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url($setAction) }}'+'/'+id[0]+'/edit');

        window.location.href = myfirst;
    }
};

$.fn.dataTable.ext.buttons.delete = {
    text: 'Delete',
    className: 'buttons-danger disabled deleteButton',
    action: function ( e, dt, node, config ) {
        var id = (table.rows('.selected').data().pluck('id').toArray());
        
        if(id.length == 0)
        {
             $('.deleteButton').addClass('disabled');
             return false;
        }
        var checkonce = confirm('Are you sure you want to delete?');
        if(checkonce)
        {
            deleteData(id);
        }
    }
};


$.fn.dataTable.ext.buttons.view = {
    text: 'View',
    className: 'buttons-alert disabled viewButton',
    action: function ( e, dt, node, config ) {
        // alert( dt.rows('.selected').data().length +' row(s) selected' );
        var id = (table.rows('.selected').data().pluck('id'));
        var myfirst = ('{{ url($setAction) }}'+'/'+id[0]);

        window.location.href = myfirst;
    }
};
   
    var table = $('#kennel_table').DataTable({
        // sDom: 'Brtlip',
        dom: "<'row'<'col-sm-6 advsearch'><'col-sm-6'B>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-5'li><'col-sm-7'p>>",
        order: [0, "desc"],
        columnDefs: [ {
            orderable: 'true',
            className: 'select-checkbox',
            targets:   0,
            visible: false

        } ],
        select: {
            style:    'opts',
            selector: 'td'
        },
        // select: true,
        processing: true,
        stateSave: true,
        deferRender: true,
        stateDuration: 30,
    //     buttons: [
    //     {
    //        text: 'Reload',
    //        extend: 'reload'
    //     }, 'excel'
    // ],
        buttons: ['delete', 'reload', 'selectAll', { extend: 'selectNone', text: 'Unselect All' }],
        serverSide: true,  // use this to load only visible columns in table.

        ajax: {
            url: '{{ url($setAction) }}',
            data: function (d) {
               
                d.name = $('select[name=name]').val();
                d.operator = $('select[name=operator]').val();
                d.post = $('input[name=post]').val();
            },

        },
        columns: [
            { data: 'id', name:'id', "searchable": false},
            { data: 'customer_name', name: 'customer_name' },
            { data: 'pet', name: 'pet' ,"searchable": false},
            { data: 'petType', name: 'petType' },
            { data: 'startdate', name: 'startdate' },
            { data: 'enddate', name: 'enddate' },
            { data: 'description', name: 'description'},
            { data: 'services', name: 'services'},
            { data: 'created_at', name: 'created_at'},
            { data: 'updated_at', name: 'updated_at'},
            { data: 'status', name: 'status'},
            { data: 'action', name: 'action', "searchable": false,"orderable":false},
        ],

    });

// $('#filter_comparator').change( function() { table.draw(); } );
//     $('#filter_value').keyup( function() { table.draw(); } );

$('body').on('submit','.advanceSearch', function(e) {
        table.draw();
        e.preventDefault();
    });

   table.on( 'select', function ( e, dt, type, indexes ) {
    console.log(indexes);

    if ( type === 'row' ) {
        var data = table.rows( indexes ).data().pluck( 'id' );

        // do something with the ID of the selected items
    }

    var counter = table.rows('.selected').data().length;
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }
} );


   table.on( 'deselect', function ( e, dt, type, indexes ) {
    console.log(type);
    
    var counter = table.rows('.selected').data().length;
    console.log(counter);
    if(counter == 0)
    {
        $('.viewButton, .deleteButton, .editButton').addClass('disabled');
    }
    else if(counter == 1)
    {
        $('.deleteButton, .viewButton, .editButton').removeClass('disabled');
        
    } else if(counter > 1){
        $('.viewButton, .editButton').addClass('disabled');
        $('.deleteButton').removeClass('disabled');
    }

    
} );
$('.dt-buttons').addClass('pull-right');
$('.advsearch').append('<form method="POST" class="form-inline advanceSearch" role="form"><div class="form-group"><select id="filter_header" name="name" ><option value="firstname">Customer Name</option> <option value="pets.name">Pet</option> <option value="startdate">Start Date</option><option value="enddate">End Date</option>  <option value="title">Title</option></select></div><div class="form-group"><select name="operator" id="operator"><option value="like">Like</option><option value="=">=</option><option value=">=">&gt=</option><option value=">">&gt</option><option value="<">&lt</option></select></div><div class="form-group"><input type="text" name="post" id="post"></div><button type="submit" class="btn btn-primary advance-serch-btn">Search</button></form>');
function deleteData(id) {
    $.ajax({
        type: 'post',
        data: {id: id, _method: 'delete'},
        url: '{{ url($setAction) }}'+'/'+id,
            
        success:function(response){

            if(response.success)
            {
                console.log(response.success);
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'success');
            }
            else
            {
                table.ajax.reload();
                $('.deleteButton').addClass('disabled');
                grown_noti(response.message,'danger');
            }
        
        },
    });
}
$('.submit').on('click', function(){
   var id = $('.ids_reply').val();
   var status = $('#status').val();
   $('.submit').attr("disabled", true);
   $('#loader').removeClass("hide");
   $.ajax({
            type:'POST',
            url:'{{ url($setAction.'/reply') }}',
            data:{'id':id,'customer_id':$('.customer_id_replay').val(),'status':$('#status').val(),'title':$('.title_reply').val(),'description':$('#description').val()},
            success:function(data){
                $('.submit').attr("disabled", false);
                $('#loader').addClass("hide");
                $('#myModal').modal('toggle');
                $('#myModal').find('form')[0].reset();
                $('.replyStatus'+id).css('display','none');
                if($('#status').val() == 1) {
                    $('.status'+id).html('Approved');
                } else if($('#status').val() == 2) {
                    $('.status'+id).html('Reject');
                } else {
                    $('.status'+id).html('Pending');  
                }
                table.ajax.reload();
              var response = jQuery.parseJSON(data);
              $.bootstrapGrowl(response.message, { type: 'success',offset: {from: 'top', amount: 70},align: 'right',width: 250,allow_dismiss: false});
           },
           error: function(data){
              var errors = data.responseJSON;
              $.each(errors, function(index, value) {
                  $('#'+index).parent().parent().removeClass('has-error');
                  $('#'+index).next().remove()
                  $('#'+index).parent().parent().addClass('has-error');
                  $('#'+index).after('<p class="help-block">'+value+'</p>');
           });
    }
    });    
});

});


</script>
@endsection
