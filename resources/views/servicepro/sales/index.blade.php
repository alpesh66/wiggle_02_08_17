@extends('layouts.master')
@section('title', trans('text.event'))
@section('content')
<div class="container">
    <form method="POST" action="{{ url('providers/generateSalesReport') }}" class="form-horizontal" role="form" id="sales_report">
    {{ csrf_field() }}
    <div class="row panel">     
        <div class="x_title">
            <h2>@lang('text.sales')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"><br/>   
            @if (Session::has('message'))
               <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            <div class="form-group {{ $errors->has('booking_status') ? 'has-error' : ''}}"> 
                 <label for="booking_status" class="control-label col-md-3 col-sm-3 col-xs-12">Booking Status</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="booking_status[]" id="booking_status" class="form-control" multiple="true">
                                               <?php
                                                foreach ($BookingStatus as $key => $value) {
                                                    echo "<option value=".$key.">".$value."</option>";
                                                }
                                                ?>
                                           </select>
                   
                 </div>
            </div>
            <div class="form-group {{ $errors->has('booking_from') ? 'has-error' : ''}}"> 
                 <label for="booking_from" class="control-label col-md-3 col-sm-3 col-xs-12">Booking From</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="booking_from[]" id="booking_from" class="form-control" multiple="true">
                                                <?php
                                                foreach ($BookingFrom as $key => $value) {
                                                    echo "<option value=".$key.">".$value."</option>";
                                                }
                                                ?>               
                                            </select>
                 </div>
            </div>
            <div class="form-group {{ $errors->has('parentid') ? 'has-error' : ''}}"> 
                 <label for="parentid" class="control-label col-md-3 col-sm-3 col-xs-12">Service Category</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="parentid[]" id="parentid" class="form-control" multiple="true">    
                        <?php
                        foreach ($categories as $key => $value) {
                            echo "<option value=".$key.">".$value."</option>";
                        }
                        ?>
                    </select>
                 </div>
            </div>
            <!-- <div class="form-group {{ $errors->has('service_id') ? 'has-error' : ''}}"> 
                 <label for="service_id" class="control-label col-md-3 col-sm-3 col-xs-12">Service</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="service_id[]" id="service_id" class="form-control" multiple="true">    
                        <option value=''>Select Service</option>
                    </select>
                 </div>
            </div> -->
            <div class="form-group {{ $errors->has('booking_status') ? 'has-error' : ''}}"> 
                 <label for="booking_status" class="control-label col-md-3 col-sm-3 col-xs-12">Payment Status</label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="payment_status[]" id="payment_status" class="form-control" multiple="true">
                         <?php
                        foreach ($PaymentStatus as $key => $value) {
                            echo "<option value=".$key.">".$value."</option>";
                        }
                        ?>                
                    </select>

                 </div>
            </div>
            <div class="form-group {{ $errors->has('startdate') ? 'has-error' : ''}}">
                {!! Form::label('startdate', 'Booking Start Date', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <div class="input-group startdate">
                    {!! Form::input('text', 'startdate', null, ['class' => 'form-control input-group date input-group-addon']) !!}
                     <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    {!! $errors->first('startdate', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('enddate') ? 'has-error' : ''}}">
                {!! Form::label('enddate', 'Booking End Date', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6" id="datetimepicker2">
                    <div class="input-group enddate">
                    {!! Form::input('text', 'enddate', null, ['class' => 'form-control input-group date input-group-addon']) !!}
                    
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    
                    </div>
                    {!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary advance-serch-btn">Export</button>
                    <button type="reset" class="btn btn-danger" id="reset">Reset</button>
                </div>
            </div> 
        </div>
    </div>
    </form>

</div>
@endsection

@section('jqueries')

<script>
$(document).ready(function(){
  /*  $('.startdate').datepicker({
        weekStart: 1,
        today:true,
        autoclose: true
    });
    $('.enddate').datepicker({
        weekStart: 1,
        today:true,
        autoclose: true
    });*/
    $("#parentid").select2();
    $("#service_id").select2();
     $("#booking_status").select2();
    $("#booking_from").select2();
    $("#payment_status").select2();

    $('#reset').on('click', function () {
        $("#parentid").select2("val", "");    
        $("#booking_status").select2("val", "");
        $("#booking_from").select2("val", "");    
        $("#payment_status").select2("val", ""); 
    });
    $('.startdate').datetimepicker({
        sideBySide:true,
        format: 'MM/DD/YYYY',
    });
    $('.enddate').datetimepicker({
        sideBySide:true,
        format: 'MM/DD/YYYY',
        useCurrent: false //Important! See issue #1075
    });
    $(".startdate").on("dp.change", function (e) {
        $('.enddate').data("DateTimePicker").minDate(e.date);
    });
    $(".enddate").on("dp.change", function (e) {
        $('.startdate').data("DateTimePicker").maxDate(e.date);
    });
    /*$("select[id='parentid']").change(function(){
         var parentid = $(this).val();
         var token = $("input[name='_token']").val();
         $.ajax({
             type: 'post',
             data: {parentid: parentid},
             url: '{{ url('providers/getCategoryList') }}',
             success:function(response){
                $("select[id='service_id'").html('');
                $("select[id='service_id'").html(response);
             },
         });
     });*/
});
</script>
@endsection
