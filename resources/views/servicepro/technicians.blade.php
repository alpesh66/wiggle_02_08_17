@extends('layouts.master')
@section('title', 'Technicians')
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Technicians</h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
                  <div class="x_title">
                    <h2>Technician</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>alphs@aslc.com</td>
                          <td>
                            <a href=" {{ url('technicians/Mark') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>jacob@fat.com</td>
                          <td>
                            <a href=" {{ url('technicians/Thornton') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>larry@examp.eco</td>
                          <td>
                            <a href=" {{ url('technicians/Larry') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">4</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>alphs@aslc.com</td>
                          <td>
                            <a href=" {{ url('technicians/Mark') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">5</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>jacob@fat.com</td>
                          <td>
                            <a href=" {{ url('technicians/Jacob') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">6</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>larry@examp.eco</td>
                          <td>
                            <a href=" {{ url('technicians/Larry') }} " title="View" class="btn btn-success btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span></a>
                          </td>
                        </tr>

                      </tbody>
                    </table>

                  </div>
                </div>
  </div>
</div>
@endsection

@section('jqueries')

@endsection