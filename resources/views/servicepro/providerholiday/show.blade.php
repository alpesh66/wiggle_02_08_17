@extends('layouts.master')
@section('title', 'Show Holiday')
@section('content')
<div class="container">

    <h1>Technician Holiday 
        <a href="{{ url('providers/providerholidays/' . $providerholiday->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit providerholidays"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['providers/providerholidays', $providerholiday->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete providerholiday',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>    
                    <th>Start</th><td>{{ $providerholiday->start }}</td>
                </tr>
                <tr>
                    <th>End</th><td>{{ $providerholiday->end }}</td>
                </tr>
                <tr>
                    <th>Type</th><td>{{ $providerholiday->type }}</td>
                </tr>
                <tr>
                    <th>Status</th><td>{{ $providerholiday->status }}</td>
                </tr>
                
            </tbody>
        </table>
    </div>

</div>
@endsection
