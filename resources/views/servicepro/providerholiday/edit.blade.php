@extends('layouts.master')
@section('title', 'Edit Holiday')
<style type="text/css">
    .disnone{
        display: none;
    }
</style>
@section('content')
<div class="container">

    <h1>Edit Holiday</h1>

    {!! Form::model($providerholiday, [
        'method' => 'PATCH',
        'url' => ['/providers/providerholidays', $providerholiday->id],
        'class' => 'form-horizontal',
        'files' => true
    ]) !!}

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('start', 'Start Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('start', date('m/d/Y',strtotime($providerholiday->start)) , ['class' => 'form-control startdate']) !!}
                {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('end', 'End Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('end', date('m/d/Y',strtotime($providerholiday->end)), ['class' => 'form-control enddate']) !!}
                {!! $errors->first('end', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('type', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="radio" class="" id="full" name="type" value="0" {{  ($providerholiday->type == 0) ? 'checked="checked"' : "" }} > Full Day
                <input type="radio" class="" id="half" name="type" value="1" {{  ($providerholiday->type == 1) ? 'checked="checked"' : "" }} > Half Day
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        
        <div class="form-group breaktime {{  ($providerholiday->type == 0) ? 'disnone' : '' }} {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('startime', 'Start Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('startime', null, ['class' => 'form-control time','id' => 'startime']) !!}
                {!! $errors->first('startime', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group breaktime {{  ($providerholiday->type == 0) ? 'disnone' : '' }} {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('endtime', 'End Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('endtime', null, ['class' => 'form-control time','id' => 'endtime']) !!}
                {!! $errors->first('endtime', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <!-- <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="checkbox" name="status" class="bootswitch" {{  ($technicianleave->status == 1) ? 'checked="checked"' : "" }} data-on-text="Yes" data-off-text="No">
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div> -->

        <div class="form-group">
            
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Update', ['class' => 'btn btn-primary ']) !!}
                <a class="btn btn-danger" href="{{ url()->previous() }}" >Cancel</a>
            </div>
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}
<script type="text/javascript">
        $(document).ready(function(){
            $('.bootswitch').bootstrapSwitch();

            $("#full").click(function(){
                $(".breaktime").hide();
                $("#startime").val('00:00');
                $("#endtime").val('00:00');
            });

            
            
                

            $("#half").click(function(){
                $(".breaktime").show();
            });

            $('.time').bootstrapMaterialDatePicker
            ({
                date: false,
                shortTime: false,
                format: 'HH:mm',
                switchOnClick: true
            });

        });

        var startDate = new Date();
        var FromEndDate = new Date();
        var ToEndDate = new Date();

        ToEndDate.setDate(ToEndDate.getDate()+365);
        $('.startdate').datepicker({
            weekStart: 1,
            today:true,
            autoclose: true,
            //dateFormat: 'yy-mm-dd' 
        }).on('changeDate', function(selected){
            startDate = new Date(selected.date.valueOf());
            var tempStartDate = new Date(startDate);
            var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate());
            $('.enddate').datepicker('setDate', default_end);
        });

        $('.enddate').datepicker({
            weekStart: 1,
            today:true,
            autoclose: true,
            //dateFormat: 'yy-mm-dd' 
        });
</script>
@endsection