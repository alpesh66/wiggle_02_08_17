@extends('layouts.master')
@section('title', 'Leave')
@section('content')
<div class="container">

    <h1>Leave 
        <a href="{{ url('providers/technicianleave/' . $technicianleave->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit technicianleave"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['providers/technicianleave', $technicianleave->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete technicianleave',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>Technician</th><td>{{ $technicianleave->technician->name }}</td>
                </tr>
                <tr>    
                    <th>Start</th><td>{{ $technicianleave->start }}</td>
                </tr>
                <tr>
                    <th>End</th><td>{{ $technicianleave->toend }}</td>
                </tr>
                <tr>
                    <th>Type</th><td>{{ $technicianleave->type }}</td>
                </tr>
                <tr>
                    <th>Status</th><td>{{ $technicianleave->status }}</td>
                </tr>
                
            </tbody>
        </table>
    </div>

</div>
@endsection
