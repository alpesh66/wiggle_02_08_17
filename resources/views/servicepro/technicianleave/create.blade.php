@extends('layouts.master')
@section('title', 'Add Leave')
@section('content')
<style type="text/css">
    .disnone{
        display: none;
    }
</style>
<div class="container">

    <h1>Create New Leave</h1>
    <hr/>

    {!! Form::open(['url' => 'providers/technicianleave', 'class' => 'form-horizontal', 'files' => true]) !!}

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('technician_id  ', 'Technician', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {{ Form::select('technician_id', $technicians, null, ['class' => 'form-control']) }}
                {!! $errors->first('technician_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('start', 'Start Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('start', null, ['class' => 'form-control startdate','id' => 'startdate']) !!}
                {!! $errors->first('start', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('toend', 'End Date', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('toend', null, ['class' => 'form-control enddate','id' => 'enddate']) !!}
                {!! $errors->first('toend', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('type', 'Type', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <!-- {!! Form::text('type', null, ['class' => 'form-control']) !!} -->
                <input type="radio" class="" id="full" name="type" value="0" checked="checked"> Full Day
                <input type="radio" class="" id="half" name="type" value="1"> Half Day
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group breaktime disnone  {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('breakstart', 'Start Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('breakstart', null, ['class' => 'form-control time','id' => 'breakstart']) !!}
                {!! $errors->first('breakstart', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group breaktime disnone {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('breakend', 'End Time', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::text('breakend', null, ['class' => 'form-control time','id' => 'breakend']) !!}
                {!! $errors->first('breakend', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', 'Status', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            <a class='btn btn-danger' href="{{ url()->previous() }}">Cancel</a>
        </div>
        </div>
    {!! Form::close() !!}

</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}
<script type="text/javascript">
        $(document).ready(function(){
            $('.bootswitch').bootstrapSwitch();

            $("#full").click(function(){
                $(".breaktime").hide();
                $("#breakstart").val('00:00:00');
                $("#breakend").val('00:00:00');
            });

            $("#half").click(function(){
                $(".breaktime").show();
            });

            $('.time').bootstrapMaterialDatePicker
            ({
                date: false,
                shortTime: false,
                format: 'HH:mm'
            });
        });

        var startDate = new Date();
        var FromEndDate = new Date();
        var ToEndDate = new Date();

        // ToEndDate.setDate(ToEndDate.getDate()+365);
        // $('.startdate').datepicker({
        //     weekStart: 1,
        //     today:true,
        //     autoclose: true
        // }).on('changeDate', function(selected){
        //     startDate = new Date(selected.date.valueOf());
        //     var tempStartDate = new Date(startDate);
        //     var default_end = new Date(tempStartDate.getFullYear(), tempStartDate.getMonth(), tempStartDate.getDate());
        //     $('.enddate').datepicker('setDate', default_end);
        // });
        // $('.enddate').datepicker({
        //     weekStart: 1,
        //     today:true,
        //     autoclose: true
        // });

        $('.startdate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now' 
        });

        $('.enddate').datetimepicker({
            sideBySide:false,
            format: 'MM/DD/YYYY',
            minDate: 'now'
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
        });

          
</script>
@endsection
