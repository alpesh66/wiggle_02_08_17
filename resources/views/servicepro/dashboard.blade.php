@extends('layouts.master')
@section('title', 'Dashboard')
@section('content')
{{-- <div class="page-title">
  <div class="title_left">
    <h3>Dashboard</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div>
</div> --}}
<div class="title_left">
    <h3>Dashboard</h3>
  </div>

<div class="clearfix"></div>
{{--
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-users"></i> @lang('text.customers')</span>
    <div class="count">{{$customers}}</div>

  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-circle"></i> @lang('text.services')</span>
    <div class="count">{{$service_count}}</div>

  </div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">

    <span class="count_top"><i class="fa fa-scissors"></i> @lang('text.technicians')</span>
    <div class="count">{{$tech_count}}</div>

  </div>

  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-check-square-o"></i> @lang('text.bookings')</span>
    <div class="count">{{$bookings}}</div>
  </div>
</div>
--}}

<div class="clearfix"></div>

<div class="row">
       <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Bookings</b><hr></h5>
                    <h1 class="mt5">{{$bookings}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Previous</b></h5>
                        <h4 class="nomargin">{{$previous}}</h4>
                    </div>
                    <div class="pull-right">
                        <h5 class="md-title nomargin"><b>Upcoming</b></h5>
                        <h4 class="nomargin">{{$upcoming}}</h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Clients</b><hr></h5>
                    <h1 class="mt5">{{$customers}}</h1>
                </div><!-- media-body -->
               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Last 30 Days</b></h5>
                        <h4 class="nomargin">{{$lastThirtyday}}</h4>
                    </div>
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    <div class="col-md-4">
        <div class="panel panel-success-alt noborder">
            <div class="panel-heading noborder">
                <div class="media-body">
                    <h5 class="md-title nomargin"><b>Total Booking Last 30 Day</b><hr></h5>
                    <h1 class="mt5">{{$booking_LastThirtyDays}}</h1>
                </div><!-- media-body -->               
                <div class="clearfix mt20">
                    <div class="pull-left">
                        <h5 class="md-title nomargin"><b>Manual Booking</b></h5>
                        <h4 class="nomargin">{{$booking_by_web}}</h4>
                    </div>
	            <div class="pull-right">
                        <h5 class="md-title nomargin"><b>App Booking</b></h5>
                        <h4 class="nomargin">{{$booking_by_app}}</h4>
                    </div>		
                </div>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div>
    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Most Used Services</b><hr></h5>
          </div><!-- media-body -->
		      @if(isset($category))			
			      @foreach($category as $item)
		          <div class="clearfix mt20">
		            <div class="pull-left">
		              <h5 class="md-title nomargin">{{ $item['name'] }} : {{$item['count']}} </h5>
		            </div>
		          </div>
			      @endforeach
		      @else		
			      <div class="clearfix mt20">
		          <div class="pull-left">
		              <h1 class="mt5"></h5>
		          </div>
		        </div>		
		      @endif	
        </div><!-- panel-body -->
      </div><!-- panel -->
    </div>
    <div class="col-md-4">
      <div class="panel panel-success-alt noborder">
        <div class="panel-heading noborder">
          <div class="media-body">
            <h5 class="md-title nomargin"><b>Last Rating And Review</b><hr></h5>
          </div><!-- media-body -->
          <div class="clearfix mt20">
          @foreach($rating_list as $item)
            <article class="media event">
              <a class="pull-left date">
                <p class="month">{{ date('M',strtotime($item->created_at)) }}</p>
                <p class="day">{{ date('d',strtotime($item->created_at)) }}</p>
              </a>
              <div class="media-body">
                  <a href="#" class="">
                    @if(isset($item->customer->firstname))
                    <label>Customer name : </label> {{ ucfirst($item->customer->firstname).' '.$item->customer->lastname }}
                    @else
                    <label>Customer name : </label> Anonymous
                    @endif
                  </a>
                  <p><label>Rate : </label>{{ $item->rate }}</p>
                  <p><label>Review : </label> {{ $item->review }}</p>
              </div>
          </article>
          @endforeach          
          </div>
        </div><!-- panel-body -->
      </div>
   </div>	
   @if($provider_type_count>0)
   <div class="col-md-4">
       <div class="panel panel-success-alt noborder">
           <div class="panel-heading noborder">
               <div class="media-body">
                   <h5 class="md-title nomargin"><b>Kennel Request</b><hr></h5>
                   <h1 class="mt5">{{$totalKennel}}</h1>
               </div><!-- media-body -->               
               <div class="clearfix mt20">
                   <div class="pull-left">
                       <h5 class="md-title nomargin"><b>Approve</b></h5>
                       <h4 class="nomargin">{{$approveKennel}}</h4>
                   </div>
                <div class="pull-left" style="margin-left: 30%;">
                    <h5 class="md-title nomargin"><b>Pending</b></h5>
                    <h4 class="nomargin">{{$pendingKennel}}</h4>
                </div>
            <div class="pull-right">
                       <h5 class="md-title nomargin"><b>Rejected</b></h5>
                       <h4 class="nomargin">{{$rejectedKennel}}</h4>
                   </div>       
               </div>
           </div><!-- panel-body -->
       </div><!-- panel -->
   </div>
   @endif   
</div>  
@endsection
