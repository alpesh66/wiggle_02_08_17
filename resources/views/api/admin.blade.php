<!DOCTYPE html>
<html>
<head>
<title>Admin Panel Rest Client</title>
<meta charset="utf-8" />

<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>

<script type="text/javascript">
var REST_API = {
        SERVER_URL:"{{ url()->current() }}",
        CLIENT_URL:"{{ url()->current() }}"
        };
        
</script>
<script type="text/javascript">
$(document).ready(function(){


    function syntaxHighlight(json) {
        json = json.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        return json.replace(/("(\u[a-zA-Z0-9]{4}|\[^u]|[^\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = "number";
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = "key";
                } else {
                    cls = "string";
                }
            } else if (/true|false/.test(match)) {
                cls = "boolean";
            } else if (/null/.test(match)) {
                cls = "null";
            }

            return "<span class=" + cls + ">" + match + "</span>";
        });
    }

    function capitaliseFirstLetter(string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function tab_chars(times){
        var str = "&nbsp;&nbsp;&nbsp;&nbsp;";
        if(times!=undefined){
            for(var i=0;i<times;i++){
                str += "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        }
        return str;
    }

    function scroll_to_top(){
        $("html, body").animate({scrollTop:0}, "slow");
    }
    
    $('#server_url').val(REST_API.SERVER_URL);

    $.ajax({
        url: REST_API.CLIENT_URL,
        success: function( data ) {
            generate_api_form(data);
        },error: function( error ){
            alert('Wrong configuration');
        }});

    function generate_api_form (data){
        var jsonData = (typeof(data) == 'string') ? jQuery.parseJSON(data) : data;
        var serviceElement = $('.service');

        if (jsonData.length > 0) {
            for ( var index = 0; index < jsonData.length; index++) {
                var methodClassData = jsonData[index].request.split('/');
                var methodName = methodClassData[1];
                var className = methodClassData[0];

                var paraDetails = jsonData[index].para;

                var requestFormat = {"request":jsonData[index].request,'para':{}};
                if(paraDetails.length > 0 ){
                    for(var pindex = 0; pindex < paraDetails.length; pindex++){
                        requestFormat.para[paraDetails[pindex]] = '';
                    }
                }

                var htm  = '<div class="api_box" id="'+methodName+'">';
                    htm += '<div class="description">'+methodName+'</div>';

                    htm += '<div class="json" style="display:none">';
                    htm += '<div class="data_source">'+JSON.stringify(requestFormat)+'</div>';

                    htm += '<a href="#" class="objc_generate">Objective C Code</a>';
                    htm += '<div class="objc_target" style="display:none;"></div>';

                    htm += '<table request="'+jsonData[index].request+'">';

                    if(paraDetails.length > 0 ){
                        for(var pindex = 0; pindex < paraDetails.length; pindex++){
                            htm += '<tr><td>'+paraDetails[pindex]+':</td><td><input type="text" name="'+paraDetails[pindex]+'"/></td></tr>'
                        }
                    }

                    htm += '<tr><td colspan="2"><input type="button" class="ac_show_response" value="click here to see response"></td></tr>'
                    htm += '</table>';

                    htm += '</div></div>';

                serviceElement.append(htm);
            }
        }

        $(".api_box").click(function(){
            $(".api_box .json").hide();
            var id = $(this).attr("id");
            $("#"+id+" .json").show();
        });

        $('.ac_show_response').click(function(){
             var t_ref = $(this).parents('table');
             var obj = {'request':t_ref.attr('request'),'para':{}};
             t_ref.find('input[type=text]').each(function(){
             obj.para[$(this).attr('name')] = $(this).val();  });
              $('#json').val(JSON.stringify(obj));
             $('.submit').click();
        });
    }

    $(".submit").click(function() {

        var url = $("#server_url").val();

        var json = $("#json").val();
        var dataString = "request_json="+ $("#json").val();
        dataString = $("#json").val();

        var ajaxStartTime = new Date().getTime();

        $('#response_status').text("Loading...");

        $.ajax({type: "POST",
                url: url,
                data: dataString,
                processData: false,
                cache: false,
                async: true,
                contentType: "application/json",
                timeout: 300000,
                dataType: "text"}).
                done(function(response) {
                                            var totalTime = new Date().getTime() - ajaxStartTime;
                                            $('#info').html("<pre>"+totalTime / 1000 + ' sec'+"</pre>");
                                            scroll_to_top();
                                             try {
                                                 if($("#json_format_check").is(':checked') == true) {
                                                        var obj = jQuery.parseJSON(response);
                                                        var str = JSON.stringify(obj, undefined, 4);
                                                        $("#highlighted").html("<pre>"+syntaxHighlight(str)+"</pre>");
                                                        $(".response_format").show();
                                                    } else {
                                                        $(".response_format").hide();
                                                    }
                                                } catch (e) {

                                                }
                                            $("#response").val(response).fadeIn();
                }).fail(function() {
                    $('#response_status').text("Fail to server requtes");
                }).always(function() {
                    $('#response_status').text("");
                });

    });

    $(document).on("click", ".objc_generate", function(){
        var json_text = $(this).siblings(".data_source").text();
        var obj = $.parseJSON(json_text);
        var api_name = obj.request.split("/")[1];
        var para_list = "";

        var is_first_para = true;
        for (var property in obj.para) {
            if(property == 'api_token'){
                continue;
            }
            if(is_first_para == true){
                is_first_para = false;
                para_list += ":(NSString*)"+property+" ";
            }else{
                para_list += "and"+capitaliseFirstLetter(property)+":(NSString*)"+property+" ";
            }
        };

        var innder_code = "";

        innder_code += '<br>'+
                        tab_chars()+'NSDictionary *req = [NSDictionary dictionaryWithObjectsAndKeys:@"'+obj.request+'",@"request",<br>'+
                                            tab_chars(9)+'[NSDictionary dictionaryWithObjectsAndKeys:<br>';
                                            innder_code += tab_chars(9) + 'APP_API_TOKEN,@"api_token",<br>';
                                                for (var property in obj.para) {
                                                    if(property == 'api_token') {
                                                        continue;
                                                    }
                                                innder_code += tab_chars(9)+property+',@"'+property+'",<br>';
                                            };

        innder_code += tab_chars(9)+'nil],@"para",nil];';



        innder_code += "<br>"+tab_chars()+"[self.objAPIConsumer doRequestPost:APP_API_URL andData:req];<br>";
        var generated_method = "-(void)"+api_name+para_list+ "{"+"<br>"+innder_code+"}";

        $(this).siblings(".objc_target").show();
        $(this).siblings(".objc_target").html(generated_method);
        return false;
    });
    });

</script>
<style type="text/css">
.service { border: 1px solid blue; background-color: #eee; padding : 5px; padding: 5px;}
.service .description { background-color: #FFEFD5; border: 1px solid black; padding: 2px; margin-bottom: 6px; cursor: pointer;}
.service .json{ padding:7px 0;}
.response{margin-bottom: 15px;}
pre { outline: 1px solid #ccc; padding: 5px; margin: 5px; width: 100%;}
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }
.objc_target{ border: 1px solid; padding: 10px; background: #DADADA;}
</style>

</head>
<body>



    <form action="" id="client" method="post">
        Request<br>
        <textarea rows="10" cols="100" name="request_json" id="json"></textarea>
        <br>
        Server url : <input type="text" id="server_url" value="" style="width:500px;" >
        <br>
        <input type="button" class="submit" value="click here to see response">
        <input type="checkbox" id="json_format_check" name="json_format" value="" checked="checked"> <label for="json_format_check">Format JSON</label>
    </form>
    <hr>
    <div class="response" style="display: block;">
        Response <span id="response_status"></span> <br>
        <textarea id="response" cols="100" rows="10" style="display: inline-block;"></textarea>
    </div>
    <div class="response_format" style="display: none;">
        <div id="info"></div>
        <div id="highlighted">
            <pre></pre>
        </div>
    </div>
    <div class="service">
    </div>
    <br>version: 1.0.4
</body>
</html>