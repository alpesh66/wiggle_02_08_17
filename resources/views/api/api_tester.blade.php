<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='en'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="api/css/select2.css"/>
        <link rel="stylesheet" href="api/css/style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script>
            window.jQuery || document.write("<script src='api/js/jquery.min.js'><\/script>");
        </script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="api/js/select2.js"></script>
        <script src="api/js/select2.full.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default  ">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= URL::to('/') ?>" style="padding: 0">
                        <img src="logo.png" class="img-responsive" width="80"/> 
                    </a>
                    <div class="clearfix"></div>
                </div>
                <ul class="nav navbar-nav">

                </ul>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-10">
                    <div class="">
                        <span><?php echo URL::to('/') ?></span> &nbsp;
                        <select name="api_name" id="api_name">
                            <option value="0">Select API</option>
                            <optgroup label="User API">
                                <option value="postLogin">/frontapi/v1/postLogin</option>
                            </optgroup>
                            <optgroup label="Adopt API">
                                <option value="postAdoption">/frontapi/v1/postAdoption</option>
                                <option value="getAdoption">/frontapi/v1/getAdoption</option>
                            </optgroup>
                            <optgroup label="Volunteer API">
                                <option value="postVolunteer">/frontapi/v1/postVolunteer</option>
                                <option value="getVolunteer">/frontapi/v1/getVolunteer</option>
                            </optgroup>
                            <optgroup label="Foster API">
                                <option value="postFostering">/frontapi/v1/postFostering</option>
                                <option value="getFoster">/frontapi/v1/getFoster</option>
                            </optgroup>
                            <optgroup label="Surrender API">
                                <option value="postSurrender">/frontapi/v1/postSurrender</option>
                                <option value="getSurrender">/frontapi/v1/getSurrender</option>
                            </optgroup>
                            <optgroup label="Kennel API">
                                <option value="postKennel">/frontapi/v1/postKennel</option>
                                <option value="getKennel">/frontapi/v1/getKennel</option>
                                <option value="missYouRequest">/frontapi/v1/missYouRequest</option>
                                <option value="getMissYouResponse">/frontapi/v1/getMissYouResponse</option>
                            </optgroup>
                            <optgroup label="Event API">
                                <option value="getEvents">/frontapi/v1/getEvents</option>
                                <option value="getEventDetails">/frontapi/v1/getEventDetails</option>
                            </optgroup>
                            <optgroup label="Pet store API">
                                <option value="getPawnPages">/frontapi/v1/getPawnPages</option>
                            </optgroup>
                            <optgroup label="Global Map API">
                                <option value="getGlobalMap">/frontapi/v1/getGlobalMap</option>
                            </optgroup>
<!--                            <optgroup label="Other API">
                                
                            </optgroup>-->
                        </select>
                    </div>
                </div>
            </div>
            <div class="main">
                <!--postLogin api form start-->
                <form class="form-horizontal" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postLogin" method="post" id="postLogin">
                    <h2>Front end user login</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">email*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="email" name="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="password">password*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="password" name="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="type">type*:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="type" name="type">
                                <option value="wg">wg</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postLogin" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postLogin form api end-->



                <!--postAdoption api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postAdoption" method="post" id="postAdoption">
                    <h2>Add adoption request</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="provider_id">provider_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="provider_id" name="provider_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="adopts">adopts*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="adopts" name="adopts"/>
                            <span>pet id</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="title">title*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="description">description*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postAdoption" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postAdoption form api end-->

                <!--getAdoption api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getAdoption" method="post" id="getAdoption">
                    <h2>get adoption data</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="paging">paging*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="paging" name="paging"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="society">society</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="society" name="society"/>
                            <br/> Society provider id
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="breed">breed:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="breed" name="breed">
                                <option value="0" selected>All</option>
                                <option value="1">Dog</option>
                                <option value="2">Cat</option>
                            </select>
                            <span>1=Dog, 2=Cat (Default show all types)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getAdoption" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getAdoption form api end-->

                <!--postVolunteer api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postVolunteer" method="post" id="postVolunteer">
                    <h2>Add volunteer</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="providers">providers*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="providers" name="providers"/>
                            <span>Add comma seprated provider id like 1,2,3</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="title">title*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="description">description*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="startdate">startdate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="startdate" name="startdate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="enddate">enddate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="enddate" name="enddate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postVolunteer" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postVolunteer form api end-->

                <!--getVolunteer api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getVolunteer" method="get" id="getVolunteer">
                    <h2>Get all volunteer</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getVolunteer" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getVolunteer form api end-->
                
                <!--postFostering api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postFostering" method="post" id="postFostering">
                    <h2>Add Fostering</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="breed_id">breed_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="breed_id" name="breed_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="providers">providers*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="providers" name="providers"/>
                            <span>Add comma seprated provider id like 1,2,3</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="title">title*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="description">description*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="startdate">startdate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="startdate" name="startdate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="enddate">enddate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="enddate" name="enddate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postFostering" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postFostering form api end-->

                <!--getFoster api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getFoster" method="get" id="getFoster">
                    <h2>Get all foster</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getFoster" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getFoster form api end-->
                
                <!--postSurrender api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postSurrender" method="post" id="postSurrender">
                     <h2>Post Surrender</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="provider_id">provider_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="provider_id" name="provider_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pets">pets*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="pets" name="pets"/>
                            <span>Add comma seprated pet id like 1,2,3</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="title">title*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="description">description*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postSurrender" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postSurrender form api end-->

                <!--getSurrender api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getSurrender" method="get" id="getSurrender">
                    <h2>Get surrender</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getSurrender" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getSurrender form api end-->
                
                <!--postKennel api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/postKennel" method="post" id="postKennel">
                    <h2>Send kennel request</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="provider_id">provider_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="provider_id" name="provider_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pets">pets*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="pets" name="pets"/>
                            <span>Add comma seprated pet id like 1,2,3</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="title">title*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="description">description*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="description" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="startdate">startdate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="startdate" name="startdate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="enddate">enddate*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="enddate" name="enddate"/>
                            <span>Date formate(YYYY-MM-DD)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="postKennel" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--postKennel form api end-->

                <!--getKennel api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getKennel" method="get" id="getKennel">
                    <h2>Get Kennels</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getKennel" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getKennel form api end-->

                <!--getEvents api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getEvents" method="get" id="getEvents">
                    <h2>Get all events</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getEvents" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getEvents form api end-->

                <!--getEventDetails api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getEventDetails" method="get" id="getEventDetails">
                    <h2>Get event details</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="event_id">event_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="event_id" name="event_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getEventDetails" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getEventDetails form api end-->

                <!--getPawnPages api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getPawnPages" method="get" id="getPawnPages">
                    <h2>Get all pet stores</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getPawnPages" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getPawnPages form api end-->

                <!--getGlobalMap api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getGlobalMap" method="post" id="getGlobalMap">
                    <h2>Get Global Map details</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="type">type:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="type" name="type">
                                <option value="0" selected>All</option>
                                <option value="1">Grooming</option>
                                <option value="2">Pet Store</option>
                                <option value="3">Societies</option>
                                <option value="4">Dog friendly areas</option>
                            </select>
                            <span>1=Grooming, 2=Pet Store, 3=Society, 4=Dog friendly areas (Default show all types)</span>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getGlobalMap" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getGlobalMap form api end-->

                <!--missYouRequest api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/missYouRequest" method="post" id="missYouRequest">
                    <h2>Send miss you request</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="provider_id">provider_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="provider_id" name="provider_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pet_id">pet_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="pet_id" name="pet_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="message">message*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="message" name="message"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="missYouRequest" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--missYouRequest form api end-->
                
                <!--getMissYouResponse api form start-->
                <form class="form-horizontal hide" role="form" action="<?= URL::to('/') ?>/frontapi/v1/getMissYouResponse" method="post" id="getMissYouResponse">
                    <h2>Get miss you response</h2>
                    <hr />
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="customer_id">customer_id*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="customer_id" name="customer_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="api_token">api_token*</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="api_token" name="api_token"/>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <input type="submit" value="getMissYouResponse" class="btn btn-primary col-md-4 col-md-offset-4" name="submit"/>
                </form>
                <!--getMissYouResponse form api end-->
                <div class="clearfix"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#api_name').select2({
                    placeholder: "Select an option"
                });
                $("#api_name").change(function () {
                    selected = $(this).val();
                    $("#" + selected).removeClass("hide");
                    $("#" + selected).siblings("form").addClass("hide");
                });
                $("#api_name").change();
            });
        </script>
    </body>
</html>