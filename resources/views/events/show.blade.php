@extends('layouts.master')
@section('title', trans('text.showEvent'))
@section('content')
<div class="container">

    <h1>Event 
        <a href="{{ url('events/' . $event->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Event"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
        {{-- <a class='btn btn-success btn-xs' title="Back" href="{{ url()->previous() }}"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/></a> --}}
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['events', $event->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Event',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
    </h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                
                @if(!empty($event->image))
                   <tr><th>Image</th><td><img src="{{$event->image}}" width="25%"></td></tr>
                @endif
                @if(!empty($event->image_ar))
                   <tr><th>Image Ar</th><td><img src="{{$event->image_ar}}" width="25%"></td></tr>
                @endif
                <tr><th> Name </th><td> {{ $event->name }} </td></tr>
                <tr><th> Name Ar</th><td> {{ $event->name_ar }} </td></tr>
                <tr><th> Description </th><td> {{ $event->desc }} </td></tr>
                <tr><th> Description Ar</th><td> {{ $event->desc_ar }} </td></tr>
                <tr><th> Startdate </th><td> {{ $event->startdate }} </td>
                </tr><th> End Date </th><td> {{ $event->enddate }} </td></tr>
            </tbody>
        </table>
    </div>

</div>
@endsection
