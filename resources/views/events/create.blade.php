@extends('layouts.master')
@section('title', 'Add Event')
@section('content')
<style type="text/css">
    .date{
        text-align: left;
    }
</style>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Create New Event</h2>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
 <br>
    {!! Form::open(['url' => '/events', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name_ar', 'Name Ar', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
                {!! Form::label('desc', 'Description *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
                {!! Form::label('desc_ar', 'Description Ar', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('desc_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('desc_ar', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('startdate') ? 'has-error' : ''}}">
                {!! Form::label('startdate', 'Startdate *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                	<div class="input-group startdate">
                    {!! Form::input('text', 'startdate', null, ['class' => 'form-control input-group date input-group-addon']) !!}
                     <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                    {!! $errors->first('startdate', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
             
            <div class="form-group {{ $errors->has('enddate') ? 'has-error' : ''}}">
                {!! Form::label('enddate', 'Enddate *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6" id="datetimepicker2">
                	<div class="input-group enddate">
                    {!! Form::input('text', 'enddate', null, ['class' => 'form-control input-group date input-group-addon']) !!}
                    
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    
                    </div>
                    {!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('weblink') ? 'has-error' : ''}}">
                {!! Form::label('weblink', 'Weblink', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('weblink', 'http://www.', ['class' => 'form-control']) !!}
                    {!! $errors->first('weblink', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address *', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
                {!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
                {!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                    {!! Form::label('location', 'Location', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                    <div class="col-sm-6">                            
                        <img width="32" height="32" onclick="valideopenerform();" style="cursor:pointer;" src="{{url('/images/map_icon.png')}}">
                    </div>
            </div>
            {{-- {!! Form::hidden('latitude', null, ['class' => 'form-control', 'id' => 'latitude']) !!}
            {!! Form::hidden('longitude', null, ['class' => 'form-control', 'id' => 'longitude']) !!} --}}
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    
                </div>
            </div>
            <div class="form-group {{ $errors->has('image_ar') ? 'has-error' : ''}}">
                {!! Form::label('image_ar', 'Image Ar', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image_ar', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('image_ar', '<p class="help-block">:message</p>') !!}
                    
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-3  col-md-3 control-label']) !!}
                <div class="col-sm-6">
                    <input type="checkbox" name="status" class="bootswitch" checked="checked" data-on-text="Yes" data-off-text="No">
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <hr>

        <div class="form-group">
             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                <a class='btn btn-danger' href="{{ url('events') }}">Cancel</a>
            </div>
        </div>
        
    {!! Form::close() !!}
 </div>
    </div>
  </div>
</div>
@endsection

@section('jqueries')
{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script> --}}

    <script type="text/javascript">
   
   function valideopenerform(lat, long) {

            var lat = $('#latitude').val();
            var long = $('#longitude').val();

            if (!lat) {
                lat = '29.31166';
            }
            if (!long) {
                long = '47.48176';
            }

            var popy = window.open('{{url('map')}}?lat=' + lat + '&long=' + long, 'popup_form', 'location=no, menubar=no, status=no, top=50%, left=50%, height=550, width=750');

        }
    $(document).ready(function(){
        $('.bootswitch').bootstrapSwitch();

        $('.startdate').datetimepicker({
            sideBySide:true,
            format: 'MM/DD/YYYY H:mm',
        });
        $('.enddate').datetimepicker({
            sideBySide:true,
            format: 'MM/DD/YYYY H:mm',
            useCurrent: false //Important! See issue #1075
        });
        
        $(".startdate").on("dp.change", function (e) {
            $('.enddate').data("DateTimePicker").minDate(e.date);
        });
        $(".enddate").on("dp.change", function (e) {
            $('.startdate').data("DateTimePicker").maxDate(e.date);
        });

    });
    </script>
@endsection