<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Provider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'providers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $dates = ['start', 'end'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','name_ar', 'url', 'about', 'about_ar', 'ptype', 'address', 'area', 'email', 'latitude', 'photo', 'icon', 'start', 'end', 'longitude', 'contact', 'status','website','facebook','twitter','instagram'];

    protected $hidden = ['created_at', 'updated_at'];
    
    public function type()
    {
        return $this->belongsToMany('\App\Type');
    }

    public function areas()
    {
        return $this->belongsTo('\App\Area', 'area');
    }

    public function protime()
    {
        return $this->hasMany('App\ProviderTiming');
    }

    /**
     * Start date column of the DB to be formatted
     * @param date $value date formatted as expected by DB
     */
    
    public function getPhotoAttribute($photo)
    {
        return ($photo == '')? '' : url('uploads/provider/').'/'.$photo;
    }

    public function setStartAttribute($value)
    {
        
        $this->attributes['start'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getStartAttribute($date)
    {
        return Carbon::parse($date)->format('m/d/Y');
    }

    public function setEndAttribute($value)
    {
        $this->attributes['end'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getEndAttribute($date)
    {
        return Carbon::parse($date)->format('m/d/Y');
    }

    public function getPtypeAttribute($ptype)
    {
        return ($ptype == 1)? 'Provider' : 'Society';
    }


    public function scopeProhasServices($q, $id)
    {
        return $q->with('type')->where('id', $id)->first()->toArray();
    }

    public function scopeApiSearch($q, $data)
    {
        $type_id = $data['type_id'];

        $q->whereHas('type', function($q) use ($type_id) {
            $q->whereTypeId($type_id);
        });

        if(isset($data['area']))
        {
            $q->orderBy('area', $data['area'] );
        }

        if(isset($data['name']))
        {
            $q->where('name', 'like', '%'.$data['name'].'%');
        }

        if(isset($data['order_by']))
        {
            $q->orderBy('name', $data['order_by']);
        }

        return $q->where('status', 1);
    }

    public function getIconAttribute($image)
    {
        return ($image == '')? '' : url('uploads/provider').'/'.$image;
    }

    public function scopeGetTypeofProviders($q, $type_id)
    {
        
        return $q->whereHas('type', function($q) use ($type_id) {
                                $q->whereTypeId($type_id);
                            })->with('areas')->where('status', 1);
    }

    public function scopeGetKennelsProviders($q)
    {
        return $q->whereHas('type', function($q) {
                                $q->whereIn('type_id', ['5', '11']);
                            })->with('areas')->where('status', 1)->whereDate('end','>=',  Carbon::now());
    }
    public function scopeGetProvidersMapAPI($q, $types = []) {
        if (!empty($types)){
            return $q->whereHas('type', function($q) use($types) {
                        $q->whereIn('type_id', $types);
                    })->with(array('areas','type'=>function($q){
                    $q->select(['id','dis_name']);
                }))->where('status', 1)->whereDate('end', '>=', Carbon::now());
        }
            return $q->with('areas')->with(array('type'=>function($q){
                    $q->select(['id','dis_name']);
                }))->where('status', 1)->whereDate('end', '>=', Carbon::now());
    }


}
