<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','utype','mobile', 'status', 'api_token','provider_id', 'technicians'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at', 'updated_at'
    ];

//
//    protected $casts = [
//        'utype' => 'boolean',
//    ];
    
    public function from_provider($data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'api_token' => str_random(60),
            'utype'     => $data['utype'],
            'status'    => 1,
            'password'  => bcrypt($data['password']),
            'provider_id' => $data['provider_id']
        ]);
    }

    public function haveYouSubscribed($service)
    {
        $pro_id = (\Auth::user()->provider_id);
        $user_provider_types = \App\Provider::ProhasServices($pro_id);
        $user_haveSubscribed =  array_pluck($user_provider_types['type'], 'id') ;
        if(in_array($service, $user_haveSubscribed))
        {
            return true;
        }
        return false;
    }

    public function scopeOnlyProviders($q)
    {
        return $q->where('provider_id', '>', 0);
    }

    public function provider()
    {
        return $this->belongsTo('\App\Provider');
    }
}
