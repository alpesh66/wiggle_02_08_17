<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VaccinationTypes extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vaccination_type';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'vaccination_duration', 'status'];

    public function scopeallTypes($q, $breed = 1) {
        $where = ['status' => 1, 'breed' => $breed];
        return $q->where($where);
    }

    /*public function medicalcards(){
        return $this->belongsTo('App\Medicalcard');
    }*/
}
