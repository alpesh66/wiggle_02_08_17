<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TechnicianBreak extends Model
{
    //
	protected $table = 'technician_breaks';

    protected $fillable = ['provider_id','technician_id', 'weekday', 'breakstart','breakend','status','created_at','updated_at'];

    public function technician()
    {
    	return $this->belongsTo('App\Technician');
    }

    public function getBreakstartAttribute($val)
    {
        return Carbon::parse($val)->format('H:i');
    }

    public function getBreakendAttribute($val)
    {
        return Carbon::parse($val)->format('H:i');
    }

}
