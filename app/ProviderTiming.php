<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderTiming extends Model
{
    //
    protected $fillable = ['provider_id','weekoff','weekday','starttime','endtime','status','created_at','updated_at'];

    public function provider()
    {
    	return $this->belongsTo('App\Provider');
    }

    
}
