<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        //Commands\CustomCommand::class,
        Commands\SendConfirmationEmailToCustomer::class,
        /*Commands\VaccinationReminder::class,*/
        Commands\VaccinationSchedule::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('customer:sendreminder')->everyMinute();
        $schedule->command('vaccinationschedule:remindervaccination')->everyMinute();
        
        //$schedule->command('custom:command')->cron('* */4 * * * *');
        //$schedule->command('vaccination:reminder')->everyMinute();
    }
}
