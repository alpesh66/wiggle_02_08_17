<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DB;
use App\VaccinationTypes;
use App\Customer;
use App\Pet;
use App\Inbox;
use App\Breed;
use Illuminate\Support\Facades\Config;

class VaccinationReminder extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vaccination:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Vaccination chart wise reminders to customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->dob_formate = 'm/d/Y';
        $this->current_time = date($this->dob_formate);
        $this->utc_time = time();
        // vaccination reminder subject
        $this->reminder_subject = push_messages('vaccination_reminder.en.title');

        $this->logFile = 'storage/logs/vaccination_reminder_' . date('d-m-Y') . '.txt';

        file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            // get all types of breed
            $getAllBreedTypes = Breed::Active()->get();
            file_put_contents($this->logFile, "\n\n ********* BREED TYPES ************ \n" . json_encode($getAllBreedTypes) . "\n\n", FILE_APPEND | LOCK_EX);

            if (empty($getAllBreedTypes->all())) {
                file_put_contents($this->logFile, "\n\n breed types not found.... \n\n", FILE_APPEND | LOCK_EX);
                exit;
            }

            foreach ($getAllBreedTypes->all() as $breedKey => $breedValue) {
                // get all types of vaccination as per breed types
                $getAllVaccTypes = VaccinationTypes::allTypes($breedValue->id)->get();
                file_put_contents($this->logFile, "\n\n  ********* VACCINATION TYPES ************ \n" . json_encode($getAllVaccTypes) . "\n\n", FILE_APPEND | LOCK_EX);

                if (empty($getAllVaccTypes->all())) {
                    file_put_contents($this->logFile, "\n\n vaccination types not found.... \n\n", FILE_APPEND | LOCK_EX);
                    exit;
                }

                foreach ($getAllVaccTypes->all() as $vaccKey => $vaccValue) {
                    // get all pets as per breed and vaccination duration.
                    $vaccination_duration = Carbon::now()->subWeeks($vaccValue->vaccination_duration)->toDateString();
                    // \DB::enableQueryLog();
                    $getAllPets = Pet::select('*')
                            ->where('pets.dob', $vaccination_duration)
                            ->where('pets.breed', $breedValue->id)
                            ->rightjoin('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
                            ->rightjoin('customers', 'customer_pet.customer_id', '=', 'customers.id')
                            ->get();

                    //dd(\DB::getQueryLog());
                    file_put_contents($this->logFile, "\n\n ********* GET PET WITH " . $vaccValue->vaccination_duration . " WEEKS BORN ON " . $vaccination_duration . " ************ \n" . json_encode($getAllPets) . "\n\n", FILE_APPEND | LOCK_EX);
                    if (is_array($getAllPets->all()) && count($getAllPets->all()) > 0) {
                        foreach ($getAllPets->all() as $key => $value) {

                            // store message in "inboxes" table
                            $reminder['message'] = "We recommend you " . $vaccValue->type . " vaccination for your " . $breedValue->name . " " . $value->name . " at " . $vaccValue->vaccination_duration . " weeks. Check with your veterinarian.";
                            $reminder['provider_id'] = 0;
                            $reminder['service_id'] = 0;
                            $reminder['booking_id'] = 0;
                            $reminder['isnew'] = 0;
                            $reminder['hadread'] = 1;
                            $reminder['type'] = 1;
                            $reminder['customer_id'] = $value->customer_id;
                            $reminder['sender'] = 1;
                            $reminder['subject'] = $this->reminder_subject;
                            file_put_contents($this->logFile, "\n\n ********* INSERT REMINDER IN INBOX ************ \n  : " . json_encode($reminder) . "\n\n", FILE_APPEND | LOCK_EX);
                            Inbox::Create($reminder);
                            //send_push_notification(push_messages('vaccination_reminder.en.message'), push_messages('vaccination_reminder.ar.message'), $value->customer_id);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            file_put_contents($this->logFile, "\n\n Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
        }
    }

    /**
     * Get difference of week between two dates
     * 
     * $this->datediffInWeeks(m/d/Y, m/d/Y);
     * 
     * @param type $date1
     * @param type $date2
     * @return type
     */
    public function datediffInWeeks($date1, $date2) {
        if ($date1 > $date2) {
            return $this->datediffInWeeks($date2, $date1);
        }
        $first = DateTime::createFromFormat('m/d/Y', $date1);
        $second = DateTime::createFromFormat('m/d/Y', $date2);
        return floor($first->diff($second)->days / 7);
    }

}
