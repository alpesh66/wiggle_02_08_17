<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DB;
use App\VaccinationTypes;
use App\Customer;
use App\Pet;
use App\Inbox;
use App\Breed;
use Illuminate\Support\Facades\Config;

class VaccinationSchedule extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vaccinationschedule:remindervaccination';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Vaccination chart wise reminders to customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        /*$this->dob_formate = 'm/d/Y';
        $this->current_time = date($this->dob_formate);
        $this->utc_time = time();
        // vaccination reminder subject
        $this->reminder_subject = push_messages('vaccination_reminder.en.title');*/

        //$this->logFile = 'storage/logs/vaccination_reminder_alpesh_' . date('d-m-Y') . '.txt';

        //file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try 
        {
          $current_date = date('Y-m-d');
          $cdate = date_create($current_date);
          date_modify($cdate, '+1 day');
          $tomordate = date_format($cdate, 'Y-m-d');
          /*--------This is for vaccination--------*/
          $pet_detail = Pet::select('id','name','dob','next_vaccination_date','vaccination_type_id','breed')->where('next_vaccination_date',$tomordate)->get()->toArray();
          foreach ($pet_detail as $petkey => $petvalue) {
            $is_send_count = 0;
            $is_send = false;
            $week = 0;
          
            $customer = \DB::table('customer_pet')->select('customers.id','customers.firstname','customers.lastname','customers.email','customer_pet.customer_id')
                ->leftjoin('customers', 'customer_pet.customer_id', '=', 'customers.id')
                ->where('customer_pet.pet_id',$petvalue['id'])
                ->get();

                if(!empty($customer)){
                  $customer_id = $customer[0]->id;
                  $customer_email = $customer[0]->email;
                  $customer_name = $customer[0]->firstname/*.' '.$customer[0]->lastname*/;
                  $pet_id = $petvalue['id'];
                  $pet_name = $petvalue['name'];
                  $vaccination_type_id = $petvalue['vaccination_type_id'];
                  $vaccination_name = "";
                  
                  $vaccination = \DB::table('vaccination_type')->select('type')
                      ->where('id',$petvalue['vaccination_type_id'])
                      ->first();
                  $vaccination_name = isset($vaccination->type) ? $vaccination->type : "";
                  $is_send_count = \DB::table('vaccinationScheduleSent')->select('id')
                      ->where('pet_id',$pet_id)
                      ->where('customer_id',$customer_id)
                      ->where('vaccination_type_id',$vaccination_type_id)
                      ->where('type','vaccination')
                      ->where('for_date',$tomordate)
                      ->count();
                  if($is_send_count==0){
                    \DB::table('vaccinationScheduleSent')->insert(
                        ['pet_id' => $pet_id, 'customer_id' => $customer_id, 'vaccination_type_id' => $vaccination_type_id,'created_at'=>$current_date,'type'=>'vaccination','for_date'=>$tomordate]
                    );
                    $message_id=1;
                    $email = array($customer_email);
                    /*$email = array('alpesh66@yopmail.com');*/
                    /*$bcc_email = array();*/
                    $bcc_email = env('SIKW_BCC_COPY', false);
                    $data = array('customer_name' => $customer_name, 'vaccination_name' => $vaccination_name,'pet_name' => $pet_name);
                    $result = Mail::send('emails.vaccination_reminder', $data, function ($message) use($email, $bcc_email) {
                        $message->to($email);
                        $message->subject(push_messages('vaccination_reminder.en.title'));
                        if ($bcc_email) {
                          $message->bcc($bcc_email);
                        }
                    });
                    $reminder['message'] = "Dear ".$customer_name.",<br/>We would like to remind you that ".$pet_name."'s ".$vaccination_name." vaccination is due soon. Hurry up! book an appointment.";
                    $reminder['provider_id'] = 5000;
                    $reminder['service_id'] = 0;
                    $reminder['booking_id'] = 0;
                    $reminder['isnew'] = 0;
                    $reminder['hadread'] = 1;
                    $reminder['type'] = 1;
                    $reminder['customer_id'] = $customer_id;
                    $reminder['sender'] = 1;
                    $reminder['subject'] = push_messages('vaccination_reminder.en.title');
                    $message = Inbox::Create($reminder);         
                    $message_id = $message->id;

                    sendPushNoti(push_messages('vaccination_reminder.en.message'), push_messages('vaccination_reminder.ar.message'),$customer_id, $message_id);
                  }

                }

                /*--------This is for vaccination end--------*/
            }
                /*--------This is for gromming start--------*/
                $pet_detail = Pet::select('id','name','created_at','next_groom_appointment','breed')->where('next_groom_appointment',$tomordate)->get()->toArray();
                foreach ($pet_detail as $petkey => $petvalue) {
                  $is_send_count = 0;
                  $is_send = false;
                  $week = 0;
                
                  $customer = \DB::table('customer_pet')->select('customers.id','customers.firstname','customers.lastname','customers.email','customer_pet.customer_id')
                      ->leftjoin('customers', 'customer_pet.customer_id', '=', 'customers.id')
                      ->where('customer_pet.pet_id',$petvalue['id'])
                      ->get();

                      if(!empty($customer)){
                        $customer_id = $customer[0]->id;
                        $customer_email = $customer[0]->email;
                        $customer_name = $customer[0]->firstname/*.' '.$customer[0]->lastname*/;
                        $pet_id = $petvalue['id'];
                        $pet_name = $petvalue['name'];
                        $is_send_count = \DB::table('vaccinationScheduleSent')->select('id')
                            ->where('pet_id',$pet_id)
                            ->where('customer_id',$customer_id)
                            ->where('type','gromming')
                            ->where('for_date',$tomordate)
                            ->count();
                        if($is_send_count==0){
                          \DB::table('vaccinationScheduleSent')->insert(
                              ['pet_id' => $pet_id, 'customer_id' => $customer_id, 'created_at'=>$current_date,'type'=>'gromming','for_date'=>$tomordate]
                          );
                          $message_id=1;
                          $email = array($customer_email);
                          //$email = array('alpesh66@yopmail.com');
                          $bcc_email = array();
                          $bcc_email = env('SIKW_BCC_COPY', false);
                          $data = array('customer_name' => $customer_name,'pet_name' => $pet_name);
                          $result = Mail::send('emails.gromming_reminder', $data, function ($message) use($email, $bcc_email) {
                              $message->to($email);
                              $message->subject(push_messages('gromming_reminder.en.title'));
                              if ($bcc_email) {
                                $message->bcc($bcc_email);
                              }
                          });
                          $reminder['message'] = "Dear ".$customer_name.",<br/>We would like to remind you that ".$pet_name."'s  gromming is due soon. Hurry up! book an appointment.";
                          $reminder['provider_id'] = 5000;
                          $reminder['service_id'] = 0;
                          $reminder['booking_id'] = 0;
                          $reminder['isnew'] = 0;
                          $reminder['hadread'] = 1;
                          $reminder['type'] = 1;
                          $reminder['customer_id'] = $customer_id;
                          $reminder['sender'] = 1;
                          $reminder['subject'] = push_messages('gromming_reminder.en.title');
                          $message = Inbox::Create($reminder);         
                          $message_id = $message->id;

                          sendPushNoti(push_messages('gromming_reminder.en.message'), push_messages('gromming_reminder.ar.message'),$customer_id, $message_id);
                        }
                      }
                      /*--------This is for gromming end--------*/
                }
        } 
        catch (Exception $e) {
          file_put_contents($this->logFile, "\n\n Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
        }
    }

}
