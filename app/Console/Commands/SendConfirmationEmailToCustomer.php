<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Booking;
use App\Inbox;
use Log;
use Mail;

class SendConfirmationEmailToCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:sendreminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will send email and push notification to the customer with confirm link to confirm there appointment which is scheduled after 4Hrs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->logFile = 'storage/logs/CustomCommand_' . date('d-m-Y') . '.txt';        
        $this->logFileCancel = 'storage/logs/BookingCancel_' . date('d-m-Y') . '.txt';        

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        file_put_contents($this->logFile, "\n\n =========Booking Appoitment[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
        $current_date = Carbon::now()->addHour(4)->format('Y-m-d H:i:00');
        
        //echo date('Y-m-d H:i:00','1502276331');
        //echo $strtotimeCurdate = strtotime($current_date);
        $bookings = Booking::leftjoin('customers', 'bookings.customer_id', '=', 'customers.id')
              ->leftjoin('technicians', 'bookings.technician_id', '=', 'technicians.id')
              ->leftjoin('pets', 'bookings.pet_id', '=', 'pets.id')
              ->leftjoin('providers', 'bookings.provider_id', '=', 'providers.id')
              ->leftjoin('services', 'bookings.service_id', '=', 'services.id')
              ->leftjoin('categories', 'services.category_id', '=', 'categories.id')
              ->where('bookings.start', $current_date)
              ->where('bookings.status', 0)
              ->select(['customers.firstname','customers.middlename','customers.lastname','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename','categories.image as category_image','bookings.start', 'bookings.type_id', 'bookings.status','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes', 'customers.id as customer_id', 'services.id as service_id'])
              ->orderby('bookings.start', 'desc')
              ->get();
          
      foreach ($bookings as $key => $booking) {
        $message = "Dear $booking->firstname,<br/>We would like to remind you of your appointment with $booking->provider_name for ";
        $date = $booking[$key]['start'];
        $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
        $inbox['provider_id'] = $booking->provider_id;
        $inbox['customer_id'] = $booking->customer_id;
        $inbox['service_id'] = $booking->service_id;
        $inbox['booking_id'] = $booking->booking_id;
        $inbox['bookdate'] = Carbon::parse($booking->start);
        $inbox['isnew'] = 1;
        $inbox['hadread'] = 1;
        $inbox['type'] = 1;  //Reminder Message
        $inbox['subject'] = push_messages('appointment_remider.en.title');
        $email_m = "";
        $message .= " $booking->petname: "."$booking->servicename";
        $email_m .= " $booking->petname: "."$booking->servicename";
        $message .= " at {$good_date}.";
        $email_m .= " at {$good_date}.";
        $inbox['message']=$message;
        $image_url = 'images/category.png';
        if (!empty($booking->category_image)) {
            if (file_exists('uploads/categories/' . $booking->category_image)) {
                $image_url = 'uploads/categories/' . $booking->category_image;
            }
        }
        $emailHtml[$booking->petname][0] = array(
            'category_image' => $image_url,
            'servicename' => $booking->servicename
        );

        $message_obj = Inbox::Create($inbox);         
        $message_id = $message_obj->id;
        $pushMessage_en=push_messages('appointment_remider.en.message');
        $pushMessage_ar=push_messages('appointment_remider.ar.message');
        $extraData=array('type'=>'APPOINTMENT','reminder'=>TRUE);
        $email = array($booking->email);
        //$email = array('alpesh66@yopmail.com');
        $bcc_email = array();
        $bcc_email = env('SIKW_BCC_COPY', false);

        $data['order_details'] = $emailHtml;
        $data['customer_name'] = $booking->firstname;
        $data['provider_name'] = $booking->provider_name;
        $data['booking_date'] = Carbon::parse($booking->start)->format('F d Y');
        $data['booking_time'] = Carbon::parse($booking->start)->format('h:i a');


        $result = Mail::send('emails.bookingEmailtoCustomerReminder', $data, function ($message) use($email, $bcc_email) {
            $message->to($email);
            $message->subject('Appointment Reminder');
            if ($bcc_email) {
              $message->bcc($bcc_email);
            }
        });
        sendPushNoti($pushMessage_en,$pushMessage_ar,$booking->customer_id,$message_id);
        file_put_contents($this->logFile, "\n\n ********* Success Reminder of Appointment ************ \n" . json_encode($message) . "\n\n", FILE_APPEND | LOCK_EX);
      }

        //--this for cancel unconfirm appointment before 1 hour
        $current_date = Carbon::now()->addHour(1)->format('Y-m-d H:i:00');
          file_put_contents($this->logFileCancel, "\n\n =========booking cancel before 1 HOUR Start[" . $current_date . "] =========\n\n", FILE_APPEND | LOCK_EX);
              
              //$current_date = '2017-07-15 09:15:00';
              $sikw_email = env('SIKW_BCC_COPY', false);
              $ontimeobj = Booking::select('ontime')->where('start', $current_date)->where('status', 0)->first();
              if(!empty($ontimeobj)){
                file_put_contents($this->logFileCancel, "\n\n =========ontime [" . $ontimeobj->ontime . "] =========\n\n", FILE_APPEND | LOCK_EX);
                $bookobj = Booking::where('ontime', $ontimeobj->ontime)->get();
                foreach ($bookobj as $key => $value) {                 
                    $bookings1['booking'][] = $value->id;                       
                    $vaccData['status'] = 2;           
                    $r = Booking::where('id', $value->id)->update($vaccData); 
                }
                file_put_contents($this->logFileCancel, "\n\n =========ontime [" .  json_encode($bookings1) . "] =========\n\n", FILE_APPEND | LOCK_EX);
                if(!empty($bookings1)){
                  file_put_contents($this->logFileCancel, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
                  file_put_contents($this->logFileCancel, "\n\n =========booking cancel: " . json_encode($bookings1) . "\n\n", FILE_APPEND | LOCK_EX);
                  foreach ($bookings1 as $value1) {
                    foreach ($value1 as $key => $bookData) {
                        $book = Booking::SingleBookingDetail($bookData)->first();
                        file_put_contents($this->logFileCancel, "\n\n =========book cancel : " . json_encode($book) . "\n\n", FILE_APPEND | LOCK_EX);
                        if ($book) {
                            $date = $book['start'];
                            $good_date = Carbon::parse($date)->format('l, M jS Y');
                            $email_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                            $booking[$key]= $book;
                            $petdata[$book['pet_id']]['petname'][] = $book['petname'];
                            $petdata[$book['pet_id']]['services'][] = $book['servicename'];
                            $servicesArray[$key] = $book['servicename'];
                          
                            if (!empty($book['category_image'])) {
                                //if (file_exists('uploads/categories/' . $book['category_image'])) {
                                    $image_url = 'uploads/categories/' . $book['category_image'];
                               /* }else{
                                  $image_url = 'images/category.png';    
                                }*/
                            }
                            else{
                              $image_url = 'images/category.png';
                            }
                            $emailHtml[$book['petname']][$key] = array(
                                'category_image' => $image_url,
                                'servicename' => $book['servicename']
                            );

                            file_put_contents($this->logFileCancel, "\n\n =========Pet Detail : " . json_encode($emailHtml) . "\n\n", FILE_APPEND | LOCK_EX);
        //                   Send Email to Provider, Customer and Admin
                            $customer_email = $book['customer_email'];
                            $provider_email = $book['providers_email'];
                            $email_datacustomer_name = $book['firstname'];
                            $email_dataprovider_name = $book['provider_name'];
                            $customers_mobile = $book['customers_mobile'];
                            $booking_date = Carbon::parse($date)->format('F d Y');
                            $booking_time = Carbon::parse($date)->format('h:i a');
                            //\App\Inbox::create($booking);
                        }
                    }
                  }
                  file_put_contents($this->logFileCancel, "\n\n =========Pet Detail1 : " . json_encode($emailHtml) . "\n\n", FILE_APPEND | LOCK_EX);
                  $messageCommon = "";
                  $uservice = array_unique($servicesArray);
               
                  $customer_id = "";
                  if (!empty($booking)) {
                    $pushMessage_en=push_messages('cancel_appointment.en.message');
                    $pushMessage_ar=push_messages('cancel_appointment.ar.message');
                   
                    $message1 = "Dear {$booking[0]['firstname']},<br/>We would like to inform you that your appointment with {$booking[0]['provider_name']} is canceled for ";
                    $date = $booking[0]['start'];
                    $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                    $inbox['provider_id'] = $booking[0]['provider_id'];
                    $inbox['customer_id'] = $booking[0]['customer_id'];
                    $inbox['service_id'] = $booking[0]['service_id'];
                    $inbox['booking_id'] = $booking[0]['bookid'];
                    $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
                    $inbox['isnew'] = 1;
                    $inbox['hadread'] = 1;
                    $inbox['type'] = 2;  // 1 for reminder message , 0 for inbox massage 
                    $inbox['subject'] = push_messages('cancel_appointment.en.title');
                    foreach ($petdata as $pet) {
                        $upet=array_unique($pet['petname']);
                        $messageCommon .= implode(",", $upet)." :" . implode(",", $uservice);
                    }
                    $message2 = " on {$good_date} .";
                    $customer_id = $booking[0]['customer_id'];
                  }
                  $message = $message1.' '.$messageCommon.' '.$message2;
                  
                  $inbox['message'] = $message;
                  $message = \App\Inbox::create($inbox);
                  $message_id = $message->id;
                  $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
                  $data['order_details'] = $emailHtml;
                  $data['customer_name'] = $email_datacustomer_name;
                  $data['provider_name'] = $email_dataprovider_name;
                  $data['booking_date'] = Carbon::parse($booking[0]['start'])->format('F d Y');
                  $data['booking_time'] = Carbon::parse($booking[0]['start'])->format('h:i a');
                  $data['customers_mobile'] = $customers_mobile;
                  file_put_contents($this->logFileCancel, "\n\n =========data : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);                
                  
                  //send email to customer
                  if ($customer_email) {
                      Mail::queue('emails.bookingCancelEmailtoCustomer', $data, function ($message) use ($customer_email, $sikw_email) {
                          $message->to($customer_email);
                          $message->subject('Wiggle - Booking Cancelled');
                          if ($sikw_email) {
                              $message->bcc($sikw_email);
                          }
                      });
                  }
                  else{
                     file_put_contents($this->logFileCancel, "\n\n =========No Customer email =========\n\n", FILE_APPEND | LOCK_EX);
                  }

                  //  send email to provider
                  if ($provider_email) {
                    Mail::queue('emails.bookingCancelEmailtoProvider', $data, function ($message) use ($provider_email, $sikw_email) {
                        $message->to($provider_email);
                        $message->subject('Wiggle - Booking Cancelled');
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });
                  }
                  else{
                    file_put_contents($this->logFileCancel, "\n\n =========No provider email =========\n\n", FILE_APPEND | LOCK_EX);
                  }
                  file_put_contents($this->logFileCancel, "\n\n =========before push send".$customer_email." pemail :".$provider_email." \n\n", FILE_APPEND | LOCK_EX);
                  sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
                  file_put_contents($this->logFileCancel, "\n\n =========push send \n\n", FILE_APPEND | LOCK_EX);
                  file_put_contents($this->logFileCancel, "\n\n ========booking cancel before 1 HOUR END==========\n\n", FILE_APPEND | LOCK_EX);
                }else{
                  file_put_contents($this->logFileCancel, "\n\n =========oops! no booking data  =========\n\n", FILE_APPEND | LOCK_EX);
                }  
              }
        file_put_contents($this->logFileCancel, "\n\n =========booking cancel before 1 HOUR End[" . $current_date . "] =========\n\n", FILE_APPEND | LOCK_EX);
        //--
        /*--==this for all past request*/
       /* $ontimeobjpast = Booking::select('ontime')->where('start','<=',$current_date)->where('status', 0)->get();
        if(!empty($ontimeobjpast)){
          foreach ($ontimeobjpast as $keypast => $value1) {
            $bookpastobj = Booking::where('ontime', $value1->ontime)->get();
            foreach ($bookpastobj as $keyp1 => $value2) {
                $vaccData1['status'] = 2;           
                $r1 = Booking::where('id', $value2->id)->update($vaccData1); 
            }
          }
        }*/
        /*--==*/
    }
}
