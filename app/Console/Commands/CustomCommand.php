<?php

namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DB;
use \App\Pet;

class CustomCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->logFile = 'storage/logs/CustomCommand_' . date('d-m-Y') . '.txt';
        file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $yesterdayDate = Carbon::tomorrow();
        $getPetsBookAppointmetReminders = \App\Pet::with('customer')->where('next_groom_appointment', $yesterdayDate)->get();

        if (is_array($getPetsBookAppointmetReminders->all()) && count($getPetsBookAppointmetReminders->all()) > 0) {
            foreach ($getPetsBookAppointmetReminders->all() as $key => $value) {
                $lastAppointmentBookedProvider = \App\Booking::join('providers', 'bookings.provider_id', '=', 'providers.id')->where('pet_id', $value->id)->orderBy('bookings.id', 'desc')->get();
                $email = array($value->Customer[0]['email']);

                $data = array(
                    'provider_name' => isset($lastAppointmentBookedProvider[0]['name']) && !empty($lastAppointmentBookedProvider[0]['name']) ? trim($lastAppointmentBookedProvider[0]['name']) : 'Provider',
                    'petname' => $value->name,
                    'customer_name' => $value->Customer[0]['firstname'] . ' ' . $value->Customer[0]['lastname'],
                    'booking_date' => Carbon::parse($value['next_groom_appointment'])->format('F d Y'),
                    'booking_time' => Carbon::parse($value['next_groom_appointment'])->format('h:i a')
                );

                try {
                    $bcc_email = env('SIKW_BCC_COPY', false);
                    Mail::queue('grommingremindermail', $data, function ($message) use ($email,$bcc_email) {
                        $message->to($email, 'Pet Grooming Reminder')->subject("Pet Grooming Reminder");
                        if ($bcc_email) {
                            $message->bcc($bcc_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    file_put_contents($this->logFile, "\n\n =========CustomCommand Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    file_put_contents($this->logFile, "\n\n =========CustomCommand Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    log_message("ERROR", $e->getMessage());
                }

                $message = $value->name . ' Pet  grooming service due on ' . $value['next_groom_appointment'];
                $inboxMessage = \App\Inbox::create(['provide_id' => '0', 'customer_id' => $value->Customer[0]['id'], 'service_id ' => '0', 'booking_id ' => '0', 'isnew' => '1', 'hadread' => '1', 'subject' => 'Pet Grooming Reminder', 'message' => $message, 'bookdate' => $value['next_groom_appointment'], 'type' => '1']);
                $updateGroomAppointment = \App\Pet::where('id', '=', $value['id'])->update(['next_groom_appointment' => '0000-00-00']);
            }
        }
    }

}
