<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','name_ar', 'desc', 'desc_ar', 'startdate', 'enddate', 'weblink', 'address', 'latitude', 'longitude', 'status','image','image_ar'];


    public function area()
    {
        return $this->belongsTo('\App\Area');
    }

    public function setStartdateAttribute($value)
    {
        $this->attributes['startdate'] = Carbon::parse($value);
    }

    public function getStartdateAttribute($date)
    {
        return Carbon::parse($date)->format('m/d/Y H:i');
    }

    public function setEnddateAttribute($value)
    {
        $this->attributes['enddate'] = Carbon::parse($value);
    }

    public function getEnddateAttribute($date)
    {
        return Carbon::parse($date)->format('m/d/Y H:i');
    }

    public function scopeAPIEvents($q)
    {
        $dt = Carbon::now();
        return $q->where('enddate', '<=', $dt)->whereStatus(1);
    }

    public function getImageAttribute($image)
    {
        return ($image == '')? '' : url('uploads/events').'/'.$image;
    }

    public function getImageArAttribute($image)
    {
        return ($image == '')? '' : url('uploads/events').'/'.$image;
    }
    
    public function customers(){
        return $this->belongsToMany('App\Customer', 'event_likes', 'event_id', 'customer_id');
    }
}
