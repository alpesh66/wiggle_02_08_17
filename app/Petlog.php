<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petlog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pet_log';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pet_id','provider_id','admin_id','customer_id','status'];
    protected $hidden = ['created_at', 'updated_at'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer','customer_id','id');
    }

    public function pet()
    {
       return  $this->belongsTo('App\Pet','pet_id','id');
    }
    
    public function providers()
    {
       return  $this->belongsTo('App\Provider','provider_id','id');
    }
}
