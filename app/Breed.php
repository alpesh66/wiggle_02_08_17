<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    protected $hidden = ['updated_at', 'created_at'];
    
    public function services()
    {
        return $this->belongsToMany('App\Service');
    }

    public function scopeActive($q)
    {
        return $this->where('status',1);
    }
}
