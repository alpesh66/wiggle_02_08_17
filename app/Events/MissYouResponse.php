<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MissYouResponse extends Event
{
    use SerializesModels;
    public $missyouId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($missyouId)
    {
        $this->missyouId = $missyouId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
