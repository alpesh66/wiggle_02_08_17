<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class KennelEvent extends Event
{
    use SerializesModels;

    public $foster;
   // public $status;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($listener)
    {
        
       echo $this->kennel=$listener;
     
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
