<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FosterEvent extends Event
{
    use SerializesModels;

    public $foster;
   // public $status;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($listener)
    {
       $this->foster=$listener;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
