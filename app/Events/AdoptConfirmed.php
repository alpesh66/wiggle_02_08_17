<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AdoptConfirmed extends Event
{
    use SerializesModels;

    public $adopt;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($adopt)
    {
        $this->adopt=$adopt;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
