<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SurrenderConfirmed extends Event
{
    use SerializesModels;

    public $surrender;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($surrender)
    {
        $this->surrender=$surrender;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
