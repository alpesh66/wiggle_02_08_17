<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendReminderofBooking extends Event
{
    use SerializesModels;

    public $ontime;
    public $customer_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ontime, $customer_id)
    {
        $this->ontime = $ontime;
        $this->customer_id = $customer_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
