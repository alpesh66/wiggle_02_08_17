<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surrender extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'surrenders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','description','title','sender','provider_id','startdate','enddate','status','parent_id','is_read'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer');
    }

    public function pet()
    {
       return  $this->belongsToMany('App\Pet');
    }

    public function scopeallMessage($q)
    {
        $where = ['provider_id' => session('provider_id'), 'sender' => 1 ];
        return $q->where($where);
    }
    public function scopeSingleBookingDetail($q, $book_id)
    {
       // echo $book_id;
        return $q->join('customers', 'surrenders.customer_id', '=', 'customers.id')
                ->join('pet_surrender', 'surrenders.id', '=', 'pet_surrender.surrender_id')
                ->join('providers', 'surrenders.provider_id', '=', 'providers.id')
                ->where('surrenders.id', $book_id)
                ->select(['surrenders.id as surrenders_id'
                    ,'customers.firstname as customer_name'
                    , 'customers.firstname as title'
                    ,'pet_surrender.pet_id as petid'
                    ,'providers.id as provider_id', 'customers.id as customer_id'
                    ,'customers.email as customer_email'
                    , 'providers.email as providers_email', 'providers.name as provider_name'
                    ,]);
    }
}
