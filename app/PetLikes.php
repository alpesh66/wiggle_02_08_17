<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetLikes extends Model
{
    protected $fillable = ['pet_id', 'customer_id'];

    public $timestamps = false; 
}
