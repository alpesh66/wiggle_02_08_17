<?php

namespace App;

use Zizaco\Entrust\EntrustRole;
use Config;
class Role extends EntrustRole
{
    protected $fillable = [
        'name', 'display_name', 'description', 'provider_id', 'type'
    ];


    public function addmyRole($data)
    {
        $data['name'] = str_random(40);
        return $this->create($data);
    }

    /**
     * Many-to-Many relations with the user model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(Config::get('auth.providers.users.model'), Config::get('entrust.role_user_table'),Config::get('entrust.role_foreign_key'),Config::get('entrust.user_foreign_key'));
       // return $this->belongsToMany(Config::get('auth.model'), Config::get('entrust.role_user_table'));
    }

}
