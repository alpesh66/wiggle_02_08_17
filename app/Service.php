<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['provider_id', 'category_id', 'status'];

    protected $hidden = ['updated_at', 'created_at'];

    public function breed()
    {
        return $this->belongsToMany('App\Breed')->select(['breed_id', 'size_id', 'price', 'servicetime']);
    }    

    public function size()
    {
        return $this->belongsTo('App\Size');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getStatusAttribute($status)
    {
        return (boolean)$status;
    }

    public function technician()
    {
        return $this->belongsToMany('App\Technician')->where('status', 1);
    }

    public function scopeActive($q)
    {
        return $q->whereStatus(1);
    }

    public function scopeGetTechnicians($q, $service)
    {
        return $q->whereHas('technician', function ($query) {
                $query->where('status', '=', '1');
            })
            ->where('id', $service)
            ->get();
    }
}
