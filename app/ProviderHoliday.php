<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderHoliday extends Model
{
    //
    protected $fillable = ['provider_id','start','end','starttime','type','endtime','status','created_at','updated_at'];
}
