<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $fillable = ['amount', 'customer_id', 'device_id','PaymentID','result','PostDate','TranID','Auth','Ref','TrackID','ip_address'];    

    // public function setResultAttribute($value)
    // {
    //     if($value == 'CAPTURED')
    //     {
    //         $value = 1;
    //     } else {
    //         $value = 0;
    //     }
    //     $this->attribute['result'] = ($value == 'CAPTURED') ? '1' : '0';
    // }

    
	public function customer()
	{
		return $this->belongsTo('\App\Customer');
	}

    // public function getResultAttribute($res)
    // {
    //     return ($res == 'CAPTURED')? '1' : '0';
    // }
}
