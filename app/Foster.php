<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Foster extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fosters';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','description','title','sender','provider_id','startdate','enddate','parent_id','status', 'breed_id','is_read'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer');
    }

    public function pet()
    {
       return  $this->belongsToMany('App\Pet');
    }

    public function scopeallMessage($q)
    {
        $where = ['provider_id' => session('provider_id'), 'sender' => 1 ];
        return $q->where($where);
    }

    public function setStartdateAttribute($value)
    {
        $this->attributes['startdate'] = Carbon::parse($value);
    }

    public function setEnddateAttribute($value)
    {
        $this->attributes['enddate'] = Carbon::parse($value);
    }
    public function scopeSingleBookingDetail($q, $book_id)
    {
       // echo $book_id;
        return $q->join('customers', 'fosters.customer_id', '=', 'customers.id')
                ->leftjoin('foster_pet', 'fosters.id', '=', 'foster_pet.foster_id')
            ->join('providers', 'fosters.provider_id', '=', 'providers.id')
                ->where('fosters.id', $book_id)
                ->select(['fosters.id as foster_id','fosters.startdate as startdate'
                    ,'fosters.enddate as enddate','customers.firstname as customer_name'
                    , 'customers.firstname as title'
                    ,'foster_pet.pet_id as petid'
                    ,'providers.id as provider_id', 'customers.id as customer_id'
                    ,'customers.email as customer_email'
                    , 'providers.email as providers_email', 'providers.name as provider_name'
                    ,]);
    }
}
