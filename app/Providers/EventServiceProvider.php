<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\BookingWasMade' => [
            'App\Listeners\BookingInboxMessage',
            'App\Listeners\SendBookingEmail',
        ],
        'App\Events\MissYouResponse' => [
            'App\Listeners\MissYouResponseListener',
        ],

        'App\Events\SendReminderofBooking' => [
            'App\Listeners\BookingReminder',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\AdminLogout'
        ],
        'App\Events\CustomerCreated' => [
            'App\Listeners\WelcomeEmail',
        ],
        'App\Events\BookingConfirmed' => [
            'App\Listeners\BookingConfirmedListener',
        ],
        'App\Events\AdoptConfirmed' => [
            'App\Listeners\AdoptConfirmedListener',
        ],
        'App\Events\FosterConfirmed' => [
            'App\Listeners\FosterConfirmedListener',
        ],
        'App\Events\SurrenderConfirmed' => [
            'App\Listeners\SurrenderConfirmedListener',
        ],
        // 'Illuminate\Auth\Events\Login' => [
        //     'App\Listeners\LogSuccessfulLogin',
        // ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
