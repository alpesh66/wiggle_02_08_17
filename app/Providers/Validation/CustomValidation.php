<?php

namespace App\Providers\Validation;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;
use Intervention\Image\ImageManagerStatic;
/**
* New Validation for Uniqueness
*/
class CustomValidation extends Validator
{
    
    public function validateUniqueWith($attribute, $value, $parameters, $validator)
    {

        // $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
        //     $query->where($attribute, '=', $value) // where firstname = value
        //         ->Where($parameters[1], '=', $value) // where lastname = value
        //         ->Where($parameters[2], '=', $value); // where lastname = value
        // })->first();

        // return $result ? false : true;
        // 
        
        // data being validated
        $data = $validator->getData();

        // remove whitespaces
        $parameters = array_map( 'trim', $parameters );

        // remove first parameter and assume it is the table name
        $table = array_shift( $parameters );

        // remove last parameter to check for except condition
        $lastParameter = array_pop( $parameters );

        // start building the query
        $query = DB::table( $table )->select( DB::raw( 1 ) );

        // add the field being validated as a condition
        // IMPORTANT: skipping it for improved consistency, see
        // note in the function's comment
        // $query->where( $attribute, $value );

        // iterates over the parameters and add as where clauses
        while ($field = array_shift( $parameters )) {
            $query->where( $field, array_get( $data, $field ) );
        }

        // check $lastParameter for except condition. Uses a regular
        // expression to check if $lastParameter contains only numbers
        // or an equal sign
        if (preg_match( '/^(?:\d+|.+?=.+)$/', $lastParameter )) {
            // is except condition

            if (preg_match( '/^\d+$/', $lastParameter )) {
                // only numbers, assume primary key is called 'id' rewrite $lastParameter
                $lastParameter = sprintf( '%s.id = %s', $table, $lastParameter );
            }

            // negate condition
            $exceptField = sprintf( '(NOT %s)', $lastParameter );

            $query->whereRaw( $exceptField );
        } else {
            // is not except condition, add as a normal where
            $query->where( $lastParameter, array_get( $data, $lastParameter ) );
        }

        // get the result from DB
        $result = $query->first();

        return empty( $result ); // true if no result was found

    }

    public function validateServiceBreedUnique($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();
        $parameters = array_map( 'trim', $parameters );
        $table = array_shift( $parameters );

        // $result = DB::table($table->where(function($query) use ($attribute, $value, $parameters, $validator) {
        //     $query->where($attribute, '=', $value) // where firstname = value
        //         ->Where($parameters[1], '=', $value) // where lastname = value
        //         ->Where($parameters[2], '=', $value); // where lastname = value
        // })->first();

        return $result ? false : true;
    }

    public function validateMobileNumber($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();

        // remove whitespaces
        $parameters = array_map( 'trim', $parameters );

        // remove first parameter and assume it is the table name
        $table = array_shift( $parameters );

        // remove last parameter to check for except condition
        $lastParameter = array_pop( $parameters );

            $country = (array_get( $data, 'country' ));
            $mobile = (array_get( $data, 'mobile' ));
            $complete_mobile = $country.'-'.$mobile;

        // start building the query
        $query = DB::table( $table )->select( DB::raw( 1 ) );

        // add the field being validated as a condition
        // IMPORTANT: skipping it for improved consistency, see
        // note in the function's comment
        // $query->where( $attribute, $value );

        $query->where('mobile',$complete_mobile);

        // check $lastParameter for except condition. Uses a regular
        // expression to check if $lastParameter contains only numbers
        // or an equal sign
        if (preg_match( '/^(?:\d+|.+?=.+)$/', $lastParameter )) {
            // is except condition

            if (preg_match( '/^\d+$/', $lastParameter )) {
                // only numbers, assume primary key is called 'id' rewrite $lastParameter
                $lastParameter = sprintf( '%s.id = %s', $table, $lastParameter );
            }

            // negate condition
            $exceptField = sprintf( '(NOT %s)', $lastParameter );

            $query->whereRaw( $exceptField );
        } else {
            // is not except condition, add as a normal where
            $query->where( $lastParameter, array_get( $data, $lastParameter ) );
        }
               // get the result from DB
        $result = $query->first();

        return empty( $result ); // true if no result was found
    }

    public function validateValidImage($attribute, $value, $parameters, $validator)
    {
        try {
            ImageManagerStatic::make($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}