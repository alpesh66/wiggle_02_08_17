<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Provider;
use \App\Category;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.master', function($view){
            $user = \Auth::user();
            $user['pro_nameDisplay'] = '';
            if($user->provider_id > 0){
                $user_pro_nameDisplay = Provider::find($user->provider_id);
                $user['pro_nameDisplay'] = $user_pro_nameDisplay['name'];
            }

            $view->with(compact('user'));
        }); 

        // Category - Update the orderby on each save.
        Category::saved(function ($category) {
            $new_display = ($category['attributes']['display']);
            $id = ($category['attributes']['id']);
            if(isset($category['original']['display'])) {
                $old_display = $category['original']['display'];
            } else {
                $old_display = 0;
            }

            if($new_display != $old_display) {
                $get_full = Category::where('display', '>=', $new_display)->where('parentid', '!=', '0')->whereNotIn('id', [$id])->orderby('display')->get();

                if($get_full)
                {
                    foreach ($get_full as $key => $val) {
                        $new_display++;
                        // dump($val);
                        DB::table('categories')->where('id', $val['id'])->update(['display' => $new_display]);
                    }
                }
            }

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
