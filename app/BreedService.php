<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BreedService extends Model
{
    protected $table = 'breed_service';

    protected $fillable = ['breed_id', 'service_id', 'size_id','price', 'servicetime'];
}
