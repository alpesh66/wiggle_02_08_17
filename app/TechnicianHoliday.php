<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianHoliday extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'technician_holidays';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['provider_id','technician_id', 'weekday','start', 'end', 'breakstart','breakend','type','status','created_at','updated_at'];

    public function technician()
    {
        return $this->belongsTo('App\Technician');
    }
}
