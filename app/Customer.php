<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class Customer extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    // protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname','middlename', 'lastname', 'email', 'password', 'gender','api_token', 'dob', 'mobile', 'photo', 'area_id', 'address', 'status', 'code','latitude','longitude','block','street','judda','house','apartment'];

    protected $hidden = ['created_at', 'updated_at', 'password'];
    
    /**
     * @return array
     */
    public function pet()
    {
        return $this->belongsToMany('\App\Pet')->where('status','!=','0');
    }

    public function area()
    {
        return $this->belongsTo('\App\Area');
    }

    public function rating()
    {
        return $this->hasmany('\App\Rating');
    }

    public function getNameAttribute()
    {
        return $this->firstname.' '.$this->middlename.' '.$this->lastname;
    }
    
    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = Carbon::parse($value);
    }

    public function getDobAttribute($date)
    {
        if($date=="0000-00-00")
            return "";
        else
            return Carbon::parse($date)->format('m/d/Y');
    }

    public function scopeSearch($q, $value)
    {
        return $this->with('pet')->where('firstname','like', '%'.$value.'%')
                        ->orWhere('lastname', 'like', '%'.$value.'%')
                        ->orWhere('mobile', 'like', '%'.$value.'%')
                        ->orWhere('email', 'like', '%'.$value.'%')
                        ->take(20)->get();
    }

    public function createCustomer($data)
    {
        return $this->create([
            'firstname'     => $data['firstname'],
            'lastname'      => $data['lastname'],
            'email'         => $data['email'],
            'mobile'        => $data['mobile'],
            'api_token'     => str_random(60),
            'status'        => 1,

        ]);
    }

    public function getPhotoAttribute($photo)
    {
        return ($photo == '')? '' : url('uploads/customer').'/'.$photo;
    }

    public function totalPetsCount()
    {
      return $this->hasOne('\App\Pet')
            ->selectRaw('pet_id, count(*) as aggregate')
        ->groupBy('pet_id');
    }
    
    public function events(){
        return $this->belongsToMany('App\Event', 'event_likes', 'customer_id', 'event_id');
    }
}
