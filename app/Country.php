<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $hidden = ['updated_at', 'created_at', 'iso','name', 'iso3', 'numcode','id'];

    public function scopeActive($q)
    {
        return $q->orderby('created_at','desc');
    }
}
