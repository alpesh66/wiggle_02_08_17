<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = ['name', 'status'];
    
    protected $hidden = ['created_at', 'updated_at'];
    
    public function services()
    {
        return $this->hasMany('App\Service');
    }
}
