<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'volunteers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','description','title','sender','provider_id','startdate','enddate','status','parent_id','is_read'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer');
    }

    public function pet()
    {
       return  $this->belongsToMany('App\Pet');
    }

    public function scopeallMessage($q)
    {
        $where = ['provider_id' => session('provider_id'), 'sender' => 1 ];
        return $q->where($where);
    }
    public function scopeSingleBookingDetail($q, $book_id)
    {
       // echo $book_id;
        return $q->join('customers', 'volunteers.customer_id', '=', 'customers.id')
                //->join('pet_volunteer', 'pet_volunteer.volunteer_id', '=', 'volunteers.id')
                ->join('providers', 'volunteers.provider_id', '=', 'providers.id')
                
                ->where('volunteers.id', $book_id)
                
                ->select(['volunteers.id as voluntid','volunteers.startdate as startdate'
                    ,'volunteers.enddate as enddate','customers.firstname as customer_name'
                    , 'customers.firstname as title'
                    //,'pet_volunteer.pet_id as petid'
                    ,'providers.id as provider_id', 'customers.id as customer_id'
                    ,'customers.email as customer_email'
                    , 'providers.email as providers_email', 'providers.name as provider_name'
                    ,]);
    }
}
