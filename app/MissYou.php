<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MissYou extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'miss_you_request';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','message','sender','provider_id','status','parent_id'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer');
    }
    
    public function kennel()
    {
       return  $this->belongsTo('App\Kennel');
    }

    public function pet()
    {
       return  $this->belongsToMany('App\Pet');
    }

    public function scopeallMessage($q)
    {
        $where = ['provider_id' => session('provider_id'), 'sender' => 1 ];
        return $q->where($where);
    }
    
     /**
     * API for the list of Adoptions
     * @param  [type] $q [description]
     * @return [type]    [description]
     */
    public function scopeallMissYouResponse($q)
    {
        $where = ['customer_id' => \Auth::guard('mobileapp')->user()->id, 'sender' => 2 ];
        return $q->where($where);
    }
}
