<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class Booking extends Model
{
    protected $fillable = ['provider_id','customer_id', 'pet_id', 'technician_id', 'service_id', 'start', 'end', 'type_id','price', 'transaction_id', 'status', 'noshow','notes', 'ontime', 'madefrom'];

    protected $dates = ['start'];

    protected $hidden = ['updated_at'];

    protected $appends = ['resources','editable'];
    // protected $maps = [
    //     'provider_id' => 'pro',
    //     'customer_id' => 'cust',
    // ];

    public function scopeOfProvider($q)
    {
        $provider_id = session('provider_id');
        return $q->where('provider_id', $provider_id);
    }

    public function scopeTypeVeternary($q)
    {
        $type = config('callme.VETERNARY');
        return $q->where('type_id', $type);
    }

    public function scopeTypeGroomer($q)
    {
        $type = config('callme.GROOMER');
        return $q->where('type_id', $type);
    }

    public function scopeTypeWalker($q)
    {
        $type = config('callme.WALKER');
        return $q->where('type_id', $type);
    }

    public function scopeTypeTrainer($q)
    {
        $type = config('callme.TRAINER');
        return $q->where('type_id', $type);
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer');
    }

    public function technician()
    {
        return $this->belongsTo('\App\Technician');
    }

    public function pet()
    {
        return $this->belongsTo('\App\Pet');
    }

    public function category()
    {
        return $this->belongsTo('\App\Category','service_id');
    }

    public function scopeBookingListing($q, $customer_id)
    {
        return $q
            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->where('bookings.customer_id','=', $customer_id)
            ->select(['customers.firstname','customers.middlename','customers.lastname', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.end', 'bookings.type_id', 'bookings.status','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes', 'bookings.transaction_id',DB::raw('count(*) as count')])
            ->groupBy('bookings.ontime')
            ->orderby('bookings.start', 'desc');
    }

    public function scopeOnTimeDetailAPI($q, $customer_id, $ontime)
    {
        return $q
            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->where('bookings.ontime', $ontime)
            ->where('bookings.customer_id','=', $customer_id)
            ->select(['customers.firstname','customers.middlename','customers.lastname','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id', 'bookings.status','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes', 'customers.id as customer_id', 'services.id as service_id'])
            ->orderby('bookings.start', 'desc');
    }

    public function scopeNextAppointmentDate($q, $pet_id, $type_id)
    {
        return $q->where('pet_id', $pet_id)
                ->where('type_id', $type_id)
                ->orderBy('start', 'desc')
                ->select('start');
    }
    public function scopeNextAppointmentDateEnd($q, $pet_id, $type_id)
    {
        return $q->where('pet_id', $pet_id)
                ->where('type_id', $type_id)
                ->orderBy('end', 'desc')
                ->select('end');
    }

    public function scopeSingleBookingDetail($q, $book_id)
    {
        return $q->join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->where('bookings.id', $book_id)
                ->select(['customers.firstname', 'customers.firstname as title', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id','bookings.end', 'bookings.status','bookings.id as bookid','bookings.id as id', 'providers.name', 'bookings.technician_id','bookings.price', 'providers.name', 'providers.id as provider_id', 'customers.id as customer_id','services.id as service_id', 'bookings.notes', 'customers.email as customer_email', 'providers.email as providers_email', 'providers.name as provider_name','categories.image as category_image','customers.mobile as customers_mobile','bookings.transaction_id']);
    }

    public function scopeOnTimeBookingDetail($q, $ontime)
    {
        return $q->join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->where('bookings.ontime', $ontime)
                ->orderby('bookings.start', 'desc')
                ->select(['customers.firstname', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id','bookings.end', 'bookings.status','bookings.id as bookid','bookings.id as id', 'providers.name', 'bookings.technician_id', 'providers.name','bookings.technician_id','bookings.price', 'providers.id as provider_id', 'customers.id as customer_id','services.id as service_id', 'bookings.notes', 'bookings.confirmation','bookings.pet_id as petId' ]);
    }
    

    public function getResourcesAttribute()
    {
        return $this->attributes['technician_id'];
    }

    public function getEditableAttribute()
    {
       $carbon = Carbon::now()->timestamp;
       $start = Carbon::parse($this->attributes['start'])->timestamp;
       return ($carbon < $start);
    }

    public function scopeCalendarListings($q, $start, $end, $techID = null)
    {
         $q
                ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->where('bookings.start', '>=', $start)->where('bookings.end', '<=', $end)
                ->whereIn('bookings.status', [0,1])
//            ->whereIn('bookings.service_id', [48,44])
                ->where('bookings.provider_id', session('provider_id'))
                ->select(['customers.firstname','customers.firstname as title', 'customers.lastname', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.end', 'bookings.type_id', 'bookings.status','bookings.id as id', 'bookings.technician_id', 'providers.name', 'providers.id as provider_id', 'customers.id as customer_id','services.id as service_id', 'bookings.notes','bookings.ontime'])
                // ->selectRaw('CONCAT(customers.firstname," booked for ",pets.name) as title')
                ;
        if($techID != null)
        {
            $q->whereIn('bookings.technician_id', $techID);
        }
        return $q;
    }

    public function scopeIsCustomerofThisProvider($q, $customer_id)
    {
        return $q->where('provider_id', session('provider_id'))->where('customer_id', $customer_id);
    }

    public function scopeGet24HrsAdvanceBookings()
    {
        $get24HrsTime = Carbon::now()->addHour(4)->format('Y-m-d H:i:00');
        $allFutureBookings = DB::table('bookings')
            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->where('bookings.start', '==', $get24HrsTime)
            ->where('bookings.status', 1)
            ->where('bookings.provider_id', session('provider_id'))
            ->select(['customers.firstname', 'customers.lastname','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.end', 'bookings.type_id', 'bookings.status','bookings.id as id', 'bookings.technician_id', 'providers.name', 'providers.id as provider_id', 'customers.id as customer_id','services.id as service_id', 'bookings.notes'])
            ->groupBy('bookings.ontime')->orderBy('bookings.start', 'DESC');
    }
}
