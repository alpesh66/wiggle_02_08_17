<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'block', 'floor', 'judda', 'building', 'street', 'area', 'email', 'latitude', 'longitude', 'contact', 'status', 'type','name_ar','about','about_ar','starttime','endtime','website','facebook','twitter','instagram','image'];

    protected $hidden = ['created_at', 'updated_at'];

    public function scopeAllStore($q)
    {
        return $q->whereStatus(1);
    }
    
     public function getImageAttribute($image)
    {
        return ($image == '')? '' : url('uploads/store').'/'.$image;
    }
}
