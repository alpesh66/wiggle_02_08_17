<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TechnicianTiming extends Model
{
    //
    protected $fillable = ['provider_id','technician_id', 'weekday', 'starttime','endtime','status','created_at','updated_at'];

    public function technician()
    {
    	return $this->belongsTo('App\Technician');
    }

    public function setEndtimeAttribute($value)
    {
        $value = ($value == '00:00') ? '24:00' : $value;
        $this->attributes['endtime'] = $value;
    }
    
    public function getStarttimeAttribute($val)
    {
        return Carbon::parse($val)->format('H:i');
    }

    public function getEndtimeAttribute($val)
    {
        return Carbon::parse($val)->format('H:i');
    }
}
