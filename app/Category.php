<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parentid', 'name', 'name_er', 'image', 'image_ar', 'display', 'status'];

    protected $hidden = ['updated_at', 'created_at'];

    public function subcategory()
    {
       return  $this->hasMany('App\Category','parentid');
    }

    public function services()
    {
       return  $this->hasMany('App\Service');
    }

    public function scopeChildCat($q)
    {
        return $q->where('parentid', '<>', 0);
    }

    public function scopeActive($q)
    {
        return $q->where('status', 1);
    }
    
    public function scopeActiveParentListing($q)
    {
        return $q->Active()->where('parentid', 0);
    }

    public function scopeParentName($q, $id)
    {
        return $q->select('name')->where('id', $id)->first();
    }

}