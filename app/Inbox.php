<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $fillable = ['id','provider_id','customer_id', 'service_id', 'booking_id','bookdate','type', 'isnew', 'hadread','subject', 'message','photo'];

    
}
