<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'pivot'];
    
    public function provider()
    {
        return $this->belongsToMany('\App\Provider');
    }

    public function scopeOfType($q, $v)
    {
        return $q->where('ptype', $v)->pluck('dis_name','id');
    }
}
