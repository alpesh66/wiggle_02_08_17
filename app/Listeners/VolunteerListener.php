<?php

namespace App\Listeners;

use App\Events\VolunteerEvent;
use App\Volunteer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Mail;
use DB;


class VolunteerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SurrenderConfirmed  $event
     * @return void
     */
    public function handle(VolunteerEvent $event)
    {
        
        $volunteers= Volunteer::with('pet')->where('id',$event->volunteer)->first();

      //$volunteers=DB::table('volunteers')->where('id','=',$event->volunteer)->get();
        if($volunteers->status==1){
            $this->handleConfirmEvent($volunteers);
        }elseif($volunteers->status==2){
            $this->handleCancelEvent($volunteers);
        }
        
        $customer_email = false;
        $provider_email = false;
        $sikw_email = env('SIKW_BCC_COPY', false);
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
       // $sikw_email ="jatin@mxicoders.com";
        $emailHtml = array();
        
    }
    public function handleConfirmEvent($volunteers){
           
        $pushMessage_en=push_messages('approved_volunteer.en.message');
        $pushMessage_ar=push_messages('approved_volunteer.ar.message');
        $customer_id=$volunteers->customer_id;
        $request_id=$volunteers->id;
        $extraData=array('type'=>'VOLUNTEER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
        
        //email by jatin
            $fost= Volunteer::SingleBookingDetail($volunteers->id)->first(); 
      //  echo "<pre>"; print_r($fost); die();
        
                    $customer_email = $fost['customer_email'];
                    $provider_email = $fost['providers_email'];
                    $email_datacustomer_name = $fost['customer_name'];
                    $startdate=$fost['startdate'];
                    $enddate=$fost['enddate'];
                 //   $sikw_email="parmarjatin834@gmail.com";
                    $email_dataprovider_name = $fost['provider_name'];
                   // $fost['petid'];
                    
      //  $pet=\DB::table('pets')->where('id',$fost['petid'])->get();
        
        $message = "Dear {$email_datacustomer_name}, Your Foster with {$email_dataprovider_name} for ";
        $messagePro="Dear {$email_dataprovider_name}, New Foster Request from {$email_datacustomer_name} for ";
      
        $message .= "date between  {$startdate} and {$enddate} is approved.";
        $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
      
                    $customers_mobile = $fost['customers_mobile'];
                   // 
                    $data['customer_name'] = $email_datacustomer_name;
                    $data['startdate']=$startdate;
                     $sikw_email = env('SIKW_BCC_COPY', false);
                    $data['enddate']=$enddate;
                    $data['provider_name'] = $email_dataprovider_name;
                    $data['customers_mobile'] = $customers_mobile;
                    $data['msgCust']="Your volunteer Request is apporved";
                    $data['msgPro']="New volunteer Request from"." ".$email_datacustomer_name." is Approved";
                    
                    
                   
             if ($customer_email) {
            try {
               Mail::queue('emails.volunteerEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject("volunteer request is approved");
                    if ($sikw_email) {
                        $message->bcc('aditya.simplifiedinformatics@gmail.com');
                     }
                   });  
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
          }
          if ($provider_email) {
            try {
                Mail::queue('emails.volunteerEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                    $messagePro->to($provider_email);
                    $messagePro->subject("New Volunteer request is Approved");
                    if ($sikw_email) {
                        $messagePro->bcc('aditya.simplifiedinformatics@gmail.com');
                    }
                });
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
        }
                    
        
      //email end by jatin  
        
        
    }
    public function handleCancelEvent($volunteers){
        
        $pushMessage_en=push_messages('reject_volunteer.en.message');
        $pushMessage_ar=push_messages('reject_volunteer.ar.message');
        $customer_id=$volunteers->customer_id;
        $request_id=$volunteers->id;
        $extraData=array('type'=>'VOLUNTEER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
