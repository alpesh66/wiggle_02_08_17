<?php

namespace App\Listeners;

use App\Events\SurrenderEvent;
use App\Surrender;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Mail;


class SurrenderListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SurrenderConfirmed  $event
     * @return void
     */
    public function handle(SurrenderEvent $event)
    {
        
        $surrenders=Surrender::with('pet')->where('id',$event->surrender)->first();
        if($surrenders->status==1){
            $this->handleConfirmEvent($surrenders);
        }elseif($surrenders->status==2){
            $this->handleCancelEvent($surrenders);
        }
        
     //   $surrender=  Surrender::with('pet')->where('id',$event->surrender)->first();
        
    }
    public function handleConfirmEvent($surrenders){
        
        $pushMessage_en=push_messages('approved_surrender.en.message');
        $pushMessage_ar=push_messages('approved_surrender.ar.message');
        $customer_id=$surrenders->customer_id;
        $request_id=$surrenders->id;
        $extraData=array('type'=>'SURRENDER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
        
        
          //email by jatin
      $fost= Surrender::SingleBookingDetail($request_id)->first(); 
      //  echo "<pre>"; print_r($fost); die();
        
                    $customer_email = $fost['customer_email'];
                    $provider_email = $fost['providers_email'];
                    $email_datacustomer_name = $fost['customer_name'];
                    $startdate=$fost['startdate'];
                    $enddate=$fost['enddate'];
                    $email_dataprovider_name = $fost['provider_name'];
                    $fost['petid'];
                    
        $pet=\DB::table('pets')->where('id',$fost['petid'])->get();
        
        $message = "Dear {$email_datacustomer_name}, Your Surrender Request with {$email_dataprovider_name} for ";
        $messagePro="Dear {$email_dataprovider_name}, New Surrener Request from {$email_datacustomer_name} for ";
        foreach($pet as $val)
        {
            $message .= "{$val->name}";
            $messagePro .= "{$val->name}";
            
        }
        $message .= "date between  {$startdate} and {$enddate} is approved.";
        $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
        
       // echo "<pre>"; print_r($pet); 
        
                    $customers_mobile = $fost['customers_mobile'];
                    $sikw_email = env('SIKW_BCC_COPY', false);
                    $data['customer_name'] = $email_datacustomer_name;
                    $data['startdate']=$startdate;
                    $data['enddate']=$enddate;
                    $data['provider_name'] = $email_dataprovider_name;
                    $data['customers_mobile'] = $customers_mobile;
                    $data['petname']=$pet[0]->name;
                    $data['msgCust']="Your surrender Request is apporved for ".$pet[0]->name;
                    $data['msgPro']="New surrender Request from"." ".$email_datacustomer_name. " "."for"." ".$pet[0]->name." "."is Approved";
                    
          //  file_put_contents($this->logFile, "\n\n =========datafoster : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);
            
             if ($customer_email) {
            try {
               Mail::queue('emails.surrenderEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject("Surrender request is approved");
                    if ($sikw_email) {
                        $message->bcc('jatin@mxicoders.com');
                    }
                });  
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========SurrenderEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========surrenderEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
          }
          if ($provider_email) {
            try {
                Mail::queue('emails.surrenderEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                    $messagePro->to($provider_email);
                    $messagePro->subject("New Surrender is Approved");
                    if ($sikw_email) {
                        $messagePro->bcc('jatin@mxicoders.com');
                    }
                });
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========surrenderEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========surrenderEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
        }
                    
        
      //email end by jatin 
        
        
        
    }
    public function handleCancelEvent($surrenders){
        
        $pushMessage_en=push_messages('reject_surrender.en.message');
        $pushMessage_ar=push_messages('reject_surrender.ar.message');
        $customer_id=$surrenders->customer_id;
        $request_id=$surrenders->id;
        $extraData=array('type'=>'SURRENDER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
