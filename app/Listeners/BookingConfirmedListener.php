<?php

namespace App\Listeners;

use App\Events\BookingConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Booking;

class BookingConfirmedListener 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingConfirmed  $event
     * @return void
     */
    public function handle(BookingConfirmed $event)
    {
        $info_book = Booking::SingleBookingDetail($event->booking)->first();
        
        if($info_book->status=='1')
        {
            $pushMessage_en=push_messages('confirm_appointment.en.message');
            $pushMessage_ar=push_messages('confirm_appointment.ar.message');
            $customer_id=$info_book->customer_id;
            $request_id=$info_book->id;
          //  $message_id=$info_book->message_id;
            $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
        }
        else if($info_book->status=='2')
        {
            $pushMessage_en=push_messages('cancel_appointment.en.message');
            $pushMessage_ar=push_messages('cancel_appointment.ar.message');
            $customer_id=$info_book->customer_id;
            $request_id=$info_book->id;
          //  $message_id=$info_book->message_id;
            $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
        }
        
    }
}
