<?php

namespace App\Listeners;

use App\Events\KennelEvent;
use App\Kennel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class KennelListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FosterConfirmed  $event
     * @return void
     */
    public function handle(KennelEvent $event)
    {
        $kennel=Kennel::with('pet')->where('id',$event->kennel)->first();
        
        if($kennel->status==1){
            $this->handleConfirmEvent($kennel);
        }elseif($kennel->status==2){
            $this->handleCancelEvent($kennel);
        }
    }
    
    public function handleConfirmEvent($kennel){
        
        $pushMessage_en=push_messages('approved_kennel.en.message');
        $pushMessage_ar=push_messages('approved_kennel.ar.message');
        $customer_id=$kennel->customer_id;
        $request_id=$kennel->id;
        $extraData=array('type'=>'KENNEL','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
    public function handleCancelEvent($kennel){
        $pushMessage_en=push_messages('reject_kennel.en.message');
        $pushMessage_ar=push_messages('reject_kennel.ar.message');
        $customer_id=$kennel->customer_id;
        $request_id=$kennel->id;
        $extraData=array('type'=>'KENNEL','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
