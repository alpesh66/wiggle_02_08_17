<?php

namespace App\Listeners;

use App\Events\BookingWasMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBookingEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingWasMade  $event
     * @return void
     */
    public function handle(BookingWasMade $event)
    {
        //
    }
}
