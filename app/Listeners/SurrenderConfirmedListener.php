<?php

namespace App\Listeners;

use App\Events\SurrenderConfirmed;
use App\Surrender;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SurrenderConfirmedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SurrenderConfirmed  $event
     * @return void
     */
    public function handle(SurrenderConfirmed $event)
    {
        $surrender=  Surrender::with('pet')->where('id',$event->surrender)->first();
        $pushMessage_en=push_messages('approved_surrender.en.message');
        $pushMessage_ar=push_messages('approved_surrender.ar.message');
        $customer_id=$surrender->customer_id;
        $request_id=$surrender->id;
        $extraData=array('type'=>'SURRENDER','reminder'=>TRUE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
