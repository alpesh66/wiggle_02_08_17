<?php

namespace App\Listeners;

use App\Events\SurrenderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSurrenderEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingWasMade  $event
     * @return void
     */
    public function handle(SurrenderEvent $event)
    {
        //
    }
}
