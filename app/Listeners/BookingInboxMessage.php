<?php

namespace App\Listeners;

use App\Events\BookingWasMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Booking;
use Carbon\Carbon;
use Mail;

class BookingInboxMessage implements ShouldQueue  {
    
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        $this->logFile = '../storage/logs/BookingInboxMessage_' . date('d-m-Y') . '.txt';
        file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Handle the event.
     *
     * @param  BookingWasMade  $event
     * @return void
     */
    public function handle(BookingWasMade $bookings) {
        $customer_email = false;
        $provider_email = false;
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
        $sikw_email = env('SIKW_BCC_COPY', false);
        $emailHtml = array();
        $booking = [];
        $petdata = [];
        $servicesArray = array();
        file_put_contents($this->logFile, "\n\n =========booking : " . json_encode($bookings) . "\n\n", FILE_APPEND | LOCK_EX);
        foreach ($bookings as $value) {

            file_put_contents($this->logFile, "\n\n =========value : " . json_encode($value) . "\n\n", FILE_APPEND | LOCK_EX);
            foreach ($value as $key => $bookData) {
                $book = Booking::SingleBookingDetail($bookData)->first();
                file_put_contents($this->logFile, "\n\n =========book : " . json_encode($book) . "\n\n", FILE_APPEND | LOCK_EX);
                if ($book) {
                    $date = $book['start'];
                    $good_date = Carbon::parse($date)->format('l, M jS Y');
                    $email_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                    $booking[$key]= $book;
                    $petdata[$book['pet_id']]['petname'][] = $book['petname'];
                    $petdata[$book['pet_id']]['services'][] = $book['servicename'];
                    $servicesArray[$key] = $book['servicename'];
                    $image_url = 'images/category.png';
                    if (!empty($book['category_image'])) {
                        if (file_exists('uploads/categories/' . $book['category_image'])) {
                            $image_url = 'uploads/categories/' . $book['category_image'];
                        }
                    }

                    $emailHtml[$book['petname']][$key] = array(
                        'category_image' => $image_url,
                        'servicename' => $book['servicename']
                    );

//                   Send Email to Provider, Customer and Admin
                    $customer_email = $book['customer_email'];
                    $provider_email = $book['providers_email'];
                    $email_datacustomer_name = $book['firstname'];
                    $email_dataprovider_name = $book['provider_name'];
                    $customers_mobile = $book['customers_mobile'];
                    $booking_date = Carbon::parse($date)->format('F d Y');
                    $booking_time = Carbon::parse($date)->format('h:i a');
                    //\App\Inbox::create($booking);
                }
            }
        }
        $messageCommon = "";
        /*echo "<pre>";
        print_r($servicesArray);*/
        $uservice = array_unique($servicesArray);
        /*print_r($uservice);
        exit();
        
        $petarray = array();
        foreach ($petdata as $k => $pet) {
            
            $petarray[$k] = implode(",", $pet['petname']);
          echo $messageCommon .= implode(",", $pet['petname'])." " . implode(",", $pet['services']).', ';
         }

         echo "string";
         print_r($petarray);
         exit();*/
         file_put_contents($this->logFile, "\n\n =========Booking Detail before: " . json_encode($booking) . "\n\n", FILE_APPEND | LOCK_EX);
        $customer_id = "";
        if (!empty($booking)) {
            //$pushMessage_en= config('push_messages.booked_appointment.en.message');
            //$pushMessage_ar= config('push_messages.booked_appointment.ar.message');
            //changed by jatin 
               // $pushMessage_en= push_messages('booked_appointment.en.message');
            //    $pushMessage_ar= push_messages('booked_appointment.ar.message');
            //
            $method="knet";
            if(isset($booking[0]['transaction_id']) && $booking[0]['transaction_id']!=0){
                $transationData = \DB::table('transactions')->select('amount','PaymentID','TranID','result')->where('id',$booking[0]['transaction_id'])->first();
                if(!empty($transationData->result) && $transationData->result=="CAPTURED"){
                    $data['result'] = 'SUCCESS';
                    $data['amount'] = $transationData->amount;
                    $data['PaymentID'] = $transationData->PaymentID;
                    $data['TranID'] = $transationData->TranID;
                }else{
                    $method="cash";    
                }
            }else{
                $method="cash";
            }
            $data['method'] = $method;
            file_put_contents($this->logFile, "\n\n =========Payment Detail : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);
           $pushMessage_en="Your appointment booked successfully";
            $pushMessage_ar="تم حجز موعدك بنجاح";
           
            $message1 = "Dear {$booking[0]['firstname']},<br/>Your appointment with {$booking[0]['provider_name']} for ";
            $date = $booking[0]['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
            $inbox['provider_id'] = $booking[0]['provider_id'];
            $inbox['customer_id'] = $booking[0]['customer_id'];
            $inbox['service_id'] = $booking[0]['service_id'];
            $inbox['booking_id'] = $booking[0]['bookid'];
            $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
            $inbox['isnew'] = 1;
            $inbox['hadread'] = 1;
            $inbox['type'] = 0;  // 1 for reminder message , 0 for inbox massage 
            $inbox['subject'] = "Appointment Booking";
            foreach ($petdata as $pet) {
                $upet=array_unique($pet['petname']);
                //$uservice=array_unique($pet['services']);
                //$messageCommon .= " {$pet['petname']}: " . implode(",", $pet['services']);
                $messageCommon .= implode(",", $upet)." :" . implode(",", $uservice);
            }
            $message2 = " on {$good_date} is booked.";
            $customer_id = $booking[0]['customer_id'];
            
            //$message.= $book['firstname'] . ' Reminder for -' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;            
        }
        $message = $message1.' '.$messageCommon.' '.$message2;
        
        $inbox['message'] = $message;
        $message = \App\Inbox::create($inbox);
        $message_id = $message->id;
            $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
            $booking_time = Carbon::parse($date)->format('h:i a');

        $data['order_details'] = $emailHtml;
        $data['customer_name'] = $email_datacustomer_name;
        $data['provider_name'] = $email_dataprovider_name;
        $data['booking_date'] = Carbon::parse($booking[0]['start'])->format('F d Y');
        $data['booking_time'] = Carbon::parse($booking[0]['start'])->format('h:i a');
        $data['customers_mobile'] = $customers_mobile;

        file_put_contents($this->logFile, "\n\n =========data : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);

        //send email to customer
        if ($customer_email) {
            try {
                Mail::queue('emails.bookingEmailtoCustomer', $data, function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject(config('callme.EmailSubjecttoCustomer'));
                    if ($sikw_email) {
                        $message->bcc($sikw_email);
                    }
                });
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========bookingEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========bookingEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
        }

        //  send email to provider
        if ($provider_email) {
            try {
                Mail::queue('emails.bookingEmailtoProvider', $data, function ($message) use ($provider_email, $sikw_email) {
                    $message->to($provider_email);
                    $message->subject(config('callme.EmailSubjecttoProvider'));
                    if ($sikw_email) {
                        $message->bcc($sikw_email);
                    }
                });
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========bookingEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========bookingEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
        }
    }

}
