<?php

namespace App\Listeners;

use App\Events\FosterEvent;
use App\Foster;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Mail;


class FosterListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FosterConfirmed  $event
     * @return void
     */
    public function handle(FosterEvent $event)
    {
        $foster=Foster::with('pet')->where('id',$event->foster)->first();
        if($foster->status==1){
            $this->handleConfirmEvent($foster);
        }elseif($foster->status==2){
            $this->handleCancelEvent($foster);
        } 
        
        $customer_email = false;
        $provider_email = false;
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
        $sikw_email = env('SIKW_BCC_COPY', false);
        $emailHtml = array();
    }
    
    public function handleConfirmEvent($foster){
        
        $pushMessage_en=push_messages('approved_foster.en.message');
        $pushMessage_ar=push_messages('approved_foster.ar.message');
        $customer_id=$foster->customer_id;
        $request_id=$foster->id;
        $extraData=array('type'=>'FOSTER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
        
        
        //email by jatin
      $fost= Foster::SingleBookingDetail($request_id)->first(); 
      //  echo "<pre>"; print_r($fost); die();
        
                    $customer_email = $fost['customer_email'];
                    $provider_email = $fost['providers_email'];
                    $email_datacustomer_name = $fost['customer_name'];
                    $startdate=$fost['startdate'];
                    $enddate=$fost['enddate'];
                    $email_dataprovider_name = $fost['provider_name'];
                    $fost['petid'];
                    
        $pet=\DB::table('pets')->where('id',$fost['petid'])->get();
        
        $message = "Dear {$email_datacustomer_name}, Your Foster with {$email_dataprovider_name} for ";
        $messagePro="Dear {$email_dataprovider_name}, New Foster Request from {$email_datacustomer_name} for ";
        foreach($pet as $val)
        {
            $message .= "{$val->name}";
            $messagePro .= "{$val->name}";
            
        }
        $message .= "date between  {$startdate} and {$enddate} is approved.";
        $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
        
       // echo "<pre>"; print_r($pet); 
        
                    $customers_mobile = $fost['customers_mobile'];
                    $sikw_email = env('SIKW_BCC_COPY', false);
                    $data['customer_name'] = $email_datacustomer_name;
                    $data['startdate']=$startdate;
                    $data['enddate']=$enddate;
                    $data['provider_name'] = $email_dataprovider_name;
                    $data['customers_mobile'] = $customers_mobile;
                    $data['petname']=$pet[0]->name;
                    $data['msgCust']="Your foster Request is apporved for ".$pet[0]->name;
                    $data['msgPro']="New foster Request from"." ".$email_datacustomer_name. " "."for"." ".$pet[0]->name." "."is Approved";
                    
          //  file_put_contents($this->logFile, "\n\n =========datafoster : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);
            
             if ($customer_email) {
            try {
               Mail::queue('emails.fosterEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject("Foster request is approved");
                    if ($sikw_email) {
                        $message->bcc('aditya.simplifiedinformatics@gmail.com');
                    }
                });  
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========FosterEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========FosterEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
          }
          if ($provider_email) {
            try {
                Mail::queue('emails.fosterEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                    $messagePro->to($provider_email);
                    $messagePro->subject("New Foster is Approved");
                    if ($sikw_email) {
                        $messagePro->bcc('aditya.simplifiedinformatics@gmail.com');
                    }
                });
            } catch (\Swift_TransportException $e) {
                file_put_contents($this->logFile, "\n\n =========fosterEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
            } catch (Exception $e) {
                file_put_contents($this->logFile, "\n\n =========fosterEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                log_message("ERROR", $e->getMessage());
            }
        }
                    
        
      //email end by jatin 
        
    }
    public function handleCancelEvent($foster){
        $pushMessage_en=push_messages('reject_foster.en.message');
        $pushMessage_ar=push_messages('reject_foster.ar.message');
        $customer_id=$foster->customer_id;
        $request_id=$foster->id;
        $extraData=array('type'=>'FOSTER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
