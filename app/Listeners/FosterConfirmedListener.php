<?php

namespace App\Listeners;

use App\Events\FosterConfirmed;
use App\Foster;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FosterConfirmedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FosterConfirmed  $event
     * @return void
     */
    public function Handle(FosterConfirmed $event)
    {
      $foster = Foster::with('pet')->where('id',$event->foster)->first();
      echo $event->foster.'asd';
      echo "<pre>";
      print_r($event);
      exit();
     // $pushMessage_en=push_messages('approved_foster.en.message');
    //  $pushMessage_ar=push_messages('approved_foster.ar.message');
        $pushMessage_en="Your foster request approved successfully";
        $pushMessage_ar="طلب اعتمادك في انتظار الموافقة";
        $customer_id=$foster->customer_id;
        $request_id=$foster->id;
        $extraData=array('type'=>'FOSTER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
