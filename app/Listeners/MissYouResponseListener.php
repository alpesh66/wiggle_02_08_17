<?php

namespace App\Listeners;

use App\Events\MissYouResponse;
use App\MissYou;
use App\Inbox;
use Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MissYouResponseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MissYouResponse  $event
     * @return void
     */
    public function handle(MissYouResponse $event)
    {
        $kennelUpdate = MissYou::findOrFail($event->missyouId);
        $customerid = $kennelUpdate->customer_id;
        $provider_id = $kennelUpdate->provider_id;
        $customer = \App\Customer::findOrFail($kennelUpdate->customer_id);
        /*print_r($kennelUpdate);
        print_r($customer);*/
        $message = MissYou::select("message")->where('parent_id',$event->missyouId)->where('sender',2)->orderBy("id","DESC")->limit(1)->get()->toArray();
        /*print_r();*/
        //      send push notification to user         
                $reminder['message'] = $message[0]['message'];
                $reminder['provider_id'] = $provider_id;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 1;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customerid;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('missyou_response.en.title');
                $messageObj = Inbox::Create($reminder);         
                $message_id = $messageObj->id;

                sendPushNoti(push_messages('missyou_response.en.message'), push_messages('missyou_response.ar.message'),$customerid, $message_id);      
                try {
                    //$email = array($customer->email);
                    //$bcc_email = array('aditya.simplifiedinformatics@gmail.com');
                    $email = array("alpesh66@yopmail.com");
                    $bcc_email = array();

                    $data = array('customer_name' => ucfirst($customer->firstname . ' ' . $customer->lastname), 'reply_message' => $message[0]['message']);

                    Mail::send('emails.missyou', $data, function ($message) use($email, $bcc_email) {
                        $message->to($email);
                        $message->subject("Miss You request response mail");
                        if ($bcc_email) {
                            $message->bcc($bcc_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    return json_encode(array('status' => '0', 'message' => $e->getMessage()));
                } catch (Exception $e) {
                    return json_encode(array('status' => '0', 'message' => $e->getMessage()));
                    log_message("ERROR", $e->getMessage());
                }

    }
}
