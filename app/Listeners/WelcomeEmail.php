<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class WelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerCreated  $event
     * @return void
     */
    public function handle(CustomerCreated $event)
    {
//        dd($event->customer);
        return true;
        $cust = $event->customer;
        $event_cust = ($cust->toArray());
        Mail::queue('emails.welcome', $event_cust, function ($message) use ($event) {
            $message->to($event->customer->email);
            $message->bcc('aditya.simplifiedinformatics@gmail.com');
            $message->subject('Welcome to Wiggle');
        });
    }
}
