<?php

namespace App\Listeners;

use App\Events\AdoptEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Adopt;

class AdoptListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdoptConfirmed  $event
     * @return void
     */
    public function handle(AdoptEvent $event)
    {
        
        $Adopts=Adopt::with('pet')->where('id',$event->adopt)->first();
        if($Adopts->status==1){
            $this->handleConfirmEvent($Adopts);
        }elseif($Adopts->status==2){
            $this->handleCancelEvent($Adopts);
        }
        
        
    }
    public function handleConfirmEvent($Adopts){
        
     //   $adoption=Adopt::with('pet')->where('id',$event->adopt)->first();
        $pushMessage_en=push_messages('approved_adoption.en.message');
        $pushMessage_ar=push_messages('approved_adoption.ar.message');
        $customer_id=$Adopts->customer_id;
        $request_id=$Adopts->id;
        $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
    public function handleCancelEvent($Adopts){
       // $adoption=Adopt::with('pet')->where('id',$event->adopt)->first();
        $pushMessage_en=push_messages('reject_adoption.en.message');
        $pushMessage_ar=push_messages('reject_adoption.ar.message');
        $customer_id=$Adopts->customer_id;
        $request_id=$Adopts->id;
        $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$request_id,$extraData);
    }
}
