<?php

namespace App\Listeners;

use App\Events\SendReminderofBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\Booking;
use \Carbon\Carbon;

class BookingReminder implements ShouldQueue {

    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendReminderofBooking  $event
     * @return void
     */
    public function handle(SendReminderofBooking $event) {

        $booking = Booking::onTimeDetailAPI($event->customer_id, $event->ontime)->get();

        if ($booking) {
            $message = "Dear {$booking[0]['firstname']}, We would like to remind you of your appointment with {$booking[0]['provider_name']} for ";
            $date = $booking[0]['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
            $inbox['provider_id'] = $booking[0]['provider_id'];
            $inbox['customer_id'] = $booking[0]['customer_id'];
            $inbox['service_id'] = $booking[0]['service_id'];
            $inbox['booking_id'] = $booking[0]['booking_id'];
            $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
            $inbox['isnew'] = 1;
            $inbox['hadread'] = 1;
            $inbox['type'] = 1;  //Reminder Message
            $inbox['subject'] = push_messages('appointment_remider.en.title');
            $petdata=[];
            foreach ($booking as $key => $book) {
                $petdata[$book['pet_id']]['petname']=$book['petname'];
                $petdata[$book['pet_id']]['services'][]=$book['servicename'];
            }
            foreach($petdata as $pet){
                $message.=" {$pet['petname']}: ".implode(",", $pet['services']);
            }
            $message.=" at {$good_date}.";
            $inbox['message']=$message;
            //$message.= $book['firstname'] . ' Reminder for -' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;
            \App\Inbox::create($inbox);
            //$pushMessage_en=config('push_messages.appointment_remider.en.message');
            //$pushMessage_ar=config('push_messages.appointment_remider.ar.message');
            $pushMessage_en=push_messages('appointment_remider.en.message');
            $pushMessage_ar=push_messages('appointment_remider.ar.message');
            $extraData=array('type'=>'APPOINTMENT','reminder'=>TRUE);
            sendPushNoti($pushMessage_en,$pushMessage_ar,$booking[0]['customer_id'],$booking[0]['booking_id'],$extraData);
        }
    }

}
