<?php

namespace App\Listeners;

use App\Events\FosterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFosterEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingWasMade  $event
     * @return void
     */
    public function handle(FosterEvent $event)
    {
        //
    }
}
