<?php

namespace App\Listeners;

use App\Events\AdoptConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Adopt;
use App\Inbox;

class AdoptConfirmedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdoptConfirmed  $event
     * @return void
     */
    public function handle(AdoptConfirmed $event)
    {
        $Adopts=Adopt::with('pet')->where('id',$event->adopt)->first();

        if($Adopts->status==1){
            $this->handleConfirmEvent($Adopts);
        }elseif($Adopts->status==2){
            $this->handleCancelEvent($Adopts);
        }
    }
    public function handleConfirmEvent($Adopts){
        $pushMessage_en=push_messages('approved_adoption.en.message');
        $pushMessage_ar=push_messages('approved_adoption.ar.message');
        $customer_id=$Adopts->customer_id;
        $provider_id=$Adopts->provider_id;
        $request_id=$Adopts->id;

        $reminder['message'] = $pushMessage_en;
        $reminder['provider_id'] = $provider_id;
        $reminder['service_id'] = 0;
        $reminder['booking_id'] = 0;
        $reminder['isnew'] = 0;
        $reminder['hadread'] = 1;
        $reminder['type'] = 1;
        $reminder['customer_id'] = $customer_id;
        $reminder['sender'] = 1;
        $reminder['subject'] = push_messages('reject_adoption.en.title');
        $message = Inbox::Create($reminder);         
        $message_id = $message->id;

        $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
    }
    public function handleCancelEvent($Adopts){
       // $adoption=Adopt::with('pet')->where('id',$event->adopt)->first();
        $pushMessage_en=push_messages('reject_adoption.en.message');
        $pushMessage_ar=push_messages('reject_adoption.ar.message');
        $customer_id=$Adopts->customer_id;
        $provider_id=$Adopts->provider_id;
        $request_id=$Adopts->id;

        $reminder['message'] = $pushMessage_en;
        $reminder['provider_id'] = $provider_id;
        $reminder['service_id'] = 0;
        $reminder['booking_id'] = 0;
        $reminder['isnew'] = 0;
        $reminder['hadread'] = 1;
        $reminder['type'] = 1;
        $reminder['customer_id'] = $customer_id;
        $reminder['sender'] = 1;
        $reminder['subject'] = push_messages('reject_adoption.en.title');
        $message = Inbox::Create($reminder);         
        $message_id = $message->id;

        $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);

    }
}
