<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kennel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kennels';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','description','title','sender','provider_id','startdate','enddate','status','parent_id','is_read'];

    public function customer()
    {
       return  $this->belongsTo('App\Customer');
    }

    public function pet()
    {
       return  $this->belongsToMany('App\Pet');
    }

    public function scopeallMessage($q)
    {
        
        
        $where = ['provider_id' => session('provider_id'), 'sender' => 1 ];
        return $q->where($where);
//         $q = \DB::table('kennels')
//            ->join('kennel_pet', 'kennel_pet.pet_id', '=', 'kennels.id')
//            ->join('pets', 'kennel_pet.pet_id', '=', 'pets.id')
//            ->join('customers', 'kennels.customer_id', '=', 'customers.id')
//            ->where('kennels.provider_id', session('provider_id'))
//            ->where('kennels.sender', 1)
//            ->groupBy('kennels.id');
    }

}
