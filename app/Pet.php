<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Pet extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'dob', 'breed', 'gender', 'height','provider_id', 'photo','photo[]', 'temperament', 
        'energy_level','maintenance','good_with_children','daily_exercises','notes','pet_status',
        'origin', 'size', 'chipno', 'weight', 'grooming', 'adopt_comments' , 'disabled_comments','disabled_question','status', 'isadopted', 'likes', 'cough', 'missyou','neutered','spayed','last_groom_appointment','next_groom_appointment','vaccination_type_id','next_vaccination_date'];

    protected $hidden = ['updated_at', 'pivot'];
    
    protected $appends = ['veternary','veternaryend', 'groom', 'breedname'];

    /**
     * get the pet
     */
    public function customer()
    {
        return $this->belongsToMany('App\Customer');
    }
    
    public function petlog(){
        return $this->hasMany('App\Petlog');
    }

    public function adopt()
    {
        return $this->belongsToMany('App\Adopt');
    }
    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = Carbon::parse($value);
    }

    public function getDobAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

    public function breeds()
    {
        return $this->belongsTo('App\Breed','breed');
    }

    public function sizes()
    {
        return $this->belongsTo('App\Size','size');
    }

    public function medicalcard()
    {
        return $this->hasMany('App\Medicalcard');
    }

    public function booking()
    {
        return $this->hasMany('\App\Booking');
    }

    public function getPhotoAttribute($photo)
    {
        return ($photo == '')? '' : url('uploads/pets').'/'.$photo;
    }
    
    public function getPhotoThumbnailAttribute()
    {   
        $photo = $this->attributes['photo'];
        return ($photo == '')? '' : url('uploads/pets/thumbnail').'/'.$photo;
    }

    public function getVeternaryAttribute()
    {
        if(isset($this->attributes['id'])){
            $pet_id = $this->attributes['id'];
            $vet =  \App\Booking::NextAppointmentDate($pet_id, 1)->first();
            if(count($vet) > 0){
                return Carbon::parse($vet['start'])->format('Y-m-d H:i:s');
            }
            return '';
        }
        return '';
    }
    public function getVeternaryendAttribute()
    {
        if(isset($this->attributes['id'])){
            $pet_id = $this->attributes['id'];
            $vet =  \App\Booking::NextAppointmentDateEnd($pet_id, 1)->first();
            if(count($vet) > 0){
                return Carbon::parse($vet['end'])->format('Y-m-d H:i:s');
            }
            return '';
        }
        return '';
    }

    public function getGroomAttribute()
    {
        if(isset($this->attributes['id'])){
            $pet_id = $this->attributes['id'];
            $groom =  \App\Booking::NextAppointmentDate($pet_id, 2)->first();
            if(count($groom) > 0){
                return Carbon::parse($groom['start'])->format('Y-m-d H:i:s');
            }
            return '';
        }
        return '';
    }

    public function getBreedNameAttribute()
    {
        return ($this->attributes['breed'] == 1) ? 'Cat' : 'Dog';
    }
    /**
     * API for the list of Adoptions
     * @param  [type] $q [description]
     * @return [type]    [description]
     */
    public function scopeAdoptions($q)
    {
        //dd($q);
        return $q->join('providers', 'pets.provider_id', '=','providers.id')
                ->where('provider_id', '!=', '0')
                ->where('pets.status', 1)
                ->where('providers.status', 1)
                ->where('isadopted', 0)
                ->where('pets.pet_status','Available')
                ->select(['pets.*', 'providers.name as providers_name', 'providers.photo as providers_photo', 'providers.icon as providers_icon'])
                ->orderBy('pets.created_at', 'desc');
    }
    public function getTemparaments(){
        return \DB::table('temperaments')
                ->join('pet_temperament','temperaments.id','=','pet_temperament.temperament_id')
                ->where('pet_temperament.pet_id',$this->attributes['id'])
                ->select('temperaments.name','temperaments.id')->get();
    }
    
    public function log(){
        return $this->hasMany('\App\Petlog');
    }

    
}
