<?php

namespace App\Http\Middleware;

use Closure;

class VerifyUserhasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        if ($request->user()->hasRole($role)) {
            return $next($request);
        }

        // Throw “Forbidden” exception. User does not have required role.
        abort(403);
    }
}
