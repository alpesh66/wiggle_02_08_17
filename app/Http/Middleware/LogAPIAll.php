<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use DB;
use Carbon\Carbon;

class LogAPIAll
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
    
        $keep_on = env('LOGtoDB', false);
        if($keep_on)
        {   
            $inn['id'] = str_random(30);
            $inn['userid'] = $request->header('authorization');
            $inn['ipaddress'] = $request->ip().'-'.$request->path();
            $inn['request'] = json_encode($request->all());
            $inn['response'] = $response->getContent();
            $inn['agent'] = $request->header('user-agent');
            $inn['created_at'] = Carbon::now();
            $inn['timestamp'] = Carbon::now()->timestamp;
            $db = DB::connection('sqlite')->table('logs')->insert($inn);
        } else {
            $logFile = 'API.txt';
            Log::useDailyFiles(storage_path().'/logs/'.$logFile);
            Log::info($request->ip().'-'.$request->path().'-'.$request->header('authorization'), ['request ' => $request->all(), 'response' => json_decode($response->getContent()) ] );
        }
    }
}
