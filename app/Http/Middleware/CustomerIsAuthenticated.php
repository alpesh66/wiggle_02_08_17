<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CustomerIsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(['status' => 1, 'message' => 'Not Authorized or Token not included', 'message_ar' => 'Not Authorized or Token not included'], 401);
            } 
        }

        return $next($request);
    }
}
