<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class TechRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'type_id'   => 'required',
            'name'      => 'required',                        
            'name_ar'   => 'required',                        
            'number'    => 'required|integer',                        
            'address'   => 'required',
            'image'     => 'mimes:jpeg,jpg,png',
        ];
    }
}
