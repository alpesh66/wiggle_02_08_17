<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MedicalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'givenon'  => 'required|date',
            'vet'      => 'required|string|max:255',
            'institution'  => 'required|string|max:255',
            'photo'    => 'mimes:jpeg,jpg,png|required'
      
        ];
    }
}
