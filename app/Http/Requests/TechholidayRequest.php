<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TechholidayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'technician_id'     => 'required',
            'holidaytype'       => 'required',
            'start'             => 'required_if:holidaytype,0',
            'end'               => 'required_if:holidaytype,0',
            'weekday'           => 'required_if:holidaytype,1', 
            'type'              => 'required',
            'breakstart'        => 'required_if:type,1',
            'breakend'          => 'required_if:type,1'            
        ];
    }

    public function messages()
    {
        return [
            'start.required_if' => 'Please select start date',
            'end.required_if' => 'Please select end date',
            'weekday.required_if' => 'Please select weekday',
            'breakstart.required_if' => 'Please select Start Time',
            'breakend.required_if' => 'Please select End Time',
        ];
    }
    
}
