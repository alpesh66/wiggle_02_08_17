<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TechleaveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'technician_id'     => 'required',
            'start'             => 'required',
            'toend'               => 'required',
            'type'              => 'required',
            'breakstart'        => 'required_if:type,1',
            'breakend'          => 'required_if:type,1'            
        ];
    }

    public function messages()
    {
        return [
            'start' => 'Please select start date',
            'end' => 'Please select end date',
            'breakstart.required_if' => 'Please select Start Time',
            'breakend.required_if' => 'Please select End Time',
        ];
    }
    
}
