<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::isMethod('post'))
        {
            return [
                'name' => 'required|max:255',
                'name_er' => 'required|max:255',
                'display' => 'integer'
            ];
        } else if (Request::isMethod('patch'))
        {
            $id = Request::segment(2);

            return [
                'name' => 'required|max:255',
                'name_er' => 'required|max:255',
                'display' => 'integer'
            ];
        }
    }
}
