<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::isMethod('post'))
        {
            return [
                'provider_id'       => 'unique_with:services,category_id',
                'allSelected'       => 'string',
                'price'             => 'required_if:allSelected,on',
                'servicetime'       => 'required_if:allSelected,on',
                'breed.*.breed_id'      => 'required_without:allSelected',
                'breed.*.size_id'       => 'required_without:allSelected',
                'breed.*.servicetime'   => 'required_without:allSelected|integer',
                'breed.*.price'         => 'required_without:allSelected|numeric',
                'technicians'   => 'required',
            ];
        }
        elseif(Request::isMethod('patch'))
        {
            $id = Request::segment(3);
            // if(!is_numeric($id)){
            //     $id = Request::segment(3);
            // }
            return[
                'provider_id' => 'unique_with:services,category_id,id='.$id,
                // 'allSelected'       => 'string',
                // 'price'             => 'required_if:allSelected,on',
                // 'servicetime'       => 'required_if:allSelected,on',
                // 'breed.*.breed_id'      => 'required_without:allSelected',
                // 'breed.*.size_id'       => 'required_without:allSelected',
                // 'breed.*.servicetime'   => 'required_without:allSelected|integer',
                // 'breed.*.price'         => 'required_without:allSelected|numeric',
                'technicians'   => 'required',
            ];
        }

    }
}
