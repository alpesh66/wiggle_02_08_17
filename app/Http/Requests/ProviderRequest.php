<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProviderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:providers',
            'email'         => 'required|unique:providers',
            'address'       => 'required',
            'area'          => 'required',
            'ptype'         => 'required',
            'type_1'        => 'required_if:ptype,1',
            'type_2'        => 'required_if:ptype,2',
            'start'         => 'required|date',
            'end'           => 'required|date',
            'latituide'     => 'numeric',
            'longitude'     => 'numeric',
            'contact'       => 'numeric',
            'users.name'     => 'required|unique:users',
            'users.email'    => 'required|email|unique:users',
            'users.password' => 'required|min:4'
        ];
    }

    public function messages()
    {
        return [
            'type_1.required_if' => 'Select services for Providers!',
            'type_2.required_if' => 'Select services for Societies!',
        ];
    }
}
