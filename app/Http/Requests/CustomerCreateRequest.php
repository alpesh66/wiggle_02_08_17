<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:100',
            'middlename' => 'sometimes|max:100',
            'lastname'  => 'required|max:100',
            'gender'    => 'sometimes|in:0,1',
            'email'     => 'required|email|max:255|unique:customers',
            'mobile'    => 'required|unique:customers',
            'dob'       => 'sometimes|date',
            'address'   => 'sometimes|max:255',
            'latitude' =>'sometimes|required',
            'longitude' =>'sometimes|required',
            'block'=>'required',
            'street'=>'required',
            'judda'=>'string',
            'house'=>'required',
            'apartment'=>'required',
            'pets.name'      => 'required|max:255',
            'pets.breed'     => 'required|in:1,2',
            'pets.dob'       => 'sometimes|date_format:Y-m-d',
            'pets.size'      => 'required|integer',
            'pets.gender'    => 'sometimes|in:0,1',
            'pets.height'    => 'sometimes|string',
       
        ];
    }
}
