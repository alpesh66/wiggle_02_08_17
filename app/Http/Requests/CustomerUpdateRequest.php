<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(2);
        return [
            'firstname' => 'required|max:100',
            'lastname'  => 'required|max:100',
            'email'     => 'required|email|unique:customers,email,'.$id,
            'mobile'    => 'required|numeric|digits_between:7,10|mobile_number:customers,country,mobile,'.$id,
            'country'    => 'required|numeric',
            'dob' => 'date',
            'latitude' =>'required',
            'longitude' =>'required',
            'block'=>'required',
            'street'=>'required',
            'judda'=>'required',
            'house'=>'required',
            'apartment'=>'required'
        ];
    }
}
