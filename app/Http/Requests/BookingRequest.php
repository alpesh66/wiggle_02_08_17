<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required',
            'pet_id' => 'required',
            'technician_id' => 'required',
            'service_id' => 'required',
            'type_id' => 'required',
            'start' => 'required|date',
            'notes' => 'max:255'
        ];
    }
}
