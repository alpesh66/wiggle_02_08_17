<?php
/*Route::get('vendor/templateEditor/kcfinder/upload.php', function()
{
    require base_path().'vendor/templateEditor/kcfinder/upload.php';
});
Route::get('vaccination_reminder', function () {
        return view('emails.vaccination_reminder');
    });*/
Route::group(['middleware' => ['web']], function() {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('map', function(Illuminate\Http\Request $request) {

        return view('map', compact('request'));
    });
    // Customer Reset Link
    Route::get('customers/password/reset/{token?}', 'Customerauth\PasswordController@showResetForm');
    Route::post('customers/password/email', 'Customerauth\PasswordController@sendResetLinkEmail');
    Route::post('customers/password/reset', 'Customerauth\PasswordController@reset');

    Route::auth();

    Route::post('subadminDashboard', 'HomeController@moveToDashboard');
    Route::post('get_unread_request_count', 'HomeController@getUnreadRequestCount');
    Route::post('superAdmin', 'HomeController@moveBacktoAdmin');

    Route::post('user/changepassword', 'UserController@changepassword');

    /**
     * Route for the Super Admin
     */
    Route::group(['middleware' => ['auth', 'permission:super-*']], function () {


        Route::get('/dashboard', 'HomeController@index');

        Route::get('provider/typeof/{id}', ['as' => 'provider.typeoftype', 'uses' => 'ProviderController@typeoftype']);

        Route::get('store/getstr', 'StoreController@providerDashboard');
        Route::get('getmedicalCard/{id}', 'PetController@datatablesmedicalCard');

        Route::get('medicalcd/{pet}', 'PetController@showPromedicalcard');
        Route::post('medicalcd/{pet}', 'PetController@storePromedicalcard');
        Route::delete('medicalcard/{id}', 'PetController@destroyMedicalcard');
        route::resource('temperament', 'TemperamentController');
        Route::resource('transaction', 'TransactionController');
        Route::post('transactionbookings', 'TransactionController@transactionBookingsDetail');
        Route::resource('species', 'SpeciesController');

        Route::group(['middleware' => ['permission:super-admin']], function() {
            Route::resource('user', 'UserController');
            Route::resource('role', 'RoleController');
        });

        Route::resource('pet', 'PetController');
        Route::get('pet-deactive', 'PetController@indexDeactive');

        Route::post('customers/customerPetDeactivation', 'CustomersController@customerPetDeactivation');
        Route::group(['middleware' => ['permission:super-customer']], function() {
            Route::resource('customers', 'CustomersController');
        });

        Route::get('customers/{customers}/customerpets', 'CustomersController@customerpets');
        Route::get('customers/{customers}/customerbookings', 'CustomersController@customerbookings');


        Route::get('booking/{id}/delbooking', 'BookingController@delbooking');


        Route::group(['middleware' => ['permission:super-provider']], function() {
            Route::resource('provider', 'ProviderController');
        });

        Route::group(['middleware' => ['permission:super-store']], function() {
            Route::resource('store', 'StoreController');
        });
        
        Route::group(['middleware' => ['permission:super-store']], function() {
            Route::post('/ratings/changestatus', 'RatingController@changestatus');
            Route::resource('ratings', 'RatingController');
        });

        Route::group(['middleware' => ['permission:super-cms']], function() {
            Route::resource('content', 'ContentController');
        });

        Route::group(['middleware' => ['permission:super-category']], function() {
            Route::resource('categories', 'CategoriesController');
        });

        Route::resource('events', 'EventsController');
        Route::resource('sales', 'SalesController');
        Route::get('Society-Report', 'SalesController@getSocietyReport');        
        Route::post('societyReport', 'SalesController@societyReport');
        Route::post('getCategoryList', 'SalesController@getCategoryList');        
        Route::post('generateSalesReport', 'SalesController@generateSalesReport');                
        Route::resource('size', 'SizeController');

        Route::get('push-all', 'PushNotificationController@forAll');
        Route::get('push-appointment', 'PushNotificationController@forAppointment');
        Route::post('sendpushForAll', 'PushNotificationController@sendpushForAll');
        Route::post('sendpushForAppointment', 'PushNotificationController@sendpushForAppointment');        
    });


    /**
     * Route for the Providers
     */
    /*Route::resource('providers/ratings', 'RatingController');*/
    Route::group(['middleware' => ['auth', 'permission:provider-*'], 'prefix' => 'providers'], function () {
        
        Route::resource('provider_sales', 'ProviderSalesController');
        Route::post('getCategoryList', 'ProviderSalesController@getCategoryList');        
        Route::post('generateSalesReport', 'ProviderSalesController@generateSalesReport');
        Route::get('/', 'ProviderHomeController@dashboard');
        Route::get('dashboard', 'ProviderHomeController@dashboard');
        Route::get('profile', 'ProviderController@profileproviders');
        Route::patch('profile_update', 'ProviderController@updateproviders');
        Route::post('setServiceType', 'ProviderController@setServiceType');
        Route::post('getServiceType', 'ProviderController@getServiceType');
        Route::get('calendar', 'ProviderHomeController@index');
        Route::get('booking', 'BookingController@index');

        Route::get('kennelservice', 'ServicesController@kennelservices');
        Route::patch('kennelservice/{id}', 'ServicesController@markkennel');
        Route::post('/kennels/reply', 'KennelController@reply');
        Route::resource('kennels', 'KennelController');
        Route::get('VaccinationSchedule', 'KennelController@VaccinationSchedule');

        Route::post('/missyou/reply', 'MissYouController@reply');
        Route::resource('missyou', 'MissYouController');

        Route::post('services/checkService', 'ServicesController@checkService');
        Route::post('/ratings/changestatus', 'RatingController@changestatus');
        Route::resource('ratings', 'RatingController');

        Route::get('/pet/{pet}', 'PetController@showProvider');
        Route::post('/pet', 'PetController@storeproviders');

        Route::get('getmedicalCard/{id}', 'PetController@datatablesmedicalCard');
        Route::get('medicalcard/{pet}', 'PetController@showmedicalcard');
        Route::post('medicalcard/', 'PetController@storemedicalcard');

        Route::get('medicalcad/{pet}', 'PetController@showProvidermedicalcard');
        Route::post('medicalcad/{pet}', 'PetController@storeProvidermedicalcard');
        Route::delete('medicalcard/{id}', 'PetController@destroyMedicalcard');

        // send inbox message and 
        Route::post('storeinboxmessage', 'BookingController@inboxmessage');
        Route::post('detailedbookings', 'BookingController@detailedonTimeBookings');
        Route::post('viewBookingPopupFromCalender', 'BookingController@viewBookingPopupFromCalender');
        Route::post('sendreminder', 'BookingController@sendReminderMessage');
        Route::post('setnoshow', 'BookingController@setNoShow');
        Route::post('updateBookStatus', 'BookingController@updateBookingStatus');
        // test Only for VUEjs
        Route::get('vue', 'ServicesController@vue');


        // Transaction UPDATE


        Route::match(['put', 'patch'], '/transaction/{transaction}', 'TransactionController@update');

        Route::get('activetechnicians', 'TechniciansController@getActiveTechnician');
        Route::post('technicianwithservice', 'TechniciansController@technicianWithService');
        Route::post('getbreed', 'ProviderHomeController@getbreed');
        Route::post('getsize', 'ProviderHomeController@getsize');
        Route::post('customers/search', 'CustomersController@searchCustomerData');

        Route::post('services/getforthispet', 'ServicesController@allServicesOfferedforSize');
        Route::post('customers/customersProviderPetDeactivation', 'CustomersController@customerProviderPetDeactivation');
        Route::post('/customers', 'CustomersController@store');
        Route::get('customers/{id}', 'CustomersController@showsociety');
        Route::get('customers/{customers}', 'CustomersController@showproviders');
        Route::get('customers/{customers}/edit', 'CustomersController@editproviders');
        Route::match(['put', 'patch'], '/customers/{customers}', 'CustomersController@providercustomerupdate');
        Route::get('customers/{customers}/customerpetspro', 'CustomersController@customerpetspro');
        Route::get('customers/{customers}/customerbookingspro', 'CustomersController@customerbookingspro');
        Route::get('showpet/{customers}', 'CustomersController@showpet');
        Route::match(['put', 'patch'], '/showpet/{pet}', 'PetController@updateproviders');
        Route::get('showpet/{pet}/edit', 'PetController@editproviders');
        Route::get('customers', 'CustomersController@indexCustomer');

        // Customers
        Route::post('getinfo', 'ProviderHomeController@CustomerData');

        Route::get('check_availability', 'BookingController@check_availability');
        Route::post('calendarevents', 'BookingController@postCalendarevents');
        Route::resource('booking', 'BookingController');


        Route::group(['middleware' => ['permission:provider-admin']], function() {


            Route::get('/user', 'UserController@indexproviders');
            Route::get('/user/create', 'UserController@createproviders');
            Route::get('/user/{user}', 'UserController@showproviders');
            Route::post('/user', 'UserController@storeproviders');
            Route::match(['put', 'patch'], '/user/{user}', 'UserController@updateproviders');
            Route::delete('/user/{user}', 'UserController@destroyproviders');
            Route::get('/user/{user}/edit', 'UserController@editproviders');

            Route::get('/role', 'RoleController@indexroles');
            Route::get('/role/create', 'RoleController@createroles');
            Route::get('/role/{role}', 'RoleController@showroles');
            Route::post('/role', 'RoleController@storeroles');
            Route::match(['put', 'patch'], '/role/{role}', 'RoleController@updateroles');
            Route::delete('/role/{role}', 'RoleController@destroyroles');
            Route::get('/role/{role}/edit', 'RoleController@editroles');

//            Technician Login Create
            Route::get('/logintech', 'UserController@indexOnlyTechnicians');
            Route::get('/logintech/create', 'UserController@createtechLoginProviders');
            Route::post('techuser', 'UserController@storeTechLoginproviders');
            Route::get('/techuser/{user}/edit', 'UserController@editTechLoginProviders');
            Route::match(['put', 'patch'], '/techuser/{user}', 'UserController@updateTechLoginProviders');
        });

        Route::group(['middleware' => ['permission:provider-service']], function() {
            Route::resource('gallery', 'GalleryController');
            Route::resource('services', 'ServicesController');
        });

        Route::group(['middleware' => ['permission:provider-service']], function() {
            Route::resource('technicians', 'TechniciansController');
            // Route::resource('technicianbreak', 'TechnicianBreakController');
            Route::resource('technicianholiday', 'TechnicianHolidayController');
            Route::resource('technicianleave', 'TechnicianLeaveController');
            Route::resource('providertimings', 'ProviderTimingController');
            Route::resource('providerholidays', 'ProviderHolidayController');
            Route::post('providertimings/updatetime', 'ProviderTimingController@updatetime');
        });



        // Route::group(['middleware' => ['permission:society-foster']], function() {
        //     Route::post('/fosters/reply', 'FosterController@reply');  
        //     Route::resource('fosters', 'FosterController');
        // });    
    });

    /**
     * ROute for the Calendar only access
     */
    Route::group(['middleware' => ['auth', 'permission:calendar-only'], 'prefix' => 'tech'], function () {
        Route::get('calendar', 'ProviderHomeController@onlyCalendarView');
        Route::post('calendarevents', 'BookingController@onlyCalendarforTechnician');
        Route::get('booking', 'BookingController@indexOnlyCalendar');
        Route::post('detailedbookings', 'BookingController@detailedonTimeBookings');        
        

        Route::post('customers/search', 'CustomersController@searchCustomerData');
        Route::post('services/getforthispet', 'ServicesController@allServicesOfferedforSize');
        Route::get('customers/{customers}', 'CustomersController@showproviders');
        Route::get('customers/{customers}/customerpetspro', 'CustomersController@customerpetspro');
        Route::get('customers/{customers}/customerbookingspro', 'CustomersController@customerbookingspro');
        Route::get('showpet/{customers}', 'CustomersController@showpet');
        Route::get('customers', 'CustomersController@indexCustomer');

        Route::get('/pet', 'PetController@indexsocieties');
        Route::get('/pet/{pet}', 'PetController@showsocieties');
        Route::get('getmedicalCard/{id}', 'PetController@datatablesmedicalCard');
        Route::get('medicalcad/{pet}', 'PetController@showProvidermedicalcard');

        // Customers
        Route::post('getinfo', 'ProviderHomeController@CustomerData');
    });
    /**
     * Route for the Societies
     */
    Route::group(['middleware' => ['auth', 'permission:society-*'], 'prefix' => 'societies'], function () {
        /*--
        Created By: alpesh patel
            Date:19-07-17
        */
        Route::resource('report', 'SocietyReportController');
        Route::post('generateReport', 'SocietyReportController@generateReport');
         Route::get('VaccinationSchedule', 'KennelController@VaccinationSchedule');
        Route::get('profile', 'SocietyHomeController@profilesociety');
        Route::patch('profile_update', 'SocietyHomeController@updatesociety');
        /*--*/
        Route::get('/', 'SocietyHomeController@index');
        Route::get('/dashboard', 'SocietyHomeController@index');
        Route::get('/surrender', 'SocietyHomeController@index');
        Route::get('/initiatives', 'SocietyHomeController@index');

        Route::get('/pet', 'PetController@indexsocieties');
        Route::get('/petlog', 'PetController@logsociety');
        Route::get('/pet/create', 'PetController@createsocieties');
        Route::get('/pet/{pet}', 'PetController@showsocieties');
        Route::post('/pet', 'PetController@storesocieties');
        Route::match(['put', 'patch'], '/pet/{pet}', 'PetController@updatesocieties');
        Route::delete('/pet/{pet}', 'PetController@destroysocieties');
        Route::get('/pet/{pet}/edit', 'PetController@editsocieties');

        Route::get('getmedicalCard/{id}', 'PetController@datatablesmedicalCard');

        Route::get('medicalcrdd/{pet}', 'PetController@showSocietymedicalcard');
        Route::post('medicalcrdd/{pet}', 'PetController@storeSocietymedicalcard');
        Route::delete('medicalcard/{id}', 'PetController@destroyMedicalcard');

        Route::get('customers', 'CustomersController@indexSocietyCustomer');
        Route::get('customers/{id}', 'CustomersController@showsociety');
        Route::get('customers/{customers}', 'CustomersController@showproviders');

        Route::get('customers/{customers}', 'CustomersController@showproviders');

        Route::post('/ratings/changestatus', 'RatingController@changestatus');
        Route::resource('ratings', 'RatingController');
        Route::resource('approve', 'ApproveController');
        
        Route::group(['middleware' => ['permission:society-admin']], function() {

            Route::get('/user', 'UserController@indexsocieties');
            Route::get('/user/create', 'UserController@createsocieties');
            Route::get('/user/{user}', 'UserController@showsocieties');
            Route::post('/user', 'UserController@storesocieties');
            Route::match(['put', 'patch'], '/user/{user}', 'UserController@updatesocieties');
            Route::delete('/user/{user}', 'UserController@destroysocieties');
            Route::get('/user/{user}/edit', 'UserController@editsocieties');

            Route::get('/role', 'RoleController@indexsocieties');
            Route::get('/role/create', 'RoleController@createsocieties');
            Route::get('/role/{role}', 'RoleController@showsocieties');
            Route::post('/role', 'RoleController@storesocieties');
            Route::match(['put', 'patch'], '/role/{role}', 'RoleController@updatesocieties');
            Route::delete('/role/{role}', 'RoleController@destroysocieties');
            Route::get('/role/{role}/edit', 'RoleController@editsocieties');
        });
        Route::group(['middleware' => ['permission:society-adopt']], function() {
            Route::post('/adopts/reply', 'AdoptsController@reply');
            Route::resource('adopts', 'AdoptsController');
        });
        Route::resource('kennels', 'KennelController');
        Route::group(['middleware' => ['permission:society-kennel']], function() {
            Route::post('/kennels/reply', 'KennelController@reply');
            Route::resource('kennels', 'KennelController');
            Route::post('/missyou/reply', 'MissYouController@reply');
            Route::resource('missyou', 'MissYouController');
        });

        Route::group(['middleware' => ['permission:society-foster']], function() {
            Route::post('/fosters/reply', 'FosterController@reply');
            Route::resource('fosters', 'FosterController');
        });
        Route::group(['middleware' => ['permission:society-vol']], function() {
            Route::post('/volunteers/reply', 'VolunteerController@reply');
            Route::resource('volunteers', 'VolunteerController');
        });
        Route::group(['middleware' => ['permission:society-surr']], function() {
            Route::post('/surrenders/reply', 'SurrenderController@reply');
            Route::resource('surrenders', 'SurrenderController');
        });
    });
});

/**
 * All ADMIN - API
 */
Route::group(['middleware' => 'api', 'prefix' => 'adminapi/v1'], function() {

    Route::get('view', 'AdminApiController@clientview');
    Route::post('postVersion', 'AdminApiController@checkversion');
    Route::post('postLogin', 'AdminApiController@getLogin');
    Route::post('postForgot', 'Auth\PasswordController@sendResetLinkEmail');
});

// ADMIN AUTHENTICATED APIS
Route::group(['middleware' => 'auth:api', 'prefix' => 'adminapi/v1'], function() {

    Route::get('getBreeds', 'AdminApiController@allBreedsData');
    Route::get('getCountries', 'AdminApiController@allCountryData');
    Route::post('postTechnicians', 'AdminApiController@allTechniciansList');
    Route::post('postTechServices', 'AdminApiController@allTechniServices');
    Route::post('postServicesforTech', 'AdminApiController@servicesForTechnicians');
    Route::get('getCategories', 'AdminApiController@allCategories');
    Route::post('postSearchCustomer', 'AdminApiController@searchCustomersData');
    Route::post('postUpdateCustomer', 'AdminApiController@updateCustomerData');
    Route::post('postAddCustomer', 'AdminApiController@newCustomerAdd');
    Route::post('postAddPets', 'AdminApiController@newPetAdd');
    Route::post('postAppointment', 'AdminApiController@bookmyAppointment');
    Route::post('postUpdateAppointment', 'AdminApiController@editmyAppointment');
});


/**
 * All FRONT - API  customerapp
 */
Route::group(['middleware' => 'mobileapp_nonauth', 'prefix' => 'frontapi/v1'], function() {

    Route::get('view', 'ApiController@index');
    Route::post('makeguzzle', 'ApiController@makeguzzle');
    Route::post('postVersion', 'FrontApiController@checkversion');
    Route::post('postLogin', 'FrontApiController@login');
    Route::post('postRegister', 'FrontApiController@registerCustomer');
    Route::get('getAllTypes', 'FrontApiController@indexTypes');
    Route::post('postForgotPass', 'Customerauth\PasswordController@sendResetLinkEmail');
    Route::get('getBreed', 'FrontApiController@allBreedsGetData');
    Route::get('getCountry', 'FrontApiController@allCountryGetData');
    Route::get('getArea', 'FrontApiController@allAreaGetData');
    Route::get('getSize', 'FrontApiController@indexSizes');
    Route::get('getCommon', 'FrontApiController@allCommonData');
    Route::post('postProviders', 'FrontApiController@indexProviders');
    Route::get('aboutus', 'FrontApiController@cmspage');
    Route::get('termsandcondition', 'FrontApiController@cmsPage');
    Route::get('getKennel', 'FrontApiController@availableForKennel');
    Route::get('getPawnPages', 'FrontApiController@indexPetStores');
    Route::post('getGlobalMap', 'FrontApiController@getGlobalMap');
    Route::get('getEvents', 'FrontApiController@indexEvents');
    Route::get('getRatingStore', 'FrontApiController@getRatingStore');
    Route::get('getRatingProvider', 'FrontApiController@getRatingProvider');
});


Route::group(['middleware' => 'mobileapp', 'prefix' => 'frontapi/v1'], function() {

    Route::post('postPassword', 'FrontApiController@changePassword');
    Route::post('postUpdateProfile', 'FrontApiController@updateCustomer');
    Route::post('postAddPet', 'FrontApiController@storePet');
    Route::post('postUpdatePet', 'FrontApiController@updatePet');
    Route::post('postAddMedicalCard', 'FrontApiController@storeMedicalCard');
    Route::get('getPetDetails', 'FrontApiController@indexPet');
    Route::get('getMyProfile', 'FrontApiController@CustomerProfile');
    Route::post('postDisablePet', 'FrontApiController@deleteCustomerPet');

    Route::post('postServices', 'FrontApiController@servicesOffered');
    Route::post('postBookAppointment', 'FrontApiController@postBookAppoint');
    Route::post('postBookAppointmentTest', 'FrontApiController@postBookAppointTest');
    Route::post('postTechnicianAvailability', 'FrontApiController@checkTechnicianAvailability');
    Route::post('postTechnicianAvailability_v2', 'FrontApiController@checkTechnicianAvailability_v2');
    
    Route::post('postMedicalCard', 'FrontApiController@petMedicalgetAllData');
    
    Route::post('postEventLike','FrontApiController@postEventLike');
    Route::post('postEventDislike','FrontApiController@postEventDislike');
    Route::get('getEventDetails', 'FrontApiController@getEventDetails');

    Route::get('getProviderData', 'FrontApiController@showProviderInformation');
    Route::get('getBookedAppointments', 'FrontApiController@indexBookedAppointmentData');
    Route::post('postOnTimeBookingDetails', 'FrontApiController@indexOntimeBookingDetails');
    Route::delete('deleteAppointment', 'FrontApiController@destroyBookedAppointment');
    Route::post('cancelAppointment', 'FrontApiController@cancelBookedAppointment');
    Route::post('confirmAppointment', 'FrontApiController@confirmBookedAppointment');
    Route::get('getMyInbox', 'FrontApiController@indexInboxData');
    Route::post('getMyInboxDetail', 'FrontApiController@indexInboxDataDetail');
    Route::get('getInboxCount', 'FrontApiController@indexCountInbox');
    Route::post('postInboxHadRead', 'FrontApiController@storeCountUpdatehadRead');
    Route::get('getInboxUpdateisNew', 'FrontApiController@storeInboxUpdateIsNEW');
    Route::delete('deleteInboxMessage', 'FrontApiController@deleteInboxMessageData');

    // Route
    Route::post('postTransaction', 'FrontApiController@storeTransaction');
    Route::post('getAdoption', 'FrontApiController@availableForAdopt');
    Route::post('postAdoption', 'FrontApiController@makeAdoptRequest');
    Route::post('postPetLikes', 'FrontApiController@storePetLikes');
    #Route::delete('deletePetLikes', 'FrontApiController@destroyPetLikes'); // commented by Aditya on 12.07.2017
	Route::post('deletePetLikes', 'FrontApiController@destroyPetLikes');

    Route::get('getVolunteer', 'FrontApiController@availableForVolunteer');
    Route::post('postVolunteer', 'FrontApiController@makeVolunteerRequest');
    Route::get('getFoster', 'FrontApiController@availableForFoster');
    Route::post('postFostering', 'FrontApiController@makeFosteringRequest');

    Route::get('getSurrender', 'FrontApiController@availableForSurrender');
    Route::post('postSurrender', 'FrontApiController@storeSurrenderRequest');

    
    //Route::get('getKennelServices', 'FrontApiController@availableForKennelServices');
    Route::post('postKennel', 'FrontApiController@storeKennelRequest');
    
    Route::post('missYouRequest', 'FrontApiController@callMissYouRequest');
    Route::post('getMissYouResponse', 'FrontApiController@getMissYouResponse');
    
    //Rate and Review
    Route::post('postRatingProvider', 'FrontApiController@postRatingProvider');
    Route::post('postRatingStore', 'FrontApiController@postRatingStore');
    
    
    
});


Route::group(['middleware' => 'mobileapp', 'prefix' => 'frontapi/v2'], function() {
// New FUnction for booking - 29Jan17
   Route::post('postBookAppointment', 'FrontApiController@logic2postBookAppoint');
});

Route::get('api_tester', function () {
    return view('api.api_tester');
});
Route::get('test_mail', 'MissYouController@test_mail');
Route::get('getConfig', 'FrontApiController@getConfig');

// ?api_token=JZdZw3AVD85pTpE8fmqz9A9QQIiEcZlQeN2LsWAu1wELYXe5KkzSeMtN3wXZ
/*
* For the SUPER Admin
* All admin will have there files here
*/

