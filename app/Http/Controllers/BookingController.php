<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BookingRequest;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use \App\Booking;
use \App\Transaction;
use \App\Inbox;
use \App\Service;
use \App\Pet;
use DateTime;
use DatePeriod;
use DB;
use App\Events\BookingWasMade;
use App\Events\SendReminderofBooking;
use Illuminate\Support\Facades\Config;
use Mail;


class BookingController extends Controller
{

    public function index(Request $request)
    {
        //  $booking = DB::table('bookings')
        //             ->join('customers', 'bookings.customer_id', '=', 'customers.id')
        //             ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
        //             ->join('pets', 'bookings.pet_id', '=', 'pets.id')
        //             ->join('services', 'bookings.service_id', '=', 'services.id')
        //             ->join('categories', 'services.category_id', '=', 'categories.id')
        //             ->select(['customers.firstname','customers.mobile','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id', 'bookings.status','bookings.ontime','bookings.id as bookid'])
        //             ->get();
        // $book = (collect($booking));
        // $booking = $book->groupBy('ontime');
        if($request->ajax())
        {
           return $this->getDatatables($request);            
        }
        return view('servicepro.booking.book');
    }

    protected function getDatatables($request)
    {   
        $booking = DB::table('bookings')
                    ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                    ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                    ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                    ->join('services', 'bookings.service_id', '=', 'services.id')
                    ->join('categories', 'services.category_id', '=', 'categories.id')
                    ->join('transactions', 'transactions.id', '=', 'bookings.transaction_id')
                    ->where('bookings.provider_id', session('provider_id'))
                    ->select(['customers.id as cid','customers.firstname','customers.mobile','customers.email', 'pets.name as petname','pets.id as pid', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.ontime', 'bookings.type_id', 'bookings.noshow', 'bookings.status','bookings.id as bookid', 'bookings.transaction_id', 'transactions.result',DB::raw('count(*) as count')])
                    ->groupBy('bookings.ontime');
                    /*->orderBy('bookings.id','ASC')*/
                    
        $datatables =  app('datatables')->of($booking)

            ->editColumn('firstname', function($booking) {
                $name = '<a href="'.url('providers').'/customers/'.$booking->cid.'"><i class=" fa fa-eye"></i>'.$booking->firstname.'</a>';
                return $name;
            })
            ->editColumn('petname', function($booking) {
                if($booking->count > 1){
                    $name = 'Multiple';
                }else{
                    $name = '<a href="'.url('providers').'/showpet/'.$booking->pid.'"><i class=" fa fa-eye"></i>'.$booking->petname.'</a>';
                }
                
                return $name;
            })
            ->editColumn('payment', function($booking) {
                return view('servicepro.booking.payment', compact('booking'))->render();
            })
            ->editColumn('start', function($booking) {
                return Carbon::parse($booking->start)->format('l jS F Y h:i a');
            })
            ->addColumn('action', function($booking) {
                return view('servicepro.booking.action', compact('booking'))->render();
            })
            ->editColumn('status', '
                @if($status == 1 && $noshow=="true") 
                    <button class="btn btn-red btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>No Show</button>
                @elseif($status == 1 && $noshow=="false") 
                    <button class="btn btn-success btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Confirmed</button>
                @elseif($status == 2)  
                    <button class="btn btn-danger btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Cancelled</button>
                @else
                    <button class="btn btn-primary btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Unconfirmed</button> 
                @endif');

// return view('servicepro.booking.statusupdate', compact('booking'))->render();
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'bookings.status')
        {
            $val = $post;
            if(strtolower($val) == 'confirmed'){
                $post = '1';
            } elseif(strtolower($val) == 'unconfirmed'){
                $post = '0';
            } else {
                #Cancelled appointments
                $post = '2';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }

    public function store(BookingRequest $request)
    {
        $request['provider_id'] = session('provider_id');
        $stardate = Carbon::parse($request->input('start'));
        $request['end']  = $stardate->addMinutes($request->input('end'));
        // $request['enddate'] = ($request->input('enddate'));
        // debug($request->all());
        // die();
        $book = Booking::create($request->all());
        // store ID in session
        $request->session()->push('booking_ids', $book->id);
        if($book)
        {
            $info_book = Booking::SingleBookingDetail($book->id)->first();
            return json_encode(['confirm' => 'booking done', 'success' => true, 'event' => $info_book]);
        }
        else {
            return json_encode(['confirm' => 'Error', 'success' => false]);
        }
    }


    public function inboxmessage(Request $request)
    {
        if($request->session()->has('booking_ids'))
        {
            $ids = $request->session()->pull('booking_ids');
            $customer_id = $request->input('customerid');

            event(new BookingWasMade($ids));
            $ontime = Carbon::now()->timestamp;

            $tran['customer_id'] = $customer_id;
            $tran['result'] = '0';
            $transaction = Transaction::create($tran)->id;

            $bookData['ontime'] = $ontime;
            $bookData['transaction_id'] = $transaction;
            Booking::whereIn('id', $ids)->update($bookData);

            $makehtml = $this->displaySummaryofBooking($ontime);
            $data['success'] = true;
            $data['message'] = 'Bookings were Successful';
            $data['summary'] = $makehtml;
            $data['transaction'] = $transaction;
        } else {
            $data['success'] = false;
            $data['message'] = 'No Bookings Found, try again';
        }
        return $data;
    }

    public function sendReminderMessage(Request $request)
    {
        $ontime = $request->input('ontime');
        $customer = $request->input('customerid');
        $book = true;
        $booking = Booking::onTimeDetailAPI($customer, $ontime)->get();
        if ($booking) {
            //event(new SendReminderofBooking($ontime, $customer));
            $message = "Dear {$booking[0]['firstname']}, We would like to remind you of your appointment with {$booking[0]['provider_name']} for ";
            $date = $booking[0]['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
            $inbox['provider_id'] = $booking[0]['provider_id'];
            $inbox['customer_id'] = $booking[0]['customer_id'];
            $inbox['service_id'] = $booking[0]['service_id'];
            $inbox['booking_id'] = $booking[0]['booking_id'];
            $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
            $inbox['isnew'] = 1;
            $inbox['hadread'] = 1;
            $inbox['type'] = 1;  //Reminder Message
            $inbox['subject'] = push_messages('appointment_remider.en.title');
            $petdata=[];
            foreach ($booking as $key => $book) {
                $petdata[$book['pet_id']]['petname']=$book['petname'];
                $petdata[$book['pet_id']]['services'][]=$book['servicename'];
            }
            $email_m = "";
            foreach($petdata as $pet){
                $message .=" {$pet['petname']}: ".implode(",", $pet['services']);
                $email_m .=" {$pet['petname']}: ".implode(",", $pet['services']);
            }
            $message.=" at {$good_date}.";
            $email_m .= " at {$good_date}.";
            $inbox['message']=$message;
            //$message.= $book['firstname'] . ' Reminder for -' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;
            /*\App\Inbox::create($inbox);*/
            $message = \App\Inbox::Create($inbox);         
            $message_id = $message->id;
            
            $pushMessage_en=push_messages('appointment_remider.en.message');
            $pushMessage_ar=push_messages('appointment_remider.ar.message');
            
            $extraData=array('type'=>'APPOINTMENT','reminder'=>TRUE);
            sendPushNoti($pushMessage_en,$pushMessage_ar,$booking[0]['customer_id'],$message_id,$extraData);
            $email = array($booking[0]['email']);
            //$email = array('alpesh66@yopmail.com');
            $bcc_email = array();
            //$bcc_email = array('aditya.simplifiedinformatics@gmail.com');
            $data = array('customer_name' => $booking[0]['firstname'],'provider_name'=>$booking[0]['provider_name'],'email_m'=>$email_m);
            $result = Mail::send('emails.appointment_4_hr_reminder', $data, function ($message) use($email, $bcc_email) {
                $message->to($email);
                $message->subject('Appointment Reminder');
                if ($bcc_email) {
                  $message->bcc($bcc_email);
                }
            });

            $data['success'] = true;
            $data['message'] = 'Reminder Sent successfully';
        } else {
            $data['success'] = false;
            $data['message'] = 'No booking details found!!';
        }
        return $data;
    }
    public function setNoShow(Request $request)
    {
        $ontime = $request->input('ontime');
        $customer = $request->input('customerid');
        $bookobj = Booking::where('ontime', $ontime)->where('customer_id', $customer)->get();
        foreach ($bookobj as $key => $value) {
            $vaccData['noshow'] = true;           
            $r = Booking::where('id', $value->id)->update($vaccData); 
        }
        $data['success'] = true;
        $data['message'] = 'Status changed successfully';
        return $data;
    }


    public function detailedonTimeBookings(Request $request)
    {
        $ontime = $request->input('ontime');
        $makehtml = $this->displaySummaryofBooking($ontime);
        return $makehtml;
    }
    public function viewBookingPopupFromCalender(Request $request)
    {
        $id = $request->input('ontime');
        $makehtml = $this->displaySummaryofBookingForPopup($id);
        return $makehtml;
    }
    protected function displaySummaryofBookingForPopup($id)
    {
        $allBookings =  Booking::SingleBookingDetail($id)->get();
        $makehtml = '';
        if($allBookings){
            $prepare = collect($allBookings);
            $petsBook = $prepare->groupBy('petname');
            /*echo "<pre>";
            print_r($petsBook->all());*/
            
            foreach ($petsBook->all() as $pet_id => $value) {
                /*echo "<pre>";
                print_r($value[$i]->petId);*/
                //{{ url($setAction.'/pet', $pets->id) }}
                //<a href="providers/pet/'.$petID.'">'.$pet_id.'</a>
                //

                //$petID='providers/pet/'.$value[$i]->petId;
                 $petlink = '<a style="color:#01aef2;" href="'.url('providers').'/pet/'.$value[0]->petId.'"><i class=" fa fa-eye"></i>'.$pet_id.'</a>';
                $makehtml .= '<div class="table-responsive"> <h3>'.$petlink.'</h3> <table class="table table-bordered table-striped table-hover"> ';
                $makehtml .= '<thead><tr><td>Technician</td><td>Service</td><td>Price</td><td>Start</td> </tr> </thead><tbody>';
                foreach ($value as $pet_key => $book_value) {
                    $makehtml .= '<tr> <td> '.$book_value['techname'].' </td> <td>'.$book_value['servicename'].'</td><td>'.$book_value['price'].'</td> <td>'.Carbon::parse($book_value['start'])->format('l jS F Y h:i a').'</td></tr>';
                }
                $makehtml .= '</tbody></table></div>';
                
            }

        }

        return $makehtml;
    }
    protected function displaySummaryofBooking($ontime)
    {
        $allBookings =  Booking::OnTimeBookingDetail($ontime)->get();
        $makehtml = '';
        if($allBookings){
            $prepare = collect($allBookings);
            $petsBook = $prepare->groupBy('petname');
            
            foreach ($petsBook->all() as $pet_id => $value) {
                $confirmation= $value[0]->confirmation;
                /*$confirmation = */
                /*echo "<pre>";
                print_r($value[$i]->petId);*/
                //{{ url($setAction.'/pet', $pets->id) }}
                //<a href="providers/pet/'.$petID.'">'.$pet_id.'</a>
                //

                //$petID='providers/pet/'.$value[$i]->petId;
                 $petlink = '<a style="color:#01aef2;" href="'.url('providers').'/pet/'.$value[0]->petId.'"><i class=" fa fa-eye"></i>'.$pet_id.'</a>';
                $makehtml .= '<div class="table-responsive"> <h3>'.$petlink.'</h3> <table class="table table-bordered table-striped table-hover"> ';
                $makehtml .= '<thead><tr><td>Technician</td><td>Service</td><td>Price</td><td>Start</td> </tr> </thead><tbody>';
                foreach ($value as $pet_key => $book_value) {
                    $makehtml .= '<tr> <td> '.$book_value['techname'].' </td> <td>'.$book_value['servicename'].'</td><td>'.$book_value['price'].'</td> <td>'.Carbon::parse($book_value['start'])->format('l jS F Y h:i a').'</td></tr>';
                }
                $makehtml .= '</tbody></table></div>';
                
            }

        }
        $data=array();
        $data['makehtml']=$makehtml;
        $data['ontime']=$ontime;
        $data['confirmation']=$confirmation;
        return $data;
    }

    public function update($id, Request $request)
    {
        $booking = Booking::findorfail($id);
        $booking->update($request->all());

        if($booking)
        {
            $data['success'] = true;
            $data['message'] = 'Update Successfully.';
            $data['event'] = Booking::SingleBookingDetail($id)->first();
            
        } else {
            $data['success'] = false;
            $data['message'] = 'Some server error, kindly update again';
        }
        return $data;
    }

    /**
     * Update from the Calendar page to update the status
     * @param  Request $request customer_id and ontime
     * @return [type]           boolean
     */
    public function updateBookingStatus(Request $request)
    {
        $this->logFile = '../storage/logs/BookedMessage_' . date('d-m-Y') . '.txt';
        file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);

        $ontime = $request->input('ontime');
        $customerid = $request->input('customerid');
        $status = $request->input('status');
        

        $bookobj = Booking::where('ontime', $ontime)->where('customer_id', $customerid)->get();
        /*$bookings = array();*/
        $customer_email = false;
        $provider_email = false;
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
        $sikw_email = env('SIKW_BCC_COPY', false);
        $emailHtml = array();
        $booking = [];
        $petdata = [];
        $servicesArray = array();
        foreach ($bookobj as $key => $value) {
            $bookings['booking'][] = $value->id;                       
            $vaccData['status'] = $status;           
            $vaccData['noshow'] = 'false';           
            $r = Booking::where('id', $value->id)->update($vaccData); 
            //this for change grom date on confirm and cancel
            if($status==1){
                $groomObj = Service::select('services.category_id','categories.parentid')
                ->leftjoin('categories', 'categories.id', '=', 'services.category_id')
                ->where('services.id',$value->service_id)
                ->first()->toArray();
                if($groomObj['parentid']==2){ //
                    $petLastGroomDdate = \DB::table('pets')->select('next_groom_appointment')->where('id',$value->pet_id)->first();
                    if(!empty($petLastGroomDdate->next_groom_appointment)){
                       /* echo $value->pet_id;
                        echo $value->start;*/
                        Pet::find($value->pet_id)->update(['last_groom_appointment' => $petLastGroomDdate->next_groom_appointment,'next_groom_appointment'=>$value->start]);    
                    }
                }
                if($groomObj['parentid']==1){ //
                    //echo "this for vet";
                }    
            }
            if($status==2){
                $groomObj = Service::select('services.category_id','categories.parentid')
                ->leftjoin('categories', 'categories.id', '=', 'services.category_id')
                ->where('services.id',$value->service_id)
                ->first()->toArray();
                if($groomObj['parentid']==2){ //
                    $petLastGroomDdate = \DB::table('pets')->select('last_groom_appointment')->where('id',$value->pet_id)->first();
                    /*echo $value->pet_id;*/
                    if(!empty($petLastGroomDdate->last_groom_appointment)){
                        Pet::find($value->pet_id)->update(['next_groom_appointment'=>$petLastGroomDdate->last_groom_appointment]);    
                    }
                }
                if($groomObj['parentid']==1){ //
                    //echo "this for vet";
                }    
            }
            
        }         

        if($status==0){

        }elseif($status==1){
            file_put_contents($this->logFile, "\n\n =========booking booked : " . json_encode($booking) . "\n\n", FILE_APPEND | LOCK_EX);
            foreach ($bookings as $value) {

                file_put_contents($this->logFile, "\n\n =========value : " . json_encode($value) . "\n\n", FILE_APPEND | LOCK_EX);
                foreach ($value as $key => $bookData) {
                    $book = Booking::SingleBookingDetail($bookData)->first();
                    file_put_contents($this->logFile, "\n\n =========book booked : " . json_encode($book) . "\n\n", FILE_APPEND | LOCK_EX);
                    if ($book) {
                        $date = $book['start'];
                        $good_date = Carbon::parse($date)->format('l, M jS Y');
                        $email_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                        $booking[$key]= $book;
                        $petdata[$book['pet_id']]['petname'][] = $book['petname'];
                        $petdata[$book['pet_id']]['services'][] = $book['servicename'];
                        $servicesArray[$key] = $book['servicename'];
                        $image_url = 'images/category.png';
                        if (!empty($book['category_image'])) {
                            if (file_exists('uploads/categories/' . $book['category_image'])) {
                                $image_url = 'uploads/categories/' . $book['category_image'];
                            }
                        }

                        $emailHtml[$book['petname']][$key] = array(
                            'category_image' => $image_url,
                            'servicename' => $book['servicename']
                        );

    //                   Send Email to Provider, Customer and Admin
                        $customer_email = $book['customer_email'];
                        $provider_email = $book['providers_email'];
                        $email_datacustomer_name = $book['firstname'];
                        $email_dataprovider_name = $book['provider_name'];
                        $customers_mobile = $book['customers_mobile'];
                        $booking_date = Carbon::parse($date)->format('F d Y');
                        $booking_time = Carbon::parse($date)->format('h:i a');
                        //\App\Inbox::create($booking);
                    }
                }
            }
            $messageCommon = "";
            $uservice = array_unique($servicesArray);
           
            $customer_id = "";
            if (!empty($booking)) {
                $pushMessage_en=push_messages('confirm_appointment.en.message');
                $pushMessage_ar=push_messages('confirm_appointment.ar.message');
               
                $message1 = "Dear {$booking[0]['firstname']},<br/>We would like to inform you that your appointment with {$booking[0]['provider_name']} is booked for ";
                $date = $booking[0]['start'];
                $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                $inbox['provider_id'] = $booking[0]['provider_id'];
                $inbox['customer_id'] = $booking[0]['customer_id'];
                $inbox['service_id'] = $booking[0]['service_id'];
                $inbox['booking_id'] = $booking[0]['bookid'];
                $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
                $inbox['isnew'] = 1;
                $inbox['hadread'] = 1; // 1 for unread // 0 for read
                $inbox['type'] = 0;  // 1 for reminder message , 0 for inbox massage 
                $inbox['subject'] = push_messages('confirm_appointment.en.title');
                foreach ($petdata as $pet) {
                    $upet=array_unique($pet['petname']);
                    $messageCommon .= implode(",", $upet)." :" . implode(",", $uservice);
                }
                $message2 = " on {$good_date} is booked.";
                $customer_id = $booking[0]['customer_id'];
            }
            $message = $message1.' '.$messageCommon.' '.$message2;
            
            $inbox['message'] = $message;
            $message = \App\Inbox::create($inbox);
            $message_id = $message->id;
                $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
                sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
                $booking_time = Carbon::parse($date)->format('h:i a');

            $data['order_details'] = $emailHtml;
            $data['customer_name'] = $email_datacustomer_name;
            $data['provider_name'] = $email_dataprovider_name;
            $data['booking_date'] = Carbon::parse($booking[0]['start'])->format('F d Y');
            $data['booking_time'] = Carbon::parse($booking[0]['start'])->format('h:i a');
            $data['customers_mobile'] = $customers_mobile;

            file_put_contents($this->logFile, "\n\n =========data : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);

            //send email to customer
            if ($customer_email) {
                try {
                    Mail::queue('emails.bookingBookedEmailtoCustomer', $data, function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject('Wiggle - Booking Confirmed');
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingBookedEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingBookedEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    log_message("ERROR", $e->getMessage());
                }
            }

            //  send email to provider
            if ($provider_email) {
                try {
                    Mail::queue('emails.bookingBookedEmailtoProvider', $data, function ($message) use ($provider_email, $sikw_email) {
                        $message->to($provider_email);
                        $message->subject('Wiggle - Booking Confirmed');
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingBookedEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingBookedEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    log_message("ERROR", $e->getMessage());
                }
            }
        }elseif($status==2){
            file_put_contents($this->logFile, "\n\n =========booking cancel : " . json_encode($booking) . "\n\n", FILE_APPEND | LOCK_EX);
            foreach ($bookings as $value) {

                file_put_contents($this->logFile, "\n\n =========value : " . json_encode($value) . "\n\n", FILE_APPEND | LOCK_EX);
                foreach ($value as $key => $bookData) {
                    $book = Booking::SingleBookingDetail($bookData)->first();
                    file_put_contents($this->logFile, "\n\n =========book cancel : " . json_encode($book) . "\n\n", FILE_APPEND | LOCK_EX);
                    if ($book) {
                        $date = $book['start'];
                        $good_date = Carbon::parse($date)->format('l, M jS Y');
                        $email_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                        $booking[$key]= $book;
                        $petdata[$book['pet_id']]['petname'][] = $book['petname'];
                        $petdata[$book['pet_id']]['services'][] = $book['servicename'];
                        $servicesArray[$key] = $book['servicename'];
                        $image_url = 'images/category.png';
                        if (!empty($book['category_image'])) {
                            if (file_exists('uploads/categories/' . $book['category_image'])) {
                                $image_url = 'uploads/categories/' . $book['category_image'];
                            }
                        }

                        $emailHtml[$book['petname']][$key] = array(
                            'category_image' => $image_url,
                            'servicename' => $book['servicename']
                        );

    //                   Send Email to Provider, Customer and Admin
                        $customer_email = $book['customer_email'];
                        $provider_email = $book['providers_email'];
                        $email_datacustomer_name = $book['firstname'];
                        $email_dataprovider_name = $book['provider_name'];
                        $customers_mobile = $book['customers_mobile'];
                        $booking_date = Carbon::parse($date)->format('F d Y');
                        $booking_time = Carbon::parse($date)->format('h:i a');
                        //\App\Inbox::create($booking);
                    }
                }
            }
            $messageCommon = "";
            $uservice = array_unique($servicesArray);
           
            $customer_id = "";
            if (!empty($booking)) {
                $pushMessage_en=push_messages('cancel_appointment.en.message');
                $pushMessage_ar=push_messages('cancel_appointment.ar.message');
               
                $message1 = "Dear {$booking[0]['firstname']},<br/>We would like to inform you that your appointment with {$booking[0]['provider_name']} is canceled for ";
                $date = $booking[0]['start'];
                $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
                $inbox['provider_id'] = $booking[0]['provider_id'];
                $inbox['customer_id'] = $booking[0]['customer_id'];
                $inbox['service_id'] = $booking[0]['service_id'];
                $inbox['booking_id'] = $booking[0]['bookid'];
                $inbox['bookdate'] = Carbon::parse($booking[0]['start']);
                $inbox['isnew'] = 1;
                $inbox['hadread'] = 1;
                $inbox['type'] = 2;  // 1 for reminder message , 0 for inbox massage 
                $inbox['subject'] = push_messages('cancel_appointment.en.title');
                foreach ($petdata as $pet) {
                    $upet=array_unique($pet['petname']);
                    $messageCommon .= implode(",", $upet)." :" . implode(",", $uservice);
                }
                $message2 = " on {$good_date} .";
                $customer_id = $booking[0]['customer_id'];
            }
            $message = $message1.' '.$messageCommon.' '.$message2;
            
            $inbox['message'] = $message;
            $message = \App\Inbox::create($inbox);
            $message_id = $message->id;
                $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
                sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
                $booking_time = Carbon::parse($date)->format('h:i a');

            $data['order_details'] = $emailHtml;
            $data['customer_name'] = $email_datacustomer_name;
            $data['provider_name'] = $email_dataprovider_name;
            $data['booking_date'] = Carbon::parse($booking[0]['start'])->format('F d Y');
            $data['booking_time'] = Carbon::parse($booking[0]['start'])->format('h:i a');
            $data['customers_mobile'] = $customers_mobile;

            file_put_contents($this->logFile, "\n\n =========data : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);

            //send email to customer
            if ($customer_email) {
                try {
                    Mail::queue('emails.bookingCancelEmailtoCustomer', $data, function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject('Wiggle - Booking Cancelled');
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingCancelEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingCancelEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    log_message("ERROR", $e->getMessage());
                }
            }

            //  send email to provider
            if ($provider_email) {
                try {
                    Mail::queue('emails.bookingCancelEmailtoProvider', $data, function ($message) use ($provider_email, $sikw_email) {
                        $message->to($provider_email);
                        $message->subject('Wiggle - Booking Cancelled');
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingCancelEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    file_put_contents($this->logFile, "\n\n =========bookingCancelEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    log_message("ERROR", $e->getMessage());
                }
            }

        }  
    $data['success'] = true;
    $data['message'] = 'Status updated Successfully';
    return $data;

        /*if($book){
            $book->status=$status;
            $book->save();
            $data['success'] = true;
            $data['message'] = 'Status updated Successfully';
            $info_book = Booking::SingleBookingDetail($book->id)->first();

            if($status==0){
                $pushMessage_en=push_messages('unconfirm_appointment.en.message');
                $pushMessage_ar=push_messages('unconfirm_appointment.ar.message');
                $customer_id=$info_book->customer_id;
                $request_id=$info_book->id;

                $reminder['message'] = $pushMessage_en;
                $reminder['provider_id'] = $book->provider_id;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = $book->id;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 1;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customer_id;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('unconfirm_appointment.en.title');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;

                $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
                sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id,$extraData);
            }elseif($status==1){               
               $pushMessage_en=push_messages('confirm_appointment.en.message');
               $pushMessage_ar=push_messages('confirm_appointment.ar.message');

               $customer_id=$info_book->customer_id;
               $request_id=$info_book->id;
               $for=$info_book->servicename;

               $reminder['message'] = $pushMessage_en;
               $reminder['provider_id'] = $book->provider_id;
               $reminder['service_id'] = 0;
               $reminder['booking_id'] = $book->id;
               $reminder['isnew'] = 0;
               $reminder['hadread'] = 1;
               $reminder['type'] = 1;
               $reminder['customer_id'] = $customer_id;
               $reminder['sender'] = 1;
               $reminder['subject'] = push_messages('confirm_appointment.en.title');
               $message = Inbox::Create($reminder);         
               $message_id = $message->id;

               $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
               sendPushNoti($pushMessage_en, $pushMessage_ar, $customer_id,$message_id,$extraData);
            }elseif($status==2){
                 $pushMessage_en=push_messages('cancel_appointment.en.message');
                 $pushMessage_ar=push_messages('cancel_appointment.ar.message');
                 $customer_id=$info_book->customer_id;
                 $request_id=$info_book->id;

                 $reminder['message'] = $pushMessage_en;
                 $reminder['provider_id'] = $book->provider_id;
                 $reminder['service_id'] = 0;
                 $reminder['booking_id'] = $book->id;
                 $reminder['isnew'] = 0;
                 $reminder['hadread'] = 1;
                 $reminder['type'] = 1;
                 $reminder['customer_id'] = $customer_id;
                 $reminder['sender'] = 1;
                 $reminder['subject'] = push_messages('cancel_appointment.en.title');
                 $message = Inbox::Create($reminder);         
                 $message_id = $message->id;
                 $extraData=array('type'=>'APPOINTMENT','reminder'=>FALSE);
                 sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id,$extraData);
            }
        } else {
            $data['success'] = false;
            $data['message'] = 'Some internal error occourred';
        }
        return $data;*/
    }

    public function destroy(Request $request, $id)
    {   
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDeleted = Booking::whereIn('ontime', $ids)->delete();

            if($isDeleted)
            {
                $data['success'] = true;
                $data['message'] = 'Booking deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }
        Booking::destroy($id);
        Session::flash('flash_message', 'Booking deleted!');

        return redirect('booking');
    }

    public function delbooking(Request $request, $id)
    {   //echo 'Hi'; exit;
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDeleted = Booking::destroy($id);

            if($isDeleted)
            {
                $data['success'] = true;
                $data['message'] = 'Booking deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }
        Booking::destroy($id);
        // Session::flash('flash_message', 'Booking deleted!');

        return redirect('customers');
    }

    public function veternary()
    {
        $booking = Booking::with('customer','technician', 'pet','category')->OfProvider()->TypeVeternary()->paginate('15');
        return view('servicepro.booking.vet', compact('booking'));
    }

    public function groomer()
    {
        $booking = Booking::with('customer','technician', 'pet','category')->OfProvider()->TypeGroomer()->paginate('15');
        return view('servicepro.booking.groom', compact('booking'));
    }

    public function trainer()
    {
        $booking = Booking::with('customer','technician', 'pet','category')->OfProvider()->TypeTrainer()->paginate('15');
        return view('servicepro.booking.train', compact('booking'));
    }

    public function check_availability()
    {
        $schedule = [
            'start' => '2016-11-18 09:00:00',
            'end' => '2016-11-18 17:00:00',
        ];

        $start = Carbon::instance(new DateTime($schedule['start']));
        $end = Carbon::instance(new DateTime($schedule['end']));

        $events = [
            
            [
                'created_at' => '2016-11-18 14:00:00',
                'updated_at' => '2016-11-18 16:00:00',
            ],
            [
                'created_at' => '2016-11-18 09:00:00',
                'updated_at' => '2016-11-18 09:30:00',
            ],
            [
                'created_at' => '2016-11-18 10:00:00',
                'updated_at' => '2016-11-18 11:15:00',
            ],
        ];

        $minSlotHours = 0;
        $minSlotMinutes = 15;
        $minInterval = CarbonInterval::hour($minSlotHours)->minutes($minSlotMinutes);

        $reqSlotHours = 0;
        $reqSlotMinutes = 15;
        $reqInterval = CarbonInterval::hour($reqSlotHours)->minutes($reqSlotMinutes);

        foreach(new DatePeriod($start, $minInterval, $end) as $slot){
            $to = $slot->copy()->add($reqInterval);

            echo $slot->toDateTimeString() . ' to ' . $to->toDateTimeString();

            if($this->slotAvailable($slot, $to, $events)){
                echo ' is available';
            }

            echo '<br />';
        }
    }

    public function slotAvailable($from, $to, $events){
        foreach($events as $event){
            $eventStart = Carbon::instance(new DateTime($event['created_at']));
            $eventEnd = Carbon::instance(new DateTime($event['updated_at']));
            if($from->between($eventStart, $eventEnd) && $to->between($eventStart, $eventEnd)){
                return false;
            }
        }
        return true;
    }

    public function postCalendarevents(Request $request)
    {
        $start  = $request->input('start');
        $end    = $request->input('end');
        $zone   = $request->input('timezone');
        $view   = $request->input('viewtype');
        $get_start = (Carbon::createFromTimestamp($start)->toDateTimeString());
        $get_final = (Carbon::createFromTimestamp($end)->toDateTimeString());

        $all_appointment = Booking::calendarListings($get_start, $get_final)->get();

        return $all_appointment;
    }

    public function onlyCalendarforTechnician(Request $request)
    {
        $start  = $request->input('start');
        $end    = $request->input('end');
        $zone   = $request->input('timezone');
        $view   = $request->input('viewtype');
        $get_start = (Carbon::createFromTimestamp($start)->toDateTimeString());
        $get_final = (Carbon::createFromTimestamp($end)->toDateTimeString());
        $techs = explode(',', \Auth::user()->technicians);
        $all_appointment = Booking::calendarListings($get_start, $get_final, $techs)->get();

        return $all_appointment;
    }

    public function indexOnlyCalendar(Request $request)
    {
        if($request->ajax())
        {
            return $this->getOnlyCalendarDatatables($request);
        }
        return view('servicepro.booking.onlytechbook');
    }

    protected function getOnlyCalendarDatatables($request)
    {
        $techns = explode(',',\Auth::user()->technicians);
        $booking = DB::table('bookings')
            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->join('transactions', 'transactions.id', '=', 'bookings.transaction_id')
            ->where('bookings.provider_id', session('provider_id'))
            ->select(['customers.id as cid','customers.firstname','customers.mobile','customers.email', 'pets.name as petname','pets.id as pid', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.ontime', 'bookings.type_id', 'bookings.status','bookings.id as bookid', 'bookings.transaction_id', 'transactions.result'])
            ->whereIn('bookings.technician_id', $techns)
            ->groupBy('bookings.ontime')
        ;

        $datatables =  app('datatables')->of($booking)

            //->editColumn('firstname', function($booking) {
//                $name = '<a href="'.url('providers').'/customers/'.$booking->cid.'"><i class=" fa fa-eye"></i>'.$booking->firstname.'</a>';
//                return $name;
                //return $booking->firstname;
            //})
            //->editColumn('petname', function($booking) {
//                $name = '<a href="'.url('providers').'/showpet/'.$booking->pid.'"><i class=" fa fa-eye"></i>'.$booking->petname.'</a>';
//                return $name;
              //  return $booking->petname;
            //})
            ->editColumn('firstname', function($booking) {
                $booking->firstname= '<a href="'.url('tech').'/customers/'.$booking->cid.'"><i class=" fa fa-eye"></i> '.$booking->firstname.'</a>';
                return $booking->firstname;
            })
            ->editColumn('petname', function($booking) {
                $booking->petname = '<a href="'.url('tech').'/showpet/'.$booking->pid.'"><i class=" fa fa-eye"></i> '.$booking->petname.'</a>';
                return $booking->petname; 
            })
            ->editColumn('payment', function($booking) {
                return view('servicepro.booking.payment', compact('booking'))->render();
            })
            ->editColumn('start', function($booking) {
                return Carbon::parse($booking->start)->format('l jS F Y h:i a');
            })
            ->addColumn('action', function($booking) {
                return view('servicepro.booking.onlytechaction', compact('booking'))->render();
            })
            ->editColumn('status', '@if($status == 1) 
                    <button class="btn btn-success btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Confirmed</button>
                @elseif($status == 2)  
                    <button class="btn btn-danger btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Cancelled</button>
                @else
                    <button class="btn btn-primary btn-xs bookingStatus" data-ontime={{$ontime}} data-customerid={{$cid}} data-currstatus={{$status}}>Unconfirmed</button> 
                @endif');

// return view('servicepro.booking.statusupdate', compact('booking'))->render();
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'bookings.status')
        {
            $val = $post;
            if(strtolower($val) == 'confirmed'){
                $post = '1';
            } elseif(strtolower($val) == 'unconfirmed'){
                $post = '0';
            } else {
                #Cancelled appointments
                $post = '2';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
}
