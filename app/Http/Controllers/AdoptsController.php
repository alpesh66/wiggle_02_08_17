<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Adopt;
use App\Customer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Http\Requests\AdoptRequest;
use Illuminate\Support\Facades\Config;
use Mail;
use App\Pet;
use App\Inbox;

class AdoptsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
       /* $adopts = Adopt::latest()->allMessage()
                        //->orderBy('is_read','DESC')
                        //->orderBy('sender','DESC')
                        //->orderBy('created_at','DESC')
                        ->paginate(15);
        //dd($adopts);
        return view('/society/adopts.index_old', compact('adopts'));
                        exit();
*/        
        if($request->ajax())
        {            
            $adopts = Adopt::
            select(['adopts.id', 'adopts.created_at', 'adopts.updated_at', 'adopts.title','adopts.status','adopt_pet.pet_id','pets.breed','pets.name as petName', 'adopts.parent_id','adopts.is_read','adopts.sender','adopts.customer_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS name')])
            ->leftjoin('adopt_pet', 'adopt_pet.adopt_id', '=', 'adopts.id')
            ->leftjoin('pets', 'adopt_pet.pet_id', '=', 'pets.id')
            ->leftjoin('customers', 'customers.id', '=', 'adopts.customer_id')
            ->where('adopts.provider_id', session('provider_id'))
            ->where('adopts.sender', 1)
            ->groupBy('adopts.id')
            /*->orderBy('adopts.created_at','DESC')*/;

            $datatables =  app('datatables')->of($adopts)

                ->addColumn('action', function($adopts) {
                    return view('society.adopts.action', compact('adopts'))->render();
                })
                ->editColumn('name', function($adopts) {
                    return "<a style='font-weight: bold;' href='".url('societies/customers/'.$adopts->customer_id)."' >".$adopts->name."</a>";
                })
                ->addColumn('petType', function($adopts) {
                    if($adopts->breed==1)
                        return 'Cat';
                    else
                        return 'Dog';
                })
                ->editColumn('petName', function($adopts) {
                    return "<a style='font-weight: bold;' href='".url('societies/pet/'.$adopts->pet_id)."' >".$adopts->petName."</a>";
                })
                ->editColumn('updated_at', function($adopts) {
                    if($adopts->parent_id==0)
                        return NULL;
                    else
                        return $adopts->updated_at;
                })
                ->editColumn('status', '@if($status == 1) Approved @elseif($status == 2) Reject @else Pending  @endif')
                ->setRowClass(function ($adopts) {
                    if($adopts->is_read == 1 && $adopts->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'approved'){
                    $post = '1';
                } elseif(strtolower($val) == 'reject'){
                    $post = '2';
                }else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('society.adopts.index');
        //return view('/society/adopts.index', compact('adopts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('/society/adopts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        
        Adopt::create($request->all());

        Session::flash('flash_message', 'Adopt added!');

        return redirect('adopts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $adopt = Adopt::with('pet')->findOrFail($id);
        $adopt->is_read = "0";
        $adopt->save();

        $customer = Customer::with('pet')->findOrFail($adopt->customer->id);
        /*echo "<pre>";
        print_r($customer);
        exit();*/

        return view('/society/adopts.show', compact('adopt','customer'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $adopt = Adopt::findOrFail($id);

        return view('society/adopts.edit', compact('adopt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $adopt = Adopt::findOrFail($id);
        $adopt->update($request->all());

        Session::flash('flash_message', 'Adopt updated!');

        return redirect('adopts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $ids = $request->id; 
        if(!empty($ids)){
            foreach ($ids as $key => $id) {
                Adopt::destroy($id);
            }    
            return json_encode(array('status'=>'1','message'=>'Adopt deleted!'));
        }else{
            Adopt::destroy($id);
            Session::flash('flash_message', 'Adopt deleted!');
            return redirect('societies/adopts');
        }   
        
        
    }

    public function reply(AdoptRequest $request){
        $adopUpdate = Adopt::findOrFail($request->input('id'));
  /*      $pet = $adopUpdate->pet->first();
        $pet_id = $pet['id'];*/
        $request['parent_id'] = $request->input('id');
        $status = $request->input('status');
        $description = $request->input('description');
        $customerid = $adopUpdate->customer_id;
        $adopUpdate->update(['parent_id' =>$request->input('id'),'is_read'=>0,'status'=>$status]);
        /*$adopUpdate->update($request->all());*/
        
        $request['provider_id'] = session('provider_id');
        $request['parent_id'] = $request->input('id');
        $request['sender'] = 2;
        if($status==1){
            $pet=$adopUpdate->pet->first();
            $petLog=[
                'pet_id'=>$pet['id'],
                'provider_id'=>session('provider_id'),
                'status'=>'Adopt',
                'admin_id'=>  session('user_id'),
                'customer_id'=>$adopUpdate->customer_id
            ];
            \App\Petlog::create($petLog);
            \App\Pet::where('id', '=', $pet['id'])->update(['pet_status' =>'Adopt','isadopted'=>1]);
            \DB::table('customer_pet')->insert(['customer_id'=>$adopUpdate->customer_id,'pet_id'=>$pet['id']]);
        }
        //event(new \App\Events\AdoptConfirmed($adopUpdate->id));
        
        Adopt::create($request->all());
        // send push notification to user            
            /*if($status==0){
                send_push_notification(push_messages('pending_adoption.en.message'), push_messages('pending_adoption.ar.message'),$customerid);
            }else*/
            $Adopts = Adopt::findOrFail($request->input('id'));
            $data = array();
            $customer_email = false;
            $provider_email = false;            
            $booking_date = false;
            $booking_time = false;
            $customers_mobile = false;
            $customer_id=$Adopts->customer_id;
            $provider_id=$Adopts->provider_id;
            $request_id=$Adopts->id;
            $sikw_email = env('SIKW_BCC_COPY', false);
            $fost= Adopt::SingleBookingDetail($request_id)->first(); 
            if(isset($fost['petid'])){
                $pet_detail = Pet::select('name')->where('id', $fost['petid'])->first();
                $pet_name = $pet_detail->name;    
            }else{
                $pet_name = "";    
            }
            $customer_email = $fost['customer_email'];
            $customer_name = $fost['customer_name'];
            $provider_name = $fost['provider_name'];
            
            $provider_email = $fost['providers_email'];
            if($status==1){
                /*send_push_notification(push_messages('approved_adoption.en.message'), push_messages('approved_adoption.ar.message'),$customerid);*/
                $pushMessage_en=push_messages('approved_adoption.en.message');
                $pushMessage_ar=push_messages('approved_adoption.ar.message');
                /*$customer_id=$Adopts->customer_id;
                $provider_id=$Adopts->provider_id;*/

                $reminder['message'] = "Dear ".$customer_name.",<br/>Your adoption request was approved successfully. Check your profile ".$pet_name."'s documents were transferred.";
                $reminder['provider_id'] = $provider_id;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 1;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customer_id;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('approved_adoption.en.title');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;
                $data = array('customer_name' => $customer_name, 'provider_name' => $provider_name,'description' => $description,'pet_name' => $pet_name);

                $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
                Mail::queue('emails.Adoption_Approval',$data,function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject("Adopt request is approved");
                    if ($sikw_email) {
                      $message->bcc($sikw_email);
                    }
                }); 
                sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
                return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
            }elseif($status==2){
                /*send_push_notification(push_messages('reject_adoption.en.message'), push_messages('reject_adoption.ar.message'),$customerid);*/
                $pushMessage_en=push_messages('reject_adoption.en.message');
                $pushMessage_ar=push_messages('reject_adoption.ar.message');
                $customer_id=$Adopts->customer_id;
                $provider_id=$Adopts->provider_id;

                $reminder['message'] = "Dear ".$customer_name.",<br/>Your Adoption request was rejected.";
                $reminder['provider_id'] = $provider_id;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 1;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customer_id;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('reject_adoption.en.title');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;
                $data = array('customer_name' => $customer_name, 'provider_name' => $provider_name,'description' => $description);
                Mail::queue('emails.Adoption_Rejected',$data,function ($message) use ($customer_email, $sikw_email) {
                    $message->to($customer_email);
                    $message->subject("Adopt request is Rejected");
                    if ($sikw_email) {
                      $message->bcc($sikw_email);
                    }
                }); 

                $extraData=array('type'=>'ADOPT','reminder'=>FALSE);
                sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
                return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
            }
        return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
        
        
    }
}
