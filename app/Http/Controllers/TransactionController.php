<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transaction;
use App\Booking;

use Carbon\Carbon;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    	if($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('transactions.index');
    }


     protected function callDatatables($request)
    {
        

        $transaction = \DB::table('transactions')
            ->select(['transactions.*','bookings.ontime','customers.id as customer_id','customers.mobile','customers.email',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS customer_name')])
          ->rightjoin('customers', 'transactions.customer_id', '=', 'customers.id')
          ->rightjoin('bookings', 'bookings.transaction_id', '=', 'transactions.id')
          ->whereIn('transactions.result', ['CAPTURED','CANCELED'])
          ->groupBy('transactions.id');
        

        $datatables =  app('datatables')->of($transaction)
          
/*            ->editColumn('created_at', function($transactions) {
                $start = Carbon::parse($transactions->created_at)->format('jS F Y');
                return $start;
            })*/
            ->editColumn('customer_name', function($transaction) {
                return "<a style='font-weight: bold;' href='".url('customers/'.$transaction->customer_id)."' ><i class='fa fa-eye'></i>".$transaction->customer_name."</a>";
            })
            ->editColumn('amount', function($transaction) {
                if(isset($transaction->amount) && $transaction->amount!=""){
                    return number_format ($transaction->amount, 3)." KD";    
                }else{
                    return "";
                }
                
            })
            ->editColumn('result', function($transactions) {
                if($transactions->result == 'CAPTURED')
                    return 'Success';
                elseif($transactions->result == 'CANCELED')
                    return 'Fail';
                        
                        
            })
            ->addColumn('action', function($transactions) {
                return view('transactions.action', compact('transactions'))->render();
            })
            ;

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $post_origin       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        


        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'result')
        {
            $val = $post_origin;
            if(strtolower($val) == 'success'){
                $post = 'CAPTURED';
            } elseif(strtolower($val) == 'fail'){
                $post = 'CANCELED';
            }else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            /*if($name=="firstname"){
                $datatables->where( $name, $operator, $post)->orwhere('lastname', $operator, $post);
            }else{*/
                $datatables->where( $name, $operator, $post);
            /*}*/
        }

        return $datatables->make(true);
        
    }
    public function transactionBookingsDetail(Request $request)
    {
        $ontime = $request->input('ontime');
        $makehtml = $this->displaySummaryofBooking($ontime);
        return $makehtml;
    }
    protected function displaySummaryofBooking($ontime)
    {
        $allBookings =  Booking::OnTimeBookingDetail($ontime)->get();
        $makehtml = '';
        if($allBookings){
            $prepare = collect($allBookings);
            $petsBook = $prepare->groupBy('petname');
            /*echo "<pre>";
            print_r($petsBook->all());*/
            
            foreach ($petsBook->all() as $pet_id => $value) {
                /*echo "<pre>";
                print_r($value[$i]->petId);*/
                //{{ url($setAction.'/pet', $pets->id) }}
                //<a href="providers/pet/'.$petID.'">'.$pet_id.'</a>
                //

                //$petID='providers/pet/'.$value[$i]->petId;
                 $petlink = '<a style="color:#01aef2;" href="'.url('pet').'/'.$value[0]->petId.'"><i class=" fa fa-eye"></i>'.$pet_id.'</a>';
                $makehtml .= '<div class="table-responsive"> <h3>'.$petlink.'</h3> <table class="table table-bordered table-striped table-hover"> ';
                $makehtml .= '<thead><tr><td>Technician</td><td>Service</td><td>Price</td><td>Start</td> </tr> </thead><tbody>';
                foreach ($value as $pet_key => $book_value) {
                    $makehtml .= '<tr> <td> '.$book_value['techname'].' </td> <td>'.$book_value['servicename'].'</td><td>'.$book_value['price'].'</td> <td>'.Carbon::parse($book_value['start'])->format('l jS F Y h:i a').'</td></tr>';
                }
                $makehtml .= '</tbody></table></div>';
                
            }

        }

        return $makehtml;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax())
        {   
            $transaction = Transaction::findOrFail($id);

            if($transaction){

                $result = $request->input('result');
                $transaction->result  = $result;
                $transaction->save();
                
                if($result == '1'){
                    $data['success'] = true;
                    $data['message'] = 'Payment Made Successfully';
                } else {
                     $data['success'] = false;
                    $data['message'] = 'Payment still Pending';
                }
            } else {
                $data['success'] = false;
                $data['message'] = 'Transaction Not found, please check again';
            }

            return $data;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
