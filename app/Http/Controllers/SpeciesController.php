<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Specie;
use Session;
use Datatables;
use DB;

class SpeciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $role = Role::where('provider_id',session('provider_id'))->paginate(15);
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('breeds.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables()
    {   
        $breeds = Specie::select(['id', 'name','name_ar','status']);
        
        $datatables =  app('datatables')->of($breeds)

            ->addColumn('action', function($breeds) {
                return view('breeds.action', compact('breeds'))->render();
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breed = \App\Breed::where('status', 1)->pluck('name', 'id');
        return view('breeds.create', compact('breed'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $species = Specie::create($request->all());

        Session::flash('flash_message', 'New Breed Added');

        return redirect('species');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $species = Specie::findOrFail($id);
        $breed = \App\Breed::where('status', 1)->pluck('name', 'id');
        return view('breeds.edit', compact('species','breed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $species = Specie::findOrFail($id);
        $species->update($request->all());

        Session::flash('flash_message', 'Specie updated!');

        return redirect('species');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->input('id');
        // $all_users = Role::with('users')->where('id', $ids)->get();
        foreach ($ids as $key) {
            
            $check_role = Role::has('users')->whereId($key)->get();                
        
        
            if ($check_role->count() == 0) {
                $role = Role::find($key);
                $role->perms()->sync([]);
                $role->delete();
                $deleteChecked = 1;
                $data['success'] = true;
                $data['message'] = 'Role deleted successfully';
            } else {
                $deleteChecked = 0;
                $data['success'] = false;
                $data['message'] = 'User still attached with this role, so can\'t be deleted';
            }
        }

        if ($request->ajax() )
        {    
            return $data;
        } else {
            Session::flash('flash_message', 'Role deleted!');
            return redirect('role');            
        }
    }
}
