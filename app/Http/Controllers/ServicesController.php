<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ServiceRequest;
use App\Http\Controllers\Controller;

use App\Service;
use Illuminate\Http\Request;
use Session;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $services = Service::with('breeds','sizes','categories')->paginate(25);
        // $services = Service::with('breed','size','category')->paginate(25);
        // return view('servicepro.services.index', compact('services'));
        if($request->ajax())
        {
            // $services = Service::with('breed','size','category');
            $services = Service::select(['services.id as sid','cat.name'])
                ->join('categories as cat', 'cat.id', '=', 'services.category_id')
                ->where('services.provider_id', session('provider_id'));

            $datatables =  app('datatables')->of($services)
                ->addColumn('action', function($services) {
                    return view('servicepro.services.action', compact('services'))->render();
                });
                // ->editColumn('status', '@if($status) Active @else Inactive @endif')
                // ->editColumn('name', function($ratings) {
                    // return $ratings->name;
                // });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = \App\Category::where('parentid', 0)->with('subcategory')->where('status',1)->get();
        $breed = \App\Breed::pluck('name','id');
        $size = \App\Size::where('status', 1)->pluck('name','id');
        $range = array_combine($range = range(15, 270, 15), $range);
        $technicians = \App\Technician::ActiveTechnicianList();
        return view('servicepro.services.create',compact('categories','breed','size','range','technicians'));
    }

    public function vue()
    {
        $categories = \App\Category::where('parentid', 0)->with('subcategory')->get();
        $breed = \App\Breed::pluck('name','id');
        $size = \App\Size::where('status', 1)->pluck('name','id');
        $range = array_combine($range = range(15, 270, 15), $range);
        $technicians = \App\Technician::ActiveTechnicianList();
        return view('servicepro.services.vue',compact('categories','breed','size','range','technicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        // dump($request->all());
        $provider_id = session('provider_id');
        $request['status'] = (isset($request->status))? 1 : 0;
        $serviceData = $request->only(['category_id','provider_id', 'status']);
        
        // $breed_id = $request->input('breed_id');
        // $size_id = $request->input('size_id');
        $price = $request->input('price');
        $servicetime = $request->input('servicetime');

//         $saljfd = \DB::table('services')
//         ->join('breed_service', function ($join) use ($breed_id, $size_id) {
//             $join->on('services.id', '=', 'breed_service.service_id')
//                  ->where('breed_service.breed_id','=',  $breed_id[0])
//                  ->where('breed_service.size_id','=', $size_id[0]);
//         })
//         ->where('provider_id', $provider_id)->first();
//         // \DB::enableQueryLog();
//     // dump(\DB::getQueryLog());
// dd(empty($saljfd));

        $service = Service::create($serviceData);
        $service_id = $service->id;
        $breedService_DATA = array();

        if($request->allSelected)
        {

            $breedData = \App\Breed::pluck('name','id');
            $sizeData = \App\Size::where('status', 1)->pluck('name','id');
            foreach ($breedData as $v => $bName) {

                foreach($sizeData as $s => $sName)
                {
                    
                    if($bName == 'Cat') {
                        if($sName == 'S' || $sName == 'L' || $sName == 'Any') {
                            $attachData['breed_id'] = $v;
                            $attachData['size_id'] = $s;
                            $attachData['service_id'] = $service_id;
                            $attachData['price']    = $price;
                            $attachData['servicetime'] = $servicetime;
                            $breedService_DATA[] = $attachData;       
                        }
                        
                    } else if($bName == 'Dog'){
                        $attachData['breed_id'] = $v;
                        $attachData['size_id'] = $s;
                        $attachData['service_id'] = $service_id;
                        $attachData['price']    = $price;
                        $attachData['servicetime'] = $servicetime;
                        $breedService_DATA[] = $attachData;    
                    }
                    
                }
            }
        }else {
            // dump($request->breed);
            $breedService_DATA = $request->breed;
            // dump($breedService_DATA);
            foreach ($breedService_DATA as $key => $value) {
                $breedService_DATA[$key]['service_id'] = $service_id;
            }
        }

         
        $service->technician()->attach($request->input('technicians'));

        $breed_service = \DB::table('breed_service')->insert($breedService_DATA);
        Session::flash('flash_message', 'Service added!');

        return redirect('providers/services');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::with('category','breed', 'technician')->findOrFail($id);
        $size = \App\Size::where('status',1)->pluck('name','id');
        $breeData = \App\Breed::where('status',1)->pluck('name','id');
        return view('servicepro.services.show', compact('service','size','breeData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::with('breed','technician')->findOrFail($id);
// dd($service->toArray());
        $categories = \App\Category::where('parentid', 0)->with('subcategory')->where('status',1)->get();
        $breed = \App\Breed::pluck('name','id');
        $size = \App\Size::pluck('name','id');
        $range = array_combine($range = range(15, 270, 15), $range);
        $technicians = \App\Technician::ActiveTechnicianList();
        $selected_techs = implode(',', array_pluck($service->technician, 'id'));
        return view('servicepro.services.edit', compact('service','categories','breed','size','range','technicians','selected_techs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\ServiceRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $request['status'] = (isset($request->status))? 1 : 0;
        $breedService_DATA = $request->breed;
        if(count($breedService_DATA) == 0){
            return redirect()->back()->withError('Breeds must be added');
        }
        foreach ($breedService_DATA as $key => $value) {
            $breedService_DATA[$key]['service_id'] = $id;
        }
         // dd($breedService_DATA);
        $service = Service::findOrFail($id);
        $service->update($request->all());
        #Delete Old Entries
        $deleteService = $service->breed()->detach();

        $service->technician()->sync($request->input('technicians'));
        $breed_service = \DB::table('breed_service')->insert($breedService_DATA);
        Session::flash('flash_message', 'Service updated!');

        return redirect('providers/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    // public function destroy($id)
    // {

    //     Service::destroy($id);

    //     Session::flash('flash_message', 'Service deleted!');

    //     return redirect('providers/services');
    // }

     public function destroy(Request $request, $id)
    {
        // Requests from the Datatables
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isCustomer = Service::destroy($ids);
            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Service deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Service::destroy($id);

        Session::flash('flash_message', 'Service deleted!');

         return redirect('providers/services');
    }

    public function checkService(Request $request)
    {
        $id = $request->technician;
        $provider_id = session('provider_id');
        $checkSer = Service::whereProviderId($provider_id)->whereCategoryId($id);
        if($request->service_id)
        {
            $checkSer->where('id', '<>', $request->service_id);
        }

        $checkSer = $checkSer->first();

        if($checkSer){
            $data['status'] = true;
            return $data;
        } else {
            $data['status'] = false;
            return $data;
        }
    }

    public function allServicesOfferedforSize(Request $request)
    {
        $breed_id   = $request->breed;
        $size_id    = $request->size;
        $provider_id = session('provider_id');

        // $bred_serv  = Service::where('service_id', $service_id)->where('breed_id', $breed_id)->where('size_id',$size_id)->get();
        $bred_serv  = Service::select('categories.parentid', 'categories.name', 'categories.name_er', 'services.id as service_id' , 'breed_service.breed_id', 'breed_service.price', 'breed_service.servicetime')
                        ->join('categories','categories.id', '=', 'services.category_id')
                        ->join('breed_service','breed_service.service_id', '=', 'services.id')
                        ->where('services.provider_id', '=', $provider_id)
                        ->where('breed_service.breed_id', '=', $breed_id)
                        ->where('breed_service.size_id', '=', $size_id)
                        ->where('services.status', '=', 1)->get();

        if(count($bred_serv) > 0)
        {
            $createHtml = '';
            foreach ($bred_serv as $key) {
                $tech_forServices = \DB::table('service_technician')->where('service_id', $key['service_id'])->pluck('technician_id');
                if(count($tech_forServices) > 0){
                    $tech = implode(',', $tech_forServices);
                    $createHtml .= '<div class="fc-event  ui-draggable ui-draggable-handle" data-serviceid='.$key['service_id'].' data-id='.$key['service_id'].' data-price='.$key['price'].' data-servicetime='.$key['servicetime'].' technician="'.$tech.'" data-parentid='.$key['parentid'].' duration='.$key['servicetime'].'>'.$key['name'].' </div>';
                }
                
            }
            $sData['html'] = $createHtml;
            $sData['success'] = true;
        } else {
            $sData['html'] = 'Pet of this Breed and Size doesn\'t have any service';
            $sData['success'] = false;
        }
        return $sData;
    }
    
    public function kennelservices(Request $request){
        if($request->ajax())
        {
            
            $services = Service::select(['services.id as sid','cat.name','is_kennel'])
                ->join('categories as cat', 'cat.id', '=', 'services.category_id')
                ->where('services.provider_id', session('provider_id'));

            $datatables =  app('datatables')->of($services)
                ->addColumn('action', function($services) {
                    return view('servicepro.services.kennelaction', compact('services'))->render();
                });
                // ->editColumn('status', '@if($status) Active @else Inactive @endif')
                // ->editColumn('name', function($ratings) {
                    // return $ratings->name;
                // });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.services.kennel');
    }
    
    public function markkennel(Request $request, $id)
    {
        // Requests from the Datatables
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $is_kennel=$request->input('is_kennel');
            $isCustomer = Service::Where('id',$ids)->update(['is_kennel'=>$is_kennel]);
            if($isCustomer)
            {
                $data['success'] = true;
                if($is_kennel!=0){
                    $data['message'] = 'Service is added as Kennel successfully';    
                }else{
                    $data['message'] = 'Service removed from Kennel successfully';    
                }
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }
    }

}
