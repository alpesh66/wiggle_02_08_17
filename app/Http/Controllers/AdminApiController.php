<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;

class AdminApiController extends Controller
{
    
    public function __construct(){
     
    }
    
    public function clientview()
    {
        return view('api.admin');
    }


    public function checkForValidation($validator)
    {
        $response['status']     = config('adminapi.Validation_ERROR');
        $response['validation'] = $validator->errors();
        $response['message']    = config('adminapi.Invalid');
        $response['message_ar'] = config('adminapi.Invalid_AR');
        return $response;
    }

    private function sendResponse($status,$message, $message_ar, $data = '')
    {
        $response['status'] = $status;
        $response['message'] = $message;
        $response['message_ar'] = $message_ar;
        if($data !='')
            $response['data'] = $data;

        return $response;
    }
    /**
     * [checkversion To check if new version is available or not]
     * @param  Request $request Takes deviceType and AlerNo to check new version
     * @return json array           return version details
     */
    public function checkversion(Request $request)
    {

        $device_type   = $request->input('device_type');
        $alert_no      = $request->input('alert_no');

        $validator = Validator::make($request->all(), [
            'device_type' => 'required|numeric|in:1,2',
            'alert_no' => 'required|numeric',
        ]);

        if ($validator->fails()) 
        {
            return $this->checkForValidation($validator);
        }

        $updateAvail    = false;
        $onlyOk         = false;
        $appVer         = 1;
        $iOS_appVer     = 1;
        $cancelAr       = 'فيما بعد';
        $cancelEn       = 'Later';
        $okAr           = 'تحديث';
        $okEn           = 'Update';

        $messageAr      = 'For best performance, please update your app to the latest version.';
        $messageEn      = 'For best performance, please update your app to the latest version.';
        $iOSLink        = 'http://apple.com';
        $androidLink    = 'https://play.google.com';
        
        if($alert_no <  $appVer && $device_type == '2')
        {
            $updateAvail = true;
            $respCategories['only_ok'] = $onlyOk;
            $respCategories['link'] = $androidLink;
            $respCategories['cancel_ar'] = $cancelAr;
            $respCategories['cancel_en'] = $cancelEn;
            $respCategories['ok_ar'] = $okAr;
            $respCategories['ok_en'] = $okEn;
            $respCategories['message_ar'] = $messageAr;
            $respCategories['message_en'] = $messageEn;
        }
        else if($alert_no < $iOS_appVer && $device_type == '1') // iPhone Alert
        {
            $updateAvail = true;
            $respCategories['only_ok'] = $onlyOk;
            $respCategories['link'] = $iOSLink;
            $respCategories['cancel_ar'] = $cancelAr;
            $respCategories['cancel_en'] = $cancelEn;
            $respCategories['ok_ar'] = $okAr;
            $respCategories['ok_en'] = $okEn;
            $respCategories['message_ar'] = $messageAr;
            $respCategories['message_en'] = $messageEn;         
        }

        // Update final response
        $respCategories['update_available'] = $updateAvail;
        
        // View code
        $response['status']     = config('adminapi.SUCCESS');
        $response['message']    = config('adminapi.Valid');
        $response['message_ar'] = config('adminapi.Valid_AR');
        $response['data']       = $respCategories;
        return $response;
    }

    /**
     * To send the User Token For Login
     * @param  Request $request Email and Password for Authentication
     * @return array           Valid response
     */
    public function getLogin(Request $request)
    {
        $email   = $request->input('email');
        $password      = $request->input('password');

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);

        $admin = \Auth::once($request->only(['email','password']));
        
        if($admin)
        {
            $user = \App\User::with('provider')->where(['email' => $email])->first();
            if($user->provider_id == 0)
            {
                // If this is not provider admin but super admin
                return $this->sendResponse(
                        config('adminapi.ERROR'),
                        config('adminapi.Invalid_User'),
                        config('adminapi.Invalid_User_AR')
                    );
            } elseif( $user->status == 0) {
                // If user is Not active Yet
                return $this->sendResponse(
                        config('adminapi.ERROR'),
                        config('adminapi.InActive_User'),
                        config('adminapi.InActive_User_AR')
                    );
            } else{
                // All Set, send the user Data
                return $this->sendResponse(
                        config('adminapi.SUCCESS'),
                        config('adminapi.Valid'),
                        config('adminapi.Valid_AR'),
                        $user
                    );
            }
        } else {
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid'),
                config('adminapi.Invalid_AR')
            );
        }
    }


    public function allBreedsData()   
    {
        $breed = \App\Breed::Active()->get();
        return $this->sendResponse(
                        config('adminapi.SUCCESS'),
                        config('adminapi.Valid'),
                        config('adminapi.Valid_AR'),
                        $breed
                    );
    }

    public function allCountryData()   
    {
        $breed = \App\Country::Active()->get();
        return $this->sendResponse(
                        config('adminapi.SUCCESS'),
                        config('adminapi.Valid'),
                        config('adminapi.Valid_AR'),
                        $breed
                    );
    }

    
    public function allTechniciansList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);

        $provider_id = $request->input('provider_id');
        $technicians = \App\Technician::ProviderTechnicians($provider_id)->ActiveTechs()->get();
        if($technicians->count() > 0)
        {
            return $this->sendResponse(
                    config('adminapi.SUCCESS'),
                    config('adminapi.Valid'),
                    config('adminapi.Valid_AR'),
                    $technicians
                );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.Invalid'),
                    config('adminapi.Invalid_AR')
                );
        }   
    }


    public function allCategories(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'provider_id' => 'required|numeric',
        // ]);

        // if ($validator->fails()) 
        //     return $this->checkForValidation($validator);

        $provider_id = \Auth::guard('api')->user()->provider_id;
        
        $service = \App\Service::with('category')->whereProviderId($provider_id)->Active()->get();
        if($service->count() > 0)
        {
            return $this->sendResponse(
                    config('adminapi.SUCCESS'),
                    config('adminapi.Valid'),
                    config('adminapi.Valid_AR'),
                    $service
                );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.Invalid'),
                    config('adminapi.Invalid_AR')
                );
        }   
    }

    public function allTechniServices(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'service_id' => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);

        $provider_id = $request->input('provider_id');
        $service_id = $request->input('service_id', false);
        $technician_id = $request->input('technician_id', false);
        
        $data = \App\Service::with('technician')->whereId($service_id)->first();

        if($data->count() > 0)
        {
            return $this->sendResponse(
                    config('adminapi.SUCCESS'),
                    config('adminapi.Valid'),
                    config('adminapi.Valid_AR'),
                    $data
                );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.Invalid'),
                    config('adminapi.Invalid_AR')
                );
        }   
    }

    /**
     * Get all services which are provided by this technician
     * @param  Request $request provider_id and technician_id
     * @return array           returns the service arrays
     */
    public function servicesForTechnicians(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'technician_id' => 'required|numeric',
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);

        $technician = $request->input('technician_id');
        $technicians = \App\Technician::with('service')->whereId($technician)->first();

        if(empty($technicians)) {
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.EmptyData'),
                    config('adminapi.EmptyData_AR')
                );
        }
        
        $fil_category = collect([]);

        foreach ($technicians->service as $services) {
            $parentid = $services->category->parentid;
            $parent_name = \App\Category::ParentName($parentid);

            $make_category['parent_id'] = $parentid;
            $make_category['id'] = $services->category->id;
            $make_category['name'] = $services->category->name;
            $make_category['parent'] = $parent_name['name'];

            $fil_category->push($make_category);
        }

        $lara_magic = $fil_category->groupBy('parent');
        if($lara_magic->count() > 0)
        {
            return $this->sendResponse(
                    config('adminapi.SUCCESS'),
                    config('adminapi.Valid'),
                    config('adminapi.Valid_AR'),
                    $data
                );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.Invalid'),
                    config('adminapi.Invalid_AR')
                );
        }   
    }

    public function searchCustomersData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider_id' => 'required|numeric',
            'customer' => 'required',
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);
        
        $name = $request->input('customer');
        $customer = \App\Customer::Search($name);
        if($customer->count() > 0)
        {
            return $this->sendResponse(
                    config('adminapi.SUCCESS'),
                    config('adminapi.Valid'),
                    config('adminapi.Valid_AR'),
                    $customer
                );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                    config('adminapi.ERROR'),
                    config('adminapi.Invalid'),
                    config('adminapi.Invalid_AR')
                );
        }   
    }


    public function newCustomerAdd(Request $request, \App\Customer $customer)
    {
        // return \Auth::guard('api')->user();
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required|max:100',
            'lastname'  => 'required|max:100',
            'email' => 'required|email|unique:customers',
            'mobile' => 'required|unique:customers'
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);
        
        $customer = $customer->createCustomer($request->all());
        if($customer->count() > 0)
        {
            return $this->sendResponse(
                config('adminapi.SUCCESS'),
                config('adminapi.Valid'),
                config('adminapi.Valid_AR'),
                $customer
            );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid'),
                config('adminapi.Invalid_AR')
            );
        }   
    }

    public function updateCustomerData(Request $request)
    {
        $customer_id = $request->input('customer_id');

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'firstname'  => 'required|max:100',
            'lastname'  => 'required|max:100',
            'email' => 'required|email|unique:customers,email,'.$customer_id,
            'mobile' => 'required|numeric|unique:customers,mobile,'.$customer_id
        ]);

        if ($validator->fails()) 
            return $this->checkForValidation($validator);

        $customer_id = $request->input('customer_id');

        $customer = \App\Customer::find($customer_id);

        if($customer)
        {
            $customer->update($request->all());

            return $this->sendResponse(
                config('adminapi.SUCCESS'),
                config('adminapi.Valid'),
                config('adminapi.Valid_AR'),
                $customer
            );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid'),
                config('adminapi.Invalid_AR')
            );
        }   

    }

    /**
     * Add new pet information for the customer
     * @param  Request $request following are the parameters
     * @return array           requested data
     */
    public function newPetAdd(Request $request)
    {
        // return \Auth::guard('api')->user();
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|numeric',
            'name'  => 'required|max:255',
            'breed'  => 'sometimes|required|in:1,2',
            'gender'  => 'sometimes|required|in:0,1',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);
        
        $customer_id = $request->input('customer_id');
        
        $customer = \App\Customer::find($customer_id);

        if($customer)
        {
            $pet = \App\Pet::create($request->all());
            $data = $customer->pet()->attach($pet);
            return $this->sendResponse(
                config('adminapi.SUCCESS'),
                config('adminapi.Valid'),
                config('adminapi.Valid_AR'),
                $pet
            );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid_User'),
                config('adminapi.Invalid_User_AR')
            );
        }   
    }

    /**
     * Book single appoinment for the Pet.
     * @param  \App\Http\Requests\BookingRequest $request [description]
     * @return [type]                                     [description]
     */
    public function bookmyAppointment(\App\Http\Requests\BookingRequest $request)
    {
        $request['provider_id'] = \Auth::guard('api')->user()->provider_id;
        $stardate = \Carbon\Carbon::parse($request->input('start'));
        $request['end']  = $stardate->addMinutes($request->input('end'));

        $book = \App\Booking::create($request->all());
        if($book)
        {
            return $this->sendResponse(
                config('adminapi.SUCCESS'),
                config('adminapi.Valid'),
                config('adminapi.Valid_AR'),
                $book
            );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid_User'),
                config('adminapi.Invalid_User_AR')
            );
        }   
    }

    /**
     * [editmyAppointment Update the booking details]
     * @param  \App\Http\Requests\BookingRequest $request [validation Check]
     * @return [array]       [status or request and booking data]
     */
    public function editmyAppointment(\App\Http\Requests\BookingRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'pet_id' => 'required',
            'technician_id' => 'required',
            'service_id' => 'required',
            'type_id' => 'required',
            'start' => 'required|date',
            'end' => 'required|date',
            'notes' => 'max:255',
            'id' => 'required'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $startDate = \Carbon\Carbon::parse($request->input('start'));
        $request['end']  = $startDate->addMinutes($request->input('end'));
        $request['start'] = $startDate;

        $book = \App\Booking::find($request->input('id'));

        if($book)
        {
            $book->update($request->all());
            return $this->sendResponse(
                config('adminapi.SUCCESS'),
                config('adminapi.Valid'),
                config('adminapi.Valid_AR'),
                $book
            );
        } else
        {
            // IF invalid provider_id passed then, empty
            return $this->sendResponse(
                config('adminapi.ERROR'),
                config('adminapi.Invalid_User'),
                config('adminapi.Invalid_User_AR')
            );
        }   
    }
}
