<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Customer;
use App\Booking;
use App\Pet;
use App\Content;
use App\Store;
use App\Provider;
use App\Technician;
use App\Rating;
use App\Kennel;
use App\Foster;
use App\Adopt;
use App\Volunteer;
use App\Surrender;
use Mail;
use Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use Session;
use DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

	$pets_count=db::table('pets')->where('breed','!=','0')->whereIN('pet_status', ['Draft','Available','Foster'])->count();
	
	$cat=db::table('pets')->where('breed','1')->whereIN('pet_status', ['Draft','Available','Foster'])->count();
	$dog=db::table('pets')->where('breed','2')->whereIN('pet_status', ['Draft','Available','Foster'])->count();
	
	$todate=carbon::today();
        // dd(\Auth::user()->provider_id);
	$todaybooking=DB::table('bookings')->where('created_at','>' ,carbon::today())->count();
		
	//print_r($todaybooking); die();
        $customers = Customer::count();
	$LastThirtyDay = DB::table('customers')->where('created_at', '>=', Carbon::now()->subDays(30))->count();

        //$bookings = Booking::count(); 
    $bookings = Booking::join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->select(['customers.id'])->get()->count();
	
	$previouss= DB::table('bookings')->join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')->where('bookings.end','<',Carbon::now())->select('bookings.start','bookings.end')->count();
	
    $upcomings= DB::table('bookings')->join('customers', 'bookings.customer_id', '=', 'customers.id')
                ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                ->join('services', 'bookings.service_id', '=', 'services.id')
                ->join('categories', 'services.category_id', '=', 'categories.id')
                ->where('bookings.end','>',Carbon::now())->select('bookings.start','bookings.end')->count();
	
	/*$booking_LastThirtyDays = DB::table('bookings')->where('created_at', '>=', Carbon::now()->subDays(30))->count();
	
	$booking_by_app=DB::table('bookings')->where('madefrom','0')->where('created_at', '>=', Carbon::now()->subDays(30))->count();
	$booking_by_web=DB::table('bookings')->where('madefrom','1')->where('created_at', '>=', Carbon::now()->subDays(30))->count();*/
        $last30 = DB::table('bookings')
                     ->select('ontime', DB::raw('count(*) as total'))
                     ->where('created_at', '>=', Carbon::now()->subDays(30))
                     ->groupBy('ontime')
                     ->get();
        $booking_LastThirtyDays=count($last30);
        $last30app = DB::table('bookings')
                     ->select('ontime', DB::raw('count(*) as total'))
                     ->where('madefrom','0')
                     ->where('created_at', '>=', Carbon::now()->subDays(30))
                     ->groupBy('ontime')
                     ->get();
        $booking_by_app=count($last30app);
        $last30web = DB::table('bookings')
                     ->select('ontime', DB::raw('count(*) as total'))
                     ->where('madefrom','1')
                     ->where('created_at', '>=', Carbon::now()->subDays(30))
                     ->groupBy('ontime')
                     ->get();
        $booking_by_web=count($last30web);
        $pets = Pet::count();
        $pet_stores = Store::count();
        $cmspages = Content::count();
        $providers = Provider::where('ptype', '1')->count();
        $store_list = Store::orderBy('id', 'desc')->take(5)->get();
        $customer_list = Customer::orderBy('id', 'desc')->take(5)->get();
        $provider_list = Provider::where('ptype', '1')->orderBy('id', 'desc')->take(5)->get();


        // Societies

        $societies = Provider::where('ptype', '2')->count();
        /*$adopt = \App\Adopt::count();*/
        /*$foster = \App\Foster::count();*/
        $volunteer = \App\Volunteer::count();
        $surrender = \App\Surrender::count();

        $totalKennel = Kennel::where('sender',1)->count();
        $approveKennel = Kennel::where('status', '1')->where('sender',1)->count();
        $rejectedKennel = Kennel::where('status', '2')->where('sender',1)->count(); 
        $pendingKennel = Kennel::where('status', '0')->where('sender',1)->count();        
        
        $adopt_list = \App\Adopt::with('customer')->take(5)->get();
        $volunteer_list = \App\Volunteer::with('customer')->take(5)->get();
        $surrender_list = \App\Surrender::with('customer')->take(5)->get();
        // dd($customer_list); exit;
        /*$adopt = Adopt::where('sender',1)->count();*/
        $adopt=0;


        $adoptDetail = Adopt::with('pet')->where('sender',1)->get();
        
        $adoptDog=0;
        $adoptCat=0;
        $adoptPending=0;
        $adoptRejected=0;
        $successAdopt=0;
        $adoptSucDog=0;
        $adoptSucCat=0;
        foreach ($adoptDetail as $key => $value) {
            
            if(isset($value->status) && $value->status==1){
  
                foreach ($value['pet'] as $key1 => $value1) {
                    $successAdopt++; 
                    if($value1['breed']==1){
                        $adoptSucCat++;    
                     
                    }else{
                         $adoptSucDog++;    

                    }
                }
            }
            
            foreach ($value['pet'] as $key1 => $value1) {
                if(isset($value->status) && $value->status==2){
                    $adoptRejected++;
                }
                if(isset($value->status) && $value->status==0){
                    $adoptPending++;   
                }
                $adopt++;
                if($value1['breed']==1){
                    $adoptCat++;    
                 
                }else{
                    $adoptDog++;    

                }
            }

        }

        $foster = Foster::where('sender',1)->count();
        $fosterApprove = Foster::where('sender',1)->where('status',1)->count();
        $fosterRejected = Foster::where('sender',1)->where('status',2)->count();
        $fosterPending = Foster::where('sender',1)->where('status',0)->count();

        $volunteer = Volunteer::where('sender',1)->count();
        $volunteerApprove = Volunteer::where('sender',1)->where('status',1)->count();
        $volunteerRejected = Volunteer::where('sender',1)->where('status',2)->count();
        $volunteerPending = Volunteer::where('sender',1)->where('status',0)->count();

        $surrender = Surrender::where('sender',1)->count();
        $surrenderApprove = Surrender::where('sender',1)->where('status',1)->count();
        $surrenderRejected = Surrender::where('sender',1)->where('status',2)->count();
        $surrenderPending = Surrender::where('sender',1)->where('status',0)->count();

        $pet = Pet::whereIN('pet_status', ['Draft','Available','Foster'])->where('breed','!=','0')->count();
        $petDraft = Pet::where('pet_status', 'Draft')->where('breed','!=','0')->count();
        $petAvailable = Pet::where('pet_status', 'Available')->where('breed','!=','0')->count();
        $petFoster = Pet::where('pet_status', 'Foster')->where('breed','!=','0')->count();

        $rankArray = array('Adopt'=>$adopt,'Foster'=>$foster,'Volunteer'=>$volunteer,'Surrender'=>$surrender);
        arsort($rankArray);

        $service=DB::table('bookings')  
            ->selectRaw('id,service_id, count(*) as count')
            ->orderBy('count','DESC')
            ->take(5)
                ->groupBy('service_id')
                ->get();
        foreach($service as $key => $val)
        {
            $catname=DB::table('services')
            ->where('services.id',$val->service_id)
            ->join('categories','services.category_id','=','categories.id')
            ->select('services.id','categories.name')
            ->get();
            if(!empty($catname)){
                $category[$key]['count']=$val->count;
                $category[$key]['name']=$catname[0]->name;              
            }
        }

        $rating_list = Rating::with('customer')->take(1)->get(); 

        return view('home', compact('customers', 'bookings', 'pets', 'pet_stores', 'cmspages', 'providers', 'store_list', 'customer_list', 'provider_list', 'societies', 'adopt', 'foster', 'volunteer', 'surrender', 'adopt_list', 'volunteer_list','previouss','upcomings','booking_LastThirtyDays','booking_by_app','todaybooking','booking_by_web', 'surrender_list','LastThirtyDay','pets_count','dog','cat','totalKennel','approveKennel','rejectedKennel','pendingKennel','adoptSucCat','adoptSucDog','successAdopt','adoptRejected','adoptPending','adoptCat','adoptDog','fosterApprove','fosterRejected','fosterPending','volunteerApprove','volunteerRejected','volunteerPending','surrenderApprove','surrenderRejected','surrenderPending','pet','petDraft','petAvailable','petFoster','rankArray','category','rating_list'));
    }

    public function providerindex() {
        return view('vendor.pro.vet');
    }

    public function moveToDashboard(Request $request) {
        
        $user_id = $request->input('user_id');
        $ptype = $request->input('ptype');
        $providerid = $request->input('providerid');
        $providerName = $request->input('provider_name');
        $userid=\Auth::user()->id;
        \Auth::logout();
        \Session::flush();
        \Auth::guard('web')->loginUsingId($user_id);
        $request->session()->put('user_type', true);
        $request->session()->put('user_id',$userid);
        $request->session()->put('provider_id', $providerid);  // Used at many places to get value stored
        $request->session()->put('superAdmin', true);
        $request->session()->put('nameProvider', $providerName);
        if ($ptype == 1) {
            $request->session()->put('provider_type', 'Provider');
            $data['success'] = 1;
            $data['url'] = url('providers/dashboard');
            return $data;
        } else if ($ptype == 2) {
            $request->session()->put('provider_type', 'Society');
            $data['success'] = 1;
            $data['url'] = url('societies/dashboard');
            return $data;
        } else {
            \Session::flush();
        }
    }

    public function moveBacktoAdmin(Request $request) {
        \Auth::logout();
        \Session::flush();
        \Auth::guard('web')->loginUsingId(1);
        $request->session()->put('user_type', true);
        $request->session()->put('provider_id', 0);  // Used at many places to get value stored
        $request->session()->put('provider_type', 'Admin');
        $data['success'] = 1;
        $data['url'] = url('dashboard');
        return $data;
    }

    /**
     * @author Nitinkumar vaghani
     * Created Date : 14-04-2017
     * 
     * @param Request $request
     * @return type
     */
    public function getUnreadRequestCount(Request $request) {
        // get unread request count
        if ($request->ajax()) {
            $provider_id = session('provider_id');
            $data['unread_foster'] = \App\Foster::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['unread_adopt'] = \App\Adopt::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['unread_volunteer'] = \App\Volunteer::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['unread_surrender'] = \App\Surrender::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['unread_kennel'] = \App\Kennel::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['unread_missyou'] = \App\MissYou::where('provider_id', $provider_id)->where('sender', 1)->where('is_read', 1)->count();
            $data['success'] = true;
            return json_encode($data);
        }
        return json_encode(['success' => false]);
    }

}
