<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    
    // $protected $guard = 'customers';

    public function index()
    {
        return view('api.index');
    }

    public function makeguzzle(Request $request)
    {
        $client = new Client(['base_uri' => 'http://wiggleapp.co/frontapi/']);

        // $url = 'v1/postTechnicianAvailability'; 
        $url = 'v1/postLogin';
        $headers = ['Content-Type' => 'application/json','Accept' => 'application/json', 'Authorization' => 'Bearer eTGTsbgkLNi63UnXm6ltdXj9BdInzfGCIZllHoBxu2D2YfPMR8mgcBUOZCSB'];
        $data['headers'] = $headers;
        $data['json'] = $request->all();
       //dd( json_encode($request->all()));
        $res = $client->request('POST', $url, $data);
        
        echo $res->getStatusCode();

        echo $res->getHeaderLine('content-type');

        echo( $res->getBody());
    }

    public function loginforme(Request $request)
    {
        echo ($request->input('para.name'));
        echo '<br/>';
        echo ($request->input('request'));
    }

    public function login(Request $request)
    {
        // dd($request);
        $email = $request->email;
        $password = $request->password;
        // $user = \Auth::guard('mobileapp')->user(); 
// dump($request->all());
        $user = auth()->guard('mobileapp')->attempt(['email' => 'asdfs', 'password' => 'asdfs']);
        return ($user);
    }

    public function store(Request $request)
    {
        $request['api_token']  = str_random(60);
        $request['password']  = $request->input('password');
        // return $request->all();
        return \App\Customer::create($request->all());
    }
}
