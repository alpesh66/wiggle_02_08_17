<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PetProviderRequest;
use App\Http\Requests\MedicalRequest;
use App\Http\Controllers\Controller;

use App\Pet;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Image;
use DB;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        // $pet = Pet::paginate(15);

        return redirect('dashboard');
        
        if ($request->ajax()){
            
            $pets = Pet::select(['id', 'name', 'dob','breed']);

            $datatables =  app('datatables')->of($pets)

                ->addColumn('action', function($pets) {
                    return view('pet.action', compact('pets'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('pet.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('pet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Pet::create($request->all());

        Session::flash('flash_message', 'Pet added!');

        return redirect('pet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show(Request $request, $id)
    {     
        $pet = Pet::with('breeds','sizes')->findOrFail($id);
        $customer_id = ($pet->customer->pluck('id')->all());
        $c_id = isset($customer_id[0] ) ? $customer_id[0] : '' ;
        return view('pet.show', compact('pet', 'c_id'));
    }

    public function datatablesmedicalCard($id)
    {
        $medical = \DB::table('medicalcards')
                    ->join('pets', 'pets.id', '=', 'medicalcards.pet_id')
                    ->join('vaccination_type', 'vaccination_type.id', '=', 'medicalcards.vaccination_type_id')
                    ->where('medicalcards.pet_id', $id)
                    ->select('medicalcards.id as gid', 'givenon', 'vet', 'institution', 'medicalcards.photo','vaccination_type.type');

        // dd($bookings); exit;            
        
        $datatables =  app('datatables')->of($medical)
                        ->editColumn('action', function($medical) {
                            return '<button class="btn btn-danger btn-xs deleteAjax" data-id="'.$medical->gid.'" data-model="users" title="Delete" name="deleteButton"><i class="fa fa-trash-o"></i></button>';
                        })
                        ->editColumn('photo', function($medical) {
                            return ($medical->photo == '')? '' : "<a target='_blank' href='".url('uploads/pets/medicalcard/'.$medical->photo)."' >Click</a>";
                        });
            
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $pet = Pet::findOrFail($id);
        $size = \App\Size::pluck('name','id');
        $breed = \App\Breed::pluck('name','id');
        $specie = \App\Specie::where('status', 1)->pluck('name','name');
        $frequency = groom_frequencyInSuperAdmin();
        return view('pet.edit', compact('pet', 'size', 'breed','frequency', 'specie'));
    }

    /**
     * Show the pets page for the Providers Page
     * @param  Request $request request
     * @param  [type]  $id      pets id
     * @return [type]           view
     */
    public function showProvider(Request $request, $id)
    {     
        $pet = Pet::with('breeds','sizes')->findOrFail($id);
        $customer_id = ($pet->customer->pluck('id')->all());
        $c_id = isset($customer_id[0] ) ? $customer_id[0] : '' ;
        return view('servicepro.pet.show', compact('pet', 'c_id'));
    }

    /**
     * Show medical Card Form for super Admin
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showPromedicalcard($id)
    {
        $pet = Pet::findOrFail($id);
        if($pet)
        {
            $vaccination = \App\VaccinationTypes::where('status',1)->where('breed',$pet->breed)->pluck('type', 'id');
            return view('pet.medicalcard', compact('pet','vaccination'));
        } else{
            return redirect()->back();
        }
    }

    /**
     * Show Medical Card Form For Providers Page
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showProvidermedicalcard($id)
    {
        $pet = Pet::findOrFail($id);
        if($pet)
        {

            return view('servicepro.pet.medicalcard', compact('pet'));
        } else{
            return redirect()->back();
        }
    }

    /**
     * Show Medical Card Form For Providers Page
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showSocietymedicalcard($id)
    {
        $pet = Pet::findOrFail($id);                
        if($pet)
        {
            $vaccination = \App\VaccinationTypes::where('status',1)->where('breed',$pet->breed)->pluck('type', 'id');
            return view('society.pet.medicalcard', compact('pet','vaccination'));
        } else{
            return redirect()->back();
        }
    }


    /**
     * Store the Medical Card From Super Admin
     * @param  [type]         $id      Pet ID
     * @param  MedicalRequest $request Array
     * @return [type]                  boolean
     */
    public function storePromedicalcard($id, MedicalRequest $request)
    {
        $data = ($request->all());
         if ($request->hasFile('photo') ) {
            
            $destinationPath = public_path('uploads/pets/medicalcard/');
            $photoname = date("YmdHis");

            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
           
            $data['photo'] = $photo_englist;
        }

        // $pet_path = public_path('uploads/pets/medicalcard/');
        // $img = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path.$photoname);

        $data['pet_id'] = $id;

        $medical = \App\Medicalcard::Create($data);
        /*--changes vaccination date in pet profile */
        $pet_id = $id;
        $petData = \DB::table('pets')->select('id','dob','breed')
                                ->where('id',$pet_id)
                                ->first();        
        $pet_dob =$petData->dob;
        $breed =$petData->breed;
        $grooming = 0;
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        /*--*/
        if($medical)
        {
            Session::flash('flash_message', 'Medical Card updated!');
            
        } else {
            Session::flash('flash_message', 'Something got wrong!');
        }
        return redirect('pet/'.$id);
    }

    /**
     * Store medical card from Provider Admin
     * @param  [type]         $id      [description]
     * @param  MedicalRequest $request [description]
     * @return [type]                  [description]
     */
    public function storeProvidermedicalcard($id, MedicalRequest $request)
    {
        $data = ($request->all());
         if ($request->hasFile('photo') ) {
            
            $destinationPath = public_path('uploads/pets/medicalcard/');
            $photoname = date("YmdHis");

            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
           
            $data['photo'] = $photo_englist;
        }

        // $pet_path = public_path('uploads/pets/medicalcard/');
        // $img = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path.$photoname);

        $data['pet_id'] = $id;

        $medical = \App\Medicalcard::Create($data);
        if($medical)
        {
            Session::flash('flash_message', 'Medical Card updated!');
            
        } else {
            Session::flash('flash_message', 'Something got wrong!');
        }
        return redirect('providers/pet/'.$id);
    }

    /**
     * Store medical card from Society Admin
     * @param  [type]         $id      [description]
     * @param  MedicalRequest $request [description]
     * @return [type]                  [description]
     */
    public function storeSocietymedicalcard($id, MedicalRequest $request)
    {
        $data = ($request->all());
        if ($request->hasFile('photo') ) {
            
            $destinationPath = public_path('uploads/pets/medicalcard/');
            $photoname = date("YmdHis");

            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
           
            $data['photo'] = $photo_englist;
        }

        // $pet_path = public_path('uploads/pets/medicalcard/');
        // $img = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path.$photoname);
        $data['pet_id'] = $id;
        $medical = \App\Medicalcard::Create($data);
        /*--changes vaccination date in pet profile */
        $pet_id = $id;
        $petData = \DB::table('pets')->select('id','dob','breed')
                                ->where('id',$pet_id)
                                ->first();        
        $pet_dob =$petData->dob;
        $breed =$petData->breed;
          $grooming = 0;
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);

       /* $current_date = date('Y-m-d');
        $next_vacci_date = "";
        $vaccination_type_id = $request->input('vaccination_type_id');

        $diff = strtotime($current_date, 0) - strtotime($pet_dob, 0);
        $weekdiff = floor($diff / 604800);  

        if($weekdiff <= 10){
             $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$breed)
                  ->where('vaccination_duration',8)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination_type_id)
                                ->orderBy('id','DESC')
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$breed)
                         ->where('vaccination_duration',10)
                         ->first();
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '8 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }                
            }
        }
        else if($weekdiff > 10 && $weekdiff <= 12){ 
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$breed)
                  ->where('vaccination_duration',10)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination_type_id)
                                ->orderBy('id','DESC')
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$breed)
                         ->where('vaccination_duration',12)
                         ->first();
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                
            }
        }
        else if($weekdiff > 12 && $weekdiff <= 14){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$breed)
                  ->where('vaccination_duration',12)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination_type_id)
                                ->orderBy('id','DESC')
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$breed)
                         ->where('vaccination_duration',48)
                         ->first();
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }                
            }

        }else if($weekdiff >= 15){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$breed)
                  ->where('vaccination_duration',48)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination_type_id)
                                ->orderBy('id','DESC')
                                ->first();

                if(isset($medical_count->givenon)){
                    $date = date_create($medical_count->givenon);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                else{
                    $date = date_create($current_date);
                    date_modify($date, '1 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
            }
        }
        if($next_vacci_date!=""){
            $vaccData['next_vaccination_date'] = $next_vacci_date;
            $vaccData['vaccination_type_id'] = $vaccination_type_id;           
            $r = Pet::where('id', $pet_id)->update($vaccData); 
        }*/
        /*--*/
        if($medical)
        {
            Session::flash('flash_message', 'Medical Card updated!');
            
        } else {
            Session::flash('flash_message', 'Something got wrong!');
        }
        return redirect('societies/pet');
    }



    public function editproviders($id)
    {
        $pet = Pet::findOrFail($id);

        $cust_data = ($pet->customer->pluck('id')->all());

        if($cust_data){
            $check_customer = \App\Booking::isCustomerofThisProvider($cust_data[0])->exists();

            if(!$check_customer){
            
            // Session::flash('flash_message', 'Invalid Customer');
                return redirect('providers/customers');
            }
        } else {
            return redirect('providers/customers');
        }
        
        $size = \App\Size::pluck('name','id');
        $breed = \App\Breed::pluck('name','id');
        $frequency = groom_frequencyInSuperAdmin();
        $specie = \App\Specie::where('status', 1)->pluck('name','name');
        return view('servicepro.pet.edit', compact('pet', 'size', 'breed','frequency','specie'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $pet = Pet::findOrFail($id);
        $data = $request->all();
        
        //$nextAppointmentDate = Carbon::parse($pet['last_groom_appointment'])->addWeeks($data['grooming']);

        
        if ($request->hasFile('photo') ) {
            
            $destinationPath = public_path('uploads/pets/');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
            
            $assets_path = url('uploads/pets/');
            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $img = Image::make($assets_path.'/'.$photo_englist)->resize(200,200)->save($thumb_path);
           
            $data['photo'] = $photo_englist;
        }
        
        $pet_dob = $request->input('dob');
        $pet_id = $id; 
        $breed = $request->input('breed');
        $grooming = $request->input('grooming');
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        //$data['next_groom_appointment'] = Carbon::parse($nextAppointmentDate)->format('Y-m-d');

        $pet->update($data);
        
        if($pet->customer) {
            $custo = $pet->customer->toArray();
            $custo = 'customers/'.$custo[0]['id'];
        } else {
            $custo = 'customers';
        }
        Session::flash('flash_message', 'Pet updated!');

        return redirect($custo);
    }


    public function updateproviders($id, Request $request)
    {
        
        $pet = Pet::findOrFail($id);
        $data = $request->all();

        $nextAppointmentDate = Carbon::parse($pet['last_groom_appointment'])->addWeeks($data['grooming']);
        if ($request->hasFile('photo') ) {
            
            $destinationPath = public_path('uploads/pets/');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
            
            $assets_path = url('uploads/pets/');
            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $img = Image::make($assets_path.'/'.$photo_englist)->resize(200,200)->save($thumb_path);
           
            $data['photo'] = $photo_englist;
        }
        $data['next_groom_appointment'] = Carbon::parse($nextAppointmentDate)->format('Y-m-d');
        $pet->update($data);
        
        if($pet->customer) {
            $custo = $pet->customer->toArray();
            $custo = 'providers/customers/'.$custo[0]['id'];
        } else {
            $custo = 'providers/customers';
        }
        Session::flash('flash_message', 'Pet updated!');

        return redirect($custo);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
      $ids = $request->id; 
      if(!empty($ids)){
          foreach ($ids as $key => $id) {
            Pet::destroy($id);
          }    
          return json_encode(array('status'=>'1','message'=>'Pet deleted!'));
      }else{
        Pet::destroy($id);
        Session::flash('flash_message', 'Pet deleted!');
        return redirect('pet');
      }           
    }

    public function destroyMedicalcard($id, Request $request)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isCustomer = \App\Medicalcard::destroy($id);

            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Medical Card deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        return redirect('pet');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function storeproviders(PetProviderRequest $request)
    {
        // if ($request->hasFile('photo') ) {
            
        //     $destinationPath = public_path('uploads/pets/');
        //     $photoname = date("YmdHis");
        //     $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
        //     $photo_englist = $photoname.$file_extention;
        //     $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
            
        //     $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;
        //     $new_filePath =  $destinationPath.'/'.$photo_englist;
        //     $img = Image::make($new_filePath)->resize(200,200)->save($thumb_path);

        //     $request['photo'] = $photo_englist;           
        // }

        $data['name'] = $request->petname;
        $data['dob'] = $request->petdob;
        $data['breed'] = $request->petbreed;
        $data['gender'] = $request->petgender;
        $data['size'] = $request->petsize;

        $pes = Pet::create($data);

        if($request->ajax()){
            if($pes){
                $customerID = $request->input('petaddCustomerID');
                $getCust = \App\Customer::find($customerID);
                if($getCust){
                    $getCust->pet()->attach($pes->id);
                    $data['success'] = true;
                    $data['message'] = "Pet has been successfully added";
                    $data['mypet'] = $pes;
                } else {
                    $data['success'] = false;
                    $data['message'] = "Pet Not attached, Try again";

                }
                
            } else {

                $data['success'] = false;
                $data['message'] = "Some internal error occurred, Please try again";
            }
            
            return $data;
        } else {
            Session::flash('flash_message', 'Pet added!');

            return redirect('societies/pet');
        }
        
        
    }



    public function indexsocieties(Request $request)
    {
        if ($request->ajax()){
            
            $provider_id = session('provider_id');
            $pets = Pet::where('provider_id', $provider_id);

            $datatables =  app('datatables')->of($pets)
                ->editColumn('gender', '@if($gender) Male @else Female @endif')
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                ->addColumn('action', function($pets) {
                    return view('society.pet.action', compact('pets'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        
        
        return view('society.pet.index', compact('pet'));

    }
    
    public function logsociety(Request $request){
        if ($request->ajax()){
            
            $provider_id = session('provider_id');
            //$pets = \App\Petlog::with('pet')->where('provider_id',$provider_id);
            
            $pets= \App\Petlog::
                 leftjoin('pets', 'pets.id', '=', 'pet_log.pet_id')
                 ->leftjoin('users', 'users.id', '=', 'pet_log.admin_id')
                 ->leftjoin('providers', 'providers.id', '=', 'pet_log.provider_id')
                 ->leftjoin('customers', 'customers.id', '=', 'pet_log.customer_id')
                 ->where('pet_log.provider_id',$provider_id)
                 ->select(['pets.id as id','users.name as admin',
                     'pets.name as pet', 
                     'customers.id as customerid',
                     'providers.id as providerid',
                     DB::raw('CONCAT(customers.firstname," ",customers.lastname)  as customer'),
                     'providers.name as provider',
                     'pets.gender','pets.breed','pet_log.status as status','pet_log.created_at as datetime'])
                 ->orderBy('pet_log.created_at','DESC');

            $datatables =  app('datatables')->of($pets)
                ->editColumn('gender', '@if($gender) Male @else Female @endif')
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                /*->addColumn('action', function($pets) {
                    return view('society.pet.logaction', compact('pets'))->render();
                })*/
                ;
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        
        return view('society.pet.petlog', compact('pet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function createsocieties()
    {
        $breed = \App\Breed::where('status',1)->pluck('name', 'id');
        $size = \App\Size::where('status',1)->pluck('name', 'id');
        $specie = \App\Specie::where('status', 1)->pluck('name','name');
        $frequency = groom_frequencyInSuperAdmin();
        $temperaments= \App\Temperaments::where('status',1)->pluck('name', 'id');
        return view('society.pet.create', compact('breed','size', 'specie', 'frequency','temperaments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function storesocieties(Request $request)
    {
        //dd(session()->all());
        $this->validate($request, [
            'temperament' => 'required',
            'name' => 'required',
            'dob' =>'required'
        ]);        
        $data=$request->except('temperament');        
        if ($request->hasFile('photo')) {
            $destinationPath = public_path('uploads/pets');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
            $assets_path = url('uploads/pets/');
            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;
            $img = Image::make($assets_path.'/'.$photo_englist)->resize(200,200)->save($thumb_path);
            $data['photo'] = $photo_englist;
        }
        $data['provider_id'] = session('provider_id');
        $data['pet_status']='Draft';
        $pet=Pet::create($data);
        $pet_id = $pet->id;
        $grooming = $request->input('grooming');
        $pet_dob = $request->input('dob');
        $breed = $request->input('breed');
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        /*---*/
        
        /*$current_date = date('Y-m-d');
        $next_vacci_date ="";
        $vaccination_id =0;
        $diff = strtotime($current_date, 0) - strtotime($pet_dob, 0);
        $weekdiff = floor($diff / 604800);  
        if($weekdiff <= 10){
             $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',8)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',10)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '8 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination_id =$vaccination->id;
                }                
            }
        }
        else if($weekdiff > 10 && $weekdiff <= 12){ 
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',10)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',12)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination_id =$vaccination->id;
                }
                
            }
        }
        else if($weekdiff > 12 && $weekdiff <= 14){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',12)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',48)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination_id =$vaccination->id;
                }                
            }

        }else if($weekdiff >= 15){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',48)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($medical_count->givenon);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                else{
                    $date = date_create($current_date);
                    date_modify($date, '1 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                $vaccination_id =$vaccination->id;
            }
        }
        if($next_vacci_date!=""){
            $vaccData['next_vaccination_date'] = $next_vacci_date;
            $vaccData['vaccination_type_id'] = $vaccination_id;           
        }
        $date = date_create($current_date);
        date_modify($date, $request->input('grooming'));
        $next_groom_date = date_format($date, 'Y-m-d');
        $vaccData['next_groom_appointment'] = $next_groom_date;
        $r = Pet::where('id', $pet_id)->update($vaccData);  */ 
        /*---*/
        $petLog=[
            'pet_id'=>$pet->id,
            'provider_id'=>session('provider_id'),
            'admin_id'=>  session('user_id'),
            'status'=>'Draft'
        ];
        \App\Petlog::create($petLog);
        $temparaments=$request->input('temperament');
        foreach($temparaments as $temparament){
            DB::table('pet_temperament')->insert(['pet_id'=>$pet->id,'temperament_id'=>$temparament]);
        }
        Session::flash('flash_message', 'Pet added!');
        return redirect('societies/pet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function showsocieties($id)
    {
        $pet = Pet::with('breeds','sizes')->findOrFail($id);
        $petlog=$pet->petlog()->get();
        $pet->temperament=$pet->getTemparaments();
        return view('society.pet.show', compact('pet','petlog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function editsocieties($id)
    {
        $pet = Pet::findOrFail($id);
        $breed = \App\Breed::where('status',1)->pluck('name', 'id');
        $size = \App\Size::where('status',1)->pluck('name', 'id');
        $specie = \App\Specie::where('status', 1)->pluck('name','name');
        $frequency = groom_frequencyInSuperAdmin();
        $temperament=$pet->getTemparaments();
        $temparaments=array();
        foreach($temperament as $temp){
            $temparaments[]=$temp->id;
        }
        $pet->temperament=$temparaments;
        $temperaments= \App\Temperaments::where('status',1)->pluck('name', 'id');
        return view('society.pet.edit', compact('pet', 'breed', 'size', 'specie','frequency','temperaments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function updatesocieties($id, Request $request)
    {
        $pet = Pet::findOrFail($id);
        $oldstatus=$pet->pet_status;
        $data=$request->except('temperament');
        if ($request->hasFile('photo')) {
            $destinationPath = public_path('uploads/pets');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);
            $assets_path = url('uploads/pets/');
            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;
            $img = Image::make($assets_path.'/'.$photo_englist)->resize(200,200)->save($thumb_path);
            $data['photo'] = $photo_englist;
        }
        $data['provider_id'] = session('provider_id');
        /*-----------*/
        
        $pet_dob = $request->input('dob');
        $pet_id = $id; 
        $breed = $request->input('breed');
        $grooming = $request->input('grooming');
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        /*$current_date = date('Y-m-d');
        $next_vacci_date ="";
        $vaccination_id =0;
        $diff = strtotime($current_date, 0) - strtotime($pet_dob, 0);
        $weekdiff = floor($diff / 604800);  
        if( $weekdiff <= 10){
             $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',8)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){ 
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',10)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '8 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination_id =$vaccination->id;
                }
                
                
            }
        }
        else if($weekdiff > 10 && $weekdiff <= 12){ 
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',10)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){

                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',12)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '10 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                $vaccination_id =$vaccination->id;
            }
        }
        else if($weekdiff > 12 && $weekdiff <= 14){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',12)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($pet_dob);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');
                    $vaccination = \DB::table('vaccination_type')->select('id','type')
                         ->where('breed',$request->input('breed'))
                         ->where('vaccination_duration',48)
                         ->first();
                    $vaccination_id =$vaccination->id;
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '12 weeks');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                $vaccination_id =$vaccination->id;
            }

        }else if($weekdiff >= 15){
            $vaccination = \DB::table('vaccination_type')->select('id','type')
                  ->where('breed',$request->input('breed'))
                  ->where('vaccination_duration',48)
                  ->first();
            if(isset($vaccination->id)){
                $medical_count = \DB::table('medicalcards')->select('id','givenon')
                                ->where('pet_id',$pet_id)
                                ->where('vaccination_type_id',$vaccination->id)
                                ->first();
                if(isset($medical_count->givenon)){
                    $date = date_create($medical_count->givenon);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                else{
                    $date = date_create($pet_dob);
                    date_modify($date, '1 years');
                    $next_vacci_date = date_format($date, 'Y-m-d');    
                }
                $vaccination_id =$vaccination->id;
            }
        }        
        if($next_vacci_date!=""){
            $data['next_vaccination_date'] = $next_vacci_date;
            $data['vaccination_type_id'] = $vaccination_id;
            $date = date_create($request->input('dob'));
            date_modify($date, $request->input('grooming'));
            $next_groom_date = date_format($date, 'Y-m-d');
            $data['next_groom_appointment'] = $next_groom_date;
        }*/
        /*----------*/
        $pet->update($data);
        if($oldstatus!=$pet->pet_status){
           $petLog=[
                'pet_id'=>$pet->id,
                'provider_id'=>session('provider_id'),
                'status'=>$pet->pet_status,
                'admin_id'=>  session('user_id'),
                'customer_id'=>0
            ];
            \App\Petlog::create($petLog);
             
        }
        $temparaments=$request->input('temperament');
        DB::table('pet_temperament')->where('pet_id',$pet->id)->delete();
        foreach($temparaments as $temparament){
            DB::table('pet_temperament')->insert(['pet_id'=>$pet->id,'temperament_id'=>$temparament]);
        }
        Session::flash('flash_message', 'Pet updated!');
        return redirect('societies/pet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroysocieties($id)
    {
        Pet::destroy($id);
        Session::flash('flash_message', 'Pet deleted!');

        return redirect('societies/pet');
    }
    /*Crated by :Alpesh Bhimani
      Date:17/10/2017
      Desc:Show deactive pet for an admin
      */
     public function indexDeactive(Request $request)
    {
        if ($request->ajax()){
            
            $pets = Pet::
              select(['pets.*','customers.id as customer_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS customer_name')])
              ->rightjoin('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
              ->rightjoin('customers', 'customer_pet.customer_id', '=', 'customers.id')
              ->where('pets.status', 0)
              ->orderBy('pets.updated_at','DESC')
              ->groupBy('pets.id');

            $datatables =  app('datatables')->of($pets)
                ->editColumn('customer_name', function($pets) {
                    return "<a style='font-weight: bold;' href='".url('customers/'.$pets->customer_id)."' ><i class='fa fa-eye'></i>".$pets->customer_name."</a>";
                })
                ->editColumn('name', function($pets) {
                    return "<a style='font-weight: bold;' href='".url('pet/'.$pets->id)."' ><i class='fa fa-eye'></i>".$pets->name."</a>";
                })
                ->editColumn('updated_at', function($pets) {
                    return Carbon::parse($pets->updated_at)->format('d-m-Y');;
                })
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                ;
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $post_origin       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'breed')
            {
                $val = $post_origin;
                if(strtolower($val) == 'dog'){
                    $post = '2';
                } else {
                    $post = '1';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('pet.index_deactive');        
    }
}
