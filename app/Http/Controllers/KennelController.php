<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Kennel;
use App\Booking;
use App\Pet;
use App\Customer;
use App\Provider;
use App\KennelPetServices;
use App\Inbox;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Http\Requests\KennelRequest;
use Illuminate\Support\Facades\Config;
use Mail;

class KennelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */    
    public function VaccinationSchedule(Request $request)    
    { 
      echo $current_date = Carbon::now()->addHour(4)->format('Y-m-d H:i:00');
      exit();
      $bookings = Booking::leftjoin('customers', 'bookings.customer_id', '=', 'customers.id')
            ->leftjoin('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->leftjoin('pets', 'bookings.pet_id', '=', 'pets.id')
            ->leftjoin('providers', 'bookings.provider_id', '=', 'providers.id')
            ->leftjoin('services', 'bookings.service_id', '=', 'services.id')
            ->leftjoin('categories', 'services.category_id', '=', 'categories.id')
            ->where('bookings.start', '2017-09-13 14:00:00')
            ->where('bookings.status', 0)
            ->select(['customers.firstname','customers.middlename','customers.lastname','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename','categories.image as category_image', 'bookings.start', 'bookings.type_id', 'bookings.status','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime','bookings.id', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes', 'customers.id as customer_id', 'services.id as service_id'])
            ->orderby('bookings.start', 'desc')
            ->get();
      foreach ($bookings as $key => $booking) {

        $message = "Dear $booking->firstname,<br/>We would like to remind you of your appointment with $booking->provider_name for ";
        $date = $booking[$key]['start'];
        $good_date = Carbon::parse($date)->format('l, M jS Y h:i a');
        $inbox['provider_id'] = $booking->provider_id;
        $inbox['customer_id'] = $booking->customer_id;
        $inbox['service_id'] = $booking->service_id;
        $inbox['booking_id'] = $booking->booking_id;
        $inbox['bookdate'] = Carbon::parse($booking->start);
        $inbox['isnew'] = 1;
        $inbox['hadread'] = 1;
        $inbox['type'] = 1;  //Reminder Message
        $inbox['subject'] = push_messages('appointment_remider.en.title');
        $email_m = "";
        $message .= " $booking->petname: "."$booking->servicename";
        $email_m .= " $booking->petname: "."$booking->servicename";
        $message .= " at {$good_date}.";
        $email_m .= " at {$good_date}.";
        $inbox['message']=$message;
        $image_url = 'images/category.png';
        if (!empty($booking->category_image)) {
            if (file_exists('uploads/categories/' . $booking->category_image)) {
                $image_url = 'uploads/categories/' . $booking->category_image;
            }
        }
        $emailHtml[$booking->petname][0] = array(
            'category_image' => $image_url,
            'servicename' => $booking->servicename
        );

        $message_obj = Inbox::Create($inbox);         
        $message_id = $message_obj->id;
        $pushMessage_en=push_messages('appointment_remider.en.message');
        $pushMessage_ar=push_messages('appointment_remider.ar.message');
        $extraData=array('type'=>'APPOINTMENT','reminder'=>TRUE);
        $email = array($booking->email);
        //$email = array('alpesh66@yopmail.com');
        $bcc_email = array();
        $bcc_email = env('SIKW_BCC_COPY', false);

        $data['order_details'] = $emailHtml;
        $data['customer_name'] = $booking->firstname;
        $data['provider_name'] = $booking->provider_name;
        $data['booking_date'] = Carbon::parse($booking->start)->format('F d Y');
        $data['booking_time'] = Carbon::parse($booking->start)->format('h:i a');


        $result = Mail::send('emails.bookingEmailtoCustomerReminder', $data, function ($message) use($email, $bcc_email) {
            $message->to($email);
            $message->subject('Appointment Reminder');
            if ($bcc_email) {
              $message->bcc($bcc_email);
            }
        });
        sendPushNoti($pushMessage_en,$pushMessage_ar,$booking->customer_id,$message_id);
        file_put_contents($this->logFile, "\n\n ********* Success Reminder of Appointment ************ \n" . json_encode($message) . "\n\n", FILE_APPEND | LOCK_EX);
      }
    }
    public function index(Request $request)
    {        
      
        /*  Created by:Alpesh Patel
            Date:18-7-17
        */                        
        /*echo "<pre>";
        print_r(session()->all());
        exit();*/
        if($request->ajax())
        {
          
            $customer = Kennel::select('kennels.*','customers.firstname', 'customers.lastname','kennel_pet.pet_id','kennel_pet.pet_id')
                ->leftjoin('customers', 'customers.id', '=', 'kennels.customer_id')
                ->leftjoin('kennel_pet', 'kennel_pet.kennel_id', '=', 'kennels.id')
                ->join('pets', 'kennel_pet.pet_id', '=', 'pets.id')
                ->where('kennels.provider_id',Session::get('provider_id'))
                ->where('sender' , 1)                
                /*->where('kennels.sender', 1)*/
                /*->orderBy('kennels.updated_at','DESC')*/
                ->groupBy('kennels.id');

            $datatables =  app('datatables')->of($customer)
                
                ->editColumn('status', '@if($status == 1) Approved @elseif($status == 2) Reject @else Pending  @endif')
                ->editColumn('customer_name', function($customer) {
                  if(strpos(\Request::path(),'societies/kennels') !==FALSE)
                    $setActionUser = 'societies/customers'; 
                  else
                    $setActionUser = 'providers/customers'; 

                    return "<u><a style='' href=".url($setActionUser.'/'.$customer->customer_id)." >".$customer->firstname.' '.$customer->lastname."</a></u>";
                })
                ->editColumn('created_at', function($customer) {
                    $created_at = date_format($customer->created_at, 'Y-m-d H:i:s');
                    return $created_at;
                })
                ->editColumn('updated_at', function($customer) {
                  $reply_date = ($customer->parent_id == 0) ? NULL : $customer->updated_at;
                  if($reply_date!=NULL)
                    $reply_date = date_format($reply_date, 'Y-m-d H:i:s');
                    return $reply_date;
                })

                ->editColumn('description', function($customer) {
                  return $customer->description;
                })
                ->editColumn('services', function($customer) {
                  $services_ids = \DB::table('kennel_pet_services')->select('service_id')
                      ->where('kennel_id',$customer->id)
                      ->get();
                  $services_ids = json_decode(json_encode($services_ids), true);
                  $ids=array();
                  foreach ($services_ids as $key => $value) {
                      $ids[] = $value['service_id'];
                  }
                  $catname=\DB::table('services')
                  ->whereIn('services.id', $ids)
                  ->join('categories','services.category_id','=','categories.id')
                  ->select('services.id','categories.name')
                  ->get();

                  $data ="<ul>";
                  foreach ($catname as $key => $value) {
                    $data .= "<li>".$value->name."</li>";
                  }
                  $data .="</ul>";
                  return $data;
                })
                ->addColumn('pet', function($customer) {
                    if(isset($customer->pet_id))
                    {
                        $pet = Pet::where('id',$customer->pet_id)->get();
                        $pet_detail = display_pet_name_return($pet);
                        return $pet_detail;
                    }
                    else{
                     return '---';   
                    }
                })
                ->addColumn('petType', function($customer) { 
                    if(isset($customer->pet_id) && $customer->pet_id!='0'){
                        $pet = Pet::where('id',$customer->pet_id)->select('breed')->first();                
                        if(isset($pet->breed) && $pet->breed!=0){
                            $breed = get_breeds_details($pet->breed);
                            if(isset($breed->name))
                                return $breed->name;    
                            else
                                return "---";    
                        }
                        else
                        { return "---"; }                        
                    }
                    else{
                        return "---";
                    }                     
                })
                ->addColumn('action', function($customer) {
                    if(strpos(\Request::path(),'societies/kennels') !==FALSE)
                      $setAction = 'societies/kennels'; 
                    else
                      $setAction = 'providers/kennels'; 

                    $buttons = '<a href="'.url($setAction.'/'.$customer->id).'" class="btn btn-success btn-xs" title="View Kennel"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>';
                    if($customer->parent_id == 0){
                        $buttons .= '<button type="button" class="btn btn-primary btn-xs replyClick replyStatus'.$customer->id.'" title="'.$customer->title.'" ids="'.$customer->id.'" customer_id="'.$customer->customer_id.'" data-toggle="modal"  data-target="#myModal" id="replyStatus'.$customer->customer_id.'" id="replyStatus" data-id="'.$customer->customer_id.'" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/></button>';

                    }
                    $buttons .= '<form method="POST" action="'.url($setAction.'/'.$customer->id).'" accept-charset="UTF-8" style="display:inline"><input name="_method" value="DELETE" type="hidden"><input name="_token" value="'.csrf_token().'" type="hidden"><button type="submit" class="btn btn-danger btn-xs" title="Delete Kennel" onclick="return confirm(&quot;Are you sure you want to delete?&quot;)"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Kennel"></span></button></form>';
                    return $buttons;
                })
                ->setRowClass(function ($customer) {
                    if($customer->is_read == 1 && $customer->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });


            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'approved'){
                    $post = '1';
                } elseif(strtolower($val) == 'reject'){
                    $post = '2';
                }else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }
            return $datatables->make(true);
            return view('servicepro.kennels.index');
        }
        else{
            $kennels = Kennel::with('pet')->allMessage()->paginate(15);
            return view('servicepro.kennels.index', compact('kennels'));
        }
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('servicepro/kennels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Kennel::create($request->all());

        Session::flash('flash_message', 'Kennel added!');

        return redirect('providers/kennel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $kennel = Kennel::findOrFail($id);
        $kennel->is_read = "0";
        $kennel->save();
        $services_ids = \DB::table('kennel_pet_services')->select('service_id')
            ->where('kennel_id',$id)
            ->get();
        $services_ids = json_decode(json_encode($services_ids), true);
        $ids=array();
        foreach ($services_ids as $key => $value) {
            $ids[] = $value['service_id'];
        }
        $catname=\DB::table('services')
        ->whereIn('services.id', $ids)
        ->join('categories','services.category_id','=','categories.id')
        ->select('services.id','categories.name')
        ->get();
        return view('/servicepro/kennels.show', compact('kennel','catname'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        /*$kennel = Kennel::findOrFail($id);
        return view('servicepro/kennels.edit', compact('kennel'));*/
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $kennelUpdate = Kennel::findOrFail($id);
        $kennelUpdate->update($request->all());

        Session::flash('flash_message', 'Kennel updated!');

        return redirect('providers/kennels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isCustomer = Kennel::destroy($ids);
            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Kennel deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }
        Kennel::destroy($id);

        Session::flash('flash_message', 'Kennel deleted!');
        
        if (strpos(\Request::path(), 'societies/kennels') !== FALSE) {
            return redirect('societies/kennels');
        }else{
            return redirect('providers/kennels');
        }
    }

  public function reply(KennelRequest $request){    
   
    $customer = Kennel::select('kennels.*', 'customers.*', 'kennel_pet.pet_id')
                   ->leftjoin('customers', 'customers.id', '=', 'kennels.customer_id')
                   ->leftjoin('kennel_pet', 'kennel_pet.kennel_id', '=', 'kennels.id')
                   /*->where('provider_id',Session::get('provider_id'))*/
                   ->where('kennels.id',$request->input('id'))
                   ->get()->toArray();
    $provider_id = Session::get('provider_id');
    $customer_id = $request['customer_id'];
    $status = $request->input('status');
    $description = $request->input('description');
    $petDetails = array();
      
    $provider = Provider::select('name')->where('id',$provider_id)->first();
    $provider_name = isset($provider->name) ? $provider->name: "";
    
    $kennelUpdate = Kennel::findOrFail($request->input('id'));
    /*$request['parent_id'] = $request->input('id');
    $request['is_read'] = 0;*/
    $kennelUpdate->update(['parent_id' =>$request->input('id'),'status' =>$status,'is_read'=>0]);
    $startdate="";
    $enddate="";
    $bookingId=0;
    if($status==1){      
      foreach ($customer as $key => $value) {
        $pet_detail = Pet::select('name')->where('id', $value['pet_id'])->first(); 
        $startdate=$value['startdate'];
        $enddate=$value['enddate'];
              
          $bookData['provider_id'] = $provider_id;
          $r = Pet::where('id', $value['pet_id'])->update($bookData);                 
          
          $service_name = "";
          $parentid = 0;
          $service_id = 0;
          $services = KennelPetServices::select('service_id')
                      ->where('kennel_id',$request->input('id'))
                      ->where('pet_id',$value['pet_id'])
                      ->get()->toArray();
                      if(!empty($services)){
                        foreach ($services as $key1 => $value1) {                        
                          $serviceNames = \App\Category::select('categories.name','categories.name_er','categories.image','categories.image_ar','parentid')
                              ->join('services', 'services.category_id', '=', 'categories.id')
                              ->where('services.id', $value1['service_id'])
                              ->get()->toArray();  
                          $image_url = 'images/category.png';
                          if (!empty($serviceNames[0]['image'])) {
                              if (file_exists('uploads/categories/' . $serviceNames[0]['image'])) {
                                  $image_url = 'uploads/categories/' . $serviceNames[0]['image'];
                              }
                          }

                          $emailHtml[$pet_detail->name][$key1] = array(
                              'category_image' => $image_url,
                              'servicename' => $serviceNames[0]['name']
                          );                         
                          $parentid = $serviceNames[0]['parentid'];
                          $service_id = $value1['service_id'];
                        }  
                      }else{
                         $emailHtml[$pet_detail->name][$key] = array(
                              'category_image' => "",
                              'servicename' => ""
                          ); 
                      }
                      
          $book = Booking::create();
          $book->provider_id = $provider_id;
          $book->start = $value['startdate'];
          $book->end = $value['enddate'];
          $book->pet_id = $value['pet_id'];
          $book->customer_id = $customer_id;
          $book->type_id = $parentid;
          $book->service_id = $service_id;
          $book->save();
          $bookingId = $book->id;
          //for an email
        //}        
      }
      
      $request['provider_id'] = $provider_id;
      $request['parent_id'] = $request->input('id');
      $request['startdate'] = $customer[0]['startdate'];
      $request['enddate'] = $customer[0]['enddate'];
      $request['sender'] = 2;
      Kennel::create($request->all());

    }

    $message_id=1;
    $email = array($customer[0]['email']);
    //$email = array('alpesh66@yopmail.com');
    //$bcc_email = array();
    $bcc_email = env('SIKW_BCC_COPY', false);

    //send push notification to user            
    if($status==0){
      //sendPushNoti(push_messages('pending_kennel.en.message'), push_messages('pending_kennel.ar.message'), $customer_id, $message_id);       
    }elseif($status==1){
      $data = array('customer_name' => ucfirst($customer[0]['firstname'] . ' ' . $customer[0]['lastname']), 'provider_name' => $provider_name,'startdate'=>$startdate,'enddate'=>$enddate,'petDetails' => $emailHtml,'description'=>$description);
      $result = Mail::send('emails.Kennel_Confirmation', $data, function ($message) use($email, $bcc_email) {
          $message->to($email);
          $message->subject("Kennel Request Confirmation");
          if ($bcc_email) {
            $message->bcc($bcc_email);
          }
      });
      $startdate = Carbon::parse($startdate)->format('l, jS M Y h:i a');
      $enddate = Carbon::parse($enddate)->format('l, jS M Y h:i a');
      foreach ($emailHtml as $pet => $details) {
        $pet_name = $pet;
      }

      $reminder['message'] = "Dear ".$customer[0]['firstname'].",<br/>Your kennel request has been approved by ".$provider_name." for ".$pet_name." from ".$startdate." to ".$enddate.".";
      $reminder['provider_id'] = $provider_id;
      $reminder['service_id'] = 0;
      $reminder['booking_id'] = $bookingId;
      $reminder['isnew'] = 0;
      $reminder['hadread'] = 1;
      $reminder['type'] = 0;
      $reminder['customer_id'] = $customer_id;
      $reminder['sender'] = 1;
      $reminder['subject'] = push_messages('approved_kennel.en.title');
      $message = Inbox::Create($reminder);         
      $message_id = $message->id;

      sendPushNoti(push_messages('approved_kennel.en.message'), push_messages('approved_kennel.ar.message'),$customer_id, $message_id);
    }elseif($status==2){
      foreach ($customer as $key => $value) {
        $pet_detail = Pet::select('name')->where('id', $value['pet_id'])->first();
        $pet_name = $pet_detail->name;
      }
      $startdate = Carbon::parse($startdate)->format('l, jS M Y h:i a');
      $enddate = Carbon::parse($enddate)->format('l, jS M Y h:i a');

      $data = array('customer_name' => ucfirst($customer[0]['firstname'] . ' ' . $customer[0]['lastname']), 'provider_name' => $provider_name,'reject_reason' => $description);
      $result = Mail::send('emails.Kennel_Rejected', $data, function ($message) use($email, $bcc_email) {
          $message->to($email);
          $message->subject("Kennel Request Rejected");
          if ($bcc_email) {
            $message->bcc($bcc_email);
          }
      });
      $reminder['message'] = "Dear ".$customer[0]['firstname'].",<br/>Your kennel request has been rejected by ".$provider_name." for ".$pet_name." from ".$startdate." to ".$enddate.".";
      $reminder['provider_id'] = $provider_id;
      $reminder['service_id'] = 0;
      $reminder['booking_id'] = 0;
      $reminder['isnew'] = 0;
      $reminder['hadread'] = 1;
      $reminder['type'] = 2;
      $reminder['customer_id'] = $customer_id;
      $reminder['sender'] = 1;
      $reminder['subject'] = push_messages('reject_kennel.en.title');
      $message = Inbox::Create($reminder);         
      $message_id = $message->id;
      
      sendPushNoti(push_messages('reject_kennel.en.message'), push_messages('reject_kennel.ar.message'),$customer_id, $message_id);
    }
    return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
  }
}
