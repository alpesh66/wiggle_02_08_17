<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';


    // protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'api_token' => str_random(60),
            'password' => bcrypt($data['password']),
            'status' => 1
        ]);
    }

    /**
    * protected method to redirect based on auth role
    */
    protected function authenticated($request,$user)
    {
        $auth_provider_types = '';
        $authUser = ($request->user()->toArray());
        $pro_id = $authUser['provider_id'];
        $provider_type = 'Admin';
        if(!empty($pro_id) && $pro_id > 0){
            $provider_data = \App\Provider::where('id', $pro_id)->first();
            $provider_type = $provider_data['ptype'];
        }
        
        $user_type  =  $authUser['utype'];
        $request->session()->put('user_id',$user->id);
        $request->session()->put('user_type', $user_type);
        $request->session()->put('provider_id', $authUser['provider_id']);  // Used at many places to get value stored
        $request->session()->put('provider_type', $provider_type);

        if(\Auth::user()->can(super_admin_roles()) ) {
            return redirect('/dashboard');
        } else if (\Auth::user()->can(provider_admin_roles()) ) {
            return redirect('/providers/dashboard');
        } else if (\Auth::user()->can(society_admin_roles()) ) {
            return redirect('/societies/dashboard');
        } else if (\Auth::user()->can('calendar-only') ) {
            return redirect('/tech/calendar');
        } else {
            return redirect()->back()->withErrors(['email', 'These credentials do not match our records.']);
        }
    }

   
}
