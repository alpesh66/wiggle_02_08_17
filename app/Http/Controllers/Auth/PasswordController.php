<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        if (!($request->ajax() || $request->wantsJson()) ) {
            
            $this->validateSendResetLinkEmail($request);
        }
        else 
        {
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($validator->fails()) 
            {
                $response['status']     = config('adminapi.Validation_ERROR');
                $response['validation'] = $validator->errors();
                $response['message']    = config('adminapi.Invalid');
                $response['message_ar'] = config('adminapi.Invalid_AR');
                return $response;
            }
        }

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        // added Custom Rule for API call
        if ($request->ajax() || $request->wantsJson()) {
            if($response == Password::RESET_LINK_SENT)
            {
                return array('status' => config('adminapi.SUCCESS'), 
                    'message' => config('adminapi.PasswordReset') , 
                    'message_ar' => config('adminapi.PasswordReset_AR') );
            }    
            else
            {
                return array('status' => config('adminapi.ERROR'), 
                    'message' => config('adminapi.PasswordResetError') , 
                    'message_ar' => config('adminapi.PasswordResetError_AR'));
            }
        }

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }
}
