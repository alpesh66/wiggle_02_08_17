<?php

namespace App\Http\Controllers\Customerauth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $linkRequestView = 'auth.passwords.customeremail';
    protected $resetView = 'auth.passwords.customerreset';
    protected $guard = 'customers';
    protected $redirectPath = 'customers/password/reset';
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \Config::set("auth.defaults.passwords","customers");
    }

    public function sendResetLinkEmail(Request $request)
    {
        if (!($request->ajax() || $request->wantsJson()) ) {
            
            $this->validateSendResetLinkEmail($request);
        }
        else 
        {
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($validator->fails()) 
            {
                $response['status']     = config('frontapi.Validation_ERROR');
                $response['validation'] = $validator->errors();
                $response['message']    = config('frontapi.Invalid');
                $response['message_ar'] = config('frontapi.Invalid_AR');
                return $response;
            }
        }

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        // added Custom Rule for API call
        if ($request->ajax() || $request->wantsJson()) {
            if($response == Password::RESET_LINK_SENT)
            {
                return array('status' => config('frontapi.SUCCESS'), 
                    'message' => config('frontapi.PasswordReset') , 
                    'message_ar' => config('frontapi.PasswordReset_AR') );
            }    
            else
            {
                return array('status' => config('frontapi.ERROR'), 
                    'message' => config('frontapi.PasswordResetError') , 
                    'message_ar' => config('frontapi.PasswordResetError_AR'));
            }
        }

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }

    /**
     * Reset the given Customers password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
        ])->save();
    }

}
