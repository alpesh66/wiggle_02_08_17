<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Http\Requests;
use Session;
use Datatables;
use DB;

class ApproveController extends Controller {

//
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
     
        $provider=session('provider_id');
        $adopts = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('adopts', 'customers.id', '=', 'adopts.customer_id')
            ->where('adopts.provider_id',$provider)
            ->where('adopts.sender',1)
            ->where('adopts.status',1)->get();
 
        $adopts = json_decode(json_encode($adopts), true);
 
        $fosters = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('fosters', 'customers.id', '=', 'fosters.customer_id')                
            ->where('fosters.provider_id',$provider)
            ->where('fosters.sender',1)
            ->where('fosters.status',1)->get();
        
        $fosters = json_decode(json_encode($fosters), true);
      
        $surrenders = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('surrenders', 'customers.id', '=', 'surrenders.customer_id')
            ->where('surrenders.provider_id',$provider)
            ->where('surrenders.sender',1)
            ->where('surrenders.status',1)->get();
        
        $surrenders = json_decode(json_encode($surrenders), true);
       
        
        $volunteers = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('volunteers', 'customers.id', '=', 'volunteers.customer_id')
            ->where('volunteers.provider_id',$provider)
            ->where('volunteers.sender',1)
            ->where('volunteers.status',1)->get();
    
        $volunteers = json_decode(json_encode($volunteers), true);
        
        $customer = array_merge($adopts,$fosters,$surrenders,$volunteers);
        //$customer = array_unique($customer1);
        $customer = array_map("unserialize", array_unique(array_map("serialize", $customer)));
        /*echo "<pre>";
        print_r($customer);
        exit();*/

        
        
        return view('society.approve.index1',compact('customer'));
    }
   /* public function index(Request $request)
    {

        if ($request->ajax())
        {
            return $this->callDatatables1($request);
        }
        return view('society.approve.index');
    }*/
    protected function callDatatables($request)
    {
        $provider=session('provider_id');
        $customer = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
            ->leftjoin('pets', 'customer_pet.pet_id', '=', 'pets.id')
            ->leftjoin('adopts', 'customers.id', '=', 'adopts.customer_id')
            ->leftjoin('fosters', 'fosters.customer_id', '=', 'customers.id')
            ->leftjoin('surrenders', 'customers.id', '=', 'surrenders.customer_id')
            ->leftjoin('volunteers', 'fosters.customer_id', '=', 'customers.id')
            ->where('volunteers.provider_id',$provider)
            ->where('volunteers.sender',1)
            ->where('volunteers.status',1)
            ->where('surrenders.provider_id',$provider)
            ->where('surrenders.sender',1)
            ->where('surrenders.status',1)
            ->where('fosters.provider_id',$provider)
            ->where('fosters.sender',1)
            ->where('fosters.status',1)
            ->where('adopts.provider_id',$provider)
            ->where('adopts.sender',1)
            ->where('adopts.status',1);
       /* $adopts = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('adopts', 'customers.id', '=', 'adopts.customer_id')
            ->where('adopts.provider_id',$provider)
            ->where('adopts.sender',1)
            ->where('adopts.status',1);
        $fosters = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('fosters', 'customers.id', '=', 'fosters.customer_id')                
            ->where('fosters.provider_id',$provider)
            ->where('fosters.sender',1)
            ->where('fosters.status',1);
        $surrenders = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('surrenders', 'customers.id', '=', 'surrenders.customer_id')
            ->where('surrenders.provider_id',$provider)
            ->where('surrenders.sender',1)
            ->where('surrenders.status',1);
        $customer = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('volunteers', 'customers.id', '=', 'volunteers.customer_id')
            ->where('volunteers.provider_id',$provider)
            ->where('volunteers.sender',1)
            ->where('volunteers.status',1)
            ->union($adopts)
            ->union($fosters)
            ->union($surrenders);*/
       // $customer = DB::table(DB::raw("({$union->toSql()}) as x"));
        /*$customer = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->union($adopts)
            ->union($fosters)
            ->union($surrenders)
            ->union($volunteers);*/

            
            /*->groupBy('customers.id');*/
        /*echo "<pre>";*/
        $datatables =  app('datatables')->of($customer)

            ->addColumn('action', function($customer) {
                return view('customers.action', compact('customer'))->render();
            })
            ->editColumn('nospets', function($customers) {
                return $customers->pet->count();
                // return '1';
            })
            ->editColumn('nosdogs', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('name', function($customer) {
                return $customer->name;
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $post1       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }



        if ($post != '' && $post1!="" ) {

        $customer1 = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            ->leftjoin('adopts', 'customers.id', '=', 'adopts.customer_id')
            ->where('adopts.provider_id',$provider)
            ->where('adopts.sender',1)
            ->where('adopts.status',1)->count();
       
        if($customer1>0){
             
            $customer = Customer::
                select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                ->leftjoin('adopts', 'customers.id', '=', 'adopts.customer_id')
                ->where('adopts.provider_id',$provider)
                ->where('adopts.sender',1)
                ->where('adopts.status',1);
            /*echo "<pre>";
            print_r($customer);
            exit();*/
            $datatables =  app('datatables')->of($customer)
            ->addColumn('action', function($customer) {
                return view('customers.action', compact('customer'))->render();
            })
            ->editColumn('nospets', function($customers) {
                return $customers->pet->count();
                // return '1';
            })
            ->editColumn('nosdogs', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('name', function($customer) {
                return $customer->name;
            });
            $datatables->where( $name, $operator, $post);
        }
        else{
            $customer1 = Customer::
                select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                ->leftjoin('fosters', 'customers.id', '=', 'fosters.customer_id')                
                ->where('fosters.provider_id',$provider)
                ->where('fosters.sender',1)
                ->where('fosters.status',1)->count();
            if($customer1>0){
                $customer = Customer::
                    select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                    ->leftjoin('fosters', 'customers.id', '=', 'fosters.customer_id')                
                    ->where('fosters.provider_id',$provider)
                    ->where('fosters.sender',1)
                    ->where('fosters.status',1);
                $datatables =  app('datatables')->of($customer)
                 ->addColumn('action', function($customer) {
                return view('customers.action', compact('customer'))->render();
            })
            ->editColumn('nospets', function($customers) {
                return $customers->pet->count();
                // return '1';
            })
            ->editColumn('nosdogs', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('name', function($customer) {
                return $customer->name;
            });
                $datatables->where( $name, $operator, $post);
            }
            else {
                $customer1 = Customer::
                    select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                    ->leftjoin('surrenders', 'customers.id', '=', 'surrenders.customer_id')
                    ->where('surrenders.provider_id',$provider)
                    ->where('surrenders.sender',1)
                    ->where('surrenders.status',1)->count();
                if($customer1>0){
                    $customer = Customer::
                    select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                    ->leftjoin('surrenders', 'customers.id', '=', 'surrenders.customer_id')
                    ->where('surrenders.provider_id',$provider)
                    ->where('surrenders.sender',1)
                    ->where('surrenders.status',1);
                    $datatables =  app('datatables')->of($customer)
                     ->addColumn('action', function($customer) {
                        return view('customers.action', compact('customer'))->render();
                    })
                    ->editColumn('nospets', function($customers) {
                        return $customers->pet->count();
                        // return '1';
                    })
                    ->editColumn('nosdogs', function($customers) {
                        $count =  $customers->pet->count();
                        if($count>0){
                            $collect = collect($customers->pet);
                            $dogs = $collect->groupBy('breed')->all();
                            return isset($dogs[2])? count($dogs[2]) : 0  ;
                        } else {
                            return 0;
                        }
                    })
                    ->editColumn('noscats', function($customers) {
                        $count =  $customers->pet->count();
                        if($count>0){
                            $collect = collect($customers->pet);
                            $dogs = $collect->groupBy('breed')->all();
                            return isset($dogs[1])? count($dogs[1]) : 0  ;
                        } else {
                            return 0;
                        }
                    })
                    ->editColumn('status', '@if($status) Active @else Inactive @endif')
                    ->editColumn('name', function($customer) {
                        return $customer->name;
                    });
                    $datatables->where( $name, $operator, $post);
                }
                else {
                    $customer = Customer::
                    select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                    ->leftjoin('volunteers', 'customers.id', '=', 'volunteers.customer_id')
                    ->where('volunteers.provider_id',$provider)
                    ->where('volunteers.sender',1)
                    ->where('volunteers.status',1);
                    $datatables =  app('datatables')->of($customer)
                     ->addColumn('action', function($customer) {
                        return view('customers.action', compact('customer'))->render();
                    })
                    ->editColumn('nospets', function($customers) {
                        return $customers->pet->count();
                        // return '1';
                    })
                    ->editColumn('nosdogs', function($customers) {
                        $count =  $customers->pet->count();
                        if($count>0){
                            $collect = collect($customers->pet);
                            $dogs = $collect->groupBy('breed')->all();
                            return isset($dogs[2])? count($dogs[2]) : 0  ;
                        } else {
                            return 0;
                        }
                    })
                    ->editColumn('noscats', function($customers) {
                        $count =  $customers->pet->count();
                        if($count>0){
                            $collect = collect($customers->pet);
                            $dogs = $collect->groupBy('breed')->all();
                            return isset($dogs[1])? count($dogs[1]) : 0  ;
                        } else {
                            return 0;
                        }
                    })
                    ->editColumn('status', '@if($status) Active @else Inactive @endif')
                    ->editColumn('name', function($customer) {
                        return $customer->name;
                    });
                    $datatables->where( $name, $operator, $post);
                }   
            }
        }
        
            
        }

        return $datatables->make(true);
        
    }
    protected function callDatatables1($request)
    {
        $provider=session('provider_id');
        $customer = Customer::
            select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
            /*->leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
            ->leftjoin('pets', 'customer_pet.pet_id', '=', 'pets.id')*/
            ->leftjoin('adopts', 'adopts.customer_id', '=', 'customers.id')
            ->leftjoin('fosters', 'fosters.customer_id', '=', 'customers.id')
            ->leftjoin('surrenders', 'surrenders.customer_id', '=', 'customers.id')
            ->leftjoin('volunteers', 'volunteers.customer_id', '=', 'customers.id')
            ->where('volunteers.provider_id',$provider)
            ->where('volunteers.sender',1)
            ->where('volunteers.status',1)
            ->where('surrenders.provider_id',$provider)
            ->where('surrenders.sender',1)
            ->where('surrenders.status',1)
            ->where('fosters.provider_id',$provider)
            ->where('fosters.sender',1)
            ->where('fosters.status',1)
            ->where('adopts.provider_id',$provider)
            ->where('adopts.sender',1)
            ->where('adopts.status',1);
       
       
        $datatables =  app('datatables')->of($customer)

            ->addColumn('action', function($customer) {
                return view('customers.action', compact('customer'))->render();
            })
            ->editColumn('nospets', function($customers) {
                return $customers->pet->count();
                // return '1';
            })
            ->editColumn('nosdogs', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('name', function($customer) {
                return $customer->name;
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $post1       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }
        if ($post != '') {
            $datatables->where( $name, $operator, $post);
        }
        return $datatables->make(true);
        
    }
}
