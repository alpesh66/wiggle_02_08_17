<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MissYou;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Inbox;
use App\Pet;
use Session;
use App\Http\Requests\MissYouRequest;
use Mail;
use Illuminate\Support\Facades\Config;

class MissYouController extends Controller {

    public function __construct() {

        if (strpos(\Request::path(), 'societies/missyou') !== FALSE) {
            $this->redirect_module = 'societies/missyou';
        } else {
            $this->redirect_module = 'providers/missyou';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request) {        
        if($request->ajax())
        {
          
            $customer = MissYou::select('miss_you_request.*','customers.firstname', 'customers.lastname','miss_you_pet.pet_id')
                ->leftjoin('customers', 'customers.id', '=', 'miss_you_request.customer_id')
                ->join('miss_you_pet', 'miss_you_pet.miss_you_id', '=', 'miss_you_request.id')
                ->join('pets', 'miss_you_pet.pet_id', '=', 'pets.id')
                ->where('miss_you_request.provider_id',Session::get('provider_id'))
                ->where('miss_you_request.sender' , 1)                
                ->orderBy('miss_you_request.id','DESC');
            /*echo "<pre>";
            print_r($customer);
            exit();*/
            $datatables =  app('datatables')->of($customer)
                
                ->editColumn('customer_name', function($customer) {
                  if(strpos(\Request::path(),'societies/kennels') !==FALSE)
                    $setActionUser = 'societies/customers'; 
                  else
                    $setActionUser = 'providers/customers'; 

                    return "<u><a style='' href=".url($setActionUser.'/'.$customer->customer_id)." >".$customer->firstname.' '.$customer->lastname."</a></u>";
                })
                ->editColumn('created_at', function($customer) {
                    $created_at = date_format($customer->created_at, 'Y-m-d H:i:s');
                    return $created_at;
                })
                ->editColumn('updated_at', function($customer) {
                  $reply_date = ($customer->parent_id == 0) ? NULL : $customer->updated_at;
                  if($reply_date!=NULL)
                    $reply_date = date_format($reply_date, 'Y-m-d H:i:s');
                    return $reply_date;
                })
               
                ->addColumn('pet', function($customer) {
                    if(isset($customer->pet_id))
                    {
                        $pet = Pet::where('id',$customer->pet_id)->get();
                        $pet_detail = display_pet_name_return($pet);
                        return $pet_detail;
                    }
                    else{
                     return '---';   
                    }
                })
                ->addColumn('petType', function($customer) { 
                    if(isset($customer->pet_id) && $customer->pet_id!='0'){
                        $pet = Pet::where('id',$customer->pet_id)->select('breed')->first();                
                        if(isset($pet->breed) && $pet->breed!=0){
                            $breed = get_breeds_details($pet->breed);
                            if(isset($breed->name))
                                return $breed->name;    
                            else
                                return "---";    
                        }
                        else
                        { return "---"; }                        
                    }
                    else{
                        return "---";
                    }                     
                })
                ->addColumn('action', function($customer) {
                    if(strpos(\Request::path(),'societies/missyou') !==FALSE)
                      $setAction = 'societies/missyou'; 
                    else
                      $setAction = 'providers/missyou'; 

                    $buttons = '<a href="'.url($setAction.'/'.$customer->id).'" class="btn btn-success btn-xs" title="View Miss You Request"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>';

                    $buttons .= '<button type="button" class="btn btn-primary btn-xs replyClick replyStatus'.$customer->id.'" title="'.$customer->title.'" ids="'.$customer->id.'" customer_id="'.$customer->customer_id.'" data-toggle="modal"  data-target="#myModal" id="replyStatus'.$customer->customer_id.'" id="replyStatus" data-id="'.$customer->customer_id.'" data-post="data-php" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"/></button>';

                    
                    $buttons .= '<form method="POST" action="'.url($setAction.'/'.$customer->id).'" accept-charset="UTF-8" style="display:inline"><input name="_method" value="DELETE" type="hidden"><input name="_token" value="'.csrf_token().'" type="hidden"><button type="submit" class="btn btn-danger btn-xs" title="Delete Miss You Request" onclick="return confirm(&quot;Are you sure you want to delete?&quot;)"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Kennel"></span></button></form>';
                    return $buttons;
                })
                ->setRowClass(function ($customer) {
                    if($customer->is_read == 1 && $customer->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });


            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }
            /*echo "<pre>";
            print_r($datatables);
            exit();*/
            return $datatables->make(true);
            return view('/servicepro/missyou.index');
        }
        else{
            $missyou = MissYou::with('pet')->allMessage()->orderBy('id','DESC')->paginate(15);
            return view('/servicepro/missyou.index', compact('missyou'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create() {
        return view('servicepro/missyou.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request) {

        MissYou::create($request->all());

        Session::flash('flash_message', 'Miss You Request added!');

        return redirect($this->redirect_module);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id) {

        $missyou = MissYou::with('pet')->findOrFail($id);
        $missyou->is_read = "0";
        $missyou->save();

        $message = MissYou::with('pet')->where('parent_id',$id)->where('sender',2)->get()->toArray();

        return view('/servicepro/missyou.show', compact('missyou','message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id) {
        $kennel = MissYou::findOrFail($id);
        return view('servicepro/missyou.edit', compact('kennel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request) {

        $foster = MissYou::findOrFail($id);
        $foster->update($request->all());

        Session::flash('flash_message', 'Miss You Request updated!');

        return redirect($this->redirect_module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id) {
        MissYou::destroy($id);
        Session::flash('flash_message', 'Miss You Request deleted!');
        return redirect($this->redirect_module);
    }

    public function reply(MissYouRequest $request) {
        $kennelUpdate = MissYou::findOrFail($request->input('id'));
        $customerid = $kennelUpdate->customer_id;
        $provider_id = $kennelUpdate->provider_id;
        $customer = \App\Customer::findOrFail($kennelUpdate->customer_id);

        $request['parent_id'] = $request->input('id');
        
        $kennelUpdate->update($request->all());

        $request['provider_id'] = session('provider_id');
        $description = $request->input('description');
        
        $request['message'] = str_replace('/vendor/templateEditor/kcfinder/upload/images', url('/') . '/vendor/templateEditor/kcfinder/upload/images', $description);

        $request['parent_id'] = $request->input('id');
        $request['sender'] = 2;
        MissYou::create($request->all());

        $current_date = date('Y-m-d H:i:s');
        $updatedData['updated_at'] = $current_date;
        $updatedData['is_read'] = 0;
        $r = MissYou::where('id', $request->input('id'))->update($updatedData);                 

//      send push notification to user         
        $reminder['message'] = $request['message'];
        $reminder['provider_id'] = $provider_id;
        $reminder['service_id'] = 0;
        $reminder['booking_id'] = 0;
        $reminder['isnew'] = 0;
        $reminder['hadread'] = 1;
        $reminder['type'] = 2;
        $reminder['customer_id'] = $customerid;
        $reminder['sender'] = 1;
        $reminder['subject'] = push_messages('missyou_response.en.title');
        $message = Inbox::Create($reminder);         
        $message_id = $message->id;

        sendPushNoti(push_messages('missyou_response.en.message'), push_messages('missyou_response.ar.message'),$customerid, $message_id);      
        try {
            $email = array($customer->email);
            //$bcc_email = array('aditya.simplifiedinformatics@gmail.com');
            //$email = array("alpesh66@yopmail.com");
            $bcc_email = array();

            $data = array('customer_name' => ucfirst($customer->firstname . ' ' . $customer->lastname), 'reply_message' => $request['message']);

            Mail::send('emails.missyou', $data, function ($message) use($email, $bcc_email) {
                $message->to($email);
                $message->subject("Miss You request response mail");
                if ($bcc_email) {
                    $message->bcc($bcc_email);
                }
            });
        } catch (\Swift_TransportException $e) {
            return json_encode(array('status' => '0', 'message' => $e->getMessage()));
        } catch (Exception $e) {
            return json_encode(array('status' => '0', 'message' => $e->getMessage()));
            log_message("ERROR", $e->getMessage());
        }


        return json_encode(array('status' => '1', 'message' => 'Reply has been submited successfully.'));
    }

    public function test_mail() {
//        error_reporting(E_ALL);
//        ini_set('display_erros', 1);
//        echo 'here';exit;
        try {
            $email = array('jamesdonald446@gmail.com');
            $data = array('customer_name' => 'nitin', 'provider_name' => "vipro", 'booking_date' => date('m D Y'), 'booking_time' => date('h:i a'));


            Mail::send('emails.missyou', $data, function ($message) use($email) {
                $message->from('vnits108@gmail.com', 'Vin Master');
                $message->to($email);
                $message->subject("Miss you request test");
            });

            if (count(Mail::failures()) > 0) {

                echo "There was one or more failures. They were: <br />";

                foreach (Mail::failures as $email_address) {
                    echo " - $email_address <br />";
                }
            } else {
                echo "No errors, all sent successfully!";
            }
        } catch (Exception $e) {

            dd($e->getMessage());
            log_message("ERROR", $e->getMessage());
        }
    }

}
