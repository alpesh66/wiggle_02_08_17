<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Surrender;
use App\Inbox;
use App\Customer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Http\Requests\SurrenderRequest;
use Illuminate\Support\Facades\Config;
use Mail;

class SurrenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        /*$surrenders = Surrender::allMessage()
                    ->orderBy('is_read','DESC')
                    ->orderBy('sender','DESC')
                    ->orderBy('created_at','DESC')
                    ->paginate(15);      
        return view('/society/surrenders.index_old', compact('surrenders'));*/
        /*$surrenders = Surrender::
        select(['surrenders.id', 'surrenders.created_at', 'surrenders.updated_at', 'surrenders.title','surrenders.status','pet_surrender.pet_id','pets.breed','pets.name as petName', 'surrenders.parent_id','surrenders.is_read','surrenders.sender','surrenders.customer_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS name')])
        ->leftjoin('pet_surrender', 'pet_surrender.surrender_id', '=', 'surrenders.id')
        ->leftjoin('pets', 'pet_surrender.pet_id', '=', 'pets.id')
        ->leftjoin('customers', 'customers.id', '=', 'surrenders.customer_id')
        ->where('surrenders.provider_id', session('provider_id'))
        ->where('surrenders.sender', 1)
        ->groupBy('surrenders.id')->get();
        echo "<pre>";
        print_r($surrenders);
        exit();*/
        if($request->ajax())
        {            
            $surrenders = Surrender::
            select(['surrenders.id', 'surrenders.created_at', 'surrenders.updated_at', 'surrenders.title','surrenders.status','pet_surrender.pet_id','pets.breed','pets.name as petName', 'surrenders.parent_id','surrenders.is_read','surrenders.sender','surrenders.customer_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS name')])
            ->leftjoin('pet_surrender', 'pet_surrender.surrender_id', '=', 'surrenders.id')
            ->leftjoin('pets', 'pet_surrender.pet_id', '=', 'pets.id')
            ->leftjoin('customers', 'customers.id', '=', 'surrenders.customer_id')
            ->where('surrenders.provider_id', session('provider_id'))
            ->where('surrenders.sender', 1)
            ->groupBy('surrenders.id');

            $datatables =  app('datatables')->of($surrenders)

                ->addColumn('action', function($surrenders) {
                    return view('society.surrenders.action', compact('surrenders'))->render();
                })
                ->editColumn('name', function($surrenders) {
                    return "<a style='font-weight: bold;' href='".url('societies/customers/'.$surrenders->customer_id)."' >".$surrenders->name."</a>";
                })
                ->addColumn('petType', function($surrenders) {
                    if($surrenders->breed==1)
                        return 'Cat';
                    else
                        return 'Dog';
                })
                ->editColumn('petName', function($surrenders) {
                    return "<a style='font-weight: bold;' href='".url('societies/pet/'.$surrenders->pet_id)."' >".$surrenders->petName."</a>";
                })
                ->editColumn('updated_at', function($surrenders) {
                    if($surrenders->parent_id==0)
                        return NULL;
                    else
                        return $surrenders->updated_at;
                })
                ->editColumn('status', '@if($status == 1) Approved @elseif($status == 2) Reject @else Pending  @endif')
                ->setRowClass(function ($surrenders) {
                    if($surrenders->is_read == 1 && $surrenders->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'approved'){
                    $post = '1';
                } elseif(strtolower($val) == 'reject'){
                    $post = '2';
                }else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('society.surrenders.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('/society/surrenders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Surrender::create($request->all());

        Session::flash('flash_message', 'Surrender added!');

        return redirect('surrender');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $surrender = Surrender::findOrFail($id);
        $surrender->is_read = "0";
        $surrender->save();
        $customer = Customer::with('pet')->findOrFail($surrender->customer->id);
        
        return view('society/surrenders.show', compact('surrender','customer'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $surrender = Surrender::findOrFail($id);

        return view('society/surrenders.edit', compact('surrender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $foster = Surrender::findOrFail($id);
        $foster->update($request->all());

        Session::flash('flash_message', 'Surrender updated!');

        return redirect('societies/surrenders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $ids = $request->id; 
        if(!empty($ids)){
            foreach ($ids as $key => $id) {
                Surrender::destroy($id);
            }    
            return json_encode(array('status'=>'1','message'=>'Surrender deleted!'));
        }else{
            Surrender::destroy($id);
            Session::flash('flash_message', 'Surrender deleted!');
            return redirect('societies/surrenders');
        } 
    }

    public function reply(SurrenderRequest $request){
        $user_id = session('user_id');
        if(isset($user_id)){
            $user_id=$user_id;
        }
        else{
            $user_id=1;   
        }
        $surrenderUpdate = Surrender::findOrFail($request->input('id'));
        $request['parent_id'] = $request->input('id');
        $status = $request->input('status');
        $description = $request->input('description');
        $customerid = $surrenderUpdate->customer_id;
        /*$surrenderUpdate->update($request->all());*/
        $surrenderUpdate->update(['parent_id' =>$request->input('id'),'status'=>$status,'is_read'=>0]);
        
        $request['provider_id'] = session('provider_id');
        $request['parent_id'] = $request->input('id');
        $request['sender'] = 2;
        if($status==1){
            $pet=$surrenderUpdate->pet->first();
            $petLog=[
                'pet_id'=>$pet['id'],
                'provider_id'=>session('provider_id'),
                'status'=>'Surrender',
                'admin_id'=>  $user_id,
                'customer_id'=>$surrenderUpdate->customer_id
            ];
            \App\Petlog::create($petLog);
            \App\Pet::where('id', '=', $pet['id'])->update(['pet_status' =>'Surrender','isadopted'=>0]);
            \DB::table('customer_pet')->where(['customer_id'=>$surrenderUpdate->customer_id,'pet_id'=>$pet['id']])->delete();
            //event(new \App\Events\SurrenderConfirmed($surrenderUpdate->id));
        }
        Surrender::create($request->all());
        //      send push notification to user            
      /*if($status==0){
          send_push_notification(push_messages('pending_surrender.en.message'), push_messages('pending_surrender.ar.message'),$customerid);
      }else*/
        $customer_email = false;
        $provider_email = false;            
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
        $sikw_email = env('SIKW_BCC_COPY', false);
        $surrenders = Surrender::findOrFail($request->input('id'));
        $customer_id=$surrenders->customer_id;
        $provider_id=$surrenders->provider_id;
        $request_id=$surrenders->id;
        $data = array();
        $fost= Surrender::SingleBookingDetail($request_id)->first(); 
        $customer_email = $fost['customer_email'];
        $provider_email = $fost['providers_email'];
        $provider_name = $fost['provider_name'];

        $email_datacustomer_name = $fost['customer_name'];
        $pet=\DB::table('pets')->where('id',$fost['petid'])->get();
        if($status==1){
            $pushMessage_en=push_messages('approved_surrender.en.message');
            $pushMessage_ar=push_messages('approved_surrender.ar.message');
            $extraData=array('type'=>'SURRENDER','reminder'=>FALSE);
            $startdate=$fost['startdate'];
            $enddate=$fost['enddate'];
            $email_dataprovider_name = $fost['provider_name'];
            $fost['petid'];
                      
            
          
            $message = "Dear {$email_datacustomer_name}, Your Surrender Request with {$email_dataprovider_name} for ";
            $messagePro="Dear {$email_dataprovider_name}, New Surrener Request from {$email_datacustomer_name} for ";
            foreach($pet as $val)
            {
                $message .= "{$val->name}";
                $messagePro .= "{$val->name}";
            }
            $message .= "date between  {$startdate} and {$enddate} is approved.";
            $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
          
            // echo "<pre>"; print_r($pet); 
          
            $customers_mobile = $fost['customers_mobile'];
            $data['customer_name'] = $email_datacustomer_name;
            $data['startdate']=$startdate;
            $data['enddate']=$enddate;
            $data['provider_name'] = $email_dataprovider_name;
            $data['customers_mobile'] = $customers_mobile;
            $data['description'] = $description;
            $data['petname']=$pet[0]->name;
            $data['msgCust']="Your surrender Request is apporved for ".$pet[0]->name;
            $data['msgPro']="New surrender Request from"." ".$email_datacustomer_name. " "."for"." ".$pet[0]->name." "."is Approved";
                      
            //  file_put_contents($this->logFile, "\n\n =========datafoster : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);
              /*add data in inbox(message)--==*/
            $reminder['message'] = "Dear ".$email_datacustomer_name.",<br/>Your surrender request has been approved for ".$pet[0]->name;
            $reminder['provider_id'] = $provider_id;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 1;
            $reminder['type'] = 1;
            $reminder['customer_id'] = $customer_id;
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('approved_surrender.en.title');
            $messageobj = Inbox::Create($reminder);         
            $message_id = $messageobj->id;
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
              /*--==*/
            if ($customer_email) {
                try {
                    /*Mail::queue('emails.surrenderEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Surrender request is approved");
                        if ($sikw_email) {
                          $message->bcc($sikw_email);
                        }
                    });*/
                    Mail::queue('emails.Surrender_Approval',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Surrender request is approved");
                        if ($sikw_email) {
                          $message->bcc($sikw_email);
                        }
                    });  
                } catch (\Swift_TransportException $e) {
                    //file_put_contents($this->logFile, "\n\n =========SurrenderEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    //file_put_contents($this->logFile, "\n\n =========surrenderEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    //log_message("ERROR", $e->getMessage());
                }
            }
            if ($provider_email) {
                try {
                    Mail::queue('emails.surrenderEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                      $messagePro->to($provider_email);
                      $messagePro->subject("New Surrender is Approved");
                      if ($sikw_email) {
                          $messagePro->bcc($sikw_email);
                      }
                    });
                } catch (\Swift_TransportException $e) {
                    //file_put_contents($this->logFile, "\n\n =========surrenderEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    //file_put_contents($this->logFile, "\n\n =========surrenderEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                    //log_message("ERROR", $e->getMessage());
                }
            }
        }elseif($status==2){

            $pushMessage_en=push_messages('reject_surrender.en.message');
            $pushMessage_ar=push_messages('reject_surrender.ar.message');
            /*$customer_id=$surrenders->customer_id;
            $provider_id=$surrenders->provider_id;*/
            $extraData=array('type'=>'SURRENDER','reminder'=>FALSE);

            $reminder['message'] = "Dear ".$email_datacustomer_name.",<br/>Your surrender request has been rejected for ".$pet[0]->name.".";
            $reminder['provider_id'] = $provider_id;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 1;
            $reminder['type'] = 1;
            $reminder['customer_id'] = $customer_id;
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('reject_surrender.en.title');
            $message = Inbox::Create($reminder);         
            $message_id = $message->id;

            $data['customer_name'] = $email_datacustomer_name;
            $data['provider_name'] = $provider_name;
            $data['petname'] = $pet[0]->name;
            $data['description'] = $description;
            

            Mail::queue('emails.Surrender_Rejected',$data,function ($message) use ($customer_email, $sikw_email) {
                $message->to($customer_email);
                $message->subject("Surrender request is Rejected");
                if ($sikw_email) {
                  $message->bcc($sikw_email);
                }
            }); 
            
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
            
        }
        return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
    }
}
