<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ProviderHoliday;
use Session;


class ProviderHolidayController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {	
        if ($request->ajax()){
            $provider_id = session('provider_id');
            $providerholidays = ProviderHoliday::select(['start','end','starttime','endtime','type','status','provider_id','id'])
                ->where('provider_id', session('provider_id'));

            $datatables =  app('datatables')->of($providerholidays)
                ->editColumn('type', '@if($type) Half Day @else Full Day Off @endif')
                ->addColumn('action', function($providerholidays) {
                    return view('servicepro.providerholiday.action', compact('providerholidays'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.providerholiday.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('servicepro.providerholiday.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $provider_id = Session::get('provider_id');
        $requestData = $request->all();
        
        $requestData['provider_id'] = $provider_id;  
        $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
        $requestData['end'] = date('Y-m-d',strtotime($requestData['end']));  
        
        // if(isset($requestData['status']) && $requestData['status'] == 'on'){
           $requestData['status'] = 1; 
        // } else {
            // $requestData['status'] = 0;
        // }

        ProviderHoliday::create($requestData);

        Session::flash('flash_message', ' added!');

        return redirect('providers/providerholidays');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $technicianleave = ProviderHoliday::findOrFail($id);
        // echo '<pre>'; print_r($technicianleave); exit;
        return view('servicepro.technicianleave.show', compact('technicianleave'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $provider_id = Session::get('provider_id');
        $technicianleave = ProviderHoliday::findOrFail($id);
        $technicians = \App\Technician::where('provider_id', $provider_id )->pluck('name','id');
        // echo '<pre>'; print_r($technicianleave); exit;
        return view('servicepro.technicianleave.edit', compact('technicianleave','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
        $requestData['toend'] = date('Y-m-d',strtotime($requestData['toend']));  
        
        if(isset($requestData['status']) && $requestData['status'] == 'on'){
           $requestData['status'] = 1; 
        } else {
            $requestData['status'] = 0;
        }

        // echo '<pre>'; print_r($requestData); exit;
        $techleave = ProviderHoliday::findOrFail($id);
        $techleave->update($requestData);

        Session::flash('flash_message', ' updated!');

        return redirect('providers/technicianleave');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        ProviderHoliday::destroy($id);
        if ($request->ajax() )
        {
            $data['success'] = true;
            $data['message'] = 'Holiday deleted successfully';    
            return $data;
        }

        Session::flash('flash_message', ' deleted!');

        return redirect('providers/technicianleave');
    }
}
