<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\EventRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Event;
use App\Provider;
use App\Category;
use App\Customer;
use App\Pet;
use App\Booking;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Excel;
use PHPExcel_Worksheet_Drawing;
class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        /*if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }*/
        $categories = Category::ActiveParentListing()->pluck('name', 'id')->toArray();
        $providers = Provider::where('ptype',1)->pluck('name', 'id')->toArray();
        $customers = Customer::select('firstname', 'lastname', 'id')->get()->toArray();
        $BookingStatus=array("1"=>"Confirm","2"=>"Cancel","3"=>"No Show");
        $BookingFrom=array("0"=>"Mobile","1"=>"Admin");
        $PaymentStatus=array("0"=>"Online","1"=>"Cash");
        return view('sales.index', compact('categories','providers','customers','BookingStatus','BookingFrom','PaymentStatus'));
    }
    public function getCategoryList(Request $request)
    {
        if($request->ajax()){
            $categories = DB::table('categories')->whereIn('parentid',$request->parentid)->pluck("name","id");
            $data = "";
            foreach ($categories as $key => $value) {
                $data .= "<option value='".$key."'>".$value."</option>";
            }
            return $data;
        }
    }
    public function generateSalesReport(Request $request)
    {
        $provider_id = $request->provider_id;
        $temparaments=$request->input('temperament');
        $customer_id = $request->customer_id;
        $parentid = $request->parentid;
        $service_id = $request->service_id;
        $booking_status = $request->booking_status;
        $booking_from = $request->booking_from;
        $payment_status = $request->payment_status;
        if(isset($request->startdate) && $request->startdate!=""){
            $startdate = $request->startdate;
            $startdate=date_create($startdate);
            $book_startdate = date_format($startdate, 'Y-m-d');    
        }else{
            $startdate = "";
            $book_startdate ="";
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $enddate = $request->enddate;
            $enddate = date_create($enddate);
            $book_enddate = date_format($enddate, 'Y-m-d');
        }else{
            $enddate = "";
            $book_enddate ="";
        }        

        $bookings = Booking::join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->select(['customers.id','customers.firstname','customers.middlename','customers.lastname', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.end', 'bookings.type_id', 'bookings.status', 'bookings.noshow','bookings.madefrom','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes','bookings.type_id','bookings.created_at']);
        /*echo "<pre>";
        print_r($provider_id);
        exit();*/
        if(isset($provider_id) && $provider_id!="")
            $bookings->whereIn('bookings.provider_id', $provider_id);
        if(isset($customer_id) && $customer_id!="")
            $bookings->whereIn('bookings.customer_id', $customer_id);
        if(isset($parentid) && $parentid!="")
            $bookings->whereIn('categories.parentid', $parentid);
        if(isset($service_id) && $service_id!="")
            $bookings->whereIn('categories.id', $service_id);
        /*if(isset($booking_status) && $booking_status!="")
            $bookings->where('bookings.status', $booking_status);*/
            
        if(isset($booking_status) && $booking_status!=""){
            if (in_array("3", $booking_status))
            {
                $bookings->where('bookings.status', 1);    
                $bookings->where('bookings.noshow', 'true');    
            }
            if(in_array("1", $booking_status))
            {
                if(in_array("3", $booking_status))
                {
                    $bookings->Orwhere('bookings.status', 1);    
                    $bookings->where('bookings.noshow', 'false');    
                }else{
                    $bookings->where('bookings.status', 1);    
                    $bookings->where('bookings.noshow', 'false');    
                }
            }
            if(in_array("2", $booking_status))
            {
                if(in_array("1", $booking_status) || in_array("3", $booking_status))
                {
                    $bookings->Orwhere('bookings.status', 2);    
                }else{
                    $bookings->where('bookings.status', 2);   
                }
            }
            /*if($booking_status==3){
                $bookings->whereIn('bookings.status', 1);    
                $bookings->whereIn('bookings.noshow', 'true');    
            }elseif($booking_status==1){
                $bookings->whereIn('bookings.status', 1);    
                $bookings->whereIn('bookings.noshow', 'false');    
            }else{
                $bookings->whereIn('bookings.status', $booking_status);    
            }*/
        }
        if(isset($booking_from)  && $booking_from!="")
            $bookings->whereIn('bookings.madefrom', $booking_from);
        if(isset($book_startdate)  && $book_startdate!="")
            $bookings->where('bookings.start','>=',$book_startdate);
        if(isset($book_enddate)  && $book_enddate!="")
            $bookings->where('bookings.end','<=',$book_enddate);
        
    
        //$result = $bookings->get();
        $result = $bookings->groupBy('bookings.ontime')->get();
        $result = json_decode(json_encode($result), true);

        /*$data[] =array();*/
        $totalConfirmBooking = 0;
        $totalCancelBooking = 0;
        $totalUnconfirmBooking = 0;
        $totalNoShow = 0;
        $bookings = 0;
        $mobile_booking = 0;
        $web_booking = 0;
        $vet= 0;
        $grooming = 0;
        $walker = 0;
        $training = 0;        
        foreach ($result as $key => $value) {
            $bookings++;
            if($value['status']==1 && $value['noshow']=='true'){
                $totalNoShow++;
                $status = "No Show";
            }
            else if($value['status']==1 && $value['noshow']=='false'){
                $status = "Confirm";
                $totalConfirmBooking++;
            }
            else if($value['status']==2){
                $status = "Cancelled";
                $totalCancelBooking++;
            }
            else{                
                $status = "Unconfirm";
                $totalUnconfirmBooking++;
            }

            if($value['madefrom']==0){
                $bookedFrom="Mobile App";
                $mobile_booking ++;
            }
            else{
                $bookedFrom="Admin";
                $web_booking ++;
            }
            if($value['type_id']==1){
                $vet++;
            }
            if($value['type_id']==2){
                $grooming++;        
            }
            if($value['type_id']==3){
                $walker++;
            }
            if($value['type_id']==4){
                $training++;
            }
            
            $categories = DB::table('categories')->where('id',$value['type_id'])->pluck("name");
            $services_cat=implode($categories, ',');
            $makehtml = array();
            $petDetail = array();
            $allBookings =  Booking::OnTimeBookingDetail($value['ontime'])->get();
            if($allBookings){
                $prepare = collect($allBookings);
                $petsBook = $prepare->groupBy('petname');

                foreach ($petsBook->all() as $pet_id => $valuep) {
                    $petDetail[] = $pet_id;
                    foreach ($valuep as $pet_key => $book_value) {
                        $makehtml[] = $book_value['servicename'];
                    }
                }

            }
            //print_r(array_unique($petDetail));
            $services = implode(array_unique($makehtml), ',');
            unset($makehtm);
            $customers_name=$value['firstname']." ".$value['lastname'];
            $transaction_no="";

            $data[] = array(
                    $value['id'],
                    $customers_name,                    
                    $value['petname'],
                    $transaction_no,
                    $value['start'],
                    $value['created_at'],
                    $services_cat,
                    $services,
                    $bookedFrom,
                    $status,
                    '',
                    ''
                );
        }
        if(isset($bookings) && $bookings!=0){
            $confirm_bookings_percent = $totalConfirmBooking*100/$bookings;
            $cancel_bookings_percent = $totalCancelBooking*100/$bookings;
            $unconfirm_bookings_percent = $totalUnconfirmBooking*100/$bookings;    
            $noshow_bookings_percent = $totalNoShow*100/$bookings;  
        }else{
            $confirm_bookings_percent = 0;
            $cancel_bookings_percent = 0;
            $unconfirm_bookings_percent = 0;    
            $noshow_bookings_percent = 0;    
        }
        if(isset($mobile_booking) && $mobile_booking!=0){
            $mob_percent = $mobile_booking*100/$bookings;
        }else{
            $mob_percent = 0;
        }
        if(isset($web_booking) && $web_booking!=0){
            $web_percent = $web_booking*100/$bookings;
        }else{
            $web_percent = 0;
        }
        if(isset($vet) && $vet!=0){
            $vet_percent = $vet*100/$bookings;
        }else{
            $vet_percent = 0;
        }
        if(isset($grooming) && $grooming!=0){
            $grooming_percent = $grooming*100/$bookings;
        }else{
            $grooming_percent = 0;
        }
        if(isset($walker) && $walker!=0){
            $walker_percent = $walker*100/$bookings;
        }else{
            $walker_percent = 0;
        }
        if(isset($training) && $training!=0){
            $training_percent = $training*100/$bookings;
        }else{
            $training_percent = 0;
        }


        $confirm_percent = round($confirm_bookings_percent,2)." %" ;
        $cancel_percent = round($cancel_bookings_percent,2)." %" ;
        $unconfirm_percent = round($unconfirm_bookings_percent,2)." %" ;
        $noshow_percent = round($noshow_bookings_percent,2)." %" ;
        $total_booking =  $bookings;

        $total_mobile_percent = round($mob_percent,2)." %" ;
        $total_web_percent = round($web_percent,2)." %" ;

        $total_vet_percent = round($vet_percent,2)." %" ;
        $total_grooming_percent = round($grooming_percent,2)." %" ;
        $total_walker_percent = round($walker_percent,2)." %" ;
        $total_training_percent = round($training_percent,2)." %" ;
       
         if($request->startdate!="" && $request->enddate!="")
            $reportDate = $request->startdate." to ".$request->enddate;
        else
            $reportDate = "";
        if(!empty($data)){
            $items = $data;
            Excel::create('SalesReport', function($excel) use($items,$totalConfirmBooking,$totalCancelBooking,$totalUnconfirmBooking,$totalNoShow,$confirm_percent,$noshow_percent,$cancel_percent,$unconfirm_percent,$bookings,$reportDate,$total_booking,$total_mobile_percent, $total_web_percent,$web_booking,$mobile_booking,$total_vet_percent,$total_grooming_percent,$total_walker_percent,$total_training_percent,$vet,$grooming,$training,$walker) {
                $excel->sheet('SalesReport', function($sheet) use($items,$totalConfirmBooking,$totalCancelBooking,$totalUnconfirmBooking,$totalNoShow,$confirm_percent,$noshow_percent,$cancel_percent,$unconfirm_percent,$bookings,$reportDate,$total_booking,$total_mobile_percent,$total_web_percent,$web_booking,$mobile_booking,$total_vet_percent,$total_grooming_percent,$total_walker_percent,$total_training_percent,$vet,$grooming,$training,$walker) {
                     $sheet->fromArray(array(
                        array('','','','','','Sales Report'),
                        array('','','','','',$reportDate),
                        array('Total Booking',$total_booking,' ','Vet Booking',$vet,$total_vet_percent,'Confirmed Booking', $totalConfirmBooking, $confirm_percent,'Total Online Payment',$bookings),
                        array('Manual Booking',$web_booking,$total_web_percent,'Grooming Booking',$grooming,$total_grooming_percent,'Cancelled Booking', $totalCancelBooking,$cancel_percent),
                        array('Mobile App Bookings',$mobile_booking,$total_mobile_percent,'Training Booking',$training,$total_training_percent,/*'Unconfirm ', $totalUnconfirmBooking,$unconfirm_percent*/'No Show ', $totalNoShow,$noshow_percent),
                        /*array('','','','Kennel Booking',$walker,$total_walker_percent),*/
                        array('','','','','',''),
                        array('Customer ID','Customer Name','Pet Name','Transaction Number','Appointment date and time','Booking Date and time', 'Service category','Service','Booked From','Booking Status','Payment Status','Payment Online')
                    ), null, 'A1', false, false);
                    $sheet->fromArray($items, null, 'A1', false, false);
                });
            })->export('xls');
        }
        else{
            return back()->with('message', 'No data found.');;
        }
    }

    public function getSocietyReport(Request $request)
    {
        
        $providers = Provider::where('providers.id','!=',5000)
                ->where('providers.ptype','=',2)->pluck('name', 'id')->toArray();
        $BookingStatus=array("1"=>"Approve","2"=>"Cancel","0"=>"Pending");
        $type=array("adopts"=>"Adopt","fosters"=>"Foster","surrenders"=>"Surrenders","volunteers"=>"Volunteers");
        return view('sales.SocietyIndex', compact('type','BookingStatus','providers'));
    }

    public function societyReport(Request $request)
    {
        /*$provider_id = Session::get('provider_id');*/
        $nameProvider ="";
        $booking_status = $request->booking_status;
        $type = $request->type;
        $providers = $request->providers;

        if(empty($providers)){
            $nameProvider ="All Society";
            $providers=array();
            $providersArray = Provider::where('providers.id','!=',5000)->where('providers.ptype','=',2)->pluck('name', 'id')->toArray();
            foreach ($providersArray as $key => $value) {
                $providers[]=($key);
            }
        }else{
            $nameProviderArray=array();
            foreach ($providers as $key => $value) {

                $providersArray = Provider::where('providers.id','!=',5000)->where('providers.id',$value)->where('providers.ptype','=',2)->select('name')->first()->toArray();
                $nameProviderArray[]=$providersArray['name'];
            }
            $nameProvider=implode(', ', $nameProviderArray);
        }
        

        if(empty($booking_status)){
            $booking_status=array("0"=>"1","1"=>"2","2"=>"0");
        }
        if(empty($type)){
            $type=array("0"=>"adopts","1"=>"fosters","2"=>"surrenders","3"=>"volunteers");
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $startdate = $request->startdate;
            $startdate = date_create($startdate);
            $book_startdate = date_format($startdate, 'Y-m-d');    
        }else{
            $startdate = "";
            $book_startdate ="";
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $enddate = $request->enddate;
            $enddate = date_create($enddate);
            $book_enddate = date_format($enddate, 'Y-m-d');
        }else{
            $book_enddate ="";
        } 
        $customer1 =array();
        foreach ($type as $key => $value) {
            if($value=="adopts"){
                $sub_table="adopt_pet";
                $sub_table_id="adopt_id";
            }
            if($value=="fosters"){
                $sub_table="foster_pet";
                $sub_table_id="foster_id";
            }
            if($value=="surrenders"){
                $sub_table="pet_surrender";
                $sub_table_id="surrender_id";
            }
            if($value=="volunteers"){
                $sub_table="pet_volunteer";
                $sub_table_id="volunteer_id";
            }
            $customersObj = Customer::
                select([$value.'.updated_at as replyDate',$value.'.created_at as requestDate',$value.'.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', $value.'.status',$value.'.type','pets.breed','pets.name as petName','providers.name as providerName'])
                ->leftjoin($value, 'customers.id', '=', $value.'.customer_id')
                ->leftjoin($sub_table, $sub_table.'.'.$sub_table_id, '=', $value.'.id')
                ->leftjoin('pets', $sub_table.'.pet_id', '=', 'pets.id')
                ->leftjoin('providers', $value.'.provider_id', '=', 'providers.id')
                ->whereIn($value.'.provider_id',$providers)
                ->where($value.'.sender',1)
                ->whereIn($value.'.status',$booking_status);

            if(isset($book_startdate)  && $book_startdate!="")
                $customersObj->where($value.'.created_at','>=',$book_startdate);
            if(isset($book_enddate)  && $book_enddate!="")
                $customersObj->where($value.'.created_at','<=',$book_enddate);

            $customersObj = $customersObj->get();
            
            $customers = json_decode(json_encode($customersObj), true);
            $customer1 = array_merge($customer1,$customers);
        }
        /*echo "<pre>";
        print_r($customer1);
        exit();*/
        $total_request=0;
        $total_approve=0;
        $total_reject=0;
        $total_pending=0;
        $adopts=0;
        $fosters=0;
        $surrenders=0;
        $volunteers=0;

        $adopts_approve=0;
        $adopts_reject=0;
        $adopts_pending=0;

        $fosters_approve=0;
        $fosters_reject=0;
        $fosters_pending=0;

        $surrenders_approve=0;
        $surrenders_reject=0;
        $surrenders_pending=0;

        $volunteers_approve=0;
        $volunteers_reject=0;
        $volunteers_pending=0;
        foreach ($customer1 as $key => $value) {

            $customers_name=$value['firstname']." ".$value['lastname'];
            if($value['breed']==1)
                $breed="Cat";
            else
                $breed="Dog";

            if($value['status']==1){
                $status="Approved";
                $total_approve++;        
            }
            if($value['status']==2){
                $status="Rejected";
                $total_reject++;        
            }
            if($value['status']==0){
                $status="Pending";
                $total_pending++;
            }
            $type="";
            if($value['type']=="adopts"){
                $adopts++;
                if($value['status']==1){
                    $adopts_approve++;        
                }
                if($value['status']==2){
                    $adopts_reject++;        
                }
                if($value['status']==0){
                    $adopts_pending++;        
                }
                $type="Adopt";
            }
            if($value['type']=="fosters"){
                $fosters++;        
                if($value['status']==1){
                    $fosters_approve++;        
                }
                if($value['status']==2){
                    $fosters_reject++;        
                }
                if($value['status']==0){
                    $fosters_pending++;        
                }
                $type="Foster";
            }
            if($value['type']=="surrenders"){
                $surrenders++;        
                if($value['status']==1){
                    $surrenders_approve++;        
                }
                if($value['status']==2){
                    $surrenders_reject++;        
                }
                if($value['status']==0){
                    $surrenders_pending++;        
                }
                $type="Surrender";
            }
            if($value['type']=="volunteers"){
                $volunteers++;
                if($value['status']==1){
                    $volunteers_approve++;        
                }
                if($value['status']==2){
                    $volunteers_reject++;        
                }
                if($value['status']==0){
                    $volunteers_pending++;        
                }
                $type="Volunteer";
            }
            $total_request++;

            $data[] = array(
                    $value['id'],
                    $value['petName'],
                    $breed,
                    $customers_name,                    
                    $type,
                    $status,
                    $value['providerName'],
                    $value['requestDate'],
                    $value['replyDate'],
                );
        }
        
        if($request->startdate!="" && $request->enddate!="")
             $reportDate = $request->startdate." to ".$request->enddate;
        else
             $reportDate = "All";

        $pet = Pet::whereIN('provider_id', $providers)->whereIN('pet_status', ['Draft','Available','Foster'])->count();

        /*echo "Total pet:".$pet."<br>";*/
        $petDraft = Pet::whereIn('provider_id', $providers)->where('pet_status', 'Draft')->count();
        /*$draft_dog= Pet::whereIn(['provider_id'=>$providers,'breed'=>2])->whereIN('pet_status',['Draft'])->count();*/
        $draft_dog= Pet::whereIn('provider_id',$providers)->where('breed',2)->whereIN('pet_status',['Draft'])->count();
        $draft_cat= Pet::whereIN('provider_id',$providers)->where('breed',1)->whereIN('pet_status',['Draft'])->count();
        
        if(isset($petDraft) && $petDraft!=0){
            $draft_dog_percent=$draft_dog*100/$petDraft;
            $draft_cat_percent=$draft_cat*100/$petDraft;
        }else{
            $draft_dog_percent=0;
            $draft_cat_percent=0;
        }
        /*echo "draft dog pet:".$draft_dog."<br>";
        echo "draft cat pet:".$draft_cat."<br>";
        echo "draft pet:".$petDraft."<br>";*/
        $petAvailable = Pet::whereIn('provider_id', $providers)->where('pet_status', 'Available')->count();
        $available_dog= Pet::whereIn('provider_id',$providers)->where('breed',2)->whereIN('pet_status',['Available'])->count();
        $available_cat= Pet::whereIn('provider_id',$providers)->where('breed',1)->whereIN('pet_status',['Available'])->count();
        if(isset($petAvailable) && $petAvailable!=0){
            $available_dog_percent=$available_dog*100/$petAvailable;
            $available_cat_percent=$available_cat*100/$petAvailable;
        }else{
            $available_dog_percent=0;
            $available_cat_percent=0;
        }
        /*echo "available dog pet:".$available_dog."<br>";
        echo "available_cat cat pet:".$available_cat."<br>";
        echo "Available pet:".$petAvailable."<br>";*/
        $petFoster = Pet::whereIn('provider_id', $providers)->where('pet_status', 'Foster')->count();
        $foster_dog= Pet::whereIn('provider_id',$providers)->where('breed',2)->whereIN('pet_status',['Foster'])->count();
        $foster_cat= Pet::whereIn('provider_id',$providers)->where('breed',1)->whereIN('pet_status',['Foster'])->count();
        if(isset($petFoster) && $petFoster!=0){
            $foster_dog_percent=$foster_dog*100/$petFoster;
            $foster_cat_percent=$foster_cat*100/$petFoster;
        }else{
            $foster_dog_percent=0;
            $foster_cat_percent=0;
        }
        /*echo "fosters dog pet:".$foster_dog."<br>";
        echo "fosters cat pet:".$foster_cat."<br>";
        echo "fosters pet:".$petFoster."<br>";*/
        $pet_dog= Pet::whereIn('provider_id',$providers)->where('breed',2)->whereIN('pet_status',['Draft','Foster','Available'])->count();
        /*echo "dog pet:".$pet_dog."<br>";*/
        $pet_cat= Pet::whereIn('provider_id',$providers)->where('breed',1)->whereIN('pet_status',['Draft','Foster','Available'])->count();
        /*echo "dog pet:".$pet_cat."<br>";*/
        if(isset($pet) && $pet!=0){
            $petDraft_percent=$petDraft*100/$pet;
            $petAvailable_percent=$petAvailable*100/$pet;
            $petFoster_percent=$petFoster*100/$pet;
            $pet_dog_percent=$pet_dog*100/$pet;
            $pet_cat_percent=$pet_cat*100/$pet;
        }else{
            $petDraft_percent=0;
            $petAvailable_percent=0;
            $petFoster_percent=0;
            $pet_dog_percent=0;
            $pet_cat_percent=0;
        }
        if(isset($total_request) && $total_request!=0){
            $adopts_percent=$adopts*100/$total_request;
            $fosters_percent=$fosters*100/$total_request;
            $volunteers_percent=$volunteers*100/$total_request;
            $surrenders_percent=$surrenders*100/$total_request;

            $total_approve_percent=$total_approve*100/$total_request;
            $total_reject_percent=$total_reject*100/$total_request;
            $total_pending_percent=$total_pending*100/$total_request;
        }else{
            $adopts_percent=0;
            $fosters_percent=0;
            $volunteers_percent=0;
            $surrenders_percent=0;

            $total_approve_percent=0;
            $total_reject_percent=0;
            $total_pending_percent=0;
        }
        if(isset($total_approve) && $total_approve!=0){
            $adopts_approve_percent = $adopts_approve*100/$total_approve;
            $fosters_approve_percent = $fosters_approve*100/$total_approve;
            $surrenders_approve_percent = $surrenders_approve*100/$total_approve;
            $volunteers_approve_percent = $volunteers_approve*100/$total_approve;
        }else{
            $adopts_approve_percent = 0;
            $fosters_approve_percent = 0;
            $surrenders_approve_percent = 0;
            $volunteers_approve_percent = 0;
        }
        $adopts_approve_percent = round($adopts_approve_percent,2)." %" ;
        $fosters_approve_percent = round($fosters_approve_percent,2)." %" ;
        $surrenders_approve_percent = round($surrenders_approve_percent,2)." %" ;
        $volunteers_approve_percent = round($volunteers_approve_percent,2)." %" ;

        if(isset($total_pending) && $total_pending!=0){
            $adopts_pending_percent = $adopts_pending*100/$total_pending;
            $fosters_pending_percent = $fosters_pending*100/$total_pending;
            $surrenders_pending_percent = $surrenders_pending*100/$total_pending;
            $volunteers_pending_percent = $volunteers_pending*100/$total_pending;
        }else{
            $adopts_pending_percent = 0;
            $fosters_pending_percent = 0;
            $surrenders_pending_percent = 0;
            $volunteers_pending_percent = 0;
        }
        $adopts_pending_percent = round($adopts_pending_percent,2)." %" ;
        $fosters_pending_percent = round($fosters_pending_percent,2)." %" ;
        $surrenders_pending_percent = round($surrenders_pending_percent,2)." %" ;
        $volunteers_pending_percent = round($volunteers_pending_percent,2)." %" ;

        if(isset($total_reject) && $total_reject!=0){
            $adopts_reject_percent = $adopts_reject*100/$total_reject;
            $fosters_reject_percent = $fosters_reject*100/$total_reject;
            $surrenders_reject_percent = $surrenders_reject*100/$total_reject;
            $volunteers_reject_percent = $volunteers_reject*100/$total_reject;
        }else{
            $adopts_reject_percent = 0;
            $fosters_reject_percent = 0;
            $surrenders_reject_percent = 0;
            $volunteers_reject_percent = 0;
        }
        $adopts_reject_percent = round($adopts_reject_percent,2)." %" ;
        $fosters_reject_percent = round($fosters_reject_percent,2)." %" ;
        $surrenders_reject_percent = round($surrenders_reject_percent,2)." %" ;
        $volunteers_reject_percent = round($volunteers_reject_percent,2)." %" ;

        $total_approve_percent = round($total_approve_percent,2)." %" ;
        $total_reject_percent = round($total_reject_percent,2)." %" ;
        $total_pending_percent = round($total_pending_percent,2)." %" ;

        $adopts_percent = round($adopts_percent,2)." %" ;
        $fosters_percent = round($fosters_percent,2)." %" ;
        $volunteers_percent = round($volunteers_percent,2)." %" ;
        $surrenders_percent = round($surrenders_percent,2)." %" ;

        $petDraft_percent = round($petDraft_percent,2)." %" ;
        $petAvailable_percent = round($petAvailable_percent,2)." %" ;
        $petFoster_percent = round($petFoster_percent,2)." %" ;
        $pet_dog_percent = round($pet_dog_percent,2)." %" ;
        $pet_cat_percent = round($pet_cat_percent,2)." %" ;
        $foster_dog_percent = round($foster_dog_percent,2)." %" ;
        $foster_cat_percent = round($foster_cat_percent,2)." %" ;
        $available_dog_percent = round($available_dog_percent,2)." %" ;
        $available_cat_percent = round($available_cat_percent,2)." %" ;
        $draft_dog_percent = round($draft_dog_percent,2)." %" ;
        $draft_cat_percent = round($draft_cat_percent,2)." %" ;


        if(!empty($data)){
            $items = $data;
            Excel::create('Society-report', function($excel) use($items,$nameProvider,$reportDate,$pet,$petDraft,$petAvailable,$petFoster,$pet_cat,$pet_dog,$petAvailable_percent,$petDraft_percent,$petFoster_percent,$pet_cat_percent,$pet_dog_percent,$draft_cat,$draft_dog,$available_dog,$available_cat,$foster_dog,$foster_cat,$foster_dog_percent,$foster_cat_percent,$available_cat_percent,$available_dog_percent,$draft_dog_percent,$draft_cat_percent,$total_request,$total_approve,$total_pending,$total_reject,$adopts,$volunteers,$fosters,$surrenders,$adopts_approve,$volunteers_approve,$fosters_approve,$surrenders_approve,$adopts_pending,$volunteers_pending,$fosters_pending,$surrenders_pending,$fosters_reject,$surrenders_reject,$volunteers_reject,$adopts_reject,$adopts_percent,$volunteers_percent,$fosters_percent,$surrenders_percent,$total_approve_percent,$total_pending_percent,$total_reject_percent,$adopts_approve_percent,$volunteers_approve_percent,$fosters_approve_percent,$surrenders_approve_percent,$adopts_pending_percent,$volunteers_pending_percent,$fosters_pending_percent,$surrenders_pending_percent,$adopts_reject_percent,$volunteers_reject_percent,$fosters_reject_percent,$surrenders_reject_percent) {
                $excel->sheet('Society-report', function($sheet) use($items,$nameProvider,$reportDate,$pet,$petDraft,$petAvailable,$petFoster,$pet_cat,$pet_dog,$petAvailable_percent,$petDraft_percent,$petFoster_percent,$pet_cat_percent,$pet_dog_percent,$draft_cat,$draft_dog,$available_dog,$available_cat,$foster_dog,$foster_cat,$foster_dog_percent,$foster_cat_percent,$available_cat_percent,$available_dog_percent,$draft_dog_percent,$draft_cat_percent,$total_request,$total_approve,$total_pending,$total_reject,$adopts,$volunteers,$fosters,$surrenders,$adopts_approve,$volunteers_approve,$fosters_approve,$surrenders_approve,$adopts_pending,$volunteers_pending,$fosters_pending,$surrenders_pending,$fosters_reject,$surrenders_reject,$volunteers_reject,$adopts_reject,$adopts_percent,$volunteers_percent,$fosters_percent,$surrenders_percent,$total_approve_percent,$total_pending_percent,$total_reject_percent,$adopts_approve_percent,$volunteers_approve_percent,$fosters_approve_percent,$surrenders_approve_percent,$adopts_pending_percent,$volunteers_pending_percent,$fosters_pending_percent,$surrenders_pending_percent,$adopts_reject_percent,$volunteers_reject_percent,$fosters_reject_percent,$surrenders_reject_percent) {
                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('smallLogo.png')); //your image path
                    $objDrawing->setCoordinates('A1');
                    $objDrawing->setWorksheet($sheet);
                    $sheet->cells('D1:D1', function($cells) {
                        $cells->setFontColor('#01aef2');
                        });
                    $sheet->cells('D2:D2', function($cells) {
                        $cells->setFontColor('#ff33cc');
                        });
                    $sheet->cells('A5:G5', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                        });
                    $sheet->cells('A11:I11', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                        });
                    $sheet->cells('A18:I18', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                         /*$cells->setAlignment('center');
                        $cells->setValignment('center');
                         $cells->setFontSize(16);*/
    
                        });
                     $sheet->fromArray(array(
                        array('','','Society Performance Report',$nameProvider),
                        array('','','Date',$reportDate),
                        array('','','','','',''),
                        array('','','','','',''),
                        array('','','','Cats','','Dogs',''),
                        array('Total Pets',"$pet",'',"$pet_cat","$pet_cat_percent","$pet_dog","$pet_dog_percent"),
                        array('Draft',"$petDraft","$petDraft_percent","$draft_cat","$draft_cat_percent","$draft_dog","$draft_dog_percent"),
                        array('Available',"$petAvailable","$petAvailable_percent","$available_cat","$available_cat_percent","$available_dog","$available_dog_percent"),
                        array('Foster',"$petFoster","$petFoster_percent","$foster_cat","$foster_cat_percent","$foster_dog","$foster_dog_percent"),
                        array('','','','','',''),
                        array('','','','Approved','','Pending','','Rejected',''),
                        array('Total Request',"$total_request","","$total_approve","$total_approve_percent","$total_pending","$total_pending_percent","$total_reject","$total_reject_percent"),
                        array('Adopt',"$adopts","$adopts_percent","$adopts_approve","$adopts_approve_percent","$adopts_pending","$adopts_pending_percent","$adopts_reject","$adopts_reject_percent"),
                        array('Volunteer',"$volunteers","$volunteers_percent","$volunteers_approve","$volunteers_approve_percent","$volunteers_pending","$volunteers_pending_percent","$volunteers_reject","$volunteers_reject_percent"),
                        array('Foster',"$fosters","$fosters_percent","$fosters_approve","$fosters_approve_percent","$fosters_pending","$fosters_pending_percent","$fosters_reject","$fosters_reject_percent"),
                        array('Surrender',"$surrenders","$surrenders_percent","$surrenders_approve","$surrenders_approve_percent","$surrenders_pending","$surrenders_pending_percent","$surrenders_reject","$surrenders_reject_percent"),
                        array('','','','','',''),
                        array('Request Number','Pet Name','Pet Species','Human Name','Request Type','Request Status','Society Name', 'Date of Request','Date of Reply')
                    ), null, 'A1', false, false);
                    $sheet->fromArray($items, null, 'A1', false, false);
                });
            })->export('xls');
        }else{
            return back()->with('message', 'No data found.');;
        }
    }
}
