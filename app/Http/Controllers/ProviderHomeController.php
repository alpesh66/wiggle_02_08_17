<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Kennel;
use App\Customer;
use App\Booking;
use App\Store;
use App\Technician;
use App\Rating;
use Mail;
use Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use Session;
use DB;

class ProviderHomeController extends Controller
{
    public function index()
    {
        $forDroptechnicians  = \App\Technician::has('service')->ActiveTechnicianList();
        $country = \App\Country::pluck('nicename','phonecode');
        $area = \App\Area::pluck('name','id');
        $size = \App\Size::pluck('name','id');
        $breed = \App\Breed::pluck('name','id');
        if(! $forDroptechnicians->count() > 0) 
        {
            Session::flash('flash_message', 'You might not have any Technician else assign them services');
            return redirect('providers/technicians');
        }
        $technicians  = $this->getTechnicianslists($forDroptechnicians);
        return view('servicepro.vet', compact('technicians','forDroptechnicians','country','area', 'size', 'breed'));
    }

    public function dashboard()
    {
        $provider_id = Session::get('provider_id');
        //echo "$provider_id";
        $customers = Booking::select('customer_id')->where('provider_id', $provider_id)->groupBy('customer_id')->get();
        $customers = ($customers->count());
	$lastThirtyday = Booking::select('customer_id')->where('provider_id', $provider_id)->where('created_at', '>=', Carbon::now()->subDays(30))->groupBy('customer_id')->get();
	$lastThirtyday = ($lastThirtyday->count());
        //$LastThirtyDay = DB::table('customers')->where('provider_id', $provider_id)->where('created_at', '>=', Carbon::now()->subDays(30))->get();
       // $bookings = Booking::where('provider_id', $provider_id)->count();
        $bookings = DB::table('bookings')
                            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->join('transactions', 'transactions.id', '=', 'bookings.transaction_id')
                            ->where('bookings.provider_id', session('provider_id'))
                            ->select(['customers.id as cid'])
                            ->groupBy('bookings.ontime')->get();
        $bookings=count($bookings);
        $previous = DB::table('bookings')
                            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->join('transactions', 'transactions.id', '=', 'bookings.transaction_id')
                            ->where('bookings.provider_id', session('provider_id'))
                            ->select(['customers.id as cid'])
                            ->groupBy('bookings.ontime')->where('end','<',Carbon::now())->get();
        $previous=count($previous);
        $upcoming = DB::table('bookings')
                            ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->join('transactions', 'transactions.id', '=', 'bookings.transaction_id')
                            ->where('bookings.provider_id', session('provider_id'))
                            ->select(['customers.id as cid'])
                            ->groupBy('bookings.ontime')->where('end','>',Carbon::now())->get();
        $upcoming=count($upcoming);


        //$previous= DB::table('bookings')->where('provider_id', $provider_id)->where('end','<',Carbon::now())->select('start','end')->count();
        //$upcoming= DB::table('bookings')->where('provider_id', $provider_id)->where('end','>',Carbon::now())->select('start','end')->count();
       
	//$booking_LastThirtyDays = DB::table('bookings')->where('provider_id', $provider_id)->where('created_at', '>=', Carbon::now()->subDays(30))->groupBy('ontime')->get();
    $last30 = DB::table('bookings')
                 ->select('ontime', DB::raw('count(*) as total'))
                 ->where('provider_id', $provider_id)
                 ->where('created_at', '>=', Carbon::now()->subDays(30))
                 ->groupBy('ontime')
                 ->get();
    $booking_LastThirtyDays=count($last30);
    $last30app = DB::table('bookings')
                 ->select('ontime', DB::raw('count(*) as total'))
                 ->where('provider_id', $provider_id)
                 ->where('madefrom','0')
                 ->where('created_at', '>=', Carbon::now()->subDays(30))
                 ->groupBy('ontime')
                 ->get();
    $booking_by_app=count($last30app);
	//$booking_by_app=DB::table('bookings')->where('provider_id', $provider_id)->where('madefrom','1')->where('created_at', '>=', Carbon::now()->subDays(30))->groupBy('ontime')->count();
    $last30web = DB::table('bookings')
                 ->select('ontime', DB::raw('count(*) as total'))
                 ->where('provider_id', $provider_id)
                 ->where('madefrom','1')
                 ->where('created_at', '>=', Carbon::now()->subDays(30))
                 ->groupBy('ontime')
                 ->get();
    $booking_by_web=count($last30web);
	//$booking_by_web=DB::table('bookings')->where('provider_id', $provider_id)->where('madefrom','0')->where('created_at', '>=', Carbon::now()->subDays(30))->groupBy('ontime')->count();
	 //echo "<pre>";echo "lastbooking30:".$booking_LastThirtyDays."<br>"."app".$booking_by_app."<br>".$booking_by_web;  die();
        $service_count = \App\Service::where('provider_id', $provider_id)->count();
	//dd($service_count);
        $tech_count = \App\Technician::where('provider_id', $provider_id)->count();
	//echo "<pre>"; print_r($tech_count );
        $tech_list = Technician::where('provider_id', $provider_id)->orderBy('id','desc')->take(5)->get();
        $customers_list = DB::table('customers')
                    ->join('bookings', 'bookings.customer_id', '=', 'customers.id')
                    ->where('bookings.provider_id', $provider_id)
                    ->select(['customers.firstname','customers.lastname','customers.email','customers.created_at'])->take(5)->groupBy('customer_id')->get();
	//$category[]=array();
	$service=DB::table('bookings')	
		->where('provider_id', $provider_id)		
		->selectRaw('id,service_id, count(*) as count')
		//->join('services','services.id','=','bookings.service_id')
		//->join('categories','categories.id','=','services.category_id')	
		->orderBy('count','DESC')
		->take(5)
	        ->groupBy('service_id')
	        ->get();
	foreach($service as $key => $val)
	{
		$catname=DB::table('services')
		->where('services.id',$val->service_id)
		->join('categories','services.category_id','=','categories.id')
		->select('services.id','categories.name')
		//->where('service_id', $val->service_id)
		->get();
        if(!empty($catname)){
            $category[$key]['count']=$val->count;
            $category[$key]['name']=$catname[0]->name;              
        }
	}
	
	
	//$cat=$category;
	//$cat);
	//echo "<pre>"; print_r($category); die();

        $rating_list = Rating::with('customer')->where('provider_id', $provider_id)->take(1)->get();            
        $provider_type_count=\DB::table('provider_type')->where('provider_id',Session::get('provider_id'))->where('type_id', 5)->count();
        if($provider_type_count>0){
            $totalKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('sender',1)->count();
            $approveKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '1')->where('sender',1)->count();
            $rejectedKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '2')->where('sender',1)->count();        
            $pendingKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '0')->where('sender',1)->count();        
        }else{
            $totalKennel = 0;
            $approveKennel = 0;
            $rejectedKennel = 0;        
            $pendingKennel = 0;

        }
        
        return view('servicepro.dashboard',compact('customers','bookings','service_count','tech_list','customers_list','rating_list','tech_count','previous','upcoming','lastThirtyday','booking_LastThirtyDays','booking_by_app','booking_by_web','category','provider_type_count','totalKennel','approveKennel','rejectedKennel','pendingKennel'));}

    public function getTechnicianslists($technicians)
    {
        $finalone = [];
        foreach ($technicians->all() as $key => $value) {
            $oneitem['id'] = $key;
            $oneitem['name'] = $value;
            // $finalone[] = $oneitem;
            array_push($finalone, $oneitem);
        }
        return json_encode($finalone);
    }

    public function getbreed(Request $request)
    {
        $cat_id = $request->input('service');
        $service_id = \App\Service::select('id')->whereCategoryId($cat_id)->first();

        // $service = \DB::table('breed_service')->where('service_id', '=', $service_id['id'])->get();
        $service = \App\BreedService::where('service_id', $service_id['id'])->get();
        // debug($service); return ;
        // $serv = collect($service);
        $get_breed = $service->groupBy('breed_id');
        $newHtml = '';
       // return $breed_names = \App\Breed::whereIn('id',$service)->pluck('name','id');
        $count_breeds = $service->count();
        $data['count'] = $count_breeds;

        foreach ($get_breed->all() as $key => $val) {
            $breed_naam = ($key == 1) ? 'Cat' : 'Dog';
            $newHtml .= '<option value='.$key.'>'.$breed_naam.'</option>';

                $size_html = '';
                foreach ($val as $value) {
                    $size_html .= '<option data-service='.$value['servicetime'].' data-price='.$value['price'].'>'.$value['size_id'].'</option>';
                continue;
                }

                $data['size'] = $size_html;

            $data['breed'] = $newHtml;
        }
        return json_encode($data);
    }

    public function getsize(Request $request)
    {
        $size    = $request->input('breed');
        $cat_id = $request->input('service');

        $service_id = \App\Service::select('id')->whereCategoryId($cat_id)->first();
        $service = \App\BreedService::where('service_id', $service_id['id'])->where('breed_id', $size)->get();

        $size_html  = '';
        foreach ($service->all() as $value) {
            $size_html .= '<option data-service='.$value['servicetime'].' data-price='.$value['price'].'>'.$value['size_id'].'</option>';
        }
        return json_encode($size_html);
    }

    public function CustomerData(Request $request)
    {
        $customer = \App\Customer::with('pet')->where('email', $request->input('email'))->first();

        foreach ($customer->pet as $value) {
            $allpets[] = '<option value='.$value['id'].'> '. $value['name'] .'</option>';
        }
        $customer['allpets'] = $allpets;
        return $customer;
    }

    public function onlyCalendarView()
    {
//        \DB::enableQueryLog();
        $techs = explode(',', \Auth::user()->technicians);
        $forDroptechnicians  = \App\Technician::OnlyCalendarTechnicians($techs);
//        dump(\DB::getQueryLog());
//        dd($forDroptechnicians);
        $country = \App\Country::pluck('nicename','phonecode');
        $area = \App\Area::pluck('name','id');
        $size = \App\Size::pluck('name','id');
        $breed = \App\Breed::pluck('name','id');
        if(! $forDroptechnicians->count() > 0)
        {
            Session::flash('flash_message', 'Invalid technician or Techinician is inactive');
            return redirect('tech/booking');
        }
        $technicians  = $this->getTechnicianslists($forDroptechnicians);
        return view('servicepro.onlycalendar', compact('technicians','forDroptechnicians','country','area', 'size', 'breed'));
    }
}
