<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ProviderRequest;
use App\Http\Controllers\Controller;

use App\Provider;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Type;
use App\User;
use Datatables;
use Image;
use DB;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return $this->callDatatables($request);
        }
        // $provider = Provider::with('area')->paginate(15);
        return view('provider.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function callDatatables($request)
    {
        // $customers = Customer::paginate(15);
        // $customer = Customer::SearchPet();
        $provider = DB::table('providers')
                ->join('users', function($join) {
                    $join->on('providers.id', '=', 'users.provider_id')
                        ->where('users.utype', '=', 1);
                })
                ->select(['providers.id', 'providers.name', 'providers.contact','providers.email', 'providers.about', 'providers.ptype','users.name as uname','users.email as uemail','users.mobile as umobile', 'users.id as uid'])
                ->where('providers.id','!=',5000);

        $datatables =  app('datatables')->of($provider)
            ->addColumn('action', function($provider){
                return view('provider.action', compact('provider'))->render();
            })
            // ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('ptype', '@if($ptype == 1) Provider @else Society @endif')
            ->editColumn('about', function($provider){
                return str_limit($provider->about, 55);
            });
        
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);            
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {        
        $providers = Type::ofType(1);
        $societies = Type::ofType(2);
        $country = \App\Country::pluck('nicename','phonecode');
        $area = \App\Area::pluck('name','id');
        
        return view('provider.create', compact('providers','country','societies', 'area'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(ProviderRequest $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',            
        ]);   
        // dd($request->all());
        // $status = ($request->input('status') == 'on')? '1' :'0' ;
        // $email  = $request->input('email');
        // $passw  = bcrypt($request->input('password'));
        // $name   = $request->input('name');
        // $provider_data = array(
        //     'name'      => $name,
        //     'about'     => $request->input('about'),
        //     'address'   => $request->input('address'),
        //     'area'      => $request->input('area'),
        //     'start'     => $request->input('start'),
        //     'email'     => $email,
        //     'end'       => $request->input('end'),
        //     'latitude'  => $request->input('latitude'),
        //     'longitude' => $request->input('longitude'),
        //     'contact'   => $request->input('country').'-'.$request->input('contact'),
        //     'status'    => $status,
        // );
        $check_ptype = $request->input('ptype');
        $provider_data          = $request->except('users');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $provider_data['status'] = $status;

        if ($request->hasFile('photo') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $provider_data['photo'] = $photo_englist;

        }

        if ($request->hasFile('icon') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('icon')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('icon')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $provider_data['icon'] = $photo_englist;

        }
        // dd($provider_data); exit;

        $provider  = Provider::create($provider_data);
        $provider->contact = $request->input('country').'-'.$request->input('contact');
        $provider->save();
        $users                  = $request->only('users');
        $users['users']['provider_id']   = $provider->id;
        $users['users']['utype']         = 1;  // utype 1: means Super Admin
        $users                  = $users['users'];
        $pr_user = $user->from_provider($users);
        if($check_ptype == 1)
        {
            $data_type1 = $request->only('type_1');
            $provider->type()->attach($data_type1['type_1']);
            
            $add_role = new \App\Role;
            $add_role->name = str_random(30).'provideradmin';
            $add_role->display_name = 'Super Admin';
            $add_role->provider_id = $provider->id;
            $add_role->description = 'Manage Providers';
            $add_role->save();
            $add_role->attachPermissions(providerAdmin_permissions());
            $pr_user->attachRole($add_role);

            //pro timings
            $weekdays = weekdays();
            foreach($weekdays as $w => $day){
                $pro['provider_id'] = $provider->id;
                $pro['weekday'] = $day;
                $pro['starttime'] = '08:00';
                $pro['endtime'] = '23:59';
                $pro['created_at'] = date('Y-m-d H:i:s');
                $pro['updated_at'] = date('Y-m-d H:i:s');
                $pro_time_data[] = $pro;            
            }
            $pro_timings = \DB::table('provider_timings')->insert($pro_time_data); 
        }
        else if($check_ptype == 2)
        {
            $data_type2 = $request->only('type_2');
            $provider->type()->attach($data_type2['type_2']);
            
            $add_role = new \App\Role;
            $add_role->name = str_random(30).'societyadmin';
            $add_role->display_name = 'Super Admin';
            $add_role->provider_id = $provider->id;
            $add_role->description = 'Manage Soocieties';
            $add_role->save();
            $add_role->attachPermissions(societyAdmin_permissions());
            $pr_user->attachRole($add_role);
        } 

        Session::flash('flash_message', 'Provider added!');

        return redirect('provider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $provider = Provider::with('areas')->findOrFail($id);

        return view('provider.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $provider = Provider::findOrFail($id);
        if($provider['ptype'] == 'Provider'){
            $type = Type::ofType(1);
        } else {
            $type = Type::ofType(2);
        }
        $selected_types = implode(',', array_pluck($provider->type, 'id'));
        $country_code = countryCode_explode($provider['contact']);
        $area = \App\Area::pluck('name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        

        return view('provider.edit', compact('provider','type','country','area','selected_types','country_code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',            
            'address' => 'required'
        ]); 
        $provider = Provider::findOrFail($id);
        $request['contact'] = $request->input('country').'-'.$request->input('contact');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;   
        // dd ($request->image);  exit;
        $data = $request->all();
        if ($request->hasFile('photo') ) {


            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['photo'] = $photo_englist;
                         
        } 

        if ($request->hasFile('icon') ) {


            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('icon')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('icon')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['icon'] = $photo_englist;
                         
        } 
        //dd($data);exit;
        $provider->update($data);
        
        // echo '<pre>'; print_r($request->hasFile('image')); exit;        
        $provider->type()->sync($request->input('type'));
        Session::flash('flash_message', 'Provider updated!');

        return redirect('provider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->id; 
        if(!empty($ids) && is_array($ids)){
            foreach ($ids as $key => $id) {
                $isCustomer = Provider::destroy($id);
                \App\User::where('provider_id', $id)->delete();
                \App\Role::where('provider_id', $id)->delete();

                if($isCustomer)
                {
                    $data['success'] = true;
                    $data['message'] = 'Provider deleted successfully';    
                } else {
                    $data['success'] = false;
                    $data['message'] = 'Some internal error occurred';
                }
            }
            return $data;
        }else{
            $isCustomer = Provider::destroy($ids);
            \App\User::where('provider_id', $ids)->delete();
            \App\Role::where('provider_id', $ids)->delete();

            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Provider deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
             return $data;
        } 
        Session::flash('flash_message', 'Provider deleted!');
        return redirect('provider');
    }

    public function typeoftype($id)
    {
        if(is_numeric($id))
        {
            $got_types = Type::ofType($id);
            $data = [];
            foreach ($got_types as $key => $value) {
                // $genHtml .= '<option value='.$key.'>'.$value.'</option>';
                $protype['id'] = $key;
                $protype['text'] = $value;
                $data[] = $protype;
            }
            return response()->json($data);
        }
        return false;
    }

    public function profileproviders()
    {
        $id = session('provider_id');
        $provider = Provider::findOrFail($id);
        $type = \App\Type::pluck('dis_name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        $country_code = explode('-', $provider['contact']);
        $area = \App\Area::pluck('name','id');
        /*echo "<pre>";
        print_r($provider);
        exit();*/

        return view('servicepro.profile.edit', compact('provider','type','area', 'country','country_code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function updateproviders(Request $request)
    {
        $id = session('provider_id');
        $provider = Provider::findOrFail($id);
        $request['contact'] = $request->input('country').'-'.$request->input('contact');
        $data = $request->all();
// dd($request->all());
        if ($request->hasFile('photo') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['photo'] = $photo_englist;
               
        }

        if ($request->hasFile('icon') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis").'icon';
            $file_extention = '.'.$request->file('icon')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('icon')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['icon'] = $photo_englist;
               
        }


        $provider->update($data);

        Session::flash('flash_message', 'Provider updated!');

        return redirect('providers/profile');
    }
    public function setServiceType(Request $request)
    {
        $response = array('status' => 'fail','msg' => 'Something wrong');

        $id = session('provider_id');
        $service_type = $request->input('radio');
        if(isset($service_type) && isset($id)){
        /*    $provider = Provider::findOrFail($id);
            $data['service_type'] = $service_type;    
            $result = $provider->update($data);*/
            $result = DB::table('providers')->where('id',$id)->update(array('service_type' => $service_type));
            $response = array('status' => 'success','msg' => 'Service type set successfully');
        }
        return \Response::json($response);
    
    }
    public function getServiceType(Request $request)
    {
        $response = array('status' => 'fail','msg' => 'Something wrong');
        $id = session('provider_id');        
        if(isset($id)){
            $provider = DB::table('providers')->select('service_type')->where('id',$id)->first();
            $response = array('status' => 'success','msg' => 'Service get successfully','service_type' => $provider->service_type);
        }
        else{
            $response = array('status' => 'success','msg' => 'Service get successfully','service_type' => 0);
        }
        return \Response::json($response);
    
    }
}
