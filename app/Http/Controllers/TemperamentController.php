<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Temperaments;
use Session;
use Datatables;
use DB;

class TemperamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('temperament.index');
    }
    public function callDatatables()
    {
        $temperament = Temperaments::select(['id', 'name']);
        
        $datatables =  app('datatables')->of($temperament)

            ->addColumn('action', function($temperament) {
                return view('temperament.action', compact('temperament'))->render();
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('temperament.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Temperaments::create($data);
        Session::flash('flash_message', 'New Temperament Added');
        return redirect('temperament');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $temperament = Temperaments::findOrFail($id);
        return view('temperament.show', compact('temperament'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $temperament = Temperaments::findOrFail($id);
        return view('temperament.edit',compact('temperament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $temperament = Temperaments::findOrFail($id);
        $data = $request->all();
        $temperament->update($data);
        Session::flash('flash_message', 'Temperament Updated');
        return redirect('temperament');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->input('id');
        foreach ($ids as $key) {
            
            $check_role = DB::table('pet_temperament')->where('temperament_id',$key)->count();                
        
        
            if ($check_role == 0) {
                $role = Temperaments::find($key);
                $role->delete();
                $deleteChecked = 1;
                $data['success'] = true;
                $data['message'] = 'Temperament deleted successfully';
            } else {
                $deleteChecked = 0;
                $data['success'] = false;
                $data['message'] = 'Temperament still attached to Pets, so can\'t be deleted';
            }
        }
        if ($request->ajax() )
        {    
            return $data;
        } else {
            Session::flash('flash_message', 'Role deleted!');
            return redirect('role');            
        }
    }
}
