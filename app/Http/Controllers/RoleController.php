<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RoleRequest;
use App\Role;
use Session;
use Datatables;
use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $role = Role::where('provider_id',session('provider_id'))->paginate(15);
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('role.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables()
    {
        $role = Role::where('provider_id',session('provider_id'))->where('type', 0)->select(['id', 'display_name']);
        
        $datatables =  app('datatables')->of($role)

            ->addColumn('action', function($role) {
                return view('role.action', compact('role'))->render();
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perm = \App\Permission::where('ptype', config('callme.ADMIN_TYPE'))->pluck('display_name', 'id');
        return view('role.create', compact('perm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request, Role $role)
    {
        $saveRole = $request->except(['permission']);

        $saveRole['provider_id'] =  $request->session()->get('provider_id');
        $gotRole = $role->addmyRole($saveRole);
        $gotRole->attachPermissions($request->input('permission'));

        Session::flash('flash_message', 'New Role Added');

        return redirect('role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::with('perms')->findOrFail($id);

        return view('role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::with('perms')->findOrFail($id);
        $perm = \App\Permission::where('ptype', config('callme.ADMIN_TYPE'))->pluck('display_name', 'id');
        $selected_perms = $role->toArray();
        $selected_perms = implode(',',array_pluck($selected_perms['perms'],'id'));
        return view('role.edit', compact('role', 'perm','selected_perms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $saveRole = $request->except(['permission']);
        $role->update($saveRole);
        $role->savePermissions($request->input('permission'));

        Session::flash('flash_message', 'Role Updated');
        return redirect('role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->input('id');
        // $all_users = Role::with('users')->where('id', $ids)->get();
        foreach ($ids as $key) {
            
            $check_role = Role::has('users')->whereId($key)->get();                
        
        
            if ($check_role->count() == 0) {
                $role = Role::find($key);
                $role->perms()->sync([]);
                $role->delete();
                $deleteChecked = 1;
                $data['success'] = true;
                $data['message'] = 'Role deleted successfully';
            } else {
                $deleteChecked = 0;
                $data['success'] = false;
                $data['message'] = 'User still attached with this role, so can\'t be deleted';
            }
        }

        if ($request->ajax() )
        {    
            return $data;
        } else {
            Session::flash('flash_message', 'Role deleted!');
            return redirect('role');            
        }


    }
                                        //   PROVIDERS START 
    /**
     * Display a listing of PROVIDERS.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexroles(Request $request)
    {
        if($request->ajax())
        {
            $role = Role::where('provider_id',session('provider_id'))->where('type', 0)->select(['id', 'display_name']);
        
            $datatables =  app('datatables')->of($role)

                ->addColumn('action', function($role) {
                    return view('servicepro.role.action', compact('role'))->render();
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createroles()
    {
        $perm = \App\Permission::where('ptype', config('callme.PROVIDER_TYPE'))->pluck('display_name', 'id');
        return view('servicepro.role.create', compact('perm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeroles(RoleRequest $request, Role $role)
    {
        $saveRole = $request->except(['permission']);

        $saveRole['provider_id'] =  $request->session()->get('provider_id');
        $gotRole = $role->addmyRole($saveRole);
        $gotRole->attachPermissions($request->input('permission'));

        Session::flash('flash_message', 'New Role Added');

        return redirect('providers/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showroles($id)
    {
        $role = Role::with('perms')->findOrFail($id);

        return view('servicepro.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editroles($id)
    {
        $role = Role::with('perms')->findOrFail($id);
        $perm = \App\Permission::where('ptype', config('callme.PROVIDER_TYPE'))->pluck('display_name', 'id');
        $selected_perms = $role->toArray();
        $selected_perms = implode(',',array_pluck($selected_perms['perms'],'id'));
        return view('servicepro.role.edit', compact('role', 'perm','selected_perms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateroles(Request $request, Role $role)
    {
        $saveRole = $request->except(['permission']);
        $role->update($saveRole);
        $role->savePermissions($request->input('permission'));

        Session::flash('flash_message', 'Role Updated');
        return redirect('providers/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyroles(Request $request, $id)
    {
        $ids = $request->input('id');
        // $all_users = Role::with('users')->where('id', $ids)->get();
        foreach ($ids as $key) {
            
            $check_role = Role::has('users')->whereId($key)->get();                
        
        
            if ($check_role->count() == 0) {
                $role = Role::find($key);
                $role->perms()->sync([]);
                $role->delete();
                $deleteChecked = 1;
                $data['success'] = true;
                $data['message'] = 'Role deleted successfully';
            } else {
                $deleteChecked = 0;
                $data['success'] = false;
                $data['message'] = 'User still attached with this role, so can\'t be deleted';
            }
        }

        if ($request->ajax() )
        {
            return $data;
        } else {
            Session::flash('flash_message', 'Role deleted!');
            return redirect('providers/role');
        }

    }
                                            //   PROVIDERS ENDS

                                            //   SOCIETIES STARTS

    /**
     * Display a listing of PROVIDERS.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexsocieties(Request $request)
    {

        if($request->ajax())
        {
            $role = Role::where('provider_id',session('provider_id'))->select(['id', 'display_name']);
        
            $datatables =  app('datatables')->of($role)

                ->addColumn('action', function($role) {
                    return view('society.role.action', compact('role'))->render();
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('society.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createsocieties()
    {
        $perm = \App\Permission::where('ptype', config('callme.SOCIETY_TYPE'))->pluck('display_name', 'id');
        return view('society.role.create', compact('perm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storesocieties(RoleRequest $request, Role $role)
    {
        $saveRole = $request->except(['permission']);

        $saveRole['provider_id'] =  $request->session()->get('provider_id');
        $gotRole = $role->addmyRole($saveRole);
        $gotRole->attachPermissions($request->input('permission'));

        Session::flash('flash_message', 'New Role Added');

        return redirect('societies/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showsocieties($id)
    {
        $role = Role::with('perms')->findOrFail($id);

        return view('society.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editsocieties($id)
    {
        $role = Role::with('perms')->findOrFail($id);
        
        $perm = \App\Permission::where('ptype', config('callme.SOCIETY_TYPE'))->pluck('display_name', 'id');
        $selected_perms = $role->toArray();
        $selected_perms = implode(',',array_pluck($selected_perms['perms'],'id'));
        return view('society.role.edit', compact('role', 'perm','selected_perms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatesocieties(Request $request, Role $role)
    {
        $saveRole = $request->except(['permission']);
        $role->update($saveRole);
        $role->savePermissions($request->input('permission'));

        Session::flash('flash_message', 'Role Updated');
        return redirect('societies/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroysocieties(Request $request, $id)
    {
        $ids = $request->input('id');
        // $all_users = Role::with('users')->where('id', $ids)->get();
        foreach ($ids as $key) {
            
            $check_role = Role::has('users')->whereId($key)->get();                
        
        
            if ($check_role->count() == 0) {
                $role = Role::find($key);
                $role->perms()->sync([]);
                $role->delete();
                $deleteChecked = 1;
                $data['success'] = true;
                $data['message'] = 'Role deleted successfully';
            } else {
                $deleteChecked = 0;
                $data['success'] = false;
                $data['message'] = 'User still attached with this role, so can\'t be deleted';
            }
        }

        if ($request->ajax() )
        {
            return $data;
        } else {
            Session::flash('flash_message', 'Role deleted!');
            return redirect('societies/role');
        }

    }
}