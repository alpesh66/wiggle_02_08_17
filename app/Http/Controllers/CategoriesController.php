<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Datatables;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return $this->callDatatables($request);
        }
        $hello = Category::where('parentid', 0)->pluck('id', 'name');
        return view('categories.index', compact('hello'));
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function callDatatables($request)
    {
        // $customers = Customer::paginate(15);
        // $customer = Customer::SearchPet();
        $category = Category::ChildCat()->select(['id', 'name','name_er','status']);

        $datatables =  app('datatables')->of($category)

            ->addColumn('action', function($category) {
                return view('categories.action', compact('category'))->render();
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif');

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        // $categories = Category::where('parentid', 0)->with('subcategory')->get();
        $categories = Category::ActiveParentListing()->pluck('name', 'id');
        // $categories->prepend('Parent', 0);
        // dd($categories);
        return view('categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(StoreCategoryRequest $request)
    {
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        $data = $request->all();
        if ($request->hasFile('image') || $request->hasFile('image_ar')) {
            
            $destinationPath = public_path('uploads/categories');
            $photoname = date("YmdHis");
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                // $data = $request->except(['image']);
                $data['image'] = $photo_englist;
            }

            if($request->hasFile('image_ar'))
            {
                $file_extention_ar = '.'.$request->file('image_ar')->getClientOriginalExtension();
                $photo_arabic = $photoname.'_ar'.$file_extention_ar;
                $file_check = $request->file('image_ar')->move($destinationPath, $photo_arabic);
                // $data = $request->except(['image_ar']);
                $data['image_ar'] = $photo_arabic;
            }

            $adsdata = Category::create($data);
        }else {            
            $adsdata = Category::create($request->all());
        }

        Session::flash('flash_message', 'Category added!');
        return redirect('categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        if($category->parentid == 0)
        {
            Session::flash('flash_message', 'Invalid Category');
            return redirect('categories');
        } else {
            $cat_name = Category::ParentName($category->parentid);
        }
        return view('categories.show', compact('category', 'cat_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        if($category->parentid == 0)
        {
            Session::flash('flash_message', 'Invalid Category');
            return redirect('categories');
        } 
        $categories = Category::ActiveParentListing()->pluck('name', 'id');
        return view('categories.edit', compact('category','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, StoreCategoryRequest $request)
    {
        $category = Category::findOrFail($id);
        if($category->parentid == 0)
        {
            Session::flash('flash_message', 'Invalid Category');
            return redirect('categories');
        }

        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        
        if ($request->hasFile('image') || $request->hasFile('image_ar')) {
            
            $destinationPath = public_path('uploads/categories');
            $photoname = date("YmdHis");
            $data = $request->all();
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                // $data = $request->except(['image']);
                $data['image'] = $photo_englist;
            }

            if($request->hasFile('image_ar'))
            {
                $file_extention_ar = '.'.$request->file('image_ar')->getClientOriginalExtension();
                $photo_arabic = $photoname.'_ar'.$file_extention_ar;
                $file_check = $request->file('image_ar')->move($destinationPath, $photo_arabic);
                // $data = $request->except(['image_ar']);
                $data['image_ar'] = $photo_arabic;
            }
            $data['status'] = $status;
            $adsdata = $category->update($data);
        }else {            
            $adsdata = $category->update($request->all());
        }

        Session::flash('flash_message', 'Category updated!');

        return redirect('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isCustomer = Category::destroy($ids);
            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Category deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Category::destroy($id);

        Session::flash('flash_message', 'Category deleted!');

        return redirect('categories');
    }
}
