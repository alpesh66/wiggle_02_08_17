<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\EventRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Event;
use Illuminate\Http\Request;
use Session;
use Datatables;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('events.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables($request)
    {
        $events = Event::select(['id', 'name','startdate','enddate', 'weblink','status']);
        
        $datatables =  app('datatables')->of($events)

            ->addColumn('action', function($events) {
                return view('events.action', compact('events'))->render();
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('startdate', function($events) {
                $start = Carbon::parse($events->startdate)->format('l jS F Y');
                return $start;
            })
            ->editColumn('enddate', function($events) {
                $end = Carbon::parse($events->enddate)->format('l jS F Y');
                return $end;
            })
            ->addColumn('time', function($events) {
                $start = Carbon::parse($events->startdate)->format('h:i a');
                $end = Carbon::parse($events->enddate)->format('h:i a');
                $time = $start.' to '.$end;
                return $time;
            });
            
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(EventRequest $request)
    {
        // dd($request);
        $requestData = $request->all();
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $requestData['status'] = $status;
        
        if ($request->hasFile('image') || $request->hasFile('image_ar')) {
            
            $destinationPath = public_path('uploads/events');
            $photoname = date("YmdHis");
            $data = $request->all();
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                // $data = $request->except(['image']);
                $data['image'] = $photo_englist;
            }

            if($request->hasFile('image_ar'))
            {
                $file_extention_ar = '.'.$request->file('image_ar')->getClientOriginalExtension();
                $photo_arabic = $photoname.'_ar'.$file_extention_ar;
                $file_check = $request->file('image_ar')->move($destinationPath, $photo_arabic);
                // $data = $request->except(['image_ar']);
                $data['image_ar'] = $photo_arabic;
            }
            $data['status'] = $status;
            Event::create($data);
        }else {
            Event::create($requestData);
        }
            
        Session::flash('flash_message', 'Event added!');

        return redirect('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $requestData['status'] = $status;
        
        $event = Event::findOrFail($id);
        
        if ($request->hasFile('image') || $request->hasFile('image_ar')) {
            
            $destinationPath = public_path('uploads/events');
            $photoname = date("YmdHis");
            $data = $request->all();
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                // $data = $request->except(['image']);
                $data['image'] = $photo_englist;
            }

            if($request->hasFile('image_ar'))
            {
                $file_extention_ar = '.'.$request->file('image_ar')->getClientOriginalExtension();
                $photo_arabic = $photoname.'_ar'.$file_extention_ar;
                $file_check = $request->file('image_ar')->move($destinationPath, $photo_arabic);
                // $data = $request->except(['image_ar']);
                $data['image_ar'] = $photo_arabic;
            }
            $data['status'] = $status;
            $event->update($data);
        }else {            
            $event->update($requestData);
        }
        

        Session::flash('flash_message', 'Event updated!');

        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDelete = Event::destroy($ids);
            if($isDelete)
            {
                $data['success'] = true;
                $data['message'] = 'Event deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Event::destroy($id);

        Session::flash('flash_message', 'Event deleted!');

        return redirect('events');
    }
}
