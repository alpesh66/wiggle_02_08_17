<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Foster;
use App\Inbox;
use App\Customer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Http\Requests\FosterRequest;
use Illuminate\Support\Facades\Config;
use Mail;
use App\Pet;

class FosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $provider=session('provider_id');
    /*    $fosters = Foster::latest()->allMessage()
                    // ->orderBy('is_read','DESC')
                    // ->orderBy('sender','DESC')
                    // ->orderBy('created_at','DESC')
                    ->paginate(30);
        
        $pet_dog= \App\Pet::where(['provider_id'=>$provider,'breed'=>2])->where('pet_status','!=','Adopt')->get()->pluck('name','id');
        $pet_cat= \App\Pet::where(['provider_id'=>$provider,'breed'=>1])->where('pet_status','!=','Adopt')->get()->pluck('name','id');      
        return view('/society/fosters.index_old', compact('fosters','pet_dog','pet_cat'));*/
            /*$fosters = Foster::
            select(['fosters.id', 'customers.firstname','customers.middlename','customers.lastname', 'fosters.created_at', 'fosters.startdate', 'fosters.enddate', 'fosters.created_at', 'fosters.updated_at', 'fosters.title','fosters.status','foster_pet.pet_id','pets.breed','pets.name as petName', 'fosters.parent_id','fosters.is_read','fosters.sender','fosters.customer_id','fosters.provider_id'])
            ->rightjoin('foster_pet', 'foster_pet.foster_id', '=', 'fosters.id')
            ->leftjoin('pets', 'pets.id', '=', 'foster_pet.pet_id')
            ->leftjoin('customers', 'customers.id', '=', 'fosters.customer_id')
            ->where('fosters.provider_id', $provider)
            ->where('fosters.sender', 1)
            ->orderBy('fosters.created_at','DESC')->get();
            echo "<pre>";
            print_r($fosters);
            exit();*/

        if($request->ajax())
        {            
            $fosters = Foster::
            select(['fosters.id','fosters.breed_id', 'fosters.created_at', 'fosters.startdate', 'fosters.enddate', 'fosters.created_at', 'fosters.updated_at', 'fosters.description','fosters.status','foster_pet.pet_id','pets.breed','pets.name as petName', 'fosters.parent_id','fosters.is_read','fosters.sender','fosters.customer_id','fosters.provider_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS name')])
            ->leftjoin('foster_pet', 'foster_pet.foster_id', '=', 'fosters.id')
            ->leftjoin('pets', 'pets.id', '=', 'foster_pet.pet_id')
            ->leftjoin('customers', 'customers.id', '=', 'fosters.customer_id')
            ->where('fosters.provider_id', $provider)
            ->where('fosters.sender', 1)
            ->groupBy('fosters.id');


            $datatables =  app('datatables')->of($fosters)

                ->addColumn('action', function($fosters) {
                    return view('society.fosters.action', compact('fosters'))->render();
                })
                ->addColumn('name', function($fosters) {
                    return "<a style='font-weight: bold;' href='".url('societies/customers/'.$fosters->customer_id)."' >".$fosters->name."</a>";
                })
                ->addColumn('petType', function($fosters) {
                    if($fosters->breed_id==1)
                        return 'Cat';
                    else
                        return 'Dog';
                })
                ->editColumn('petName', function($fosters) {
                    return "<a style='font-weight: bold;' href='".url('societies/pet/'.$fosters->pet_id)."' >".$fosters->petName."</a>";
                })
                ->editColumn('updated_at', function($fosters) {
                    if($fosters->parent_id==0)
                        return NULL;
                    else
                        return $fosters->updated_at;
                })
                ->editColumn('status', '@if($status == 1) Approved @elseif($status == 2) Reject @else Pending  @endif')
                ->setRowClass(function ($fosters) {
                    if($fosters->is_read == 1 && $fosters->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'approved'){
                    $post = '1';
                } elseif(strtolower($val) == 'reject'){
                    $post = '2';
                }else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        $pet_dog= \App\Pet::where(['provider_id'=>$provider,'breed'=>2])->where('pet_status','=','Draft')->get()->pluck('name','id');
        $pet_cat= \App\Pet::where(['provider_id'=>$provider,'breed'=>1])->where('pet_status','=','Draft')->get()->pluck('name','id');   
        return view('/society/fosters.index', compact('pet_dog','pet_cat'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('/society/fosters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Foster::create($request->all());

        Session::flash('flash_message', 'Foster added!');

        return redirect('fosters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $foster = Foster::findOrFail($id);
        $foster->is_read = "0";
        $foster->save();
        $customer = Customer::with('pet')->findOrFail($foster->customer->id);
        
        return view('/society/fosters.show', compact('foster','customer'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $foster = Foster::findOrFail($id);

        return view('society/fosters.edit', compact('foster'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $foster = Foster::findOrFail($id);
        $foster->update($request->all());

        Session::flash('flash_message', 'Foster updated!');

        return redirect('fosters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $ids = $request->id; 
        if(!empty($ids)){
            foreach ($ids as $key => $id) {
                Foster::destroy($id);
            }    
            return json_encode(array('status'=>'1','message'=>'Foster deleted!'));
        }else{
            Foster::destroy($id);
            Session::flash('flash_message', 'Foster deleted!');
            return redirect('societies/fosters');
        }   
    }

    public function reply(FosterRequest $request){        
        $fosterUpdate = Foster::findOrFail($request->input('id'));
        $request['parent_id'] = $request->input('id');
        $pet=$request->input('pet');
        $status = $request->input('status');
        $description = $request->input('description');
        $customerid = $fosterUpdate->customer_id;
        /*$fosterUpdate->update($request->all());*/
        $fosterUpdate->update(['parent_id' =>$request->input('id'),'is_read'=>0,'status'=>$status]);
        $request['provider_id'] = session('provider_id');
        $request['parent_id'] = $request->input('id');
        $request['sender'] = 2;
        if($status==1){
            if(!empty($pet)){
                foreach ($pet as $k => $petid) {
                    \DB::table('foster_pet')->insert(['pet_id'=>$petid,'foster_id'=>$fosterUpdate->id]);
                    $petLog=[
                        'pet_id'=>$petid,
                        'provider_id'=>session('provider_id'),
                        'status'=>'Foster',
                        'admin_id'=>  session('user_id'),
                        'customer_id'=>$fosterUpdate->customer_id
                    ];
                    \App\Pet::where('id', '=', $petid)->update(['pet_status' =>'Foster']);
                    \App\Petlog::create($petLog);
                    \DB::table('customer_pet')->insert(['customer_id'=>$fosterUpdate->customer_id,'pet_id'=>$petid]);
                }
            }

            //event(new \App\Events\FosterConfirmed($fosterUpdate->id));
        }
        Foster::create($request->all());
        
        // send push notification to user            
           /* if($status==0){
                send_push_notification(push_messages('pending_foster.en.message'), push_messages('pending_foster.ar.message'),$customerid);
            }else*/
        $foster = Foster::findOrFail($request->input('id'));
        $data = array();
        $customer_email = false;
        $provider_email = false;            
        $booking_date = false;
        $booking_time = false;
        $customers_mobile = false;
        $customer_id=$foster->customer_id;
        $provider_id=$foster->provider_id;
        $request_id=$foster->id;
        $sikw_email = env('SIKW_BCC_COPY', false);

        $fost= Foster::SingleBookingDetail($request_id)->first(); 
        //  echo "<pre>"; print_r($fost); die();
        if(isset($fost['petid'])){
            $pet_detail = Pet::select('name')->where('id', $fost['petid'])->first();
            $pet_name = $pet_detail->name;    
        }else{
            $pet_name = "";    
        }
        $customer_email = $fost['customer_email'];
        $provider_email = $fost['providers_email'];
        $customer_name = $fost['customer_name'];
        $provider_name = $fost['provider_name'];

        if($status==1){
            $pushMessage_en=push_messages('approved_foster.en.message');
            $pushMessage_ar=push_messages('approved_foster.ar.message');
           
            $extraData=array('type'=>'FOSTER','reminder'=>FALSE);
            $email_datacustomer_name = $fost['customer_name'];
            $startdate=$fost['startdate'];
            $enddate=$fost['enddate'];
            $email_dataprovider_name = $fost['provider_name'];
            $fost['petid'];
            
            $pet=\DB::table('pets')->where('id',$fost['petid'])->get();
            $message = "Dear {$email_datacustomer_name}, Your Foster with {$email_dataprovider_name} for ";
            $messagePro="Dear {$email_dataprovider_name}, New Foster Request from {$email_datacustomer_name} for ";
            foreach($pet as $val)
            {
                $message .= "{$val->name}";
                $messagePro .= "{$val->name}";
            }
            $message .= "date between  {$startdate} and {$enddate} is approved.";
            $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
                  
            // echo "<pre>"; print_r($pet); 
            $customers_mobile = $fost['customers_mobile'];
            $data['customer_name'] = $email_datacustomer_name;
            $data['startdate']=$startdate;
            $data['enddate']=$enddate;
            $data['pet_name']=$pet_name;
            $data['provider_name'] = $email_dataprovider_name;
            $data['customers_mobile'] = $customers_mobile;
            $data['petname']=$pet[0]->name;
            $data['msgCust']="Your foster Request is apporved for ".$pet[0]->name;
            $data['msgPro']="New foster Request from"." ".$email_datacustomer_name. " "."for"." ".$pet[0]->name." "."is Approved";
                              
            //  file_put_contents($this->logFile, "\n\n =========datafoster : " . json_encode($data) . "\n\n", FILE_APPEND | LOCK_EX);
            /*add data in inbox(message)--==*/
            $startdate = Carbon::parse($startdate)->format('l, jS M Y h:i a');
            $enddate = Carbon::parse($enddate)->format('l, jS M Y h:i a');
            $reminder['message'] = "Dear ".$customer_name.",<br/>Your foster request has been approved for ".$pet_name." starting from ".$startdate." to ".$enddate.".";
            $reminder['provider_id'] = $provider_id;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 1;
            $reminder['type'] = 1;
            $reminder['customer_id'] = $customer_id;
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('approved_foster.en.title');
            $messageObj = Inbox::Create($reminder);         
            $message_id = $messageObj->id;
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
            /*--==*/
            if ($customer_email) {
                try {
                   /* Mail::queue('emails.fosterEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Foster request is approved");
                        if ($sikw_email) {
                              $message->bcc($sikw_email);
                        }
                    });*/  
                    Mail::queue('emails.Foster_Approval',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Foster request is approved");
                        if ($sikw_email) {
                          $message->bcc($sikw_email);
                        }
                    }); 
                } catch (\Swift_TransportException $e) {
                    //file_put_contents($this->logFile, "\n\n =========FosterEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                    //file_put_contents($this->logFile, "\n\n =========FosterEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                          //log_message("ERROR", $e->getMessage());
                }
            }
            if ($provider_email) {
                try {
                    Mail::queue('emails.fosterEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                        $messagePro->to($provider_email);
                        $messagePro->subject("New Foster is Approved");
                        if ($sikw_email) {
                            $messagePro->bcc($sikw_email);
                        }
                    });
                    
                } catch (\Swift_TransportException $e) {
                    //file_put_contents($this->logFile, "\n\n =========fosterEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                      //file_put_contents($this->logFile, "\n\n =========fosterEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                      log_message("ERROR", $e->getMessage());
                }
            }
        }elseif($status==2){
            $pushMessage_en=push_messages('reject_foster.en.message');
            $pushMessage_ar=push_messages('reject_foster.ar.message');
            $customer_id=$foster->customer_id;
            $provider_id=$foster->provider_id;
            $request_id=$foster->id;
            $extraData=array('type'=>'FOSTER','reminder'=>FALSE);

            $reminder['message'] = "Dear ".$customer_name.",<br/>Your foster request has been rejected.";
            $reminder['provider_id'] = $provider_id;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 1;
            $reminder['type'] = 2;
            $reminder['customer_id'] = $customer_id;
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('reject_foster.en.title');
            $message = Inbox::Create($reminder);         
            $message_id = $message->id;

            $data['customer_name'] = $customer_name;
            $data['provider_name'] = $provider_name;
            $data['description'] = $description;
            

            Mail::queue('emails.Foster_Rejected',$data,function ($message) use ($customer_email, $sikw_email) {
                $message->to($customer_email);
                $message->subject("Foster request is Rejected");
                if ($sikw_email) {
                  $message->bcc($sikw_email);
                }
            }); 
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
        }
        return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
    }
}
