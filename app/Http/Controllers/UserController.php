<?php

namespace App\Http\Controllers;

use App\Role;
use App\Technician;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ChangepasswordRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\PatchUserRequest;
use App\User;
use Session;
use Datatables;
use DB;

class UserController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return $this->callDatatables($request);
        }
        return view('user.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables()
    {

        $user = DB::table('users')
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->where('users.provider_id', 0)
                    ->select(['users.id', 'users.name','users.email','users.mobile','users.utype','users.status', 'roles.display_name as role']);
                    
        $datatables =  app('datatables')->of($user)
            ->addColumn('action', function($user){
                return view('user.action', compact('user'))->render();
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif');
        
        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');


        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if($name && $name == 'name')
        {
            $name = 'users.name';
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provider_id = Session::get('provider_id');
        $roles = \App\Role::where('provider_id', $provider_id )->pluck('display_name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        return view('user.create',compact('roles','country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        // $data['name'] = $request->input('name');
        // $data['email'] = $request->input('email');
        $request['password'] = bcrypt($request->input('password'));
        $request['api_token'] = str_random(60);
        $request['mobile'] = $request->input('country').'-'.$request->input('mobile');

        $role = $request->input('roles');
        $user = User::create($request->all());
        $user->attachRole($role);
        Session::flash('flash_message', 'Admin added!');

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('roles')->findOrFail($id);
        $country = \App\Country::pluck('nicename','phonecode');
        $provider_id = Session::get('provider_id');
        $country_code = countryCode_explode($user['mobile']);
        $roles = \App\Role::where('provider_id', $provider_id)->pluck('display_name','id');
        return view('user.edit', compact('user','roles','country','country_code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatchUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['mobile'] = $request->input('country').'-'.$request->input('mobile');
        $role['id'] = $request->input('roles');

        $user->update($data);
        if(!empty($request->input('roles')))
        {
            $user->roles()->sync($role);
        }

        Session::flash('flash_message', 'User updated!');

        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $i = 0;
            foreach ($ids as $user_id) {
                // $isDeleted = User::where('utype','0')->where('id',$ids)->delete();

                $isDeleted = User::find($user_id);
                // dump($isDeleted);
                if($isDeleted->utype == 0)
                {
                    $isDeleted->roles()->detach();
                    $isDeleted->delete();
                    $i++;
                }
                
            }
            if(count($ids) == $i)
            {
                $data['success'] = true;
                $data['message'] = 'Admin deleted successfully';    
            } else if(count($ids) > $i) {
                $data['success'] = true;
                $data['message'] = 'Super Admin cannot be removed';
            }else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occoured!!';
            }
            
            return $data;
        }
        
        $isDeleted = User::find($id);
        // dump($isDeleted);
        if($isDeleted->utype == 0)
        {
            $isDeleted->roles()->detach();
            $isDeleted->delete();
            Session::flash('flash_message', 'User deleted!');
        } else {

            Session::flash('flash_message', 'Admin cannot be deleted.');
        }


        return redirect('user');
    }

    /**
     * Display a listing of the PROVIDER ADMIN
     *
     * @return \Illuminate\Http\Response
     */
    public function indexproviders(Request $request)
    {
        $pro_id = Session::get('provider_id');
        // $user = User::with('roles')->where('provider_id', $pro_id)->paginate(15);
        if ($request->ajax()){
            $pro_id = Session::get('provider_id');
            
            // $user = User::with('roles')->where('provider_id', $pro_id)->select(['id', 'name','email','mobile','utype']);
            
            $user = DB::table('users')
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->where('users.provider_id', $pro_id)
                    ->whereIn('users.utype', [0,1])
                    ->select(['users.id', 'users.name','users.email','users.mobile','users.utype','users.status', 'roles.display_name as role']);

            $datatables =  app('datatables')->of($user)

                ->addColumn('action', function($user) {
                    return view('servicepro.user.action', compact('user'))->render();
                });
                // ->editColumn('status', '@if($status) Active @else Inactive @endif')
                // ->editColumn('name', function($ratings) {
                    // return $ratings->name;
                // });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
            // return Datatables::of($provider)->make(true);       
        }
        return view('servicepro.user.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatablesprovider()
    {
        $pro_id = Session::get('provider_id');
        $provider = User::with('roles')->where('provider_id', $pro_id)->select(['id', 'name','email','utype']);
        return Datatables::of($provider)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createproviders()
    {
        $provider_id = Session::get('provider_id');
        $roles = \App\Role::where('provider_id', $provider_id )->where('type', 0)->pluck('display_name','id');

        return view('servicepro.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeproviders(StoreUserRequest $request)
    {
        // $data['name'] = $request->input('name');
        // $data['email'] = $request->input('email');
        $request['password'] = bcrypt($request->input('password'));
        $request['api_token'] = str_random(60);
        $request['status'] = 1;
        $request['provider_id'] = Session::get('provider_id');
        $role = $request->input('roles');
        $user = User::create($request->all());
        $user->attachRole($role);
        Session::flash('flash_message', 'Admin added!');

        return redirect('providers/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showproviders($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return view('servicepro.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editproviders($id)
    {
        $user = User::with('roles')->findOrFail($id);
        $provider_id = Session::get('provider_id');
        $roles = \App\Role::where('provider_id', $provider_id)->where('type', 0)->pluck('display_name','id');
        return view('servicepro.user.edit', compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateproviders(PatchUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['password'] = $request->input('password');

        $role['id'] = $request->input('roles');

        $user->update($data);
        if(!empty($request->input('roles')))
        {
            $user->roles()->sync($role);
        }

        Session::flash('flash_message', 'User updated!');

        return redirect('providers/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyproviders(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $i = 0;
            foreach ($ids as $user_id) {
                // $isDeleted = User::where('utype','0')->where('id',$ids)->delete();

                $isDeleted = User::find($user_id);
                // dump($isDeleted);
                if($isDeleted->utype != 1)
                {
                    $isDeleted->roles()->detach();
                    $isDeleted->delete();
                    $i++;
                }
                
            }
            if(count($ids) == $i)
            {
                $data['success'] = true;
                $data['message'] = 'Admin deleted successfully';    
            } else if(count($ids) > $i) {
                $data['success'] = true;
                $data['message'] = 'Super Admin cannot be removed';
            }else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occoured!!';
            }
            
            return $data;
        }
        
        $isDeleted = User::find($id);
        // dump($isDeleted);
        if($isDeleted->utype != 1)
        {
            $isDeleted->roles()->detach();
            $isDeleted->delete();
            Session::flash('flash_message', 'User deleted!');
        } else {

            Session::flash('flash_message', 'Admin cannot be deleted.');
        }


        return redirect('providers/user');
    }

    /** For only Technician  Login Users Listings
     * @param Request $request
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexOnlyTechnicians(Request $request)
    {
        $pro_id = Session::get('provider_id');
        // $user = User::with('roles')->where('provider_id', $pro_id)->paginate(15);
        if ($request->ajax()){
            $pro_id = Session::get('provider_id');

            // $user = User::with('roles')->where('provider_id', $pro_id)->select(['id', 'name','email','mobile','utype']);

            $user = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.provider_id', $pro_id)
                ->where('users.utype', 2)
                ->select(['users.id', 'users.name','users.email','users.mobile','users.utype','users.status', 'roles.display_name as role', 'users.technicians']);

            $datatables =  app('datatables')->of($user)

                ->addColumn('action', function($user) {
                    return view('servicepro.user.techaction', compact('user'))->render();
                })
             ->editColumn('technicians', function($user) {
               return Technician::where('id',$user->technicians)->pluck('name');
            });
            // ->editColumn('name', function($ratings) {
            // return $ratings->name;
            // });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
            // return Datatables::of($provider)->make(true);
        }
        return view('servicepro.user.onlytechindex');
    }

    public function createtechLoginProviders()
    {
        $technicians = \App\Technician::where('provider_id', Session::get('provider_id') )->where('status', 1)->pluck('name','id');
        return view('servicepro.user.techcreate',compact('technicians'));
    }

    public function editTechLoginProviders($id)
    {
        $user = User::findOrFail($id);
        $technicians = \App\Technician::where('provider_id', Session::get('provider_id') )->where('status', 1)->pluck('name','id');
        return view('servicepro.user.techedit', compact('user','technicians'));
    }

    public function storeTechLoginproviders(Requests\TechLoginUserRequest $request, Role $role)
    {
        $request['password'] = bcrypt($request->input('password'));
        $request['api_token'] = str_random(60);
        $request['status'] = 1;
        $request['provider_id'] = Session::get('provider_id');
        $request['utype'] = 2;
        $user = User::create($request->all());

        $saveRole['display_name'] = 'Technician Calendar Login';
        $saveRole['description'] = 'Specialist Login';
        $saveRole['provider_id'] = Session::get('provider_id');
        $saveRole['type'] = 1;  // 1: means for the Tech login only
        $gotRole = $role->addmyRole($saveRole);
        $gotRole->attachPermission(25);
        $user->attachRole($gotRole);
        Session::flash('flash_message', 'Specialist Login added!');

        return redirect('providers/logintech');
    }

    public function updateTechLoginProviders(PatchUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
//        dd($user);
        $user->update($request->all());
        Session::flash('flash_message', 'Specialist updated!');

        return redirect('providers/logintech');
    }
    /**
     * Display a listing of the SOCIETIES
     *
     * @return \Illuminate\Http\Response
     */
    public function indexsocieties(Request $request)
    {
        // $user = User::with('roles')->where('provider_id', $pro_id)->paginate(15);
        if($request->ajax()) {
            $pro_id = Session::get('provider_id');
            $user = DB::table('users')
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->where('users.provider_id', $pro_id)
                    ->select(['users.id', 'users.name','users.email','users.mobile','users.utype','users.status', 'roles.display_name as role']);
                        
            $datatables =  app('datatables')->of($user)
                ->addColumn('action', function($user){
                    return view('society.user.action', compact('user'))->render();
                })
                ->editColumn('status', '@if($status) Active @else Inactive @endif');
            
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true); 
        }
        
        return view('society.user.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createsocieties()
    {
        $provider_id = Session::get('provider_id');
        $roles = \App\Role::where('provider_id', $provider_id )->pluck('display_name','id');
        
        return view('society.user.create', compact('roles','country','country_code'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storesocieties(StoreUserRequest $request)
    {
        // $data['name'] = $request->input('name');
        // $data['email'] = $request->input('email');
        $request['password'] = bcrypt($request->input('password'));
        $request['api_token'] = str_random(60);
        $request['status'] = 1;
        $request['provider_id'] = Session::get('provider_id');
        $role = $request->input('roles');
        $user = User::create($request->all());
        $user->attachRole($role);
        Session::flash('flash_message', 'Admin added!');

        return redirect('societies/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showsocieties($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return view('society.user.show', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editsocieties($id)
    {
        $user = User::with('roles')->findOrFail($id);
        $provider_id = Session::get('provider_id');
        $country = \App\Country::pluck('nicename','phonecode');
        $country_code = countryCode_explode($user['mobile']);
        $roles = \App\Role::where('provider_id', $provider_id)->pluck('display_name','id');

        return view('society.user.edit', compact('user','roles','country','country_code'));
        
        
    }

    /**
     * change password methos
     */
    public function changepassword(ChangepasswordRequest $request)
    {
        if ($request->ajax() )
        {
            $user_id = \Auth::user()->id;
            $user = User::findOrFail($user_id);
            $data['password'] = bcrypt($request->input('password'));
            if($user->update($data)){
                $data['success'] = true;
                $data['message'] = 'Password has been changed successfully.'; 
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            return $data;
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatesocieties(PatchUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['password'] = $request->input('password');
        $data['mobile'] = $request->input('country').'-'.$request->input('mobile');
        $role['id'] = $request->input('roles');

        $user->update($data);
        if(!empty($request->input('roles')))
        {
            $user->roles()->sync($role);
        }

        Session::flash('flash_message', 'User updated!');

        return redirect('societies/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroysocieties(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $i = 0;
            foreach ($ids as $user_id) {
                // $isDeleted = User::where('utype','0')->where('id',$ids)->delete();

                $isDeleted = User::find($user_id);
                // dump($isDeleted);
                if($isDeleted->utype == 0)
                {
                    $isDeleted->roles()->detach();
                    $isDeleted->delete();
                    $i++;
                }
                
            }
            if(count($ids) == $i)
            {
                $data['success'] = true;
                $data['message'] = 'Admin deleted successfully';    
            } else if(count($ids) > $i) {
                $data['success'] = true;
                $data['message'] = 'Super Admin cannot be removed';
            }else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occoured!!';
            }
            
            return $data;
        }
        
        $isDeleted = User::find($id);
        // dump($isDeleted);
        if($isDeleted->utype == 0)
        {
            $isDeleted->roles()->detach();
            $isDeleted->delete();
            Session::flash('flash_message', 'User deleted!');
        } else {

            Session::flash('flash_message', 'Admin cannot be deleted.');
        }


        return redirect('societies/user');
    }
}
