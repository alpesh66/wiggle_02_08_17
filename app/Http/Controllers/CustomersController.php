<?php

namespace App\Http\Controllers;

use App\Events\CustomerCreated;
use App\Http\Requests;
use App\Http\Requests\CustomerUpdateRequest;
use App\Http\Requests\CustomerCreateRequest;
use App\Http\Requests\providerCustomerRequest;

use App\Http\Controllers\Controller;
use DB;
use App\Customer;
use App\Pet;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Datatables;
use Illuminate\Support\Str;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

//           $customer = DB::table('pets')->select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status','count(customer_pet.pet_id) as totalpet'])
//                 ->join('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
//                 ->join('customers', 'customers.id', '=', 'customer_pet.customer_id')
//                 ->selectRaw('count(pet_id) as totalPets')
// ->get()
//                 ;
//                 // ->groupBy('customers.id');

// dd($customer);
        if ($request->ajax())
        {
            return $this->callDatatables($request);
        }
        return view('customers.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function callDatatables($request)
    {
//        $customer = Customer::with('pet')->select(['id', 'firstname', 'middlename','lastname', 'email','mobile', 'status']);
         $customer = Customer::
                 leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
                 ->leftjoin('pets', 'customer_pet.pet_id', '=', 'pets.id')
                ->select(['customers.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', 'customers.status'])
                 ->groupBy('customers.id');



        $datatables =  app('datatables')->of($customer)

            ->addColumn('action', function($customer) {
                return view('customers.action', compact('customer'))->render();
            })
            ->editColumn('nospets', function($customers) {
                return $customers->pet->count();
                // return '1';
            })
            ->editColumn('nosdogs', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customers) {
                $count =  $customers->pet->count();
                if($count>0){
                    $collect = collect($customers->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif')
            ->editColumn('name', function($customer) {
                return $customer->name;
            });

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }



        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(CustomerCreateRequest $request)
    {
        $request['api_token'] = str_random(60);
        $request['mobile'] = $request->input('country').'-'.$request->input('mobile');
        $request['status'] = 1;

        $pets = $request->input('pets');
        event(new CustomerCreated($customer = Customer::create($request->all()) ));


        $pets = Pet::create($pets);

        if($pets){

            $customer->pet()->attach($pets);
             $resp['success'] = true;
            $resp['message'] = 'New client added';
            $resp['customer'] = $customer;
        } else {
             $resp['success'] = false;
            $resp['message'] = 'Some error occoured';
        }
       
        return $resp;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $customer = Customer::with('pet','area')->findOrFail($id);

        return view('customers.show', compact('customer'));
    }

    public function customerpets(Request $request,$id){
        $customer = Customer::findOrFail($id);

        if ($request->ajax()){
            
            // $customer_pets = Pet::select(['id', 'name', 'dob','breed'])->where('id', $id);
            // $customer_pets = DB::table('pets')->select(['pets.id as id', 'pets.name', 'pets.dob','pets.breed'])
            //     ->join('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
            //     ->join('customers', 'customers.id', '=', 'customer_pet.customer_id')
            //     ->where('customer_pet.customer_id', $id);
            $customer_pets = Pet::whereHas('customer', function($query) use ($id){
                $query->where('customer_id', $id);
            })->where('pets.status','1');

            $datatables =  app('datatables')->of($customer_pets)
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                ->editColumn('gender', '@if($gender) Male @else Female @endif')
                ->addColumn('action', function($customer_pets) {
                    return view('customers.petaction', compact('customer_pets'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('customers.show');
    }

    public function customerbookings(Request $request,$id){
        $customer = Customer::findOrFail($id);
        
        if ($request->ajax()){

            $bookings = DB::table('bookings')
                        ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                        ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                        ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                        ->join('services', 'bookings.service_id', '=', 'services.id')
                        ->join('categories', 'services.category_id', '=', 'categories.id')
                        ->join('providers', 'providers.id', '=', 'bookings.provider_id')
                        ->select(['customers.firstname','customers.mobile','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id', 'bookings.status','bookings.id as bookid', 'bookings.provider_id', 'providers.name as provider_name'])
                        ->where('bookings.customer_id', $id);

            // dd($bookings); exit;            
            
            $datatables =  app('datatables')->of($bookings)
                ->editColumn('status', '@if($status == 0) Unconfirmed @elseif($status == 1) Confirmed @elseif($status == 2) Cancelled @endif')
                ->editColumn('type_id', '@if($type_id == 1) Veternary @elseif($type_id == 2) Groom @elseif($type_id == 4) Trainer @endif')
                // ->addColumn('action', function($bookings) {
                //     return view('customers.bookaction', compact('bookings'))->render();
                // })
                ;
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('customers.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        $dial_number = countryCode_explode($customer['mobile']);

        $area = \App\Area::pluck('name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        return view('customers.edit', compact('customer','area','country','dial_number'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, CustomerUpdateRequest $request)
    {
        
        $request['mobile'] = $request->input('country').'-'.$request->input('mobile');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;

        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        Session::flash('flash_message', 'Customer updated!');

        return redirect('customers');
    }

    public function providercustomerupdate ($id, providerCustomerRequest $request) {

        $request['mobile'] = $request->input('country').'-'.$request->input('mobile');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;

        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        Session::flash('flash_message', 'Customer updated!');

        return redirect('providers/customers');   
    }

    public function customerPetDeactivation (Request $request) {

        if ($request->ajax() )
        {

            $id = $request->input('pet_id');
            $deletePet = \App\Pet::findOrFail($id);
            $response = $deletePet->update($request->all());
            if($response)
            {
                $data['success'] = true;
                $data['message'] = 'Pet has been deactivated successfully.';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        return redirect('customers');   
    }

    public function customerProviderPetDeactivation (Request $request) {
        
        if ($request->ajax() )
        {

            $id = $request->input('pet_id');
            $deletePet = \App\Pet::findOrFail($id);
            $response = $deletePet->update($request->all());
            if($response)
            {
                $data['success'] = true;
                $data['message'] = 'Pet has been deactivated successfully.';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        return redirect('providers/customers');   
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        // Requests from the Datatables
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isCustomer = Customer::destroy($ids);
            if($isCustomer)
            {
                $data['success'] = true;
                $data['message'] = 'Users deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Customer::destroy($id);

        Session::flash('flash_message', 'Customer deleted!');

        return redirect('customers');
    }

    public function searchCustomerData(Request $request)
    {
        $name = $request->input('name');
        $conditions = array(array('firstname', 'like', '%'.$name.'%'), array('lastname', 'like', '%'.$name.'%') );
        return $customer = Customer::with('pet')->select('firstname as label', 'customers.*', 'firstname as value')->where('firstname','like', '%'.$name.'%')
                        ->orWhere('lastname', 'like', '%'.$name.'%')
                        ->orWhere('mobile', 'like', '%'.$name.'%')
                        ->orWhere('email', 'like', '%'.$name.'%')
                        ->take(20)->get();
    }

    /**
     * Display a listing of the Customer in the Provider Admin.
     *
     * @return void
     */
    public function indexCustomer(Request $request)
    {
        if($request->ajax())
        {            
            $customer = Customer::
                leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
                ->leftjoin('pets', 'customer_pet.pet_id', '=', 'pets.id')
                ->select(['customers.id', 'firstname','middlename','lastname', 'email','mobile', 'customers.status'])
            // $customer = DB::table('customers')
                ->join('bookings', 'bookings.customer_id', '=', 'customers.id')
                ->where('bookings.provider_id', session('provider_id'))
                ->groupBy('bookings.customer_id');
                // ->select(['customers.id', 'firstname','middlename','lastname', 'email','mobile', 'customers.status']);

            $datatables =  app('datatables')->of($customer)

                ->addColumn('action', function($customer) {
                    return view('servicepro.customers.action', compact('customer'))->render();
                })
                ->editColumn('status', '@if($status) Active @else Inactive @endif')
                ->editColumn('nosdogs', function($customer) {
                $count =  $customer->pet->count();
                if($count>0){
                    $collect = collect($customer->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[2])? count($dogs[2]) : 0  ;
                } else {
                    return 0;
                }
            })
            ->editColumn('noscats', function($customer) {
                $count =  $customer->pet->count();
                if($count>0){
                    $collect = collect($customer->pet);
                    $dogs = $collect->groupBy('breed')->all();
                    return isset($dogs[1])? count($dogs[1]) : 0  ;
                } else {
                    return 0;
                }
            })
                ->editColumn('name', function($customer) {
                    return $customer->name;
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.customers.index');
    }

    public function showproviders($id)
    {

        $check_customer = \App\Booking::isCustomerofThisProvider($id)->exists();

        if(!$check_customer){
        
        // Session::flash('flash_message', 'Invalid Customer');
            return redirect('providers/customers');
        }

        $customer = Customer::with('pet')->findOrFail($id);
        return view('servicepro.customers.show', compact('customer'));
    }

    public function editproviders($id)
    {
        $check_customer = \App\Booking::isCustomerofThisProvider($id)->exists();

        if(!$check_customer){
        
            // Session::flash('flash_message', 'Invalid Customer');
            return redirect('providers/customers');            
        }

        $customer = Customer::findOrFail($id);
        $dial_number = countryCode_explode($customer['mobile']);

        $area = \App\Area::pluck('name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        return view('servicepro.customers.edit', compact('customer','area','country','dial_number'));
    }

    public function customerbookingspro(Request $request,$id){
        // $customer = Customer::findOrFail($id);
        
        if ($request->ajax()){

            $bookings = DB::table('bookings')
                        ->join('customers', 'bookings.customer_id', '=', 'customers.id')
                        ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                        ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                        ->join('services', 'bookings.service_id', '=', 'services.id')
                        ->join('categories', 'services.category_id', '=', 'categories.id')
                        ->select(['customers.firstname','customers.mobile','customers.email', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start', 'bookings.type_id', 'bookings.status','bookings.id as bookid'])
                        ->where('bookings.provider_id', session('provider_id'))
                        ->where('bookings.customer_id', $id);

            // dd($bookings); exit;            
            
            $datatables =  app('datatables')->of($bookings)
                ->editColumn('status', '@if($status == 0) Unconfirmed @elseif($status == 1) Confirmed @elseif($status == 2) Cancelled @endif')
                ->editColumn('type_id', '@if($type_id == 1) Veternary @elseif($type_id == 2) Groom @elseif($type_id == 4) Trainer @endif')
                ->addColumn('action', function($bookings) {
                    return view('servicepro.customers.bookaction', compact('bookings'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.customers.show');
    }

    public function customerpetspro(Request $request,$id){
        // $customer = Customer::findOrFail($id);

        if ($request->ajax()){
            
            $customer_pets = Pet::select(['pets.id as id', 'pets.*', 'customers.id as cid'])
            // $customer_pets = DB::table('pets')->select(['pets.id as id', 'pets.name', 'pets.dob','pets.breed'])
                ->join('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
                ->join('customers', 'customers.id', '=', 'customer_pet.customer_id')
                ->where('customer_pet.customer_id', $id);


            $datatables =  app('datatables')->of($customer_pets)
                ->editColumn('gender', '@if($gender) Male @else Female @endif')
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                ->addColumn('action', function($customer_pets) {
                    if(strpos(\Request::path(),'tech/customers') !==FALSE){
                        return view('servicepro.customers.techpetaction', compact('customer_pets'))->render();
                    }else{
                        return view('servicepro.customers.petaction', compact('customer_pets'))->render();
                    }
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('customers.show');
    }

    public function showpet($id)
    {   
        $pet = Pet::with('breeds')->findOrFail($id);

        $cust_data = ($pet->customer->pluck('id')->all());

        if($cust_data){
            $check_customer = \App\Booking::isCustomerofThisProvider($cust_data[0])->exists();

            if(!$check_customer){
            
            // Session::flash('flash_message', 'Invalid Customer');
                return redirect('providers/customers');
            }
        } else {
            return redirect('providers/customers');
        }

        $size = \App\Size::where('status', 1)->pluck('name', 'id');
        return view('servicepro.customers.showpet', compact('pet', 'size'));
    }

    public function indexSocietyCustomer(Request $request)
    {
        if ($request->ajax()){
            
            $customer = Customer::with('pet')->select(['id', 'firstname', 'middlename','lastname', 'email','mobile', 'status'])
                        ->rightJoin()
            // $customer_pets = DB::table('pets')->select(['pets.id as id', 'pets.name', 'pets.dob','pets.breed'])
                ->join('customer_pet', 'customer_pet.pet_id', '=', 'pets.id')
                ->join('customers', 'customers.id', '=', 'customer_pet.customer_id')
                ->where('customer_pet.customer_id', $id);

            $datatables =  app('datatables')->of($customer)
                ->editColumn('gender', '@if($gender) Male @else Female @endif')
                ->editColumn('breed', '@if($breed == 1) Cat @else Dog @endif')
                ->addColumn('action', function($customer_pets) {
                    return view('servicepro.customers.petaction', compact('customer_pets'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('society.customers.index');
    }
    public function showsociety($id)
    {
        $customer = Customer::with('pet')->findOrFail($id);
        return view('society.customers.show', compact('customer'));
    }
}
