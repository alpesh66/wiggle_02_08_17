<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Customer;
use App\Booking;
use App\Store;
use App\Technician;
use App\Rating;
use App\Provider;
use App\Category;
use Mail;
use Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use Session;
use DB;
use Excel;

class ProviderSalesController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        /*if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }*/
        $categories = Category::ActiveParentListing()->pluck('name', 'id')->toArray();
        $BookingStatus=array("1"=>"Confirm","2"=>"Cancel","3"=>"No Show");
        $BookingFrom=array("0"=>"Mobile","1"=>"Admin");
        $PaymentStatus=array("0"=>"Online","1"=>"Cash");
        return view('servicepro.sales.index', compact('categories','BookingStatus','BookingFrom','PaymentStatus'));
    }
    public function getCategoryList(Request $request)
    {
        if($request->ajax()){
            $categories = DB::table('categories')->whereIn('parentid',$request->parentid)->pluck("name","id");
            $data = "";
            foreach ($categories as $key => $value) {
                $data .= "<option value='".$key."'>".$value."</option>";
            }
            return $data;
        }
    }
    public function generateSalesReport(Request $request)
    {
        $provider_id = Session::get('provider_id');
        $parentid = $request->parentid;
        $service_id = $request->service_id;
        $booking_status = $request->booking_status;
        $booking_from = $request->booking_from;
        $payment_status = $request->payment_status;
        if(isset($request->startdate) && $request->startdate!=""){
            $startdate = $request->startdate;
            $startdate=date_create($startdate);
            $book_startdate = date_format($startdate, 'Y-m-d');    
        }else{
            $startdate = "";
            $book_startdate ="";
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $enddate = $request->enddate;
            $enddate = date_create($enddate);
            $book_enddate = date_format($enddate, 'Y-m-d');
        }else{
            $enddate = "";
            $book_enddate ="";
        }        

        $bookings = Booking::join('customers', 'bookings.customer_id', '=', 'customers.id')
            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
            ->join('services', 'bookings.service_id', '=', 'services.id')
            ->join('categories', 'services.category_id', '=', 'categories.id')
            ->select(['customers.id','customers.firstname','customers.middlename','customers.lastname', 'pets.name as petname', 'technicians.name as techname', 'categories.name as servicename', 'bookings.start','bookings.end', 'bookings.type_id', 'bookings.status', 'bookings.noshow','bookings.madefrom','bookings.id as booking_id', 'providers.name as provider_name', 'bookings.technician_id','providers.name_ar as provider_name_ar', 'bookings.ontime', 'bookings.provider_id', 'providers.photo', 'providers.icon', 'pets.photo as petphoto', 'bookings.notes','bookings.type_id','bookings.created_at','bookings.ontime']);
            
        if(isset($provider_id) && $provider_id!="")
            $bookings->where('bookings.provider_id','=', $provider_id);
        if(isset($parentid) && $parentid!="")
            $bookings->whereIn('categories.parentid', $parentid);
        if(isset($service_id) && $service_id!="")
            $bookings->whereIn('categories.id', $service_id);
        /*if(isset($booking_status) && $booking_status!=""){
            if($booking_status==3){
                $bookings->where('bookings.status', 1);    
                $bookings->where('bookings.noshow', 'true');    
            }else{
                $bookings->where('bookings.status', $booking_status);    
            }
        }*/
        if(isset($booking_status) && $booking_status!=""){
             if (in_array("3", $booking_status))
            {
                $bookings->where('bookings.status', 1);    
                $bookings->where('bookings.noshow', 'true');    
            }
            if(in_array("1", $booking_status))
            {
                if(in_array("3", $booking_status))
                {
                    $bookings->Orwhere('bookings.status', 1);    
                    $bookings->where('bookings.noshow', 'false');    
                }else{
                    $bookings->where('bookings.status', 1);    
                    $bookings->where('bookings.noshow', 'false');    
                }
            }
            if(in_array("2", $booking_status))
            {
                if(in_array("1", $booking_status) || in_array("3", $booking_status))
                {
                    $bookings->where('bookings.status', 2);    
                }else{
                    $bookings->where('bookings.status', 2);   
                }
            }
        }
            
        if(isset($booking_from)  && $booking_from!="")
             $bookings->whereIn('bookings.madefrom', $booking_from);
        if(isset($book_startdate)  && $book_startdate!="")
            $bookings->where('bookings.start','>=',$book_startdate);
        if(isset($book_enddate)  && $book_enddate!="")
            $bookings->where('bookings.end','<=',$book_enddate);
        
    
        $result = $bookings->groupBy('bookings.ontime')->get();
        $result = json_decode(json_encode($result), true);
        /*$data[] =array();*/
        $totalConfirmBooking = 0;
        $totalNoShow = 0;
        $totalCancelBooking = 0;
        $totalUnconfirmBooking = 0;
        $bookings = 0;
        $mobile_booking = 0;
        $web_booking = 0;
        $vet= 0;
        $grooming = 0;
        $walker = 0;
        $training = 0;        
        foreach ($result as $key => $value) {
            $bookings++;
            if($value['status']==1 && $value['noshow']=='true'){
                $totalNoShow++;
                $status = "No Show";
            }
            else if($value['status']==1 && $value['noshow']=='false'){
                $status = "Confirm";
                $totalConfirmBooking++;
            }
            else if($value['status']==2){
                $status = "Cancelled";
                $totalCancelBooking++;
            }
            else{                
                $status = "Unconfirm";
                $totalUnconfirmBooking++;
            }

            if($value['madefrom']==0){
                $bookedFrom="Mobile App";
                $mobile_booking ++;
            }
            else{
                $bookedFrom="Admin";
                $web_booking ++;
            }
            if($value['type_id']==1){
                $vet++;
            }
            if($value['type_id']==2){
                $grooming++;        
            }
            if($value['type_id']==3){
                $walker++;
            }
            if($value['type_id']==4){
                $training++;
            }

            $categories = DB::table('categories')->where('id',$value['type_id'])->pluck("name");
            $services_cat=implode($categories, ',');
            $makehtml = array();
            $petDetail = array();
            $allBookings =  Booking::OnTimeBookingDetail($value['ontime'])->get();
            if($allBookings){
                $prepare = collect($allBookings);
                $petsBook = $prepare->groupBy('petname');

                foreach ($petsBook->all() as $pet_id => $valuep) {
                    $petDetail[] = $pet_id;
                    foreach ($valuep as $pet_key => $book_value) {
                        $makehtml[] = $book_value['servicename'];
                    }
                }

            }
            //print_r(array_unique($petDetail));
            $services = implode(array_unique($makehtml), ',');
            unset($makehtm);

            $customers_name=$value['firstname']." ".$value['lastname'];
            $transaction_no="";

            $data[] = array(
                    $value['id'],
                    $customers_name,                    
                    $value['petname'],
                    $transaction_no,
                    $value['start'],
                    $value['created_at'],
                    $services_cat,
                    $services,
                    $bookedFrom,
                    $status,
                    '',
                    ''
                );
        }
        if(isset($bookings) && $bookings!=0){
            $confirm_bookings_percent = $totalConfirmBooking*100/$bookings;
            $cancel_bookings_percent = $totalCancelBooking*100/$bookings;
            $unconfirm_bookings_percent = $totalUnconfirmBooking*100/$bookings;    
                  
        }else{
            $confirm_bookings_percent = 0;
            $cancel_bookings_percent = 0;
            $unconfirm_bookings_percent = 0;    
            $noshow_bookings_percent = 0;    
        }
        if(isset($mobile_booking) && $mobile_booking!=0){
            $mob_percent = $mobile_booking*100/$bookings;
        }else{
            $mob_percent = 0;
        }
        if(isset($web_booking) && $web_booking!=0){
            $web_percent = $web_booking*100/$bookings;
        }else{
            $web_percent = 0;
        }
        if(isset($vet) && $vet!=0){
            $vet_percent = $vet*100/$bookings;
        }else{
            $vet_percent = 0;
        }
        if(isset($grooming) && $grooming!=0){
            $grooming_percent = $grooming*100/$bookings;
        }else{
            $grooming_percent = 0;
        }
        if(isset($walker) && $walker!=0){
            $walker_percent = $walker*100/$bookings;
        }else{
            $walker_percent = 0;
        }
        if(isset($training) && $training!=0){
            $training_percent = $training*100/$bookings;
        }else{
            $training_percent = 0;
        }
        $confirm_percent = round($confirm_bookings_percent,2)." %" ;
        $cancel_percent = round($cancel_bookings_percent,2)." %" ;
        $unconfirm_percent = round($unconfirm_bookings_percent,2)." %" ;
        $noshow_percent = round($noshow_bookings_percent,2)." %" ;

        $total_booking =  $bookings;

        $total_mobile_percent = round($mob_percent,2)." %" ;
        $total_web_percent = round($web_percent,2)." %" ;

        $total_vet_percent = round($vet_percent,2)." %" ;
        $total_grooming_percent = round($grooming_percent,2)." %" ;
        $total_walker_percent = round($walker_percent,2)." %" ;
        $total_training_percent = round($training_percent,2)." %" ;
       if($request->startdate!="" && $request->enddate!="")
            $reportDate = $request->startdate." to ".$request->enddate;
        else
            $reportDate = "";
        if(!empty($data)){
            $items = $data;
            Excel::create('SalesReport', function($excel) use($items,$totalConfirmBooking,$totalCancelBooking,$totalUnconfirmBooking,$totalNoShow,$confirm_percent,$cancel_percent,$unconfirm_percent,$noshow_percent,$bookings,$reportDate,$total_booking,$total_mobile_percent, $total_web_percent,$web_booking,$mobile_booking,$total_vet_percent,$total_grooming_percent,$total_walker_percent,$total_training_percent,$vet,$grooming,$training,$walker) {
                $excel->sheet('SalesReport', function($sheet) use($items,$totalConfirmBooking,$totalCancelBooking,$totalUnconfirmBooking,$totalNoShow,$confirm_percent,$cancel_percent,$unconfirm_percent,$noshow_percent,$bookings,$reportDate,$total_booking,$total_mobile_percent,$total_web_percent,$web_booking,$mobile_booking,$total_vet_percent,$total_grooming_percent,$total_walker_percent,$total_training_percent,$vet,$grooming,$training,$walker) {
                     $sheet->fromArray(array(
                        array('','','','','','Sales Report'),
                        array('','','','','',$reportDate),
                        array('Total Booking',$total_booking,' ','Vet Booking',$vet,$total_vet_percent,'Confirmed Booking', $totalConfirmBooking, $confirm_percent,'Total Online Payment',$bookings),
                        array('Manual Booking',$web_booking,$total_web_percent,'Grooming Booking',$grooming,$total_grooming_percent,'Cancelled Booking', $totalCancelBooking,$cancel_percent),
                        array('Mobile App Bookings',$mobile_booking,$total_mobile_percent,'Training Booking',$training,$total_training_percent,/*'Unconfirm ', $totalUnconfirmBooking,$unconfirm_percent,*/'No Show ', $totalNoShow,$noshow_percent),
                        /*array('','','','Kennel Booking',$walker,$total_walker_percent),*/
                        array('','','','','',''),
                        array('Customer ID','Customer Name','Pet Name','Transaction Number','Appointment date and time','Booking Date and time', 'Service category','Service','Booked From','Booking Status','Payment Status','Payment Online')
                    ), null, 'A1', false, false);
                    $sheet->fromArray($items, null, 'A1', false, false);
                });
            })->export('xls');
        }else{
            return back()->with('message', 'No data found.');;
        }
    }
}
