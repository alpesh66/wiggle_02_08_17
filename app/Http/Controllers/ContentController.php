<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\StoreContentRequest;
use App\Http\Controllers\Controller;

use App\Content;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Datatables;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }

        return view('content.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables()
    {
        $content = Content::select(['id', 'name','name_ar','status']);
        
        $datatables =  app('datatables')->of($content)

            ->addColumn('action', function($content) {
                return view('content.action', compact('content'))->render();
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif');

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(StoreContentRequest $request)
    {
        
        Content::create($request->all());

        Session::flash('flash_message', 'Content added!');

        return redirect('content');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $content = Content::findOrFail($id);

        return view('content.show', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $content = Content::findOrFail($id);

        return view('content.edit', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, StoreContentRequest $request)
    {
        
        $content = Content::findOrFail($id);

        $request['status'] = (isset($request->status))? 1 : 0;

        $content->update($request->all());

        Session::flash('flash_message', 'Content updated!');

        return redirect('content');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Content::destroy($id);

        Session::flash('flash_message', 'Content deleted!');

        return redirect('content');
    }
}
