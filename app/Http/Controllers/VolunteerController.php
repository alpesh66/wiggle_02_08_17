<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Volunteer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Inbox;
use App\Customer;
use App\Http\Requests\VolunteerRequest;
use Illuminate\Support\Facades\Config;
use Mail;

class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        /*$volunteers = Volunteer::latest()->allMessage()
                    ->paginate(15);
        return view('society/volunteers.index', compact('volunteers'));*/
        if($request->ajax())
        {            
            $volunteers = Volunteer::
            select(['volunteers.id', 'volunteers.created_at', 'volunteers.updated_at', 'volunteers.description','volunteers.status',/*'adopt_pet.pet_id','pets.breed','pets.name as petName',*/ 'volunteers.parent_id','volunteers.is_read','volunteers.sender','volunteers.customer_id',\DB::raw('CONCAT(customers.firstname, " ", customers.lastname) AS name')])
            /*->leftjoin('pet_volunteer', 'pet_volunteer.volunteer_id', '=', 'volunteers.id')
            ->leftjoin('pets', 'pet_volunteer.pet_id', '=', 'pets.id')*/
            ->leftjoin('customers', 'customers.id', '=', 'volunteers.customer_id')
            ->where('volunteers.provider_id', session('provider_id'))
            ->where('volunteers.sender', 1)
            ->groupBy('volunteers.id')
            /*->orderBy('volunteers.created_at','DESC')*/;

            $datatables =  app('datatables')->of($volunteers)

                ->addColumn('action', function($volunteers) {
                    return view('society.volunteers.action', compact('volunteers'))->render();
                })
                ->editColumn('name', function($volunteers) {
                    return "<a style='font-weight: bold;' href='".url('societies/customers/'.$volunteers->customer_id)."' >".$volunteers->name."</a>";
                })
                /*->addColumn('petType', function($volunteers) {
                    if($volunteers->breed==1)
                        return 'Cat';
                    else
                        return 'Dog';
                })
                ->editColumn('petName', function($volunteers) {
                    return "<a style='font-weight: bold;' href='".url('societies/pet/'.$volunteers->pet_id)."' >".$volunteers->petName."</a>";
                })*/
                ->editColumn('updated_at', function($volunteers) {
                    if($volunteers->parent_id==0)
                        return NULL;
                    else
                        return $volunteers->updated_at;
                })
                ->editColumn('status', '@if($status == 1) Approved @elseif($status == 2) Reject @else Pending  @endif')
                ->setRowClass(function ($volunteers) {
                    if($volunteers->is_read == 1 && $volunteers->sender == 1)
                        return "read_color";
                    else
                        return "no_color";
                });

            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'approved'){
                    $post = '1';
                } elseif(strtolower($val) == 'reject'){
                    $post = '2';
                }else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('society/volunteers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('/society/volunteers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Volunteer::create($request->all());

        Session::flash('flash_message', 'Volunteer added!');

        return redirect('volunteer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $volunteer = Volunteer::findOrFail($id);
        $volunteer->is_read = "0";
        $volunteer->save();
        $customer = Customer::with('pet')->findOrFail($volunteer->customer->id);
        
        return view('society/volunteers.show', compact('volunteer','customer'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $volunteer = Volunteer::findOrFail($id);

        return view('society/volunteers.edit', compact('volunteer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $foster = Volunteer::findOrFail($id);
        $foster->update($request->all());

        Session::flash('flash_message', 'Volunteer updated!');

        return redirect('societies/volunteers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
      $ids = $request->id; 
      if(!empty($ids)){
          foreach ($ids as $key => $id) {
              Volunteer::destroy($id);
          }    
          return json_encode(array('status'=>'1','message'=>'Volunteer deleted!'));
      }else{
          Volunteer::destroy($id);
          Session::flash('flash_message', 'Volunteer deleted!');
          return redirect('societies/volunteers');
      } 
    }

    public function reply(VolunteerRequest $request){

        $volunteerUpdate = Volunteer::findOrFail($request->input('id'));
        $request['parent_id'] = $request->input('id');
        $status = $request->input('status');
        $description = $request->input('description');
        $customerid = $volunteerUpdate->customer_id;        
        /*$volunteerUpdate->update($request->all());*/
        $volunteerUpdate->update(['parent_id' =>$request->input('id'),'is_read'=>0,'status'=>$status]);
        
        $request['provider_id'] = session('provider_id');
        $request['parent_id'] = $request->input('id');
        $request['sender'] = 2;
        Volunteer::create($request->all());
//      send push notification to user            
      /*if($status==0){
          send_push_notification(push_messages('pending_volunteer.en.message'), push_messages('pending_volunteer.ar.message'),$customerid);
      }else*/
      $customer_email = false;
      $provider_email = false;            
      $booking_date = false;
      $booking_time = false;
      $customers_mobile = false;
      $data = array();
      $sikw_email = env('SIKW_BCC_COPY', false);

      $volunteers = Volunteer::findOrFail($request->input('id'));
      $customer_id=$volunteers->customer_id;
      $provider_id=$volunteers->provider_id;
      $fost= Volunteer::SingleBookingDetail($volunteers->id)->first(); 
      $customer_email = $fost['customer_email'];
      $provider_email = $fost['providers_email'];
      $provider_name = $fost['provider_name'];
      $email_datacustomer_name = $fost['customer_name'];
      $customer_name = $fost['customer_name'];
        if($status==1){
            $pushMessage_en=push_messages('approved_volunteer.en.message');
            $pushMessage_ar=push_messages('approved_volunteer.ar.message');
            
            $request_id=$volunteers->id;
            $extraData=array('type'=>'VOLUNTEER','reminder'=>FALSE);
            $startdate=$fost['startdate'];
            $enddate=$fost['enddate'];
            $email_dataprovider_name = $fost['provider_name'];
              
            $message = "Dear {$email_datacustomer_name}, Your Volunteer with {$email_dataprovider_name} for ";
            $messagePro="Dear {$email_dataprovider_name}, New Volunteer Request from {$email_datacustomer_name} for ";
            
            $message .= "date between  {$startdate} and {$enddate} is approved.";
            $messagePro .= "date between  {$startdate} and {$enddate} is approved.";
            
            $customers_mobile = $fost['customers_mobile'];
                         // 
            $data['customer_name'] = $email_datacustomer_name;
            $data['startdate']=$startdate;
            $data['enddate']=$enddate;
            $data['provider_name'] = $email_dataprovider_name;
            $data['customers_mobile'] = $customers_mobile;
            $data['msgCust']="Your volunteer Request is apporved";
            $data['msgPro']="New volunteer Request from"." ".$email_datacustomer_name." is Approved";
                   
            /*add data in inbox(message)--==*/
            $startdate = Carbon::parse($startdate)->format('l, jS M Y h:i a');
            $enddate = Carbon::parse($enddate)->format('l, jS M Y h:i a');
            $reminder['message'] = "Dear ".$email_datacustomer_name.",<br/>Your volunteer request has been approved starting from ".$startdate." to ".$enddate.".";
            $reminder['provider_id'] = $provider_id;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 1;
            $reminder['type'] = 2;
            $reminder['customer_id'] = $customer_id;
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('approved_volunteer.en.title');
            $messageobj = Inbox::Create($reminder);         
            $message_id = $messageobj->id;
            sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
            /*--==*/

            if ($customer_email) {
                try {
                    /*Mail::queue('emails.volunteerEmailtoCustomer',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Volunteer request is approved");
                        if ($sikw_email) {
                            $message->bcc($sikw_email);
                        }
                    });  */
                    Mail::queue('emails.Volunteer_Approval',$data,function ($message) use ($customer_email, $sikw_email) {
                        $message->to($customer_email);
                        $message->subject("Volunteer request is Approved");
                        if ($sikw_email) {
                          $message->bcc($sikw_email);
                        }
                    }); 
                } catch (\Swift_TransportException $e) {
                    //file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoCustomer Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                      //file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoCustomer Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                      //log_message("ERROR", $e->getMessage());
                }
            }
            if ($provider_email) {
                try {
                    Mail::queue('emails.volunteerEmailtoProvider', $data, function ($messagePro) use ($provider_email, $sikw_email) {
                          $messagePro->to($provider_email);
                          $messagePro->subject("New Volunteer request is Approved");
                          if ($sikw_email) {
                            $messagePro->bcc($sikw_email);
                        }
                    });
                } catch (\Swift_TransportException $e) {
                      //file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoProvider Swift_TransportException : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                } catch (Exception $e) {
                      //file_put_contents($this->logFile, "\n\n =========VolunteerEmailtoProvider Exception : " . json_encode($e->getMessage()) . "\n\n", FILE_APPEND | LOCK_EX);
                      //log_message("ERROR", $e->getMessage());
                }
            }
      }elseif($status==2){
        $pushMessage_en=push_messages('reject_volunteer.en.message');
        $pushMessage_ar=push_messages('reject_volunteer.ar.message');
        $customer_id=$volunteers->customer_id;
        $provider_id=$volunteers->provider_id;

        $reminder['message'] = "Dear ".$email_datacustomer_name.",<br/>Your volunteer request has been rejected.";
        $reminder['provider_id'] = $provider_id;
        $reminder['service_id'] = 0;
        $reminder['booking_id'] = 0;
        $reminder['isnew'] = 0;
        $reminder['hadread'] = 1;
        $reminder['type'] = 2;
        $reminder['customer_id'] = $customer_id;
        $reminder['sender'] = 1;
        $reminder['subject'] = push_messages('reject_volunteer.en.title');
        $message = Inbox::Create($reminder);         
        $message_id = $message->id;

        $data['customer_name'] = $customer_name;
        $data['provider_name'] = $provider_name;
        $data['description'] = $description;

        Mail::queue('emails.Volunteer_Rejected',$data,function ($message) use ($customer_email, $sikw_email) {
            $message->to($customer_email);
            $message->subject("Volunteer request is Rejected");
            if ($sikw_email) {
              $message->bcc($sikw_email);
            }
        }); 

        $extraData=array('type'=>'VOLUNTEER','reminder'=>FALSE);
        sendPushNoti($pushMessage_en,$pushMessage_ar,$customer_id,$message_id);
      }
        return json_encode(array('status'=>'1','message'=>'Reply has been submited successfully.'));
    }
}
