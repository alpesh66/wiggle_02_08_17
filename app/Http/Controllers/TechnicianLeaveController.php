<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TechleaveRequest;

use Carbon\Carbon;
use App\TechnicianLeave;
use Illuminate\Http\Request;
use Session;

class TechnicianLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $techleave = TechnicianLeave::with('technician')->paginate(25);
        //echo '<pre>'; print_r($techleave); exit;
        if ($request->ajax()){
            $provider_id = session('provider_id');
            // $technicianholiday = TechnicianHoliday::with('technician')->where('provider_id', '=', $provider_id);
            $technicianleave = TechnicianLeave::select(['technician_leaves.id as tid','technician_leaves.weekday','technician_leaves.start','technician_leaves.toend','technician_leaves.breakstart as stime','technician_leaves.breakend as etime','technicians.title','technicians.name','technician_leaves.type as type'])
                ->join('technicians', 'technicians.id', '=', 'technician_leaves.technician_id')
                ->where('technician_leaves.provider_id', session('provider_id'));

            $datatables =  app('datatables')->of($technicianleave)
                ->editColumn('start', function($technicianleave) {
                    if($technicianleave->type == '1'){
                        $start = $technicianleave->start.' '.$technicianleave->stime;
                        $start = Carbon::parse($start)->format('l jS F Y h:i a');
                    } else {
                        $start = $technicianleave->start;
                        $start = Carbon::parse($start)->format('l jS F Y');
                    }
                    return $start;
                })
                ->editColumn('toend', function($technicianleave) {
                    if($technicianleave->type == '1'){
                        $end = $technicianleave->toend.' '.$technicianleave->etime;
                        $end = Carbon::parse($end)->format('l jS F Y h:i a');
                    } else {
                        $end = $technicianleave->toend;
                        $end = Carbon::parse($end)->format('l jS F Y');
                    }
                    return $end;
                })
                ->editColumn('type', function($technicianleave) {
                    if($technicianleave->type == '0'){
                        $leavetype = 'Full Day Off ';
                    } else {
                        $leavetype = 'Half Day Off ';
                    }
                    return $leavetype;
                })
                ->addColumn('action', function($technicianleave) {
                    return view('servicepro.technicianleave.action', compact('technicianleave'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.technicianleave.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $provider_id = Session::get('provider_id');
        $technicians = \App\Technician::where('provider_id', $provider_id )->pluck('name','id');

        return view('servicepro.technicianleave.create',compact('technicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TechleaveRequest $request)
    {        
        $provider_id = Session::get('provider_id');
        $requestData = $request->all();
        
        $requestData['provider_id'] = $provider_id;  
        $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
        $requestData['toend'] = date('Y-m-d',strtotime($requestData['toend']));  
        
        if(isset($requestData['status']) && $requestData['status'] == 'on'){
           $requestData['status'] = 1; 
        } else {
            $requestData['status'] = 0;
        }

        TechnicianLeave::create($requestData);

        Session::flash('flash_message', ' added!');

        return redirect('providers/technicianleave');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $technicianleave = TechnicianLeave::with('technician')->findOrFail($id);
        // echo '<pre>'; print_r($technicianleave); exit;
        return view('servicepro.technicianleave.show', compact('technicianleave'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $provider_id = Session::get('provider_id');
        $technicianleave = TechnicianLeave::findOrFail($id);
        $technicians = \App\Technician::where('provider_id', $provider_id )->pluck('name','id');
        // echo '<pre>'; print_r($technicianleave); exit;
        return view('servicepro.technicianleave.edit', compact('technicianleave','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, TechleaveRequest $request)
    {
        
        $requestData = $request->all();
        
        $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
        $requestData['toend'] = date('Y-m-d',strtotime($requestData['toend']));  
        
        if(isset($requestData['status']) && $requestData['status'] == 'on'){
           $requestData['status'] = 1; 
        } else {
            $requestData['status'] = 0;
        }

        // echo '<pre>'; print_r($requestData); exit;
        $techleave = TechnicianLeave::findOrFail($id);
        $techleave->update($requestData);

        Session::flash('flash_message', ' updated!');

        return redirect('providers/technicianleave');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TechnicianLeave::destroy($id);

        Session::flash('flash_message', ' deleted!');

        return redirect('providers/technicianleave');
    }
}
