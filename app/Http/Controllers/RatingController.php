<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rating;
use App\Http\Requests;
use Session;
use Datatables;
use DB;

class RatingController extends Controller {

//
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
// $provider_id = session('provider_id');
// $ratings = Rating::with('customer')->where('provider_id', '=', $provider_id)->paginate(10);
        if ($request->ajax()) {
            $user = \Auth::user();
            if ($user->can(super_admin_roles())) {
                $ratings = Rating::select(['ratings.id as rid', 'ratings.rate', 'ratings.rating_for','ratings.review', 'ratings.status', 'customers.firstname', 'customers.middlename', 'customers.lastname', 'customers.email', 'customers.mobile','stores.name','stores.name as store_name','providers.name as provider_name', 'ratings.customer_id'])
// $ratings = DB::table('customers')
                                ->leftJoin('customers', function ($join) {
                                    $join->on('customers.id', '=', 'ratings.customer_id')
                                    ->where('ratings.anonymous', '=', 0);
                                })->join('stores','stores.id','=','ratings.provider_id')
                                ->join('providers','providers.id','=','ratings.provider_id');
                                        /*->where('rating_for', 'Store')*/;
            } else {
                $ratings = Rating::select(['ratings.id as rid', 'ratings.rating_for', 'ratings.rate', 'ratings.review', 'ratings.status', 'customers.firstname', 'customers.middlename', 'customers.lastname', 'customers.email', 'customers.mobile','stores.name as store_name','providers.name as provider_name', 'ratings.customer_id'])
// $ratings = DB::table('customers')
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })->leftjoin('stores','stores.id','=','ratings.provider_id')
                                ->leftjoin('providers','providers.id','=','ratings.provider_id')
                        ->where('ratings.provider_id', session('provider_id'));
            }
// ->select(['customers.id', 'firstname','middlename','lastname', 'email','mobile', 'customers.status']);

            $datatables = app('datatables')->of($ratings)
                    ->addColumn('action', function($ratings) {
                        return view('servicepro.ratings.action', compact('ratings'))->render();
                    })
                    ->editColumn('status', '@if($status) Approved @else Pending @endif')
                    //->editColumn('firstname', '@if($firstname==null) Anonymous @else {{$firstname}} @endif')
                    ->editColumn('firstname', function($ratings) {
                        if(strpos(\Request::path(),'societies/ratings') !==FALSE)
                            $setActionUser = 'societies/customers'; 
                        else if(strpos(\Request::path(),'providers/ratings') !==FALSE)
                            $setActionUser = 'providers/customers'; 
                        else
                            $setActionUser = 'customers'; 
                        if($ratings->firstname==null){
                            return "Anonymous";
                        }else{
                        return "<u><i class='fa fa-eye'></i><a href=".url($setActionUser.'/'.$ratings->customer_id)." >".$ratings->firstname.' '.$ratings->lastname."</a></u>";    
                        }
                        
                    })
                    ->editColumn('rating_for', '@if($rating_for=="Provider") {{ $provider_name }} PawPages @else {{ $store_name }} {{ $rating_for }} @endif');
// ->editColumn('name', function($ratings) {
// return $ratings->name;
// });
// additional Search parameter
            $post = $datatables->request->get('post');
            $operator = $datatables->request->get('operator');
            $name = $datatables->request->get('name');

            /*if (isset($operator) && $operator!=" " && $operator == 'like') {
                $post = '%' . $post . '%';
            }*/

            if ($name && $name == 'status') {
                $val = $post;
                if (strtolower($val) == 'active') {
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '') {
                $datatables->where($name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.ratings.index');
// return view('servicepro.ratings.index', compact('ratings'));
    }

    public function show($id) {
        $rating = Rating::with('customer')->findOrFail($id);
// dd($rating->toArray());
        return view('servicepro.ratings.show', compact('rating'));
    }

    public function destroy(Request $request,$id) {
        
        $ids = $request->id;        
        foreach ($ids as $key => $id) {
            $ratings = Rating::findOrFail($id);
            $stars = $ratings->rate;

            if ($ratings->status) {
                \DB::table('providers')
                        ->where('id', session('provider_id'))
                        ->update([
                            'count_votes' => DB::raw('count_votes - 1'),
                            'stars' => DB::raw('stars - ' . $stars)]);
            }
            $ratings->delete();    
        }
        $user = \Auth::user();
        if ($user->can(super_admin_roles())):
            $url = 'ratings/';
        elseif ($user->can(provider_admin_roles())):
            $url = 'providers/ratings/';
        elseif ($user->can(society_admin_roles())):
            $url = 'societies/ratings/';
        endif;
        if ($request->ajax()) {
            $data['success']=TRUE;
            $data['message']='deleted!';
            return $data;
        }else{
            Session::flash('flash_message', 'deleted!');
            return redirect($url);
        }
        
    }

    public function changestatus(Request $request) {
//echo '<pre>'; print_r($request);

        $requestData = $request->all();

        $rating = Rating::findOrFail($requestData['id']);
        $rating->update($request->all());

        $status = $request->input('status');
        $stars = $rating->rate;
        if ($status == 1) {
// Auto increments and add in Provider column
            \DB::table('providers')
                    ->where('id', session('provider_id'))
                    ->update([
                        'count_votes' => DB::raw('count_votes + 1'),
                        'stars' => DB::raw('stars + ' . $stars),
            ]);
        } else if ($status == 0) {
            \DB::table('providers')
                    ->where('id', session('provider_id'))
                    ->update([
                        'count_votes' => DB::raw('count_votes - 1'),
                        'stars' => DB::raw('stars - ' . $stars)]);
        }

        return json_encode(array('status' => '1', 'message' => 'Status has been updated successfully.'));
    }

}
