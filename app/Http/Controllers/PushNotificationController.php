<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\EventRequest;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Event;
use App\Provider;
use App\Category;
use App\Customer;
use App\Booking;
use App\Type;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Excel;
use App\Inbox;


class PushNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function forAll(Request $request)
    {
        $customers= Customer::where('status',1)->pluck('email', 'id');
        $type=array("1"=>"Cat","2"=>"Dog");
        
        return view('push.createall', compact('customers','type'));
    }
    public function forAppointment(Request $request)
    {
        $appointment=array("1"=>"Booked","2"=>"Not Booked");
        $providers= Provider::where('status',1)->pluck('name', 'id');
        return view('push.createappointment', compact('providers','appointment'));
    }
    public function sendpushForAll(Request $request)
    {
        $type=$request->input('type');
        $android=$request->input('android');
        $ios=$request->input('ios');
        $customers=$request->input('customers');
        $message_en=$request->input('message_en');
        if(empty($customers)){
            if(!empty($type)){
                $customerData = \DB::table('customer_pet')->select('customers.id','customer_pet.customer_id','customer_pet.pet_id')
                            ->join('customers', 'customer_pet.customer_id', '=', 'customers.id')
                            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id')
                            ->whereIn('pets.breed',$type)
                            ->get();
                
            }else{
                $customerData = \DB::table('customer_pet')->select('customers.id','customer_pet.customer_id','customer_pet.pet_id')
                            ->join('customers', 'customer_pet.customer_id', '=', 'customers.id')
                            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id')
                            ->get();    
            }
            $customerData = json_decode(json_encode($customerData), true);
            $customer_ids=array();
            foreach ($customerData as $key => $value) {
                $customer_ids[$key] = $value['customer_id'];
            }
            $total_customer_ids = array_unique($customer_ids);
            /*echo "<pre>";
            echo "blank customers";
            print_r($total_customer_ids);
            exit();*/
            foreach ($total_customer_ids as $key => $customer_id) {
                //echo $customer_id;
                $reminder['message'] = $message_en;
                $reminder['provider_id'] = 5000;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 0;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customer_id;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('wiggle_admin.en.message');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;
                sendPushNoti($message_en, push_messages('wiggle_admin.ar.message'),$customer_id, $message_id);
            }

            /*$customers= Customer::where('status',1)->pluck('devicetype','id');
            foreach ($customers as $key => $value) {
                if($value==$android){
                    $reminder['message'] = $message_en;
                    $reminder['provider_id'] = 5000;
                    $reminder['service_id'] = 0;
                    $reminder['booking_id'] = 0;
                    $reminder['isnew'] = 0;
                    $reminder['hadread'] = 0;
                    $reminder['type'] = 1;
                    $reminder['customer_id'] = $key;
                    $reminder['sender'] = 1;
                    $reminder['subject'] = push_messages('wiggle_admin.en.message');
                    $message = Inbox::Create($reminder);         
                    $message_id = $message->id;
                    sendPushNoti($message_en, push_messages('wiggle_admin.ar.message'),$key, $message_id);
                    
                }
                if($value==$ios){
                    $reminder['message'] = $message_en;
                    $reminder['provider_id'] = 5000;
                    $reminder['service_id'] = 0;
                    $reminder['booking_id'] = 0;
                    $reminder['isnew'] = 0;
                    $reminder['hadread'] = 0;
                    $reminder['type'] = 1;
                    $reminder['customer_id'] = $key;
                    $reminder['sender'] = 1;
                    $reminder['subject'] = push_messages('wiggle_admin.en.title');
                    $message = Inbox::Create($reminder);         
                    $message_id = $message->id;
                    sendPushNoti(push_messages('wiggle_admin.en.message'), push_messages('wiggle_admin.ar.message'),$key, $message_id);
                }
            }*/
        }
        else{
            $type=$request->input('type');

            if(!empty($type)){
                $customerData = \DB::table('customer_pet')->select('customers.id','customer_pet.customer_id','customer_pet.pet_id')
                            ->join('customers', 'customer_pet.customer_id', '=', 'customers.id')
                            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id')
                            ->whereIn('pets.breed',$type)
                            ->whereIn('customers.id',$customers)
                            ->get();
            }else{
                $customerData = \DB::table('customer_pet')->select('customers.id','customer_pet.customer_id','customer_pet.pet_id')
                            ->join('customers', 'customer_pet.customer_id', '=', 'customers.id')
                            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id')
                            ->whereIn('customers.id',$customers)
                            ->get();    
            }
            $customerData = json_decode(json_encode($customerData), true);

            $customer_ids=array();
            foreach ($customerData as $key => $value) {
                $customer_ids[$key] = $value['customer_id'];
            }
            $total_customer_ids = array_unique($customer_ids);
            /*echo "<pre>";
            echo "string";
            print_r($total_customer_ids);
            exit();
            */
            foreach ($total_customer_ids as $key => $customer_id) {
                //echo $customer_id;
                $reminder['message'] = $message_en;
                $reminder['provider_id'] = 5000;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 0;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $customer_id;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('wiggle_admin.en.message');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;
                sendPushNoti($message_en, push_messages('wiggle_admin.ar.message'),$customer_id, $message_id);
            }
            /*foreach ($customers as $key => $value) {
                $reminder['message'] = $message_en;
                $reminder['provider_id'] = 5000;
                $reminder['service_id'] = 0;
                $reminder['booking_id'] = 0;
                $reminder['isnew'] = 0;
                $reminder['hadread'] = 0;
                $reminder['type'] = 1;
                $reminder['customer_id'] = $value;
                $reminder['sender'] = 1;
                $reminder['subject'] = push_messages('wiggle_admin.en.message');
                $message = Inbox::Create($reminder);         
                $message_id = $message->id;
                sendPushNoti($message_en, push_messages('wiggle_admin.ar.message'),$value, $message_id);
            }*/
        }
        Session::flash('message', "Notification sent successful.");
        return back();
    }    
    public function sendpushForAppointment(Request $request)
    {
        if(isset($request->start) && $request->start!=""){
            $start = $request->start;
            $start=date_create($start);
            $book_start = date_format($start, 'Y-m-d');    
        }else{
            $startdate = "";
            $book_startdate ="";
        }
        if(isset($request->end) && $request->end!=""){
            $end = $request->end;
            $end = date_create($end);
            $book_end = date_format($end, 'Y-m-d');
        }else{
            $book_end ="";
        }      
        //$type=$request->input('appointment');
        $appointment=$request->input('appointment');
        /*print_r($appointment);
        exit();*/
        $providers=$request->input('providers');
        $message_en=$request->input('message_en');
    
        $customer = Customer::leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
            ->select(['customers.id', 'firstname','bookings.start','bookings.end', DB::raw('MAX(bookings.start) AS bid')])
            ->join('bookings', 'bookings.customer_id', '=', 'customers.id')
            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id');

        /*if(!empty($type)){
            $customer->whereIn('pets.breed',$type);
        }*/
        if(!empty($providers))
            $customer -> whereIn('bookings.provider_id', $providers);
        if(!empty($appointment)){
            if(isset($appointment[0]) && $appointment[0]==1 && isset($appointment[1]) && $appointment[1]==2){
                //booked or not booked means all record which is under date validation
                /*if(isset($book_start)  && $book_start!=""){
                    $customer->where('bookings.start','<=',$book_start);
                    //$customer->where('bookings.end','<=',$book_start);
                 
                }
                if(isset($book_end)  && $book_end!=""){
                    $customer->where('bookings.start','>=',$book_end);
                    //$customer->where('bookings.end','>=',$book_end);   
                }   */
            }else if(isset($appointment[0]) && $appointment[0]==1){
                if(isset($book_start)  && $book_start!="")
                    $customer->where('bookings.start','>=',$book_start);

                if(isset($book_end)  && $book_end!="")
                    $customer->where('bookings.start','<=',$book_end);
            }elseif(isset($appointment[0]) && $appointment[0]==2){
                if(isset($book_start)  && $book_start!="")
                    $customer->where('bookings.start','>=',$book_start);

                if(isset($book_end)  && $book_end!="")
                    $customer->where('bookings.start','<=',$book_end);

                /*if(isset($book_start)  && $book_start!=""){
                    $customer->where('bookings.start','<=',$book_start);
                    //$customer->where('bookings.end','<=',$book_start);
                }
                if(isset($book_end)  && $book_end!=""){
                    $customer->orwhere('bookings.start','>=',$book_end);
                    //$customer->where('bookings.end','>=',$book_end);   
                }*/
            }
        }else{
            //appointment no select then date is not consider
            /*if(isset($book_start)  && $book_start!=""){
                $customer->where('bookings.start','<=',$book_start);
                $customer->where('bookings.end','<=',$book_start);
                 
            }
            if(isset($book_end)  && $book_end!=""){
                $customer->orwhere('bookings.start','>=',$book_end);
                $customer->where('bookings.end','>=',$book_end);   
            }*/
        }
        $result = $customer->orderBy('bookings.start','DESC')->groupBy('bookings.customer_id')->get()->toArray();
        if(isset($appointment[0]) && $appointment[0]==2){
            $customerArray=array();
            foreach ($result as $key => $value) {
                $customerArray[] = $value['id'];
            }
            
            $customer = Customer::leftjoin('customer_pet', 'customer_pet.customer_id', '=', 'customers.id')
            ->select(['customers.id', 'firstname','bookings.start','bookings.end', DB::raw('MAX(bookings.start) AS bid')])
            ->join('bookings', 'bookings.customer_id', '=', 'customers.id')
            ->join('pets', 'pets.id', '=', 'customer_pet.pet_id')
            ->whereNotIn('bookings.customer_id',$customerArray);
            $result = $customer->orderBy('bookings.start','DESC')->groupBy('bookings.customer_id')->get()->toArray();

        }
        /*echo "<pre>";
        print_r($result);
        exit();*/
        /*
        * Send All Customer Notification
        */
        if($appointment === null) {
            $result = $customers= Customer::where('status',1)->get()->toArray();
        }
        
        
        
        foreach ($result as $key => $value) {
            $reminder['message'] = $message_en;
            $reminder['provider_id'] = 5000;
            $reminder['service_id'] = 0;
            $reminder['booking_id'] = 0;
            $reminder['isnew'] = 0;
            $reminder['hadread'] = 0;
            $reminder['type'] = 1;
            $reminder['customer_id'] = $value['id'];
            $reminder['sender'] = 1;
            $reminder['subject'] = push_messages('wiggle_admin.en.message');
            $message = Inbox::Create($reminder);         
            $message_id = $message->id;
            sendPushNoti($message_en, push_messages('wiggle_admin.ar.message'),$value['id'], $message_id);
        }
        Session::flash('message', "Notification sent successful.");
        return back();
    }    

}
