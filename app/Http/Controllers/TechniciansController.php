<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TechnicianRequest;
use App\Http\Requests\TechRequest;

use App\Technician;
use App\TechnicianTiming;
use App\TechnicianBreak;
use App\ProviderTiming;
use Illuminate\Http\Request;
use Session;
use Image;

class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {   
        $provider_id = session('provider_id');
        // $technicians = Technician::where('provider_id', '=', $provider_id)->paginate(10);
            
        if ($request->ajax()){
            
            $technicians = Technician::where('provider_id', '=', $provider_id);

            $datatables =  app('datatables')->of($technicians)

                ->addColumn('action', function($technicians) {
                    return view('servicepro.technicians.action', compact('technicians'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }

        return view('servicepro.technicians.index', compact('technicians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $salutions = salution(); 
        $country = \App\Country::pluck('nicename','phonecode');
        $types = \App\Type::OfType(1);
        $provider_id = session('provider_id');
        $providerData = ProviderTiming::where('provider_id', '=', $provider_id)->get();
        
        return view('servicepro.technicians.create',compact('salutions','country','types','providerData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TechRequest $request)
    {
        $request['number'] = $request->input('country').'-'.$request->input('number');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        $requestData = $request->all();
        $provider_id = Session::get('provider_id');
        // $technicians = new Technician;
// dd($request->all());
        if ($request->hasFile('image')) {
            $data = $request->all();
            $data['provider_id'] = $provider_id;

            $destinationPath = public_path('uploads/technicians');
            $photoname = date("YmdHis");
            
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;
                $new_filePath =  $destinationPath.'/'.$photo_englist;
                $img = Image::make($new_filePath)->fit(100)->save($thumb_path);

                $data['image'] = $photo_englist;
            }
            
            $technicians = Technician::create($data);
        }else {
            $request['provider_id'] = $provider_id;            
            $technicians = Technician::create($request->all());
        }

        //last insert id
        $id = $technicians->id;       

        $technicianTImin = $request->input('techtimes');
        $timingData = array();
        foreach ($technicianTImin as $key => $value) {
            if(count($value) == 2){
                $timingData[$key]['weekoff'] = 1;
            } else {
                $timingData[$key]['weekoff'] = 0;
            }

            $timingData[$key]['technician_id'] = $id;
            $timingData[$key]['provider_id'] = $provider_id;
            $timingData[$key]['weekday'] = $this->get_day($key);
            $timingData[$key]['starttime'] = $value['stime'];
            $timingData[$key]['endtime'] = $value['etime'];
            $timingData[$key]['status'] = '1';      

        }
        \DB::table('technician_timings')->insert($timingData);

        foreach($requestData['breaktime']['stime'] as $key => $stime){
            
            $breakdata = new TechnicianBreak();
            
            $etime = $requestData['breaktime']['etime'][$key];
            
            if(isset($requestData['breaktime']['day'][$key]) && $requestData['breaktime']['day'][$key] != ''){
                $day = $requestData['breaktime']['day'][$key];
                $weekoff = '0';
            } else {
                $day = $this->get_day($key);
                $weekoff = '1';
            }  

            //insert
            $breakdata->technician_id = $id;
            $breakdata->provider_id = $provider_id;
            $breakdata->weekday = $day;
            $breakdata->breakstart = $stime;
            $breakdata->breakend = $etime;
            $breakdata->status = '1';
            $technicians->techbreaktime()->save($breakdata);
        }

        Session::flash('flash_message', ' added!');
        return redirect('providers/technicians');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $technician = Technician::with('techtime','techbreaktime')->findOrFail($id);

        $country = \App\Country::pluck('nicename','phonecode');
        
        return view('servicepro.technicians.show', compact('technician','country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $salutions = salution();

        $technician = Technician::with('techtime','techbreaktime')->findOrFail($id);
        $types = \App\Type::OfType(1);   
        $country = \App\Country::pluck('nicename','phonecode');
        $country_code = countryCode_explode($technician['number']); 
        $provider_id = session('provider_id');
        $providerData = ProviderTiming::where('provider_id', '=', $provider_id)->get();
        return view('servicepro.technicians.edit', compact('technician', 'salutions','types','country_code','country','providerData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        // dump($request->all());
        $technicians = Technician::findOrFail($id);
        $provider_id = Session::get('provider_id');
        $request['number'] = $request->input('country').'-'.$request->input('number');
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        
        // Update the Technitian Time Data
        $get_Technicians_Timings = $request->input('techtimedata');
        foreach ($get_Technicians_Timings as $key => $value) {
            if(count($value) == 3){
                $get_Technicians_Timings[$key]['weekoff'] = 1;
            } else {
                $get_Technicians_Timings[$key]['weekoff'] = 0;
            }
            \DB::table('technician_timings')->where('id', $value['id'])->update($get_Technicians_Timings[$key]);
        }
        
        // Update the Tech Break
        $get_Techni_Breaks = $request->input('breaktime');
        foreach ($get_Techni_Breaks as $skey => $valu) {
            \DB::table('technician_breaks')->where('id', $valu['id'])->update($get_Techni_Breaks[$skey]);
        }

        if ($request->hasFile('image')) {
            
            $destinationPath = public_path('uploads/technicians');
            $photoname = date("YmdHis");
            $data = $request->all();
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);

                $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;
                $new_filePath =  $destinationPath.'/'.$photo_englist;
                $img = Image::make($new_filePath)->fit(100)->save($thumb_path);

                $data['image'] = $photo_englist;
            }
            
            $adsdata = $technicians->update($data);
        }else {            
            $adsdata = $technicians->update($request->all());
        }
       
        Session::flash('flash_message', 'Technician data updated!');

        return redirect('providers/technicians');
    }

    public function get_day($number){
        if($number == 0){
            $day = 'sunday';
        } else if($number == 1){
            $day = 'monday';
        } else if($number == 2){
            $day = 'tuesday';
        } else if($number == 3){
            $day = 'wednesday';
        } else if($number == 4){
            $day = 'thursday';
        } else if($number == 5){
            $day = 'friday';
        } else if($number == 6){
            $day = 'saturday';
        } else {
            $day = '';
        }

        return $day;
    }

    public function tech_time_exist($pid, $tid, $week)
    {
        $techcount = TechnicianTiming::where('provider_id', $pid)
            ->where('technician_id', $tid)
            ->where('weekday', $week)
            ->first();
        return $techcount;
    }

    public function tech_breaktime_exist($pid, $tid, $week)
    {
        $techcount = TechnicianBreak::where('provider_id', $pid)
            ->where('technician_id', $tid)
            ->where('weekday', $week)
            ->first();
        return $techcount;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        
        // Requests from the Datatables
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDelete = Technician::destroy($ids);
            \App\TechnicianBreak::whereIn('technician_id', $ids)->delete();
            \App\TechnicianHoliday::whereIn('technician_id', $ids)->delete();
            \App\TechnicianLeave::whereIn('technician_id', $ids)->delete();
            \App\TechnicianTiming::whereIn('technician_id', $ids)->delete();
            \DB::table('service_technician')->whereIn('technician_id', $ids)->delete();

            if($isDelete)
            {
                $data['success'] = true;
                $data['message'] = 'Technician deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Technician::destroy($id);

        Session::flash('flash_message', ' deleted!');

        return redirect('providers/technicians');
    }

    public function getActiveTechnician()
    {
        $techs = Technician::ActiveTechnicianList();
        $finalone = [];
        foreach ($techs->all() as $key => $value) {
            $oneitem['id'] = $key;
            $oneitem['value'] = $value;
            // $finalone[] = $oneitem;
            array_push($finalone, $oneitem);
        }
        return $finalone;
    }

    public function technicianWithService(Request $request)
    {
        $techid = $request->input('techID');
        $technicians = Technician::with('service')->where('id', $techid)->first();
        $get_services = '';
        // dd($technicians);
// $tehccc = $technicians->toArray();
// debug($technicians->category);

        if(empty($technicians)) {
            return json_encode('No Service found');
        }
        $fil_category = collect([]);
        foreach ($technicians->service as $services) {
            $make_category['parentid'] = $services->category->parentid;
            $make_category['categid'] = $services->category->id;
            $make_category['categname'] = $services->category->name;
            // $fil_category[] = $make_category;
            $fil_category->push($make_category);
            // $sendHtm .= '<option value='. $services->category->id . ' >'. $services->category->name . $services->category->parentid.'</option>';
        }

        $lara_magic = $fil_category->groupBy('parentid');
        $sendHtm = '';
        foreach ($lara_magic->all() as $key => $value) {
            $getCategName = \App\Category::select('name')->where('id', $key)->first();
            $sendHtm .= '<optgroup label='.$getCategName['name'].'>';
            foreach ($value as $v) {
                $sendHtm .= '<option value='.$v['categid'].' data-typeid='.$key.'>'. $v['categname'] .'</option>';
            }
            $sendHtm .= '</optgroup>';

        }
        return json_encode($sendHtm);
        // return $technicians->service;
        // return ($tehccc['number']);
    }
}
