<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Size;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {

        $size = Size::paginate(15);
        return view('size.index', compact('size'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('size.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Size::create($request->all());

        Session::flash('flash_message', 'Size added!');

        return redirect('size');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $size = Size::findOrFail($id);

        return view('size.show', compact('size'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $size = Size::findOrFail($id);

        return view('size.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $size = Size::findOrFail($id);
        $size->update($request->all());

        Session::flash('flash_message', 'Size updated!');

        return redirect('size');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Size::destroy($id);

        Session::flash('flash_message', 'Size deleted!');

        return redirect('size');
    }
}
