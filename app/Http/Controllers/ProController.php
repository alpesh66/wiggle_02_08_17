<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProController extends Controller
{
    public function vet()
    {
        return view('servicepro.vet');
    }

    public function technicians()
    {
        return view('servicepro.technicians');
    }

    public function viewtechnicians($id)
    {
        return view('servicepro.view', compact('id'));
    }

    public function usersend()
    {
        return 'hi';
    }
}