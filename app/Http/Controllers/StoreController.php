<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PetStoreRequest;
use App\Http\Controllers\Controller;

use App\Store;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Datatables;
use Validator;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }
        return view('store.index');
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callDatatables($request)
    {
        // $customers = Customer::paginate(15);
        // $customer = Customer::SearchPet();
        $store = Store::select(['id', 'name','name_ar','about','about_ar','block', 'floor','email','status','contact']);
        
        $datatables =  app('datatables')->of($store)

            ->addColumn('action', function($store) {
                return view('store.action', compact('store'))->render();
            })
            ->editColumn('status', '@if($status) Active @else Inactive @endif');

        // additional Search parameter
        $post       = $datatables->request->get('post');
        $operator   = $datatables->request->get('operator');
        $name       = $datatables->request->get('name');

        if($operator && $operator == 'like')
        {
            $post = '%'.$post.'%';
        }

        if($name && $name == 'status')
        {
            $val = $post;
            if(strtolower($val) == 'active'){
                $post = '1';
            } else {
                $post = '0';
            }
        }

        if ($post != '' ) {
            $datatables->where( $name, $operator, $post);
        }

        return $datatables->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $country = \App\Country::pluck('nicename','phonecode');
        $area = \App\Area::pluck('name','id');
        $type = array(
                        1=>"Gromming",
                        2=>"petStores",
                        3=>"Socities",
                        4=>"Dog friendly areas"
                );
        return view('store.create', compact('country','area','type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(PetStoreRequest $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);        
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        $request['contact'] = $request->input('country').'-'.$request->input('contact');
        if ($request->hasFile('image')) {
            $destinationPath = public_path('uploads/store');
            $photoname = date("YmdHis");
            $data = $request->all();
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                $data['image'] = $photo_englist;
            }
            Store::create($data);
        }else {
            Store::create($request->all());
        }

        Session::flash('flash_message', 'Store added!');

        return redirect('store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $store = Store::findOrFail($id);

        return view('store.show', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);
        $area = \App\Area::pluck('name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        $country_code = countryCode_explode($store['contact']);
        $type = array(
                    1=>"Gromming",
                    2=>"petStores",
                    3=>"Socities",
                    4=>"Dog friendly areas"
            );
        return view('store.edit', compact('store','country','area','country_code','type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, PetStoreRequest $request)
    {
        
        $store = Store::findOrFail($id);
        $status = ($request->input('status') == 'on') ? 1 : 0;
        $request['status'] = $status;
        $request['contact'] = $request->input('country').'-'.$request->input('contact');

        if ($request->hasFile('image')) {
            
            $destinationPath = public_path('uploads/store');
            $photoname = date("YmdHis");
            $data = $request->all();
            
            if($request->hasFile('image')){
                $file_extention = '.'.$request->file('image')->getClientOriginalExtension();
                $photo_englist = $photoname.$file_extention;
                $file_check = $request->file('image')->move($destinationPath, $photo_englist);
                // $data = $request->except(['image']);
                $data['image'] = $photo_englist;
            }

            $store->update($data);
        } else {            
            $store->update($request->all());
        }

        // $store->update($request->all());

        Session::flash('flash_message', 'Store updated!');

        return redirect('store');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDelete = Store::destroy($ids);
            if($isDelete)
            {
                $data['success'] = true;
                $data['message'] = 'Store deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        Store::destroy($id);

        Session::flash('flash_message', 'Store deleted!');

        return redirect('store');
    }

    public function providerDashboard()
    {
        echo 'Hello from get provider';
        // return redirect('store');
    }
}
