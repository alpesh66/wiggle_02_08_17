<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Image;
// use Symfony\Component\HttpFoundation\File\UploadedFile;

class GalleryController extends Controller
{
    public function index()
    {
        return view('servicepro.profile.gallery');
    }

    public function store(Request $request)
    {
        $newfile = $request->file('file');

        $destinationPath = public_path('uploads/gallery');
        $photoname = date("YmdHis");
        if($request->hasFile('file'))
        {
            $file_extention = '.'.$request->file('file')->getClientOriginalExtension();
            $file_name = $photoname.$file_extention;
            $file_check = $request->file('file')->move($destinationPath, $file_name);
            $data['file'] = $file_name;

            $thumb_path = $destinationPath.'/thumbnail/'.$file_name;
            $new_filePath =  $destinationPath.'/'.$file_name;
            // $img = \Image::make($destinationPath.$file_name)->resize(100,100)->save($thumb_path);
            $img = Image::make($new_filePath)->fit(100)->save($thumb_path);
            $data['provider_id'] = session('provider_id');
            $data['image']  = $file_name;
            $data['status'] = 1;
            $gallery_save   = \App\Gallery::create($data);
            return true;
        }

        return view('provider/gallery');
    }
}
