<?php

namespace App\Http\Controllers;

use App\Events\CustomerCreated;
use Illuminate\Http\Request;
use App\Events\BookingWasMade;
use App\Http\Requests;
use App\Customer;
use App\Pet;
use App\Inbox;
use App\Kennel;
use App\Booking;
use App\Service;
use Hash;
use Validator;
use Auth;
use Image;
use Mail;
use Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use DB;
use Illuminate\Support\Facades\Config;

class FrontApiController extends Controller {

    public function __construct() {
        try {
            $path = '../api_logs';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $this->logFile = '../api_logs/apilog' . date('d-m-Y') . '.txt';
            file_put_contents($this->logFile, "\n\n =========LOG[" . date('Y-m-d H:i:s') . "] =========\n\n", FILE_APPEND | LOCK_EX);
        } catch (Exception $e) {
            //
        }       
    }
    public function index()
    {
        echo "hii";
    }
    public function getConfig() {
        $response['status'] = 1;
        $response['constantsAll'] = Config::get('push_messages');
        return $response;
    }

    public function checkForValidation($validator) {
        $response['status'] = config('frontapi.Validation_ERROR');
        $response['message'] = $validator->errors()->first();
        $response['message_ar'] = config('frontapi.Invalid_AR');
        return $response;
    }

    private function sendResponse($status, $message, $message_ar, $data = '') {
        $response['status'] = $status;
        $response['message'] = $message;
        $response['message_ar'] = $message_ar;
        if ($data != '') {
            $response['data'] = $data;
        }
        return $response;
    }

    /**
     * [checkversion To check if new version is available or not]
     * @param  Request $request Takes deviceType and AlerNo to check new version
     * @return json array           return version details
     */
    
    public function checkversion(Request $request) {

        $device_type = $request->input('device_type');
        $alert_no = $request->input('alert_no');

        // Device Type: 1: iPhone, 2:Android
        $validator = Validator::make($request->all(), [
                    'device_type' => 'required|numeric|in:1,2',
                    'alert_no' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->checkForValidation($validator);
        }

        $updateAvail = false;
        $onlyOk = false;
        $appVer = 1;
        $iOS_appVer = 1;
        $cancelAr = 'فيما بعد';
        $cancelEn = 'Later';
        $okAr = 'تحديث';
        $okEn = 'Update';

        $messageAr = 'For best performance, please update your app to the latest version.';
        $messageEn = 'For best performance, please update your app to the latest version.';
        $iOSLink = 'https://goo.gl/qNzZB4';
        $androidLink = 'https://play.google.com';

        if ($alert_no < $appVer && $device_type == '2') {
            $updateAvail = true;
            $respCategories['only_ok'] = $onlyOk;
            $respCategories['link'] = $androidLink;
            $respCategories['cancel_ar'] = $cancelAr;
            $respCategories['cancel_en'] = $cancelEn;
            $respCategories['ok_ar'] = $okAr;
            $respCategories['ok_en'] = $okEn;
            $respCategories['message_ar'] = $messageAr;
            $respCategories['message_en'] = $messageEn;
        } else if ($alert_no < $iOS_appVer && $device_type == '1') { // iPhone Alert
            $updateAvail = true;
            $respCategories['only_ok'] = $onlyOk;
            $respCategories['link'] = $iOSLink;
            $respCategories['cancel_ar'] = $cancelAr;
            $respCategories['cancel_en'] = $cancelEn;
            $respCategories['ok_ar'] = $okAr;
            $respCategories['ok_en'] = $okEn;
            $respCategories['message_ar'] = $messageAr;
            $respCategories['message_en'] = $messageEn;
        }

        // Update final response
        $respCategories['update_available'] = $updateAvail;

        // View code
        $response['status'] = config('frontapi.SUCCESS');
        $response['message'] = config('frontapi.Valid');
        $response['message_ar'] = config('frontapi.Valid_AR');
        $response['data'] = $respCategories;
        return $response;
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email|max:255',
                    'password' => 'required_if:type,wg|min:6',
                    'type' => 'sometimes|required|in:fb,go,wg',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $email = $request->input('email');
        $type = $request->input('type', 'wg');
        $is_newCustomer = false;

        if ($type == 'wg') {
            $users = \Auth::guard('mobileweb')->once($request->only(['email', 'password']));
        } else {
            $users = \App\Customer::where(['email' => $email])->exists();
            if (!$users) {
                $users = \App\Customer::create(['email' => $email, 'api_token' => str_random(60), 'gender' => 2, 'status' => '1']);
                $is_newCustomer = true;
            }
        }

        if ($users) {
            $loginCustomer = \App\Customer::with('pet')->where(['email' => $email])->first();

            if ($loginCustomer->status == '0') {
                // If Customer is Not active Yet
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.InActive_User'), config('frontapi.InActive_User_AR')
                );
            } else {
                // All Set, send the user Data
                $loginCustomer['isNew'] = $is_newCustomer;
                return $this->sendResponse(
                                config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $loginCustomer
                );
            }
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid_credentials'), config('frontapi.Invalid_credentials_AR')
            );
        }
    }

    public function CustomerProfile() {
        $id = Auth::guard('mobileapp')->user()->id;
        $loginCustomer = \App\Customer::with('pet')->find($id);
        if ($loginCustomer->status == '0') {
            // If Customer is Not active Yet
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.InActive_User'), config('frontapi.InActive_User_AR')
            );
        } else {
            // All Set, send the user Data
            $current_date = date('Y-m-d');
            $current_totime = strtotime($current_date);
            $petDetail = json_decode(json_encode($loginCustomer->pet), true);
            foreach ($petDetail as $key => $value) {
                    
                    $kennelObje = Kennel::select('kennels.id','kennels.startdate','kennels.enddate','kennel_pet.pet_id')
                        ->leftjoin('kennel_pet', 'kennel_pet.kennel_id', '=', 'kennels.id')
                        ->where('kennels.customer_id',$id)
                        ->where('kennels.status',1)
                        ->where('kennels.sender',1)
                        ->where('kennel_pet.pet_id',$value['id'])
                        ->orderBy('kennels.id','DESC')->get()->toArray();
                        /*print_r($kennelObje);*/
                    if(!empty($kennelObje)){
                        $veternary = $kennelObje[0]['startdate'];
                        $veternaryend = $kennelObje[0]['enddate'];
                    }
                    else{
                        $veternary = "";
                        $veternaryend = "";
                    }
                    /*echo "pet id : ".$value['id'];*/
                    if($veternary == "")
                    {
                        /*$date = date_create($value['created_at']);
                        date_modify($date, $value['grooming']);
                        $next_veternary_date = date_format($date, 'Y-m-d');
                        $next_veternary = date_format($date, 'Y-m-d');*/
                        /*echo "here";*/
                         $next_veternary_totime = strtotime($value['next_vaccination_date']);
                        /*if($loginCustomer->pet[$key]['missyou'] != 1){*/
                            if($next_veternary_totime == $current_totime ){
                                Pet::find($value['id'])->update(['missyou' => 1]);
                                $loginCustomer->pet[$key]['missyou'] = 1; 
                            }else{
                                 /*if($value['id']==368)
                                    echo $next_veternary_totime ."string". $current_totime;*/

                              Pet::find($value['id'])->update(['missyou' => 0]);
                                $loginCustomer->pet[$key]['missyou'] = 0; 
                            }
                        /*}else{

                        }*/
                       

                        $loginCustomer->pet[$key]['next_veternary_date'] = $value['next_vaccination_date']; 
                        
                    }
                    else{
                        $enddate = date_create($veternaryend);
                        $end_veternary = date_format($enddate, 'Y-m-d');
                        $end_veternary_totime = strtotime($end_veternary);

                        /*echo*/ $loginCustomer->pet[$key]['next_veternary_date'] = $veternary; 
                        $loginCustomer->pet[$key]['veternaryend'] = $end_veternary; 
                        $date = date_create($veternary);
                        $next_veternary = date_format($date, 'Y-m-d');
                        $next_veternary_totime = strtotime($next_veternary);
                        /*if($loginCustomer->pet[$key]['missyou'] != 1){*/
                            if($next_veternary_totime <= $current_totime && $end_veternary_totime >= $current_totime){
                                /*echo "missyou";*/
                                /*if($value['id']==368)
                                    echo $next_veternary_totime ."string". $current_totime;*/
                                Pet::find($value['id'])->update(['missyou' => 1]);
                                $loginCustomer->pet[$key]['missyou'] = 1; 
                            }else{
                                /*echo "not missyou";*/
                                /*if($value['id']==368)
                                    echo $next_veternary_totime ."string". $current_totime;*/
                              Pet::find($value['id'])->update(['missyou' => 0]);
                                $loginCustomer->pet[$key]['missyou'] = 0; 
                            }
                        /*}*/
                        
                    }
                    if($value['groom'] == "")
                    {
                        /*$date = date_create($value['created_at']);
                        date_modify($date, $value['grooming']);
                        $next_groom_date = date_format($date, 'Y-m-d');
                        $loginCustomer->pet[$key]['next_groom_date'] = $next_groom_date;
                        Pet::find($value['id'])->update(['next_groom_appointment' => $next_groom_date]);
                        */
                        $loginCustomer->pet[$key]['next_groom_date'] = $value['next_groom_appointment'];
                        
                    }
                    else{
                        $loginCustomer->pet[$key]['next_groom_date'] = $value['groom']; 
                        $date = date_create($value['groom']);
                        date_modify($date, $value['grooming']);
                        $next_groom_date = date_format($date, 'Y-m-d');
                        $loginCustomer->pet[$key]['next_groom_date'] = $next_groom_date;
                        Pet::find($value['id'])->update(['next_groom_appointment' => $next_groom_date]);
                    }
            }            
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $loginCustomer
            );
        }
    }

    public function indexTypes() {
        $types = \App\Type::all();
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $types
        );
    }

    public function indexSizes() {
        $size = \App\Size::where('status', 1)->get();
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $size
        );
    }
                    
    public function registerCustomer(Request $request) {
        $validator = Validator::make($request->all(), [
                    'firstname' => 'required|max:100',
                    'middlename' => 'sometimes|max:100',
                    'lastname' => 'required|max:100',
                    'gender' => 'sometimes|in:0,1,2',
                    'email' => 'required|email|max:255|unique:customers',
                    'mobile' => 'required|unique:customers',
                    'password' => 'required|min:6',
                    'dob' => 'sometimes|date',
                    // 'latitude'    => 'required',
                    // 'longitude'    => 'required',
                    // 'block'    => 'required',
                    // 'street'    => 'required',
                    // 'judda'    => 'required',
                    // 'house'    => 'required',
                    // 'apartment' => 'required',
                    'photo' => 'sometimes|min:1255',
                    'pets.*.name' => 'sometimes|required|max:255',
                    'pets.*.breed' => 'sometimes|required|in:1,2',
                    'pets.*.dob' => 'sometimes|date',
                    'pets.*.size' => 'sometimes|required|integer',
                    'pets.*.gender' => 'sometimes|in:0,1,2',
                    'pets.*.height' => 'sometimes|string',
                    'pets.*.temperament' => 'sometimes|string',
                    'pets.*.origin' => 'sometimes|string',
                    'pets.*.chipno' => 'sometimes|string',
                    'pets.*.weight' => 'sometimes|string',
                    'pets.*.grooming' => 'sometimes|string',
                    'pets.*.photo' => 'sometimes|min:1255',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);




        $data = $request->except('pets');
        //print_r($data);exit;
        $data['api_token'] = str_random(60);
        $data['status'] = 1;
        $data['password'] = bcrypt($request->password);

        if ($request->input('photo')) {
            $pet_path = public_path('uploads/customer/');
            $pet_path_thumb = public_path('uploads/customer/thumbnail/');
            $photoname = date("YmdHis") . '-' . str_random(10) . '.png';

            $img = Image::make($request->input('photo'))->save($pet_path . $photoname);

            $img_thumb = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path_thumb . $photoname);

            $data['photo'] = $photoname;
        }

        if ($request->input('area_id') == 0) {
            $area_id = \App\Area::create(['country_id' => '114', 'name' => $request->input('area_name'), 'name_ar' => $request->input('area_name'), 'status' => '1'])->id;
            $data['area_id'] = $area_id;
        }
        
        if($data['dob']==""){
            $data['dob']="0000-00-00";
        }
        $pets = $request->input('pets');
        event(new CustomerCreated($customer = Customer::create($data)));

        $all_pet = '';
        if ($customer) {
            if (is_array($pets) && count($pets) > 0) {
                foreach ($pets as $pet) {
                    if (isset($pet['photo']) && strlen($pet['photo']) > 250 && !empty($pet['photo'])) {
                        $pet_path = public_path('uploads/pets/');
                        $pet_path_thumb = public_path('uploads/pets/thumbnail/');
                        $photoname = date("YmdHis") . '-' . str_random(10) . '.png';

                        $img = Image::make($pet['photo'])->save($pet_path . $photoname);

                        $img_thumb = Image::make($pet['photo'])->resize(200, 200)->save($pet_path_thumb . $photoname);

                        $pet['photo'] = $photoname;
                    }
                    $all_pet[] = \App\Pet::create($pet)->id;
                }
            }

            if (!empty($all_pet)) {

                $customer->pet()->attach($all_pet);
            }
            $updated_customer = Customer::with('pet')->find($customer->id);
            // dd($updated_customer);
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $updated_customer
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function allCommonData() {
        $data['breed'] = \App\Breed::Active()->get();
        $data['country'] = \App\Country::Active()->get();
        $data['area'] = \App\Area::Active()->get();
        $data['size'] = \App\Size::where('status', 1)->get();
        $data['specie_cat'] = \App\Specie::where('breed_id', 1)->get();
        $data['specie_dog'] = \App\Specie::where('breed_id', 2)->get();
        $data['frequency'] = groom_frequency();
        $data['vaccination_type']= \App\VaccinationTypes::where('status',1)->select(['id','type','breed','vaccination_duration'])->get();

        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
    }

    public function allCountryGetData() {
        $breed = \App\Country::Active()->get();
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $breed
        );
    }

    public function allBreedsGetData() {
        $breed = \App\Breed::Active()->get();
        return $this->sendResponse(
                        config('adminapi.SUCCESS'), config('adminapi.Valid'), config('adminapi.Valid_AR'), $breed
        );
    }

    public function allAreaGetData() {
        $breed = \App\Area::Active()->get();
        return $this->sendResponse(
                        config('adminapi.SUCCESS'), config('adminapi.Valid'), config('adminapi.Valid_AR'), $breed
        );
    }

    public function updateCustomer(Request $request, Customer $customer) {
        $id = Auth::guard('mobileapp')->user()->id;
        ini_set("memory_limit", -1);

        $validator = Validator::make($request->all(), [
                    'firstname' => 'sometimes|required|max:100',
                    'lastname' => 'sometimes|required|max:100',
                    'email' => 'sometimes|required|email|max:255|unique:customers,email,' . $id,
                    'mobile' => 'sometimes|required|unique:customers,mobile,' . $id,
                    'dob' => 'sometimes|date',
                    'address' => 'sometimes|max:255',
                    'gender' => 'sometimes|in:0,1,2',
                    'photo' => 'sometimes',
                        // 'latitude'    => 'required',
                        // 'longitude'    => 'required',
                        // 'block'    => 'required',
                        // 'street'    => 'required',
                        // 'judda'    => 'required',
                        // 'house'    => 'required',
                        // 'apartment' => 'required'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $customer = Customer::findOrFail($id);
        if ($request->input('photo')) {
            $pet_path = public_path('uploads/customer/');
            $pet_path_thumb = public_path('uploads/customer/thumbnail/');
            $photoname = date("YmdHis") . '-' . str_random(10) . '.png';

            $img = Image::make($request->input('photo'))->save($pet_path . $photoname);

            $img_thumb = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path_thumb . $photoname);

            $request['photo'] = $photoname;
        }
        $data = array();
        $data = $request->all();
        /* if($request->input('area_id') == 0) {
          $area_id = \App\Area::create(['country_id' => '114', 'name' => $request->input('area_name'),'name_ar' => $request->input('area_name'), 'status' => '1'])->id;
          $data['area_id'] = $area_id;
          } */

        if($data['dob']==""){
          $data['dob']="0000-00-00";
        }
        $customer->update($data);
        $customer->gender = (int) $customer->gender;
        if ($customer) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $customer
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function storePet(Request $request) {
        $customer = Auth::guard('mobileapp')->user();

        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'dob' => 'sometimes|date',
                    'breed' => 'required|in:1,2',
                    'size' => 'required|in:1,2,3,4,5',
                    'gender' => 'sometimes|in:0,1',
                    'height' => 'sometimes|string',
                    'temperament' => 'sometimes|string',
                    'origin' => 'sometimes|string',
                    'chipno' => 'sometimes|string',
                    'weight' => 'sometimes|string',
                    'grooming' => 'sometimes|string',
                    'photo' => 'sometimes',
                    'neutered' => 'sometimes|in:0,1',
                    'spayed' => 'sometimes|in:0,1'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        if ($request->input('photo')) {
            $pet_path = public_path('uploads/pets/');
            $pet_path_thumb = public_path('uploads/pets/thumbnail/');
            $photoname = date("YmdHis") . '-' . str_random(10) . '.png';

            $img = Image::make($request->input('photo'))->save($pet_path . $photoname);

            $img_thumb = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path_thumb . $photoname);

            $request['photo'] = $photoname;
        }
        
        $pet = \App\Pet::Create($request->all());
        $pet_id = $pet->id;
        $pet_dob = $request->input('dob');
        $breed = $request->input('breed');
        $grooming = $request->input('grooming');
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        $customer->pet()->attach($pet);

        if ($customer) {
            $customers = \App\Pet::find($pet->id);
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $customers
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function updatePet(Request $request) {
        $validator = Validator::make($request->all(), [
                    'id' => 'required|integer',
                    'name' => 'sometimes|required|max:255',
                    'dob' => 'sometimes|date',
                    'breed' => 'sometimes|in:1,2',
                    'size' => 'sometimes|in:1,2,3,4,5',
                    'gender' => 'sometimes|in:0,1,2',
                    'height' => 'sometimes|string',
                    'temperament' => 'sometimes|string',
                    'origin' => 'sometimes|string',
                    'chipno' => 'sometimes|string',
                    'weight' => 'sometimes|string',
                    'grooming' => 'sometimes|string',
                    'photo' => 'sometimes',
                    'neutered' => 'sometimes|in:0,1',
                    'spayed' => 'sometimes|in:0,1'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $pet = \App\Pet::findOrFail($request->id);

        if ($request->input('photo')) {
            $pet_path = public_path('uploads/pets/');
            $pet_path_thumb = public_path('uploads/pets/thumbnail/');
            $photoname = date("YmdHis") . '-' . str_random(10) . '.png';
            try {
                $img = Image::make($request->input('photo'))->save($pet_path . $photoname);
            } catch (\Exception $e) {
                return $this->sendResponse(
                                config('frontapi.ERROR'), $e->getMessage(), config('frontapi.Invalid_AR')
                );
            }

            try {
                $img_thumb = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path_thumb . $photoname);
            } catch (\Exception $ex) {
                return $this->sendResponse(
                                config('frontapi.ERROR'), $ex->getMessage(), config('frontapi.Invalid_AR')
                );
            }
            $request['photo'] = $photoname;
        }
        $pet_id = $request->input('id');
        $pet_dob = $request->input('dob');
        $grooming = $request->input('grooming');
        $breed = $request->input('breed');
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        /*echo "<pre>";
        print_r($request->all());
        exit();*/
        if ($pet) {
            $pet->update($request->all());
            $pet->gender = (int) $pet->gender;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $pet
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function storeMedicalCard(Request $request) {
        $validator = Validator::make($request->all(), [
                    'pet_id' => 'required|integer',
                    'givenon' => 'required|date|date_format:Y-m-d',
                    'vet' => 'required|string|max:255',
                    'institution' => 'required|string|max:255',
                    'photo' => 'required',
                    'vaccination_type_id'=>'sometimes|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $pet_path = public_path('uploads/pets/medicalcard/');
        $photoname = date("YmdHis") . '-' . str_random(10) . '.png';

        $img = Image::make($request->input('photo'))->resize(200, 200)->save($pet_path . $photoname);

        $request['photo'] = $photoname;

        $medical = \App\Medicalcard::Create($request->all());
        $medical->load('vaccination_type');
        /*--changes vaccination date in pet profile */
        $pet_id = $request->input('pet_id');
        $petData = \DB::table('pets')->select('id','dob','breed')
                                ->where('id',$pet_id)
                                ->first();        
        $pet_dob =$petData->dob;
        $breed =$petData->breed;
        $grooming = 0;
        setVaccinationGroomDate($pet_dob,$pet_id,$grooming,$breed);
        if ($medical) {

            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $medical
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function petMedicalgetAllData(Request $request) {
        $validator = Validator::make($request->all(), [
                    'pet_id' => 'required|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = $request->input('pet_id');
        $medical = \App\Medicalcard::where('pet_id', $id)->orderby('givenon', 'desc')->with('vaccination_type')->get();

        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $medical
        );
    }

    public function indexPet(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;
        $pet = Customer::find($id)->pet()->get();
        $petDetail = json_decode(json_encode($pet), true);
        
        foreach ($petDetail as $key => $value) {
                if($value['veternary'] == "")
                {
                    $date = date_create($value['created_at']);
                    date_modify($date, $value['grooming']);
                    $finishDate = date_format($date, 'Y-m-d H:i:s');
                    $petDetail[$key]['next_veternary_date'] = $finishDate; 
                    
                }
                else{
                    $petDetail[$key]['next_veternary_date'] = $value['veternary']; 
                }
                if($value['groom'] == "")
                {
                    $date = date_create($value['created_at']);
                    date_modify($date, $value['grooming']);
                    $finishDate = date_format($date, 'Y-m-d H:i:s');
                    $petDetail[$key]['next_groom_date'] = $finishDate; 
                    
                }
                else{
                    $petDetail[$key]['next_groom_date'] = $value['groom']; 
                }
        }
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $petDetail
        );
    }

    public function deleteCustomerPet(Request $request) {
        $validator = Validator::make($request->all(), [
                    'pet_id' => 'required|integer',
                    'disabled_question' => 'required'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        // TODO check if this pet belongsTo this user only
        $id = $request->input('pet_id');
        $deletePet = \App\Pet::findOrFail($id);
        $response = $deletePet->update($request->all());



        if ($response) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Desable'), config('frontapi.Desable_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

  public function indexProviders(Request $request) {
        $validator = Validator::make($request->all(), [
                    'type_id' => 'required_without:service_id|integer',
                    'service_id' => 'sometimes|required|integer',
                    'order_by' => 'sometimes|string|in:asc,desc',
                    'name' => 'sometimes|string',
                    'area' => 'sometimes|in:asc,desc',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);
        $provider = array();
        $service_id = $request->input('service_id');
        if ($service_id && $service_id > 0) {
            $provider_ids = \App\Service::where('category_id', $service_id)->select('provider_id')->get();
            // dd($provider_ids);
            //print_r($provider_ids); die();
//            foreach($provider_ids as $key => $val)
//            {
//                $provider['time']=DB::table('providers')
//                ->Join('provider_timings', 'providers.id', '=', 'provider_timings.provider_id')
//                ->select('provider_timings.starttime as Start_time','provider_timings.endtime as End_time')
//                ->take(1)
//                //->where('provider_timings.weekday','=>', $dt->format('d'))
//                ->where('providers.id',$val['provider_id'])                
//                ->get();
//               // echo $key .'='. $val;
//            }
         //   die();
           // echo "<pre>"; print_r($provider); die();
            $provider['provider'] = \App\Provider::with('areas')->whereStatus(1)->whereIn('id', $provider_ids)->whereDate('end', '>=', Carbon::now())->get();
            foreach($provider['provider'] as $key => $val)
            {
                $provider['provider'][$key]['time']=DB::table('providers')
                ->Join('provider_timings', 'providers.id', '=', 'provider_timings.provider_id')
                ->select('provider_timings.starttime as Start_time','provider_timings.endtime as End_time')
                ->take(1)
                //->where('provider_timings.weekday','=>', $dt->format('d'))
                ->where('providers.id',$val['id'])                
                ->get();
                
                $provider['provider'][$key]['rate']=\DB::table('ratings')
                        
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })
                        ->select(DB::raw('count(rate) as count,avg(rate) average_rate'))
                        ->where(['rating_for' =>'Provider', 'provider_id' => $val['id']])->get();
               // echo $key .'='. $val;
            }
        } elseif (!empty($request->input('type_id'))) {

            $provider['provider'] = \App\Provider::with('areas')->ApiSearch($request->all())->whereDate('end', '>=', Carbon::now())->get();
            
            foreach($provider['provider'] as $key => $val)
            {
                $provider['provider'][$key]['time']=DB::table('providers')
                ->Join('provider_timings', 'providers.id', '=', 'provider_timings.provider_id')
                ->select('provider_timings.starttime as Start_time','provider_timings.endtime as End_time')
                ->take(1)
                //->where('provider_timings.weekday','=>', $dt->format('d'))
                ->where('providers.id',$val['id'])                
                ->get();
               // echo $key .'='. $val;
            }
               
           
            $service_name = $request->input('name');
            $provider['service'] = \App\Category::where('parentid', '<>', 0)
                            ->where([['name', 'like', '%' . $service_name . '%'],
                                ['name_er', 'like', '%' . $service_name . '%']])->pluck('name', 'id');            
            
            
        }
//print_r($abc);die();
// dump(DB::getQueryLog());
        // $provider = \App\Provider::with(['type' => function ($query) use ($type_id) {
        //         $query->where('type_id', $type_id);
        //     }])->get();
        // dump(\DB::getQueryLog());
// Log::info(\DB::getQueryLog());
        // $log = new Logger('name');
// $log->pushHandler(new StreamHandler(storage_path().'/logs/sqlquery.log', Logger::WARNING));
// add records to the log
// $log->warning('dsfasdfd');
// $log->addInfo(\DB::getQueryLog());
//         $monolog = \Log::getMonolog('name');
//         dump($monolog);

        if (count($provider) > 0) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $provider
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function indexInboxData(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;
        $paging = $request->input('paging', 20);

        $bookings = \App\Inbox::select('inboxes.id', 'inboxes.provider_id', 'inboxes.customer_id', 'inboxes.service_id', 'inboxes.booking_id', 'inboxes.bookdate', 'inboxes.type', 'inboxes.isnew', 'inboxes.hadread', 'inboxes.subject', 'inboxes.message', 'providers.photo', 'bookings.status', 'bookings.ontime','bookings.start as appointmentdate','providers.name')
                        ->leftJoin('providers', 'providers.id', '=', 'inboxes.provider_id')
                        ->leftJoin('bookings', function($join) {
                            $join->on('bookings.id', '=', 'inboxes.booking_id')/*->where('inboxes.type', '=', 1)->where('bookings.status', '=', 0)*/;
                        })->where('inboxes.customer_id', '=', $id)->orderBy('inboxes.created_at', 'desc')->paginate($paging);
        // dd($bookings->all());
        foreach ($bookings->all() as $key => $value) {

            if (!empty($value['photo'])) {
                $bookings[$key]['photo'] = url('uploads/provider/') . '/' . $value['photo'];
                //print_R($value['photo']);
            }
            if ($value['status'] === 0) {
                $bookings[$key]['ask_for_conformation'] = 1;
            } else {
                $bookings[$key]['ask_for_conformation'] = 0;
            }
            unset($bookings[$key]['status']);
        }

        $data['inbox'] = $bookings;
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
    }
    public function indexInboxDataDetail(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;
        /*$paging = $request->input('paging', 20);*/
        $validator = Validator::make($request->all(), [
                    'inbox_id' => 'required|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $inbox = $request->input('inbox_id');
        $bookings = \App\Inbox::select('inboxes.id', 'inboxes.provider_id', 'inboxes.customer_id', 'inboxes.service_id', 'inboxes.booking_id', 'inboxes.bookdate', 'inboxes.type', 'inboxes.isnew', 'inboxes.hadread', 'inboxes.subject', 'inboxes.message', 'providers.photo', 'bookings.status', 'bookings.ontime','providers.name')
                        ->leftJoin('providers', 'providers.id', '=', 'inboxes.provider_id')
                        ->leftJoin('bookings', function($join) {
                            $join->on('bookings.id', '=', 'inboxes.booking_id')->where('inboxes.type', '=', 1)->where('bookings.status', '=', 0);
                        })->where('inboxes.id', '=', $inbox)->get();
        //dd($bookings->all());
        foreach ($bookings->all() as $key => $value) {

            if (!empty($value['photo'])) {
                $bookings[$key]['photo'] = url('uploads/provider/') . '/' . $value['photo'];
                //print_R($value['photo']);
            }
            if ($value['status'] === 0) {
                $bookings[$key]['ask_for_conformation'] = 1;
            } else {
                $bookings[$key]['ask_for_conformation'] = 0;
            }
            unset($bookings[$key]['status']);
        }

        $data = $bookings;
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $bookings
        );
    }

    public function indexCountInbox(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;

        $countInbox['count'] = \App\Inbox::where('customer_id', '=', $id)->where('hadread', 1)->count();

        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $countInbox
        );
    }

    public function storeCountUpdatehadRead(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;

        $validator = Validator::make($request->all(), [
                    'inbox_id' => 'required|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $inbox = $request->input('inbox_id');
        $inbox_read = \App\Inbox::where('customer_id', '=', $id)->find($inbox);

        if ($inbox_read) {
            $inbox_read->hadread = 0;
            $inbox_read->save();
            
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        }
    }

    public function deleteInboxMessageData(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;

        $validator = Validator::make($request->all(), [
                    'inbox_id' => 'required|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $inbox = $request->input('inbox_id');
        $inbox_read = \App\Inbox::destroy($inbox);

        if ($inbox_read) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Deleted'), config('frontapi.Deleted_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function storeInboxUpdateIsNEW(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;

        $inbox_read = \App\Inbox::where('customer_id', '=', $id)->where('isnew', 1)->update(['isnew' => 0]);

        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
        );
    }

    public function servicesOffered(Request $request) {
        $validator = Validator::make($request->all(), [
                    'type_id' => 'required|integer',
                    'provider_id' => 'required|integer',
                    'breed_id' => 'required',
                    'size_id' => 'required',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        // \DB::enableQueryLog();
        $id = Auth::guard('mobileapp')->user()->id;
        $provider_id = $request->input('provider_id');
        // $data['pets'] = Customer::find($id)->pet()->get();
        $ohhalldata = array();
        // $data['provider'] = \App\Provider::with('type')->whereId($request->only('id'))->first();
        // $data['services'] = DB::table('services')
        $data['services'] = \App\Service::select('categories.parentid', 'categories.name', 'categories.name_er', 'services.id as service_id')
                        ->join('categories', 'categories.id', '=', 'services.category_id')
                        ->where('services.provider_id', '=', $provider_id)
                        ->where('services.status', '=', 1)->get();

        // $service = \App\Service::with('category')->where('provider_id', $provider_id)->get();
        // dd($service->toArray());
        // $alsdkjf = collect($data['services']);
        $dataget_serivce = $data['services']->groupBy('parentid');
// dump($dataget_serivce->all());
        $adkls = $dataget_serivce->toArray();
// dd($adkls); 

        $breed_id = $request->input('breed_id');
        $size_id = $request->input('size_id');

        foreach ($adkls as $servc => $val) {
            $service_mainName = \App\Category::where('id', $servc)->first();

            foreach ($val as $key => $value) {
                // dump($value['service_id']);
                $service_id = ($value['service_id']);
                $servcie_data = \App\BreedService::where('service_id', $service_id)->where('breed_id', $breed_id)->where('size_id', $size_id)->get();
                // dump($servcie_data);
                if ($servcie_data->count() > 0) {
                    // $adkls[$servc][$key]['servicebreed'] = $servcie_data->toArray();
                    $serv_data = $servcie_data->toArray();
                    $mainName = $service_mainName['name'];

                    $ohhalldata[$service_mainName->name][] = array_merge($adkls[$servc][$key], $serv_data[0]);
                }
                // $adkls[$servc][$value][] = $servcie_data;
                // $dataget_serivce->push($servcie_data);
                // dump($value);
                // dd($servcie_data);
            }
        }
// dump($adkls);
        // dd($ohhalldata);
        if (count($ohhalldata) > 0) {
            foreach ($ohhalldata as $alKey => $alValue) {
                foreach ($alValue as $techkey => $Techvalue) {
                    $getTechData = \App\Service::with('technician')->find($Techvalue['service_id'])->toArray();

                    $ohhalldata[$alKey][$techkey]['technicians'] = $getTechData['technician'];
                }
            }
        }


        // $service_daa = \App\Service::with('technician')->find(1)->toArray();
        // dd($ohhalldata);

        if ($ohhalldata) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $ohhalldata
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.petsizeNotServiced'), config('frontapi.petsizeNotServiced_AR')
            );
        }
    }

    // public function getAvailableSlots(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'provider_id'   => 'required|integer',
    //         'start' => 'required|date|date_format:Y-m-d H:i:s',
    //         'end' => 'required|integer|min:5|max:120',
    //         'tech' => 'required|integer',
    //     ]);
    //     if ($validator->fails())
    //         return $this->checkForValidation($validator);
    // }
//     public function check_availability($provider_id, $start, $slot)
//     {
//         $get_day = strtotime($start);
//         $day = date('l', $get_day);
//         $dayBeginsAt = \App\ProviderTiming::where('provider_id','=', $provider_id)
//                         ->where('weekday', '=', $day)->first();
//         // if($dayBeginsAt->)
//         // dump($dayBeginsAt->toArray());
//         // dump($dayBeginsAt->starttime);
//         // dump($dayBeginsAt->weekoff);
//         if($dayBeginsAt->weekoff)
//         {
//             return $this->sendResponse(
//                 config('frontapi.ERROR'),
//                 config('frontapi.ItsWeekOff'),
//                 config('frontapi.ItsWeekOff_AR')
//             );
//         }
// // dump($start);
//         $startdat = Carbon::parse($start)->format('Y-m-d');
// // dump($startdat);
//         $schedule['start'] = $startdat.' '.$dayBeginsAt->starttime;
//         $schedule['end'] = $startdat.' '.$dayBeginsAt->endtime;
// // dump($schedule);
//         $endDate = Carbon::parse($start)->addDay()->format('Y-m-d');
//         $start = Carbon::instance(new DateTime($schedule['start']));
//         $end = Carbon::instance(new DateTime($schedule['end']));
// // DB::enableQueryLog();
//         // $get_booked_data = Booking::where('provider_id', $provider_id)
//         //                     ->where('')
//         $events = \App\Booking::where('start', '>=', $startdat.' 00:00:00')
//     ->where('end', '<=', $endDate.' 00:00:00')
//     ->select('start', 'end')
//     ->get();
// // dump(DB::getQueryLog());
// // dump($events->toArray());
//     $events = $events->toArray();
//         // $events = [
//         //     [
//         //         'created_at' => '2016-11-18 14:00:00',
//         //         'updated_at' => '2016-11-18 16:00:00',
//         //     ],
//         //     [
//         //         'created_at' => '2016-11-18 09:00:00',
//         //         'updated_at' => '2016-11-18 09:30:00',
//         //     ],
//         //     [
//         //         'created_at' => '2016-11-18 10:00:00',
//         //         'updated_at' => '2016-11-18 11:15:00',
//         //     ],
//         // ];
//         // $minSlotHours = 0;
//         $minSlotMinutes = 5;
//         $minInterval = CarbonInterval::minutes($minSlotMinutes);
// // dump($minInterval);
//         $reqSlotHours = 0;
//         $reqSlotMinutes = $slot;
//         $reqInterval = CarbonInterval::hour($reqSlotHours)->minutes($reqSlotMinutes);
// // dump($reqInterval);
//         $availableSlotofTheDay = collect([]);
//         foreach(new DatePeriod($start, $minInterval, $end) as $slot){
//             // $to = $slot->copy()->add($reqInterval);
//             // echo $slot->toDateTimeString() . ' to ' . $to->toDateTimeString();
//             $availableSlotofTheDay->push($slot->format('H:i'));
//             // if($this->slotAvailable($slot, $to, $events)){
//             //     echo ' is available';
//             // }
//             // echo '<br />';
//         }
//         $allotedSlots = collect([]);
//         foreach ($events as  $value) {
//             $allreadyBooked_start = Carbon::instance(new DateTime($value['start']));
//             $allreadyBooked_End = Carbon::instance(new DateTime($value['end']));
//             foreach(new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot){
//                 // $to = $slot->copy()->add($reqInterval);
//                 // echo $slot->toDateTimeString() . ' to ' . $to->toDateTimeString();
//                 $allotedSlots->push($noSlot->format('H:i'));
//                 // if($this->slotAvailable($slot, $to, $events)){
//                 //     echo ' is available';
//                 // }
//                 // echo '<br />';
//             }
//         }
//     // dump($allotedSlots->all());
//         // if($this->slotAvailable($slot, $to, $events)){
//         //         echo ' is available';
//         //     }
//         // dump($availableSlotofTheDay->all());
//         $finalAvailable = $availableSlotofTheDay->intersect($allotedSlots);
//         $aohadlsfjdslj = array_values(array_diff($availableSlotofTheDay->toArray(), $finalAvailable->toArray()));
//         return $aohadlsfjdslj;
//     }

    public function slotAvailable($from, $to, $events) {

        foreach ($events as $event) {
            $eventStart = Carbon::instance(new DateTime($event['start']));
            $eventEnd = Carbon::instance(new DateTime($event['end']));
            if ($from->between($eventStart, $eventEnd) && $to->between($eventStart, $eventEnd)) {
                return false;
            }
        }
        return true;
    }

    public function postBookAppointTest(Request $request) {

        $tech_id_passed = false;
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'notes' => 'string',
                    'start' => 'required|date|date_format:Y-m-d H:i:s',
                    'transaction_id' => 'required',
                    'book.*.pet_id' => 'required|integer',
                    'book.*.technician_id' => 'required',
                    'book.*.service_id' => 'required|integer',
                    'book.*.type_id' => 'required|integer|in:1,2,3,4,5',
                    'book.*.price' => 'string',
                    'book.*.end' => 'required|integer|min:5|max:120',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $customer_id = \Auth::guard('mobileapp')->user()->id;

        $provider_id = $request->input('provider_id');
        $notes = $request->input('notes');
        $start = $request->input('start');

        $transaction_id = $request->input('transaction_id');  #remove this 1, when transaction ID is set on LIVE 29Nov16

        $book_data = $request->only('book');
        $petsAll = collect($book_data['book']);
        $allPetsData = $petsAll->sortBy('type_id')->groupBy('technician_id');
        // dump($allPetsData->all());

        $ontime = Carbon::now()->timestamp;

        $book = array();
        $startFrom = Carbon::parse($start)->format('H:i');
        $startFrom1 = Carbon::parse($start)->format('H:i');

        foreach ($allPetsData->all() as $technician_id => $value) {

            foreach ($value as $seconKey => $finValue) {
                /*echo "<pre>Here : ".$seconKey." : ";
                echo $technician_id = $finValue['technician_id'];             */
                /*exit();*/

                if ($finValue['technician_id'] != 'any') {
                    $vet_tech = self::checkTechnicianAva($start, $provider_id, $finValue['technician_id'], $finValue['end'], $startFrom);
                    if ($vet_tech['status'] == 0) {
                        $currentPet = \App\Pet::find($finValue['pet_id']);
                        $size = $currentPet->size;
                        $breed = $currentPet->breed;
                        $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                        return $q->where('size_id', $size)->where('breed_id', $breed);
                                    }])->find($finValue['service_id']);
                        $price = $currentService->breed[0]->price;
                        $get_alds[$technician_id] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $makeStartDate = Carbon::parse($start)->format('Y-m-d') . ' ' . $startFrom;
                        $current_date_time = date('Y-m-d H:i:s');
                        $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                        $hours = $seconds / 60 / 60;
                        if($hours < 4)
                            $status=1;
                        else
                            $status=0;

                        $store_start = Carbon::parse($makeStartDate);
                        $store_data['end'] = $store_start->addMinutes($finValue['end']);
                        $store_data['status'] = $status; # Within 4 hours will be automatically booked
                        $store_data['start'] = Carbon::parse($makeStartDate);
                        $store_data['ontime'] = $ontime;
                        $store_data['pet_id'] = $finValue['pet_id'];
                        $store_data['technician_id'] = $finValue['technician_id'];
                        $store_data['transaction_id'] = $transaction_id;
                        $store_data['service_id'] = $finValue['service_id'];
                        $store_data['type_id'] = $finValue['type_id'];
                        $store_data['price'] = $price;
                        $store_data['provider_id'] = $provider_id;
                        $store_data['customer_id'] = $customer_id;
                        $store_data['notes'] = $notes;
                        $petData = \App\Pet::select('grooming')->where('pets.id', $finValue['pet_id'])->first();
                        $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                        $book[] = \App\Booking::create($store_data)->id;
                        DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'), 'next_groom_appointment' => Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);

                        $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                    } else {
                        $get_alds[$technician_id] = $vet_tech['message'];
                    }
                } elseif ($finValue['technician_id'] == 'any') {
                    $currentPet = \App\Pet::find($finValue['pet_id']);
                    $size = $currentPet->size;
                    $breed = $currentPet->breed;
                    $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                    return $q->where('size_id', $size)->where('breed_id', $breed);
                                }])->find($finValue['service_id']);
                    $price = $currentService->breed[0]->price;

                    $tech_id_passed = true;
                        $serviceID = $finValue['service_id'];
                        $abooked=false;
                        $getnearFreeTechOjb = \DB::table('technicians')->select('service_technician.technician_id as id','bookings.end')
                              ->rightjoin('service_technician', 'service_technician.service_id', '=', 'technicians.id')
                              ->leftjoin('bookings', 'bookings.technician_id', '=', 'service_technician.technician_id')
                              ->where('service_technician.service_id',$serviceID)
                              ->where('bookings.end','>=',$start)
                              ->where('bookings.end','<=',Carbon::parse($start)->format('Y-m-d').' 23:59:59')
                              ->GROUPBy('bookings.technician_id')
                              ->orderBy('bookings.end','ASC')
                              ->get();
                        //echo $serviceID."==".$start.'=='.Carbon::parse($start)->format('Y-m-d').' 23:59:59';
                       /* $getnearFreeTechOjb1 = \DB::table('technicians')->select('service_technician.technician_id as id','bookings.end')
                              ->rightjoin('service_technician', 'service_technician.service_id', '=', 'technicians.id')
                              ->leftjoin('bookings', 'bookings.technician_id', '=', 'service_technician.technician_id')
                              ->where('service_technician.service_id',$serviceID)
                              ->where('bookings.end','>=',$start)
                              ->where('bookings.end','<=',Carbon::parse($start)->format('Y-m-d').' 23:59:59')
                              ->GROUPBy('bookings.technician_id')
                              ->orderBy('bookings.end','ASC')
                              ->toSql();*/
                        //print_r($getnearFreeTechOjb1);
                        //echo $serviceID."==".$start.'=='.Carbon::parse($start)->format('Y-m-d').' 23:59:59';
                        $allAvailable_Technician = json_decode(json_encode($getnearFreeTechOjb), true);
                        echo "<pre>in booking";
                        print_r($allAvailable_Technician);

                        if(count($allAvailable_Technician) > 0){
                            $bookedTechnician = array();
                            foreach ($allAvailable_Technician as $techs) {
                                $bookedTechnician[]= $techs['id'];
                            }
                            foreach ($allAvailable_Technician as $techs) {
                                $getnearFreeTechOjb = \DB::table('technicians')->select('service_technician.technician_id as id','technician_timings.starttime')
                                      ->rightjoin('service_technician', 'service_technician.service_id', '=', 'technicians.id')
                                      ->join('technician_timings', 'technician_timings.technician_id', '=', 'service_technician.technician_id')
                                      ->where('service_technician.service_id',$serviceID)
                                      ->where('technician_timings.starttime','<=',$techs['end'])
                                      ->whereNotIn('service_technician.technician_id',$bookedTechnician)
                                      ->groupBy('technician_timings.technician_id')
                                      ->orderBy('technician_timings.starttime','ASC')
                                      ->get();
                                $allAvailable_Technician1 = json_decode(json_encode($getnearFreeTechOjb), true);

                                if(!empty($allAvailable_Technician1)){
                                    $technician_id = $allAvailable_Technician1[0]['id'];
                                 }
                                 else{
                                    $technician_id = $techs['id'];
                                 }
                                $checkTechData = self::checkTechnicianAva($start,$provider_id, $technician_id, $finValue['end'], $startFrom1);
                                
                                if($checkTechData['status'] == 0){
                                    $abooked=true;
                                    $get_alds[$technician_id] = $checkTechData['data'];
                                    $startFrom = $checkTechData['data'][0];
                                    $makeStartDate = Carbon::parse($start)->format('Y-m-d').' '.$startFrom;
                                    $current_date_time = date('Y-m-d H:i:s');
                                    $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                                    $hours = $seconds / 60 / 60;
                                    if($hours < 4)
                                        $status=1;
                                    else
                                        $status=0;

                                    $store_start = Carbon::parse($makeStartDate);
                                    $store_data['end']  = $store_start->addMinutes($finValue['end']);
                                    $store_data['status'] = $status;
                                    $store_data['start'] = Carbon::parse($makeStartDate);
                                    $store_data['ontime'] = $ontime;
                                    $store_data['pet_id'] = $finValue['pet_id'];
                                    $store_data['technician_id'] = $technician_id;
                                    $store_data['service_id'] = $finValue['service_id'];
                                    $store_data['transaction_id'] = $transaction_id;
                                    $store_data['type_id']      = $finValue['type_id'];
                                    $store_data['price'] = $price;
                                    $store_data['provider_id'] = $provider_id;
                                    $store_data['customer_id'] = $customer_id;
                                    $store_data['notes'] = $notes;
                                    $petData = \App\Pet::select('grooming')->where('pets.id',$finValue['pet_id'])->first();
                                    $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                                    $book[] = \App\Booking::create($store_data)->id;
                                    DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                        ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'),'next_groom_appointment'=>Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);
                                    $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                                    break;
                                }  else {
                                    $get_alds[$technician_id] = $checkTechData['message'];
                                }
                            }
                        }
                        
                        if($abooked==false){
                            $getnearFreeTechOjb = \DB::table('technicians')->select('service_technician.technician_id as id','technician_timings.starttime')
                                  ->rightjoin('service_technician', 'service_technician.service_id', '=', 'technicians.id')
                                  ->join('technician_timings', 'technician_timings.technician_id', '=', 'service_technician.technician_id')
                                  ->where('service_technician.service_id',$serviceID)
                                  ->groupBy('technician_timings.technician_id')
                                  ->orderBy('technician_timings.starttime','ASC')
                                  ->get();
                            $allAvailable_Technician = json_decode(json_encode($getnearFreeTechOjb), true);
                            /*echo "first time here";
                            print_r($allAvailable_Technician);*/
                            if(count($allAvailable_Technician) > 0){
                                foreach ($allAvailable_Technician as $techs) {
                                    $technician_id = $techs['id'];
                                    $checkTechData = self::checkTechnicianAva($start,$provider_id, $technician_id, $finValue['end'], $startFrom);
                                    if($checkTechData['status'] == 0){
                                        // $get_alds[$technician_id]['service_id'] = $serviceID;
                                        $get_alds[$technician_id] = $checkTechData['data'];
                                        $startFrom = $checkTechData['data'][0];
                                        $makeStartDate = Carbon::parse($start)->format('Y-m-d').' '.$startFrom;
                                        $current_date_time = date('Y-m-d H:i:s');
                                        $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                                        $hours = $seconds / 60 / 60;
                                        if($hours < 4)
                                            $status=1;
                                        else
                                            $status=0;
                                        $store_start = Carbon::parse($makeStartDate);
                                        $store_data['end']  = $store_start->addMinutes($finValue['end']);
                                        $store_data['status'] = $status;
                                        $store_data['start'] = Carbon::parse($makeStartDate);
                                        $store_data['ontime'] = $ontime;
                                        $store_data['pet_id'] = $finValue['pet_id'];
                                        $store_data['technician_id'] = $technician_id;
                                        $store_data['service_id'] = $finValue['service_id'];
                                        $store_data['transaction_id'] = $transaction_id;
                                        $store_data['type_id']      = $finValue['type_id'];
                                        $store_data['price'] = $price;
                                        $store_data['provider_id'] = $provider_id;
                                        $store_data['customer_id'] = $customer_id;
                                        $store_data['notes'] = $notes;
                                        $petData = \App\Pet::select('grooming')->where('pets.id',$finValue['pet_id'])->first();
                                        $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                                        $book[] = \App\Booking::create($store_data)->id;
                                        DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                            ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'),'next_groom_appointment'=>Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);
                                        $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                                        
                                        break;
                                    }  else {
                                        $get_alds[$technician_id] = $checkTechData['message'];
                                    }
                                }
                            }
                        }
                    /*echo $finValue['pet_id']." is done";*/
                }

            }
        }
        /*echo "end";
        exit();*/
        if (count($book) > 0) {
            $bookingDatas['booking'] = \App\Booking::select('bookings.*', 'pets.name as petname', 'categories.name as servicename', 'categories.name_er as servicename_ar', 'technicians.name_ar as techname_ar', 'technicians.name as techname', 'providers.name as providername', 'providers.name_ar as providername_ar')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->whereIn('bookings.id', $book)->get();

            event(new BookingWasMade($book));

            $book = \App\Booking::SingleBookingDetail($book[0])->first();

            $date = $book['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y');
            $inboxMessage = $book['firstname'] . ' booked ' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;
            $bookingDatas['message'] = $inboxMessage;

            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $bookingDatas
            );
        } else {
            $err = ($tech_id_passed) ? 'Technician ID must be passed, getting value as any' : config('frontapi.Invalid');
            return $this->sendResponse(
                            config('frontapi.ERROR'), $err, config('frontapi.Invalid_AR')
            );
        }
    }
    public function sortArray( $data, $field ) {
        $field = (array) $field;
        uasort( $data, function($a, $b) use($field) {
            $retval = 0;
            foreach( $field as $fieldname ) {
                if( $retval == 0 ) $retval = strnatcmp( $a[$fieldname], $b[$fieldname] );
            }
            return $retval;
        } );
        return $data;
    }
    public function phparraysort($Array, $SortBy=array(), $Sort = SORT_REGULAR) {
        if (is_array($Array) && count($Array) > 0 && !empty($SortBy)) {
                $Map = array();         
                foreach ($Array as $Key => $Val) {
                    $Sort_key = '';             
                    foreach ($SortBy as $Key_key) {
                        $Sort_key .= $Val[$Key_key];
                    }                
                    $Map[$Key] = $Sort_key;
                }
                arsort($Map, $Sort);
                $Sorted = array();
                foreach ($Map as $Key => $Val) {
                    $Sorted[] = $Array[$Key];
                }
                return array_reverse($Sorted);
        }
        return $Array;
    }
    
    public function postBookAppoint(Request $request) {

        $tech_id_passed = false;
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'notes' => 'string',
                    'start' => 'required|date|date_format:Y-m-d H:i:s',
                    'transaction_id' => 'required',
                    'book.*.pet_id' => 'required|integer',
                    'book.*.technician_id' => 'required',
                    'book.*.service_id' => 'required|integer',
                    'book.*.type_id' => 'required|integer|in:1,2,3,4,5',
                    'book.*.price' => 'string',
                    'book.*.end' => 'required|integer|min:5|max:120',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $customer_id = \Auth::guard('mobileapp')->user()->id;

        $provider_id = $request->input('provider_id');
        $notes = $request->input('notes');
        $start = $request->input('start');

        $transaction_id = $request->input('transaction_id');  #remove this 1, when transaction ID is set on LIVE 29Nov16

        $book_data = $request->only('book');
        $petsAll = collect($book_data['book']);
        $ontime = Carbon::now()->timestamp;

        $allPetsData = $petsAll->sortBy('pet_id')->groupBy('pet_id');

        $book = array();
        $startFrom = Carbon::parse($start)->format('H:i');
        $startFrom1 = Carbon::parse($start)->format('H:i');

        /*$allPetsDataJson = json_decode(json_encode($allPetsData), true);
        $SortArrayMuti = $this->phparraysort($allPetsDataJson, array('pet_id','type_id'));
        foreach ($SortArrayMuti as $key => $finValue) {
                 if ($finValue['technician_id'] != 'any') {                    
                    $vet_tech = self::checkTechnicianAva($start, $provider_id, $finValue['technician_id'], $finValue['end'], $startFrom);
                    echo $start.', '.$provider_id.', '.$finValue['technician_id'].', '.$finValue['end'].', '.$startFrom;
                    echo "<br>";
                    if ($vet_tech['status'] == 0) {
                        $currentPet = \App\Pet::find($finValue['pet_id']);
                        $size = $currentPet->size;
                        $breed = $currentPet->breed;
                        $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                        return $q->where('size_id', $size)->where('breed_id', $breed);
                                    }])->find($finValue['service_id']);
                        $price = $currentService->breed[0]->price;
                        $get_alds[$finValue['technician_id']] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $makeStartDate = Carbon::parse($start)->format('Y-m-d') . ' ' . $startFrom;
                        $current_date_time = date('Y-m-d H:i:s');
                        $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                        $hours = $seconds / 60 / 60;
                        if($hours < 4)
                            $status=1;
                        else
                            $status=0;

                        $store_start = Carbon::parse($makeStartDate);
                        $store_data['end'] = $store_start->addMinutes($finValue['end']);
                        $store_data['status'] = $status; # Within 4 hours will be automatically booked
                        $store_data['start'] = Carbon::parse($makeStartDate);
                        $store_data['ontime'] = $ontime;
                        $store_data['pet_id'] = $finValue['pet_id'];
                        $store_data['technician_id'] = $finValue['technician_id'];
                        $store_data['transaction_id'] = $transaction_id;
                        $store_data['service_id'] = $finValue['service_id'];
                        $store_data['type_id'] = $finValue['type_id'];
                        $store_data['price'] = $price;
                        $store_data['provider_id'] = $provider_id;
                        $store_data['customer_id'] = $customer_id;
                        $store_data['notes'] = $notes;
                        $petData = \App\Pet::select('grooming')->where('pets.id', $finValue['pet_id'])->first();
                        $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                        $book[] = \App\Booking::create($store_data)->id;
                        DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'), 'next_groom_appointment' => Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);

                        //$startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                    } else {
                        $get_alds[$finValue['technician_id']] = $vet_tech['message'];
                    }
                } elseif ($finValue['technician_id'] == 'any') {
                    $currentPet = \App\Pet::find($finValue['pet_id']);
                    $size = $currentPet->size;
                    $breed = $currentPet->breed;
                    $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                    return $q->where('size_id', $size)->where('breed_id', $breed);
                                }])->find($finValue['service_id']);
                    $price = $currentService->breed[0]->price;

                    $tech_id_passed = true;
                    $serviceID = $finValue['service_id'];
                    $allAvailable_Technician = \App\Service::with('technician')->find($serviceID)->toArray();
                    $allAvailable_Technician = $allAvailable_Technician['technician'];
                    // dump($allAvailable_Technician);
                    if(count($allAvailable_Technician) > 0){
                        $finaltech = array();
                        foreach ($allAvailable_Technician as $techs) {
                            // dd($techs);
                            $technician_id = $techs['id'];
                            $checkTechData = self::checkTechnicianAva($start,$provider_id, $technician_id, $finValue['end'], $startFrom);
                            $tech['time'] = $checkTechData['data']['0'];
                            $tech['tech_id'] = $technician_id;
                            $finaltech[] = $tech;

                        }
                        echo "any ";
                        echo $start.', '.$provider_id.', '.$technician_id.', '.$finValue['end'].', '.$startFrom;
                        echo "<br>";
                        print_r($finaltech);
                        $SortedfinalData = $this->sortArray( $finaltech, 'time' );
                        
                        
                        foreach ($SortedfinalData as $key => $finalData) {
                            
                              
                            $get_alds[$technician_id] = $finalData['tech_id'];
                            $startFrom = $finalData['time'];

                            $makeStartDate = Carbon::parse($start)->format('Y-m-d').' '.$startFrom;
                            $store_start = Carbon::parse($makeStartDate);
                            $store_data['end']  = $store_start->addMinutes($finValue['end']);
                            $store_data['status'] = 1;
                            $store_data['start'] = Carbon::parse($makeStartDate);
                            $store_data['ontime'] = $ontime;
                            $store_data['pet_id'] = $finValue['pet_id'];
                            $store_data['technician_id'] =  $finalData['tech_id'];
                            $store_data['service_id'] = $finValue['service_id'];
                            $store_data['transaction_id'] = $transaction_id;
                            $store_data['type_id']      = $finValue['type_id'];
                            $store_data['provider_id'] = $provider_id;
                            $store_data['customer_id'] = $customer_id;
                            $store_data['notes'] = $notes;
                            $petData = \App\Pet::select('grooming')->where('pets.id',$finValue['pet_id'])->first();
                            $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                            $book[] = \App\Booking::create($store_data)->id;
                            DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'),'next_groom_appointment'=>Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);
                            //$startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                            break;
                        }
                       // }

                    }
                }    
                $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                echo "final:".$startFrom."<br>";            
        }*/

        foreach ($allPetsData->all() as $technician_id => $value) {
            $value = $value->sortBy('type_id');
            $startFrom = Carbon::parse($start)->format('H:i');
            foreach ($value as $seconKey => $finValue) {
                /*echo "<pre>Here : ".$seconKey." : ";
                echo $technician_id = $finValue['technician_id'];             */
                /*exit();*/

                if ($finValue['technician_id'] != 'any') {                    
                    $vet_tech = self::checkTechnicianAva($start, $provider_id, $finValue['technician_id'], $finValue['end'], $startFrom);
                    /*echo $start.', '.$provider_id.', '.$finValue['technician_id'].', '.$finValue['end'].', '.$startFrom;
                    echo "<br>";*/
                    if ($vet_tech['status'] == 0) {
                        $currentPet = \App\Pet::find($finValue['pet_id']);
                        $size = $currentPet->size;
                        $breed = $currentPet->breed;
                        $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                        return $q->where('size_id', $size)->where('breed_id', $breed);
                                    }])->find($finValue['service_id']);
                        $price = $currentService->breed[0]->price;
                        $get_alds[$technician_id] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $makeStartDate = Carbon::parse($start)->format('Y-m-d') . ' ' . $startFrom;
                        $current_date_time = date('Y-m-d H:i:s');
                        $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                        $hours = $seconds / 60 / 60;
                        if($hours < 4)
                            $status=1;
                        else
                            $status=0;

                        $store_start = Carbon::parse($makeStartDate);
                        $store_data['end'] = $store_start->addMinutes($finValue['end']);
                        $store_data['status'] = $status; # Within 4 hours will be automatically booked
                        $store_data['start'] = Carbon::parse($makeStartDate);
                        $store_data['ontime'] = $ontime;
                        $store_data['pet_id'] = $finValue['pet_id'];
                        $store_data['technician_id'] = $finValue['technician_id'];
                        $store_data['transaction_id'] = $transaction_id;
                        $store_data['service_id'] = $finValue['service_id'];
                        $store_data['type_id'] = $finValue['type_id'];
                        $store_data['price'] = $price;
                        $store_data['provider_id'] = $provider_id;
                        $store_data['customer_id'] = $customer_id;
                        $store_data['notes'] = $notes;
                        $petData = \App\Pet::select('grooming')->where('pets.id', $finValue['pet_id'])->first();
                        $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                        $book[] = \App\Booking::create($store_data)->id;
                        DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'), 'next_groom_appointment' => Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);

                       // $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                    } else {
                        $get_alds[$technician_id] = $vet_tech['message'];
                    }
                } elseif ($finValue['technician_id'] == 'any') {
                    $currentPet = \App\Pet::find($finValue['pet_id']);
                    $size = $currentPet->size;
                    $breed = $currentPet->breed;
                    $currentService = \App\Service::with(['breed' => function($q) use($size, $breed) {
                                    return $q->where('size_id', $size)->where('breed_id', $breed);
                                }])->find($finValue['service_id']);
                    $price = $currentService->breed[0]->price;

                    $tech_id_passed = true;
                    $serviceID = $finValue['service_id'];
                    $allAvailable_Technician = \App\Service::with('technician')->find($serviceID)->toArray();
                    $allAvailable_Technician = $allAvailable_Technician['technician'];
                    // dump($allAvailable_Technician);
                    if(count($allAvailable_Technician) > 0){
                        $finaltech = array();
                        foreach ($allAvailable_Technician as $techs) {
                            $technician_id = $techs['id'];
                            $checkTechData = self::checkTechnicianAva($start,$provider_id, $technician_id, $finValue['end'], $startFrom);
                            $tech['time'] = $checkTechData['data']['0'];
                            $tech['tech_id'] = $technician_id;
                            $finaltech[] = $tech;

                        }
                        /*echo "any ";
                        echo $start.', '.$provider_id.', '.$technician_id.', '.$finValue['end'].', '.$startFrom;
                        echo "<br>";*/
                        $SortedfinalData = $this->sortArray( $finaltech, 'time' );
                        
                        foreach ($SortedfinalData as $key => $finalData) {
                            $get_alds[$technician_id] = $finalData['tech_id'];
                            $startFrom = $finalData['time'];

                            $makeStartDate = Carbon::parse($start)->format('Y-m-d').' '.$startFrom;
                            $current_date_time = date('Y-m-d H:i:s');
                            $seconds = strtotime($makeStartDate) - strtotime($current_date_time);
                            $hours = $seconds / 60 / 60;
                            if($hours < 4)
                                $status=1;
                            else
                                $status=0;
                            $store_start = Carbon::parse($makeStartDate);
                            $store_data['end']  = $store_start->addMinutes($finValue['end']);
                            $store_data['status'] = $status;
                            $store_data['start'] = Carbon::parse($makeStartDate);
                            $store_data['ontime'] = $ontime;
                            $store_data['pet_id'] = $finValue['pet_id'];
                            $store_data['technician_id'] =  $finalData['tech_id'];
                            $store_data['service_id'] = $finValue['service_id'];
                            $store_data['transaction_id'] = $transaction_id;
                            $store_data['type_id']      = $finValue['type_id'];
                            $store_data['provider_id'] = $provider_id;
                            $store_data['customer_id'] = $customer_id;
                            $store_data['notes'] = $notes;
                            $petData = \App\Pet::select('grooming')->where('pets.id',$finValue['pet_id'])->first();
                            $nextAppointmentDate = Carbon::parse($start)->addWeeks($petData['grooming']);
                            $book[] = \App\Booking::create($store_data)->id;
                            DB::table('pets')->where('id', $finValue['pet_id'])->update(
                                ['last_groom_appointment' => Carbon::parse($start)->format('Y-m-d'),'next_groom_appointment'=>Carbon::parse($nextAppointmentDate)->format('Y-m-d')]);
                            break;
                        }
                        
                       // }

                    }
                        
                }
                $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
            }

        }
        if (count($book) > 0) {
            $bookingDatas['booking'] = \App\Booking::select('bookings.*', 'pets.name as petname', 'categories.name as servicename', 'categories.name_er as servicename_ar', 'technicians.name_ar as techname_ar', 'technicians.name as techname', 'providers.name as providername', 'providers.name_ar as providername_ar')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->whereIn('bookings.id', $book)->get();

            event(new BookingWasMade($book));

            $book = \App\Booking::SingleBookingDetail($book[0])->first();

            $date = $book['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y');
            $inboxMessage = $book['firstname'] . ' booked ' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;
            $bookingDatas['message'] = $inboxMessage;

            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $bookingDatas
            );
        } else {
            $err = ($tech_id_passed) ? 'Technician ID must be passed, getting value as any' : config('frontapi.Invalid');
            return $this->sendResponse(
                            config('frontapi.ERROR'), $err, config('frontapi.Invalid_AR')
            );
        }
    }

    // public function getTechniciansForService($service_id)
    // {
    //     $technicians = \App\Service::with('technician')->find($service_id)->toArray();
    // }

    public function checkTechnicianAvailability(Request $request) {
        $current = Carbon::yesterday()->format('Y-m-d');
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'notes' => 'string',
                    'start' => 'required|date|date_format:Y-m-d|after:' . $current,
                    'book.*.pet_id' => 'required|integer',
                    'book.*.technician_id' => 'required',
                    'book.*.service_id' => 'required|integer',
                    'book.*.type_id' => 'required|integer|in:1,2,3,4,5',
                    'book.*.end' => 'required|integer|min:5|max:120',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $customer_id = \Auth::guard('mobileapp')->user()->id;
        $provider_id = $request->input('provider_id');
        $start = $request->input('start');

        $book_data = $request->only('book');


        $petsAll = collect($book_data['book']);



        //dump($petsAll->all());
// dump($petsAll->sortBy('type_id')->all());
        $allPetsData = $petsAll->sortBy('type_id')->groupBy('technician_id');
        // dump($allPetsData->all());
        // dump($allPetsData->groupBy('technician_id'));
        // dd($allPetsData->all());

        $get_alds = $tarun = array();

        $startFrom = 0;

        $isVetBookingNeeded = false;

        foreach ($allPetsData->all() as $key => $value) {

            $totalEnd = ($value->sum('end'));
            //echo "<pre>";print_r($value);
            foreach ($value as $otherkey => $othervalue) {
                $technician_id = $key;


                //echo $technician_id;
                if (is_int($technician_id) && $technician_id != 'any') {

                    $vet_tech = self::checkTechnicianAva($start, $provider_id, $technician_id, $totalEnd, $startFrom);

                    if ($vet_tech['status'] == 0) {
                        $get_alds[$technician_id] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $tarun['service_id'] = $othervalue['service_id'];
                        $tarun['technician_id'] = $technician_id;
                        $tarun['slots'] = $vet_tech['data'];
                        $tarun['pet_id'] = $value[$otherkey]['pet_id'];
                    } else {
                        $get_alds[$technician_id] = $vet_tech['message'];
                    }
                    $isVetBookingNeeded = true;
                } elseif ($technician_id == 'any') {
                    // echo $value[$otherkey]['pet_id'];
                    $all_available_technicians = [];
                    $techni_first_available_slot = collect([]);
                    $serviceID = $othervalue['service_id'];
                    $allAvailable_Technician = \App\Service::with('technician')->find($serviceID)->toArray();
                    $allAvailable_Technician = $allAvailable_Technician['technician'];
                    // dump($allAvailable_Technician);
                    if (count($allAvailable_Technician) > 0) {

                        foreach ($allAvailable_Technician as $techs) {
                            // dd($techs);
                            $technician_id = $techs['id'];
                            $checkTechData = self::checkTechnicianAva($start, $provider_id, $technician_id, $totalEnd, $startFrom);


                            if ($checkTechData['status'] == 0) {
                                // $get_alds[$technician_id]['service_id'] = $serviceID;
                                $all_available_technicians[$technician_id] = $checkTechData['data'];
                                // $techni_first_available_slot->put($technician_id,  strtotime($checkTechData['data'][0]));
                                $techni_first_available_slot->push(['tech' => $technician_id, 'time' => strtotime($checkTechData['data'][0]), 'timez' => $checkTechData['data'][0]]);
                                // break;
                            } else {
                                $get_alds[$technician_id] = $checkTechData['message'];
                            }
                        }
                        // Check for best timings
                        if (count($all_available_technicians) > 0) {
                            // dump($techni_first_available_slot);

                            $aljsf = ($techni_first_available_slot->sortBy('time')->first());
                            // dump($aljsf);
                            $get_alds[$aljsf['tech']] = $all_available_technicians[$aljsf['tech']];
                            $tarun['service_id'] = $othervalue['service_id'];
                            $tarun['technician_id'] = $aljsf['tech'];
                            $tarun['pet_id'] = $value[$otherkey]['pet_id'];
                            $tarun['slots'] = $all_available_technicians[$aljsf['tech']];
                        }
                    }
                }
                if (is_array($tarun)) {
                    $ohmygodDaTA[] = $tarun;
                } else {
                    $ohmygodDaTA = array();
                }
            }
        }
        // dump($get_alds);
        // dd($ohmygodDaTA);
        $message = '';
        $found_slot = true;
        $tarfound_slot = true;
        // foreach ($get_alds as $checkKey => $checkValue) {
        //     $found_slot = is_array($checkValue);
        //     if(!$found_slot){
        //         $message = $checkValue;
        //         break;
        //     }
        // }
        // dd($ohmygodDaTA);
        $nowtotalfinal = [];
        if (count($ohmygodDaTA) > 0) {
            $i = 0;
            // print_r($ohmygodDaTA);exit;
            foreach ($ohmygodDaTA as $checkKeyas => $checkValueas) {
                $tarfound_slot = count($checkValueas) > 0;
                if (!$tarfound_slot) {
                    $tarmessage = $checkValueas;
                    break;
                }
                $okjaanu['service_id'] = $checkValueas['service_id'];
                $okjaanu['technician_id'] = $checkValueas['technician_id'];
                $okjaanu['pet_id'] = $checkValueas['pet_id'];
                // $okjaanu['slots'] = $checkValueas['slots'];
                $nowtotalfinal['services'][] = $okjaanu;
                // array_push($nowtotalfinal, $okjaanu);
                if ($i == 0) {
                    $nowtotalfinal111['slot'] = $checkValueas['slots'];
                }
                $i++;
            }
        }

        // $get_alds  = array_values($get_alds); #if Getting array
        // if($found_slot && isset($get_alds[0]))
        if ($tarfound_slot) {
            $ihopeThisworks = array_merge($nowtotalfinal, $nowtotalfinal111);
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),
                            // $get_alds[0]
                            $ihopeThisworks
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.NoSlotsAvailable'), config('frontapi.NoSlotsAvailable_AR')
            );
        }
    }

    public function checkTechnicianAvailability_v2(Request $request) {
        $current = Carbon::yesterday()->format('Y-m-d');
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'notes' => 'string',
                    'start' => 'required|date|date_format:Y-m-d|after:' . $current,
                    'book.*.pet_id' => 'required|integer',
                    'book.*.technician_id' => 'required',
                    'book.*.service_id' => 'required|integer',
                    'book.*.type_id' => 'required|integer|in:1,2,3,4,5',
                    'book.*.end' => 'required|integer|min:5|max:120',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $customer_id = \Auth::guard('mobileapp')->user()->id;
        $provider_id = $request->input('provider_id');
        $start = $request->input('start');

        $book_data = $request->only('book');


        $petsAll = collect($book_data['book']);

        //dump($petsAll->all());
// dump($petsAll->sortBy('type_id')->all());
        $allPetsData = $petsAll->sortBy('type_id')->groupBy('technician_id');
        // dump($allPetsData->all());
        // dump($allPetsData->groupBy('technician_id'));
        // dd($allPetsData->all());

        $get_alds = $tarun = array();

        $startFrom = 0;

        $isVetBookingNeeded = false;

        foreach ($allPetsData->all() as $technician_id => $value) {

            $totalEnd = ($value->sum('end'));

            foreach ($value as $otherkey => $othervalue) {

                if (is_int($technician_id) && $technician_id != 'any') {

                    $vet_tech = self::checkTechnicianAva_v2($start, $provider_id, $technician_id, $totalEnd, $startFrom);

                    if ($vet_tech['status'] == 0) {
                        $get_alds[$technician_id] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $tarun['service_id'] = $othervalue['service_id'];
                        $tarun['technician_id'] = $technician_id;
                        $tarun['slots'] = $vet_tech['data'];
                    } else {
                        $get_alds[$technician_id] = $vet_tech['message'];
                    }
                    $isVetBookingNeeded = true;
                } elseif ($technician_id == 'any') {
                    $all_available_technicians = [];
                    $techni_first_available_slot = collect([]);
                    $serviceID = $othervalue['service_id'];
                    $allAvailable_Technician = \App\Service::with('technician')->find($serviceID)->toArray();
                    $allAvailable_Technician = $allAvailable_Technician['technician'];
                    // dump($allAvailable_Technician);
                    if (count($allAvailable_Technician) > 0) {

                        foreach ($allAvailable_Technician as $techs) {
                            // dd($techs);
                            $technician_id = $techs['id'];
                            $checkTechData = self::checkTechnicianAva_v2($start, $provider_id, $technician_id, $totalEnd, $startFrom);

                            if ($checkTechData['status'] == 0) {
                                // $get_alds[$technician_id]['service_id'] = $serviceID;
                                $all_available_technicians[$technician_id] = $checkTechData['data'];
                                // $techni_first_available_slot->put($technician_id,  strtotime($checkTechData['data'][0]));
                                $techni_first_available_slot->push(['tech' => $technician_id, 'time' => strtotime($checkTechData['data'][0]), 'timez' => $checkTechData['data'][0]]);
                                // break;
                            } else {
                                $get_alds[$technician_id] = $checkTechData['message'];
                            }
                        }
                        // Check for best timings
                        if (count($all_available_technicians) > 0) {
                            // dump($techni_first_available_slot);

                            $aljsf = ($techni_first_available_slot->sortBy('time')->first());
                            // dump($aljsf);
                            $get_alds[$aljsf['tech']] = $all_available_technicians[$aljsf['tech']];
                            $tarun['service_id'] = $othervalue['service_id'];
                            $tarun['technician_id'] = $aljsf['tech'];
                            $tarun['slots'] = $all_available_technicians[$aljsf['tech']];
                        }
                    }
                }
                $ohmygodDaTA[] = $tarun;
            }
        }
        // dump($get_alds);
        // dd($ohmygodDaTA);
        $message = '';
        $found_slot = true;
        $tarfound_slot = true;
        // foreach ($get_alds as $checkKey => $checkValue) {
        //     $found_slot = is_array($checkValue);
        //     if(!$found_slot){
        //         $message = $checkValue;
        //         break;
        //     }
        // }
        // dd($ohmygodDaTA);
        $nowtotalfinal = [];
        if (count($ohmygodDaTA) > 0) {
            $i = 0;
            foreach ($ohmygodDaTA as $checkKeyas => $checkValueas) {
                $tarfound_slot = count($checkValueas) > 0;
                if (!$tarfound_slot) {
                    $tarmessage = $checkValueas;
                    break;
                }
                $okjaanu['service_id'] = $checkValueas['service_id'];
                $okjaanu['technician_id'] = $checkValueas['technician_id'];
                // $okjaanu['slots'] = $checkValueas['slots'];
                $nowtotalfinal['services'][] = $okjaanu;
                // array_push($nowtotalfinal, $okjaanu);
                if ($i == 0) {
                    $nowtotalfinal111['slot'] = $checkValueas['slots'];
                }
                $i++;
            }
        }

        // $get_alds  = array_values($get_alds); #if Getting array
        // if($found_slot && isset($get_alds[0]))
        if ($tarfound_slot) {
            $ihopeThisworks = array_merge($nowtotalfinal, $nowtotalfinal111);
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),
                            // $get_alds[0]
                            $ihopeThisworks
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), $message, config('frontapi.Invalid_AR')
            );
        }
    }

    public function checkTechnicianAva($start, $provider_id, $technician, $total_time, $startFrom, $make_slot = 5) {

        // $validator = Validator::make($request->all(), [
        //     'provider_id'   => 'required|integer',
        //     'start'     => 'required|date|date_format:Y-m-d H:i:s',
        //     'technician'   => 'sometimes|integer',
        //     'service_id'   => 'required|integer',
        //     'total_time' => 'required|integer|min:5|max:120',
        // ]);
        // if ($validator->fails())
        //     return $this->checkForValidation($validator);
        // $start = $request->input('start');
        // $provider_id = $request->input('provider_id');
        // $technician = $request->input('technician');
        // $service_id = $request->input('service_id');
        // $total_time = $request->input('total_time');

        $response = '';

        if ($start) {
            $date = Carbon::parse($start)->format('Y-m-d');
        } else {
            $date = Carbon::now()->format('Y-m-d');
            ;
        }

        //If no technician passed, then get all technicians of this service
        // if (empty($technician)) {
        //     $techn = \App\Service::with('technician')->where('status', '=', 1)->find($service_id)->toArray();
        //     foreach ($techn['technician'] as $key => $value) {
        //         $ =
        //     }
        // }
        // DB::enableQueryLog();

        $day = Carbon::parse($start)->format('l'); //date("l", strtotime($date));
        // check technican wkk day leave
        $getdate = strtotime(date("Y-m-d"));
        $today_date = date("Y-m-d");
        // calculate the number of days since Monday
        $dow = date('w', $getdate);


        $offset = $dow - 1;
        if ($offset < 0) {
            $offset = 6;
        }
        // calculate timestamp for Monday and Sunday
        $monday = $getdate - ($offset * 86400);
        $sunday = $monday + (6 * 86400);

        // print dates for Monday and Sunday in the current week
        $s_date = date("Y-m-d", $monday);
        $e_date = date("Y-m-d", $sunday);


        $techdayslot = array();
        $techdateslot = array();
        $techbreakslot = collect([]);
        $minSlotMinutes = 15;
        $minInterval = CarbonInterval::minutes($minSlotMinutes);

        // Check For Provider Availability
        $provider_WeekOff = \App\ProviderTiming::where('provider_id', '=', $provider_id)
                        ->where('weekday', '=', $day)->first();

        if (isset($provider_WeekOff->weekoff) && $provider_WeekOff->weekoff) {

            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.StoreClosed'), config('frontapi.StoreClosed_AR')
            );
        }

        // Provider check for Full Day Leave or Half Day Leave
        $providerLeave = \App\ProviderHoliday::where('provider_id', '=', $provider_id)
                        ->where('start', '<=', $date)
                        ->where('end', '>=', $date)->first();


        if ($providerLeave) {
            if ($providerLeave->type) {
                #check the start and end time
                $providerHolidayTime = Carbon::instance(new DateTime($providerLeave->starttime));
                $providerHoliday_End = Carbon::instance(new DateTime($providerLeave->endtime));
                foreach (new DatePeriod($providerHolidayTime, $minInterval, $providerHoliday_End) as $unavailable) {
                    $techbreakslot->push($unavailable->format('H:i'));
                }
            } elseif ($providerLeave->type == 0) {
                #Full Day Holiday
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.ProviderFullDayHoliday'), config('frontapi.ProviderFullDayHoliday_AR')
                );
            }
        }
// dd($techbreakslot->all());
        if ($technician != "") {
            // check technician holiday

            $techHoliday = \App\TechnicianHoliday::where('technician_id', $technician)->where('start', '<=', $date)
                            ->where('end', '>=', $date)->first();


            if (isset($techHoliday) && $techHoliday->type == 0) {

                #Full Day Holiday
                //Tarun Need to check

                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.TechnicianFullDayHoliday'), config('frontapi.TechnicianFullDayHoliday_AR')
                );
                // $responseData = array('status'=>config('frontapi.ERROR'),'message'=> config('frontapi.TechnicianFullDayHoliday'),'message_ar'=> config('frontapi.TechnicianFullDayHoliday_AR'));
                // echo json_encode($responseData);exit;
            } else {
                // check technician break time
                $techbreak = \App\TechnicianHoliday::where('technician_id', $technician)->where('start', '<=', $date)
                                ->where('end', '>=', $date)->first();

                if (count($techbreak) > 0 && isset($techHoliday)) {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techbreak->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techbreak->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }

            // check technician timing        
            $techweekoff = \App\TechnicianTiming::where('technician_id', $technician)->where('weekday', $day)->first();

            //dd($techweekoff);
            if ($techweekoff->weekoff == 1) {

                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.TechnicianWeekOff'), config('frontapi.TechnicianWeekOff_AR')
                );
            } else {
                // check technician break time
                $techbreak = \App\TechnicianBreak::where('technician_id', $technician)->where('weekday', $day)->first();
                if (count($techbreak) > 0) {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techbreak->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techbreak->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }

            // check technician week day leave
            $techdayleave = \App\TechnicianLeave::where('technician_id', $technician)->where('start', '>=', $s_date)
                            ->where('toend', '<=', $e_date)->where('weekday', $day)->first();

            // 
            if (count($techdayleave) > 0) {
                // check technician full day or half day not avalbile
                if ($techdayleave->type == "0") {

                    return $this->sendResponse(
                                    config('frontapi.ERROR'), config('frontapi.TechnicianLeave'), config('frontapi.TechnicianLeave_AR')
                    );
                } else if ($techdayleave->type == "1") {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techdayleave->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techdayleave->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }
        }


        $bookedtimeslot = collect([]);

        $tech_available_slt = collect([]);
        if ($technician != "") {
            $techweekwork = \App\TechnicianTiming::where('technician_id', $technician)
                            ->where('weekday', $day)->first();


            $tech_start = Carbon::instance(new DateTime($techweekwork->starttime));
            $tech_end = Carbon::instance(new DateTime($techweekwork->endtime));
            foreach (new DatePeriod($tech_start, $minInterval, $tech_end) as $techSlot) {
                $tech_available_slt->push($techSlot->format('H:i'));
            }

            $nextDate = Carbon::parse($start)->addDay()->format('Y-m-d H:i:s');
            $startDate = Carbon::parse($start)->format('Y-m-d H:i:s');
            $getBookedAppointments = \App\Booking::where('technician_id', $technician)->where('start', '>=', $startDate)
                            ->where('end', '<=', $nextDate)->whereIn('status', [0, 1])->get();

            if ($getBookedAppointments->count() > 0) {

                foreach ($getBookedAppointments as $book_key => $book_value) {
                    // dd($book_value->start);
                    // $todaysBookings = $book_value->start;
                    // $todayBookinEnd = $book_value->end;
                    $todaysBookings = Carbon::instance(new DateTime($book_value->start));
                    $todayBookinEnd = Carbon::instance(new DateTime($book_value->end));
                    foreach (new DatePeriod($todaysBookings, $minInterval, $todayBookinEnd) as $ownSlot) {
                        $techbreakslot->push($ownSlot->format('H:i'));
                    }
                }
                // dump($techbreakslot->toArray());
                // dd($bookedtimeslot->toArray());
            }
// dump($tech_available_slt);
            //  This final time contains all unavailabe slot for this tech
            $final_time_slot = $techbreakslot->unique()->sort()->all();


            $avalible_array = array_values(array_diff($tech_available_slt->all(), $final_time_slot));
 /*dump(DB::getQueryLog());
 dd($avalible_array);*/
            $curr_timeings = strtotime(date("Y-m-d H:i:s"));
            // dump($curr_timeings);
            // echo $startFrom = '00:00';
            if ($startFrom == 0) {
                $requested_bookingTime = $date . ' 00:00:00';
            } else {
                $requested_bookingTime = $date . ' ' . $startFrom;
            }
            $requested_bookingTime = strtotime($requested_bookingTime);
            // dump($requested_bookingTime);
            if ($requested_bookingTime <= $curr_timeings) {
                $current = date('H:i');
            } else {
                $current = $startFrom;
            }

            // dump($current);
            // dump($total_time);
            // dd(1);
            $slot_required = (ceil($total_time / 15) - 1);
            // dump($avalible_array);
            // available_tech_slots
            // 
            $available_tech_slots = [];

            for ($i = 0; $i < count($avalible_array); $i++) {

                $time = $avalible_array[$i];
                //dump($time);
                if (strtotime($time) >= strtotime($current)) {

                    $add_minutes = (15 * $slot_required) . ' minutes';

                    $expected_slot = strtotime($time . "+ $add_minutes");

                    if (isset($avalible_array[$i + $slot_required])) {
                        $actual_slot = strtotime($avalible_array[$i + $slot_required]);
                        if ($expected_slot == $actual_slot) {
                            //echo $i.'i-'.($expected_slot). ' == '. $actual_slot;
                            $available_tech_slots[] = $time;
                        }
                    }
                }
            }

            if (count($available_tech_slots) > 0) {
                #Need All Available Slots 17Nov16
                // $available_tech_slots = array_slice($available_tech_slots, 0, $make_slot);
                return $this->sendResponse(
                                config('frontapi.SUCCESS'), config('frontapi.slotAvailable'), config('frontapi.slotAvailable_AR'), $available_tech_slots
                );
            } else {
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.NoSlotsAvailable'), config('frontapi.NoSlotsAvailable_AR')
                );
            }
        }

        return $this->sendResponse(
                        config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
        );
    }

    public function checkTechnicianAva_v2($start, $provider_id, $technician, $total_time, $startFrom, $make_slot = 5) {

        // $validator = Validator::make($request->all(), [
        //     'provider_id'   => 'required|integer',
        //     'start'     => 'required|date|date_format:Y-m-d H:i:s',
        //     'technician'   => 'sometimes|integer',
        //     'service_id'   => 'required|integer',
        //     'total_time' => 'required|integer|min:5|max:120',
        // ]);
        // if ($validator->fails())
        //     return $this->checkForValidation($validator);
        // $start = $request->input('start');
        // $provider_id = $request->input('provider_id');
        // $technician = $request->input('technician');
        // $service_id = $request->input('service_id');
        // $total_time = $request->input('total_time');

        $response = '';

        if ($start) {
            $date = Carbon::parse($start)->format('Y-m-d');
        } else {
            $date = Carbon::now()->format('Y-m-d');
            ;
        }

        //If no technician passed, then get all technicians of this service
        // if (empty($technician)) {
        //     $techn = \App\Service::with('technician')->where('status', '=', 1)->find($service_id)->toArray();
        //     foreach ($techn['technician'] as $key => $value) {
        //         $ =
        //     }
        // }
        // DB::enableQueryLog();

        $day = Carbon::parse($start)->format('l'); //date("l", strtotime($date));
        // check technican wkk day leave
        $getdate = strtotime(date("Y-m-d"));
        $today_date = date("Y-m-d");
        // calculate the number of days since Monday
        $dow = date('w', $getdate);


        $offset = $dow - 1;
        if ($offset < 0) {
            $offset = 6;
        }
        // calculate timestamp for Monday and Sunday
        $monday = $getdate - ($offset * 86400);
        $sunday = $monday + (6 * 86400);

        // print dates for Monday and Sunday in the current week
        $s_date = date("Y-m-d", $monday);
        $e_date = date("Y-m-d", $sunday);


        $techdayslot = array();
        $techdateslot = array();
        $techbreakslot = collect([]);
        $minSlotMinutes = 5;
        $minInterval = CarbonInterval::minutes($minSlotMinutes);

        // Check For Provider Availability
        $provider_WeekOff = \App\ProviderTiming::where('provider_id', '=', $provider_id)
                        ->where('weekday', '=', $day)->first();

        if ($provider_WeekOff->weekoff) {

            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.StoreClosed'), config('frontapi.StoreClosed_AR')
            );
        }

        // Provider check for Full Day Leave or Half Day Leave
        $providerLeave = \App\ProviderHoliday::where('provider_id', '=', $provider_id)
                        ->where('start', '<=', $date)
                        ->where('end', '>=', $date)->first();


        if ($providerLeave) {
            if ($providerLeave->type) {
                #check the start and end time
                $providerHolidayTime = Carbon::instance(new DateTime($providerLeave->starttime));
                $providerHoliday_End = Carbon::instance(new DateTime($providerLeave->endtime));
                foreach (new DatePeriod($providerHolidayTime, $minInterval, $providerHoliday_End) as $unavailable) {
                    $techbreakslot->push($unavailable->format('H:i'));
                }
            } elseif ($providerLeave->type == 0) {
                #Full Day Holiday
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.ProviderFullDayHoliday'), config('frontapi.ProviderFullDayHoliday_AR')
                );
            }
        }
// dd($techbreakslot->all());
        if ($technician != "") {
            // check technician holiday
            $techHoliday = \App\TechnicianHoliday::where('technician_id', $technician)->where('start', '<=', $date)
                            ->where('end', '>=', $date)->first();


            if (isset($techHoliday) && $techHoliday->type == 0) {

                #Full Day Holiday

                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.TechnicianFullDayHoliday'), config('frontapi.TechnicianFullDayHoliday_AR')
                );
            } else {
                // check technician break time
                $techbreak = \App\TechnicianHoliday::where('technician_id', $technician)->where('start', '<=', $date)
                                ->where('end', '>=', $date)->first();

                if (count($techbreak) > 0 && isset($techHoliday)) {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techbreak->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techbreak->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }

            // check technician timing           
            $techweekoff = \App\TechnicianTiming::where('technician_id', $technician)->where('weekday', $day)->first();

            //dd($techweekoff);
            if ($techweekoff->weekoff == 1) {

                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.TechnicianWeekOff'), config('frontapi.TechnicianWeekOff_AR')
                );
            } else {
                // check technician break time
                $techbreak = \App\TechnicianBreak::where('technician_id', $technician)->where('weekday', $day)->first();
                if (count($techbreak) > 0) {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techbreak->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techbreak->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }

            // check technician week day leave
            $techdayleave = \App\TechnicianLeave::where('technician_id', $technician)->where('start', '>=', $s_date)
                            ->where('toend', '<=', $e_date)->where('weekday', $day)->first();

            // 
            if (count($techdayleave) > 0) {
                // check technician full day or half day not avalbile
                if ($techdayleave->type == "0") {

                    return $this->sendResponse(
                                    config('frontapi.ERROR'), config('frontapi.TechnicianLeave'), config('frontapi.TechnicianLeave_AR')
                    );
                } else if ($techdayleave->type == "1") {

                    $allreadyBooked_start = Carbon::instance(new DateTime($techdayleave->breakstart));
                    $allreadyBooked_End = Carbon::instance(new DateTime($techdayleave->breakend));
                    foreach (new DatePeriod($allreadyBooked_start, $minInterval, $allreadyBooked_End) as $noSlot) {
                        $techbreakslot->push($noSlot->format('H:i'));
                    }
                }
            }
        }


        $bookedtimeslot = collect([]);

        $tech_available_slt = collect([]);
        if ($technician != "") {
            $techweekwork = \App\TechnicianTiming::where('technician_id', $technician)
                            ->where('weekday', $day)->first();


            $tech_start = Carbon::instance(new DateTime($techweekwork->starttime));
            $tech_end = Carbon::instance(new DateTime($techweekwork->endtime));
            foreach (new DatePeriod($tech_start, $minInterval, $tech_end) as $techSlot) {
                $tech_available_slt->push($techSlot->format('H:i'));
            }

            $nextDate = Carbon::parse($start)->addDay()->format('Y-m-d H:i:s');
            $startDate = Carbon::parse($start)->format('Y-m-d H:i:s');
            $getBookedAppointments = \App\Booking::where('technician_id', $technician)->where('start', '>=', $startDate)
                            ->where('end', '<=', $nextDate)->where('status', 1)->get();

            if ($getBookedAppointments->count() > 0) {

                foreach ($getBookedAppointments as $book_key => $book_value) {
                    // dd($book_value->start);
                    // $todaysBookings = $book_value->start;
                    // $todayBookinEnd = $book_value->end;
                    $todaysBookings = Carbon::instance(new DateTime($book_value->start));
                    $todayBookinEnd = Carbon::instance(new DateTime($book_value->end));
                    foreach (new DatePeriod($todaysBookings, $minInterval, $todayBookinEnd) as $ownSlot) {
                        $techbreakslot->push($ownSlot->format('H:i'));
                    }
                }
                // dump($techbreakslot->toArray());
                // dd($bookedtimeslot->toArray());
            }
// dump($tech_available_slt);
            //  This final time contains all unavailabe slot for this tech
            $final_time_slot = $techbreakslot->unique()->sort()->all();


            $avalible_array = array_values(array_diff($tech_available_slt->all(), $final_time_slot));
// dump(DB::getQueryLog());
// dd($avalible_array);
            $curr_timeings = strtotime(date("Y-m-d H:i:s"));
            // dump($curr_timeings);
            // echo $startFrom = '00:00';
            if ($startFrom == 0) {
                $requested_bookingTime = $date . ' 00:00:00';
            } else {
                $requested_bookingTime = $date . ' ' . $startFrom;
            }
            $requested_bookingTime = strtotime($requested_bookingTime);
            // dump($requested_bookingTime);
            if ($requested_bookingTime <= $curr_timeings) {
                $current = date('H:i');
            } else {
                $current = $startFrom;
            }

            // dump($current);
            // dump($total_time);
            // dd(1);
            $slot_required = (ceil($total_time / 5) - 1);
            // dump($avalible_array);
            // available_tech_slots
            // 
            $available_tech_slots = [];

            for ($i = 0; $i < count($avalible_array); $i++) {

                $time = $avalible_array[$i];
                // dump($time);
                if (strtotime($time) >= strtotime($current)) {

                    $add_minutes = (5 * $slot_required) . ' minutes';

                    $expected_slot = strtotime($time . "+ $add_minutes");

                    if (isset($avalible_array[$i + $slot_required])) {
                        $actual_slot = strtotime($avalible_array[$i + $slot_required]);
                        if ($expected_slot == $actual_slot) {
                            //echo $i.'i-'.($expected_slot). ' == '. $actual_slot;
                            $available_tech_slots[] = $time;
                        }
                    }
                }
            }

            if (count($available_tech_slots) > 0) {
                #Need All Available Slots 17Nov16
                // $available_tech_slots = array_slice($available_tech_slots, 0, $make_slot);
                return $this->sendResponse(
                                config('frontapi.SUCCESS'), config('frontapi.slotAvailable'), config('frontapi.slotAvailable_AR'), $available_tech_slots
                );
            } else {
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.NoSlotsAvailable'), config('frontapi.NoSlotsAvailable_AR')
                );
            }
        }

        return $this->sendResponse(
                        config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
        );
    }

    public function indexEvents() {
        $events = \App\Event::/*APIEvents()->*/with(array('customers' => function($query) {
                        $query->select('id');
                    }))->get();

        $data = [];
        $likedbycustomer = FALSE;
        $authData = \Auth::guard('mobileapp')->user();
        if(!empty($authData)){
            $customer_id = \Auth::guard('mobileapp')->user()->id;
        }else{
            $customer_id = 0;
        }
        foreach ($events as $key => $event) {
            $data[$key] = $event->toArray();
            $data[$key]['likes'] = count($data[$key]['customers']);
            foreach ($data[$key]['customers'] as $customer) {
                if ($customer['id'] == $customer_id) {
                    $likedbycustomer = TRUE;
                    break;
                }
            }
            unset($data[$key]['customers']);
            $data[$key]['liked_by_me'] = ($likedbycustomer) ? 1 : 0;
            $likedbycustomer = FALSE;
        }
        
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
    }
    public function postEventLike(Request $request) {
        $validator = Validator::make($request->all(), [
                    'event_id' => 'required|integer'
        ]);
        if ($validator->fails())
            return $this->checkForValidation($validator);
        
        //$customer_id = \Auth::guard('mobileapp')->user()->id;
        $customer_id = $request->input('customer_id');
        $event_id = $request->input('event_id');
        $insertData = [
            'event_id' => $event_id,
            'customer_id' => $customer_id,
        ];
        $res1= DB::table('event_likes')->where($insertData)->get();
        if(!empty($res1)){ 
            $count=DB::table('event_likes')->where('event_id',$event_id)->get();
            $total=count($count);
           // echo $count; die();
            return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),$total
                   );          
        }
        else
        {
           
           DB::table('event_likes')->insert($insertData);
            $count=DB::table('event_likes')->where('event_id',$event_id)->get();
            $total=count($count);
            return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),$total
           );
        }
    }
//    public function postEventLike(Request $request) {
//        $validator = Validator::make($request->all(), [
//                    'event_id' => 'required|integer'
//        ]);
//
//        if ($validator->fails())
//            return $this->checkForValidation($validator);
//
//        $customer_id = \Auth::guard('mobileapp')->user()->id;
//        $event_id = $request->input('event_id');
//        $insertData = [
//            'event_id' => $event_id,
//            'customer_id' => $customer_id,
//        ];
//        \DB::table('event_likes')->insert($insertData);
//        return $this->sendResponse(
//                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
//        );
//    }
    //updated by jatin
    public function postEventDislike(Request $request) {
        $validator = Validator::make($request->all(), [
                    'event_id' => 'required|integer'
        ]);
        
        if ($validator->fails())
            return $this->checkForValidation($validator);

        //$customer_id = \Auth::guard('mobileapp')->user()->id;
        $customer_id = $request->input('customer_id');
        $event_id = $request->input('event_id');
        $deleteData = [
            'event_id' => $event_id,
            'customer_id' => $customer_id,
        ];
        //\DB::table('event_likes')->update(['envent_id' => $event_id,'customer_id'=>$customer_id]);
         \DB::table('event_likes')->where($deleteData)->delete();
          $count=DB::table('event_likes')->where('event_id',$event_id)->get();
           $total=count($count);
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),$total
        );
    }
    //updated-end by jatin

   public function indexPetStores() {
       // echo "HIII";
        $store=array();
         $stores = \App\Store::AllStore()->get();
        
        foreach($stores as $keys=>$vals)
        {
             $ids=$vals['id'];
           $stores[$keys]['rate']=\DB::table('ratings')
                        
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })
                        ->select(DB::raw('count(rate) as count,avg(rate) average_rate'))
                        ->where(['rating_for' => 'Store', 'provider_id' => $ids])->get();
        }
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $stores
        );
    }

    public function indexBookedAppointmentData(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;
        $paging = $request->input('paging', 20);
        $bookings = \App\Booking::BookingListing($id)->paginate($paging);

        //  Add the Url for the icon, photo

        foreach ($bookings as $key => $value) {

            if (!empty($value['photo'])) {
                $bookings[$key]['photo'] = url('uploads/provider/') . '/' . $value['photo'];
            }

            if (!empty($value['icon'])) {
                $bookings[$key]['icon'] = url('uploads/provider/') . '/' . $value['icon'];
            }

            if (!empty($value['petphoto'])) {
                $bookings[$key]['petphoto'] = url('uploads/pets/') . '/' . $value['petphoto'];
            }
            if ($value['count'] > 1) {
                $bookings[$key]['multiplePets'] = 1;
            }else{
                $bookings[$key]['multiplePets'] = 0;
            }
        }

        $data['bookings'] = $bookings;
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
    }

    public function indexOntimeBookingDetails(Request $request) {
        $validator = Validator::make($request->all(), [
                    'ontime' => 'required',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $id = Auth::guard('mobileapp')->user()->id;
        $ontime = $request->input('ontime');
        $paging = $request->input('paging', 20);

        $bookings = \App\Booking::OnTimeDetailAPI($id, $ontime)->paginate($paging);

        foreach ($bookings as $key => $value) {

            if (!empty($value['photo'])) {
                $bookings[$key]['photo'] = url('uploads/provider/') . '/' . $value['photo'];
            }

            if (!empty($value['icon'])) {
                $bookings[$key]['icon'] = url('uploads/provider/') . '/' . $value['icon'];
            }

            if (!empty($value['petphoto'])) {
                $bookings[$key]['petphoto'] = url('uploads/pets/') . '/' . $value['petphoto'];
            }
        }

        $data['bookings'] = $bookings;
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
        // } else {
        //     return $this->sendResponse(
        //         config('frontapi.ERROR'),
        //         config('frontapi.noBookingsAvailable'),
        //         config('frontapi.noBookingsAvailable_AR'),$id
        //     );
        // }
    }

    public function destroyBookedAppointment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'booking_id' => 'required_without:ontime|integer',
                    'ontime' => 'required_without:booking_id|string',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $book = $request->input('booking_id');
        $ontime = $request->input('ontime');
        $delets = false;
        if (!empty($book) && empty($ontime)) {

            $delets = \App\Booking::destroy($book);
        } elseif (!empty($ontime) && empty($book)) {

            $delets = \App\Booking::where('ontime', $ontime)->delete();
        }

        if ($delets) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Deleted'), config('frontapi.Deleted_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function cancelBookedAppointment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'booking_id' => 'required_without:ontime|integer',
                    'ontime' => 'required_without:booking_id|string',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $book = $request->input('booking_id');
        $ontime = $request->input('ontime');
        $response = false;
        /*if (!empty($book) && empty($ontime)) {
            $cancelAppointment = \App\Booking::findOrFail($book);
            $response = $cancelAppointment->update(['status' => 2]);
        } elseif (!empty($ontime) && empty($book)) {
            $response = \App\Booking::where('ontime', $ontime)->update(['status' => 2]);
        }*/
        $bookobj = Booking::where('ontime', $ontime)->get();
        foreach ($bookobj as $key => $value) {
            $response=true;
            $bookings['booking'][] = $value->id;                       
            $vaccData['status'] = 2;           
            $r = Booking::where('id', $value->id)->update($vaccData); 
            //this for change grom date on confirm and cancel
            $groomObj = Service::select('services.category_id','categories.parentid')
            ->leftjoin('categories', 'categories.id', '=', 'services.category_id')
            ->where('services.id',$value->service_id)
            ->first()->toArray();
            if($groomObj['parentid']==2){ //
                $petLastGroomDdate = \DB::table('pets')->select('last_groom_appointment')->where('id',$value->pet_id)->first();
                /*echo $value->pet_id;*/
                if(!empty($petLastGroomDdate->last_groom_appointment)){
                    Pet::find($value->pet_id)->update(['next_groom_appointment'=>$petLastGroomDdate->last_groom_appointment]);    
                }
            }
            if($groomObj['parentid']==1){ //
                //echo "this for vet";
            }    
        }   

        if ($response) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.BookingAppointmentCanceled'), config('frontapi.BookingAppointmentCanceled_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function confirmBookedAppointment(Request $request) {
        $validator = Validator::make($request->all(), [
                    'booking_id' => 'required_without:ontime|integer',
                    'ontime' => 'required_without:booking_id|string',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $book = $request->input('booking_id');
        $ontime = $request->input('ontime');
        $response = false;
        /*if (!empty($book) && empty($ontime)) {
            $cancelAppointment = \App\Booking::findOrFail($book);
            $response = $cancelAppointment->update(['status' => 1]);
        } elseif (!empty($ontime) && empty($book)) {
            $response = \App\Booking::where('ontime', $ontime)->update(['status' => 1]);
        }*/
        $bookobj = Booking::where('ontime', $ontime)->get();
        foreach ($bookobj as $key => $value) {
            $response=true;
            $bookings['booking'][] = $value->id;                       
            $vaccData['status'] = 1;           
            $r = Booking::where('id', $value->id)->update($vaccData); 
            //this for change grom date on confirm and cancel
            $groomObj = Service::select('services.category_id','categories.parentid')
            ->leftjoin('categories', 'categories.id', '=', 'services.category_id')
            ->where('services.id',$value->service_id)
            ->first()->toArray();
            if($groomObj['parentid']==2){ //
                $petLastGroomDdate = \DB::table('pets')->select('next_groom_appointment')->where('id',$value->pet_id)->first();
                if(!empty($petLastGroomDdate->next_groom_appointment)){
                    Pet::find($value->pet_id)->update(['last_groom_appointment' => $petLastGroomDdate->next_groom_appointment,'next_groom_appointment'=>$value->start]);    
                }
            }
            if($groomObj['parentid']==1){ //
                //echo "this for vet";
            }    
        } 

        if ($response) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.BookingAppointmentConfirm'), config('frontapi.BookingAppointmentConfirm_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

 /*   public function showProviderInformation(Request $request) {

        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = $request->input('provider_id');
        $provider = \App\Provider::find($id);

        if ($provider) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $provider
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.noProviderFound'), config('frontapi.noProviderFound_AR')
            );
        }
    }   */
    
    
        public function showProviderInformation(Request $request) {

        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
        ]);
    
        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id=$request->input('provider_id');
        
        $data = DB::table('providers')
            ->leftJoin('provider_timings', 'providers.id', '=', 'provider_timings.provider_id')
            ->select('providers.*','provider_timings.starttime as Start_time','provider_timings.endtime as End_time')
                ->take(1)
               // ->where('provider_timings.weekday','=>', $dt->format('d'))
                ->where('providers.id',$id)                
            ->get();

        if ($data) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.noRatingsAvailable'), config('frontapi.noRatingsAvailable_AR')
            );
        }
    }

    public function storeTransaction(Request $request) {
        $validator = Validator::make($request->all(), [
                    'result' => 'required',
                    'amount' => 'sometimes|string',
                    'device_id' => 'sometimes|string',
                    'PaymentID' => 'sometimes|string',
                    'PostDate' => 'sometimes|string',
                    'TranID' => 'sometimes|string',
                    'Auth' => 'sometimes|string',
                    'Ref' => 'sometimes|string',
                    'TrackID' => 'sometimes|string',
                    'ip_address' => 'sometimes|string',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = Auth::guard('mobileapp')->user()->id;
        $request['customer_id'] = $id;
        $request['result'] = 0;
        $transaction = \App\Transaction::create($request->all());

        if ($transaction) {
            // TODO Remove this once its live and payment gateway is set
            $transaction['result'] = 1;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $transaction
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.noProviderFound'), config('frontapi.noProviderFound_AR')
            );
        }
    }

    /**
     * All Pets Listing available for Adoption
     * @param  Request $request [description]
     * @return [type]           Pets array
     */
    public function availableForAdopt(Request $request) {
        //\DB::enableQueryLog();
        $paging = $request->input('paging', 20);
        $society = $request->input('society');
        $breed = $request->input('breed');
        
        if ($society == 1 && $breed == 1) {
            // only for society and dogs
            $pet['adoption'] = \App\Pet::Adoptions()->where('providers.ptype', 2)->where('pets.breed', 1)->paginate($paging);
        } elseif ($society == 1 && $breed == 2) {
            // only for society and cats
            $pet['adoption'] = \App\Pet::Adoptions()->where('providers.ptype', 2)->where('pets.breed', 2)->paginate($paging);
        } elseif ($breed == 1) {
            // only for society and dogs
            $pet['adoption'] = \App\Pet::Adoptions()->where('pets.breed', 1)->paginate($paging);
        } elseif ($breed == 2) {
            // only for society and cats
            $pet['adoption'] = \App\Pet::Adoptions()->where('pets.breed', 2)->paginate($paging);
        } elseif ($society == 1) {
            // only for society and cats
            $pet['adoption'] = \App\Pet::Adoptions()->where('providers.ptype', 1)->paginate($paging);
        } else {
            // for dog or cat
            $pet['adoption'] = \App\Pet::Adoptions()->paginate($paging);
        }
        
        if (($pet)) {
            foreach ($pet['adoption'] as $key => $value) {
		$pet_id=$value->id;
		$customer_id=$request->input('customer_id');
		//$customer_id=Auth::guard('mobileapp')->user()->id;
		$findData = [
		    'pet_id' => $pet_id,
		    'customer_id' => $customer_id,
      		  ];
		//echo "<pre>";print_r($findData); die();
		 
		if(!empty(DB::table('pet_likes')->where($findData)->get()))
		{
			$pet['adoption'][$key]['likedByme']="YES";
		}
		else
		{
			$pet['adoption'][$key]['likedByme']="No";
		}               
		$origin=$value->origin;
                $name=$value->name;
                $gen=$value->gender; 
                $breed=$value->breed; 
                $dob=$value->dob;
                
               // $different= $this->dob->diff(Carbon::now())->format('%y years, %m months and %d days');
                
                $diff=\Carbon\Carbon::parse($value->dob)->diff(\Carbon\Carbon::now())->format('%y years, %m months');
                $diff = str_replace("0 years","year",$diff);
                $diff = str_replace("1 years","1 year",$diff);
               // $diff_ar =$diff;
                $diff_ar = str_replace("year","عام",$diff);
                $diff_ar = str_replace("months","الشهور",$diff_ar);

                if($gen=='1'){
                    $gen="Male";
                    $gen_ar="ذكر";
                }else{
                    $gen="Female";
                    $gen_ar="أنثى";
                }
               
                if($breed==2){
                     $breed='Dog';
                     $breed_ar='الكلب';
                }
                else{
                    $breed='Cat';
                    $breed_ar='قط';
                }
                $dob = \Carbon\Carbon::parse($dob)->format('d M Y');
               // echo $name.','.$gen.','.$origin.','.$breed.',"'.$diff.'",(Born on'.$dob.')';
                //die();
               
                $pet['adoption'][$key]['description']=$name.','.$gen.','.$origin.','.$breed.','.$diff.',(Born on ' .$dob.')';
                $pet['adoption'][$key]['description_ar']=$name.','.$gen_ar.','.$origin.','.$breed_ar.','.$diff_ar.',(ولد على ' .$dob.')';
                $pet['adoption'][$key]['temperament'] = $value->getTemparaments();
                if (!empty($value['providers_photo'])) {
                    $pet['adoption'][$key]['providers_photo'] = url('uploads/provider/') . '/' . $value['providers_photo'];
                }

                if (!empty($value['icon'])) {
                    $pet['adoption'][$key]['icon'] = url('uploads/provider/') . '/' . $value['icon'];
                }

                if (!empty($value['petphoto'])) {
                    $pet['adoption'][$key]['petphoto'] = url('uploads/pets/') . '/' . $value['petphoto'];
                }
              
            }
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $pet
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function makeAdoptRequest(Request $request) {
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'title' => 'sometimes|string',
                    'description' => 'sometimes|string',
                    'adopts' => 'required|integer'
        ]);
        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = Auth::guard('mobileapp')->user()->id;

        $data = $request->except(['adopts']);
        $data['customer_id'] = $id;
        $data['sender'] = 1;  #1 is customer
        $requ_adopts = $request->only('adopts');
        $pets = explode(',', $requ_adopts['adopts']);

        $adoption = \App\Adopt::create($data);

        $adoption->pet()->attach($pets);

        if ($adoption) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function storePetLikes(Request $request) {
        $validator = Validator::make($request->all(), [
                    'pet_id' => 'required',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = Auth::guard('mobileapp')->user()->id;

        $data['pet_id'] = $request->input('pet_id');
        $data['customer_id'] = $id;

        $pet_lik = \App\PetLikes::where($data)->exists();

        if ($pet_lik) {
            \DB::table('pet_likes')->where('pet_id', $data['pet_id'])->where('customer_id', $id)->delete();
            $petlike = \DB::table('pets')->where('id', $data['pet_id']);
            $increment = $petlike->decrement('likes');
            $petlike = \DB::table('pets')->where('id', $data['pet_id'])->get();
            $response['total_likes'] = $petlike[0]->likes;
            $response['liked'] = false;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $response
            );
        } else {
            $customer_petLikes = \DB::table('pet_likes')->insert($data);
            $petlike = \DB::table('pets')->where('id', $data['pet_id']);
            $increment = $petlike->increment('likes');
            $petlike = \DB::table('pets')->where('id', $data['pet_id'])->get();
            $response['total_likes'] = $petlike[0]->likes;
            $response['liked'] = true;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $response
            );
        }
    }

    public function destroyPetLikes(Request $request) {
        $validator = Validator::make($request->all(), [
                    'pet_id' => 'required',
		   // 'customer_id' => 'required',
        ]);
        if ($validator->fails())
            return $this->checkForValidation($validator);
            $id = Auth::guard('mobileapp')->user()->id;
	    //$id=$request->input('customer_id');
        $pet_id = $request->input('pet_id');
	$deleteData = [
            'pet_id' => $pet_id,
            'customer_id' => $id,
        ];
       	$get=DB::table('pet_likes')->where($deleteData)->get();
        	if(!empty($get))
        	{
					
                $petlike = \DB::table('pets')->where('id', $pet_id);
                $increment = $petlike->decrement('likes');

        	   $customer_petLikes= \DB::table('pet_likes')->where($deleteData)->delete();
        	   $count=DB::table('pet_likes')->where('pet_id',$pet_id)->get();
                   $total=count($count);
        		
                   if ($customer_petLikes) {
        
					#die('in if now!');
		
                    return $this->sendResponse(
                                    config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),$total
                    );
                   } else {
					   
					   #die('in if else now!');
					   
                    return $this->sendResponse(
                                    config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')  );
                    
                   }		
        
        	}
        	else
        	{
				#die('in else now!');
        		 return $this->sendResponse(
                                    config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR') );
        	}
	
	
    }

    public function availableForVolunteer(Request $request) {
        $type_id = 8; #ID for the volunteer
        $volunt = \App\Provider::GetTypeofProviders($type_id)->get();

        if (count($volunt) > 0) {
            $data['provider'] = $volunt;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function makeVolunteerRequest(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'sometimes|string',
                    'description' => 'sometimes|string',
                    'providers' => 'required',
                    'startdate' => 'required|date',
                    'enddate' => 'required|date',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = Auth::guard('mobileapp')->user()->id;
        $data = $request->except(['providers']);

        $requ_adopts = $request->only('providers');
        $pets = explode(',', $requ_adopts['providers']);

        foreach ($pets as $key) {
            $data['customer_id'] = $id;
            $data['provider_id'] = $key;
            $data['sender'] = 1;  #1 is customer

            $volunteer = \App\Volunteer::create($data);
        }

        if ($volunteer) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function availableForFoster(Request $request) {
        $type_id = 7; #ID for the Foster is 7
        $foster = \App\Provider::GetTypeofProviders($type_id)->get();

        if (count($foster) > 0) {
            $data['provider'] = $foster;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function makeFosteringRequest(Request $request) {
        $validator = Validator::make($request->all(), [
                    'breed_id' => 'required|in:1,2',
                    'title' => 'sometimes|string',
                    'description' => 'sometimes|string',
                    'startdate' => 'required|date',
                    'enddate' => 'required|date',
                    'providers' => 'required'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = Auth::guard('mobileapp')->user()->id;
        $data = $request->except(['providers']);

        $requ_adopts = $request->only('providers');
        $pets = explode(',', $requ_adopts['providers']);

        foreach ($pets as $key) {
            $data['customer_id'] = $id;
            $data['provider_id'] = $key;
            $data['sender'] = 1;  #1 is customer
            $volunteer = \App\Foster::create($data);
        }

        if ($volunteer) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function availableForSurrender(Request $request) {
        $type_id = 9; #ID for the Surrender is 9
        $surrender = \App\Provider::GetTypeofProviders($type_id)->get();

        if (count($surrender) > 0) {
            $data['provider'] = $surrender;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function storeSurrenderRequest(Request $request) {
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required',
                    'pets' => 'required',
                    'title' => 'sometimes|string',
                    'description' => 'sometimes|string',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $id = Auth::guard('mobileapp')->user()->id;
        $data = $request->all();
        $data['customer_id'] = $id;
        $data['sender'] = 1; #sent by customer

        $surren_pets = $request->only('pets');
        $pets = explode(',', $surren_pets['pets']);

        $surren_providers = $request->only('provider_id');
        $providers = explode(',', $surren_providers['provider_id']);

        foreach ($providers as $provider) {
            $data['provider_id'] = $provider;
            $volunteer = \App\Surrender::create($data);
            $volunteer->pet()->attach($pets);
        }
        if ($volunteer) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function availableForKennel(Request $request) {
        #Kennel services are of ID# 5 and # 11
        $kennel = \App\Provider::GetKennelsProviders()->get();
// dd($kennel->toArray());
     foreach($kennel as $keys=>$vals)
        {
           $ids=$vals['id'];
           
           $kennel[$keys]['time']=DB::table('providers')
                ->Join('provider_timings', 'providers.id', '=', 'provider_timings.provider_id')
                ->select('provider_timings.starttime as Start_time','provider_timings.endtime as End_time')
                ->take(1)
                //->where('provider_timings.weekday','=>', $dt->format('d'))
                ->where('providers.id',$vals['id'])                
                ->get();
            
            $breeds = $kennel[$keys]['service_type'];    
            $kennel[$keys]['breed_id'] = $breeds;
           
           $kennel[$keys]['rate']=\DB::table('ratings')
                        
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })
                        ->select(DB::raw('count(rate) as count,avg(rate) average_rate'))
                        ->where(['rating_for' => 'Provider', 'provider_id' => $ids])->get();
        }
        if (count($kennel) > 0) {
            $data['provider'] = $kennel;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    /* public function availableForKennelServices(Request $request) {
      $validator = Validator::make($request->all(), [
      'provider_id' => 'required|integer'
      ]);

      if ($validator->fails())
      return $this->checkForValidation($validator);

      $provider_id= $request->input('provider_id');

      $services = \App\Service::with('category')->where('provider_id',$provider_id)->where('status',1)->where('is_kennel',1)->get();
      //dd($services->toArray());
      if (count($services) > 0) {
      $data['service'] = $services;
      return $this->sendResponse(
      config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
      );
      } else {
      return $this->sendResponse(
      config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
      );
      }
      } */

    /* public function storeKennelRequest(Request $request) {
      $validator = Validator::make($request->all(), [
      'provider_id' => 'required',
      'title' => 'sometimes|string',
      'description' => 'sometimes|string',
      'startdate' => 'required|date',
      'enddate' => 'required|date',
      'pets' => 'required'
      ]);

      if ($validator->fails())
      return $this->checkForValidation($validator);

      $id = Auth::guard('mobileapp')->user()->id;
      $data = $request->all();
      $data['customer_id'] = $id;
      $data['sender'] = 1; #sent by customer

      $kennel = $request->only('pets');
      $pets = explode(',', $kennel['pets']);
      $medicine = $request->input('cuff_medicine');
      $med = explode(',', $medicine);
      $attach = array();
      foreach ($pets as $key => $val) {
      $attach[$val] = ['cuff_medicine' => (isset($med[$key])) ? $med[$key] : 0];
      }
      $storeKennel = \App\Kennel::create($data);
      $storeKennel->pet()->attach($attach);

      if ($storeKennel) {
      return $this->sendResponse(
      config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
      );
      } else {
      return $this->sendResponse(
      config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
      );
      }
      } */

    public function storeKennelRequest(Request $request) {

        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required',
                    'title' => 'sometimes|string',
                    'description' => 'sometimes|string',
                    'startdate' => 'required|date',
                    'enddate' => 'required|date',
                    'pets.*.pet_id' => 'required|integer',
                    'pets.*.cuff_medicine' => 'required',
                    'book.*.services' => 'sometimes',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);
        $customer_id = Auth::guard('mobileapp')->user()->id;
        $kennelData = [];
        $kennelData['customer_id'] = $customer_id;
        $kennelData['startdate'] = $request->input('startdate');
        $kennelData['enddate'] = $request->input('enddate');
        $kennelData['provider_id'] = $request->input('provider_id');
        $kennelData['title'] = $request->input('title');
        $kennelData['description'] = $request->input('description');
        $kennelData['sender'] = 1;
        $storeKennel = \App\Kennel::create($kennelData);
        $pets = $request->only('pets');
        $kennel_pet = array();
        $kennel_service = array();
        foreach ($pets['pets'] as $key => $pet) {
            $kennel_pet[$key] = [
                'pet_id' => $pet['pet_id'],
                'cuff_medicine' => (isset($pet['cuff_medicine'])) ? $pet['cuff_medicine'] : 0
            ];
            if (!empty($pet['services'])) {
                foreach ($pet['services'] as $service) {
                    $kennel_service[] = [
                        'kennel_id' => $storeKennel->id,
                        'pet_id' => $pet['pet_id'],
                        'service_id' => $service
                    ];
                }
            }
        }
        $storeKennel->pet()->attach($kennel_pet);
        \DB::table('kennel_pet_services')->insert($kennel_service);
        if ($storeKennel) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    /* Booking logic update */

    public function logic2postBookAppoint(Request $request) {
        $today = Carbon::yesterday()->format('Y-m-d H:i:s');
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'notes' => 'string',
                    'start' => 'required|date|date_format:Y-m-d H:i:s|after:' . $today,
                    'transaction_id' => 'required',
                    'book.*.pet_id' => 'required|integer',
                    'book.*.technician_id' => 'required',
                    'book.*.service_id' => 'required|integer',
                    'book.*.type_id' => 'required|integer|in:1,2,3,4,5',
                    'book.*.price' => 'string',
                    'book.*.end' => 'required|integer|min:5|max:120',
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);


        $customer_id = \Auth::guard('mobileapp')->user()->id;

        $provider_id = $request->input('provider_id');
        $notes = $request->input('notes');
        $start = $request->input('start');

        $transaction_id = $request->input('transaction_id');  #remove this 1, when transaction ID is set on LIVE 29Nov16

        $book_data = $request->only('book');


        $petsAll = collect($book_data['book']);

        $allPetsData = $petsAll->sortBy('type_id')->groupBy('technician_id');
        // dump($allPetsData->all());

        $ontime = Carbon::now()->timestamp;

        $book = array();
        $startFrom = Carbon::parse($start)->format('H:i');

        foreach ($allPetsData->all() as $technician_id => $value) {

            foreach ($value as $seconKey => $finValue) {


                if (is_int($technician_id) && $technician_id != 'any') {

                    $vet_tech = self::checkTechnicianAva($start, $provider_id, $technician_id, $finValue['end'], $startFrom);
// dd($vet_tech);
                    if ($vet_tech['status'] == 0) {
                        $get_alds[$technician_id] = $vet_tech['data'];
                        $startFrom = $vet_tech['data'][0];
                        $makeStartDate = Carbon::parse($start)->format('Y-m-d') . ' ' . $startFrom;
                        $store_start = Carbon::parse($makeStartDate);
                        $store_data['end'] = $store_start->addMinutes($finValue['end']);
                        $store_data['status'] = 1; # Bookings wil be confirmed bydefault from apps
                        $store_data['start'] = Carbon::parse($makeStartDate);
                        $store_data['ontime'] = $ontime;
                        $store_data['pet_id'] = $finValue['pet_id'];
                        $store_data['technician_id'] = $technician_id;
                        $store_data['transaction_id'] = $transaction_id;
                        $store_data['service_id'] = $finValue['service_id'];
                        $store_data['type_id'] = $finValue['type_id'];
                        $store_data['price'] = $finValue['price'];
                        $store_data['provider_id'] = $provider_id;
                        $store_data['customer_id'] = $customer_id;
                        $store_data['notes'] = $notes;

                        $book[] = \App\Booking::create($store_data)->id;

                        $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                    } else {
                        $get_alds[$technician_id] = $vet_tech['message'];
                    }
                }
                // elseif ($technician_id == 'any') {
                //     $serviceID = $finValue['service_id'];
                //     $allAvailable_Technician = \App\Service::with('technician')->find($serviceID)->toArray();
                //     $allAvailable_Technician = $allAvailable_Technician['technician'];
                //     // dump($allAvailable_Technician);
                //     if(count($allAvailable_Technician) > 0){
                //         foreach ($allAvailable_Technician as $techs) {
                //             // dd($techs);
                //             $technician_id = $techs['id'];
                //             $checkTechData = self::checkTechnicianAva($start,$provider_id, $technician_id, $finValue['end'], $startFrom);
                //             if($checkTechData['status'] == 0){
                //                 // $get_alds[$technician_id]['service_id'] = $serviceID;
                //                 $get_alds[$technician_id] = $checkTechData['data'];
                //                 $startFrom = $checkTechData['data'][0];
                //                 $makeStartDate = Carbon::parse($start)->format('Y-m-d').' '.$startFrom;
                //                 $store_start = Carbon::parse($makeStartDate);
                //                 $store_data['end']  = $store_start->addMinutes($finValue['end']);
                //                 $store_data['status'] = 1;
                //                 $store_data['start'] = Carbon::parse($makeStartDate);
                //                 $store_data['ontime'] = $ontime;
                //                 $store_data['pet_id'] = $finValue['pet_id'];
                //                 $store_data['technician_id'] = $technician_id;
                //                 $store_data['service_id'] = $finValue['service_id'];
                //                 $store_data['transaction_id'] = $transaction_id;
                //                 $store_data['type_id']      = $finValue['type_id'];
                //                 $store_data['provider_id'] = $provider_id;
                //                 $store_data['customer_id'] = $customer_id;
                //                 $store_data['notes'] = $notes;
                //                 $book[] = \App\Booking::create($store_data)->id;
                //                 $startFrom = Carbon::parse($makeStartDate)->addMinutes($finValue['end'])->format('H:i');
                //                 break;
                //             }  else {
                //                 $get_alds[$technician_id] = $checkTechData['message'];
                //             }
                //         }
                //     }
                // }
            }
        }


        if (count($book) > 0) {
            $bookingDatas['booking'] = \App\Booking::select('bookings.*', 'pets.name as petname', 'categories.name as servicename', 'categories.name_er as servicename_ar', 'technicians.name_ar as techname_ar', 'technicians.name as techname', 'providers.name as providername', 'providers.name_ar as providername_ar')
                            ->join('pets', 'bookings.pet_id', '=', 'pets.id')
                            ->join('technicians', 'bookings.technician_id', '=', 'technicians.id')
                            ->join('providers', 'bookings.provider_id', '=', 'providers.id')
                            ->join('services', 'bookings.service_id', '=', 'services.id')
                            ->join('categories', 'services.category_id', '=', 'categories.id')
                            ->whereIn('bookings.id', $book)->get();


            event(new BookingWasMade($book));

            $book = \App\Booking::SingleBookingDetail($book[0])->first();

            $date = $book['start'];
            $good_date = Carbon::parse($date)->format('l, M jS Y');
            $inboxMessage = $book['firstname'] . ' booked ' . $book['servicename'] . ' for ' . $book['petname'] . ' with ' . $book['techname'] . ' at ' . $good_date;
            $bookingDatas['message'] = $inboxMessage;

            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $bookingDatas
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function getGlobalMap(Request $request) {
        $types = array_filter($request->input('type',[]));
        //print_r($types); die();
        $provider_type = [];
        $event = FALSE;
        $petStore = FALSE;
        $area = FALSE;
        $societies = FALSE;
                    
        if (!empty($types)) {
            foreach ($types as $type) {
                if ($type == '') {
                    continue;
                }
                switch ($type) {
                    case 1: $provider_type[] = 1;
                        break;
                    case 2: $provider_type[] = 2;
                        break;
                    case 3: $provider_type[] = 4;
                        break;
                    case 4: $provider_type[] = 5;
                        $provider_type[] = 11;
                        break;
                    case 5: $event = TRUE;
                        break;
                    case 6: $petStore = TRUE;
                        break;
                    case 7: $area = TRUE;
                        break;
                    case 8: $societies = TRUE;
                        break;
                }
            }
        } else {
            $provider_type = [1, 2, 4, 5, 11];
            $event = TRUE;
            $petStore = TRUE;
            $area = TRUE;
            $societies = TRUE;
        }


        if (!empty($provider_type)) {
            $data['providers'] = \App\Provider::GetProvidersMapAPI($provider_type)->get();
        }
        if ($event) {
            $events = \App\Event::APIEvents()->with(array('customers' => function($query) {
                            $query->select('id');
                        }))->get();
            $likedbycustomer = FALSE;
            if(!empty($authData)){
                $customer_id = \Auth::guard('mobileapp')->user()->id;
            }else{
                $customer_id = 0;
            }
            foreach ($events as $key => $event) {
                $events[$key]['likes'] = count($events[$key]['customers']);
                foreach ($events[$key]['customers'] as $customer) {
                    if ($customer['id'] == $customer_id) {
                        $likedbycustomer = TRUE;
                        break;
                    }
                }
                unset($events[$key]['customers']);
                $events[$key]['liked_by_me'] = ($likedbycustomer) ? 1 : 0;
            }
            $data['events'] = $events;
        }

        if ($petStore) {
            $data['pet_store'] = \App\Store::whereType(2)->get();
        }

        if ($area) {
            $data['area'] = \App\Store::whereType(4)->get();
        }

        if ($societies) {
            $data['society'] = \App\Store::whereType(3)->get();
        }
        //dd($kennel);
        /* if ($type == 1) {
          $data['grooming'] = \App\Store::whereType(1)->get();
          } else if ($type == 2) {
          $data['pet_store'] = \App\Store::whereType(2)->get();
          } else if ($type == 3) {
          $data['society'] = \App\Store::whereType(3)->get();
          } else if ($type == 4) {
          $data['area'] = \App\Store::whereType(4)->get();
          } else {
          $data['grooming'] = \App\Store::whereType(1)->get();
          $data['pet_store'] = \App\Store::whereType(2)->get();
          $data['society'] = \App\Store::whereType(3)->get();
          $data['area'] = \App\Store::whereType(4)->get();
          } */

        ///if (!empty($data['grooming']) || !empty($data['pet_store']) || !empty($data['society']) || !empty($data['area'])) {
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
        // } else {
        //    return $this->sendResponse(
        //                    config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
        //    );
        // }
    }
//    public function getProviderTimming(Request $request){
//         $validator = Validator::make($request->all(), [
//                    'provider_id' => 'required|integer'
//        ]);
//         
//          if ($validator->fails())
//           return $this->checkForValidation($validator);
//          $provider_id= $request->input('provider_id');
//          $res= DB::table('event_likes')->where('provider_id',$provider_id)->get();
//          
//          if(!empty($res))
//          {
//            return $this->sendResponse(
//                       config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'),$res
//            );             
//          }
//    }

    public function getEventDetails(Request $request) {
        $validator = Validator::make($request->all(), [
                    'event_id' => 'required|integer'
        ]);
        
        if ($validator->fails())
            return $this->checkForValidation($validator);

        $events = \App\Event::APIEvents()->with('customers')->whereId($request->input('event_id'))->get();
        
        if (!empty($events) && count($events) > 0) {
            $customer_id = \Auth::guard('mobileapp')->user()->id;
            $likedbycustomer = FALSE;
            foreach ($events[0]->customers as $customer) {
                if ($customer->id == $customer_id) {
                    $likedbycustomer = TRUE;
                    break;
                }
            }
            $data = $events[0]->toArray();
            $data['likes'] = count($data['customers']);
            unset($data['customers']);
            $data['liked_by_me'] = ($likedbycustomer) ? 1 : 0;
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function callMissYouRequest(Request $request) {
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'pet_id' => 'required|integer'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $pet = \App\Pet::find($request->pet_id);

        if ($pet) {
            $request['customer_id'] = \Auth::guard('mobileapp')->user()->id;
            $request['sender'] = 1;
            $missYou = \App\MissYou::Create($request->all());
            $missYou->pet()->attach($request->input('pet_id'));

            if ($missYou) {
                return $this->sendResponse(
                                config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR')
                );
            } else {
                return $this->sendResponse(
                                config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
                );
            }
        } else {
            return $this->sendResponse(
                            config('frontapi.NotFound_ERROR'), config('frontapi.noPetFound'), config('frontapi.noPetFound_AR')
            );
        }
    }

    public function getMissYouResponse(Request $request) {
//        \DB::enableQueryLog();
        $missYou = \App\MissYou::allMissYouResponse()->get();

        //dd(\DB::getQueryLog());
        if ($missYou) {
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $missYou
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
    }

    public function cmsPage(Request $request) {
        $pagename = $request->segment(3);
        $data = [];
        $font_div_start = '<div style="font-size: 12.0pt; line-height: 107%;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica;  color: #7f7f7f;">';
        $font_div_end = "</div>";
        if ($pagename == 'aboutus') {
            $data = \App\Content::where('id', 1)->select(['name', 'name_ar', 'content', 'content_ar'])->first();
            $data['content'] = $font_div_start.$data->content.$font_div_end;
            $data['content_ar'] = $font_div_start.$data->content_ar.$font_div_end;
        } elseif ($pagename == 'termsandcondition') {
            $data = \App\Content::where('id', 2)->select(['name', 'name_ar', 'content', 'content_ar'])->first();
            $data['content'] = $font_div_start.$data->content.$font_div_end;
            $data['content_ar'] = $font_div_start.$data->content_ar.$font_div_end;
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR')
            );
        }
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $data
        );
    }

    public function postRatingProvider(Request $request) {

        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
                    'rate' => 'required|numeric',
                    'review' => 'sometimes'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        //$customer_id = \Auth::guard('mobileapp')->user()->id;
        $customer_id = $request->input('customer_id');
        $provider_id = $request->input('provider_id');
        $rate = $request->input('rate');
        $review = $request->input('review');
        $anonymous = $request->input('anonymous', 0);
        $ratingData = [
            'rating_for' => 'Provider',
            'provider_id' => $provider_id,
            'customer_id' => $customer_id,
            'anonymous' => $anonymous,
            'rate' => $rate,
            'review' => $review,
            'status' => ($review == '') ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        \DB::table('ratings')->insert($ratingData);
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.RatingsSuccess'), config('frontapi.RatingsSuccess_AR')
        );
    }

    public function postRatingStore(Request $request) {

        $validator = Validator::make($request->all(), [
                    'store_id' => 'required|integer',
                    'rate' => 'required|numeric',
                    'review' => 'sometimes'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $customer_id = \Auth::guard('mobileapp')->user()->id;
        $provider_id = $request->input('store_id');
        $rate = $request->input('rate');
        $review = $request->input('review');
        $anonymous = $request->input('anonymous', 0);
        $ratingData = [
            'rating_for' => 'Store',
            'provider_id' => $provider_id,
            'customer_id' => $customer_id,
            'anonymous' => $anonymous,
            'rate' => $rate,
            'review' => $review,
            'status' => ($review == '') ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        \DB::table('ratings')->insert($ratingData);
        return $this->sendResponse(
                        config('frontapi.SUCCESS'), config('frontapi.RatingsSuccess'), config('frontapi.RatingsSuccess_AR')
        );
    }

    public function getRatingProvider(Request $request) {
        $validator = Validator::make($request->all(), [
                    'provider_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = $request->input('provider_id');
        $ratingdata = \DB::table('ratings')->select('rate', 'review', 'ratings.status', 'anonymous', 'ratings.created_at as rated_on', DB::raw('CONCAT(customers.firstname," ",customers.lastname) as customer'))
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })
                        ->where(['rating_for' => 'Provider', 'provider_id' => $id, 'ratings.status' => 1])
                        ->get();

        if ($ratingdata) {
            foreach ($ratingdata as $key => $value) {
                if ($value->status == 0) {
                    $ratingdata[$key]->review = null;
                }
                unset($ratingdata[$key]->status);
            }
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $ratingdata
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.noRatingsAvailable'), config('frontapi.noRatingsAvailable_AR')
            );
        }
    }

    public function getRatingStore(Request $request) {
        $validator = Validator::make($request->all(), [
                    'store_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return $this->checkForValidation($validator);

        $id = $request->input('store_id');
        $ratingdata = \DB::table('ratings')->select('rate', 'review', 'ratings.status', 'anonymous', 'ratings.created_at as rated_on', DB::raw('CONCAT(customers.firstname," ",customers.lastname) as customer'))
                        ->leftJoin('customers', function ($join) {
                            $join->on('customers.id', '=', 'ratings.customer_id')
                            ->where('ratings.anonymous', '=', 0);
                        })
                        ->where(['rating_for' => 'Store', 'provider_id' => $id])->get();
      //  $ratingdata = $ratingdata->toArray();
        if ($ratingdata) {
            foreach ($ratingdata as $key => $value) {
                if ($value->status == 0) {
                    $ratingdata[$key]->review = null;
                }
                unset($ratingdata[$key]->status);
            }
            return $this->sendResponse(
                            config('frontapi.SUCCESS'), config('frontapi.Valid'), config('frontapi.Valid_AR'), $ratingdata
            );
        } else {
            return $this->sendResponse(
                            config('frontapi.ERROR'), config('frontapi.noRatingsAvailable'), config('frontapi.noRatingsAvailable_AR')
            );
        }
    }
    public function changePassword(Request $request) {
        $id = Auth::guard('mobileapp')->user()->id;
        ini_set("memory_limit", -1);

        $validator = Validator::make($request->all(), [
                    'currentPassword' => 'required|min:6',
                    'newPassword' => 'required|min:6'
        ]);

        if ($validator->fails())
            return $this->checkForValidation($validator);

        $customer = \DB::table('customers')->select('id','password')->where('id','=',$id)->get();              
         if(Hash::check($request->currentPassword, $customer[0]->password))
         {           
            $vaccData1['password'] = Hash::make($request->newPassword);
            $r1 = Customer::where('id',$id)->update($vaccData1); 
            if ($r1) {
                return $this->sendResponse(config('frontapi.SUCCESS'), 'Password change successfull', 'Password change successfull');
            } else {
                 return $this->sendResponse(config('frontapi.ERROR'), config('frontapi.Invalid'), config('frontapi.Invalid_AR'));
            }
         }
         else
         {           
           return $this->sendResponse(config('frontapi.ERROR'), 'Please enter correct current password', 'Please enter correct current password');
         }
    }

}
