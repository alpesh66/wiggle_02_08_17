<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProviderTiming;


class ProviderTimingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index()
    {   
        $provider_id = session('provider_id');
        $protiming = ProviderTiming::where('provider_id', '=', $provider_id)->paginate(10);
        // echo '<pre>'; print_r($protiming); exit;
        return view('servicepro.providertiming.index', compact('protiming'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $provider_id = session('provider_id');
        $protiming = ProviderTiming::where('provider_id', '=', $provider_id)->paginate(10);
        // echo '<pre>'; print_r($protiming); exit;
        return view('servicepro.providertiming.create', compact('protiming'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatetime(Request $request)
    {
        $requestData = $request->all();

        $protiming = ProviderTiming::findOrFail($requestData['id']);
        $protiming->update($request->all());

        return json_encode(array('status'=>'1','message'=>'Time has been updated successfully.'));      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
                              
        
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
   
}