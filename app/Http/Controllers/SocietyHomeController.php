<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Foster;
use App\Adopt;
use App\Volunteer;
use App\Surrender;
use App\Pet;
use App\Kennel;
use App\MissYou;
use App\Provider;
use Session;
use Image;
use Carbon\Carbon;
//use App\Initiative;


class SocietyHomeController extends Controller
{
    public function index(Request $request)
    {
        $user = \Auth::user();
        $provider_id = session('provider_id');

        // count
        $foster = Foster::where('provider_id', $provider_id)->where('sender',1)->count();
        $fosterApprove = Foster::where('provider_id', $provider_id)->where('sender',1)->where('status',1)->count();
        $fosterRejected = Foster::where('provider_id', $provider_id)->where('sender',1)->where('status',2)->count();
        $fosterPending = Foster::where('provider_id', $provider_id)->where('sender',1)->where('status',0)->count();

        $adopt = Adopt::where('provider_id', $provider_id)->where('sender',1)->count();
        $adoptDetail = Adopt::with('pet')->where('provider_id', $provider_id)->where('sender',1)->get();
        $adoptDog=0;
        $adoptCat=0;
        $adoptPending=0;
        $adoptRejected=0;
        $successAdopt=0;
        $adoptSucDog=0;
        $adoptSucCat=0;
        foreach ($adoptDetail as $key => $value) {
            $value=$value->toArray();
            if($value['status']==1){
                $successAdopt++;
                if(isset($value['pet'][0]['breed']) && $value['pet'][0]['breed']==1){
                    $adoptSucCat++;    
                }else{
                    $adoptSucDog++;    
                }    
                
                
            }
            if($value['status']==2){
                $adoptRejected++;
            }
            if($value['status']==0){
             $adoptPending++;   
            }
            
            if(isset($value['pet'][0]['breed']) && $value['pet'][0]['breed']==1){
                $adoptCat++;    
            }else{
                $adoptDog++;    
            }

        }

        $volunteer = Volunteer::where('provider_id', $provider_id)->where('sender',1)->count();
        $volunteerApprove = Volunteer::where('provider_id', $provider_id)->where('sender',1)->where('status',1)->count();
        $volunteerRejected = Volunteer::where('provider_id', $provider_id)->where('sender',1)->where('status',2)->count();
        $volunteerPending = Volunteer::where('provider_id', $provider_id)->where('sender',1)->where('status',0)->count();

        $surrender = Surrender::where('provider_id', $provider_id)->where('sender',1)->count();
        $surrenderApprove = Surrender::where('provider_id', $provider_id)->where('sender',1)->where('status',1)->count();
        $surrenderRejected = Surrender::where('provider_id', $provider_id)->where('sender',1)->where('status',2)->count();
        $surrenderPending = Surrender::where('provider_id', $provider_id)->where('sender',1)->where('status',0)->count();

        $pet = Pet::where('provider_id', $provider_id)->whereIN('pet_status', ['Draft','Available','Foster'])->count();
        $petDraft = Pet::where('provider_id', $provider_id)->where('pet_status', 'Draft')->count();
        $petAvailable = Pet::where('provider_id', $provider_id)->where('pet_status', 'Available')->count();
        $petFoster = Pet::where('provider_id', $provider_id)->where('pet_status', 'Foster')->count();

        $kennel = Kennel::where('provider_id', $provider_id)->where('sender',1)->count();
        $kennelApprove = Kennel::where('provider_id', $provider_id)->where('sender',1)->where('status',1)->count();
        $kennelRejected = Kennel::where('provider_id', $provider_id)->where('sender',1)->where('status',2)->count();
        $kennelPending = Kennel::where('provider_id', $provider_id)->where('sender',1)->where('status',0)->count();

        // listing 
        $from = Carbon::now()->addDays(1)->format('Y-m-d');
        $to =  Carbon::now()->addDays(8)->format('Y-m-d');
        
        $foster_list = Foster::with('customer')->where('provider_id', $provider_id)->whereBetween('enddate', array($from, $to))->where('status',1)->take(5)->get();
        $foster_list_pending = Foster::with('customer')->where('provider_id', $provider_id)->whereBetween('startdate', array($from, $to))->where('status',0)->take(5)->get();

        $adopt_list = Adopt::with('customer')->where('provider_id', $provider_id)->take(5)->get();          
        $volunteer_list = Volunteer::with('customer')->where('provider_id', $provider_id)->whereBetween('enddate', array($from, $to))->take(5)->get();
        $surrender_list = Surrender::with('customer')->where('provider_id', $provider_id)->take(5)->get();            
        $pet_list = Pet::with('breeds')->where('provider_id', $provider_id)->take(5)->get();
        $kennel_list = Kennel::with('customer')->where('provider_id', $provider_id)->take(5)->get();
        
        $provider_type_count=\DB::table('provider_type')->where('provider_id',Session::get('provider_id'))->where('type_id', 11)->count();
        if($provider_type_count>0){
            $totalKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('sender',1)->count();
            $approveKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '1')->where('sender',1)->count();
            $rejectedKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '2')->where('sender',1)->count();        
            $pendingKennel = Kennel::where('provider_id',Session::get('provider_id'))->where('status', '0')->where('sender',1)->count();
        }else{
            $totalKennel = 0;
            $approveKennel = 0;
            $rejectedKennel = 0;        
            $pendingKennel = 0;

        }
        $rankArray = array('Adopt'=>$adopt,'Foster'=>$foster,'Volunteer'=>$volunteer,'Surrender'=>$surrender);
        arsort($rankArray);
        return view('society.dashboard', compact('adopt','foster','volunteer','surrender','pet','kennel','foster_list','adopt_list','volunteer_list','surrender_list','pet_list','kennel_list','provider_type_count','totalKennel','approveKennel','rejectedKennel','pendingKennel','petDraft','petAvailable','petFoster','kennelApprove','kennelRejected','kennelPending','adoptDog','adoptCat','successAdopt','adoptSucCat','adoptSucDog','adoptRejected','adoptPending','volunteerApprove','volunteerRejected','volunteerPending','surrenderApprove','surrenderRejected','surrenderPending','fosterApprove','fosterRejected','fosterPending','rankArray','foster_list_pending'));
    }
     public function profilesociety()
    {

        $id = session('provider_id');
        $provider = Provider::findOrFail($id);
        $type = \App\Type::pluck('dis_name','id');
        $country = \App\Country::pluck('nicename','phonecode');
        $country_code = explode('-', $provider['contact']);
        $area = \App\Area::pluck('name','id');

        return view('society.profile.edit', compact('provider','type','area', 'country','country_code'));
    }
    public function updatesociety(Request $request)
    {
        /*echo "<pre>";*/
        $id = session('provider_id');
        $provider = Provider::findOrFail($id);
        //$request['contact'] = $request->input('country').'-'.$request->input('contact');
        $data = $request->all();
        
        if ($request->hasFile('photo') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis");
            $file_extention = '.'.$request->file('photo')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('photo')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['photo'] = $photo_englist;
               
        }

        if ($request->hasFile('icon') ) {

            $destinationPath = public_path('uploads/provider');
            $photoname = date("YmdHis").'icon';
            $file_extention = '.'.$request->file('icon')->getClientOriginalExtension();
            $photo_englist = $photoname.$file_extention;
            $file_check = $request->file('icon')->move($destinationPath, $photo_englist);

            $thumb_path = $destinationPath.'/thumbnail/'.$photo_englist;

            $assets_path = public_path('uploads/provider/');

            $img = Image::make($assets_path.'/'.$photo_englist)->fit(200)->save($thumb_path);
            $data['icon'] = $photo_englist;
               
        }

        $provider->update($data);
        Session::flash('flash_message', 'Society updated!');
        return redirect('societies/profile');
    }   
}
