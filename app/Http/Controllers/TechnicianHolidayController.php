<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TechholidayRequest;

use Carbon\Carbon;
use App\TechnicianHoliday;
use Illuminate\Http\Request;
use Session;

class TechnicianHolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $provider_id = session('provider_id');
        // $techleave = TechnicianHoliday::with('technician')->paginate(25);
        // echo '<pre>'; print_r($techleave); exit;
        if ($request->ajax()){
            $provider_id = session('provider_id');
            // $technicianholiday = TechnicianHoliday::with('technician')->where('provider_id', '=', $provider_id);
            $technicianholiday = TechnicianHoliday::select(['technician_holidays.id as tid','technician_holidays.weekday','technician_holidays.start','technician_holidays.end','technicians.title','technicians.name','technician_holidays.breakstart','technician_holidays.breakend','technician_holidays.type'])
                ->join('technicians', 'technicians.id', '=', 'technician_holidays.technician_id')
                ->where('technician_holidays.provider_id', session('provider_id'));

            $datatables =  app('datatables')->of($technicianholiday)
                ->editColumn('start', function($technicianholiday) {
                    if($technicianholiday->type == '1'){
                        $start = $technicianholiday->start.' '.$technicianholiday->breakstart;
                        $start = Carbon::parse($start)->format('l jS F Y h:i a');
                    } else {
                        $start = $technicianholiday->start;
                        $start = Carbon::parse($start)->format('l jS F Y');
                    }
                    return $start;
                    // return Carbon::parse($technicianholiday->start)->format('l jS F Y');
                })
                ->editColumn('end', function($technicianholiday) {
                    if($technicianholiday->type == '1'){
                        $end = $technicianholiday->end.' '.$technicianholiday->breakend;
                        $end = Carbon::parse($end)->format('l jS F Y h:i a');
                    } else {
                        $end = $technicianholiday->end;
                        $end = Carbon::parse($end)->format('l jS F Y');
                    }
                    return $end;
                    // return Carbon::parse($technicianholiday->end)->format('l jS F Y');
                })
                ->addColumn('action', function($technicianholiday) {
                    return view('servicepro.technicianholiday.action', compact('technicianholiday'))->render();
                });
                
            // additional Search parameter
            $post       = $datatables->request->get('post');
            $operator   = $datatables->request->get('operator');
            $name       = $datatables->request->get('name');

            if($operator && $operator == 'like')
            {
                $post = '%'.$post.'%';
            }

            if($name && $name == 'status')
            {
                $val = $post;
                if(strtolower($val) == 'active'){
                    $post = '1';
                } else {
                    $post = '0';
                }
            }

            if ($post != '' ) {
                $datatables->where( $name, $operator, $post);
            }

            return $datatables->make(true);
        }
        return view('servicepro.technicianholiday.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $provider_id = Session::get('provider_id');
        $technicians = \App\Technician::where('provider_id', $provider_id )->pluck('name','id');

        return view('servicepro.technicianholiday.create',compact('technicians'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TechholidayRequest $request)
    {        
        $provider_id = Session::get('provider_id');
        $requestData = $request->all();
// dd($requestData);
        $requestData['provider_id'] = $provider_id;  
        if(isset($requestData['start']) && $requestData['start'] != '' && isset($requestData['end']) && $requestData['end'] != ''){
            $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
            $requestData['end'] = date('Y-m-d',strtotime($requestData['end'])); 
        }
        
        if(isset($requestData['status']) && $requestData['status'] == 'on'){
            $requestData['status'] = 1; 
        } else {
            $requestData['status'] = 0;
        }

        TechnicianHoliday::create($requestData);

        Session::flash('flash_message', ' added!');

        return redirect('providers/technicianholiday');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $technicianholiday = TechnicianHoliday::with('technician')->findOrFail($id);
        $technicianholiday['start'] = Carbon::parse($technicianholiday['start'])->format('l jS F Y');
        $technicianholiday['end'] = Carbon::parse($technicianholiday['end'])->format('l jS F Y');
        return view('servicepro.technicianholiday.show', compact('technicianholiday'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $provider_id = Session::get('provider_id');
        $technicianholiday = TechnicianHoliday::findOrFail($id);
        $technicians = \App\Technician::where('provider_id', $provider_id )->pluck('name','id');
        // echo '<pre>'; print_r($technicianholiday); exit;
        return view('servicepro.technicianholiday.edit', compact('technicianholiday','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, TechholidayRequest $request)
    {
        
        $requestData = $request->all();
        
        if(isset($requestData['start']) && $requestData['start'] != '' && isset($requestData['end']) && $requestData['end'] != ''){
            $requestData['start'] = date('Y-m-d',strtotime($requestData['start']));  
            $requestData['end'] = date('Y-m-d',strtotime($requestData['end'])); 
        }       
        
        if(isset($requestData['status']) && $requestData['status'] == 'on'){
           $requestData['status'] = 1; 
        } else {
            $requestData['status'] = 0;
        }

        // echo '<pre>'; print_r($requestData); exit;
        $techleave = TechnicianHoliday::findOrFail($id);
        $techleave->update($requestData);

        Session::flash('flash_message', ' updated!');

        return redirect('providers/technicianholiday');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    // public function destroy($id)
    // {
    //     TechnicianHoliday::destroy($id);

    //     Session::flash('flash_message', 'Attendence deleted successfully!');

    //     return redirect('providers/technicianholiday');
    // }

    public function destroy(Request $request, $id)
    {
        
        // Requests from the Datatables
        if ($request->ajax() )
        {
            $ids = $request->input('id');
            $isDelete = TechnicianHoliday::destroy($ids);
            
            if($isDelete)
            {
                $data['success'] = true;
                $data['message'] = 'Attendence deleted successfully';    
            } else {
                $data['success'] = false;
                $data['message'] = 'Some internal error occurred';
            }
            
            return $data;
        }

        TechnicianHoliday::destroy($id);

        Session::flash('flash_message', 'Attendence deleted successfully');

        return redirect('providers/technicianholiday');
    }
}
