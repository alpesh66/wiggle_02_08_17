<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Customer;
use App\Booking;
use App\Pet;
use App\Store;
use App\Technician;
use App\Rating;
use App\Provider;
use App\Category;
use Mail;
use Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use DatePeriod;
use Session;
use DB;
use Excel;
use PHPExcel_Worksheet_Drawing;
class SocietyReportController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        /*if ($request->ajax())
        {
            return $this->callDatatables($request);   
        }*/
        /*$categories = Category::ActiveParentListing()->pluck('name', 'id')->toArray();*/
        $BookingStatus=array("1"=>"Approve","2"=>"Cancel","0"=>"Pending");
        $type=array("adopts"=>"Adopt","fosters"=>"Foster","surrenders"=>"Surrenders","volunteers"=>"Volunteers");
        return view('society.report.index', compact('type','BookingStatus'));
    }
    
    public function generateReport(Request $request)
    {
        $provider_id = Session::get('provider_id');
        $nameProvider = Session::get('nameProvider');

        
        
        $booking_status = $request->booking_status;
        $type = $request->type;
        
        if(empty($booking_status)){
            $booking_status=array("0"=>"1","1"=>"2","2"=>"0");
        }
        if(empty($type)){
            $type=array("0"=>"adopts","1"=>"fosters","2"=>"surrenders","3"=>"volunteers");
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $startdate = $request->startdate;
            $startdate = date_create($startdate);
            $book_startdate = date_format($startdate, 'Y-m-d');    
        }else{
            $startdate = "";
            $book_startdate ="";
        }
        if(isset($request->startdate) && $request->startdate!=""){
            $enddate = $request->enddate;
            $enddate = date_create($enddate);
            $book_enddate = date_format($enddate, 'Y-m-d');
        }else{
            $book_enddate ="";
        } 
        $customer1 =array();
        foreach ($type as $key => $value) {
            if($value=="adopts"){
                $sub_table="adopt_pet";
                $sub_table_id="adopt_id";
            }
            if($value=="fosters"){
                $sub_table="foster_pet";
                $sub_table_id="foster_id";
            }
            if($value=="surrenders"){
                $sub_table="pet_surrender";
                $sub_table_id="surrender_id";
            }
            if($value=="volunteers"){
                $sub_table="pet_volunteer";
                $sub_table_id="volunteer_id";
            }
            $customersObj = Customer::
                select([$value.'.updated_at as replyDate',$value.'.created_at as requestDate',$value.'.id', 'customers.firstname', 'customers.middlename','customers.lastname', 'customers.email','customers.mobile', $value.'.status',$value.'.type','pets.breed','pets.name as petName'])
                ->leftjoin($value, 'customers.id', '=', $value.'.customer_id')
                ->leftjoin($sub_table, $sub_table.'.'.$sub_table_id, '=', $value.'.id')
                ->leftjoin('pets', $sub_table.'.pet_id', '=', 'pets.id')
                ->where($value.'.provider_id',$provider_id)
                ->where($value.'.sender',1)
                ->whereIn($value.'.status',$booking_status);

            if(isset($book_startdate)  && $book_startdate!="")
                $customersObj->where($value.'.created_at','>=',$book_startdate);
            if(isset($book_enddate)  && $book_enddate!="")
                $customersObj->where($value.'.created_at','<=',$book_enddate);

            $customersObj = $customersObj->get();
            
            $customers = json_decode(json_encode($customersObj), true);
            $customer1 = array_merge($customer1,$customers);
        }
        $total_request=0;
        $total_approve=0;
        $total_reject=0;
        $total_pending=0;
        $adopts=0;
        $fosters=0;
        $surrenders=0;
        $volunteers=0;

        $adopts_approve=0;
        $adopts_reject=0;
        $adopts_pending=0;

        $fosters_approve=0;
        $fosters_reject=0;
        $fosters_pending=0;

        $surrenders_approve=0;
        $surrenders_reject=0;
        $surrenders_pending=0;

        $volunteers_approve=0;
        $volunteers_reject=0;
        $volunteers_pending=0;
        foreach ($customer1 as $key => $value) {

            $customers_name=$value['firstname']." ".$value['lastname'];
            if($value['breed']==1)
                $breed="Cat";
            else
                $breed="Dog";

            if($value['status']==1){
                $status="Approved";
                $total_approve++;        
            }
            if($value['status']==2){
                $status="Rejected";
                $total_reject++;        
            }
            if($value['status']==0){
                $status="Pending";
                $total_pending++;
            }
            $type="";
            if($value['type']=="adopts"){
                $adopts++;
                if($value['status']==1){
                    $adopts_approve++;        
                }
                if($value['status']==2){
                    $adopts_reject++;        
                }
                if($value['status']==0){
                    $adopts_pending++;        
                }
                $type="Adopt";
            }
            if($value['type']=="fosters"){
                $fosters++;        
                if($value['status']==1){
                    $fosters_approve++;        
                }
                if($value['status']==2){
                    $fosters_reject++;        
                }
                if($value['status']==0){
                    $fosters_pending++;        
                }
                $type="Foster";
            }
            if($value['type']=="surrenders"){
                $surrenders++;        
                if($value['status']==1){
                    $surrenders_approve++;        
                }
                if($value['status']==2){
                    $surrenders_reject++;        
                }
                if($value['status']==0){
                    $surrenders_pending++;        
                }
                $type="Surrender";
            }
            if($value['type']=="volunteers"){
                $volunteers++;
                if($value['status']==1){
                    $volunteers_approve++;        
                }
                if($value['status']==2){
                    $volunteers_reject++;        
                }
                if($value['status']==0){
                    $volunteers_pending++;        
                }
                $type="Volunteer";
            }
            $total_request++;

            $data[] = array(
                    $value['id'],
                    $value['petName'],
                    $breed,
                    $customers_name,                    
                    $type,
                    $status,
                    $value['requestDate'],
                    $value['replyDate'],
                );
        }
        
        if($request->startdate!="" && $request->enddate!="")
             $reportDate = $request->startdate." to ".$request->enddate;
        else
             $reportDate = "All";

        $pet = Pet::where('provider_id', $provider_id)->whereIN('pet_status', ['Draft','Available','Foster'])->count();
        /*echo "Total pet:".$pet."<br>";*/
        $petDraft = Pet::where('provider_id', $provider_id)->where('pet_status', 'Draft')->count();
        $draft_dog= Pet::where(['provider_id'=>$provider_id,'breed'=>2])->whereIN('pet_status',['Draft'])->count();
        $draft_cat= Pet::where(['provider_id'=>$provider_id,'breed'=>1])->whereIN('pet_status',['Draft'])->count();
        if(isset($petDraft) && $petDraft!=0){
            $draft_dog_percent=$draft_dog*100/$petDraft;
            $draft_cat_percent=$draft_cat*100/$petDraft;
        }else{
            $draft_dog_percent=0;
            $draft_cat_percent=0;
        }
        /*echo "draft dog pet:".$draft_dog."<br>";
        echo "draft cat pet:".$draft_cat."<br>";
        echo "draft pet:".$petDraft."<br>";*/
        $petAvailable = Pet::where('provider_id', $provider_id)->where('pet_status', 'Available')->count();
        $available_dog= Pet::where(['provider_id'=>$provider_id,'breed'=>2])->whereIN('pet_status',['Available'])->count();
        $available_cat= Pet::where(['provider_id'=>$provider_id,'breed'=>1])->whereIN('pet_status',['Available'])->count();
        if(isset($petAvailable) && $petAvailable!=0){
            $available_dog_percent=$available_dog*100/$petAvailable;
            $available_cat_percent=$available_cat*100/$petAvailable;
        }else{
            $available_dog_percent=0;
            $available_cat_percent=0;
        }
        /*echo "available dog pet:".$available_dog."<br>";
        echo "available_cat cat pet:".$available_cat."<br>";
        echo "Available pet:".$petAvailable."<br>";*/
        $petFoster = Pet::where('provider_id', $provider_id)->where('pet_status', 'Foster')->count();
        $foster_dog= Pet::where(['provider_id'=>$provider_id,'breed'=>2])->whereIN('pet_status',['Foster'])->count();
        $foster_cat= Pet::where(['provider_id'=>$provider_id,'breed'=>1])->whereIN('pet_status',['Foster'])->count();
        if(isset($petFoster) && $petFoster!=0){
            $foster_dog_percent=$foster_dog*100/$petFoster;
            $foster_cat_percent=$foster_cat*100/$petFoster;
        }else{
            $foster_dog_percent=0;
            $foster_cat_percent=0;
        }
        /*echo "fosters dog pet:".$foster_dog."<br>";
        echo "fosters cat pet:".$foster_cat."<br>";
        echo "fosters pet:".$petFoster."<br>";*/
        $pet_dog= Pet::where(['provider_id'=>$provider_id,'breed'=>2])->whereIN('pet_status',['Draft','Foster','Available'])->count();
        /*echo "dog pet:".$pet_dog."<br>";*/
        $pet_cat= Pet::where(['provider_id'=>$provider_id,'breed'=>1])->whereIN('pet_status',['Draft','Foster','Available'])->count();
        /*echo "dog pet:".$pet_cat."<br>";*/
        if(isset($pet) && $pet!=0){
            $petDraft_percent=$petDraft*100/$pet;
            $petAvailable_percent=$petAvailable*100/$pet;
            $petFoster_percent=$petFoster*100/$pet;
            $pet_dog_percent=$pet_dog*100/$pet;
            $pet_cat_percent=$pet_cat*100/$pet;
        }else{
            $petDraft_percent=0;
            $petAvailable_percent=0;
            $petFoster_percent=0;
            $pet_dog_percent=0;
            $pet_cat_percent=0;
        }
        if(isset($total_request) && $total_request!=0){
            $adopts_percent=$adopts*100/$total_request;
            $fosters_percent=$fosters*100/$total_request;
            $volunteers_percent=$volunteers*100/$total_request;
            $surrenders_percent=$surrenders*100/$total_request;

            $total_approve_percent=$total_approve*100/$total_request;
            $total_reject_percent=$total_reject*100/$total_request;
            $total_pending_percent=$total_pending*100/$total_request;
        }else{
            $adopts_percent=0;
            $fosters_percent=0;
            $volunteers_percent=0;
            $surrenders_percent=0;

            $total_approve_percent=0;
            $total_reject_percent=0;
            $total_pending_percent=0;
        }
        if(isset($total_approve) && $total_approve!=0){
            $adopts_approve_percent = $adopts_approve*100/$total_approve;
            $fosters_approve_percent = $fosters_approve*100/$total_approve;
            $surrenders_approve_percent = $surrenders_approve*100/$total_approve;
            $volunteers_approve_percent = $volunteers_approve*100/$total_approve;
        }else{
            $adopts_approve_percent = 0;
            $fosters_approve_percent = 0;
            $surrenders_approve_percent = 0;
            $volunteers_approve_percent = 0;
        }
        $adopts_approve_percent = round($adopts_approve_percent,2)." %" ;
        $fosters_approve_percent = round($fosters_approve_percent,2)." %" ;
        $surrenders_approve_percent = round($surrenders_approve_percent,2)." %" ;
        $volunteers_approve_percent = round($volunteers_approve_percent,2)." %" ;

        if(isset($total_pending) && $total_pending!=0){
            $adopts_pending_percent = $adopts_pending*100/$total_pending;
            $fosters_pending_percent = $fosters_pending*100/$total_pending;
            $surrenders_pending_percent = $surrenders_pending*100/$total_pending;
            $volunteers_pending_percent = $volunteers_pending*100/$total_pending;
        }else{
            $adopts_pending_percent = 0;
            $fosters_pending_percent = 0;
            $surrenders_pending_percent = 0;
            $volunteers_pending_percent = 0;
        }
        $adopts_pending_percent = round($adopts_pending_percent,2)." %" ;
        $fosters_pending_percent = round($fosters_pending_percent,2)." %" ;
        $surrenders_pending_percent = round($surrenders_pending_percent,2)." %" ;
        $volunteers_pending_percent = round($volunteers_pending_percent,2)." %" ;

        if(isset($total_reject) && $total_reject!=0){
            $adopts_reject_percent = $adopts_reject*100/$total_reject;
            $fosters_reject_percent = $fosters_reject*100/$total_reject;
            $surrenders_reject_percent = $surrenders_reject*100/$total_reject;
            $volunteers_reject_percent = $volunteers_reject*100/$total_reject;
        }else{
            $adopts_reject_percent = 0;
            $fosters_reject_percent = 0;
            $surrenders_reject_percent = 0;
            $volunteers_reject_percent = 0;
        }
        $adopts_reject_percent = round($adopts_reject_percent,2)." %" ;
        $fosters_reject_percent = round($fosters_reject_percent,2)." %" ;
        $surrenders_reject_percent = round($surrenders_reject_percent,2)." %" ;
        $volunteers_reject_percent = round($volunteers_reject_percent,2)." %" ;

        $total_approve_percent = round($total_approve_percent,2)." %" ;
        $total_reject_percent = round($total_reject_percent,2)." %" ;
        $total_pending_percent = round($total_pending_percent,2)." %" ;

        $adopts_percent = round($adopts_percent,2)." %" ;
        $fosters_percent = round($fosters_percent,2)." %" ;
        $volunteers_percent = round($volunteers_percent,2)." %" ;
        $surrenders_percent = round($surrenders_percent,2)." %" ;

        $petDraft_percent = round($petDraft_percent,2)." %" ;
        $petAvailable_percent = round($petAvailable_percent,2)." %" ;
        $petFoster_percent = round($petFoster_percent,2)." %" ;
        $pet_dog_percent = round($pet_dog_percent,2)." %" ;
        $pet_cat_percent = round($pet_cat_percent,2)." %" ;
        $foster_dog_percent = round($foster_dog_percent,2)." %" ;
        $foster_cat_percent = round($foster_cat_percent,2)." %" ;
        $available_dog_percent = round($available_dog_percent,2)." %" ;
        $available_cat_percent = round($available_cat_percent,2)." %" ;
        $draft_dog_percent = round($draft_dog_percent,2)." %" ;
        $draft_cat_percent = round($draft_cat_percent,2)." %" ;


        if(!empty($data)){
            $items = $data;
            Excel::create('Report', function($excel) use($items,$nameProvider,$reportDate,$pet,$petDraft,$petAvailable,$petFoster,$pet_cat,$pet_dog,$petAvailable_percent,$petDraft_percent,$petFoster_percent,$pet_cat_percent,$pet_dog_percent,$draft_cat,$draft_dog,$available_dog,$available_cat,$foster_dog,$foster_cat,$foster_dog_percent,$foster_cat_percent,$available_cat_percent,$available_dog_percent,$draft_dog_percent,$draft_cat_percent,$total_request,$total_approve,$total_pending,$total_reject,$adopts,$volunteers,$fosters,$surrenders,$adopts_approve,$volunteers_approve,$fosters_approve,$surrenders_approve,$adopts_pending,$volunteers_pending,$fosters_pending,$surrenders_pending,$fosters_reject,$surrenders_reject,$volunteers_reject,$adopts_reject,$adopts_percent,$volunteers_percent,$fosters_percent,$surrenders_percent,$total_approve_percent,$total_pending_percent,$total_reject_percent,$adopts_approve_percent,$volunteers_approve_percent,$fosters_approve_percent,$surrenders_approve_percent,$adopts_pending_percent,$volunteers_pending_percent,$fosters_pending_percent,$surrenders_pending_percent,$adopts_reject_percent,$volunteers_reject_percent,$fosters_reject_percent,$surrenders_reject_percent) {
                $excel->sheet('Report', function($sheet) use($items,$nameProvider,$reportDate,$pet,$petDraft,$petAvailable,$petFoster,$pet_cat,$pet_dog,$petAvailable_percent,$petDraft_percent,$petFoster_percent,$pet_cat_percent,$pet_dog_percent,$draft_cat,$draft_dog,$available_dog,$available_cat,$foster_dog,$foster_cat,$foster_dog_percent,$foster_cat_percent,$available_cat_percent,$available_dog_percent,$draft_dog_percent,$draft_cat_percent,$total_request,$total_approve,$total_pending,$total_reject,$adopts,$volunteers,$fosters,$surrenders,$adopts_approve,$volunteers_approve,$fosters_approve,$surrenders_approve,$adopts_pending,$volunteers_pending,$fosters_pending,$surrenders_pending,$fosters_reject,$surrenders_reject,$volunteers_reject,$adopts_reject,$adopts_percent,$volunteers_percent,$fosters_percent,$surrenders_percent,$total_approve_percent,$total_pending_percent,$total_reject_percent,$adopts_approve_percent,$volunteers_approve_percent,$fosters_approve_percent,$surrenders_approve_percent,$adopts_pending_percent,$volunteers_pending_percent,$fosters_pending_percent,$surrenders_pending_percent,$adopts_reject_percent,$volunteers_reject_percent,$fosters_reject_percent,$surrenders_reject_percent) {
                    $objDrawing = new PHPExcel_Worksheet_Drawing;
                    $objDrawing->setPath(public_path('smallLogo.png')); //your image path
                    $objDrawing->setCoordinates('A1');
                    $objDrawing->setWorksheet($sheet);
                    $sheet->cells('D1:D1', function($cells) {
                        $cells->setFontColor('#01aef2');
                        });
                    $sheet->cells('D2:D2', function($cells) {
                        $cells->setFontColor('#ff33cc');
                        });
                    $sheet->cells('A5:G5', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                        });
                    $sheet->cells('A11:I11', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                        });
                    $sheet->cells('A18:H18', function($cells) {
                        $cells->setBackground('#01aef2'); 
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontWeight('bold');
                         /*$cells->setAlignment('center');
                        $cells->setValignment('center');
                         $cells->setFontSize(16);*/
 
                        });
                     $sheet->fromArray(array(
                        array('','','Performance Report',$nameProvider),
                        array('','','Date',$reportDate),
                        array('','','','','',''),
                        array('','','','','',''),
                        array('','','','Cats','','Dogs',''),
                        array('Total Pets',"$pet",'',"$pet_cat","$pet_cat_percent","$pet_dog","$pet_dog_percent"),
                        array('Draft',"$petDraft","$petDraft_percent","$draft_cat","$draft_cat_percent","$draft_dog","$draft_dog_percent"),
                        array('Available',"$petAvailable","$petAvailable_percent","$available_cat","$available_cat_percent","$available_dog","$available_dog_percent"),
                        array('Foster',"$petFoster","$petFoster_percent","$foster_cat","$foster_cat_percent","$foster_dog","$foster_dog_percent"),
                        array('','','','','',''),
                        array('','','','Approved','','Pending','','Rejected',''),
                        array('Total Request',"$total_request","","$total_approve","$total_approve_percent","$total_pending","$total_pending_percent","$total_reject","$total_reject_percent"),
                        array('Adopt',"$adopts","$adopts_percent","$adopts_approve","$adopts_approve_percent","$adopts_pending","$adopts_pending_percent","$adopts_reject","$adopts_reject_percent"),
                        array('Volunteer',"$volunteers","$volunteers_percent","$volunteers_approve","$volunteers_approve_percent","$volunteers_pending","$volunteers_pending_percent","$volunteers_reject","$volunteers_reject_percent"),
                        array('Foster',"$fosters","$fosters_percent","$fosters_approve","$fosters_approve_percent","$fosters_pending","$fosters_pending_percent","$fosters_reject","$fosters_reject_percent"),
                        array('Surrender',"$surrenders","$surrenders_percent","$surrenders_approve","$surrenders_approve_percent","$surrenders_pending","$surrenders_pending_percent","$surrenders_reject","$surrenders_reject_percent"),
                        array('','','','','',''),
                        array('Request Number','Pet Name','Pet Species','Human Name','Request Type','Request Status', 'Date of Request','Date of Reply')
                    ), null, 'A1', false, false);
                    $sheet->fromArray($items, null, 'A1', false, false);
                });
            })->export('xls');
        }else{
            return back()->with('message', 'No data found.');;
        }
    }
}
