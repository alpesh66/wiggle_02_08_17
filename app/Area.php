<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $hidden = ['updated_at', 'created_at'];

    protected $fillable = ['country_id', 'name', 'name_ar','status'];

    public function provider()
    {
        return $this->hasOne('\App\Provider');
    }

    public function scopeActive($q)
    {
        return $this->where('status',1);
    }
}
