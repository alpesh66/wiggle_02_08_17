<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    //

    protected $fillable = ['provider_id', 'name', 'name_ar','image','number','address','biodata','status','title','email'];
    protected $hidden = ['created_at', 'updated_at', 'pivot', 'type_id'];
    
    public function techtime()
    {
    	return $this->hasMany('App\TechnicianTiming')->orderBy('id');
    }

    public function techbreaktime()
    {
    	return $this->hasMany('App\TechnicianBreak')->orderBy('id');
    }

    public function techleave()
    {
        return $this->hasMany('App\TechnicianLeave');
    }

    public function service()
    {
        return $this->belongsToMany('App\Service');
    }

    public function category()
    {
        return $this->belongsTo('\App\Category');
    }

    public function scopeProviderTechnicians($q, $provider_id)
    {
        return $q->where('provider_id', $provider_id);
    }

    public function scopeActiveTechs($q)
    {
        return $q->where('status', 1);
    }

    public function scopeActiveTechnicianList($q)
    {
        $provider_id = session('provider_id');
        return $q->ProviderTechnicians($provider_id)->ActiveTechs()->pluck('name', 'id');
    }

    public function getImageAttribute($photo)
    {
        return ($photo == '')? '' : url('uploads/technicians').'/'.$photo;
    }

    public function scopeOnlyCalendarTechnicians($q,$technicians)
    {
        $provider_id = session('provider_id');
        return $q->ProviderTechnicians($provider_id)->ActiveTechs()->whereIn('id',$technicians)->pluck('name', 'id');
    }
}
