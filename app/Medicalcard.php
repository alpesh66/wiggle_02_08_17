<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicalcard extends Model
{
    protected $fillable = ['pet_id', 'givenon', 'vet', 'vaccination_type_id','institution', 'photo'];

    protected $hidden = ['created_at', 'updated_at'];
    public function pet()
    {
        return $this->belongsTo('\App\Pet');
    }

    public function getPhotoAttribute($photo)
    {
        return ($photo == '')? '' : url('uploads/pets/medicalcard').'/'.$photo;
    }
    
    public function vaccination_type(){
        return $this->belongsTo('App\VaccinationTypes', 'vaccination_type_id')->select(['id','type','breed','vaccination_duration']);
    }
}
