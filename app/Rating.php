<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    //
    protected $fillable = ['provider_id', 'customer_id', 'rate','review','status',' created_at','updated_at'];

    public function customer()
    {
        return $this->belongsTo('\App\Customer');
    }
}
