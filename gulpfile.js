var elixir = require('laravel-elixir');

elixir.config.assetsDir = 'resources/assets/'; 
var paths = {
    'jquery': 'bower/jquery/dist',
    'bootstrap': 'bower/bootstrap/dist',
    'fontawesome': 'bower/font-awesome',

}


elixir(function(mix) {

    
    mix.styles([
        'bootstrap.min.css','font-awesome.min.css','custom.min.css', 'nprogress.css', 
    ]);

    mix.scripts(
        ['jquery.min.js','bootstrap.min.js','fastclick.js','nprogress.js','custom.js']
    );

    // mix.copy(paths.bootstrap + '/fonts/**', 'public/fonts')

    // .scripts([
    // paths.jquery + "/jquery.min.js",
    // paths.bootstrap + "/js/bootstrap.min.js",


    // ],'public/css/all.js', './')

    // .styles([

    // paths.bootstrap + "/css/bootstrap.css",
    // paths.fontawesome + "/css/font-awesome.css",
    
    
    // ],'public/css/all.css', './');

    mix.version(['css/all.css','js/all.js']);
});
