<?php
return [
    'SUCCESS'   => 0,
    'ERROR'     => 1,
    'Validation_ERROR'     => 1,

    'Valid'     => 'Valid Response',
    'Valid_AR'     => 'Valid Response',

    'Invalid'     => 'Invalid Response',
    'Invalid_AR'     => 'Invalid Response',

    'Deleted' => 'Deleted Successfully',
    'Deleted_AR' => 'Deleted Successfully',

    'Desable' => 'Desable Successfully',
    'Desable_AR' => 'Desable Successfully',

    'EmptyData' => 'Its Empty Dataset',
    'EmptyData_AR' => 'Its Empty Dataset',

    'Invalid_User' => 'No User Found',
    'Invalid_User_AR' => 'No User Found',
    
    'InActive_User' => 'You don\'t have an active account',
    'InActive_User_AR' => 'You don\'t have an active account',

    'Invalid_credentials' => 'Email is not register! Please register your email',
    'Invalid_credentials_AR' => 'Email is not register! Please register your email',

    'PasswordReset' => 'Password reset email sent',
    'PasswordReset_AR' => 'Password reset email sent',

    'PasswordResetError' => 'This email doesn\'t exists!',
    'PasswordResetError_AR' => 'This email doesn\'t exists!',

    'ItsWeekOff' => 'This Service Provider is having Week Off Today',
    'ItsWeekOff_AR' => 'This Service Provider is having Week Off Today',

    'TechnicianWeekOff' => 'Technician is having Weekoff',
    'TechnicianWeekOff_AR' => 'Technician is having Weekoff',

    'BookingAppointmentCanceled' => 'Appointment has been canceled successfully',
    'BookingAppointmentCanceled_AR' => 'Appointment has been canceled successfully',

    'BookingAppointmentConfirm' => 'Appointment has been Confirmed successfully',
    'BookingAppointmentConfirm_AR' => 'Appointment has been Confirmed successfully',
    
    'TechnicianLeave' => 'Technician is on Leave',
    'TechnicianLeave_AR' => 'Technician is on Leave',

    'StoreClosed' => 'Store is Closed',
    'StoreClosed_AR' => 'Store is Closed',

    'NoSlotsAvailable' => 'No slots available for this day',
    'NoSlotsAvailable_AR' => 'No slots available for this day',

    'slotAvailable' => 'Following slots available',
    'slotAvailable_AR' => 'Following slots available',

    'ProviderFullDayHoliday' => 'Provider not available due to Holiday',
    'ProviderFullDayHoliday_AR' => 'Provider not available due to Holiday',

    'TechnicianFullDayHoliday' => 'Technician is on Holiday',
    'TechnicianFullDayHoliday_AR' => 'Technician is on Holiday',

    'noProviderFound' => 'No Provider Found',
    'noProviderFound_AR' => 'No Provider Found',
    
    'noBookingsAvailable' => 'No Bookings available yet',
    'noBookingsAvailable_AR' => 'No Bookings available yet',

    'petsizeNotServiced' => 'This pet size or specie is not serviced',
    'petsizeNotServiced_AR' => 'This pet size or specie is not serviced',
    
    'noRatingsAvailable' => 'Be the first to add ratings and review',
    'noRatingsAvailable_AR' => 'Be the first to add ratings and review',
    
    'RatingsSuccess'=>'Thank you for submitting review and ratings. Once your review is approved will be published soon',
    'RatingsSuccess_AR'=>'Thank you for submitting ratings'
];