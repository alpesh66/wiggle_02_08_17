<?php

return [
    'app_name'=>"Wiggle",
    'confirm_appointment' => [
        "en" => [
            "title" => "Appointment Confirmed",
            "message" => "Your appointment booked successfully"
        ],
        "ar" => [
            "title" => "تم تأكيد الموعد",
            "message" => "تم حجز موعدك بنجاح"
        ],
    ],
    'unconfirm_appointment' => [
        "en" => [
            "title" => "Appointment Unconfirmed",
            "message" => "Your appointment has not been confirmed"
        ],
        "ar" => [
            "title" => "موعد غير مؤكد",
            "message" => "لم يتم تأكيد موعدك بواسطة المشرف"
        ],
    ],
    'wiggle_admin' => [
        "en" => [
            "title" => "Wiggle Admin",
            "message" => "News from Admin"
        ],
        "ar" => [
            "title" => "تذبذب المشرف",
            "message" => "الأخبار من المشرف"
        ],
    ],
    'cancel_appointment' => [
        "en" => [
            "title" => "Appointment Cancelled",
            "message" => "Your appointment has been cancelled"
        ],
        "ar" => [
            "title" => "تم إلغاء التعيين",
            "message" => "تم إلغاء موعدك بواسطة المشرف"
        ],
    ],
    
    'appointment_remider' => [
        "en" => [
            "title" => "Confirm your Appointment",
            "message" => "Your appointment requires confirmation!"
        ],
        "ar" => [
            "title" => "تذكير التعيين",
            "message" => "يتطلب موعدك تأكيد!"
        ],
    ],
    'vaccination_reminder' => [
        "en" => [
            "title" => "Vaccination Reminder",
            "message" => "Your pet is due for a vaccination. Hurry up, book an appointement."
        ],
        "ar" => [
            "title" => "تذكير التطعيم",
            "message" => "ونحن ننصح لتطعيم المفضل لديك. تحقق مع الطبيب البيطري."
        ],
    ],
    'gromming_reminder' => [
        "en" => [
            "title" => "Gromming Reminder",
            "message" => "We recommend you for gromming your pet. Check with your gromming."
        ],
        "ar" => [
            "title" => "تذكير غرومينغ",
            "message" => "ونحن ننصح لتطعيم المفضل لديك. تحقق مع الطبيب البيطري."
        ],
    ],
    'pending_adoption' => [
        "en" => [
            "title" => "Adoption Request Pending",
            "message" => "Your adoption request approval is pending",
        ],
        "ar" => [
            "title" => "طلب التبني معلق",
            "message" => "طلب اعتمادك في انتظار الموافقة"
        ],
    ],
    'approved_adoption' => [
        "en" => [
            "title" => "Adoption Approved",
            "message" => "Your adoption request has been successfully approved",
        ],
        "ar" => [
            "title" => "اعتماد معتمد",
            "message" => "تمت الموافقة على طلب اعتمادك بنجاح"
        ],
    ],
    'reject_adoption' => [
        "en" => [
            "title" => "Adoption Rejected",
            "message" => "Your adoption request has been rejected"
        ],
        "ar" => [
            "title" => "رفض رفض",
            "message" => "تم رفض طلب اعتمادك من قبل المشرف"
        ],
    ],
    'pending_foster' => [
        "en" => [
            "title" => "Foster Request Pending",
            "message" => "Your foster request approval is pending",
        ],
        "ar" => [
            "title" => "فوستر طلب معلق",
            "message" => "طلب اعتمادك في انتظار الموافقة"
        ],
    ],
    'approved_foster' => [
        "en" => [
            "title" => "Foster Approved",
            "message" => "Your foster request has been successfully approved",
        ],
        "ar" => [
            "title" => "فوستر المعتمدة",
            "message" => "تمت الموافقة على طلب الحاضنة بنجاح"
        ],
    ],
    'reject_foster' => [
        "en" => [
            "title" => "Foster Rejected",
            "message" => "Your foster request has been rejected"
        ],
        "ar" => [
            "title" => "فوستر مرفوضة",
            "message" => "تم رفض طلب الحاضنة من قبل المشرف"
        ],
    ],
    'pending_volunteer' => [
        "en" => [
            "title" => "Volunteer Request Pending",
            "message" => "Your volunteer request approval is pending",
        ],
        "ar" => [
            "title" => "طلب متطوع معلق",
            "message" => "طلب المتطوعين في انتظار الموافقة"
        ],
    ],
    'approved_volunteer' => [
        "en" => [
            "title" => "Volunteer Approved",
            "message" => "Your volunteer request has been successfully approved",
        ],
        "ar" => [
            "title" => "متطوع معتمد",
            "message" => "تمت الموافقة على طلب المتطوعين بنجاح"
        ],
    ],
    'reject_volunteer' => [
        "en" => [
            "title" => "Volunteer Rejected",
            "message" => "Your volunteer request has been rejected"
        ],
        "ar" => [
            "title" => "متطوع رفض",
            "message" => "تم رفض طلب المتطوع من قبل المشرف"
        ],
    ],
    'pending_surrender' => [
        "en" => [
            "title" => "Surrender Request Pending",
            "message" => "Your surrender request approval is pending",
        ],
        "ar" => [
            "title" => "استسلام طلب معلق",
            "message" => "طلب الاستسلام في انتظار الموافقة"
        ],
    ],
    'approved_surrender' => [
        "en" => [
            "title" => "Surrender Approved",
            "message" => "Your surrender request has been successfully approved",
        ],
        "ar" => [
            "title" => "تم قبول الاستسلام",
            "message" => "تمت الموافقة على طلب التسليم بنجاح"
        ],
    ],
    'reject_surrender' => [
        "en" => [
            "title" => "Surrender Rejected",
            "message" => "Your surrender request has been rejected"
        ],
        "ar" => [
            "title" => "تم رفض الاستسلام",
            "message" => "تم رفض طلب الاستسلام من قبل المشرف"
        ],
    ],
    'pending_kennel' => [
        "en" => [
            "title" => "Kennel Request Pending",
            "message" => "Your kennel request approval is pending",
        ],
        "ar" => [
            "title" => "طلب بيت الكلب في انتظار",
            "message" => "طلب طلب الإقامة في انتظار الموافقة"
        ],
    ],
    'approved_kennel' => [
        "en" => [
            "title" => "Kennel Approved",
            "message" => "Your kennel request has been successfully approved",
        ],
        "ar" => [
            "title" => "وافق بيت الكلب",
            "message" => "تمت الموافقة على طلب بيتك بنجاح"
        ],
    ],
    'reject_kennel' => [
        "en" => [
            "title" => "Kennel Rejected",
            "message" => "Your kennel request has been rejected"
        ],
        "ar" => [
            "title" => "رفض البيت",
            "message" => "تم رفض طلب بيتك بواسطة المشرف"
        ],
    ],
    'missyou_response' => [
        "en" => [
            "title" => "Miss You Response",
            "message" => "Your miss you request has been replied"
        ],
        "ar" => [
            "title" => "الآنسة أنت رد",
            "message" => "مدير تعطيك الرد على يغيب لك طلب"
        ],
    ],
];


