<?php
return [
    'SUCCESS'   => 0,
    'ERROR'     => 1,
    'Validation_ERROR'     => 101,

    'Valid'     => 'Valid Response',
    'Valid_AR'     => 'Valid Response',

    'Invalid'     => 'Invalid Response',
    'Invalid_AR'     => 'Invalid Response',

    'EmptyData' => 'Its Empty Dataset',
    'EmptyData_AR' => 'Its Empty Dataset',

    'Invalid_User' => 'No User Found',
    'Invalid_User_AR' => 'No User Found',
    
    'InActive_User' => 'You don\'t have an active account',
    'InActive_User_AR' => 'You don\'t have an active account',

    'PasswordReset' => 'Password reset email sent',
    'PasswordReset_AR' => 'Password reset email sent',

    'PasswordResetError' => 'This email doesn\'t exists!',
    'PasswordResetError_AR' => 'This email doesn\'t exists!',
];