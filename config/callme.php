<?php
return [
    'ADMIN_TYPE'    => 0,
    'PROVIDER_TYPE' => 1,
    'SOCIETY_TYPE'  => 2,
    
    'VETERNARY'     => 1,
    'GROOMER'       => 2,
    'WALKER'        => 3,
    'TRAINER'       => 4,
    'KENNEL'        => 5,

    'FromEmail' => 'support@wiggle.com',
    'FromName'  => 'Wiggle',
    'EmailSubjecttoCustomer' => 'Wiggle - Your booking details',
    'EmailSubjecttoProvider' => 'Wiggle - New Booking arrived',
];