<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdoptPetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adopt_pet', function (Blueprint $table) {
            //$table->increments('id');
            //$table->timestamps();
            $table->integer('adopt_id')->unsigned();
            $table->integer('pet_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adopt_pet');
    }
}
