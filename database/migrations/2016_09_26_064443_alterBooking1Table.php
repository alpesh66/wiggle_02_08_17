<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBooking1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('appointmentdate');
            $table->dateTime('start')->after('service_id');
            $table->dateTime('end')->after('start');
            $table->tinyInteger('type_id')->after('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->date('appointmentdate');
            $table->dropColumn(['start','end','type_id']);
        });
    }
}
