<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->string('name_ar')->after('name');
            $table->string('about')->after('name_ar');
            $table->string('about_ar')->after('about');
            $table->string('starttime')->after('about_ar');
            $table->string('endtime')->after('starttime');
            $table->string('image')->after('endtime');
            $table->string('website')->after('type');
            $table->string('facebook')->after('website');
            $table->string('twitter')->after('facebook');
            $table->string('instagram')->after('twitter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
            $table->dropColumn('name_ar');
            $table->dropColumn('about');
            $table->dropColumn('about_ar');
            $table->dropColumn('starttime');
            $table->dropColumn('endtime');
            $table->dropColumn('image');
            $table->dropColumn('website');
            $table->dropColumn('facebook');
            $table->dropColumn('twitter');
            $table->dropColumn('instagram');
        });
    }
}
