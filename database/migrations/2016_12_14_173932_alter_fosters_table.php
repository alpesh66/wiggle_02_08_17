<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fosters', function (Blueprint $table) {
            $table->tinyInteger('breed_id')->after('is_read')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fosters', function (Blueprint $table) {
            $table->dropColumn('breed_id');
        });
    }
}
