<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updatesurrender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surrenders', function (Blueprint $table) {
            Schema::table('adopts', function (Blueprint $table) {
            $table->integer('parent_id')->after('sender')->default(0);
            });
            Schema::table('surrenders', function (Blueprint $table) {
                $table->integer('parent_id')->after('sender')->default(0);
            });
            Schema::table('volunteers', function (Blueprint $table) {
                $table->integer('parent_id')->after('sender')->default(0);
            });
            Schema::table('fosters', function (Blueprint $table) {
                $table->integer('parent_id')->after('sender')->default(0);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surrenders', function (Blueprint $table) {
            Schema::table('adopts', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
        Schema::table('surrenders', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
        Schema::table('volunteers', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
        Schema::table('fosters', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
        });
    }
}
