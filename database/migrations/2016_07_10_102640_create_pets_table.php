<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('dob');
            $table->string('breed');
            $table->boolean('gender');
            $table->string('height');
            $table->string('picture');
            $table->string('temperament');
            $table->string('origin');
            $table->tinyInteger('size')->default(1);
            $table->string('chipno');
            $table->string('weight');
            $table->string('grooming');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pets');
    }
}
