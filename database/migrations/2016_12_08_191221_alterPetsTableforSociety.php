<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPetsTableforSociety extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pets', function (Blueprint $table) {
            $table->string('adopt_comments')->after('grooming');
            $table->string('disabled_comments')->after('adopt_comments');
            $table->boolean('isadopted')->after('grooming')->default(0);
            $table->integer('likes')->after('isadopted')->default(0);
            $table->date('cough')->after('likes');
            $table->boolean('missyou')->after('cough')->default(0);
            $table->boolean('status')->after('missyou')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pets', function (Blueprint $table) {
            $table->dropColumn(['adopt_comments', 'isadopted','likes','cough','missyou', 'disabled_comments','status']);
        });
    }
}
