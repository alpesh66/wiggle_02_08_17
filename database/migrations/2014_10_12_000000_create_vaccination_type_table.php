<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaccinationTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccination_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->tinyInteger('breed')->comment('1=Dog, 2=Cat');
            $table->string('vaccination_duration')->comment('Duration in week');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vaccination_type');
    }
}
