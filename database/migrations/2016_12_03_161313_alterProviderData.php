<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProviderData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->bigInteger('count_votes')->unsigned()->after('status')->comment('Number of customers gave review');
            $table->bigInteger('stars')->unsigned()->after('status')->comment('total stars each customer gave');
            $table->bigInteger('total_booked')->unsigned()->after('status');
            $table->bigInteger('total_cancelled')->unsigned()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn(['count_votes', 'stars', 'total_booked', 'total_cancelled']);
        });
    }
}
