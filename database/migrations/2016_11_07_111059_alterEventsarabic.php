<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEventsarabic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('name_ar')->after('name');
            $table->text('desc_ar')->after('desc');
            $table->string('image')->after('address');
            $table->string('image_ar')->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['name_ar', 'desc_ar', 'image', 'image_ar']);
        });
    }
}
