<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFosterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fosters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->date('startdate');
            $table->date('enddate');
            $table->integer('provider_id');
            $table->string('title');
            $table->text('description');
            $table->boolean('is_read')->default('1');
            $table->tinyInteger('sender')->comment('1 = Customer,2 = Provider');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fosters');
    }
}
