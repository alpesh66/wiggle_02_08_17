<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updatesocietytable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adopts', function (Blueprint $table) {
            $table->tinyInteger('status')->after('sender')->default(0);
        });
        Schema::table('surrenders', function (Blueprint $table) {
            $table->tinyInteger('status')->after('sender')->default(0);
        });
        Schema::table('volunteers', function (Blueprint $table) {
            $table->tinyInteger('status')->after('sender')->default(0);
        });
        Schema::table('fosters', function (Blueprint $table) {
            $table->tinyInteger('status')->after('sender')->default(0);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adopts', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('surrenders', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('volunteers', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('fosters', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
