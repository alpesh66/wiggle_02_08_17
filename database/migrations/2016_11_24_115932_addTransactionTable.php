<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->integer('customer_id')->unsigned();
            $table->string('device_id');
            $table->string('PaymentID');
            $table->boolean('result');
            $table->string('PostDate');
            $table->string('TranID');
            $table->string('Auth');
            $table->string('Ref');
            $table->string('TrackID');
            $table->ipAddress('ip_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
