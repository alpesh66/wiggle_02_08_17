<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePtypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->tinyInteger('ptype')->nullable()->after('status')->comment('Type of users as we have diff providers');
        });

        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->tinyInteger('ptype')->nullable()->after('name')->comment('1 means providers, 2 = societies');
        });

        Schema::table('types', function (Blueprint $table) {
            $table->tinyInteger('ptype')->nullable()->after('dis_name')->comment('1 = providers, 2 = societies');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->tinyInteger('ptype')->nullable()->after('display_name')->comment('1 = providers, 2 = societies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('ptype');
        });

        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn('ptype');
        });

        Schema::table('types', function (Blueprint $table) {
            $table->dropColumn('ptype');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('ptype');
        });


    }
}
