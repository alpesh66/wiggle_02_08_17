<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomerAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('latitude')->after('mobile')->default('');
            $table->string('longitude')->after('latitude')->default('');
            $table->string('block')->after('longitude')->default('');
            $table->string('street')->after('block')->default('');
            $table->string('judda')->after('street')->default('');
            $table->string('house')->after('judda')->default('');
            $table->string('apartment')->after('house')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn(['latitude', 'longitude', 'block', 'street','judda','house','apartment']);
        });
    }
}
