<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateKennel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kennels', function (Blueprint $table) {
          $table->tinyInteger('status')->after('sender')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kennels', function (Blueprint $table) {
                  $table->dropColumn('parent_id');   
        });
    }
}
