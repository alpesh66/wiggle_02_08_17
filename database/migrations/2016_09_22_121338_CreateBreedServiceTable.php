<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreedServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breed_service', function (Blueprint $table) {

            $table->integer('breed_id')->unsigned()->index();
            $table->integer('size_id')->unsigned()->index();
            $table->string('price');
            $table->string('servicetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('breed_service');
    }
}
