<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdoptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adopts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('provider_id');
            $table->string('title');
            $table->text('description');
            $table->boolean('is_read')->default('1');
            $table->tinyInteger('sender')->comment('1 = Customer,2 = Provider');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adopts');
    }
}
