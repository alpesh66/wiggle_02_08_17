<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('block',100);
            $table->string('floor',100);
            $table->string('judda',100);
            $table->string('building',100);
            $table->string('street',100);
            $table->string('area',100);
            $table->string('email',100);
            $table->string('latitude',100);
            $table->string('longitude',100);
            $table->string('contact',100);
            $table->tinyInteger('status');
            $table->tinyInteger('type')->comment('1:Gromming, 2:petStores, 3:Socities, 4:Dog friendly areas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
