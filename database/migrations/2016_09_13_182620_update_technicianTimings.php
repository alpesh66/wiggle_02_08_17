<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTechnicianTimings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('technician_timings', function (Blueprint $table) {
            $table->tinyInteger('weekoff')->after('weekday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technician_timings', function (Blueprint $table) {
            $table->dropColumn('weekoff');
        });
    }
}
