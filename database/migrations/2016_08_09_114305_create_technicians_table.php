<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->unsigned();
            $table->string('name', 128);
            $table->string('name_ar', 128);
            $table->string('image', 128);
            $table->string('number', 32);
            $table->string('address', 128);
            $table->text('biodata');
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::create('technician_timings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('technician_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->string('weekday',128);
            $table->time('starttime');
            $table->time('endtime');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::create('technician_breaks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('technician_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->string('weekday',128);
            $table->tinyInteger('number');
            $table->time('breakstart');
            $table->time('breakend');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::create('technician_holidays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('technician_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->string('weekday',128);
            $table->time('breakstart');
            $table->time('breakend');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::create('technician_leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('technician_id')->unsigned();
            $table->integer('provider_id')->unsigned();
            $table->string('weekday',128);
            $table->date('start');
            $table->date('toend');
            $table->time('breakstart');
            $table->time('breakend');
            $table->tinyInteger('type');
            $table->tinyInteger('status');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('technicians');
        Schema::drop('technician_timings');
        Schema::drop('technician_breaks');
        Schema::drop('technician_holidays');
        Schema::drop('technician_leaves');
    }
}
