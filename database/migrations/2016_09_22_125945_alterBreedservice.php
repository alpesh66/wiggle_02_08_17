<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBreedservice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breed_service', function (Blueprint $table) {
            $table->integer('service_id')->after('breed_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breed_service', function (Blueprint $table) {
            $table->dropColumn('service_id');
        });
    }
}
