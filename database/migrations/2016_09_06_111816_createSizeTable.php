<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('status');
            $table->timestamps();
        });

        $date = new \Carbon\Carbon;
        DB::table('sizes')->insert(array(
            array('name' => 'All', 'status' => 1, 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'L', 'status' => 1, 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'M', 'status' => 1, 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'S', 'status' => 1, 'created_at' => $date, 'updated_at' => $date),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sizes');
    }
}
