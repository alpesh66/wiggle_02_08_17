<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTechnicianHolidays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('technician_holidays', function (Blueprint $table) {
            $table->date('start')->after('weekday');
            $table->date('end')->after('start');
            $table->tinyInteger('type')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technician_holidays', function (Blueprint $table) {
            $table->dropColumn(['start','end','type']);
        });
    }
}
