<?php

use Illuminate\Database\Seeder;

use App\Breed;

class breedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $breed = new Breed();
        $breed->name = 'Cats';
        $breed->name_ar = 'Cat';
        $breed->status = '1';
        $breed->save();

        $breed = new Breed();
        $breed->name = 'Dogs';
        $breed->name_ar = 'Dog';
        $breed->status = '1';
        $breed->save();
    }
}
