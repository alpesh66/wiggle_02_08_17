<?php

use Illuminate\Database\Seeder;

use App\Type;

class typesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $type = new Type();
        $type->name = 'vet';
        $type->dis_name = 'Veternary';
        $type->ptype = 1;
        $type->save();

        $type = new Type();
        $type->name = 'groomer';
        $type->dis_name = 'Groomer';
        $type->ptype = 1;
        $type->save();

        $type = new Type();
        $type->name = 'walker';
        $type->dis_name = 'Walker';
        $type->ptype = 1;
        $type->save();

        $type = new Type();
        $type->name = 'trainer';
        $type->dis_name = 'Trainer';
        $type->ptype = 1;
        $type->save();

        $type = new Type();
        $type->name = 'kennel';
        $type->dis_name = 'Kennel';
        $type->ptype = 1;
        $type->save();

        $type = new Type();
        $type->name = 'adopt';
        $type->dis_name = 'Adopt';
        $type->ptype = 2;
        $type->save();

        $type = new Type();
        $type->name = 'foster';
        $type->dis_name = 'Foster';
        $type->ptype = 2;
        $type->save();

        $type = new Type();
        $type->name = 'volunteer';
        $type->dis_name = 'Volunteer';
        $type->ptype = 2;
        $type->save();

        $type = new Type();
        $type->name = 'surrender';
        $type->dis_name = 'Surrender';
        $type->ptype = 2;
        $type->save();

        $type = new Type();
        $type->name = 'initiative';
        $type->dis_name = 'Initiatives';
        $type->ptype = 2;
        $type->save();

        $type = new Type();
        $type->name = 'kennels';
        $type->dis_name = 'Kennel';
        $type->ptype = 2;
        $type->save();

    }
}
