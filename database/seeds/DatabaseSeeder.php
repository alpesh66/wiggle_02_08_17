<?php

use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsAction::class);
        $this->call(breedsSeeder::class);
        $this->call(typesSeeder::class);
    }
}
