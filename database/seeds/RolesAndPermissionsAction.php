<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RolesAndPermissionsAction extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = new \Carbon\Carbon;

        $permissions = array(
            array('name' => 'super-dashboard', 'display_name' => 'Manage Dashboard', 'ptype' => 9, 'description' => 'Manage all the Dashboard Items', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-admin', 'display_name' => 'Manage Admin Users', 'ptype' => 0, 'description' => 'Manage the User admin', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-customer', 'display_name' => 'Manage the Customers', 'ptype' => 0, 'description' => 'Manage the customer Module', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-category', 'display_name' => 'Manage Category', 'ptype' => 0, 'description' => 'Manage Categories', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-cms', 'display_name' => 'Manage CMS', 'ptype' => 0, 'description' => 'Manage CMS Pages', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-provider', 'display_name' => 'Manage Provider Admin', 'ptype' => 0, 'description' => 'Manage Providers Data', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-store', 'display_name' => 'Manage the Stores', 'ptype' => 0, 'description' => 'Manage Store Data', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-dashboard', 'display_name' => 'Manage Dashboard', 'ptype' => 9, 'description' => 'Manage the Dashboard ', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-vet', 'display_name' => 'Manage Veternary', 'ptype' => 1, 'description' => 'Manage Veternary', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-groomer', 'display_name' => 'Manage Groomer', 'ptype' => 1, 'description' => 'Manage Groomer', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-walker', 'display_name' => 'Manage Walker', 'ptype' => 1, 'description' => 'Manage Walker', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-trainer', 'display_name' => 'Manange Trainer', 'ptype' => 1, 'description' => 'Manage Trainer', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-kennel', 'display_name' => 'Manage Kennel', 'ptype' => 1, 'description' => 'Manage Kennel', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-dashboard', 'display_name' => 'Manage Dashboard', 'ptype' => 9, 'description' => 'Manage Dasbhoard', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-adopt', 'display_name' => 'Manage Adopt', 'ptype' => 2, 'description' => 'Manage Adopt', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-foster', 'display_name' => 'Manage Foster', 'ptype' => 2, 'description' => 'Manage Foster', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-vol', 'display_name' => 'Manage Volunteer', 'ptype' => 2, 'description' => 'Manage Volunteer', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-surr', 'display_name' => 'Manage Surrender', 'ptype' => 2, 'description' => 'Manage Surrender', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-ini', 'display_name' => 'Manage Initiative', 'ptype' => 2, 'description' => 'Manage Initiative', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-admin', 'display_name' => 'Manage Users', 'ptype' => 1, 'description' => 'Manage Users', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-admin', 'display_name' => 'Manage Users', 'ptype' => 2, 'description' => 'Manage Users', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-service', 'display_name' => 'Manage Service', 'ptype' => 1, 'description' => 'Manage Service', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-event', 'display_name' => 'Manage Events', 'ptype' => 0, 'description' => 'Manage Events', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'provider-technician', 'display_name' => 'Manage Technician', 'ptype' => 1, 'description' => 'Manage Technicians', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-species', 'display_name' => 'Manage Species', 'ptype' => 0, 'description' => 'Manage Species', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'super-transaction', 'display_name' => 'Manage Transaction', 'ptype' => 0, 'description' => 'Manage Transaction', 'created_at' => $date, 'updated_at' => $date),
            array('name' => 'society-kennel', 'display_name' => 'Manage Kennel', 'ptype' => 2, 'description' => 'Manage Kennel', 'created_at' => $date, 'updated_at' => $date),
            );
        DB::table('permissions')->delete();
        
        $roles = new Role();
        $roles->name = 'super_admin';
        $roles->display_name = 'Super Admin';
        $roles->description = 'Super admin is having all the power of the website';
        $roles->save();

        //insert some dummy records
        DB::table('permissions')->insert($permissions);
        

        DB::table('permission_role')->insert(array(
            array('permission_id' => 1, 'role_id' => 1),
            array('permission_id' => 2, 'role_id' => 1),
            array('permission_id' => 3, 'role_id' => 1),
            array('permission_id' => 4, 'role_id' => 1),
            array('permission_id' => 5, 'role_id' => 1),
            array('permission_id' => 6, 'role_id' => 1),
            array('permission_id' => 7, 'role_id' => 1),
        ));

        $user = new User();
        $user->name = 'Simplified Informatics';
        $user->email = 'admin@si-kw.com';
        $user->password = bcrypt('admin123');
        $user->status = '1';
        $user->utype = '1';
        $user->provider_id = '0';
        $user->api_token = str_random(60);
        $user->save();

        $user->attachRole($roles);
    }

}
