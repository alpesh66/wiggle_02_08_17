<?php
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt('admin123'),
        'gender' => $faker->boolean,
        'address' => $faker->address,
        'dob' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'status' => 1,
        'area_id' => rand(1,120),
        'api_token' => str_random(60),
        'mobile' => '965-'.$faker->randomNumber,
    ];
});

$factory->define(App\Pet::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'breed' => $faker->randomElement($array = array ('1','2')),
        'gender' => $faker->boolean,
        'height' => rand(1,120),
        'temperament' => $faker->randomElement($array = array ('Obedient','Cute','Faithful')),
        'size' => $faker->randomElement($array = array ('2','3','4')),
        'dob' => $faker->dateTimeThisCentury->format('Y-m-d'),
    ];
});

$factory->define(App\Provider::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'ptype' => $faker->randomElement($array = array ('1','2')),
        'about' => $faker->text($maxNbChars = 200),
        'address' => $faker->address,
        'area' => rand(1,120),
        'email' => $faker->safeEmail,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'start' => $faker->dateTimeBetween('now', '+2 years')->format('Y-m-d'),
        'end' => $faker->dateTimeBetween('+1 years', '+2 years')->format('Y-m-d'),
        'status' => $faker->boolean,
        'contact' => '965-'.$faker->randomNumber,
    ];
});

$factory->define(App\Event::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'desc' => $faker->sentence,
        'weblink' => $faker->url,
        'address' => $faker->address,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'startdate' => $faker->dateTimeBetween('now', '+1 weeks')->format('Y-m-d H:i:s'),
        'enddate' => $faker->dateTimeBetween('+1 days', '+2 weeks')->format('Y-m-d H:i:s'),
        'status' => $faker->boolean,
        'area_id' => rand(1,120),
    ];
});

$factory->define(App\Rating::class, function (Faker\Generator $faker) {
    $customer_id = \App\Customer::where('status', 1)->pluck('id')->toArray();
    $provider_id = \App\Provider::wherePtype(1)->pluck('id')->toArray();
    return [
        'provider_id' => $faker->randomElement($provider_id),
        'customer_id' => $faker->randomElement($customer_id),
        'rate' => rand(1,5),
        'review' => $faker->sentence,
        'status' => 0,
    ];
});

$factory->define(App\Transaction::class, function (Faker\Generator $faker) {

    $customer = \App\Booking::pluck('customer_id')->toArray();
    return [
        'customer_id' => $faker->randomElement($customer),

        'result' => $faker->boolean,
    ];
});


